package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 10:13 AM
 * Determines if the transaction field has not been provided or if it is has is it a string that only contains
 * non-whitespace characters
 */
public class notTransactionField implements IAssertionFunction {
  private IStringResolver constant;

  public notTransactionField(IStringResolver constant) {
    this.constant = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField(constant.resolve(context, value));
    if (fieldValue == null || fieldValue instanceof IFinsurvContext.Undefined ||
         (fieldValue instanceof String && ((String)fieldValue).trim().length() == 0))
      return true;
    else
      return false;
  }
}
/*
function notTransactionField(field) {
  return function(context, value) {
    var fieldValue = context.getTransactionField(field);
    if (!fieldValue || (typeof fieldValue == "string" && fieldValue.trim().length == 0))
      return true;
    else
      return false;
  }
}
*/