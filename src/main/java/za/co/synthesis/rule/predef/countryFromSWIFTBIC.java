package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * User: jake
 * Date: 4/19/16
 * Time: 3:42 PM
 * Determines the country code from the SWIFTBIC in the given field value. Only if the country code is valid will
 * it be returned
 */
public class countryFromSWIFTBIC implements IDisplayFunction {
  private IStringResolver field;

  public countryFromSWIFTBIC(IStringResolver field) {
    this.field = field;
  }

  @Override
  public String execute(FinsurvContext context, Object value) {
    String fieldStr = field.resolve(context, value);
    Object swiftBICObj = context.getTransactionField(fieldStr);

    notEmpty notEmpty = new notEmpty();
    if (notEmpty.execute(context, swiftBICObj)) {
      String swiftBIC = swiftBICObj.toString();
      if (swiftBIC.length() >= 6) {
        String country = swiftBIC.substring(4, 6);
        if (context.getLookups().isValidCountryCode(country))
          return country;
      }
    }
    return "";
  }
}

/*
  _export.countryFromSWIFTBIC = function (field) {
    return function (context) {
      var swiftBIC = context.getTransactionField(field);

      if (_export.notEmpty(context, swiftBIC)) {
        if (swiftBIC.length >= 6) {
          var country = swiftBIC[4] + swiftBIC[5];
          if (context.lookups.isValidCountryCode(country))
            return country;
        }
      }
      return "";
    }
  }
*/