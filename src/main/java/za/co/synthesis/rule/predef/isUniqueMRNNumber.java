package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

public class isUniqueMRNNumber implements IAssertionFunction {
    public boolean execute(FinsurvContext context, Object value) {
        if(value == null)
            return true;

        int counter = 0;
        int startingIndex;
        for(int i = context.getCurrentMoneyInstance(); i < context.getMoneySize(); i++) {
            if (i == context.getCurrentMoneyInstance()) {
                startingIndex = context.getCurrentImportExportInstance();
            } else {
                startingIndex = 0;
            }
            for (int j = startingIndex; j < context.getImportExportSize(i); j++) {
                if (context.getImportExportField(i, j, "ImportControlNumber").equals(value)) {
                    counter++;
                }
            }
        }
        return counter > 1;
    }
}
