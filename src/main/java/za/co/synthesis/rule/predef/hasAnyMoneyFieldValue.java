package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.*;

/**
 * Created by jake on 11/2/16.
 */
public class hasAnyMoneyFieldValue implements IAssertionFunction {
  private IStringResolver field;
  private StringResolverList constants = null;

  public hasAnyMoneyFieldValue(IStringResolver field, IStringResolver constant) {
    this.field = field;
    this.constants = new StringResolverList();
    this.constants.add(constant);
  }

  public hasAnyMoneyFieldValue(IStringResolver field, StringResolverList constant) {
    this.field = field;
    this.constants = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    for (int moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
      Object fieldValue = context.getMoneyField(moneyInstance, field.resolve(context, value));
      if (fieldValue != null) {
        for (IStringResolver constant : constants) {
          String str = constant.resolve(context, value);
          if (fieldValue.equals(str))
            return true;
        }
      }
    }
    return false;
  }
}
/*
_export.hasAnyMoneyFieldValue = function hasAnyMoneyFieldValue(field, constant) {
    return function (context, value) {
      var moneyInstance;
      for (moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
        var fieldValue = context.getMoneyField(moneyInstance, field);
        if (fieldValue) {
          if (typeof constant === "string") {
            if (fieldValue === constant)
              return true;
          } else {
            for (var i = 0; i < constant.length; i++) {
              if (fieldValue === constant[i])
                return true;
            }
          }
        }
      }
      return false;
    }
  }}*/
