package za.co.synthesis.rule.support;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 4/9/16.
 */
public class FieldDisplay {
  private StringList fieldList;
  private List<DisplayRule> display;

  public FieldDisplay(String field) {
    this.fieldList = new StringList();
    this.fieldList.add(field);
    this.display = new ArrayList<DisplayRule>();
  }

  public FieldDisplay(StringList fieldList) {
    this.fieldList = fieldList;
    this.display = new ArrayList<DisplayRule>();
  }

  public FieldDisplay addDisplay(DisplayRule rule) {
    this.display.add(rule);
    return this;
  }

  public List<String> getFieldList() {
    return fieldList;
  }

  public List<DisplayRule> getDisplay() {
    return display;
  }

  public void setDisplay(List<DisplayRule> displayRules) {
    this.display = displayRules;
  }
}
