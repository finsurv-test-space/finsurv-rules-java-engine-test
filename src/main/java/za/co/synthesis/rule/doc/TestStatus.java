package za.co.synthesis.rule.doc;

public enum TestStatus {
  NoTestDefined,
  Passed,
  Failed
}
