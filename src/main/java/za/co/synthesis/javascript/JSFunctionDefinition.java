package za.co.synthesis.javascript;

/**
 * User: jake
 * Date: 4/24/16
 * Time: 8:53 PM
 * This is a definition of a function and opposed to a JSFunctionCall as it contains the definition of a function itself
 * as opposed to just being a call to a pre-defined function. In the spirit of the javascript classes in this package
 * the class essentially stores the data definitions within the class and is not meant to capture any code aspects that
 * a typical class would do. I.e. this is here to support the Asynchronous Module Definition (AMD) used for
 * JavaScript modules.
 */
public class JSFunctionDefinition implements IJSSnippetSupport {
  protected final JSSnippetSupport snippets = new JSSnippetSupport();
  protected final String name;
  protected JSArray parameters;
  protected final JSScope scope;
  protected Object returned;

  public JSFunctionDefinition(String name, JSScope parentScope) {
    this.name = name;
    this.parameters = new JSArray();
    this.scope = new JSScope(parentScope);
  }

  public JSFunctionDefinition(String name, JSArray parameters, JSScope parentScope) {
    this.name = name;
    if (parameters != null)
      this.parameters = parameters;
    else
      this.parameters = new JSArray();
    this.scope = new JSScope(parentScope);
  }

  public JSFunctionDefinition(JSFunctionCall call, JSScope parentScope) {
    this.name = call.getName();
    this.parameters = call.getParameters();
    this.scope = new JSScope(parentScope);
    this.snippets.copy(call.snippets);
  }

  public String getName() {
    return name;
  }

  public JSArray getParameters() {
    return parameters;
  }

  public void setParameters(JSArray parameters) {
    this.parameters = parameters;
  }

  public JSScope getScope() {
    return scope;
  }

  public Object getReturned() {
    return returned;
  }

  public void setReturned(Object returned) {
    this.returned = returned;
  }

  @Override
  public String toString() {
    StringBuilder params = new StringBuilder();
    params.append("(");
    if (parameters != null) {
      for (Object obj : parameters) {
        if (params.length() > 1)
          params.append(", ");
        if (obj instanceof String)
          params.append('"').append(JSUtil.escapeJavaScriptString(obj.toString(), false)).append('"');
        else if (obj == null)
          params.append("null");
        else
          params.append(obj.toString());
      }
    }
    params.append(")");
    params.append(" {...}");
    return name + params.toString();
  }

  public void compose(JSWriter writer) {
    writer.append("(");
    boolean bFirst = true;
    if (parameters != null) {
      for (Object obj : parameters) {
        if (!bFirst)
          writer.append(", ");
        else
          bFirst = false;

        if (obj == null) {
          writer.append("null");
        } else if (obj instanceof String) {
          writer.append('"');
          JSUtil.escapeJavaScriptString(obj.toString(), false, writer);
          writer.append('"');
        } else if (obj instanceof JSObject) {
          ((JSObject) obj).compose(writer);
        } else if (obj instanceof JSArray) {
          ((JSArray) obj).compose(writer);
        } else if (obj instanceof JSFunctionDefinition) {
          ((JSFunctionDefinition) obj).compose(writer);
        } else if (obj instanceof JSFunctionCall) {
          ((JSFunctionCall) obj).compose(writer);
        } else if (obj instanceof JSIdentifier) {
          ((JSIdentifier) obj).compose(writer);
        } else if (obj instanceof JSRegExLiteral) {
          ((JSRegExLiteral) obj).compose(writer);
        } else if (obj instanceof Boolean) {
          writer.append(((Boolean) obj) ? "true" : "false");
        } else if (obj instanceof Number || Number.class.isAssignableFrom(obj.getClass())) {
          writer.append(obj.toString());
        } else {
          writer.append('"');
          writer.append(obj.toString());
          writer.append('"');
        }
      }
    }
    writer.append(")");
    writer.append(" {...}");
  }

  @Override
  public void setPreceding(JSSnippetSupport preceding, int precedingIndex) {
    snippets.setPreceding(preceding, precedingIndex);
  }

  @Override
  public void addSnippet(JSSnippet snippet) {
    snippets.addSnippet(snippet);
  }

  @Override
  public int currentSnippetIndex() {
    return snippets.currentSnippetIndex();
  }

  @Override
  public void augmentEndSnippet(String extraText) {
    snippets.augmentEndSnippet(extraText);
  }

  @Override
  public void composeSnippets(JSWriter writer) {
    JSSnippet snippet = snippets.getFirstSnippetByType(JSSnippet.Type.Start);
    if (snippet != null) {
      writer.append(snippet.getText());
    } else {
      writer.append("(");
    }

    if (parameters != null) {
      int index = 0;
      for (Object obj : parameters) {
        if (index > 0) {
          snippet = snippets.getSeparatorByValue(index);
          if (snippet != null) {
            writer.append(snippet.getText());
          } else {
            writer.append(", ");
          }
        }
        index++;

        snippet = snippets.getSnippetForValue(obj);

        if (obj == null) {
          writer.append("null");
        } else if (obj instanceof String) {
          if (snippet != null) {
            writer.append(snippet.getText());
          } else {
            writer.append('"');
            JSUtil.escapeJavaScriptString(obj.toString(), false, writer);
            writer.append('"');
          }
        } else if (obj instanceof JSObject) {
          ((JSObject) obj).composeSnippets(writer);
        } else if (obj instanceof JSArray) {
          ((JSArray) obj).composeSnippets(writer);
        } else if (obj instanceof JSFunctionDefinition) {
          ((JSFunctionDefinition) obj).composeSnippets(writer);
        } else if (obj instanceof JSFunctionCall) {
          ((JSFunctionCall) obj).composeSnippets(writer);
        } else if (obj instanceof JSIdentifier) {
          ((JSIdentifier) obj).composeSnippets(writer);
        } else if (obj instanceof JSRegExLiteral) {
          ((JSRegExLiteral) obj).composeSnippets(writer);
        } else if (obj instanceof Boolean) {
          if (snippet != null) {
            writer.append(snippet.getText());
          } else {
            writer.append(((Boolean) obj) ? "true" : "false");
          }
        } else if (obj instanceof Number || Number.class.isAssignableFrom(obj.getClass())) {
          if (snippet != null) {
            writer.append(snippet.getText());
          } else {
            writer.append(obj.toString());
          }
        } else {
          if (snippet != null) {
            writer.append(snippet.getText());
          } else {
            writer.append('"');
            writer.append(obj.toString());
            writer.append('"');
          }
        }
      }
    }

    snippet = snippets.getFirstSnippetByType(JSSnippet.Type.End);
    if (snippet != null) {
      writer.append(snippet.getText());
    } else {
      writer.append(")");
    }

    scope.composeSnippets(writer);
  }

  @Override
  public void removeExtraWhitespace() {
    snippets.removeExtraWhitespace(this);
  }

  @Override
  public void injectWhitespace(Object parentObject) {
    snippets.injectWhitespace(parentObject,this);
  }
}
