package za.co.synthesis.rule.core;

/**
 * Created by jake on 6/15/16.
 */
public interface IMatchingContext {
  public static class Undefined {}

  Object getField(final String field);

  void logMatch(final double confidence, final String matchName, final IMatchingContext otherSide);
}
