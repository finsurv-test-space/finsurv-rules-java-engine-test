package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 5:27 AM
 * Determines if the AccountFlow (provided as a custom value) is not equal to the provided transaction flow
 */
public class notAccountFLow implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    Object accFlow = context.getCustomValue().get("AccountFlow");
    return (accFlow != null && !accFlow.equals(context.getFlowAsString()));
  }
}
/*
function notAccountFLow(context, value) {
  var accFlow =  context.getCustomValue("AccountFlow");
  return accFlow && accFlow != context.flow;
}
*/