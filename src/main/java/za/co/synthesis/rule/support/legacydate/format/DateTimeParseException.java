package za.co.synthesis.rule.support.legacydate.format;

public class DateTimeParseException extends RuntimeException {
  public DateTimeParseException(String message) {
    super(message);
  }

  public DateTimeParseException(String message, Throwable cause) {
    super(message, cause);
  }
}
