package za.co.synthesis.validate;

import za.co.synthesis.rule.core.ICustomValidateFactory;
import za.co.synthesis.rule.core.ValidationEngine;

/**
 * User: jake
 * Date: 3/21/16
 * Time: 4:28 PM
 * Used to register all the test custom validate functions
 */
public class TestValidateFactory implements ICustomValidateFactory {
  @Override
  public void register(ValidationEngine engine) {
    TestUtility.registerTestFunctions(engine);
  }
}
