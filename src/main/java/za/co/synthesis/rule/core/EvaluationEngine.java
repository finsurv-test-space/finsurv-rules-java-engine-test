package za.co.synthesis.rule.core;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.rule.evaluation.*;
import za.co.synthesis.rule.support.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif


/**
 * User: jake
 * Date: 3/5/15
 * Time: 9:36 AM
 * This should be a singleton object within the scope of a single set of evaluation rules. I.e. If all transactions
 * are evaluated against one set of evaluation rules then there is only a need to have one instance of this class.
 * Multi threading: All setup methods MUST called first before this class is used for evaluation purposes. All
 * calls to the setup methods will cause an exception once evaluators are issued
 */
public class EvaluationEngine {
  private boolean setupTime = true;
  private boolean raiseExceptionOnDuplicateRules = true;
  private Evaluations evaluations;
  private Assumptions assumptions;
  private String basePackagePath = null;
  private Context context;

  public EvaluationEngine() {
  }

  private void checkSetup() throws EvaluationException {
    if (!setupTime) {
      throw new EvaluationException("Evaluators have already been issued. You cannot make any further call to setup functions.");
    }
  }

  public static EngineVersion getVersion() {
    return EngineVersion.getVersion();
  }

  public String getBasePackagePath() {
    return basePackagePath;
  }

  public void setBasePackagePath(String basePackagePath) {
    this.basePackagePath = basePackagePath;
  }

  /**
   * This will load rules from a JavaScript file (In the evaluation rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once evaluators are issued
   * @param name
   * @param reader
   * @throws Exception
   */
  public synchronized void setupLoadRule(final String name, final Reader reader) throws EvaluationException {
    checkSetup();

    try {
      JSStructureParser parser = new JSStructureParser(reader);
      Object obj = parser.parse();

      List<JSObject> evalRules = new ArrayList<JSObject>();
      JSUtil.findObjectsWith(obj, "drResStatus", evalRules);

      JSArray jsRuleset = new JSArray();
      for (JSObject evalRule : evalRules) {
        jsRuleset.add(evalRule);
      }
      if (jsRuleset.size() > 0) {
        if (evaluations == null) {
          evaluations = new Evaluations();
        }
        evaluations.loadEvaluations(jsRuleset, raiseExceptionOnDuplicateRules);
      }
/*
      List<JSObject> assumptionArray = new ArrayList<JSObject>();
      JSUtil.findObjectsWith(obj, "rules", assumptionArray);

      JSArray jsAssumptions = new JSArray();
      for (JSObject assumption : assumptionArray) {
        jsAssumptions.add(assumption);
      }
      if (jsAssumptions.size() > 0) {
        if (assumptions == null) {
          assumptions = new Assumptions();
        }
        assumptions.loadAssumptions(jsAssumptions);
      }
*/
    }
    catch (Exception e) {
      throw new EvaluationException("Could not parse '" + name + "' evaluation rules", e);
    }
  }

  /**
   * This will load rules from a JavaScript file (In the evaluation rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once evaluators are issued
   * @param filename
   * @throws Exception
   */
  public void setupLoadRuleFile(final String filename) throws EvaluationException {
    try {
      FileReader fileReader = new FileReader(filename);
      setupLoadRule(filename, fileReader);
    }
    catch (Exception e) {
      throw new EvaluationException("Could not load '" + filename + "' evaluation rule file", e);
    }
  }

  /**
   * This will load rules from a JavaScript resource (In the evaluation rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once evaluators are issued
   * @param fileUrl
   * @throws Exception
   */
  public void setupLoadRuleUrl(final URL fileUrl) throws EvaluationException {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));
      setupLoadRule(fileUrl.getPath(), buffReader);
    }
    catch (Exception e) {
      throw new EvaluationException("Could not load '" + fileUrl.getPath() + "' evaluation rule url", e);
    }
  }

  private void setupLoadEvalRulesFromPath(final String packagepath) throws EvaluationException {
    String dataPath = packagepath + File.separator + "evaluation";
    File file = new File(dataPath);
    if (file.isDirectory()) {
      String nameEvalRules = dataPath + File.separator + "evalRules.js";
      File fileEvalRules = new File(nameEvalRules);
      if (fileEvalRules.isFile())
        setupLoadRuleFile(nameEvalRules);

      String nameEvalAssumptionRules = dataPath + File.separator + "evalScenarios.js";
      File fileEvalAssumptionRules = new File(nameEvalAssumptionRules);
      if (fileEvalAssumptionRules.isFile()) {
        setupLoadAssumptionsFile(nameEvalAssumptionRules);
      }
      else {
        nameEvalAssumptionRules = dataPath + File.separator + "evalAssumptions.js";
        fileEvalAssumptionRules = new File(nameEvalAssumptionRules);
        if (fileEvalAssumptionRules.isFile())
          setupLoadAssumptionsFile(nameEvalAssumptionRules);
      }

      if (context == null) {
        String nameEvalContext = dataPath + File.separator + "evalContext.js";
        File fileEvalContext = new File(nameEvalContext);
        if (fileEvalContext.isFile())
          setupLoadContextFile(nameEvalContext);
      }
    }
  }

  private String determinePackagePath(String packageName, String currentPackageRoot, String basePackageRoot) {
    String packagePath = currentPackageRoot + File.separator + packageName;
    File file = new File(packagePath);
    if (file.isDirectory()) {
      return packagePath;
    }
    else {
      if (basePackageRoot != null) {
        packagePath = basePackageRoot + File.separator + packageName;
        file = new File(packagePath);
        if (file.isDirectory()) {
          return packagePath;
        }
      }
    }
    return currentPackageRoot + File.separator + packageName;
  }

  /**
   * This will load rules and lookups from a package path. This is the simplest way to load all appropriate
   * configurations for a specific application.
   * @param filepath
   * @return the engine version required to execute this package of rules
   * @throws Exception
   */
  public EngineVersion setupLoadPackagePath(final String filepath) throws ValidationException {
    EngineVersion highestVersion = null;
    String highestVersionPackageName = null;
    try {
      File file = new File(filepath);
      if (file.isDirectory()) {
        String packageRoot = file.getParent();
        String packagePath = file.getAbsolutePath();
        String packageFileName = packagePath + File.separator + "package.js";
        FileReader fileReader = new FileReader(packageFileName);

        RulePackage rule_package = RulePackages.loadRulePackage(fileReader);
        if (rule_package == null) {
          throw new ValidationException("Could not correctly parse '" + filepath + "' package. Has it been properly defined?");
        }
        highestVersion = rule_package.getEvaluationEngine();
        highestVersionPackageName = file.getName();
        setupLoadEvalRulesFromPath(packagePath);

        if (rule_package.getDependsOn() != null) {
          EngineVersion reqVersion = setupLoadPackagePath(determinePackagePath(rule_package.getDependsOnName(), packageRoot, basePackagePath));
          if (reqVersion.isHigher(highestVersion)) {
            highestVersionPackageName = rule_package.getDependsOnName();
            highestVersion = reqVersion;

          }
        }
      }
    }
    catch (Exception e) {
      throw new ValidationException("Could not load '" + filepath + "' package", e);
    }

    if (highestVersion != null) {
      if (highestVersion.isHigher(getVersion())) {
        throw new ValidationException("The '" + highestVersionPackageName + "' package requires version " + highestVersion.toString() +
                " and this engine version is " + getVersion().toString());
      }
    }
    return highestVersion;
  }

  /**
   * This will load assumptions from a JavaScript file (In the assumption rule DSL format). Earlier assumptions loaded
   * first will take precedence to those loaded later. This means that custom rules should be loaded first with
   * standard rules being loaded later.
   * This is a setup function and cannot be called once evaluators are issued
   * @param name
   * @param reader
   * @throws Exception
   */
  public synchronized void setupLoadAssumptions(final String name, final Reader reader) throws EvaluationException {
    checkSetup();

    if (assumptions == null) {
      assumptions = new Assumptions();
    }
    try {
      JSStructureParser parser = new JSStructureParser(reader);
      Object obj = parser.parse();

      String loadContaining = "rules";
      List<JSObject> evalContainer = new ArrayList<JSObject>();
      JSUtil.findObjectsWith(obj, "scenarios", evalContainer);
      if (evalContainer.size() > 0) {
        loadContaining = "match";
      }
      else {
        JSUtil.findObjectsWith(obj, "assumptions", evalContainer);
        if (evalContainer.size() > 0) {
          loadContaining = "knownSide";
        }
      }

      List<JSObject> evalRules = new ArrayList<JSObject>();
      JSUtil.findObjectsWith(obj, loadContaining, evalRules);

      JSArray jsRuleset = new JSArray();
      for (JSObject evalRule : evalRules) {
        jsRuleset.add(evalRule);
      }
      assumptions.loadAssumptions(jsRuleset);
    }
    catch (Exception e) {
      throw new EvaluationException("Could not parse '" + name + "' assumption rules", e);
    }
  }

  /**
   * This will load assumptions from a JavaScript file (In the assumption rule DSL format). Earlier assumptions loaded
   * first will take precedence to those loaded later. This means that custom rules should be loaded first with
   * standard rules being loaded later.
   * This is a setup function and cannot be called once evaluators are issued
   * @param filename
   * @throws Exception
   */
  public void setupLoadAssumptionsFile(final String filename) throws EvaluationException {
    try {
      FileReader fileReader = new FileReader(filename);
      setupLoadAssumptions(filename, fileReader);
    }
    catch (Exception e) {
      throw new EvaluationException("Could not load '" + filename + "' assumption rules file", e);
    }
  }

  /**
   * This will load assumptions from a JavaScript url (In the assumption rule DSL format). Earlier assumptions loaded
   * first will take precedence to those loaded later. This means that custom rules should be loaded first with
   * standard rules being loaded later.
   * This is a setup function and cannot be called once evaluators are issued
   * @param fileUrl
   * @throws Exception
   */
  public void setupLoadAssumptionsUrl(final URL fileUrl) throws EvaluationException {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));
      setupLoadAssumptions(fileUrl.getPath(), buffReader);
    }
    catch (Exception e) {
      throw new EvaluationException("Could not load '" + fileUrl.getPath() + "' assumption rules url", e);
    }
  }

  /**
   * This will load the evaluation context from a JavaScript file (In the evaluation context rule DSL format).
   * This is a setup function and cannot be called once evaluators are issued
   * @param name
   * @param reader
   * @throws Exception
   */
  public synchronized void setupLoadContext(final String name, final Reader reader) throws EvaluationException {
    checkSetup();

    if (context == null) {
      context = new Context();
    }
    try {
      JSStructureParser parser = new JSStructureParser(reader);
      Object obj = parser.parse();

      List<JSObject> evalContexts = new ArrayList<JSObject>();
      JSUtil.findObjectsWith(obj, "whoAmIRegex", evalContexts);

      for (JSObject evalContext : evalContexts) {
        context.loadContext((Map)evalContext);
      }
    }
    catch (Exception e) {
      throw new EvaluationException("Could not parse '" + name + "' evaluation context", e);
    }
  }

  /**
   * This will load evaluation context from a JavaScript file (In the evaluation context DSL format).
   * This is a setup function and cannot be called once evaluators are issued
   * @param filename
   * @throws Exception
   */
  public void setupLoadContextFile(final String filename) throws EvaluationException {
    try {
      FileReader fileReader = new FileReader(filename);
      setupLoadContext(filename, fileReader);
    }
    catch (Exception e) {
      throw new EvaluationException("Could not load '" + filename + "' evaluation context file", e);
    }
  }

  public boolean isRaiseExceptionOnDuplicateRules() {
    return raiseExceptionOnDuplicateRules;
  }

  public void setRaiseExceptionOnDuplicateRules(boolean raiseExceptionOnDuplicateRules) {
    this.raiseExceptionOnDuplicateRules = raiseExceptionOnDuplicateRules;
  }

  /**
   * This will load evaluation context from a JavaScript url (In the evaluation context DSL format).
   * This is a setup function and cannot be called once evaluators are issued
   * @param fileUrl
   * @throws Exception
   */
  public void setupLoadContextUrl(final URL fileUrl) throws EvaluationException {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));
      setupLoadContext(fileUrl.getPath(), buffReader);
    }
    catch (Exception e) {
      throw new EvaluationException("Could not load '" + fileUrl.getPath() + "' evaluation context url", e);
    }
  }

  public synchronized Evaluator issueEvaluator(LocalDate valueDate) throws EvaluationException {
    setupTime = false;
    List<Evaluations.DuplicateEvaluation> duplicateRules = new ArrayList<Evaluations.DuplicateEvaluation>();

    if (evaluations != null) {
      List<Evaluation> rules = evaluations.evaluationRulesForValueDate(context, valueDate, duplicateRules);
      List<Assumption> assumptionList = null;
      if (assumptions != null)
        assumptionList = assumptions.assumptionRules();
      return new Evaluator(context, rules, assumptionList, duplicateRules);
    }
    throw new EvaluationException("No Evaluation rules have been loaded. Call setupLoadRule methods before issueing Evaluator");
  }

  public Evaluator issueEvaluator() throws EvaluationException {
    return issueEvaluator(LocalDate.now());
  }
}
