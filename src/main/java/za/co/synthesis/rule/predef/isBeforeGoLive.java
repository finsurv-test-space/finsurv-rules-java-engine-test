package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif

/**
 * User: jake
 * Date: 8/8/14
 * Time: 11:33 AM
 * Determines if the current value is before the go live date
 */
public class isBeforeGoLive implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    LocalDate date = Util.date(value);
    if (date != null)
      return date.isBefore(context.getSupporting().getGoLiveDate());
    return false;
  }
}
/*
function isBeforeGoLive(context, value) {
  return value < rule_parameters.goLiveDate;
}
*/