package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 2:26 PM
 * Determines if the current value is an invalid sub category code
 */
public class hasInvalidSubCategory implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value instanceof IFinsurvContext.Undefined)
      return true;
    Object category = context.getMoneyField(context.getCurrentMoneyInstance(), "CategoryCode");
    if (category == null)
      return true;
    return !context.getLookups().isValidSubCategory(context.getFlowAsString(), category.toString(), value != null ? value.toString() : null);
  }
}
/*
function hasInvalidSubCategory(context, value) {
  if (typeof value === 'undefined')
    return true;
  var flow = context.flow;
  var category = context.getMoneyField(context.currentMoneyInstance, 'CategoryCode');
  return !context.lookups.isValidSubCategory(flow, category, value);
}
*/