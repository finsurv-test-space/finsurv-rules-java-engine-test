package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

import java.util.Calendar;

/**
 * User: jake
 * Date: 4/19/16
 * Time: 3:42 PM
 * Determines the date of birth from the given field value
 */
public class dateOfBirthFromSAID implements IDisplayFunction {
  private String field;

  public dateOfBirthFromSAID(String field) {
    this.field = field;
  }

  @Override
  public String execute(FinsurvContext context, Object value) {
    Object idObj;
    if (field.equals("ThirdParty.Individual.IDNumber"))
      idObj = context.getMoneyField(context.getCurrentMoneyInstance(), field);
    else
      idObj = context.getTransactionField(field);

    notEmpty notEmpty = new notEmpty();
    if (notEmpty.execute(context, idObj)) {
      String idStr = idObj.toString();
      String dateStr;

      idStr = idStr.trim();
      if (idStr.length() >= 6 && Character.isDigit(idStr.charAt(0)) && Character.isDigit(idStr.charAt(1))) {
        int year = Integer.parseInt(idStr.substring(0, 2));
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        if ((year+1900) <= (currentYear - 101)){
          dateStr = "20";
        } else {
          dateStr = "19";
        }

        dateStr += idStr.charAt(0);
        dateStr += idStr.charAt(1);
        dateStr += '-';
        dateStr += idStr.charAt(2);
        dateStr += idStr.charAt(3);
        dateStr += '-';
        dateStr += idStr.charAt(4);
        dateStr += idStr.charAt(5);
        return dateStr;
      }
    }
    return "";
  }
}


/*
  _export.dateOfBirthFromSAID = function (field) {
    return function (context) {
      var idStr;
      if (field === "ThirdParty.Individual.IDNumber")
        idStr = context.getMoneyField(context.currentMoneyInstance, field);
      else
        idStr = context.getTransactionField(field);

      if (_export.notEmpty(context, idStr)) {
        var dateStr;
        idStr = idStr.trim();
        if (idStr.length >= 6) {
          var year = Number(idStr[0] + idStr[1]);
          if (year > 25)
            dateStr = "19";
          else if (year < 10)
            dateStr = "20";
          else
            return "";

          dateStr += idStr[0];
          dateStr += idStr[1];
          dateStr += "-";
          dateStr += idStr[2];
          dateStr += idStr[3];
          dateStr += "-";
          dateStr += idStr[4];
          dateStr += idStr[5];
          return dateStr;
        }
      }
      return "";
    }
  }

*/