package za.co.synthesis.rule.core;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.impl.JSFinsurvContext;
import za.co.synthesis.rule.core.impl.PojoFinsurvContext;
import za.co.synthesis.rule.support.*;

import java.util.*;

/**
 * Created by jake on 4/12/16.
 */
public class Displayor {
  private final Map<String, String> mappings;
  private final ILookups lookups;
  private final ISupporting supporting;
  private final Displays displays;
  private final DisplayRuleBehaviour displayRuleBehaviour = new DisplayRuleBehaviour();
  private CustomValidateRegistry customValidateRegistry;
  private CustomValidateCache customValidateCache;

  public Displayor(Map<String, String> mappings, ILookups lookups, ISupporting supporting, Displays displays) {
    this.mappings = mappings;
    this.lookups = lookups;
    this.supporting = supporting;
    this.displays = displays;
    this.customValidateRegistry = new CustomValidateRegistry();
    this.customValidateCache = new CustomValidateCache(this.customValidateRegistry);
  }

  public Map<String, String> getMappings() {
    return mappings;
  }

  public DisplayRuleBehaviour getDisplayRuleBehaviour() {
    return displayRuleBehaviour;
  }

  private Collection<DisplayEntry> runDisplayRules(String fieldFilter, FinsurvContext context) {
    context.setDisplayRuleBehaviour(this.getDisplayRuleBehaviour());
    List<Display> filteredRules = displays.displayRulesFromRuleSets(
            context.getFlow(), context.getSection(), context.getCategories(), context.getMainCategories(), fieldFilter);
    StringList appendedFields = new StringList();

    for (Display displayRule : filteredRules) {
      displayRule.run(context, appendedFields);
    }
    return context.getDisplayResultList();
  }

  private Collection<DisplayEntry> runDisplayRules(FinsurvContext context) {
    return runDisplayRules(null, context);
  }

  private Collection<DisplayEntry> runDisplayRules(String fieldFilter, Object dto, ICustomValue customValues) throws Exception {
    List<ResultEntry> resultList = new ArrayList<ResultEntry>();
    PojoFinsurvContext context = new PojoFinsurvContext(dto);
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    return runDisplayRules(fieldFilter, fc);
  }

  private Collection<DisplayEntry> runDisplayRules(Object dto, ICustomValue customValues) throws Exception {
    return runDisplayRules(null, dto, customValues);
  }

  public Collection<DisplayEntry> runDisplayRule(String fieldFilter, JSObject jsObj, ICustomValue customValues) {
    JSFinsurvContext context = new JSFinsurvContext(jsObj);
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    return runDisplayRules(fieldFilter, fc);
  }

  public Collection<DisplayEntry> runDisplayRule(JSObject jsObj, ICustomValue customValues) {
    JSFinsurvContext context = new JSFinsurvContext(jsObj);
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    return runDisplayRules(fc);
  }

  public Collection<DisplayEntry> runDisplayRule(String fieldFilter, IFinsurvContext context, ICustomValue customValues) {
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    return runDisplayRules(fieldFilter, fc);
  }

  public Collection<DisplayEntry> runDisplayRule(IFinsurvContext context, ICustomValue customValues) {
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    return runDisplayRules(fc);
  }
}

