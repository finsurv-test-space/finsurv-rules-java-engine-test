package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.matching.IMatchingFunction;
import za.co.synthesis.rule.matching.MatchingContext;

/**
 * Created by jake on 6/16/16.
 */
public class sameFirst implements IMatchingFunction {
  private final String fieldname;
  private final int first;

  public sameFirst(String fieldname, String first) {
    this.fieldname = fieldname;
    this.first = Integer.parseInt(first);
  }

  @Override
  public boolean execute(MatchingContext context) {
    Object valueLhs = context.getFieldLhs(fieldname);
    Object valueRhs = context.getFieldRhs(fieldname);

    if (valueLhs != null) {
      if (valueRhs != null) {
        String strLhs = valueLhs.toString();
        String strRhs = valueRhs.toString();
        String firstLhs = strLhs.length() > first ? strLhs.substring(0, first) : strLhs;
        String firstRhs = strRhs.length() > first ? strRhs.substring(0, first) : strRhs;
        return firstLhs.equals(firstRhs);
      }
      else
        return false;
    }
    else {
      return false;
    }
  }
}

