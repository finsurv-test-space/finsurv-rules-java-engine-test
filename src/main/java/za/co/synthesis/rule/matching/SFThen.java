package za.co.synthesis.rule.matching;

import za.co.synthesis.rule.core.IMatchingContext;

/**
 * Created by jake on 6/16/16.
 */
public class SFThen implements IShardFunction {
  private IShardFunction left;
  private IShardFunction right;

  public SFThen(IShardFunction left, IShardFunction right) {
    this.left = left;
    this.right = right;
  }

  @Override
  public String[] execute(IMatchingContext context) {
    String[] lhsArray = left.execute(context);
    String[] rhsArray = right.execute(context);

    String[] result = new String[lhsArray.length*rhsArray.length];
    int i=0;
    for (String lhsValue : lhsArray) {
      for (String rhsValue : rhsArray) {
        result[i++] = lhsValue + "|" + rhsValue;
      }
    }
    return result;
  }
}
