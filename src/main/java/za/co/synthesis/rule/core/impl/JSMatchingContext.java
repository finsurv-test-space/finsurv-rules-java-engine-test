package za.co.synthesis.rule.core.impl;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.IClassificationContext;
import za.co.synthesis.rule.core.IMatchingContext;

/**
 * Created by jake on 6/16/16.
 */
public class JSMatchingContext implements IMatchingContext {
  private final JSObject jsObj;

  public JSMatchingContext(JSObject jsObj) {
    this.jsObj = jsObj;
  }

  public Object getDataField(Object objLevel, final String field) {
    String[] fields = field.split("\\.");
    String fieldName = null;
    for (int i = 0; i < fields.length; i++) {
      fieldName = fields[i];
      objLevel = ((JSObject) objLevel).get(fieldName);
      if (objLevel == null) {
        if (i >= fields.length - 1)
          return null;
        else {
          return null;
        }
      }
    }
    return objLevel;
  }

  public Object getField(final String field) {
    return getDataField(jsObj, field);
  }

  @Override
  public void logMatch(double confidence, String matchName, IMatchingContext otherSide) {
  }
}
