package za.co.synthesis.rule.difference.validation;

import za.co.synthesis.rule.difference.DiffUtils;
import za.co.synthesis.rule.difference.FieldRule;
import za.co.synthesis.rule.difference.IDifference;
import za.co.synthesis.rule.difference.RuleMap;
import za.co.synthesis.rule.support.*;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/***
 * This class is used to determine the validation differences between two packages. It is important to understand that this checks
 * for differences between the raw packages as opposed to differences between compound packages (with all dependsOn dependencies included).
 * This is typically used to determine the edits have been made to the validation rules.
 */
public class ValidationDifferences {
  public static List<IDifference> getPackageDifferences(String oldPackagePath, String newPackagePath) throws Exception {
    File oldFilePackage = Paths.get(oldPackagePath, "package.js").toFile();
    RulePackage oldRulePackage = null;
    if (oldFilePackage.isFile()) {
      oldRulePackage = RulePackages.loadRulePackage(new FileReader(oldFilePackage));
    }

    File newFilePackage = Paths.get(newPackagePath, "package.js").toFile();
    RulePackage newRulePackage = null;
    if (newFilePackage.isFile()) {
      newRulePackage = RulePackages.loadRulePackage(new FileReader(newFilePackage));
    }

    List<IDifference> results = new ArrayList<IDifference>();
    if (oldRulePackage != null && newRulePackage != null) {
      Map<String, String> oldMap = oldRulePackage.getMapping();
      Map<String, String> newMap = newRulePackage.getMapping();

      for (Map.Entry<String, String> newEntry : newMap.entrySet()) {
        String key = newEntry.getKey();
        if (oldMap.containsKey(key)) {
          String oldValue = oldMap.get(key);
          String newValue = newMap.get(key);
          if (!DiffUtils.areObjectsTheSame(newValue, oldValue)) {
            results.add(MappingDifference.createChangedMapping(key, oldValue, newValue));
          }
        } else {
          // The key has been added
          results.add(MappingDifference.createAddedMapping(key, newEntry.getValue()));
        }
      }
      for (Map.Entry<String, String> oldEntry : oldMap.entrySet()) {
        String key = oldEntry.getKey();
        if (!newMap.containsKey(key)) {
          // The old key is not in the new Map
          results.add(MappingDifference.createRemovedMapping(key, oldEntry.getValue()));
        }
      }
    }
    return results;
  }


  private static FieldMap getFieldMapFromRuleSets(List<RuleSet> ruleSets) {
    FieldMap fieldMap = new FieldMap();

    for (RuleSet rs : ruleSets) {
      for (FieldValidation fv : rs.getValidations()) {
        for (String field :  fv.getFieldList()) {
          fieldMap.add(new FieldSize(rs.getScope(), field, fv.getMinLen(), fv.getMaxLen(), fv.getLen()));
        }
      }
    }
    return fieldMap;
  }

  private static boolean messagesDifferent(Rule rule1, Rule rule2) {
    if (DiffUtils.areObjectsTheSame(rule1.getCode(), rule2.getCode()) &&
            DiffUtils.areObjectsTheSame(rule1.getMessage(), rule2.getMessage())) {
      return false;
    }
    return true;
  }

  public static List<IDifference> getMessageDifferences(String oldPackagePath, String newPackagePath) throws Exception {
    List<IDifference> result = new ArrayList<IDifference>();
    List<RuleSet> oldRuleSets = DiffUtils.loadValidationRulesets(oldPackagePath);
    List<RuleSet> newRuleSets = DiffUtils.loadValidationRulesets(newPackagePath);

    // Changes to Codes and Messages via the message stuff
    RuleMap oldMessageRules = DiffUtils.getRuleMapFromRuleSets(oldRuleSets, RuleType.Message);
    RuleMap newMessageRules = DiffUtils.getRuleMapFromRuleSets(newRuleSets, RuleType.Message);

    for (String name : newMessageRules.ruleList()) {
      FieldRule oldFieldRule = oldMessageRules.get(name);
      FieldRule newFieldRule = newMessageRules.get(name);
      if (oldFieldRule == null) {
        Rule newRule = newFieldRule.getRule();
        result.add(CodeMessageDifference.createAddedCodeMessage(
                name, newRule.getCode(), newRule.getMessage()));
      }
      else {
        Rule oldRule = oldFieldRule.getRule();
        Rule newRule = newFieldRule.getRule();
        if (messagesDifferent(oldRule, newRule)) {
          result.add(CodeMessageDifference.createChangedCodeMessage(
                  name,
                  oldRule.getCode(), oldRule.getMessage(),
                  newRule.getCode(), newRule.getMessage()));
        }
      }
      oldMessageRules.remove(name);
    }
    for (String name : oldMessageRules.ruleList()) {
      Rule rule = oldMessageRules.get(name).getRule();
      result.add(CodeMessageDifference.createRemovedCodeMessage(
              rule.getName(), rule.getCode(), rule.getMessage()));
    }
    return result;
  }

  public static List<IDifference> getRuleDifferences(String oldPackagePath, String newPackagePath) throws Exception {
    List<IDifference> result = new ArrayList<IDifference>();
    List<RuleSet> oldRuleSets = DiffUtils.loadValidationRulesets(oldPackagePath);
    List<RuleSet> newRuleSets = DiffUtils.loadValidationRulesets(newPackagePath);

    // Changes to Rules
    RuleMap oldRules = DiffUtils.getRuleMapFromRuleSets(oldRuleSets, RuleType.Error, RuleType.Warning, RuleType.Ignore);
    RuleMap newRules = DiffUtils.getRuleMapFromRuleSets(newRuleSets, RuleType.Error, RuleType.Warning, RuleType.Ignore);

    for (String name : newRules.ruleList()) {
      FieldRule oldFieldRule = oldRules.get(name);
      FieldRule newFieldRule = newRules.get(name);
      if (oldFieldRule == null) {
        result.add(RuleDifference.createAddedRule(newFieldRule.getScope(), newFieldRule.getFields(), newFieldRule.getRule()));
      }
      else {
        if (!oldFieldRule.equalsRule(newFieldRule.getRule())) {
          result.add(RuleDifference.createChangedRule(
                  oldFieldRule.getScope(), oldFieldRule.getFields(), oldFieldRule.getRule(),
                  newFieldRule.getScope(), newFieldRule.getFields(), newFieldRule.getRule()));
        }
      }
      oldRules.remove(name);
    }
    for (String name : oldRules.ruleList()) {
      FieldRule fieldRule = oldRules.get(name);
      result.add(RuleDifference.createRemovedRule(fieldRule.getScope(), fieldRule.getFields(), fieldRule.getRule()));
    }
    return result;
  }

  private static boolean isFieldEmpty(FieldSize field) {
    if (field.getMinLen() == null && field.getMaxLen() == null && field.getLen() == null) {
      return true;
    }
    return false;
  }

  private static boolean fieldsDifferent(FieldSize field1, FieldSize field2) {
    if (DiffUtils.areObjectsTheSame(field1.getMinLen(), field2.getMinLen()) &&
            DiffUtils.areObjectsTheSame(field1.getMaxLen(), field2.getMaxLen()) &&
            DiffUtils.areObjectsTheSame(field1.getLen(), field2.getLen())) {
      return false;
    }
    return true;
  }

  public static List<IDifference> getFieldDifferences(String oldPackagePath, String newPackagePath) throws Exception {
    List<IDifference> result = new ArrayList<IDifference>();
    List<RuleSet> oldRuleSets = DiffUtils.loadValidationRulesets(oldPackagePath);
    List<RuleSet> newRuleSets = DiffUtils.loadValidationRulesets(newPackagePath);

    // Changes to Rules
    FieldMap oldFields = getFieldMapFromRuleSets(oldRuleSets);
    FieldMap newFields = getFieldMapFromRuleSets(newRuleSets);

    for (String key : newFields.fieldKeys()) {
      FieldSize oldFieldSize = oldFields.get(key);
      FieldSize newFieldSize = newFields.get(key);
      if (oldFieldSize == null) {
        if (!isFieldEmpty(newFieldSize)) {
          result.add(FieldDifference.createAddedField(newFieldSize));
        }
      }
      else {
        if (fieldsDifferent(oldFieldSize, newFieldSize)) {
          result.add(FieldDifference.createChangedField(oldFieldSize, newFieldSize));
        }
      }
      oldFields.remove(key);
    }
    for (String name : oldFields.fieldKeys()) {
      FieldSize fieldSize = oldFields.get(name);
      if (!isFieldEmpty(fieldSize)) {
        result.add(FieldDifference.createRemovedField(fieldSize));
      }
    }
    return result;
  }
}
