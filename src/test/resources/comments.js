/**
 * Created by Sasha Slankamenac on 2015-06-04.
 */
// Single line comments
var value = {
  StartSLInStr: "//Boof", // This is a comment
  SLCommentInStr1: "The // is not a comment", // This is a comment
  MLCommentInStr1: "My /* not a comment */ name is (not a comment)", /* So is this
  onto next line */
  SLCommentInStr2: 'The // is not a comment', // This is a comment
  MLCommentInStr2: 'My /* not a comment */ name is (not a comment)' /* So is this
  onto next line */
}
