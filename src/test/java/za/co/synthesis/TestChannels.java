package za.co.synthesis;

import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.core.impl.JSCustomValues;
import za.co.synthesis.rule.support.IBAN;
import za.co.synthesis.rule.support.RulePackage;
import za.co.synthesis.rule.support.Util;

import static org.junit.Assert.assertTrue;
import static za.co.synthesis.rule.support.ValidationTest.createValidationTest;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import za.co.synthesis.rule.support.ValidationTest;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 3:16 PM
 * Runs all the JavaScript test cases
 */
public class TestChannels {
  private String[] getPackages(String path) {
    File file = new File(path);
    String[] directories = file.list(new FilenameFilter() {
      @Override
      public boolean accept(File current, String name) {
        return new File(current, name).isDirectory();
      }
    });
    return directories;
  }

  private ValidationTest.TestContext runPackageTestCases(String packagePath) throws Exception {
  ValidationEngine engine = new ValidationEngine();

    // Package: sbNA
    long startLoad = System.currentTimeMillis();
    String testLookups = packagePath + "/tests/testLookups.js";
    File file = new File(testLookups);
    if (file.exists() && file.isFile()) {
      engine.setupLoadLookupFile(testLookups);
    }
    engine.setupLoadPackagePath(packagePath);
    DefaultSupporting supporting = new DefaultSupporting();
    supporting.setCurrentDate(Util.date("2013-01-20"));
    supporting.setGoLiveDate(Util.date("2013-08-18"));
    engine.setupSupporting(supporting);

    ValidationTest validationTest = createValidationTest(engine);

    System.out.println("Load Time: " + (validationTest.getEndLoad()-validationTest.getStartLoad()) +
            "ms , Run Time: " + (validationTest.getEndRun()-validationTest.getStartRun()) + "ms");
    return validationTest.getDefaultTestContext();
  }

  private boolean runAllPackageTestCases(String repoPath) throws Exception {
		int errorCounter = 0;

    String[] paths = getPackages(repoPath);

    // String[] paths = { "coreRBM" };

    if (paths == null) { System.out.println("\n No files found at: " + repoPath + "\n"); }
    for (String path : paths) {
      if (path.equals("stdBankNBOL")) {
        System.out.println(">>>*** Skipping tests on " + path );
        continue;
      }

      String packagePath = repoPath + "/" + path;
      File file = new File(packagePath + "/tests/testCases.js");
      if (file.exists() && file.isFile()) {
        System.out.println(">>>*** Running tests on " + path );

        ValidationTest.TestContext testContext;
        try {
          testContext = runPackageTestCases(packagePath);
        }
        catch (Exception e) {
          System.out.println("Exception running tests for " + path);
          throw e;
        }

        for (ValidationTest.RuleTestResult res : testContext.getTestResults()) {
          for (String error : res.getErrorList()) {
            System.out.println(error);
          }
        }

        if (testContext.getTestCount() == 0) {
          System.out.println("No tests in package" );
        }
        else
        if (testContext.getErrorCount() == 0) {
          System.out.println("Completed " + testContext.getTestCount() + " tests Successfully :-)" );
        }
        else {
          if (testContext.getErrorCount() == 1) {
						System.out.println("Ran " + testContext.getTestCount() + " tests with 1 error!");
						errorCounter++;
					} 
          else {
						System.out.println("Ran " + testContext.getTestCount() + " tests with " + testContext.getErrorCount() + " errors!");
						errorCounter += testContext.getErrorCount();
					}
            
        }
      }
      else {
        System.out.println("*** No file testCases.js for " + path );
      }
    }

    return errorCounter == 0;
  }

  @Test
  public void TestAllChannels() throws Exception {
    TestChannels testChannels = new TestChannels();

    boolean errors = testChannels.runAllPackageTestCases("../finsurv-rules/reg_rule_repo");
    assertTrue("Errors in Validation Tests", errors);
  }

  public static void main( String[] args ) throws Exception {
    TestChannels testChannels = new TestChannels();

    testChannels.runAllPackageTestCases("../finsurv-rules/reg_rule_repo");
  }
}

