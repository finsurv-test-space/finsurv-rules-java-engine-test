package za.co.synthesis.rule.evaluation;

import za.co.synthesis.rule.core.BankAccountType;
import za.co.synthesis.rule.core.DrCr;
import za.co.synthesis.rule.core.ResidenceStatus;
import za.co.synthesis.rule.core.impl.EvaluationDecision;

/**
 * Created by jake on 6/28/16.
 */
public class AssumptionRule {
  private AssumptionRuleType type;
  private final ResidenceStatus resStatus;
  private final BankAccountType accType;
  private final EvaluationDecision decision;
  private final IAssumptionAssertionFunction assertion;

  public AssumptionRule(final AssumptionRuleType type,
                        final ResidenceStatus resStatus, final BankAccountType accType,
                        final IAssumptionAssertionFunction assertion) {
    this.type = type;
    this.resStatus = resStatus;
    this.accType = accType;
    this.decision = null;
    this.assertion = assertion;
  }

  public AssumptionRule(final AssumptionRuleType type,
                        final EvaluationDecision decision,
                        final IAssumptionAssertionFunction assertion) {
    this.type = type;
    this.resStatus = null;
    this.accType = null;
    this.decision = decision;
    this.assertion = assertion;
  }

  public AssumptionRule(final AssumptionRuleType type,
                        final IAssumptionAssertionFunction assertion) {
    this.type = type;
    this.resStatus = null;
    this.accType = null;
    this.decision = null;
    this.assertion = assertion;
  }

  public void updateAssumptionRuleType(DrCr knownSide) {
    if (this.type.equals(AssumptionRuleType.Use)) {
      if (knownSide.equals(DrCr.DR)) {
        this.type = AssumptionRuleType.UseCr;
      }
      else {
        this.type = AssumptionRuleType.UseDr;
      }
    }
  }

  public AssumptionRuleType getType() {
    return type;
  }

  public ResidenceStatus getResStatus() {
    return resStatus;
  }

  public BankAccountType getAccType() {
    return accType;
  }

  public EvaluationDecision getDecision() {
    return decision;
  }

  public IAssumptionAssertionFunction getAssertion() {
    return assertion;
  }
}
