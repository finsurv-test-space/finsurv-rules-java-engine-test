package za.co.synthesis.javascript;

import java.util.List;

/**
 * User: jake
 * Date: 4/26/16
 * Time: 5:09 PM
 * Some common utility functions used for parsing the JS object structures
 */
public class JSUtil {
  public static Object get(JSObject jsObj, String property) {
    String[] parts = property.split("[.]");
    JSObject js = jsObj;
    for (int i=0; i<parts.length; i++) {
      Object obj = js.get(parts[i]);
      if (i == parts.length-1) {
        return obj;
      }
      else {
        if (obj instanceof JSObject) {
          js = (JSObject) obj;
        }
        else {
          return null;
        }
      }
    }
    return null;
  }

  public static String getString(JSObject jsObj, String property) {
    Object obj = get(jsObj, property);
    if (obj != null) {
      return obj.toString();
    }
    return null;
  }

  public static JSArray getJSArray(JSObject jsObj, String property) {
    Object obj = get(jsObj, property);
    if (obj instanceof JSArray) {
      return (JSArray) obj;
    }
    return null;
  }

  public static JSObject getJSObject(JSObject jsObj, String property) {
    Object obj = get(jsObj, property);
    if (obj instanceof JSObject) {
      return (JSObject) obj;
    }
    return null;
  }

  public static JSFunctionCall getJSFunctionCall(JSObject jsObj, String property) {
    Object obj = get(jsObj, property);
    if (obj instanceof JSFunctionCall) {
      return (JSFunctionCall) obj;
    }
    return null;
  }

  public static JSIdentifier getJSIdentifier(JSObject jsObj, String property) {
    Object obj = get(jsObj, property);
    if (obj instanceof JSIdentifier) {
      return (JSIdentifier) obj;
    }
    return null;
  }

  public static JSFunctionDefinition getJSFunctionDefinition(JSObject jsObj, String property) {
    Object obj = get(jsObj, property);
    if (obj instanceof JSFunctionDefinition) {
      return (JSFunctionDefinition) obj;
    }
    return null;
  }

  public static JSRegExLiteral getJSRegExLiteral(JSObject jsObj, String property) {
    Object obj = get(jsObj, property);
    if (obj instanceof JSRegExLiteral) {
      return (JSRegExLiteral) obj;
    }
    return null;
  }

  /**
   * This is a helper function that searches the given parsed JavaScript tree structure and pulls out the object
   * with the given property. This allows the Javascript to be arbitrarily complex without the need for the code to
   * know the structure beforehand to know where to find the targeted DSL structures. This allows the use of require
   * friendly modules.
   * @param obj - the parsed JavaScript tree structure
   * @param property - the property contained in the target structure
   * @return a JSObject that contains the given property or null if none found
   */
  public static JSObject findObjectWith(Object obj, String property) {
    JSObject result = null;
    if (obj instanceof JSObject) {
      JSObject jsObj = (JSObject)obj;
      if (jsObj.containsKey(property)) {
        result = jsObj;
      }
      else {
        for (Object innerObj : jsObj.values()) {
          result = findObjectWith(innerObj, property);
          if (result != null)
            break;
        }
      }
    }
    else
    if (obj instanceof JSArray) {
      JSArray jsArray = (JSArray)obj;
      for (Object innerObj : jsArray) {
        result = findObjectWith(innerObj, property);
        if (result != null)
          break;
      }
    }
    else
    if (obj instanceof JSFunctionCall) {
      JSFunctionCall jsFunc = (JSFunctionCall)obj;
      for (Object param : jsFunc.getParameters()) {
        result = findObjectWith(param, property);
        if (result != null)
          break;
      }
    }
    else
    if (obj instanceof JSFunctionDefinition) {
      JSFunctionDefinition jsFunc = (JSFunctionDefinition)obj;
      for (Object param : jsFunc.getParameters()) {
        result = findObjectWith(param, property);
        if (result != null)
          break;
      }
      if (result == null && jsFunc.getScope().containsKey(property)) {
        result = jsFunc.getScope();
      }
      if (result == null) {
        for (Object innerObj : jsFunc.getScope().values()) {
          result = findObjectWith(innerObj, property);
          if (result != null)
            break;
        }
      }
      if (result == null)
        result = findObjectWith(jsFunc.getReturned(), property);
    }
    return result;
  }

  /**
   * This is a helper function that searches the given parsed JavaScript tree structure and pulls out the list of
   * objects with the given property. This allows the Javascript to be arbitrarily complex without the need for the code to
   * know the structure beforehand to know where to find the targeted DSL structures. This allows the use of require
   * friendly modules.
   * @param obj - the parsed JavaScript tree structure
   * @param property - the property contained in the target structure
   * @param results - return list of JSObject instances that contain the given property or empty if none found
   */
  public static void findObjectsWith(final Object obj, final String property, List<JSObject> results) {
    if (obj instanceof JSObject) {
      JSObject jsObj = (JSObject)obj;
      if (jsObj.containsKey(property)) {
        if (!results.contains(jsObj))
          results.add(jsObj);
      }
      else {
        for (Object innerObj : jsObj.values()) {
          findObjectsWith(innerObj, property, results);
        }
      }
    }
    else
    if (obj instanceof JSArray) {
      JSArray jsArray = (JSArray)obj;
      for (Object innerObj : jsArray) {
        findObjectsWith(innerObj, property, results);
      }
    }
    else
    if (obj instanceof JSFunctionCall) {
      JSFunctionCall jsFunc = (JSFunctionCall)obj;
      for (Object param : jsFunc.getParameters()) {
        findObjectsWith(param, property, results);
      }
    }
    else
    if (obj instanceof JSFunctionDefinition) {
      JSFunctionDefinition jsFunc = (JSFunctionDefinition)obj;
      for (Object param : jsFunc.getParameters()) {
        findObjectsWith(param, property, results);
      }
      if (jsFunc.getScope().containsKey(property)) {
        if (!results.contains(jsFunc.getScope()))
          results.add(jsFunc.getScope());
      }
      for (Object innerObj : jsFunc.getScope().values()) {
        findObjectsWith(innerObj, property, results);
      }
      findObjectsWith(jsFunc.getReturned(), property, results);
    }
  }

  /**
   * This is a helper function that searches the given parsed JavaScript tree structure and pulls out the list of
   * objects with the given property. This allows the Javascript to be arbitrarily complex without the need for the code to
   * know the structure beforehand to know where to find the targeted DSL structures. This allows the use of require
   * friendly modules.
   * @param obj - the parsed JavaScript tree structure
   * @param property - the property contained in the target structure
   * @param results - return list of JSObject instances that contain the given property or empty if none found
   */
  public static void findByName(final Object obj, final String property, List<Object> results) {
    if (obj instanceof JSObject) {
      JSObject jsObj = (JSObject)obj;
      if (jsObj.containsKey(property)) {
        Object value = jsObj.get(property);
        if (!results.contains(value))
          results.add(value);
      }
      else {
        for (Object innerObj : jsObj.values()) {
          findByName(innerObj, property, results);
        }
      }
    }
    else
    if (obj instanceof JSArray) {
      JSArray jsArray = (JSArray)obj;
      for (Object innerObj : jsArray) {
        findByName(innerObj, property, results);
      }
    }
    else
    if (obj instanceof JSFunctionCall) {
      JSFunctionCall jsFunc = (JSFunctionCall)obj;
      for (Object param : jsFunc.getParameters()) {
        findByName(param, property, results);
      }
    }
    else
    if (obj instanceof JSFunctionDefinition) {
      JSFunctionDefinition jsFunc = (JSFunctionDefinition)obj;
      for (Object param : jsFunc.getParameters()) {
        findByName(param, property, results);
      }
      if (jsFunc.getScope().containsKey(property)) {
        Object value = jsFunc.getScope().get(property);
        if (!results.contains(value))
          results.add(value);
      }
      for (Object innerObj : jsFunc.getScope().values()) {
        findByName(innerObj, property, results);
      }
      findByName(jsFunc.getReturned(), property, results);
    }
  }

  /**
   * This is a helper function that searches the given parsed JavaScript tree structure and pulls out the list of
   * JSFunctionDefinition objects with the given name.
   * @param obj - the parsed JavaScript tree structure
   * @param name - the name of the function to search for in the given target structure
   * @param results - return list of JSFunctionDefinition instances given name or empty if none found
   */
  public static void findFunctionDefinitions(final Object obj, final String name, List<JSFunctionDefinition> results) {
    if (obj instanceof JSObject) {
      for (Object innerObj : ((JSObject)obj).values()) {
        findFunctionDefinitions(innerObj, name, results);
      }
    }
    else
    if (obj instanceof JSFunctionCall) {
      JSFunctionCall jsFunc = (JSFunctionCall)obj;
      for (Object param : jsFunc.getParameters()) {
        findFunctionDefinitions(param, name, results);
      }
    }
    else
    if (obj instanceof JSFunctionDefinition) {
      JSFunctionDefinition jsFunc = (JSFunctionDefinition)obj;
      if (jsFunc.getName().equals(name)) {
        if (!results.contains(jsFunc))
          results.add(jsFunc);
      }
      for (Object param : jsFunc.getParameters()) {
        findFunctionDefinitions(param, name, results);
      }
      for (Object innerObj : jsFunc.getScope().values()) {
        findFunctionDefinitions(innerObj, name, results);
      }
      findFunctionDefinitions(jsFunc.getReturned(), name, results);
    }
  }

  private static String hex(char ch) {
    return Integer.toHexString(ch).toUpperCase();
  }

  public static String escapeJavaScriptString(String str, boolean escapeSingleQuote) {
    if (str == null) {
      return str;
    }

    JSWriter writer = new JSWriter();
    escapeJavaScriptString(str, escapeSingleQuote, writer);
    return writer.toString();
  }

  public static void escapeJavaScriptString(String str, boolean escapeSingleQuote, JSWriter writer) {
    if (str == null) {
      return;
    }

    int sz;
    sz = str.length();
    for (int i = 0; i < sz; i++) {
      char ch = str.charAt(i);

      if (ch > 0xfff) {
        writer.append("\\u").append(hex(ch));
      } else if (ch > 0xff) {
        writer.append("\\u0").append(hex(ch));
      } else if (ch > 0x7f) {
        writer.append("\\u00").append(hex(ch));
      } else if (ch < 32) {
        switch (ch) {
          case '\b':
            writer.append('\\');
            writer.append('b');
            break;
          case '\n':
            writer.append('\\');
            writer.append('n');
            break;
          case '\t':
            writer.append('\\');
            writer.append('t');
            break;
          case '\f':
            writer.append('\\');
            writer.append('f');
            break;
          case '\r':
            writer.append('\\');
            writer.append('r');
            break;
          default :
            if (ch > 0xf) {
              writer.append("\\u00").append(hex(ch));
            } else {
              writer.append("\\u000").append(hex(ch));
            }
            break;
        }
      } else {
        switch (ch) {
          case '\'':
            if (escapeSingleQuote) {
              writer.append('\\');
            }
            writer.append('\'');
            break;
          case '"':
            writer.append('\\');
            writer.append('"');
            break;
          case '\\':
            writer.append('\\');
            writer.append('\\');
            break;
          default :
            writer.append(ch);
            break;
        }
      }
    }
  }
}
