package za.co.synthesis.rule.core.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.core.IFinsurvContextCache;
import za.co.synthesis.rule.core.ResultType;
import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.rule.support.DisplayRuleType;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
import za.co.synthesis.rule.support.legacydate.format.DateTimeFormatter;
#else*/
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
//#endif
import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 5:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class PojoFinsurvContext implements IFinsurvContext {
  Logger logger = LoggerFactory.getLogger(PojoFinsurvContext.class);

  private final Object trx;
  private final IFinsurvContextCache validationCache;
  private final List monetaryAmountList;
  private final List<List> importExportLists;
  private final boolean useMethod;
  private DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  private DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

  private Method getMethodByName(Method[] methods, String name) {
    for (Method method : methods) {
      if (method.getName().equals(name)) {
        return method;
      }
    }
    return null;
  }
  public PojoFinsurvContext(Object trn) throws Exception {
    this(trn, new DefaultFinsurvContextCache());
  }

  public PojoFinsurvContext(Object trn, IFinsurvContextCache validationCache) throws Exception {
    this.trx = trn;
    this.validationCache = validationCache;

    Method[] methods = trn.getClass().getMethods();
    if (getMethodByName(methods, "getMonetaryAmount") != null)
      useMethod = true;
    else
      useMethod = false;

    if (useMethod) {
      Method mMA = getMethodByName(methods, "getMonetaryAmount");
      Object maObjList = mMA.invoke(trx);
      this.monetaryAmountList = (List)maObjList;

      importExportLists = new ArrayList<List>();
      for (Object maObj : this.monetaryAmountList) {
        Method[] maMethods = maObj.getClass().getMethods();
        Method mIE = getMethodByName(maMethods, "getImportExport");
        Object ieObjList = mIE.invoke(maObj);
        this.importExportLists.add((List) ieObjList);
      }
    }
    else {
      Field fMA = trn.getClass().getField("MonetaryAmount");
      Object maObjList = fMA.get(trx);
      this.monetaryAmountList = (List)maObjList;

      importExportLists = new ArrayList<List>();
      for (Object maObj : this.monetaryAmountList) {
        Field fIE = maObj.getClass().getField("ImportExport");
        Object ieObjList = fIE.get(maObj);
        this.importExportLists.add((List) ieObjList);
      }
    }
  }

  private Object marshallObject(Object obj) {
    if (obj instanceof String) {
      return obj;
    }
    else if (obj instanceof BigDecimal) {
      return obj;
    }
    else if (obj instanceof LocalDate) {
      if (df1 == null)
      return df1.format((LocalDate)obj);
    }
    else if (obj instanceof java.util.Date) {
      if (df2 == null)
      return df2.format(obj);
    }
    else if (obj.getClass().isEnum()) {
      try {
        Method method = obj.getClass().getMethod("getName");
        return method.invoke(obj);
      } catch (Exception e) {
        //logger.warn("Cannot marshall " + obj.getClass().getName() + " to a String", e);
      }
    }
    else {
      try {
        if (obj.getClass().equals(String.class)) {
          return obj;
        } else {
          Field field = obj.getClass().getField("elementData");
          return field.get(obj);
        }
      } catch (IllegalAccessException e) {
        //logger.warn("Cannot marshall " + obj.getClass().getName() + " elementData to a String", e);
      } catch (NoSuchFieldException e) {
        // It is not a class with element data so ignore error
      }
    }
    return obj;
  }

  public Object getDataField(Scope scope, Object objLevel, final String field) {
    String[] fields = field.split("\\.");
    String fieldName = null;
    try {
      for (int i=0; i<fields.length; i++) {
        fieldName = fields[i];
        if (useMethod) {
          try {
            Method[] methods = objLevel.getClass().getMethods();
            Method m = getMethodByName(methods, "get" + fieldName);
            if (m != null) {
              objLevel = m.invoke(objLevel);
            }
            else {
              logger.debug("Cannot find method 'get" + fieldName + "' on class '" + objLevel.getClass().getName() + "' (" + field + ")");
              objLevel = null;
            }
          }
          catch (InvocationTargetException e) {
            logger.warn("Cannot call '" + "get" + fieldName + "'", e);
          }
        }
        else {
          Field f = objLevel.getClass().getField(fieldName);
          if (f != null) {
            objLevel = f.get(objLevel);
          }
          else {
            logger.debug("Cannot find field '" + fieldName + "' on class '" + objLevel.getClass().getName() + "' (" + field + ")");
            objLevel = null;
          }
        }
        if (objLevel == null) {
          if (i >= fields.length-1)
            return null;
          else {
            if (scope == Scope.Transaction) {

              if (i < 2 && (field.startsWith("Resident.") || field.startsWith("NonResident.")))
                return new IFinsurvContext.Undefined();
            }
            return null;
          }
        }
      }
      return marshallObject(objLevel);

    } catch (NoSuchFieldException e) {
      //logger.warn("Field " + fieldName + " not found while looking up '" + field + "' on a " + level, e);
      return new IFinsurvContext.Undefined();
    } catch (IllegalAccessException e) {
      //logger.warn("Cannot access Field " + fieldName + " while looking up '" + field + "' on a " + level, e);
      return new IFinsurvContext.Undefined();
    }
  }

  @Override
  public IFinsurvContextCache getFinsurvContextCache() {
    return validationCache;
  }

  public boolean hasField(Object obj, final String field) {
    try {
      if (useMethod) {
        try {
          Method[] methods = obj.getClass().getMethods();
          Method m = getMethodByName(methods, "get" + field);
          return (m != null) && (m.invoke(obj) != null);
        }
        catch (InvocationTargetException e) {
          return false;
        }
      }
      else {
        Field f = obj.getClass().getField(field);
        return f.get(obj) != null;
      }
    } catch (NoSuchFieldException e) {
      return false;
    } catch (IllegalAccessException e) {
      return false;
    }
  }

  public Object getTransactionField(final String field) {
    return getDataField(Scope.Transaction, trx, field);
  }

  public int getMoneySize() {
    return monetaryAmountList != null ? monetaryAmountList.size() : 0;
  }

  public Object getMoneyField(final int instance, final String field) {
    return getDataField(Scope.Money, monetaryAmountList.get(instance), field);
  }

  public int getImportExportSize(final int moneyInstance) {
    List list = importExportLists.get(moneyInstance);
  	if (list != null)
  		return list.size();
  	else
  		return 0;
  }

  public Object getImportExportField(final int moneyInstance, final int instance, final String field) {
    List list = importExportLists.get(moneyInstance);
    if (list != null)
      return getDataField(Scope.ImportExport, list.get(instance), field);
    else
      return null;
  }

  public void logTransactionEvent(final ResultType type, final String field, final String code, final String msg) {
  }

  public void logMoneyEvent(final ResultType type, final int instance, final String field, final String code, final String msg) {
  }

  public void logImportExportEvent(final ResultType type, final int moneyInstance, final int instance, final String field, final String code, final String msg) {
  }

  public void logTransactionDisplayEvent(final DisplayRuleType type, final String field, final List<String> values) {

  }

  public void logMoneyDisplayEvent(final DisplayRuleType type, int instance, final String field, final List<String> values) {

  }

  public void logImportExportDisplayEvent(final DisplayRuleType type, int moneyInstance, int instance, final String field, final List<String> values) {

  }
}
