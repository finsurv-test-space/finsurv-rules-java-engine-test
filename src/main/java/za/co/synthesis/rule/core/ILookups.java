package za.co.synthesis.rule.core;

import java.util.Map;

/**
 * User: jake
 * Date: 8/5/14
 * Time: 10:27 PM
 * This interface provides a set of function calls that check the validity of data against the internal lookup tables
 */
public interface ILookups {
  Boolean isValidCountryCode(final String code);

  Boolean isValidCurrencyCode(final String code);

  Boolean isValidCategory(final String flow, final String code);

  Boolean isValidSubCategory(final String flow, final String code, final String subcode);

  Boolean isReportingQualifier(final String value);

  Boolean isNonResExceptionName(final String value);

  Boolean isResExceptionName(final String value);

  Boolean isMoneyTransferAgent(final String value);

  Boolean isValidProvince(final String value);

  Boolean isValidCardType(final String value);

  Boolean isValidInstitutionalSector(final String value);

  Boolean isValidIndustrialClassification(final String value);

  Boolean isValidCustomsOfficeCode(final String value);

  String getLookupField(final String lookup, final Map<String,String> key, final String fieldName);
}
