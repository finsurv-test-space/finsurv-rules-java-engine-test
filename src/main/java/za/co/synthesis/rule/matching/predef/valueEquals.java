package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.matching.IMatchingFunction;
import za.co.synthesis.rule.matching.MatchingContext;
import za.co.synthesis.rule.support.Util;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif
import java.util.Date;

/**
 * Created by jake on 10/21/16.
 */
public class valueEquals implements IMatchingFunction {
  private final String fieldname;

  public valueEquals(String fieldname) {
    this.fieldname = fieldname;
  }

  @Override
  public boolean execute(MatchingContext context) {
    Object valueLhs = context.getFieldLhs(fieldname);
    Object valueRhs = context.getFieldRhs(fieldname);

    if (valueLhs != null) {
      if (valueRhs != null) {
        if ((valueLhs instanceof Date || valueLhs instanceof LocalDate) &&
                (valueRhs instanceof Date || valueRhs instanceof LocalDate)) {
          LocalDate dtLhs = Util.date(valueLhs);
          LocalDate dtRhs = Util.date(valueRhs);
          return dtLhs.equals(dtRhs);
        }
        else {
          if (valueLhs instanceof String && ((String)valueLhs).length() == 0)
            return false;
          if (valueRhs instanceof String && ((String)valueRhs).length() == 0)
            return false;
          return valueLhs.equals(valueRhs);
        }
      }
      else
        return false;
    }
    else {
      return false;
    }
  }
}

