package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.matching.IMatchingFunction;
import za.co.synthesis.rule.matching.MatchingContext;
import za.co.synthesis.rule.support.Util;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif
import java.util.Date;

/**
 * Created by jake on 6/16/16.
 */
public class equals implements IMatchingFunction {
  private final String fieldname;

  public equals(String fieldname) {
    this.fieldname = fieldname;
  }

  @Override
  public boolean execute(MatchingContext context) {
    Object valueLhs = context.getFieldLhs(fieldname);
    Object valueRhs = context.getFieldRhs(fieldname);

    if (valueLhs != null) {
      if (valueRhs != null) {
        if ((valueLhs instanceof Date || valueLhs instanceof LocalDate) &&
            (valueRhs instanceof Date || valueRhs instanceof LocalDate)) {
          LocalDate dtLhs = Util.date(valueLhs);
          LocalDate dtRhs = Util.date(valueRhs);
          return dtLhs.equals(dtRhs);
        }
        else if (fieldname.equals("DrCr")) {
          if (valueLhs.equals("-")) {
            return true;
          }
          else if (valueRhs.equals("-")) {
            return true;
          }
        }

        return valueLhs.equals(valueRhs);
      }
      else {
        // valueRhs == null
        if (fieldname.equals("DrCr")) {
            return true;
        }
        return false;
      }
    }
    else {
      // valueLhs == null
      if (fieldname.equals("DrCr")) {
        return true;
      }
      return (valueRhs == null);
    }
  }
}
