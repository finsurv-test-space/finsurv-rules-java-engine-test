package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/5/14
 * Time: 11:19 PM
 * Returns true if the given value is not a valid reporting qualifier
 */
public class notReportingQualifier implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !context.getLookups().isReportingQualifier(value.toString());
  }
}

/*
function notReportingQualifier(context, value) {
  if (!value)
    return true;
  return !context.lookups.isReportingQualifier(value);
}
*/