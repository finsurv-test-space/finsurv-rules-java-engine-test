package za.co.synthesis.rule.doc;

public enum SourceStatus {
  Inherited,
  Overriden,
  Defined
}
