package za.co.synthesis.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:20 PM
 * A Test implementation of the Validate_ReplacementTrnReference function. Needs to raise the following SARB errorS:
 * 389, "If ReplacementTransaction is N and the TrnReference is the same as a transaction previously cancelled, the ReplacementTransaction must be Y"
 * 212, "Transaction reference of cancelled transaction not stored on SARB database"
 */
public class Validate_ReplacementTrnReference implements ICustomValidate {
  private Map<String, TrnRefInfo> trnInfoMap;

  public Validate_ReplacementTrnReference(Map<String, TrnRefInfo> trnInfoMap) {
    this.trnInfoMap = trnInfoMap;
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String trnRef = null;
    if (otherInputs.length > 0)
      trnRef = otherInputs[0] != null ? otherInputs[0].toString() : null;
    String replacementYN = null;
    if (otherInputs.length > 1)
      replacementYN = otherInputs[1] != null ? otherInputs[1].toString() : null;
    String replacementTrnRef = null;
    if (otherInputs.length > 2)
      replacementTrnRef = otherInputs[2] != null ? otherInputs[2].toString() : null;


    String errorCode;
    String errorDesc;
    if (replacementYN != null && replacementYN.equals("N")) {
      errorCode = "389";
      errorDesc = "If ReplacementTransaction is N and the TrnReference is the same as a transaction previously cancelled, the ReplacementTransaction must be Y";

      if (trnInfoMap.containsKey(trnRef)) {
        TrnRefInfo trnInfo = trnInfoMap.get(trnRef);

        if (trnInfo.isExists() && trnInfo.isCancelled())
          return new CustomValidateResult(StatusType.Fail, errorCode, errorDesc);
        else
          return new CustomValidateResult(StatusType.Pass);
      }
    }
    else
    if (replacementYN != null && replacementYN.equals("Y") && replacementTrnRef != null && replacementTrnRef.length() > 0)
    {
      errorCode = "212";
      errorDesc = "Transaction reference of cancelled transaction not stored on SARB database";

      if (trnInfoMap.containsKey(replacementTrnRef)) {
        TrnRefInfo trnInfo = trnInfoMap.get(replacementTrnRef);

        if (!trnInfo.isExists() || !trnInfo.isCancelled())
          return new CustomValidateResult(StatusType.Fail, errorCode, errorDesc);
        else
          return new CustomValidateResult(StatusType.Pass);
      }
    }
    return new CustomValidateResult(StatusType.Pass);
  }
}
