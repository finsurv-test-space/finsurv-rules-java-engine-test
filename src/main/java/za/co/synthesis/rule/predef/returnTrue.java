package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

public class returnTrue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return true;
  }
}
