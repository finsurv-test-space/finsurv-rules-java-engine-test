package za.co.synthesis.javascript;

import java.util.regex.Pattern;

/**
 * User: jake
 * Date: 8/5/14
 * Time: 10:09 PM
 * This relates to the use of regular expression literals (/.../). These provide compilation of the regular expression
 * when the script is loaded. When the regular expression will remain constant then this is used for better performance.
 */
public class JSRegExLiteral implements IJSSnippetSupport {
  protected final JSSnippetSupport snippets = new JSSnippetSupport();
  private Pattern pattern;

  public JSRegExLiteral(String regex) {
    this.pattern = Pattern.compile(regex);
  }

  public Pattern getPattern() {
    return pattern;
  }

  @Override
  public String toString() {
    if (pattern != null)
      return "/" + pattern.toString() + "/";
    return "null";
  }

  public void compose(JSWriter writer) {
    if (pattern != null)
      writer.append('/').append(pattern.toString()).append('/');
    else
      writer.append("null");
  }

  @Override
  public void setPreceding(JSSnippetSupport preceding, int precedingIndex) {
    snippets.setPreceding(preceding, precedingIndex);
  }

  @Override
  public void addSnippet(JSSnippet snippet) {
    snippets.addSnippet(snippet);
  }

  @Override
  public int currentSnippetIndex() {
    return snippets.currentSnippetIndex();
  }

  @Override
  public void augmentEndSnippet(String extraText) {
    snippets.augmentEndSnippet(extraText);
  }

  @Override
  public void composeSnippets(JSWriter writer) {
    JSSnippet snippet = snippets.getFirstSnippetByType(JSSnippet.Type.Variable);
    if (pattern != null) {
      if (snippet != null) {
       writer.append(snippet.getText());
      }
      else {
        writer.append('/').append(pattern.toString()).append('/');
      }
    }
    else
      writer.append("null");
  }

  @Override
  public void removeExtraWhitespace() {
    snippets.removeExtraWhitespace(this);
  }

  @Override
  public void injectWhitespace(Object parentObject) {
    snippets.injectWhitespace(parentObject, this);
  }
}
