package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import java.util.Date;
import java.text.SimpleDateFormat;

public class isDateBefore implements IAssertionFunction {
    private Date date;

    public isDateBefore(String date) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        this.date = formatter.parse(date);
    }

    public boolean execute(FinsurvContext context, Object value) {
        try {
            String stringDate = value.toString();
            SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
            Date tempDate = formatter2.parse(stringDate);

            return tempDate.before(this.date);
        } catch (Exception e) {
            return false;
        }
    }
}
