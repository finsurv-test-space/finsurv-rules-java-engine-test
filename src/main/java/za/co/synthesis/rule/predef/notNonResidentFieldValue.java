package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.StringList;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 12:24 PM
 * Determines if the given nonresident field has not been provided or if it has been provided is not equal to the given
 * constant value
 */
public class notNonResidentFieldValue implements IAssertionFunction {
  private String field;
  private StringList constants = null;

  public notNonResidentFieldValue(String field, String constant) {
    this.field = field;
    this.constants = new StringList();
    this.constants.add(constant);
  }

  public notNonResidentFieldValue(String field, StringList constant) {
    this.field = field;
    this.constants = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField("NonResident.Individual." + field);
    if (fieldValue == null || fieldValue instanceof IFinsurvContext.Undefined) {
      fieldValue = context.getTransactionField("NonResident.Entity." + field);
    }
    if (fieldValue != null) {
      for (String constant : constants) {
        if (fieldValue.equals(constant))
          return false;
      }
    }
    return true;
  }
}
/*
function notNonResidentFieldValue(field, constant) {
  return function(context, value) {
    var fieldValue = context.getTransactionField("NonResident.Individual." + field);
    if (!fieldValue)
      fieldValue = context.getTransactionField("NonResident.Entity." + field);
    if (fieldValue) {
      if (typeof constant === "string") {
        if (fieldValue === constant)
          return false;
      } else {
        for ( var i = 0; i < constant.length; i++) {
          if (fieldValue === constant[i])
            return false;
        }
      }
    }
    return true;
  }
}
*/

