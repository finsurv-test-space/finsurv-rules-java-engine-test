package za.co.synthesis.rule.difference;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.rule.difference.validation.DifferenceType;

public interface IDifference {
  DifferenceType getType();
  String diffHTML();
  void addTo(JSArray array);
}
