package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * Created by jake on 11/2/16.
 */
public class hasCustomField implements IAssertionFunction {
  private String fieldname;

  public hasCustomField(String fieldname) {
    this.fieldname = fieldname;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object cv = context.getCustomValue().get(fieldname);
    return (cv != null);
  }
}

/*
  _export.hasCustomField = function hasCustomField(field){
    return function (context, value) {
      return !!(context.getCustomValue(field));
    }
  }*/
