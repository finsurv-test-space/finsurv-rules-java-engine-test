package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 12/10/15
 * Time: 3:39 PM
 * Returns true if there is no ImportUndertakingClient custom value or this value is not set to Y
 */
public class notImportUndertakingClient implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    Object cv = context.getCustomValue().get("LUClient");
    return cv != null && "N".equals(cv);
  }
}

/*
  _export.notImportUndertakingClient = function(context, value) {
    var iuc = context.getCustomValue("LUClient");
    return iuc && (iuc === "N");
  }
*/
