package za.co.synthesis.mapping.source;

import za.co.synthesis.mapping.configuration.MappingConfig;
import za.co.synthesis.mapping.configuration.MappingScope;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 11:38 AM
 * All sources need to implement the source manager interface. The source manager provides access to transaction entry
 * objects. The data points associated with the entries can then be accessed via named source data objects.
 * I.e. These source names are defined in the mapping file.
 */
public interface ISourceManager {
  void setup(MappingConfig mappingConfig);

  Object getNextEntry();
  ISourceData getSourceData(Object entry, String source, MappingScope mappingScope);
}
