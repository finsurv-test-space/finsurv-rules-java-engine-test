package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 12:47 PM
 * Interface to retireve custom values
 */
public interface ICustomValue {
  Object get(final String field);
}
