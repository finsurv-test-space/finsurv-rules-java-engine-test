package za.co.synthesis.rule.core;

import java.util.ArrayList;
import java.util.List;

public class ValidationResultsAndStats {
    private ValidationStats stats;
    private List<ResultEntry> results;

    public ValidationResultsAndStats() {
        this.stats = null;
        this.results = new ArrayList<ResultEntry>();
    }

    public ValidationResultsAndStats(ValidationStats stats, List<ResultEntry> results) {
        this.stats = stats;
        this.results = results;
    }

    public ValidationStats getStats() {
        return stats;
    }

    public void setStats(ValidationStats stats) {
        this.stats = stats;
    }

    public List<ResultEntry> getResults() {
        return results;
    }

    public void setResults(List<ResultEntry> results) {
        this.results = results;
    }
}
