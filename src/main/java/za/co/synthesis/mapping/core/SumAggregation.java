package za.co.synthesis.mapping.core;

import java.math.BigDecimal;
import java.util.List;

/**
 * User: jake
 * Date: 1/27/16
 * Time: 4:32 AM
 * Implements the sum (adding together) of all given values
 */
public class SumAggregation implements IAggregation {
  @Override
  public String aggregate(List<String> values) {
    if (values.size() > 0) {
      // Now do the sum
      BigDecimal total = new BigDecimal(0.00);
      for (String val : values) {
        BigDecimal valDec = new BigDecimal(0.00);
        if ((val != null) && (val.length() > 0)) {
          try {
            valDec = (new BigDecimal(val));
          } catch (Exception e) {
            //Expect this to fail on (blank values, null values or) non-numeric values - ie: "R10.00" etc.
//          System.err.println("Unable to parse '" + val + "' as a BigDecimal value (defaulted 0.00 value instead): "+ e.getMessage());
//          e.printStackTrace();
          }
        }
        total = total.add(valDec);
      }
      return total.toPlainString();
    }
    return null;
  }
}
