package za.co.synthesis.rule.classification.predef;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.classification.ClassificationContext;
import za.co.synthesis.rule.classification.IClassificationFunction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jake on 6/7/16.
 */
public class notPattern implements IClassificationFunction {
  private Pattern pattern;

  public notPattern(String regex) {
    this.pattern = Pattern.compile(regex);
  }

  public notPattern(JSRegExLiteral regex) {
    this.pattern = regex.getPattern();
  }

  public boolean execute(ClassificationContext context, Object value) {
    if (value == null)
      return true;

    Matcher matcher = pattern.matcher(value.toString());
    return !matcher.find();
  }
}
