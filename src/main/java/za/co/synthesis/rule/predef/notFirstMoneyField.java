package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 12:38 PM
 * Determine if we are currently processing 2nd or later monetary amounts
 */
public class notFirstMoneyField implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return context.getCurrentMoneyInstance() > 0;
  }
}
/*
function notFirstMoneyField(context, value) {
  return context.currentMoneyInstance > 0;
}
*/

