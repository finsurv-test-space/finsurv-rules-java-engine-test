package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.ILookups;

import java.util.Map;

/**
 * User: jake
 * Date: 8/11/14
 * Time: 3:44 PM
 * This lookup object always returns true ans is the base lookup object for the LookupAggregator
 */
public class AlwaysTrueLookup implements ILookups {
  @Override
  public Boolean isValidCountryCode(String code) {
    return true;
  }

  @Override
  public Boolean isValidCurrencyCode(String code) {
    return true;
  }

  @Override
  public Boolean isValidCategory(String flow, String code) {
    return true;
  }

  @Override
  public Boolean isValidSubCategory(String flow, String code, String subcode) {
    return true;
  }

  @Override
  public Boolean isReportingQualifier(String value) {
    return true;
  }

  @Override
  public Boolean isNonResExceptionName(String value) {
    return true;
  }

  @Override
  public Boolean isResExceptionName(String value) {
    return true;
  }

  @Override
  public Boolean isMoneyTransferAgent(String value) {
    return true;
  }

  @Override
  public Boolean isValidProvince(String value) {
    return true;
  }

  @Override
  public Boolean isValidCardType(String value) {
    return true;
  }

  @Override
  public Boolean isValidInstitutionalSector(String value) {
    return true;
  }

  @Override
  public Boolean isValidIndustrialClassification(String value) {
    return true;
  }

  @Override
  public Boolean isValidCustomsOfficeCode(String value) {
    return true;
  }

  @Override
  public String getLookupField(final String lookup, final Map<String,String> key, final String fieldName) {
    return null;
  }
}
