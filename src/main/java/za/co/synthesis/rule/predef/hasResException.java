package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 9:42 AM
 * Determines if the resident exception name matches the given constant
 */
public class hasResException implements IAssertionFunction {
  private String constant;

  public hasResException(String constant) {
    this.constant = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object exception = context.getTransactionField("Resident.Exception.ExceptionName");
    if (exception != null)
      return exception.equals(constant);
    return false;
  }
}
/*
function hasResException(type) {
  return function(context, value) {
    var exception = context.getTransactionField("Resident.Exception.ExceptionName");
    if (exception)
      return exception === type;
    return false;
  }
}
*/