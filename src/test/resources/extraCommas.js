 define([ 'require', './predef',,], function (require,  predef,,) {
  var detailTrans, detailMoney, detailImportExport, displayFuncs;

  displayFuncs = {
    cleanName: stripRegex("[\\/().,]",,).then(replaceRegex(/\s+/, " ",),).then(replaceRegex(/\s-+\s|\s-+|-+\s/, "-",),).then(trim,).then(nullIfEmpty,),
    cleanAddress: stripRegex("[\\/().,]",,).then(replaceRegex(/\s+/, " ",),).then(replaceRegex(/\s-+\s|\s-+|-+\s/, "-",),).then(trim,).then(nullIfEmpty,),
  };

  with (predef,) {
    with (displayFuncs,) {
      detailTrans = {
        ruleset: "Transaction \"Enrichment Rules",
        scope  : "transaction",
      };
      detailMoney = [
        {stuff: "value"},,
      ]
    }
  }
  return detailTrans;
});
