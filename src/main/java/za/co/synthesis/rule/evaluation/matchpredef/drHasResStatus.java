package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.core.ResidenceStatus;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;
import za.co.synthesis.rule.support.JSConstant;

import java.util.ArrayList;
import java.util.List;

public class drHasResStatus implements IMatchAssertionFunction {
  private List<ResidenceStatus> constants = new ArrayList<ResidenceStatus>();

  public drHasResStatus() {
  }

  public drHasResStatus(Object value) {
    if (value != null) {
      constants.add(JSConstant.residenceStatus(value));
    }
  }

  public drHasResStatus(List<Object> values) {
    if (values != null) {
      for (Object value : values) {
        constants.add(JSConstant.residenceStatus(value));
      }
    }
  }

  public void setConstant(ResidenceStatus constant) {
    this.constants.clear();
    this.constants.add(constant);
  }

  public void setConstants(List<ResidenceStatus> constants) {
    this.constants = constants;
  }

  @Override
  public boolean execute(MatchInfo matchInfo) {
    ResidenceStatus resStatus = matchInfo.getDrResStatus();

    if (resStatus != null) {
      return constants.contains(resStatus);
    }
    return false;
  }
}