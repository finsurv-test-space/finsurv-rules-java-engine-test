package za.co.synthesis.rule.support;

/**
 * User: jake
 * Date: 4/20/16
 * Time: 12:09 PM
 * This class allows for function chaining of display functions so that the output from one function can act as
 * input to next function
 */
public class DFThen implements IDisplayFunction {
  private IDisplayFunction left;
  private IDisplayFunction right;

  public DFThen(IDisplayFunction left, IDisplayFunction right) {
    this.left = left;
    this.right = right;
  }

  @Override
  public String execute(FinsurvContext context, Object value) {
    return right.execute(context, left.execute(context, value));
  }
}
