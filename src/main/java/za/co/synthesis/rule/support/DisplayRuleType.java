package za.co.synthesis.rule.support;

/**
 * Created by jake on 4/9/16.
 */
public enum DisplayRuleType {
  ExcludeValue,
  LimitValue,
  Show,
  Hide,
  Enable,
  Disable,
  ClearValue,
  SetValue,
  AppendValue,
  SetLabel,
  SetField,
  Extend
}
/*  ////------------------------------------------------------
  //// Display support functions.
  ////------------------------------------------------------

  function displaybase(type, value, srcField, assertion) {
    var result = {
      type     : type,
      value    : value,
      srcField : srcField,
      assertion: assertion
    };
    result.onInflow = function () {
      this.flow = "IN";
      return this;
    };
    result.onOutflow = function () {
      this.flow = "OUT";
      return this;
    };
    result.onSection = function (sections) {
      this.section = sections;
      return this;
    };
    result.onCategory = function (categories) {
      if (typeof categories == "string") {
        this.categories = [];
        this.categories.push(categories);
      } else
        this.categories = categories;
      return this;
    };
    result.notOnCategory = function (categories) {
      if (typeof categories == "string") {
        this.notCategories = [];
        this.notCategories.push(categories);
      } else
        this.notCategories = categories;
      return this;
    };
    return result;
  }

  function returnTrue(context, value) {
    return true;
  }


  function excludeValue(value, assertion) {
    if (!assertion)
      return displaybase("EXCLUDE", value, null, returnTrue);
    else
      return displaybase("EXCLUDE", value, null, assertion);
  }

  function limitValue(value, assertion) {
    if (!assertion)
      return displaybase("LIMIT", value, null, returnTrue);
    else
      return displaybase("LIMIT", value, null, assertion);
  }


  function show(assertion) {
    if (!assertion)
      return displaybase("SHOW", null, null, returnTrue);
    else
      return displaybase("SHOW", null, null, assertion);
  }

  function hide(assertion) {
    if (!assertion)
      return displaybase("HIDE", null, null, returnTrue);
    else
      return displaybase("HIDE", null, null, assertion);
  }

  function enable(assertion) {
    if (!assertion)
      return displaybase("ENABLE", null, null, returnTrue);
    else
      return displaybase("ENABLE", null, null, assertion);
  }

  function disable(assertion) {
    if (!assertion)
      return displaybase("DISABLE", null, null, returnTrue);
    else
      return displaybase("DISABLE", null, null, assertion);
  }

  function clearValue(assertion) {
    if (!assertion)
      return displaybase("CLEAR", null, null, returnTrue);
    else
      return displaybase("CLEAR", null, null, assertion);
  }

  function setValue(value, srcField, assertion) {
    if (!assertion)
      return displaybase("SET", value, srcField, returnTrue);
    else
      return displaybase("SET", value, srcField, assertion);
  }


  function setLabel(value, srcField, assertion) {
    if (!assertion)
      return displaybase("SETLABEL", value, srcField, returnTrue);
    else
      return displaybase("SETLABEL", value, srcField, assertion);
  }

  function appendValue(value, srcField, assertion) {
    if (!assertion)
      return displaybase("APP", value, srcField, returnTrue);
    else
      return displaybase("APP", value, srcField, assertion);
  }
*/