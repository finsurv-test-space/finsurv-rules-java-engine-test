package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

/**
 * User: jake
 * Date: 4/27/16
 * Time: 10:53 AM
 * This is a display function that is used to convert the current field to lowercase
 */
public class toLowerCase implements IDisplayFunction {
  @Override
  public String execute(FinsurvContext context, Object value) {
    if (value != null) {
      return value.toString().toLowerCase();
    }
    return null;
  }
}
