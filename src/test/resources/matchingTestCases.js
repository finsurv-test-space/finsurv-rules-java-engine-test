define(['matchBase'], function (matchBase) {
  with (testBase) {
    var test_cases = [
      assertNoMatch("Exact match (with cents)",
        {ValueDate: "2106-01-01", Currency: "USD", Amount: 24.56, DrCr: "D", TrnRef: "ref1", RelatedRef: ""},
        {ValueDate: "2106-01-01", Currency: "USD", Amount: 24.56, DrCr: "D", TrnRef: "ref2", RelatedRef: ""}),
      assertConfidence("Exact match (with cents)", 1.0,
        {ValueDate: "2106-01-01", Currency: "USD", Amount: 24.56, DrCr: "D", TrnRef: "12345678901234567890"},
        {ValueDate: "2106-01-01", Currency: "USD", Amount: 24.56, DrCr: "D", TrnRef: "12345678901234567890"}),
      assertConfidence("Exact match (with cents)", 1.0,
        {ValueDate: "2106-01-01", Currency: "USD", Amount: 24.56, DrCr: "D", TrnRef: "12345678901234567890"},
        {ValueDate: "2106-01-01", Currency: "USD", Amount: 24.56, DrCr: "D", TrnRef: "1234567890123456"}),
      assertNoMatch("Exact match (with cents)",
        {ValueDate: "2106-01-01", Currency: "USD", Amount: 24.56, DrCr: "D", TrnRef: "12345678901234567890"},
        {ValueDate: "2106-01-02", Currency: "USD", Amount: 24.56, DrCr: "D", TrnRef: "12345678901234567890"}),
      assertNoMatch("Ref match, amounts similar, dates similar",
        {ValueDate: "2016-06-07", Currency: "USD", Amount: 1513.85, DrCr: "C", TrnRef: "160607IC00849363", SourceRef: "160607IC00849363"},
        {ValueDate: "2016-01-02", Currency: "USD", Amount: 1510.35, DrCr: "C", TrnRef: "1605180155TT4660", SourceRef: "1605180155TT4660"}),
      assertConfidence("Ref match, amounts similar, dates similar", 0.778,
        {ValueDate: "2016-08-26", Currency: "USD", Amount: 100.00, DrCr: "D", TrnRef: "1608260155TT3061", SourceRef: "1608260155TT3061"},
        {ValueDate: "2016-08-29", Currency: "USD", Amount: 100.00, DrCr: "D", TrnRef: "1608260155TT3061", SourceRef: "1608260155TT3061"})
    ]
  }
})
