package za.co.synthesis.rule.evaluation;

/**
 * Created by jake on 6/28/16.
 */
public enum AssumptionRuleType {
  DecideCr,
  DecideDr,
  UseBoth,
  Use, // Deprecated (the functions UseCr and UseDr will be used going forward instead)
  UseCr,
  UseDr
}
