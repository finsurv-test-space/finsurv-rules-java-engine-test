package za.co.synthesis.rule.support;

/**
 * Created by jake on 4/10/16.
 */
public class Hide extends DisplayRule {
  public Hide(IAssertionFunction assertion) {
    super(DisplayRuleType.Hide, null, null, null, assertion);
  }
}
