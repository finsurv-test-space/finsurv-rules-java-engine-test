package za.co.synthesis.rule.difference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RuleMap {
  private List<String> ruleOrder = new ArrayList<String>();
  private Map<String, FieldRule> fieldRuleMap = new HashMap<String, FieldRule>();

  public void clear() {
    ruleOrder.clear();
    fieldRuleMap.clear();
  }

  public void add(FieldRule fieldRule) {
    String name = fieldRule.getRule().getName();
    if (fieldRuleMap.put(name, fieldRule) != null) {
      ruleOrder.remove(name);
    }
    ruleOrder.add(name);
  }

  public FieldRule get(String name) {
    return fieldRuleMap.get(name);
  }

  public void remove(String name) {
    if (fieldRuleMap.remove(name) != null) {
      ruleOrder.remove(name);
    }
  }

  public List<String> ruleList() {
    return ruleOrder;
  }
}