package za.co.synthesis.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:16 PM
 * A Test implementation of the Validate_ValidCCN function. Needs to raise the following SARB error:
 * 322, "Not a registered customs client number"
 */
public class Validate_ValidCCN implements ICustomValidate {
  private Map<String, CCNInfo> ccnInfoMap;

  public Validate_ValidCCN(Map<String, CCNInfo> ccnInfoMap) {
    this.ccnInfoMap = ccnInfoMap;
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String ccn = value.toString();

    String errorCode = "322";
    String errorDesc = "Not a registered customs client number";

    if (ccnInfoMap.containsKey(ccn)) {
      CCNInfo ccnInfo = ccnInfoMap.get(ccn);

      if (ccnInfo.isValidCCN())
        return new CustomValidateResult(StatusType.Pass);
      else
        return new CustomValidateResult(StatusType.Fail, errorCode, errorDesc);
    }
    else {
      return new CustomValidateResult(StatusType.Pass);
    }
  }
}
