package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 1:05 PM
 * Determine if the current value equals the given monetary amount field value
 */
public class equalsMoneyField implements IAssertionFunction {
  private IStringResolver field;

  public equalsMoneyField(IStringResolver field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getMoneyField(context.getCurrentMoneyInstance(), field.resolve(context, value));

    if(field.resolve(context, value).equals("ForeignValue"))
    {
      try {
        return Float.parseFloat(fieldValue.toString()) == Float.parseFloat(value.toString());
      } catch(Exception e) {
        return false;
      }
    }
    else {
      if (fieldValue != null)
        return fieldValue.equals(value);
      else
        return false;
    }
  }
}
/*
function equalsMoneyField(field) {
  return function(context, value) {
    var fieldValue = context.getMoneyField(context.currentMoneyInstance, field);
    if (fieldValue)
      return (fieldValue == value);
    else
      return false;
  }
}
*/
