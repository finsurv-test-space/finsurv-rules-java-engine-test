package za.co.synthesis.rule.core;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.rule.classification.Classifications;
import za.co.synthesis.rule.matching.Matchings;
import za.co.synthesis.rule.support.RulePackage;
import za.co.synthesis.rule.support.RulePackages;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/16/16.
 */
public class MatchingEngine {
  private boolean setupTime = true;
  private Matchings matchings;

  public MatchingEngine() {}

  private void checkSetup() throws MatchingException {
    if (!setupTime) {
      throw new MatchingException("Validators have already been issued. You cannot make any further call to setup functions.");
    }
  }

  public static EngineVersion getVersion() {
    return EngineVersion.getVersion();
  }

  /**
   * This will load classification rules from a JavaScript source (In the classification rule DSL format).
   * Earlier rules loaded first will take mwb950493
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once validators are issued
   * @param name
   * @param reader
   * @throws Exception
   */
  public synchronized void setupLoadRule(final String name, final Reader reader) throws MatchingException {
    checkSetup();

    if (matchings == null) {
      matchings = new Matchings();
    }
    try {
      JSStructureParser parser = new JSStructureParser(reader);

      Object obj = parser.parse();

      List<JSObject> rulesetObjs = new ArrayList<JSObject>();
      JSUtil.findObjectsWith(obj, "ruleset", rulesetObjs);
      for (JSObject rulesetObj : rulesetObjs) {
        matchings.loadRuleSet(rulesetObj);
      }
    }
    catch (Exception e) {
      throw new MatchingException("Could not parse '" + name + "' rule file", e);
    }
  }

  /**
   * This will load rules from a JavaScript file (In the rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once validators are issued
   * @param filename
   * @throws Exception
   */
  public void setupLoadRuleFile(final String filename) throws MatchingException {
    try {
      FileReader fileReader = new FileReader(filename);
      setupLoadRule(filename, fileReader);
    }
    catch (Exception e) {
      throw new MatchingException("Could not load '" + filename + "' rule file", e);
    }
  }

  /**
   * This will load rules from a JavaScript file (In the rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once validators are issued
   * @param fileUrl
   * @throws Exception
   */
  public void setupLoadRuleUrl(final URL fileUrl) throws MatchingException {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));
      setupLoadRule(fileUrl.getPath(), buffReader);
    }
    catch (Exception e) {
      throw new MatchingException("Could not load '" + fileUrl.getPath() + "' rule file", e);
    }
  }

  private void setupLoadMatchingsFromPath(final String packagepath) throws MatchingException {
    String validationPath = packagepath + File.separator + "matching";
    File file = new File(validationPath);
    if (file.isDirectory()) {
      String nameMatching = validationPath + File.separator + "matching.js";
      File fileMatching = new File(nameMatching);
      if (fileMatching.isFile())
        setupLoadRuleFile(nameMatching);
    }
  }

  /**
   * This will load rules and lookups from a package path. This is the simplest way to load all appropriate
   * configurations for a specific application.
   * @param filepath
   * @return the engine version required to execute this package of rules
   * @throws Exception
   */
  public EngineVersion setupLoadPackagePath(final String filepath) throws MatchingException {
    EngineVersion highestVersion = null;
    String highestVersionPackageName = null;
    try {
      File file = new File(filepath);
      if (file.isDirectory()) {
        String packageRoot = file.getParent();
        String packagePath = file.getAbsolutePath();
        String packageFileName = packagePath + File.separator + "package.js";
        FileReader fileReader = new FileReader(packageFileName);

        RulePackage rule_package = RulePackages.loadRulePackage(fileReader);
        if (rule_package == null) {
          throw new ValidationException("Could not correctly parse '" + filepath + "' package. Has it been properly defined?");
        }
        highestVersion = rule_package.getValidationEngine();
        highestVersionPackageName = file.getName();
        setupLoadMatchingsFromPath(packagePath);

        if (rule_package.getDependsOn() != null) {
          EngineVersion reqVersion = setupLoadPackagePath(packageRoot + File.separator + rule_package.getDependsOnName());
          if (reqVersion.isHigher(highestVersion)) {
            highestVersionPackageName = rule_package.getDependsOnName();
            highestVersion = reqVersion;

          }
        }
      }
    }
    catch (Exception e) {
      throw new MatchingException("Could not load '" + filepath + "' package", e);
    }

    if (highestVersion != null) {
      if (highestVersion.isHigher(getVersion())) {
        throw new MatchingException("The '" + highestVersionPackageName + "' package requires version " + highestVersion.toString() +
                " and this engine version is " + getVersion().toString());
      }
    }
    return highestVersion;
  }

  public synchronized Matcher issueMatcher() throws MatchingException {
    setupTime = false;

    return new Matcher(matchings.matchingConfidences(null));
  }

  public synchronized Sharder issueSharder() throws MatchingException {
    setupTime = false;

    return new Sharder(matchings.matchingBuckets(null));
  }
}
