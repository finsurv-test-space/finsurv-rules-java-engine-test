package za.co.synthesis.rule.core;

import za.co.synthesis.rule.core.ResultType;
import za.co.synthesis.rule.support.DisplayRuleType;

import java.util.List;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 8:11 PM
 * The interface to a data context that will allow access to all data
 */
public interface IFinsurvContext {
  public static class Undefined {}

  IFinsurvContextCache getFinsurvContextCache();

  boolean hasField(Object obj, final String field);

  Object getTransactionField(final String field);

  int getMoneySize();

  Object getMoneyField(final int instance, final String field);

  int getImportExportSize(final int moneyInstance);

  Object getImportExportField(final int moneyInstance, final int instance, final String field);

  void logTransactionEvent(final ResultType type, final String field, final String code, final String msg);

  void logMoneyEvent(final ResultType type, final int instance, final String field, final String code, final String msg);

  void logImportExportEvent(final ResultType type, final int moneyInstance, final int instance, final String field, final String code, final String msg);

  void logTransactionDisplayEvent(final DisplayRuleType type, final String field, final List<String> values);

  void logMoneyDisplayEvent(final DisplayRuleType type, int instance, final String field, final List<String> values);

  void logImportExportDisplayEvent(final DisplayRuleType type, int moneyInstance, int instance, final String field, final List<String> values);
}
