package za.co.synthesis.rule.evaluation.predef;

import za.co.synthesis.rule.evaluation.GivenInfo;
import za.co.synthesis.rule.evaluation.IAssumptionAssertionFunction;

/**
 * Created by jake on 6/28/16.
 */
public class field72 implements IAssumptionAssertionFunction {
  private final String contains;

  public field72(String contains) {
    this.contains = contains;
  }

  @Override
  public boolean execute(GivenInfo givenInfo) {
    if (givenInfo.getField72() != null) {
      return givenInfo.getField72().contains(contains);
    }
    return false;
  }
}
