package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

public class evalIEField implements IAssertionFunction {
  private IStringResolver field;
  private IAssertionFunction fEval = null;

  public evalIEField(IStringResolver field, IAssertionFunction fEval) {
    this.field = field;
    this.fEval = fEval;
  }

  public boolean execute(FinsurvContext context, Object value) {
    int moneyIndex = context.getCurrentMoneyInstance();
    if (moneyIndex == -1)
      moneyIndex = 0;
    int index = context.getCurrentImportExportInstance();
    if (index == -1)
      index = 0;
    Object fieldValue = context.getImportExportField(moneyIndex, index, field.resolve(context, value));
    if (fieldValue != null && fEval != null) {
      return fEval.execute(context, fieldValue);
    }
    return false;
  }
}

/*
    _export.evalIEField = function (field, fEval) {
      return function(context, value) {
        var moneyIndex = context.currentMoneyInstance;
        if (moneyIndex == -1)
          moneyIndex = 0;
        var index = context.currentImportExportInstance;
        if (index == -1)
          index = 0;
        var fieldValue = context.getImportExportField(moneyIndex,index, field);

        if (fieldValue) {
          return fEval(context, fieldValue);
        }
        return false;
      };
    }
*/