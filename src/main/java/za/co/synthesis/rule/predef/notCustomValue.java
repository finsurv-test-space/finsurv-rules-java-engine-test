package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

public class notCustomValue  implements IAssertionFunction {
    private String fieldname;
    private String constant;

    public notCustomValue(String fieldname, String constant) {
        this.fieldname = fieldname;
        this.constant = constant;
    }

    public boolean execute(FinsurvContext context, Object value) {
        Object cv = context.getCustomValue().get(fieldname);
        return (!constant.equals(cv));
    }
}
