package za.co.synthesis.rule.core;

import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.core.impl.JSLookups;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.rule.support.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 12:12 PM
 * This should be a singleton object within the scope of a single set of validation rules. I.e. If all transactions
 * are validated against one set of validation rules then there is only a need to have one instance of this class.
 * Multi threading: All setup methods MUST called first before this class is used for validation purposes. All
 * calls to the setup methods will cause an exception once validators are issued
 */
public class ValidationEngine {
  private boolean setupTime = true;
  private LookupAggregator lookups = new LookupAggregator();
  private ISupporting supporting;
  private CustomValidateRegistry customValidateRegistry = new CustomValidateRegistry();
  private CentralCustomValidationCache centralValidateCache;
  private Validations validations;
  private ValidationsCache validationsCache;
  private boolean overrideLookups = true;
  private boolean ignoreMissingCustomValidate = false;
  private String basePackagePath = null;
  private String packagePath = null;
  private String packageName = null;
  private RulePackage loadedPackage = null;
  private Map<String, String> mappings = null;

  public ValidationEngine() {
    this.supporting = new DefaultSupporting();
    this.centralValidateCache = new CentralCustomValidationCache(customValidateRegistry);
  }

  public String getPackagePath() {
    return packagePath;
  }

  public String getPackageName() {
    return packageName;
  }

  public String getBasePackagePath() {
    return basePackagePath;
  }

  public void setBasePackagePath(String basePackagePath) {
    this.basePackagePath = basePackagePath;
  }

  private void checkSetup() throws ValidationException {
    if (!setupTime) {
      throw new ValidationException("Validators have already been issued. You cannot make any further call to setup functions.");
    }
  }

  /***
   * Retrieves the underlying rulesets. This is typically only required by code that needs detailed and raw information
   * associated with the rules and their respective rulesets.
   * @return The list of rulesets that were loaded
   */
  public List<RuleSet> getRuleSets() {
    if (validations != null) {
      return validations.getRuleSets();
    }
    return new ArrayList<RuleSet>();
  }

  /***
   * Retrieves the mappings to use from either the loaded package data or the inline mappings provided in the rules file.
   * The Loaded Packae mapping take precidence if supplied.
   * @return
   */
  public Map<String, String> getMappings() {
    return loadedPackage != null ? loadedPackage.getMapping() : mappings;
  }

  public static EngineVersion getVersion() {
    return EngineVersion.getVersion();
  }

  public synchronized void registerCustomValidate(String name, ICustomValidate callback) {
    customValidateRegistry.register(name, callback);
  }

  public synchronized void registerCustomValidate(String name, CacheStrategy cacheStrategy, ICustomValidate callback) {
    customValidateRegistry.register(name, callback, cacheStrategy);
  }

  public CentralCustomValidationCache getCentralValidateCache() {
    return centralValidateCache;
  }

  public void setCentralValidateCache(CentralCustomValidationCache centralValidateCache) {
    this.centralValidateCache = centralValidateCache;
  }

  public synchronized void registerCustomValidate(String name, ICustomValidate callback, String... fields) {
    String[] inputs = new String[fields.length];
    for (int i=0; i<fields.length; i++) {
      inputs[i] = fields[i];
    }
    customValidateRegistry.register(name, callback, inputs);
  }

  public synchronized void registerCustomValidate(String name, CacheStrategy cacheStrategy, ICustomValidate callback, String... fields) {
    String[] inputs = new String[fields.length];
    for (int i=0; i<fields.length; i++) {
      inputs[i] = fields[i];
    }
    customValidateRegistry.register(name, callback, inputs, cacheStrategy);
  }


  public synchronized void clearCustomValidate() {
    customValidateRegistry.clear();
  }

  public List<String> getCustomValidateList() { return validations.customValidateList(); }

  public boolean isIgnoreMissingCustomValidate() {
    return ignoreMissingCustomValidate;
  }

  public void setIgnoreMissingCustomValidate(boolean ignoreMissingCustomValidate) {
    this.ignoreMissingCustomValidate = ignoreMissingCustomValidate;
  }

  public synchronized void setupCustomValidateFactory(String customValidateFactory) throws ValidationException {
    try {
      ICustomValidateFactory factory = (ICustomValidateFactory)Class.forName(customValidateFactory).newInstance();
      factory.register(this);
    } catch (InstantiationException e) {
      throw new ValidationException("Cannot register custom validations", e);
    } catch (IllegalAccessException e) {
      throw new ValidationException("Cannot register custom validations", e);
    } catch (ClassNotFoundException e) {
      throw new ValidationException("Cannot register custom validations from '" + customValidateFactory + "'", e);
    }
  }

  public synchronized void setupCustomValidateFactory(ICustomValidateFactory factory) {
    factory.register(this);
  }

  /**
   * This will load lookups from a JavaScript file. Later lookup instances will replace earlier lookups in the event
   * that a second lookup file is used. Therefore custom lookup list must be loaded after the standard lookups.
   * This is a setup function and cannot be called once validators are issued
   * @param reader
   * @throws Exception
   */
  public synchronized void setupLoadLookup(final String name, final Reader reader) throws ValidationException {
    checkSetup();

    ILookups jsLookups = new JSLookups();
    boolean hasData = false;

    try {
      JSStructureParser parser = new JSStructureParser(reader);

      Object obj = parser.parse();

      JSObject lookupObj = JSUtil.findObjectWith(obj, "ReportingQualifier");
      if (lookupObj == null)
        lookupObj = JSUtil.findObjectWith(obj, "currencies");
      if (lookupObj == null)
        lookupObj = JSUtil.findObjectWith(obj, "countries");
      if (lookupObj == null)
        lookupObj = JSUtil.findObjectWith(obj, "provinces");
      if (lookupObj != null) {
        hasData = true;
        ((JSLookups)jsLookups).load(lookupObj);
      }
    }
    catch (Exception e) {
      throw new ValidationException("Could not parse '" + name + "' lookup file", e);
    }
    if (hasData) {
      if (overrideLookups)
        lookups.overrideLookups(jsLookups);
      else
        lookups.appendLookups(jsLookups);
    }
  }

  /**
   * This will load lookups from a JavaScript file. Later lookup instances will replace earlier lookups in the event
   * that a second lookup file is used. Therefore custom lookup list must be loaded after the standard lookups.
   * This is a setup function and cannot be called once validators are issued
   * @param filename
   * @throws Exception
   */
  public void setupLoadLookupFile(final String filename) throws ValidationException {
    try {
      FileReader fileReader = new FileReader(filename);
      setupLoadLookup(filename, fileReader);
    }
    catch (Exception e) {
      throw new ValidationException("Could not load '" + filename + "' lookup file", e);
    }
  }

  /**
   * This will load lookups from a JavaScript file. Later lookup instances will replace earlier lookups in the event
   * that a second lookup file is used. Therefore custom lookup list must be loaded after the standard lookups.
   * This is a setup function and cannot be called once validators are issued
   * @param fileUrl
   * @throws Exception
   */
  public void setupLoadLookupUrl(final URL fileUrl) throws ValidationException {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));
      setupLoadLookup(fileUrl.getPath(), buffReader);
    }
    catch (Exception e) {
      throw new ValidationException("Could not load '" + fileUrl.getPath() + "' lookup file", e);
    }
  }

  public synchronized void setupLookups(ILookups lookups) throws ValidationException {
    checkSetup();

    this.lookups.overrideLookups(lookups);
  }

  public synchronized void setupSupporting(ISupporting supporting) throws ValidationException {
    checkSetup();

    this.supporting = supporting;
  }

  /**
   * This will load rules from a JavaScript source (In the rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once validators are issued
   * @param name
   * @param reader
   * @throws Exception
   */
  private synchronized void setupLoadRule(final String packageName, final String name, final Reader reader) throws ValidationException {
    checkSetup();

    if (validations == null) {
      validations = new Validations();
      validationsCache = new ValidationsCache(validations, 50);
    }
    try {
      JSStructureParser parser = new JSStructureParser(reader);

      Object obj = parser.parse();

      List<JSObject> rulesetObjs = new ArrayList<JSObject>();
      JSUtil.findObjectsWith(obj, "ruleset", rulesetObjs);

      JSObject jsResult = JSUtil.findObjectWith(obj, "mappings");
      if (jsResult != null) {
        Object objMappings = jsResult.get("mappings");
        if (objMappings instanceof JSObject) {
          mappings = new HashMap<String, String>();
          for (Map.Entry<String, Object> entry : ((JSObject)objMappings).entrySet()) {
            mappings.put(entry.getKey(), entry.getValue() != null ? entry.getValue().toString() : null);
          }
        }
      }
      for (JSObject rulesetObj : rulesetObjs) {
        validations.loadRuleSet(packageName, getMappings(), rulesetObj);
      }
    }
    catch (Exception e) {
      throw new ValidationException("Could not parse '" + name + "' rule file", e);
    }
  }

  /**
   * This will load rules from a JavaScript source (In the rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once validators are issued
   * @param name
   * @param reader
   * @throws Exception
   */
  public synchronized void setupLoadRule(final String name, final Reader reader) throws ValidationException {
    setupLoadRule("unspecified", name, reader);
  }

  /**
   * This will load rules from a JavaScript file (In the rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once validators are issued
   * @param filename
   * @throws Exception
   */
  public void setupLoadRuleFile(final String filename) throws ValidationException {
    try {
      FileReader fileReader = new FileReader(filename);
      setupLoadRule(filename, fileReader);
    }
    catch (Exception e) {
      throw new ValidationException("Could not load '" + filename + "' rule file", e);
    }
  }

  /**
   * This will load rules from a JavaScript file (In the rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later. This implementation is used internally and links the package name to the news.
   * This is a setup function and cannot be called once validators are issued
   * @param packageName - the package name associated with the rules being loaded.
   * @param filename - the name of the file with the rules that will be loaded
   * @throws Exception
   */
  private void setupLoadRuleFile(final String packageName, final String filename) throws ValidationException {
    try {
      FileReader fileReader = new FileReader(filename);
      setupLoadRule(packageName, filename, fileReader);
    }
    catch (Exception e) {
      throw new ValidationException("Could not load '" + filename + "' rule file", e);
    }
  }

  /**
   * This will load rules from a JavaScript file (In the rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once validators are issued
   * @param fileUrl
   * @throws Exception
   */
  public void setupLoadRuleUrl(final URL fileUrl) throws ValidationException {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));
      setupLoadRule(fileUrl.getPath(), buffReader);
    }
    catch (Exception e) {
      throw new ValidationException("Could not load '" + fileUrl.getPath() + "' rule file", e);
    }
  }

  public static List<RuleSet> loadValidationRuleSetsFromPath(final String packageName, final String packagepath) throws ValidationException {
    ValidationEngine engine = new ValidationEngine();
    engine.setupLoadValidationsFromPath(packageName, packagepath);
    return engine.getRuleSets();
  }

  private void setupLoadValidationsFromPath(final String packageName, final String packagepath) throws ValidationException {
    String validationPath = packagepath + File.separator + "validation";
    File file = new File(validationPath);
    if (file.isDirectory()) {
      String nameTransaction = validationPath + File.separator + "transaction.js";
      File fileTransaction = new File(nameTransaction);
      if (fileTransaction.isFile())
        setupLoadRuleFile(packageName, nameTransaction);

      String nameMoney = validationPath + File.separator + "money.js";
      File fileMoney = new File(nameMoney);
      if (fileMoney.isFile())
        setupLoadRuleFile(packageName, nameMoney);

      String nameImpExp = validationPath + File.separator + "importexport.js";
      File fileImpExp = new File(nameImpExp);
      if (fileImpExp.isFile())
        setupLoadRuleFile(packageName, nameImpExp);
    }
  }

  public static List<RuleSet> loadDocumentRuleSetsFromPath(final String packageName, final String packagepath) throws ValidationException {
    ValidationEngine engine = new ValidationEngine();
    engine.setupLoadDocumentsRulesFromPath(packageName, packagepath);
    return engine.getRuleSets();
  }

  private void setupLoadDocumentsRulesFromPath(final String packageName, final String packagepath) throws ValidationException {
    String validationPath = packagepath + File.separator + "document";
    File file = new File(validationPath);
    if (file.isDirectory()) {
      String nameTransaction = validationPath + File.separator + "transaction.js";
      File fileTransaction = new File(nameTransaction);
      if (fileTransaction.isFile())
        setupLoadRuleFile(packageName, nameTransaction);

      String nameMoney = validationPath + File.separator + "money.js";
      File fileMoney = new File(nameMoney);
      if (fileMoney.isFile())
        setupLoadRuleFile(packageName, nameMoney);

      String nameImpExp = validationPath + File.separator + "importexport.js";
      File fileImpExp = new File(nameImpExp);
      if (fileImpExp.isFile())
        setupLoadRuleFile(packageName, nameImpExp);
    }
  }

  private void setupLoadLookupsFromPath(final String packagepath) throws ValidationException {
    String dataPath = packagepath + File.separator + "data";
    File file = new File(dataPath);
    if (file.isDirectory()) {
      String nameLookups = dataPath + File.separator + "lookups.js";
      File fileLookups = new File(nameLookups);
      if (fileLookups.isFile())
        setupLoadLookupFile(nameLookups);
    }
  }

  public EngineVersion setupLoadPackagePath(final String filepath) throws ValidationException {
    return setupLoadPackagePath(filepath, true, false);
  }

  private String determinePackagePath(String packageName, String currentPackageRoot, String basePackageRoot) {
    String packagePath = currentPackageRoot + File.separator + packageName;
    File file = new File(packagePath);
    if (file.isDirectory()) {
      return packagePath;
    }
    else {
      if (basePackageRoot != null) {
        packagePath = basePackageRoot + File.separator + packageName;
        file = new File(packagePath);
        if (file.isDirectory()) {
          return packagePath;
        }
      }
    }
    return currentPackageRoot + File.separator + packageName;
  }

  /**
   * This will load rules and lookups from a package path. This is the simplest way to load all appropriate
   * configurations for a specific application.
   * @param filepath
   * @return the engine version required to execute this package of rules
   * @throws Exception
   */
  public EngineVersion setupLoadPackagePath(final String filepath, boolean loadValidationRulesInPackage, boolean loadDocumentRulesInPackage) throws ValidationException {
    overrideLookups = false;
    EngineVersion highestVersion = null;
    String highestVersionPackageName = null;
    try {
      File file = new File(filepath);
      if (file.isDirectory()) {
        String packageName = file.getName();
        String packageRoot = file.getParent();
        String packagePath = file.getAbsolutePath();
        String packageFileName = packagePath + File.separator + "package.js";
        FileReader fileReader = new FileReader(packageFileName);

        RulePackage rule_package = RulePackages.loadRulePackage(fileReader);
        if (rule_package == null) {
          throw new ValidationException("Could not correctly parse '" + filepath + "' package. Has it been properly defined?");
        }
        if (loadedPackage == null) {
          this.packagePath = filepath;
          this.packageName = packageName;
          loadedPackage = new RulePackage(rule_package);
        }
        else {
          loadedPackage.mergePriorPackage(rule_package);
        }

        highestVersion = rule_package.getValidationEngine();
        highestVersionPackageName = file.getName();
        setupLoadLookupsFromPath(packagePath);
        if (loadValidationRulesInPackage)
          setupLoadValidationsFromPath(packageName, packagePath);
        if (loadDocumentRulesInPackage)
          setupLoadDocumentsRulesFromPath(packageName, packagePath);

        if (rule_package.getFeatureSize() > 0) {
          for (int i=0; i<rule_package.getFeatureSize(); i++) {
            String feature_path = determinePackagePath(rule_package.getFeatureName(i), packageRoot, basePackagePath);
            String feature_packageFileName = feature_path + File.separator + "package.js";
            FileReader feature_fileReader = new FileReader(feature_packageFileName);
            RulePackage feature_rule_package = RulePackages.loadRulePackage(feature_fileReader);

            if (feature_rule_package != null) {
              loadedPackage.mergePriorPackage(feature_rule_package);
              EngineVersion reqVersion = feature_rule_package.getValidationEngine();
              if (reqVersion != null && reqVersion.isHigher(highestVersion)) {
                highestVersionPackageName = rule_package.getFeatureName(i);
                highestVersion = reqVersion;
              }
            }
            setupLoadLookupsFromPath(feature_path);
            if (loadValidationRulesInPackage)
              setupLoadValidationsFromPath(packageName, feature_path);
            if (loadDocumentRulesInPackage)
              setupLoadDocumentsRulesFromPath(packageName, feature_path);

            if (feature_rule_package.getDependsOn() != null) {
              EngineVersion reqVersion = setupLoadPackagePath(
                      determinePackagePath(feature_rule_package.getDependsOnName(), packageRoot, basePackagePath),
                      loadValidationRulesInPackage, loadDocumentRulesInPackage);
              if (reqVersion.isHigher(highestVersion)) {
                highestVersionPackageName = feature_rule_package.getDependsOnName();
                highestVersion = reqVersion;
              }
            }
          }
        }

        if (rule_package.getDependsOn() != null) {
          EngineVersion reqVersion = setupLoadPackagePath(
                  determinePackagePath(rule_package.getDependsOnName(), packageRoot, basePackagePath),
                  loadValidationRulesInPackage, loadDocumentRulesInPackage);
          if (reqVersion.isHigher(highestVersion)) {
            highestVersionPackageName = rule_package.getDependsOnName();
            highestVersion = reqVersion;
          }
        }
      }
    }
    catch (Exception e) {
      throw new ValidationException("Could not load '" + filepath + "' package", e);
    }

    if (highestVersion != null) {
      if (highestVersion.isHigher(getVersion())) {
        throw new ValidationException("The '" + highestVersionPackageName + "' package requires version " + highestVersion.toString() +
          " and this engine version is " + getVersion().toString());
      }
    }
    return highestVersion;
  }

  private synchronized void checkValidationFunctions()  throws ValidationException {
    List<String> customValidates = validations.customValidateList();
    boolean missingCustomValidates = false;
    StringBuilder sb = new StringBuilder();

    for (String cv : customValidates) {
      if (!customValidateRegistry.contains(cv)) {
        if (ignoreMissingCustomValidate) {
          registerCustomValidate(cv, new Validate_PassAll());
        }
        else {
          missingCustomValidates = true;
          if (sb.length() > 0)
            sb.append(", ");
          sb.append(cv);
        }
      }
    }

    if (missingCustomValidates) {
      throw new ValidationException("Have not registered the following custom validate functions: " + sb.toString());
    }
  }

  public synchronized Validator issueValidator() throws ValidationException {
    // Check that all required custom validation functions have been registered
    if (setupTime) {
      checkValidationFunctions();
    }
    setupTime = false;

    return new Validator(getMappings(),
            lookups, supporting, validationsCache, centralValidateCache, customValidateRegistry);
  }

  public ValidationSchema.Structure getStructure() {
    return validations.getStructure();
  }

  public RulePackage getLoadedPackage() {
    return loadedPackage;
  }

  /**
   * Utility function to guess what documents may be required for a given category code and flow type.
   * @param categoryCode - Compound category code in the format "mmm[/ss]" where mmm is the main category code
   *                     and ss is the sub category code. eg "401" or "101/03"
   * @param flow - Flow type. Optional, if not specified returns for IN and OUT.
   * @return List of rules that match
   */
  public List<Rule> getPotentialDocuments(String categoryCode, FlowType flow) throws ValidationException{
    if(validations==null) throw new ValidationException("Cannot run. The validation engine has not been set up yet.");

    ArrayList<Rule> docRules = (ArrayList<Rule>) validations.getDocumentRules();
    String mainCat = categoryCode.split("/")[0];

    ArrayList<Rule> out = new ArrayList<Rule>();

    for(Rule rule: docRules){
      // check flow first...
      FlowType ruleFlow = rule.getFlow();
      if (ruleFlow == null){
        ruleFlow = FlowType.Unknown;
      }
      if(flow != null && ruleFlow != FlowType.Unknown && ruleFlow != flow) continue;
      // check categories...
      List<String> categories = rule.getCategories();
      List<String> notCategories = rule.getNotCategories();
      // if categories or notCategories aren't specified, include this rule.
      if ((categories == null || categories.size() == 0) && (notCategories == null || notCategories.size() == 0)) {
        out.add(rule);
        continue;
      }
      Boolean found = true;
      //If there is an inclusion (white) list
      if (categories != null){
        //and it's NOT in the list, then exclude the rule from output.
        if (validations.categoryNotInList(categoryCode, mainCat, categories)) {
          found = false;
        }
      }
      //if there's an exclusion (black) list
      if (notCategories != null) {
        //and it's IN the exclusion list, then exclude from output.
        if (!validations.categoryNotInList(categoryCode, mainCat, notCategories)) {
          found = false;
        }
      }
      if(found){
        out.add(rule);
      }
    }

    return out;
  }
}
