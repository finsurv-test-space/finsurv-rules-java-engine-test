package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.*;

import java.util.HashMap;
import java.util.Map;

/**
 * User: jake
 * Date: 2/26/15
 * Time: 10:20 AM
 * A registry of all the validation callbacks registered with the validation engine
 */
public class CustomValidateRegistry {
  public static class Entry {
    private ICustomValidate customValidate;
    private String[] inputParams;
    private CacheStrategy cacheStrategy;

    public Entry(ICustomValidate customValidate) {
      this.customValidate = customValidate;
      this.inputParams = new String[0];
      this.cacheStrategy = completeCacheStrategy(null);
    }

    public Entry(ICustomValidate customValidate, String[] inputParams) {
      this.customValidate = customValidate;
      this.inputParams = inputParams;
      this.cacheStrategy = completeCacheStrategy(null);
    }

    public Entry(ICustomValidate customValidate, String[] inputParams, CacheStrategy cacheStrategy) {
      this.customValidate = customValidate;
      this.inputParams = inputParams;
      this.cacheStrategy = completeCacheStrategy(cacheStrategy);
    }

    public ICustomValidate getCustomValidate() {
      return customValidate;
    }

    public String[] getInputParams() {
      return inputParams;
    }

    private CacheStrategy completeCacheStrategy(CacheStrategy cacheStrategy) {
      CacheStrategy result = cacheStrategy;
      if (result == null) {
        result = CacheStrategy.set(StatusType.Pass, CachePeriod.Long);
      }
      StatusCacheStrategy sct = result.getCacheStrategyByStatus(StatusType.Pass);
      if (sct == null) {
        result.and(StatusType.Pass, CachePeriod.Long);
      }
      sct = result.getCacheStrategyByStatus(StatusType.Fail);
      if (sct == null) {
        result.and(StatusType.Fail, CachePeriod.Medium);
      }
      sct = result.getCacheStrategyByStatus(StatusType.Error);
      if (sct == null) {
        result.and(StatusType.Error, CachePeriod.Short);
      }
      return result;
    }

    public CachePeriod getCachePeriodByStatus(StatusType statusType) {
      return cacheStrategy.getCacheStrategyByStatus(statusType).getCachePeriod();
    }
  }

  private final Map<String, Entry> registry = new HashMap<String, Entry>();

  public CustomValidateRegistry() {
  }

  public CustomValidateRegistry(CustomValidateRegistry other) {
    registry.putAll(other.registry);
  }

  public void clear() {
    registry.clear();
  }

  public void register(String name, ICustomValidate callback, String[] inputParams, CacheStrategy cacheStrategy) {
    if (registry.containsKey(name)) {
      registry.remove(name);
    }
    registry.put(name, new Entry(callback, inputParams, cacheStrategy));
  }

  public void register(String name, ICustomValidate callback, String[] inputParams) {
    if (registry.containsKey(name)) {
      registry.remove(name);
    }
    registry.put(name, new Entry(callback, inputParams));
  }

  public void register(String name, ICustomValidate callback) {
    if (registry.containsKey(name)) {
      registry.remove(name);
    }
    registry.put(name, new Entry(callback));
  }

  public void register(String name, ICustomValidate callback, CacheStrategy cacheStrategy) {
    if (registry.containsKey(name)) {
      registry.remove(name);
    }
    registry.put(name, new Entry(callback, new String[0], cacheStrategy));
  }

  public Entry get(String name) {
    if (registry.containsKey(name)) {
      return registry.get(name);
    }
    return null;
  }

  public boolean contains(String name) {
    return registry.containsKey(name);
  }
}
