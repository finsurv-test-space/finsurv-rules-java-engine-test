package za.co.synthesis.rule.matching;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.matching.predef.constantConfidence;
import za.co.synthesis.rule.support.DSLSupport;
import za.co.synthesis.rule.support.StringList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/16/16.
 */
public class Matchings {
  Logger logger = LoggerFactory.getLogger(Matchings.class);

  private final List<MatchingRuleSet> ruleSets = new ArrayList<MatchingRuleSet>();

  public Matchings() {
  }

  private IMatchingFunction loadApplyFunc(IMatchingFunction left, JSIdentifier apply) throws Exception {
    IMatchingFunction result = left;
    if (left != null && apply != null) {
      if (apply instanceof JSFunctionCall) {
        JSFunctionCall applyFunc = (JSFunctionCall)apply;
        if (applyFunc.getName().equals("and")) {
          result = new MFAnd(left, loadAssertion((JSIdentifier)applyFunc.getParameters().get(0)));
        }
        else
        if (applyFunc.getName().equals("or")) {
          result = new MFOr(left, loadAssertion((JSIdentifier)applyFunc.getParameters().get(0)));
        }

        result = loadApplyFunc(result, applyFunc.getApply());
      }
    }
    return result;
  }

  private IMatchingFunction loadAssertion(JSIdentifier identifier) throws Exception {
    IMatchingFunction result = null;
    if (identifier instanceof JSFunctionCall) {
      JSFunctionCall func = (JSFunctionCall)identifier;

      if (func.getParameters().size() == 1) {
        Object param = func.getParameters().get(0);
        if (param instanceof JSArray) {
          StringList params = DSLSupport.composeStringList(param);
          result = MatchingFunctionFactory.createFunction(identifier.getName(), params);
        }
        else {
          if (param instanceof String)
            result = MatchingFunctionFactory.createFunction(identifier.getName(), (String) param);
          else
          if (param instanceof JSRegExLiteral)
            result = MatchingFunctionFactory.createFunction(identifier.getName(), (JSRegExLiteral) param);
          else
          if (param instanceof JSIdentifier)
            result = MatchingFunctionFactory.createFunction(identifier.getName(), loadAssertion((JSIdentifier) param));
          else {
            result = MatchingFunctionFactory.createFunction(identifier.getName(), param.toString());
          }
        }
      }

      if (func.getParameters().size() == 2) {
        String param1 = func.getParameters().get(0).toString();
        Object param2 = func.getParameters().get(1);
        if (param2 instanceof JSArray) {
          StringList params = DSLSupport.composeStringList(param2);
          result = MatchingFunctionFactory.createFunction(identifier.getName(), param1, params);
        }
        else
        if (param2 instanceof JSIdentifier) {
          result = MatchingFunctionFactory.createFunction(identifier.getName(), param1, loadAssertion((JSIdentifier) param2));
        }
        else {
          result = MatchingFunctionFactory.createFunction(identifier.getName(), param1, param2.toString());
        }
      }
    }
    else {
      result = MatchingFunctionFactory.createFunction(identifier.getName());
    }

    result = loadApplyFunc(result, identifier.getApply());
    return result;
  }

  private IMatchingFunction getAssertion(JSFunctionCall jsFunc, int paramIndex) throws Exception {
    if (jsFunc.getParameters().size() > paramIndex) {
      Object objExp = jsFunc.getParameters().get(paramIndex);
      if (objExp instanceof JSIdentifier) {
        return loadAssertion((JSIdentifier)objExp);
      }
    }
    return null;
  }

  private IConfidenceFunction loadConfidenceApplyFunc(IConfidenceFunction left, JSIdentifier apply) throws Exception {
    IConfidenceFunction result = left;
    if (left != null && apply != null) {
      if (apply instanceof JSFunctionCall) {
        JSFunctionCall applyFunc = (JSFunctionCall)apply;
        if (applyFunc.getName().equals("multiply")) {
          result = new CFMultiply(left, loadConfidence((JSIdentifier)applyFunc.getParameters().get(0)));
        }
        result = loadConfidenceApplyFunc(result, applyFunc.getApply());
      }
    }
    return result;
  }


  private IConfidenceFunction loadConfidence(JSIdentifier identifier) throws Exception {
    IConfidenceFunction result = null;
    if (identifier instanceof JSFunctionCall) {
      JSFunctionCall func = (JSFunctionCall)identifier;

      if (func.getParameters().size() == 5) {
        Object param1 = func.getParameters().get(0);
        Object param2 = func.getParameters().get(1);
        Object param3 = func.getParameters().get(2);
        Object param4 = func.getParameters().get(3);
        Object param5 = func.getParameters().get(4);
        if (param1 instanceof String && param2 instanceof String &&
                param3 instanceof String && param4 instanceof String && param5 instanceof String) {
          result = ConfidenceFunctionFactory.createFunction(identifier.getName(),
                  (String)param1, (String)param2, (String)param3, (String)param4, (String)param5);
        }
      }
    }
    else {
      result = ConfidenceFunctionFactory.createFunction(identifier.getName());
    }

    result = loadConfidenceApplyFunc(result, identifier.getApply());
    return result;
  }

  private IConfidenceFunction getConfidence(JSFunctionCall jsFunc, int paramIndex) throws Exception {
    if (jsFunc.getParameters().size() > paramIndex) {
      Object objExp = jsFunc.getParameters().get(paramIndex);
      if (objExp instanceof JSIdentifier) {
        return loadConfidence((JSIdentifier)objExp);
      }
      else
      if (objExp instanceof String) {
        return new constantConfidence(objExp.toString());
      }
    }
    return null;
  }

  private MatchConfidence loadRule(JSFunctionCall jsFunc) throws Exception {
    MatchConfidence result = null;

    if (jsFunc.getName().equals("confidence")) {
      String ruleName = jsFunc.getParameters().get(0).toString();
      IConfidenceFunction confidence = getConfidence(jsFunc, 1);
      IMatchingFunction assertion = getAssertion(jsFunc, 2);
      if (assertion != null) {
        result = new MatchConfidence(ruleName, confidence, assertion);
      }
    }
    return result;
  }

  private IShardFunction loadShardApplyFunc(IShardFunction left, JSIdentifier apply) throws Exception {
    IShardFunction result = left;
    if (left != null && apply != null) {
      if (apply instanceof JSFunctionCall) {
        JSFunctionCall applyFunc = (JSFunctionCall)apply;
        if (applyFunc.getName().equals("then")) {
          result = new SFThen(left, loadShard((JSIdentifier)applyFunc.getParameters().get(0)));
        }
        result = loadShardApplyFunc(result, applyFunc.getApply());
      }
    }
    return result;
  }

  private IShardFunction loadShard(JSIdentifier identifier) throws Exception {
    IShardFunction result = null;
    if (identifier instanceof JSFunctionCall) {
      JSFunctionCall func = (JSFunctionCall)identifier;

      if (func.getParameters().size() == 1) {
        Object param1 = func.getParameters().get(0);
        if (param1 instanceof String) {
          result = ShardFunctionFactory.createFunction(identifier.getName(), (String)param1);
        }
      }
      else
      if (func.getParameters().size() == 2) {
        Object param1 = func.getParameters().get(0);
        Object param2 = func.getParameters().get(1);
        if (param1 instanceof String && param2 instanceof String) {
          result = ShardFunctionFactory.createFunction(identifier.getName(), (String)param1, (String)param2);
        }
      }
    }
    else {
      result = ShardFunctionFactory.createFunction(identifier.getName());
    }

    result = loadShardApplyFunc(result, identifier.getApply());
    return result;
  }

  public void loadRuleSet(JSObject jsRuleSet) {
    Object rulesetName = jsRuleSet.get("ruleset");

    if (rulesetName != null) {
      MatchingRuleSet ruleset = new MatchingRuleSet(rulesetName.toString());

      JSArray jsMatches = (JSArray)jsRuleSet.get("match");
      if (jsMatches != null) {
        for (Object obj : jsMatches) {
          if (obj instanceof JSFunctionCall) {
            try {
              MatchConfidence matchConfidence = loadRule((JSFunctionCall) obj);
              ruleset.addMatchConfidence(matchConfidence);
            } catch (Exception e) {
              logger.info("Error loading match", e);
              System.out.println("Error loading match: " + e.getMessage());
            }
          }
        }
      }

      JSArray jsBuckets = (JSArray)jsRuleSet.get("buckets");
      if (jsBuckets != null) {
        for (Object obj : jsBuckets) {
          if (obj instanceof JSObject) {
            JSObject jsBucket = (JSObject) obj;
            String bucketName = (String) jsBucket.get("name");
            Object shard = jsBucket.get("shard");
            if (shard instanceof JSIdentifier) {
              try {
                IShardFunction shardFunc = loadShard((JSIdentifier) shard);
                ruleset.addMatchBucket(new MatchBucket(bucketName, shardFunc));
              } catch (Exception e) {
                logger.info("Error loading shard", e);
                System.out.println("Error loading shard: " + e.getMessage());
              }
            }
          }
        }
      }
      ruleSets.add(ruleset);
    }
  }

  public List<MatchConfidence> matchingConfidences(String rulename) {
    List<MatchConfidence> result = new ArrayList<MatchConfidence>();
    for (MatchingRuleSet ruleset : ruleSets) {
      for (MatchConfidence matchConfidence : ruleset.getMatchConfidences()) {
        // if there is a rulename, match it with rule.name
        if (rulename != null && matchConfidence.getName() != null) {
          if (!matchConfidence.getName().equals(rulename)) {
            continue;
          }
        }
        result.add(matchConfidence);
      }
    }
    return result;
  }

  public List<MatchBucket> matchingBuckets(String bucketname) {
    List<MatchBucket> result = new ArrayList<MatchBucket>();
    for (MatchingRuleSet ruleset : ruleSets) {
      for (MatchBucket matchBucket : ruleset.getMatchBuckets()) {
        // if there is a rulename, match it with rule.name
        if (bucketname != null && matchBucket.getName() != null) {
          if (!matchBucket.getName().equals(bucketname)) {
            continue;
          }
        }
        result.add(matchBucket);
      }
    }
    return result;
  }
}
