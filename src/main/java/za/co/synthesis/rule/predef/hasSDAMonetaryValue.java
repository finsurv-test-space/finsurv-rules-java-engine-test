package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

public class hasSDAMonetaryValue implements IAssertionFunction
{
    @Override
    public boolean execute(FinsurvContext context, Object value) {
        BigDecimal result = new BigDecimal(0.00);
        String idNumber = value.toString();
        map mapper = new map("{{LocalValue}}");

        for (int i = 0; i < context.getMoneySize(); i++) {
            Object subjectField = context.getMoneyField(i, "AdHocRequirement.Subject");
            BigDecimal amountField = Util.number(context.getMoneyField(i, mapper.resolve(context, value)));
            String fieldIdNumber = null;

            if (subjectField != null && subjectField.equals("SDA")) {
                if (amountField != null) {
                    Object fieldId = context.getMoneyField(i, "ThirdParty.Individual.IDNumber");
                    if (fieldId != null) {
                        fieldIdNumber = fieldId.toString();
                    }
                    else {
                        fieldId = context.getTransactionField("Resident.Individual.IDNumber");
                        if (fieldId != null) {
                            fieldIdNumber = fieldId.toString();
                        }
                    }

                    if (fieldIdNumber != null && fieldIdNumber.equals(idNumber)) {
                        result = result.add(amountField);
                    }
                }
            }
        }
        return result.floatValue() > 0.01;
    }
}