package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.Scope;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 4/9/16.
 */
public class DisplayRuleSet {
  private String ruleset;
  private Scope scope;
  private List<FieldDisplay> fields = new ArrayList<FieldDisplay>();

  public DisplayRuleSet(String ruleset, Scope scope) {
    this.ruleset = ruleset;
    this.scope = scope;
  }

  public DisplayRuleSet addField(FieldDisplay field) {
    fields.add(field);
    return this;
  }

  public String getRuleset() {
    return ruleset;
  }

  public Scope getScope() {
    return scope;
  }

  public List<FieldDisplay> getFields() {
    return fields;
  }
}
