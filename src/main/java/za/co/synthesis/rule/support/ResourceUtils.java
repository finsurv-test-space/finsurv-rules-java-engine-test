package za.co.synthesis.rule.support;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class ResourceUtils {
  public static final String CLASSPATH = "classpath:";
  public static final String FILE_NOHOST = "file:///";
  public static final String FILE = "file:/";

  public static enum ResourceType {
    NONE,
    CLASSPATH,
    FILE
  };

  public static class Resource {
    private final ResourceType type;
    private String name;

    public Resource(ResourceType type, String resourceName) {
      this.type = type;
      this.name = resourceName;
    }

    public ResourceType getType() {
      return type;
    }

    public String getName() {
      return name;
    }
  }

  public static ClassLoader getDefaultClassLoader() {
    ClassLoader cl = null;
    try {
      cl = Thread.currentThread().getContextClassLoader();
    }
    catch (Throwable ex) {
      // Cannot access thread context ClassLoader - falling back...
    }
    if (cl == null) {
      // No thread context class loader -> use class loader of this class.
      cl = ResourceUtils.class.getClassLoader();
      if (cl == null) {
        // getClassLoader() returning null indicates the bootstrap ClassLoader
        try {
          cl = ClassLoader.getSystemClassLoader();
        }
        catch (Throwable ex) {
          // Cannot access system ClassLoader - oh well, maybe the caller can live with null...
        }
      }
    }
    return cl;
  }

  public static String composeClasspathPath(String... strings) {
    String result = "";
    for (String str : strings) {
      if (result.length() > 0) {
        result += "/";
      }
      result += str;
    }
    return result;
  }

  public static Resource parseResource(String path) {
    ResourceType type = ResourceType.NONE;
    String resourceName = null;

    if (path.startsWith(CLASSPATH)) {
      type = ResourceType.CLASSPATH;
      resourceName = path.substring(CLASSPATH.length());
      if(resourceName.startsWith("/"))
        resourceName = resourceName.substring(1);
    }
    else
    if (path.startsWith(FILE_NOHOST)) {
      type = ResourceType.FILE;
      resourceName = path.substring(FILE_NOHOST.length());
    }
    else
    if (path.startsWith(FILE)) {
      type = ResourceType.FILE;
      resourceName = path.substring(FILE.length());
    }
    else
    if (path.startsWith("/") || path.startsWith(".")) {
      type = ResourceType.FILE;
      resourceName = path;
    }
    return new Resource(type, resourceName);
  }

  public static File getResourceAsFile(String path) {
    ResourceUtils.Resource resource = ResourceUtils.parseResource(path);
    if (resource.getType() == ResourceType.CLASSPATH) {
      ClassLoader loader = ResourceUtils.getDefaultClassLoader();
      URL url = loader.getResource(resource.getName());
      if (url != null)
        return new File(url.getPath());
    } else if (resource.getType() == ResourceType.FILE) {
      return new File(resource.getName());
    }
    return null;
  }

  private static String readFileAsString(File file) throws IOException {
    InputStream in = new FileInputStream(file);
    byte[] b  = new byte[(int) file.length()];
    int len = b.length;
    int total = 0;

    while (total < len) {
      int result = in.read(b, total, len - total);
      if (result == -1) {
        break;
      }
      total += result;
    }

    return new String(b, StandardCharsets.UTF_8);
  }

  private static String readInputStreamAsString(InputStream inputStream) throws IOException {
    ByteArrayOutputStream result = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    for (int length; (length = inputStream.read(buffer)) != -1; ) {
      result.write(buffer, 0, length);
    }
    return result.toString("UTF-8");
  }

  public static String getResourceAsString(String path) throws IOException {
    ResourceUtils.Resource resource = ResourceUtils.parseResource(path);
    if (resource.getType() == ResourceType.CLASSPATH) {
      ClassLoader loader = ResourceUtils.getDefaultClassLoader();
      InputStream is = loader.getResourceAsStream(resource.getName());
      if (is != null)
        return readInputStreamAsString(is);
    } else if (resource.getType() == ResourceType.FILE) {
      return readFileAsString(new File(resource.getName()));
    }
    return null;
  }

  public static void copyFile(File source, File dest) throws IOException {
    InputStream in = new BufferedInputStream(new FileInputStream(source));
    OutputStream out = new BufferedOutputStream(new FileOutputStream(dest));
    try {
      byte[] buffer = new byte[1024];
      int lengthRead;
      while ((lengthRead = in.read(buffer)) > 0) {
        out.write(buffer, 0, lengthRead);
        out.flush();
      }
    } finally {
      in.close();
      out.close();
    }
  }

  public static File[] getResourceFolderFiles (String folder) {
    ResourceUtils.Resource resource = ResourceUtils.parseResource(folder);
    if (resource.getType() == ResourceType.CLASSPATH) {
      ClassLoader loader = ResourceUtils.getDefaultClassLoader();
      URL url = loader.getResource(resource.getName());
      if (url != null) {
        String path = url.getPath();
        return new File(path).listFiles();
      }
    } else if (resource.getType() == ResourceType.FILE) {
      return new File(resource.getName()).listFiles();
    }
    return new File[0];
  }

  public static boolean hasResourceFolder (String folder) {
    ResourceUtils.Resource resource = ResourceUtils.parseResource(folder);
    if (resource.getType() == ResourceType.CLASSPATH) {
      ClassLoader loader = ResourceUtils.getDefaultClassLoader();
      URL url = loader.getResource(resource.getName());
      return (url != null);
    } else if (resource.getType() == ResourceType.FILE) {
      return new File(resource.getName()).exists();
    }
    return false;
  }
}
