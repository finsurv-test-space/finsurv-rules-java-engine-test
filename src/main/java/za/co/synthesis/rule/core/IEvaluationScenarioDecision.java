package za.co.synthesis.rule.core;

import java.util.List;

public interface IEvaluationScenarioDecision {
  List<IEvaluationDecision> getDecisions();

  String getScenario();

  String getInformation();
}
