package za.co.synthesis.rule.difference;

import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.rule.support.Rule;

import java.util.List;

public class FieldRule {
  private final Scope scope;
  private final List<String> fields;
  private final Rule rule;

  public FieldRule(Scope scope, List<String> fields, Rule rule) {
    this.scope = scope;
    this.fields = fields;
    this.rule = rule;
  }

  public Scope getScope() {
    return scope;
  }

  public List<String> getFields() {
    return fields;
  }

  public Rule getRule() {
    return rule;
  }

  public boolean equalsRule(Rule rule) {
    if (DiffUtils.areObjectsTheSame(this.rule.getCode(), rule.getCode()) &&
            DiffUtils.areObjectsTheSame(this.rule.getMessage(), rule.getMessage()) &&
            DiffUtils.areObjectsTheSame(this.rule.getType(), rule.getType()) &&
            DiffUtils.areObjectsTheSame(this.rule.getFlow(), rule.getFlow()) &&
            DiffUtils.areObjectsTheSame(this.rule.getSection(), rule.getSection()) &&
            DiffUtils.areObjectsTheSame(this.rule.getCategories(), rule.getCategories()) &&
            DiffUtils.areObjectsTheSame(this.rule.getNotCategories(), rule.getNotCategories()) &&
            DiffUtils.areObjectsTheSame(this.rule.getAssertionDSL(), rule.getAssertionDSL())) {
      return true;
    }
    return false;
  }
}