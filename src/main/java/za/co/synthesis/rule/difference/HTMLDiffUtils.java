package za.co.synthesis.rule.difference;

import za.co.synthesis.rule.support.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HTMLDiffUtils {
  private static class StringPart {
    private String part;
    private String newPart;

    public StringPart(String part) {
      this.part = part;
    }

    public StringPart(String part, String newPart) {
      this.part = part;
      this.newPart = newPart;
    }

    public String getPart() {
      return part;
    }

    public String getNewPart() {
      return newPart;
    }
  }

  public static void main(String[] args) {
    System.out.println(composeStrings("this is a example", "this is a examp")); // prints (le,)
    System.out.println(composeStrings("is is a example", "this is a example")); // prints (le,)
    System.out.println(composeStrings("Honda", "Hyundai")); // prints (o,yui)
    System.out.println(composeStrings("Toyota", "Coyote")); // prints (Ta,Ce)
    System.out.println(composeStrings("Flomax", "Volmax")); // prints (Fo,Vo)
  }

  private static String commonStart(String oldString, String newString) {
    int len = oldString.length();
    if (len > newString.length()) {
      len = newString.length();
    }
    for(int i=0; i<len; i++) {
      if (oldString.charAt(i) != newString.charAt(i)) {
        if (i > 0) {
          return oldString.substring(0, i);
        }
        else {
          return "";
        }
      }
      else {
        if (i == len-1) {
          return oldString.substring(0, len);
        }
      }
    }
    return "";
  }

  private static String commonEnd(String oldString, String newString) {
    int len = oldString.length();
    if (len > newString.length()) {
      len = newString.length();
    }
    for(int i=1; i<=len; i++) {
      if (oldString.charAt(oldString.length() - i) != newString.charAt(newString.length() - i)) {
        if (i > 1) {
          return oldString.substring(oldString.length() - i + 1);
        }
        else {
          return "";
        }
      }
      else {
        if (i == len) {
          return oldString.substring(oldString.length() - len);
        }
      }
    }
    return "";
  }

  public static String composeWholeStrings(String oldString, String newString) {
    if (oldString == null || oldString.length() == 0) {
      if (newString == null || newString.length() == 0) {
        return "";
      }
      else {
        return "<ins>" + newString + "</ins>";
      }
    }
    else
    if (newString == null || newString.length() == 0) {
      return "<del>" + oldString + "</del>";
    }
    else
    if(oldString.equals(newString)) {
      return oldString;
    }
    else {
      return "<del>" + oldString + "</del><ins>" + newString + "</ins>";
    }
  }

  public static String composeStrings(String oldString, String newString) {
    if (oldString == null || oldString.length() == 0) {
      if (newString == null || newString.length() == 0) {
        return "";
      }
      else {
        return "<ins>" + newString + "</ins>";
      }
    }
    else
    if (newString == null || newString.length() == 0) {
      return "<del>" + oldString + "</del>";
    }
    else
    if(oldString.equals(newString)) {
      return oldString;
    }
    else {
      String commonStart = commonStart(oldString, newString);
      String commonEnd = commonEnd(oldString, newString);

      int endIndex = newString.length()-commonEnd.length();
      int beginIndex = commonStart.length();
      String insString;
      if (endIndex - beginIndex > 0) {
        insString = newString.substring(beginIndex, endIndex);
      }
      else {
        if (commonEnd.length() > 0) {
          commonEnd = commonEnd.substring(1);
        }
        insString = "";
      }

      String delString = oldString.substring(commonStart.length(), oldString.length()-commonEnd.length());


      return commonStart +
              (delString.length() > 0 ? "<del>" + delString + "</del>" : "") +
              (insString.length() > 0 ? "<ins>" + insString + "</ins>" : "") +
              commonEnd;
    }
  }

  public static String composeNumberOrString(Object value) {
    if (value == null) {
      return "null";
    }
    else {
      if (Util.isNumeric(value)) {
        return value.toString();
      }
      else {
        return "\"" + value.toString() + "\"";
      }
    }
  }

  public static String composeValues(String oldValue, String newValue) {
    if (oldValue == null) {
      if (newValue == null) {
        return "null";
      }
      else {
        return "<del>null</del><ins>" + composeNumberOrString(newValue) + "</ins>";
      }
    }
    else {
      if (newValue == null) {
        return "<del>" + composeNumberOrString(oldValue) + "</del><ins>null</ins>";
      }
      else {
        if (oldValue.equals(newValue)) {
          return composeNumberOrString(oldValue);
        }
        else {
          return "<del>" + composeNumberOrString(oldValue) + "</del><ins>" + composeNumberOrString(newValue) + "</ins>";
        }
      }
    }
  }

  public static void appendNameValue(StringBuilder sb, final String separator, final String name, Object value) {
    if (value != null) {
      if (sb.length() > 0) {
        sb.append(separator);
      }
      sb.append(name).append(": ").append(composeNumberOrString(value));
    }
  }

  public static void appendNameValues(StringBuilder sb, final String separator, final String name,
                                      Object oldValue, Object newValue) {
    if (oldValue == null) {
      if (newValue == null) {
        return;
      }
      else {
        if (sb.length() > 0) {
          sb.append(separator);
        }
        sb.append("<ins>").append(name).append(": ").append(composeNumberOrString(newValue)).append("</ins>");
      }
    }
    else {
      if (sb.length() > 0) {
        sb.append(separator);
      }
      if (newValue == null) {
        sb.append("<del>").append(name).append(": ").append(composeNumberOrString(oldValue)).append("</del>");
      }
      else {
        if (oldValue.equals(newValue)) {
          sb.append(name).append(": ").append(composeNumberOrString(oldValue));
        }
        else {
          sb.append(name).append(": ").append("<del>").append(composeNumberOrString(oldValue)).
                  append("</del><ins>").append(composeNumberOrString(newValue)).append("</ins>");
        }
      }
    }
  }

  /* This is an experimental piece of code to refine string differences */
  private static class DiffParts {
    public String first;
    public String second;

    public DiffParts(String first, String second) {
      this.first = first;
      this.second = second;
    }

    public List<String> parts = new ArrayList<String>();

    public String toString() {
      return "(" + first + "," + second + ")";
    }
  }

  /**
   * Examples
   */
  public static void main2(String[] args) {
    System.out.println(diff("this is a example", "this is a examp")); // prints (le,)
    System.out.println(diff("Honda", "Hyundai")); // prints (o,yui)
    System.out.println(diff("Toyota", "Coyote")); // prints (Ta,Ce)
    System.out.println(diff("Flomax", "Volmax")); // prints (Fo,Vo)
  }

  /**
   * Returns a minimal set of characters that have to be removed from (or added to) the respective
   * strings to make the strings equal.
   */
  private static DiffParts diff(String a, String b) {
    return diffHelper(a, b, new HashMap<Long, DiffParts>());
  }

  /**
   * Recursively compute a minimal set of characters while remembering already computed substrings.
   * Runs in O(n^2).
   */
  private static DiffParts diffHelper(String a, String b, Map<Long, DiffParts> lookup) {
    long key = ((long) a.length()) << 32 | b.length();
    if (!lookup.containsKey(key)) {
      DiffParts value;
      if (a.isEmpty() || b.isEmpty()) {
        value = new DiffParts(a, b);
      } else if (a.charAt(0) == b.charAt(0)) {
        value = diffHelper(a.substring(1), b.substring(1), lookup);
        value.parts.add(a.substring(0, 1));
      } else {
        DiffParts aa = diffHelper(a.substring(1), b, lookup);
        DiffParts bb = diffHelper(a, b.substring(1), lookup);
        if (aa.first.length() + aa.second.length() < bb.first.length() + bb.second.length()) {
          value = new DiffParts(a.charAt(0) + aa.first, aa.second);
          value.parts.add(a.substring(0, 1));
          value.parts.add(aa.first);
        } else {
          value = new DiffParts(bb.first, b.charAt(0) + bb.second);
          value.parts.add(b.substring(0, 1));
          value.parts.add(bb.first);
        }
      }
      lookup.put(key, value);
    }
    return lookup.get(key);
  }
}