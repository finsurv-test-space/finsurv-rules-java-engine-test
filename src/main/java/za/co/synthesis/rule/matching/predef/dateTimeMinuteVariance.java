package za.co.synthesis.rule.matching.predef;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.rule.matching.IConfidenceFunction;
import za.co.synthesis.rule.matching.MatchingContext;
import za.co.synthesis.rule.support.Util;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDateTime;
import za.co.synthesis.rule.support.legacydate.temporal.ChronoUnit;
#else*/
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
//#endif

/**
 * Created by jake on 10/21/16.
 */
public class dateTimeMinuteVariance implements IConfidenceFunction {
  Logger logger = LoggerFactory.getLogger(numberVariance.class);

  private final String fieldName;
  private final double fromAmount;
  private final double toAmount;
  private final double fromConfidence;
  private final double toConfidence;

  public dateTimeMinuteVariance(String fieldName, String fromAmount, String toAmount, String fromConfidence, String toConfidence) {
    this.fieldName = fieldName;
    this.fromAmount = Double.parseDouble(fromAmount);
    this.toAmount = Double.parseDouble(toAmount);
    this.fromConfidence = Double.parseDouble(fromConfidence);
    this.toConfidence = Double.parseDouble(toConfidence);
  }

  @Override
  public double execute(MatchingContext context) {
    double result = 0;
    Object valueLhs = context.getFieldLhs(fieldName);
    Object valueRhs = context.getFieldRhs(fieldName);
    try {
      if (valueLhs != null && valueRhs != null) {
        LocalDateTime dateLhs = Util.dateTime(valueLhs);
        LocalDateTime dateRhs = Util.dateTime(valueRhs);

        double diff = Math.abs(ChronoUnit.MINUTES.between(dateLhs, dateRhs));
        if (diff >= fromAmount && diff <= toAmount) {
          result = fromConfidence + ((toConfidence-fromConfidence) * diff / (toAmount-fromAmount));
        }
      }
    }
    catch (Exception e) {
      logger.warn("Cannot calculate variance as " + fieldName +
              " values are not datetimes (lhs " + valueLhs.toString() + ", rhs " + valueRhs.toString() + ")", e);
    }
    return result;
  }
}

