package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;

public class drTransferLOCAL implements IMatchAssertionFunction {
  @Override
  public boolean execute(MatchInfo matchInfo) {
    String currency = matchInfo.getDrCurrency();

    if (currency != null) {
      return currency.matches(matchInfo.getContext().getLocalCurrencyRegex());
    }
    return false;
  }
}