package za.co.synthesis.rule.support;

import java.lang.reflect.Constructor;

public class StringResolverFactory {
  private static final String functionPackage = "za.co.synthesis.rule.predef.";

  public static IStringResolver createFunction(final String name) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    return (IStringResolver) cls.newInstance();
  }

  public static IStringResolver createFunction(final String name, final String param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class);
    return (IStringResolver)clsConstructor.newInstance(param1);
  }

  public static IStringResolver createFunctionDefault(final String name, final String param1, IStringResolver defaultValue)  {
    try {
      return createFunction(name, param1);
    }
    catch (Exception e) {
      return defaultValue;
    }
  }
}