package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 12:50 PM
 * Determines if the import/export field has been provided and if it is a string does it contain any non-whitespace
 * characters
 */
public class hasImportExportField implements IAssertionFunction {
  private String field;

  public hasImportExportField(String field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getImportExportField(context.getCurrentMoneyInstance(), context.getCurrentImportExportInstance(), field);
    if (fieldValue != null && (!(fieldValue instanceof String) || ((String)fieldValue).trim().length() > 0))
      return true;
    else
      return false;
  }
}
/*
function hasImportExportField(field) {
  return function(context, value) {
    var fieldValue = context.getImportExportField(context.currentMoneyInstance, context.currentImportExportInstance, field);
    if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
      return true;
    else
      return false;
  }
}
*/

