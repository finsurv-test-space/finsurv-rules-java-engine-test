package za.co.synthesis.mapping.configuration;

import za.co.synthesis.rule.core.Scope;

/**
 * User: jake
 * Date: 1/7/16
 * Time: 12:54 PM
 * The mapping scope is a superset of the Scope enumeration that includes Result
 */
public enum MappingScope {
  Transaction
  ,Money
  ,ImportExport
  ,Results
  ;

  public Scope getScope() {
    Scope result = null;
    if (this == Transaction)
      result = Scope.Transaction;
    if (this == Money)
      result = Scope.Money;
    if (this == ImportExport)
      result = Scope.ImportExport;
    return result;
  }
}
