package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 1/15/16
 * Time: 10:57 AM
 * Returns true if there are any spaces in the value
 */
public class hasAdditionalSpaces implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value != null) {
      String s = value.toString();
      return s.contains("  ") || !s.trim().equals(s);
    }
    return false;
  }
}


/*
  _export.hasAdditionalSpaces = function hasAdditionalSpaces(context, value) {
    return value && (typeof value == "string" && value.indexOf(' ') > -1);
  }
*/
