package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 11:07 AM
 * Determines if the current value does not start with the given transaction field value
 */
public class notStartsWithTransactionField implements IAssertionFunction {
  private String field;

  public notStartsWithTransactionField(String field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField(field);
    if (value != null && fieldValue != null) {
      if (value.toString().startsWith(fieldValue.toString()))
        return false;
    }
    return true;
  }
}
/*
function notStartsWithTransactionField(field) {
  return function(context, value) {
    var fieldValue = context.getTransactionField(field);
    if (value && fieldValue) {
      if (value.indexOf(fieldValue) === 0)
        return false;
    }
    return true;
  }
}
*/