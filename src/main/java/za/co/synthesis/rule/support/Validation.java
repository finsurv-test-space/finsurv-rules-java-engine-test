package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.*;

import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 5:12 PM
 * A validation object contains the rule that will be run in the future. This intermediary object allows different
 * messages and codes to be associated with a validation run and also allows multiple validations to be associated
 * with a single rule (array fields)
 */
public class Validation {
  private static final String openMustache = "{{";
  private static final String closeMustache = "}}";

  private final Scope scope;
  private final String field;
  private final RuleType type;
  private final String code;
  private final String message;
  private final String validation;
  private final IAssertionFunction assertion;
  private final List<String> categories;
  private final List<String> notCategories;

  public Validation(Map<String, String> mapping, Scope scope, String field, RuleType type, String code, String message, String validation,
                    IAssertionFunction assertion, List<String> categories, List<String> notCategories) {
    this.scope = scope;
    this.field = DSLSupport.applyMappingToStr(mapping, field);
    this.type = type;
    this.code = code;
    this.message = DSLSupport.applyMappingToStr(mapping, message);
    this.validation = validation;
    this.assertion = assertion;
    this.categories = categories;
    this.notCategories = notCategories;
  }

  private boolean mustRunRule(Scope scope, String field, Object value) {
    return (value == null || !(value instanceof IFinsurvContext.Undefined));
  }

  private ResultType convert(RuleType ruleType) {
    if (ruleType == RuleType.Error)
      return ResultType.Error;
    if (ruleType == RuleType.Warning)
      return ResultType.Warning;
    if (ruleType == RuleType.Deprecated)
      return ResultType.Deprecated;
    if (ruleType == RuleType.Document)
      return ResultType.Document;
    return null;
  }

  private ResultType convert(StatusType statusType) {
    if (statusType == StatusType.Fail)
      return ResultType.Error;
    if (statusType == StatusType.Error)
      return ResultType.Warning;
    return null;
  }

  private boolean categoryNotInList(String category, String mainCategory, List<String> ruleList) {
    boolean result = true;
    for (String ruleItem : ruleList) {
      int pos = ruleItem.indexOf('/');
      if (pos == -1) {
        if (ruleItem.equals(mainCategory)) {
          result = false;
          break;
        }
      } else {
        if (ruleItem.equals(category)) {
          result = false;
          break;
        }
      }
    }
    return result;
  }

  private Object lookupScopedFieldValue(FinsurvContext context, Object value, Scope scope, String field) throws Exception {
    Scope lookupScope = scope;
    String lookupField = field;
    int moneyInstance = context.getCurrentMoneyInstance();
    int ieInstance = context.getCurrentImportExportInstance();
    Object lookupValue = null;
    int posScope = lookupField.indexOf("::");
    if (posScope > -1) {
      String scopeStr = lookupField.substring(0, posScope);
      lookupField = lookupField.substring(posScope + 2);
      if (scopeStr.equals("function")) {
        lookupScope = null;
        lookupValue = CalculationFunctionFactory.createFunction(lookupField).execute(context, value);
      }
      else
      if (scopeStr.equals("transaction"))
        lookupScope = Scope.Transaction;
      else
      if (scopeStr.equals("money"))
        lookupScope = Scope.Money;
      else
      if (scopeStr.equals("importexport"))
        lookupScope = Scope.ImportExport;
    }
    if (lookupScope == Scope.Transaction) {
      lookupValue = context.getTransactionField(lookupField);
    }
    if (lookupScope == Scope.Money) {
      if (moneyInstance == -1) {
        moneyInstance = 0;
      }
      lookupValue = context.getMoneyField(moneyInstance, lookupField);
    }
    if (lookupScope == Scope.ImportExport) {
      if (moneyInstance == -1) {
        moneyInstance = 0;
      }
      if (ieInstance == -1) {
        ieInstance = 0;
      }
      lookupValue = context.getImportExportField(moneyInstance, ieInstance, lookupField);
    }
    return lookupValue;
  }

  private Object[] lookupScopedFieldValues(FinsurvContext context, Object value, Scope scope, String[] fieldList) {
    Object[] result = new Object[fieldList.length];
    for (int i = 0; i < fieldList.length; i++) {
      try {
        result[i] = lookupScopedFieldValue(context, value, scope, fieldList[i]);
        if (result[i] instanceof IFinsurvContext.Undefined){
          result[i] = null;
        }
      } catch (Exception e) {
        result[i] = null;
      }
    }
    return result;
  }

  private CustomValidateResult customCall(FinsurvContext context, String validation, Scope scope, Object value) {
    CustomValidateResult result;

    CustomValidateRegistry.Entry validateEntry = context.getCustomValidateRegistry().get(validation);
    if (validateEntry != null) {
      Object[] inputs = lookupScopedFieldValues(context, value, scope, validateEntry.getInputParams());
      CustomValidateCache.ValidateInputs validateInputs = new CustomValidateCache.ValidateInputs(value, inputs);

      // Look to see if the validation call is cached
      result = context.getCustomValidateCache().getResult(validation, validateInputs);
      if (result == null) {
        result = validateEntry.getCustomValidate().call(value, inputs);

        // Add the latest results from an external validation call to the cache
        if (result instanceof CustomValidateResultWithResponse) {
          context.getCustomValidateCache().cacheResult(validation,
                  validateInputs,
                  result,
                  ((CustomValidateResultWithResponse)result).getResponse(), null);
        }
        else {
          context.getCustomValidateCache().cacheResult(validation,
                  validateInputs,
                  result,
                  null, null);
        }
      }
    }
    else {
      result = new CustomValidateResult(StatusType.Fail, "", "The validation callback " + validation + " has not been registered");
    }
    return result;
  }

  private String composeMessage(FinsurvContext context, Scope scope, Object value, String message) {
    return composeMessage(context, scope, value, message, 0);
  }
  private String composeMessage(FinsurvContext context, Scope scope, Object value, String message, int pos) {
    if (message != null) {
      int openPos = message.indexOf(openMustache, pos);
      if (openPos != -1) {
        int closePos = message.indexOf(closeMustache, openPos);
        if (closePos != -1) {
          String key = message.substring(openPos + openMustache.length(), closePos);
          Object replace = value;
          if (!key.equals("value")) {
            try {
              replace = lookupScopedFieldValue(context, value, scope, key);
            } catch (Exception e) {
              replace = null;
            }
          }
          if (replace != null) {
            String newMessage = message.replace(openMustache + key + closeMustache, replace.toString());
            return composeMessage(context, scope, value, newMessage, openPos);
          } else {
            return composeMessage(context, scope, value, message, closePos);
          }
        }
      }
    }
    return message;
  }

  public boolean run(FinsurvContext context) {
    boolean matchedRule = false;
    boolean runRule;
    Object value;
    int moneySize;
    String moneyCategory;
    String moneyMainCategory;
    int i;

    context.setCurrentScope(scope);

    if (scope == Scope.Transaction) {
      value = context.getTransactionField(field);
      matchedRule = true;
      if (mustRunRule(scope, field, value)) {
        context.setCurrentMoneyInstance(-1);
        context.setCurrentImportExportInstance(-1);
        if (assertion == null || assertion.execute(context, value)) {
          if (type == RuleType.Validate) {
            CustomValidateResult cvRes = customCall(context, validation, scope, value);
            ResultType rt = convert(cvRes.getStatus());
            if (rt != null)
              context.logTransactionEvent(rt, field, cvRes.getCode(), cvRes.getMessage());
          }
          else
            context.logTransactionEvent(convert(type), field, code, composeMessage(context, scope, value, message));
        }

        //Code to perform mandatory detection
        value = null;
        if (assertion == null || assertion.execute(context, value)) {
          if (type == RuleType.Validate) {
            CustomValidateResult cvRes = customCall(context, validation, scope, value);
            ResultType rt = convert(cvRes.getStatus());
            if (rt != null && rt == ResultType.Error) {
              context.logTransactionEvent(ResultType.Mandatory, field, cvRes.getCode(), cvRes.getMessage());
            }
          }
          else {
            if (type == RuleType.Error)
              context.logTransactionEvent(ResultType.Mandatory, field, code, composeMessage(context, scope, value, message));
          }
        }
      }
    } else if (scope == Scope.Money) {
      moneySize = context.getMoneySize();
      for (i = 0; i < moneySize; i++) {
        runRule = true;
        moneyCategory = context.getCategories().get(i);
        moneyMainCategory = context.getMainCategories().get(i);
        if (categories != null && !categories.contains(moneyCategory) && !categories.contains(moneyMainCategory))
          runRule = false;
        if (notCategories != null && !categoryNotInList(moneyCategory, moneyMainCategory, notCategories))
          runRule = false;
        if (runRule) {
          matchedRule = true;
          value = context.getMoneyField(i, field);
          if (mustRunRule(scope, field, value)) {
            context.setCurrentMoneyInstance(i);
            context.setCurrentImportExportInstance(-1);
            if (assertion == null || assertion.execute(context, value)) {
              if (type == RuleType.Validate) {
                CustomValidateResult cvRes = customCall(context, validation, scope, value);
                ResultType rt = convert(cvRes!=null?cvRes.getStatus():StatusType.Error);
                if (rt != null)
                  context.logMoneyEvent(rt, i, field, (cvRes!=null?cvRes.getCode():"NOP"), (cvRes!=null?cvRes.getMessage():"Validation returned no result"));
              }
              else
                context.logMoneyEvent(convert(type), i, field, code, composeMessage(context, scope, value, message));
            }

            //Code to perform mandatory detection
            value = null;
            if (assertion == null || assertion.execute(context, value)) {
              if (type == RuleType.Validate) {
                CustomValidateResult cvRes = customCall(context, validation, scope, value);
                ResultType rt = convert(cvRes!=null?cvRes.getStatus():StatusType.Error);
                if (rt != null && rt == ResultType.Error)
                  context.logMoneyEvent(ResultType.Mandatory, i, field, (cvRes!=null?cvRes.getCode():"NOP"), (cvRes!=null?cvRes.getMessage():"Validation returned no result"));
              }
              else {
                if (type == RuleType.Error)
                  context.logMoneyEvent(ResultType.Mandatory, i, field, code, composeMessage(context, scope, value, message));
              }
            }
          }
        }
      }
    } else if (scope == Scope.ImportExport) {
      moneySize = context.getMoneySize();
      for (i = 0; i < moneySize; i++) {
        runRule = true;
        moneyCategory = context.getCategories().get(i);
        moneyMainCategory = context.getMainCategories().get(i);
        if (categories != null && !categories.contains(moneyCategory) && !categories.contains(moneyMainCategory))
          runRule = false;
        if (notCategories != null && !categoryNotInList(moneyCategory, moneyMainCategory, notCategories))
          runRule = false;

        if (runRule) {
          int importSize = context.getImportExportSize(i);
          for ( int j = 0; j < importSize; j++) {
            value = context.getImportExportField(i, j, field);
            matchedRule = true;
            if (mustRunRule(scope, field, value)) {
              context.setCurrentMoneyInstance(i);
              context.setCurrentImportExportInstance(j);
              if (assertion == null || assertion.execute(context, value)) {
                if (type == RuleType.Validate) {
                  CustomValidateResult cvRes = customCall(context, validation, scope, value);
                  ResultType rt = convert(cvRes!=null?cvRes.getStatus():StatusType.Error);
                  if (rt != null)
                    context.logImportExportEvent(rt, i, j, field, (cvRes!=null?cvRes.getCode():"NOP"), (cvRes!=null?cvRes.getMessage():"Validation returned no result"));
                }
                else
                  context.logImportExportEvent(convert(type), i, j, field, code, composeMessage(context, scope, value, message));
              }

              //Code to perform mandatory detection
              value = null;
              if (assertion == null || assertion.execute(context, value)) {
                if (type == RuleType.Validate) {
                  CustomValidateResult cvRes = customCall(context, validation, scope, value);
                  ResultType rt = convert(cvRes!=null?cvRes.getStatus():StatusType.Error);
                  if (rt != null && rt == ResultType.Error)
                    context.logImportExportEvent(ResultType.Mandatory, i, j, field, (cvRes!=null?cvRes.getCode():"NOP"), (cvRes!=null?cvRes.getMessage():"Validation returned no result"));
                }
                else {
                  if (type == RuleType.Error)
                    context.logImportExportEvent(ResultType.Mandatory, i, j, field, code, composeMessage(context, scope, value, message));
                }
              }
            }
          }
        }
      }
    }
    return matchedRule;
  }
}
