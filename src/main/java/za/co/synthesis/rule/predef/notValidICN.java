package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.Util;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 3:13 PM
 * Determines if the current value is not a valid import control number
 */
public class notValidICN implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !Util.isValidICN(value.toString(), context.getLookups());
  }
}

/*
function notValidICN(context, value) {
    if (!value)
      return true;
    if (value.length != 18)
      return true;
    if (!context.lookups.isValidCustomsOfficeCode(value.substr(0, 3)))
      return true;
    var regDate = /^(19|20)[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])$/;
    if (regDate.test(value.substr(3, 8)) == false)
      return true;
    if (isNaN(new Date(value.substr(3, 4) + '-' + value.substr(7, 2) + '-' + value.substr(9, 2))))
      return true;
    var regNumeric = /^[0-9]*$/;
    if (regNumeric.test(value.substr(11, 7)) == false)
      return true;
    return false;
}
*/