package za.co.synthesis.rule.core;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif

/**
 * User: jake
 * Date: 8/2/14
 * Time: 8:37 PM
 * Provides access to the support data associated with the Finsurv data. Typically this support data includes:
 * 1. Current Date (allows unit tests to define a specific date to be executed for)
 * 2. Custom data provided alongside the core Finsurv data
 */
public interface ISupporting {
  LocalDate getCurrentDate();
  LocalDate getGoLiveDate();
}
