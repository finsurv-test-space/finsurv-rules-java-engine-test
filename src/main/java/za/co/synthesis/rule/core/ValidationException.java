package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 1:20 PM
 * This is thrown by the Validation classes whenever an error is encountered
 */
public class ValidationException extends Exception {
  public ValidationException(String message) {
    super(message);
  }

  public ValidationException(String message, java.lang.Throwable throwable) {
    super(message, throwable);
  }
}