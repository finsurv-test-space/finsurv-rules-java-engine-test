package za.co.synthesis.rule.core;

import za.co.synthesis.rule.support.CustomValidateCache;

public class CustomValidateResultWithResponse extends CustomValidateResult {
    private CustomValidateCache.ResponseCache response;

    public CustomValidateResultWithResponse(StatusType status, String code, String message, CustomValidateCache.ResponseCache response) {
        super(status, code, message);
        this.response = response;
    }

    public CustomValidateResultWithResponse(StatusType status, String code, String message) {
        super(status, code, message);
    }

    public CustomValidateResultWithResponse(StatusType status) {
        super(status);
    }

    public CustomValidateCache.ResponseCache getResponse() {
        return response;
    }
}
