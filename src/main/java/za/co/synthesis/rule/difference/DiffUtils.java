package za.co.synthesis.rule.difference;

import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.ValidationEngine;
import za.co.synthesis.rule.core.ValidationException;
import za.co.synthesis.rule.core.Validator;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.support.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DiffUtils {
  public static boolean areObjectsTheSame(Object obj1, Object obj2) {
    if (obj1 != null) {
      if (!obj1.equals(obj2))
        return false;
    }
    else {
      if (obj2 != null)
        return false;
    }
    return true;
  }

  public static List<RuleSet> loadValidationRulesets(String packagePath) throws ValidationException {
    String packageName = new File(packagePath).getName();
    return ValidationEngine.loadValidationRuleSetsFromPath(packageName, packagePath);
  }

  public static List<RuleSet> loadDocumentRulesets(String packagePath) throws ValidationException {
    String packageName = new File(packagePath).getName();
    return ValidationEngine.loadDocumentRuleSetsFromPath(packageName, packagePath);
  }

  public static List<JSFunctionCall> loadValidationTests(String packagePath) throws Exception {
    List<JSFunctionCall> result = new ArrayList<JSFunctionCall>();
    Path testCases = Paths.get(packagePath,"tests", "testCases.js");
    FileReader fileReader = new FileReader(testCases.toFile());
    JSStructureParser parser = new JSStructureParser(fileReader);
    Object obj = parser.parse();

    // retrieve the testCases
    JSObject testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
    if (testCaseContainer != null) {
      JSArray cases = (JSArray) testCaseContainer.get("test_cases");

      for (Object objCase : cases) {
        if (objCase instanceof JSFunctionCall) {
          result.add((JSFunctionCall) objCase);
        }
      }
    }
    return result;
  }

  public static RuleMap getRuleMapFromRuleSets(List<RuleSet> ruleSets, RuleType... filterRuleTypes) {
    RuleMap ruleMap = new RuleMap();

    for (RuleSet rs : ruleSets) {
      for (FieldValidation fv : rs.getValidations()) {
        for (Rule rule :  fv.getRules()) {
          if (filterRuleTypes == null) {
            ruleMap.add(new FieldRule(rs.getScope(), fv.getFieldList(), rule));
          }
          else {
            // Check to see if the type is in the list
            for (RuleType rt : filterRuleTypes) {
              if (rule.getType().equals(rt)) {
                ruleMap.add(new FieldRule(rs.getScope(), fv.getFieldList(), rule));
                break;
              }
            }
          }
        }
      }
    }
    return ruleMap;
  }
}
