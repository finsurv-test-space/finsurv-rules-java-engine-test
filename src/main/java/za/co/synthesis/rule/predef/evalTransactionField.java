package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * User: jake
 * Date: 12/17/15
 * Time: 12:12 PM
 * Evaluate the given field with an existing function
 */
public class evalTransactionField implements IAssertionFunction {
  private IStringResolver field;
  private IAssertionFunction fEval = null;

  public evalTransactionField(IStringResolver field, IAssertionFunction fEval) {
    this.field = field;
    this.fEval = fEval;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField(field.resolve(context, value));
    if (fieldValue != null && fEval != null) {
      return fEval.execute(context, fieldValue);
    }
    return false;
  }
}

/*
  _export.evalTransactionField = function (field, fEval) {
    return function(context, value) {
      var fieldValue = context.getTransactionField(field);

      if (fieldValue) {
        return fEval(context, fieldValue);
      }
      return false;
    };
  }
*/