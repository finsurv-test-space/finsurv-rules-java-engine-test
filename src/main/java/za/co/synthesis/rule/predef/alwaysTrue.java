package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * Created by jake on 4/10/16.
 */
public class alwaysTrue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return true;
  }
}
