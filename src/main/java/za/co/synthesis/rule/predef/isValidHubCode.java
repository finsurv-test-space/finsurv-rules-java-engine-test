package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 9/6/16.
 */
public class isValidHubCode implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value != null && value instanceof String) {
      String hubCode = (String) value;
      Map<String, String> key = new HashMap<String, String>();
      key.put("hubCode", hubCode);
      String lookupCode = context.getLookups().getLookupField("branches", key, "hubCode");
      if (hubCode.equals(lookupCode))
        return true;
    }
    return false;
  }
}

/*
  _export.isValidHubCode = function (context, value) {
    var hubCode = value;
    if (context.lookups.lookups.branches) {
      var i;
      var hub;
      for (i = 0; i < context.lookups.lookups.branches.length; i++) {
        hub = context.lookups.lookups.branches[i];
        if (hub.hubCode === hubCode) {
          return true;
        }
      }
    }
    return false;
  }
*/