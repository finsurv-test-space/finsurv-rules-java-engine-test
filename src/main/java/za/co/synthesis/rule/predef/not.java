package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 1:32 PM
 * Executes to given function and returns the logical not of the returned boolean value
 */
public class not implements IAssertionFunction {
  private final IAssertionFunction func;

  public not(IAssertionFunction func) {
    this.func = func;
  }

  public boolean execute(FinsurvContext context, Object value) {
    return !func.execute(context, value);
  }
}

/*
function not(func) {
    return function(context, value) {
        return !func(context, value);
    }
}
*/