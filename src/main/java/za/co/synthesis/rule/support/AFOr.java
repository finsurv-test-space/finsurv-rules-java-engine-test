package za.co.synthesis.rule.support;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 8:32 PM
 * The logical OR context function. The constructor takes in a left and a right context function and then logically
 * ands the result of the execute
 */
public class AFOr implements IAssertionFunction {
  private IAssertionFunction left;
  private IAssertionFunction right;

  public AFOr(IAssertionFunction left, IAssertionFunction right) {
    this.left = left;
    this.right = right;
  }

  public boolean execute(FinsurvContext context, Object value) {
    return left.execute(context, value) || right.execute(context, value);
  }
}

