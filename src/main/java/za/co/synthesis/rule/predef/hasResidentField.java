package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 10:16 AM
 * Determines if the resident individual/entity field has been provided and if it is a string does it contain any
 * non-whitespace characters
 */
public class hasResidentField implements IAssertionFunction {
  private String constant;

  public hasResidentField(String constant) {
    this.constant = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField("Resident.Individual." + constant);
    if (fieldValue == null || fieldValue instanceof IFinsurvContext.Undefined) {
      fieldValue = context.getTransactionField("Resident.Entity." + constant);
    }
    if (fieldValue != null && !(fieldValue instanceof IFinsurvContext.Undefined) &&
        (!(fieldValue instanceof String) || ((String)fieldValue).trim().length() > 0))
      return true;
    else
      return false;
  }
}
/*
function hasResidentField(field) {
  return function(context, value) {
    var fieldValue = context.getTransactionField("Resident.Individual." + field);
    if (!fieldValue)
      fieldValue = context.getTransactionField("Resident.Entity." + field);
    if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
      return true;
    else
      return false;
  }
}
*/