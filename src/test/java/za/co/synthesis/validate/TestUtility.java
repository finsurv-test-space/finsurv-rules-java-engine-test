package za.co.synthesis.validate;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.mapping.core.Entity;
import za.co.synthesis.rule.core.FlowType;
import za.co.synthesis.rule.core.ValidationEngine;

import java.util.HashMap;
import java.util.Map;

/**
 * User: jake
 * Date: 1/19/16
 * Time: 12:02 PM
 * Grouping together of common test code and services
 */
public class TestUtility {
  private static Map<String, CCNInfo> ccnInfoMap = new HashMap<String, CCNInfo>();
  private static Map<String, TrnRefInfo> trnInfoMap = new HashMap<String, TrnRefInfo>();
  private static Map<String, LoanInfo> loanInfoMap = new HashMap<String, LoanInfo>();
  private static Map<String, IVSInfo> ivsInfoMap = new HashMap<String, IVSInfo>();


  public static void setInvalidCCN(Object ccnObj) {
    if (ccnObj != null) {
      String ccn = ccnObj.toString();
      if (ccnInfoMap.containsKey(ccn)) {
        CCNInfo ccnInfo = ccnInfoMap.get(ccn);
        ccnInfo.setValidCCN(false);
      }
      else {
        CCNInfo ccnInfo = new CCNInfo();
        ccnInfo.setValidCCN(false);
        ccnInfo.setImportUndertaking(true);
        ccnInfoMap.put(ccn, ccnInfo);
      }
    }
  }

  public static void setNotLUClientCCN(Object ccnObj) {
    if (ccnObj != null) {
      String ccn = ccnObj.toString();
      if (ccnInfoMap.containsKey(ccn)) {
        CCNInfo ccnInfo = ccnInfoMap.get(ccn);
        ccnInfo.setImportUndertaking(false);
      }
      else {
        CCNInfo ccnInfo = new CCNInfo();
        ccnInfo.setImportUndertaking(false);
        ccnInfo.setValidCCN(true);
        ccnInfoMap.put(ccn, ccnInfo);
      }
    }
  }

  public static void setLUClientCCN(Object ccnObj) {
    if (ccnObj != null) {
      String ccn = ccnObj.toString();
      if (ccnInfoMap.containsKey(ccn)) {
        CCNInfo ccnInfo = ccnInfoMap.get(ccn);
        ccnInfo.setImportUndertaking(true);
      }
      else {
        CCNInfo ccnInfo = new CCNInfo();
        ccnInfo.setImportUndertaking(true);
        ccnInfo.setValidCCN(true);
        ccnInfoMap.put(ccn, ccnInfo);
      }
    }
  }

  public static void setTrnRef(Object trnRefObj, FlowType flowType, String category) {
    if (trnRefObj != null) {
      String trnRef = trnRefObj.toString();
      if (trnInfoMap.containsKey(trnRef)) {
        TrnRefInfo trnInfo = trnInfoMap.get(trnRef);
        trnInfo.setExists(true);
        trnInfo.setCancelled(false);
        trnInfo.setCategory(category);
        trnInfo.setFlow(flowType.toString());
      }
      else {
        TrnRefInfo trnInfo = new TrnRefInfo();
        trnInfo.setExists(true);
        trnInfo.setCancelled(false);
        trnInfo.setFlow(flowType.toString());
        trnInfo.setCategory(category);
        trnInfoMap.put(trnRef, trnInfo);
      }
    }
  }


  public static void setCancelledTrnRef(Object trnRefObj, FlowType flowType) {
    if (trnRefObj != null) {
      String trnRef = trnRefObj.toString();
      if (trnInfoMap.containsKey(trnRef)) {
        TrnRefInfo trnInfo = trnInfoMap.get(trnRef);
        trnInfo.setExists(true);
        trnInfo.setCancelled(true);
        trnInfo.setFlow(flowType.toString());
      }
      else {
        TrnRefInfo trnInfo = new TrnRefInfo();
        trnInfo.setExists(true);
        trnInfo.setCancelled(true);
        trnInfo.setFlow(flowType.toString());
        trnInfoMap.put(trnRef, trnInfo);
      }
    }
  }

  public static void setTrnRefNotInDB(Object trnRefObj) {
    if (trnRefObj != null) {
      String trnRef = trnRefObj.toString();
      if (trnInfoMap.containsKey(trnRef)) {
        TrnRefInfo trnInfo = trnInfoMap.get(trnRef);
        trnInfo.setExists(false);
        trnInfo.setCancelled(true);
      }
      else {
        TrnRefInfo trnInfo = new TrnRefInfo();
        trnInfo.setExists(false);
        trnInfo.setCancelled(true);
        trnInfoMap.put(trnRef, trnInfo);
      }
    }
  }

  public static void setLoanRef(Object loanRefObj) {
    if (loanRefObj != null) {
      String loanRef = loanRefObj.toString();
      if (loanInfoMap.containsKey(loanRef)) {
        LoanInfo loanInfo = loanInfoMap.get(loanRef);
        loanInfo.setValidLoanRef(true);
      }
      else {
        LoanInfo loanInfo = new LoanInfo();
        loanInfo.setValidLoanRef(true);
        loanInfoMap.put(loanRef, loanInfo);
      }
    }
  }

  public static void setInvalidLoanRef(Object loanRefObj) {
    if (loanRefObj != null) {
      String loanRef = loanRefObj.toString();
      if (loanInfoMap.containsKey(loanRef)) {
        LoanInfo loanInfo = loanInfoMap.get(loanRef);
        loanInfo.setValidLoanRef(false);
      }
      else {
        LoanInfo loanInfo = new LoanInfo();
        loanInfo.setValidLoanRef(false);
        loanInfoMap.put(loanRef, loanInfo);
      }
    }
  }

  public static void removeMRNfromIVS(Object importControlNo) {
    if (importControlNo != null) {
      String icn = importControlNo.toString();
      if (ivsInfoMap.containsKey(icn)) {
        IVSInfo ivsInfo = ivsInfoMap.get(icn);
        ivsInfo.setMRN(icn);
        ivsInfo.setOnIVS(false);
      }
      else {
        IVSInfo ivsInfo = new IVSInfo();
        ivsInfo.setMRN(icn);
        ivsInfo.setOnIVS(false);
        ivsInfoMap.put(icn, ivsInfo);
      }
    }
  }

  public static void addMRNtoIVS(Object importControlNo) {
    if (importControlNo != null) {
      String icn = importControlNo.toString();
      if (ivsInfoMap.containsKey(icn)) {
        IVSInfo ivsInfo = ivsInfoMap.get(icn);
        ivsInfo.setMRN(icn);
        ivsInfo.setOnIVS(true);
      }
      else {
        IVSInfo ivsInfo = new IVSInfo();
        ivsInfo.setMRN(icn);
        ivsInfo.setOnIVS(true);
        ivsInfoMap.put(icn, ivsInfo);
      }
    }
  }

  public static String getUCR(JSObject tran, int money_index, int ie_index) {
    JSArray money = (JSArray)tran.get("MonetaryAmount");
    if (money != null && money.size() >= money_index) {
      JSArray ie = (JSArray)((JSObject)money.get(money_index-1)).get("ImportExport");
      if (ie != null && ie.size() >= ie_index) {
        Object UCR = ((JSObject)ie.get(ie_index-1)).get("UCR");
        if (UCR != null) {
          return UCR.toString();
        }
      }
    }
    return null;
  }

  public static String getCategory(JSObject tran, int money_index) {
    JSArray money = (JSArray)tran.get("MonetaryAmount");
    if (money_index > 0 && money != null && money.size() >= money_index) {
      Object Category = ((JSObject)money.get(money_index-1)).get("CategoryCode");
      Object SubCategory = ((JSObject)money.get(money_index-1)).get("CategorySubCode");
      String result = null;
      if (Category != null)
        result = Category.toString();
      if (result != null && SubCategory != null)
        result += "/" + SubCategory.toString();
      return result;
    }
    return null;
  }

  public static Object getLoanRef(JSObject tran, int money_index) {
    JSArray money = (JSArray)tran.get("MonetaryAmount");
    if (money_index > 0 && money != null && money.size() >= money_index) {
      return ((JSObject)money.get(money_index-1)).get("LoanRefNumber");
    }
    return null;
  }

  public static Object getImportControlNumber(JSObject tran, int money_index, int ie_index) {
    JSArray money = (JSArray)tran.get("MonetaryAmount");
    if (money_index > 0 && money != null && money.size() >= money_index) {
      JSArray ie = (JSArray)((JSObject)money.get(money_index-1)).get("ImportExport");
      if (ie_index > 0 && ie != null && ie.size() >= ie_index) {
        return ((JSObject)ie.get(ie_index-1)).get("ImportControlNumber");
      }
    }
    return null;
  }

  public static void printUCR(JSObject tran, Object ccnObj, String errorCode, String errorDesc) {
    JSArray money = (JSArray)tran.get("MonetaryAmount");
    if (money != null && money.size() == 1) {
      JSArray ie = (JSArray)((JSObject)money.get(0)).get("ImportExport");
      if (ie != null && ie.size() == 1) {
        Object UCR = ((JSObject)ie.get(0)).get("UCR");
        if (UCR != null) {
          JSObject tp = (JSObject)((JSObject)money.get(0)).get("ThirdParty");
          if (tp != null) {
            Object tpCCN = tp.get("CustomsClientNumber");
            System.out.print(errorCode + " - UCR: " + UCR.toString() + "; TP CCN: " + (tpCCN != null ? tpCCN.toString() : "-") + "; Res CCN: " + (ccnObj != null ? ccnObj.toString() : "-"));
          }
          else {
            System.out.print(errorCode + " - UCR: " + UCR.toString() + "; Res CCN: " + (ccnObj != null ? ccnObj.toString() : "-"));
          }
          System.out.println(" // " + errorDesc);
        }
      }
    }
  }

  public static void registerTestFunctions(ValidationEngine engine) {
    engine.registerCustomValidate("Validate_ImportUndertakingCCN", new Validate_ImportUndertakingCCN(ccnInfoMap));
    engine.registerCustomValidate("Validate_ValidCCN", new Validate_ValidCCN(ccnInfoMap));
    engine.registerCustomValidate("Validate_ValidCCNinUCR", new Validate_ValidCCNinUCR(ccnInfoMap));
    engine.registerCustomValidate("Validate_ReversalTrnRef", new Validate_ReversalTrnRef(trnInfoMap), "transaction::Flow", "ReversalTrnSeqNumber");
    engine.registerCustomValidate("Validate_ReplacementTrnReference", new Validate_ReplacementTrnReference(trnInfoMap),
            "TrnReference", "ReplacementTransaction", "ReplacementOriginalReference");
    engine.registerCustomValidate("Validate_IndividualSDA", new Validate_IndividualSDA(),
            "transaction::TrnReference", "transaction::ValueDate", "function::localAmountForIDNumber");

    engine.registerCustomValidate("Validate_LoanRef", new Validate_LoanRef(loanInfoMap));
    engine.registerCustomValidate("Validate_MRNonIVS", new Validate_MRNonIVS(ivsInfoMap), "ImportControlNumber");
  }

  public static void clearTestInfo(){
    ccnInfoMap.clear();
    trnInfoMap.clear();
    loanInfoMap.clear();
    ivsInfoMap.clear();


  }

  public static void populateTestInfo(Entity tran, String errorCode, String errorDesc, int money_index, int ie_index, Object ccnObj) {
    //"212","TRANSACTION REFERENCE/SEQUENCE OF CANCELLED TRANSACTION NOT STORED ON SARB DATABASE"
    if (errorDesc.equals("TRANSACTION REFERENCE/SEQUENCE OF CANCELLED TRANSACTION NOT STORED ON SARB DATABASE.")) {
      Object trnRefObj = tran.getJsValue("ReplacementOriginalReference");
      if (trnRefObj != null)
        setTrnRefNotInDB(trnRefObj);
    }
    //"322","INVALID CUSTOMS CLIENT NUMBER"
    if (errorDesc.equals("INVALID CUSTOMS CLIENT NUMBER")) {
      setInvalidCCN(ccnObj);
    }
    //"514","INVALID CCN COMPLETED IN UCR"
    if (errorDesc.equals("INVALID CCN COMPLETED IN UCR")) {
      String UCR = getUCR(tran.getJsObject(), money_index, ie_index);
      if ((UCR != null) && (UCR.length() > 11)){
        setInvalidCCN(UCR.substring(3, 11));
      }
    }
    //"323","NOT A REGISTERED IMPORT UNDERTAKING ENTITY"
    if (errorDesc.equals("NOT A REGISTERED IMPORT UNDERTAKING ENTITY")) {
      setNotLUClientCCN(ccnObj);
    }
    //"503","TRANSPORTDOCUMENTNUMBER MAY NOT BE COMPLETED" on category code is 105 or 106 (can infer that LUClient)
    if (errorCode.equals("503")) {
      String category = getCategory(tran.getJsObject(), money_index);
      if (category.equals("105") || category.equals("106"))
        setLUClientCCN(ccnObj);
    }
    //"389","TRANSACTION REFERENCE NUMBER EQUALS A CANCELLED TRANSATION REFERENCE NUMBER.  INCORRECT REPLACEMENTTRANSACTION INDICATOR USED."
    if (errorCode.equals("389")) {
      Object trnRefObj = tran.getJsValue("TrnReference");
      if (trnRefObj != null)
        setCancelledTrnRef(trnRefObj.toString(), FlowType.Inflow);
    }
    //"368", "INVALID BOPCATEGORY" and category is LUClient specific
    if (errorCode.equals("368") && errorDesc.equals("INVALID BOPCATEGORY")) {
      String flow = tran.getJsObject().get("Flow").toString();
      String category = getCategory(tran.getJsObject(), money_index);
      if (flow.equals("OUT") && category != null) {
        if (category.contains("104")) { // Outflow 104 is for clients "in terms of import undertaking"
          setNotLUClientCCN(ccnObj);
        }
        else
        if (category.contains("103")) { // Outflow 103 is for clients "not in terms of import undertaking"
          setLUClientCCN(ccnObj);
        }
      }
    }
    //"374", "INVALID LOAN REFERENCE NUMBER"
    if (errorCode.equals("374")) {
      Object loanRef = getLoanRef(tran.getJsObject(), money_index);
      if (loanRef != null) {
        setInvalidLoanRef(loanRef);
      }
    }
    //"221", "NOMRNONIVS MUST HAVE A VALUE Y"  OR "500", "MRN NOT STORED ON IVS"
    if (errorCode.equals("221") || errorCode.equals("500")) {
      Object icn = getImportControlNumber(tran.getJsObject(), money_index, ie_index);
      if (icn != null) {
        removeMRNfromIVS(icn);
      }
    }
    //"232", NOMRNONIVS MAY NOT BE COMPLETED SINCE THE MRN EXISTS ON THE IVS DATABASE
    if (errorCode.equals("232")) {
      Object icn = getImportControlNumber(tran.getJsObject(), money_index, ie_index);
      if (icn != null) {
        addMRNtoIVS(icn);
      }
    }
  }

  public static void populateCustomData(Object ccnObj, JSObject customData) {
    if (ccnObj != null) {
      CCNInfo ccnInfo = ccnInfoMap.get(ccnObj.toString());
      if (ccnInfo != null) {
        if (ccnInfo.isImportUndertaking())
          customData.put("LUClient", "Y");
        if (!ccnInfo.isImportUndertaking())
          customData.put("LUClient", "N");
      }
    }
  }
}
