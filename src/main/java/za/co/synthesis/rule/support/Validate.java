package za.co.synthesis.rule.support;

/**
 * User: jake
 * Date: 2/25/15
 * Time: 4:41 PM
 * Validate rule used to specify a new rule that leverages a registered custom validation function to implement
 * the validation
 */
public class Validate extends Rule {
  public Validate(String name, String validation, IAssertionFunction assertion, String assertionDSL, String ruleDSL) {
    super(RuleType.Validate, name, "XXX", "not used", validation, assertion, assertionDSL, ruleDSL);
  }
}

