package za.co.synthesis;

import com.opencsv.CSVReader;
import za.co.synthesis.rule.support.Util;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * User: jake
 * Date: 2/10/16
 * Time: 3:02 PM
 * Runs through the list of CCN Test cases
 */
public class CcnTest {
  private static CSVReader reader;
  private static String[] headers;

  public static void main( String[] args ) throws Exception {
    PrintWriter filePrinter = new PrintWriter(new BufferedWriter(new FileWriter("./src/test/resources/CCNTestDataOut.csv", false)));
    filePrinter.println("CCN, ExpectedPassFail");

    reader = new CSVReader(new FileReader("./src/test/resources/CCNTestData.csv"));
    headers = reader.readNext();

    String[] data;
    String ccn;
    int total = 0;
    int needsPadding = 0;
    int inError = 0;

    while((data = reader.readNext()) != null) {
      if (data.length > 0) {
        ccn = data[0].trim();
        if (ccn.length() > 0) {
          total++;
          boolean isValid = Util.isValidCCN(ccn);

          filePrinter.println(ccn + "," + (isValid ? "1" : "0"));

          if (!isValid) {
            inError++;
            if (ccn.length() != 8) {
              String paddedCcn = ccn;
              while (paddedCcn.length() < 8) {
                paddedCcn = "0" + paddedCcn;
              }
              boolean isPaddedValid = Util.isValidCCN(paddedCcn);
              if (isPaddedValid) {
                needsPadding++;
                inError--;
                System.out.println(ccn + " - wrong length (but if padded to " + paddedCcn + " then correct)");
              }
              else
                System.out.println(ccn + " - wrong length and failed checksum");
            }
            else
              System.out.println(ccn + " - failed checksum");
          }
        }
      }
    }
    System.out.println("Total: " + total + ", in Error: " + inError + ", needs padding: " + needsPadding);
  }

}
