package za.co.synthesis.javascript;

import java.util.List;

public interface IJSSnippetSupport {
  void setPreceding(JSSnippetSupport preceding, int precedingIndex);
  void addSnippet(JSSnippet snippet);
  int currentSnippetIndex();
  void composeSnippets(JSWriter writer);
  void augmentEndSnippet(String extraText);
  void removeExtraWhitespace();
  void injectWhitespace(Object parentObject);
}
