package za.co.synthesis.rule.support.legacydate;

import za.co.synthesis.rule.support.legacydate.format.DateTimeFormatter;
import za.co.synthesis.rule.support.legacydate.format.DateTimeParseException;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class LocalDateTime extends Temporal{
  public LocalDateTime(Date date) {
    this.calendar = new GregorianCalendar();
    this.calendar.setTime(date);
  }

  public LocalDateTime(Calendar calendar) {
    this.calendar = calendar;
  }

  public LocalDateTime() {
    this.calendar = GregorianCalendar.getInstance();
  }

  public static LocalDateTime now() {
    return new LocalDateTime();
  }

  public synchronized static LocalDateTime parse(String value, DateTimeFormatter format) throws DateTimeParseException {
    try {
      return format.parse(value);
    } catch (ParseException e) {
      throw new DateTimeParseException(e.getMessage(), e);
    }
  }
}
