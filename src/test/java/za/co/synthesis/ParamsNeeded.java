package za.co.synthesis;

import za.co.synthesis.rule.core.*;

import java.util.ArrayList;
import java.util.List;

public class ParamsNeeded {

  private static class TransactionReportableType {
    private List<BankAccountType> fromAccTypes;
    private List<BankAccountType> toAccTypes;
    private ReportableType repType;

    private TransactionReportableType(BankAccountType fromAccType, List<BankAccountType> toAccTypes, ReportableType repType) {
      this.fromAccTypes = new ArrayList<BankAccountType>();
      this.fromAccTypes.add(fromAccType);
      this.toAccTypes = toAccTypes;
      this.repType = repType;
    }

    private TransactionReportableType(List<BankAccountType> fromAccTypes, BankAccountType toAccType, ReportableType repType) {
      this.fromAccTypes = fromAccTypes;
      this.toAccTypes = new ArrayList<BankAccountType>();
      this.toAccTypes.add(toAccType);
      this.repType = repType;
    }

    private List<BankAccountType> getFromAccTypes() {
      return fromAccTypes;
    }

    private List<BankAccountType> getToAccTypes() {
      return toAccTypes;
    }

    private ReportableType getRepType() {
      return repType;
    }
  }

  private static EvaluationEngine createEvaluationEngine() throws Exception {
    EvaluationEngine engine = new EvaluationEngine();

    engine.setupLoadRuleFile("../finsurv-rules/evalRules.js");

    return engine;
  }

  private static boolean crAccTypeParamNeeded(Evaluator evaluator, ResidenceStatus drResStatus, ResidenceStatus crResStatus,
                                              List<TransactionReportableType> transactionReportableTypes) {
    boolean result = false;
    BankAccountType[] accountTypes = {BankAccountType.CASH_CURR, BankAccountType.CASH_LOCAL, BankAccountType.CFC, BankAccountType.FCA, BankAccountType.NOSTRO, BankAccountType.LOCAL_ACC, BankAccountType.VOSTRO};

    for (BankAccountType bat : accountTypes) {
      List<IEvaluationDecision> evalRes = evaluator.evaluate(
              drResStatus, bat,
              crResStatus, BankAccountType.Unknown);
      List<BankAccountType> initBatList = null;
      for (IEvaluationDecision dec : evalRes) {
        transactionReportableTypes.add(new TransactionReportableType(bat, dec.getPossibleCrAccountTypes(), dec.getReportable()));

        if (initBatList == null) {
          initBatList = dec.getPossibleCrAccountTypes();
        }
        else {
          for (BankAccountType crBat : dec.getPossibleCrAccountTypes()) {
            if (!initBatList.contains(crBat)) {
              result = true;
            }
          }
        }
      }
    }

    return result;
  }

  private static boolean drAccTypeParamNeeded(Evaluator evaluator, ResidenceStatus drResStatus, ResidenceStatus crResStatus,
                                              List<TransactionReportableType> transactionReportableTypes) {
    boolean result = false;
    BankAccountType[] accountTypes = {BankAccountType.CASH_CURR, BankAccountType.CASH_LOCAL, BankAccountType.CFC, BankAccountType.FCA, BankAccountType.NOSTRO, BankAccountType.LOCAL_ACC, BankAccountType.VOSTRO};

    for (BankAccountType bat : accountTypes) {
      List<IEvaluationDecision> evalRes = evaluator.evaluate(
              drResStatus, BankAccountType.Unknown,
              crResStatus, bat);
      List<BankAccountType> initBatList = null;
      for (IEvaluationDecision dec : evalRes) {
        transactionReportableTypes.add(new TransactionReportableType(dec.getPossibleDrAccountTypes(), bat, dec.getReportable()));

        if (initBatList == null) {
          initBatList = dec.getPossibleDrAccountTypes();
        }
        else {
          for (BankAccountType drBat : dec.getPossibleDrAccountTypes()) {
            if (!initBatList.contains(drBat)) {
              result = true;
            }
          }
        }
      }
    }

    return result;
  }


  private static String composeTransactionReportableType(TransactionReportableType tran) {
    String result = "";
    if (tran.getFromAccTypes().size() == 0) {
      result += "*";
    }
    else
    if (tran.getFromAccTypes().size() == 1) {
      result += tran.getFromAccTypes().get(0).name();
    }
    else {
      boolean bFirst = true;
      for (BankAccountType bat : tran.getFromAccTypes()) {
        if (bFirst)
          bFirst = false;
        else
          result += "|";
        result += bat.name();
      }
    }

    result += "->";

    if (tran.getToAccTypes().size() == 0) {
      result += "*";
    }
    else
    if (tran.getToAccTypes().size() == 1) {
      result += tran.getToAccTypes().get(0).name();
    }
    else {
      boolean bFirst = true;
      for (BankAccountType bat : tran.getToAccTypes()) {
        if (bFirst)
          bFirst = false;
        else
          result += "|";
        result += bat.name();
      }
    }

    return result;
  }

  private static String composeAccountTypes(List<TransactionReportableType> transactionReportableTypes) {
    String result = "";
    boolean bFound = false;
    for (TransactionReportableType tran : transactionReportableTypes) {
      if (tran.getRepType() == ReportableType.Reportable) {
        if (!bFound) {
          bFound = true;
          result += "\n  Reportable: ";
        }
        else {
          result += ", ";
        }
        result += composeTransactionReportableType(tran);
      }
    }
    bFound = false;
    for (TransactionReportableType tran : transactionReportableTypes) {
      if (tran.getRepType() == ReportableType.NotReportable) {
        if (!bFound) {
          bFound = true;
          result += "\n  Not Reportable: ";
        }
        else {
          result += ", ";
        }
        result += composeTransactionReportableType(tran);
      }
    }
    bFound = false;
    for (TransactionReportableType tran : transactionReportableTypes) {
      if (tran.getRepType() == ReportableType.ZZ1Reportable) {
        if (!bFound) {
          bFound = true;
          result += "\n  ZZ1 Reportable: ";
        }
        else {
          result += ", ";
        }
        result += composeTransactionReportableType(tran);
      }
    }
    bFound = false;
    for (TransactionReportableType tran : transactionReportableTypes) {
      if (tran.getRepType() == ReportableType.Unknown) {
        if (!bFound) {
          bFound = true;
          result += "\n  No Rule Defined: ";
        }
        else {
          result += ", ";
        }
        result += composeTransactionReportableType(tran);
      }
    }
    return result;
  }

  public static void main( String[] args ) throws Exception {
    EvaluationEngine engine = createEvaluationEngine();
    Evaluator evaluator = engine.issueEvaluator();

    boolean res;

    List<TransactionReportableType> transactionReportableType = new ArrayList<TransactionReportableType>();
    System.out.println("Is There a need For Cr Account Type?");
    System.out.println("====================================");

    System.out.print("1. Dr Resident -> Cr Non Resident: ");
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.Resident, ResidenceStatus.NonResident, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("2. Dr Resident -> Cr Resident: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.Resident, ResidenceStatus.Resident, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("3. Dr Non Resident -> Cr Resident: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.NonResident, ResidenceStatus.Resident, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("4. Dr Non Resident -> Cr Non Resident: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.NonResident, ResidenceStatus.NonResident, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("5. Dr HOLDCO -> Cr Non Resident: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.HOLDCO, ResidenceStatus.NonResident, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("6. Dr HOLDCO -> Cr Resident: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.HOLDCO, ResidenceStatus.Resident, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("7. Dr Non Resident -> Cr HOLDCO: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.NonResident, ResidenceStatus.HOLDCO, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("8. Dr Resident -> Cr HOLDCO: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.Resident, ResidenceStatus.HOLDCO, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("9. Dr IHQ -> Cr Non Resident: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.IHQ, ResidenceStatus.NonResident, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("10. Dr IHQ -> Cr Resident: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.IHQ, ResidenceStatus.Resident, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("11. Dr Non Resident -> Cr IHQ: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.NonResident, ResidenceStatus.IHQ, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("12. Dr Resident -> Cr IHQ: ");
    transactionReportableType.clear();
    res = crAccTypeParamNeeded(evaluator, ResidenceStatus.Resident, ResidenceStatus.IHQ, transactionReportableType);
    if (res)
      System.out.println("Must provide Cr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Cr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");


    System.out.println();
    System.out.println();
    System.out.println("Is There a need For Dr Account Type?");
    System.out.println("====================================");

    System.out.println();
    System.out.print("1. Dr Resident -> Cr Non Resident: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.Resident, ResidenceStatus.NonResident, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("2. Dr Resident -> Cr Resident: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.Resident, ResidenceStatus.Resident, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("3. Dr Non Resident -> Cr Resident: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.NonResident, ResidenceStatus.Resident, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("4. Dr Non Resident -> Cr Non Resident: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.NonResident, ResidenceStatus.NonResident, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("5. Dr HOLDCO -> Cr Non Resident: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.HOLDCO, ResidenceStatus.NonResident, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("6. Dr HOLDCO -> Cr Resident: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.HOLDCO, ResidenceStatus.Resident, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("7. Dr Non Resident -> Cr HOLDCO: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.NonResident, ResidenceStatus.HOLDCO, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("8. Dr Resident -> Cr HOLDCO: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.Resident, ResidenceStatus.HOLDCO, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("9. Dr IHQ -> Cr Non Resident: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.IHQ, ResidenceStatus.NonResident, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("10. Dr IHQ -> Cr Resident: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.IHQ, ResidenceStatus.Resident, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("11. Dr Non Resident -> Cr IHQ: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.NonResident, ResidenceStatus.IHQ, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");

    System.out.println();
    System.out.print("12. Dr Resident -> Cr IHQ: ");
    transactionReportableType.clear();
    res = drAccTypeParamNeeded(evaluator, ResidenceStatus.Resident, ResidenceStatus.IHQ, transactionReportableType);
    if (res)
      System.out.println("Must provide Dr Account Type" + composeAccountTypes(transactionReportableType) + ".");
    else
      System.out.println("Dr Account Type need NOT be provided (set to Unknown)" + composeAccountTypes(transactionReportableType) + ".");
  }
}
