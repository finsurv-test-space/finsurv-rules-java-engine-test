package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;
import java.math.BigInteger;

public class notValidNumber implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return !Util.isNumeric(value);
  }
}