package za.co.synthesis.rule.core.impl;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.IFinsurvContextCache;
import za.co.synthesis.rule.core.StatusType;
import za.co.synthesis.rule.support.CentralCustomValidationCache.ValidationCacheEntry;
import za.co.synthesis.rule.support.CustomValidateCache;
import za.co.synthesis.rule.support.Util;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
#else*/
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
//#endif
import java.util.ArrayList;
import java.util.List;

/*
 The following is the format to be used by this component to store validations
 "ValidationCache": [
   {
     "Name": "",
     "Expiry": "",
     "Inputs": {
       "Value": "",
       "Other": [ "", "", "" ]
     },
     "Result": {
       "Status": "", //Pass, Fail, Error
       "Code": "",
       "Message": ""
     },
     "Response": {...}
   }
 ]
 */
public class JSFinsurvContextCache implements IFinsurvContextCache {
    private final JSObject jsCache;

    public JSFinsurvContextCache(JSObject cache) {
        this.jsCache = cache;
    }

    public JSObject getJsCache() {
        return jsCache;
    }

    @Override
    public void clearCache() {
        jsCache.remove("ValidationCache");
    }

    private String objToString(Object value) {
        if (value != null) {
            if (value instanceof String)
                return (String) value;
            if (value instanceof Boolean)
                return ((Boolean) value) ? "true" : "false";
            return value.toString();
        }
        return null;
    }

    private String statusToString(StatusType value) {
        if (value != null) {
            if (value.equals(StatusType.Pass))
                return "Pass";
            if (value.equals(StatusType.Error))
                return "Error";
            if (value.equals(StatusType.Fail))
                return "Fail";
        }
        return null;
    }

    @Override
    public void marshalValidate(ValidationCacheEntry entry) {
        Object objCache = jsCache.get("ValidationCache");

        if (objCache == null || !(objCache instanceof JSArray)) {
            objCache = new JSArray();
            jsCache.put("ValidationCache", objCache);
        }

        JSArray arrayCache = (JSArray)objCache;

        JSObject jsVal = new JSObject();
        jsVal.put("Name", entry.getValidateName());

        if (entry.getExpiryMillis() != null) {
            /*#if OLDDATE
            LocalDateTime ldt = new LocalDateTime(new Date(entry.getExpiryMillis()));
            jsVal.put("Expiry", Util.dateTimeToString(ldt));
            #else*/
            Instant instant = Instant.ofEpochMilli(entry.getExpiryMillis());
            LocalDateTime ldt = instant.atOffset(ZoneOffset.UTC).toLocalDateTime();
            jsVal.put("Expiry", Util.dateTimeToString(ldt));
            //#endif
        }

        JSObject jsInputs = new JSObject();
        jsInputs.put("Value", objToString(entry.getValidateInputs().getValue()));
        if (entry.getValidateInputs().getOtherInputs() != null) {
            JSArray jsOtherInputs = new JSArray();
            jsInputs.put("Other", jsOtherInputs);
            for (Object objInput : entry.getValidateInputs().getOtherInputs()) {
                jsOtherInputs.add(objToString(objInput));
            }
        }
        jsVal.put("Inputs", jsInputs);

        JSObject jsResult = new JSObject();
        jsResult.put("Status", statusToString(entry.getResult().getStatus()));
        if (entry.getResult().getCode() != null) {
            jsResult.put("Code", entry.getResult().getCode());
        }
        if (entry.getResult().getMessage() != null) {
            jsResult.put("Message", entry.getResult().getMessage());
        }
        jsVal.put("Result", jsResult);

        if (entry.getResponseCache() != null) {
            JSObject jsResponse = new JSObject();
            jsResponse.putAll(entry.getResponseCache());
            jsVal.put("Response", jsResult);
        }

        arrayCache.add(jsVal);
    }

    private CustomValidateCache.ValidateInputs readInputs(Object objInputs) {
        if (objInputs instanceof JSObject) {
            Object value;
            Object[] otherInputs = null;

            JSObject jsInputs = (JSObject)objInputs;
            value = jsInputs.get("Value");
            Object objOther = jsInputs.get("Other");
            if (objOther instanceof JSArray) {
                JSArray arrayInputs = (JSArray) objOther;
                otherInputs = new Object[arrayInputs.size()];
                int i = 0;
                for (Object objInput : arrayInputs) {
                    otherInputs[i++] = objInput;
                }
            }
            return new CustomValidateCache.ValidateInputs(value, otherInputs);
        }
        return null;
    }

    private CustomValidateResult readResult(Object objResult) {
        if (objResult instanceof JSObject) {
            JSObject jsResult = (JSObject)objResult;

            StatusType status = null;
            String code;
            String message;

            Object objStatus = jsResult.get("Status");
            if (objStatus != null) {
                if (objStatus.equals("Pass")) {
                    status = StatusType.Pass;
                }
                else
                if (objStatus.equals("Error")) {
                    status = StatusType.Error;
                }
                else
                if (objStatus.equals("Fail")) {
                    status = StatusType.Fail;
                }
            }
            code = (String)jsResult.get("Code");
            message = (String)jsResult.get("Message");

            if (status != null) {
                return new CustomValidateResult(status, code, message);
            }
        }
        return null;
    }

    private CustomValidateCache.ResponseCache readResponseCache(Object objResponse) {
        if (objResponse instanceof JSObject) {
            JSObject jsResponse = (JSObject)objResponse;

            CustomValidateCache.ResponseCache value = new CustomValidateCache.ResponseCache();
            value.putAll(jsResponse);
            return value;
        }
        return null;
    }

    @Override
    public List<ValidationCacheEntry> unmarshalCache() {
        List<ValidationCacheEntry> result = new ArrayList<ValidationCacheEntry>();
        Object obj = jsCache.get("ValidationCache");
        if (obj instanceof JSArray) {
            JSArray array = (JSArray) obj;

            for (Object objVal : array) {
                if (objVal instanceof JSObject) {
                    JSObject jsVal = (JSObject)objVal;
                    ValidationCacheEntry entry = new ValidationCacheEntry();
                    entry.setValidateName((String)jsVal.get("Name"));
                    Object objExpiry = jsVal.get("Expiry");
                    if (objExpiry != null) {
                        LocalDateTime ldt = Util.dateTime(objExpiry);
                        /*#if OLDDATE
                        entry.setExpiryMillis(ldt.getCalendar().getTimeInMillis());
                        #else*/
                        entry.setExpiryMillis(ldt.toInstant(ZoneOffset.UTC).toEpochMilli());
                        //#endif
                    }
                    entry.setValidateInputs(readInputs(jsVal.get("Inputs")));
                    entry.setResult(readResult(jsVal.get("Result")));
                    entry.setResponseCache(readResponseCache(jsVal.get("Response")));

                    if (entry.getValidateName() != null && entry.getValidateInputs() != null && entry.getResult() != null) {
                        result.add(entry);
                    }
                }
            }
        }
        return result;
    }
}
