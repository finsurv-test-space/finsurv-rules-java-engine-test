package za.co.synthesis.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:21 PM
 * A Test implementation of the Validate_ReversalTrnRef function. Needs to raise the following SARB errorS:
 * 410, "Original transaction and SequenceNumber combination not stored on database"
 * 410, "Original transaction and SequenceNumber combination stored on database, but not with an opposite flow
 * 411, "Incorrect reversal category used with original transaction category"
 */
public class Validate_ReversalTrnRef implements ICustomValidate {
  private Map<String, TrnRefInfo> trnInfoMap;

  public Validate_ReversalTrnRef(Map<String, TrnRefInfo> trnInfoMap) {
    this.trnInfoMap = trnInfoMap;
  }

  private String getFlowForTrn(String trnRef, Integer sequence) {
    if (trnInfoMap.containsKey(trnRef)) {
      TrnRefInfo trnInfo = trnInfoMap.get(trnRef);
      if (trnInfo.getFlow() != null) {
        return trnInfo.getFlow();
      }
    }
    return null;
  }

  private String getCategoryForTrn(String trnRef, Integer sequence) {
    if (trnInfoMap.containsKey(trnRef)) {
      TrnRefInfo trnInfo = trnInfoMap.get(trnRef);
      if (trnInfo.getCategory() != null) {
        return trnInfo.getCategory();
      }
    }
    return "";
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    Integer sequence = null;
    String flow = null;
    String category = null;
    if (otherInputs.length > 0)
      flow = otherInputs[0].toString();
    if (otherInputs.length > 1 && otherInputs[1] != null) {
      String seqNum = otherInputs[1].toString();
      if (seqNum.length() > 0)
        sequence = Integer.parseInt(seqNum);
    }
    if (otherInputs.length > 2 && otherInputs[2] != null) {
      category = otherInputs[2].toString();
    }
    String trnRef = value.toString();

    // call tp find flow of old transaction with trnRef and sequence
    String oldFlow = getFlowForTrn(trnRef, sequence);
    String oldCategory = getCategoryForTrn(trnRef, sequence);

    if (oldFlow == null) {
      return new CustomValidateResult(StatusType.Fail, "410", "Original transaction and SequenceNumber combination not stored on database");
    }
    else
    if (oldFlow.equals(flow)) {
      return new CustomValidateResult(StatusType.Fail, "410", "Original transaction and SequenceNumber combination stored on database, but not with an opposite flow");
    }
    else
    if (category != null && category.length() > 0 && oldCategory.length() > 0 &&
            category.charAt(0) != oldCategory.charAt(0)) {
      // ErrorId: 2
      return new CustomValidateResult(StatusType.Fail, "411", "Incorrect reversal category used with original transaction category. Original category is " + oldCategory);
    }
    else {
      return new CustomValidateResult(StatusType.Pass);
    }
  }
}
