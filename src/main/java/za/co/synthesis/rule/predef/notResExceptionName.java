package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 2:42 PM
 * Determines if the current value is not a valid resident exception name
 */
public class notResExceptionName implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !context.getLookups().isResExceptionName(value.toString());
  }
}

/*
function notResExceptionName(context, value) {
  if (!value)
    return true;
  return !context.lookups.isResExceptionName(value);
}
*/