var mapping = {
  src: {
//    cd3_ack: {
//      description: "SARB Response",
//      select: "SELECT m.UNIQUE_BOPCUS_N, ack.* FROM CD3_MATRIX m JOIN CD3_ACK ack ON ack.UNIQUE_ACK_N = m.UNIQUE_ACK_N",
//      transaction_field: "m.UNIQUE_BOPCUS_N",
//      order_by: "m.UNIQUE_BOPCUS_N",
////      select: "SELECT r.* FROM Result r",
////      transaction_field: "r.TRN_REFERENCE",
////      order_by: "r.TRN_REFERENCE",
//      master_src: "cd3_tran"
//    },
//    cd3_tran: {
//      description: "Transaction",
//      select: "select    cd3_util.get_flow(x.SYSTEM_ID, x.BUSINESS_TRAN_C, x.B_S_IND) AS FLOW, x.* FROM CD3_TRAN_STG x",
//      transaction_field: "x.TRN_REFERENCE",
//      order_by: "x.TRN_REFERENCE, x.UNIQUE_TRAN_N, x.SEQUENCE_NUMBER"
//      select: "SELECT t.* FROM CoreTransaction t",
//      transaction_field: "t.TRN_REFERENCE",
//      order_by: "t.TRN_REFERENCE"
//    },
    cd3_money: {
      description: "Monetary Amount",
      select: "select cd3_util.get_flow(x.SYSTEM_ID, x.BUSINESS_TRAN_C, x.B_S_IND) AS FLOW, b.BRANCH_NAME as LU_BRANCH_NAME, x.FOREIGN_VALUE + x.RAND_VALUE as CALC_TOTAL_VALUE, x.* FROM CD3_TRAN_STG x LEFT JOIN CD3_SBSA_BRANCH b ON b.BRANCH_CODE = x.BRANCH_CODE",
      transaction_field: "x.TRN_REFERENCE",
      order_by: "x.TRN_REFERENCE, x.SEQUENCE_NUMBER",
      money_field: "SEQUENCE_NUMBER"
//      select: "SELECT m.* FROM MonetaryAmount m",
//      transaction_field: "m.TRN_REFERENCE",
//      money_field: "m.SEQUENCE_NUMBER",
//      order_by: "m.TRN_REFERENCE, m.SEQUENCE_NUMBER",
    },
    cd3_importexport: {
      description: "Import Export entries",
      select: "SELECT ie.* FROM CD3_IMP_EXP_STG ie",
      transaction_field: "ie.TRN_REFERENCE",
      money_field: "ie.TRN_SEQUENCE",
      order_by: "ie.TRN_REFERENCE, ie.TRN_SEQUENCE, ie.SUB_SEQUENCE",
//      select: "SELECT ie.* FROM ImportExport ie",
//      transaction_field: "ie.TRN_REFERENCE",
//      money_field: "ie.SEQUENCE_NUMBER",
//      order_by: "ie.TRN_REFERENCE, ie.SEQUENCE_NUMBER",
      master_src: "cd3_money"
    }
  },
  dest: [
//    { scope: "result",
//      src: "cd3_ack",
//      fields: [
//        {col: "ERROR_CODE",          field: "ErrorCode"},
//        {col: "ERROR_DESCRIPTION",   field: "ErrorDescription"},
//        {col: "ATTRIBUTE",           field: "ErrorAttribute"},
//        {col: "TRN_REFERENCE",       field: "TrnReference"},
//        {col: "UNIQUE_BOPCUS_N",     field : "UNIQUE_BOPCUS_N"}, // 129881782
//        {col: "SEQUENCE_NUMBER",     field: "Level1SeqNo"},
//        {col: "SEQUENCE_NUMBER",     field: "Level2SeqNo"},
//        {col: "TRN_STATUS",          field: "TrnStatus"}
//      ]
//    },

      /*  CHECK FOR ERROR_LOG TABLE FOR STAGING DATA  - THIS SHOULD HAVE EVALUATION RESULTS */
    { scope: "transaction",
      src: "cd3_money",
      fields: [
        {col: "TRN_REFERENCE",       field: "TrnReference", aggregate: "first"},
        {col: "SYSTEM_ID",           meta : "SYSTEM_ID", aggregate: "first"},
        {col: "SOURCE_REF_X",           meta : "SOURCE_REF_X", aggregate: "first"},
        {col: "RELATED_REF_C",           meta : "RELATED_REF_C", aggregate: "first"},
        {col: "IMP_EXP_IND",         field : "IMP_EXP_IND", aggregate: "first"},
        {col: "BUSINESS_TRAN_C",     meta : "BUSINESS_TRAN_C", aggregate: "first"},
        {col: "ATTRIBUTE",           meta : "ATTRIBUTE", aggregate: "first"},
        {col: "ERROR_CODE",          meta : "ERROR_CODE", aggregate: "first"},
        {col: "ERROR_DESCRIPTION",   meta : "ERROR_DESCRIPTION", aggregate: "first"},
        {col: "UNIQUE_BOPCUS_N",     meta : "UNIQUE_BOPCUS_N", aggregate: "first"}, // 129881782
        {col: "MESSAGE_TYPE",        meta : "MESSAGE_TYPE", aggregate: "first"}, // 9
        {col: "RECORD_TYPE",         meta : "RECORD_TYPE", aggregate: "first"}, // V
          /*FIND / CREATE this value...*/
        {col: "REPORTING_Q", field: "ReportingQualifier", aggregate: "first"}, // BOPCUS
        {col: "FLOW",                field: "Flow", aggregate: "first"}, // OUT
        {col: "REPLACEMENT_TRAN_REF", field: "ReplacementTransaction", aggregate: "first"}, // N
        {col: "REPLACEMENT_ORIGINAL_REF", field: "ReplacementOriginalReference", aggregate: "first"}, //
        {col: "VALUE_DATE",          field: "ValueDate", type: "date", aggregate: "first"}, // 22/DEC/14
        {col: "BRANCH_CODE",         field: "BranchCode", default: "", aggregate: "first"}, // 02015500
        {col: "BRANCH_NAME",         meta: "BRANCH_NAME", default: "", aggregate: "first"},
        {col: "BRANCH_NAME",         field: "BranchName", default: "", aggregate: "first"}, // TPS TRADE PROCESSING
        {col: "LU_BRANCH_NAME",      field: "BranchName", default: "", aggregate: "first", condition: {BRANCH_NAME: ""}},
        {col: "HUB_CODE",            field: "HubCode", aggregate: "first"}, //
        {col: "HUB_NAME",            field: "HubName", aggregate: "first"}, //
        {col: "ORIGINATING_BANK",    field: "OriginatingBank", aggregate: "first"}, // SBZAZAJJ
        {col: "ORIGINATING_COUNTRY", field: "OriginatingCountry", aggregate: "first"}, // ZA
        {col: "CORRESPONDENT_BANK",    field: "CorrespondentBank", aggregate: "first"}, // SCBLUS33
        {col: "CORRESPONDENT_COUNTRY", field: "CorrespondentCountry", aggregate: "first"}, // US
        {col: "RECEIVING_BANK",      field: "ReceivingBank", aggregate: "first"}, // BOTKJPJT
        {col: "RECEIVING_COUNTRY",   field: "ReceivingCountry", aggregate: "first"}, // JP
        {col: "CALC_TOTAL_VALUE",   field: "TotalValue", aggregate: "sum"}, // JP
        {col: "FOREIGN_CURRENCY_CODE", field: "FlowCurrency", aggregate: "first", default: "ZAR"},
        {col: "FOREIGN_CURRENCY_CODE", meta: "FOREIGN_CURRENCY_CODE", aggregate: "first", default: "ZAR"},
        {col: "FOREIGN_VALUE",       field: "TotalForeignValue", aggregate: "sum"}, //
        {col: "RAND_VALUE",          field: "TotalForeignValue", aggregate: "sum", condition: {FOREIGN_CURRENCY_CODE: "ZAR"}}, // 454659.56
        {col: "NON_RES_CUST_TYPE",   meta : "NON_RES_CUST_TYPE", aggregate: "first"}, // I, E, X
        {col: "NON_RES_SURNAME",     field: "NonResident.Individual.Surname", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_NAME",        field: "NonResident.Individual.Name", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_GENDER",        field: "NonResident.Individual.Gender", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_PASSPORT_NUMBER", field: "NonResident.Individual.PassportNumber", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_PASSPORT_COUNTRY", field: "NonResident.Individual.PassportCountry", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_ADDRESS_LINE1", field: "NonResident.Individual.Address.AddressLine1", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_ADDRESS_LINE2", field: "NonResident.Individual.Address.AddressLine2", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_ADDRESS_LINE3", field: "NonResident.Individual.Address.Suburb", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_CITY",          field: "NonResident.Individual.Address.City", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_STATE",         field: "NonResident.Individual.Address.State", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_ZIP_CODE",      field: "NonResident.Individual.Address.PostalCode", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_COUNTRY",       field: "NonResident.Individual.Address.Country", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "NON_RES_ACCOUNT_IDENTIFIER", field: "NonResident.Individual.AccountIdentifier", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"}, // NON RESIDENT OTHER
        {col: "NON_RES_ACCOUNT_NUMBER", field: "NonResident.Individual.AccountNumber", condition: {NON_RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "NON_RES_LEGAL_ENTITY_NAME", field: "NonResident.Entity.EntityName", condition: {NON_RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "NON_RES_ACCOUNT_IDENTIFIER", field: "NonResident.Entity.AccountIdentifier", condition: {NON_RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "NON_RES_ACCOUNT_NUMBER", field: "NonResident.Entity.AccountNumber", condition: {NON_RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "NON_RES_ADDRESS_LINE1", field: "NonResident.Entity.Address.AddressLine1", condition: {NON_RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "NON_RES_ADDRESS_LINE2", field: "NonResident.Entity.Address.AddressLine2", condition: {NON_RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "NON_RES_ADDRESS_LINE3", field: "NonResident.Entity.Address.Suburb", condition: {NON_RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "NON_RES_CITY",          field: "NonResident.Entity.Address.City", condition: {NON_RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "NON_RES_STATE",         field: "NonResident.Entity.Address.State", condition: {NON_RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "NON_RES_ZIP_CODE",      field: "NonResident.Entity.Address.PostalCode", condition: {NON_RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "NON_RES_COUNTRY",       field: "NonResident.Entity.Address.Country", condition: {NON_RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "NON_RES_EXCEPTION_NAME", field: "NonResident.Exception.ExceptionName", condition: {NON_RES_CUST_TYPE: "X"}, default: "", aggregate: "first"}, //
        {col: "RES_CUST_TYPE",         meta : "RES_CUST_TYPE", aggregate: "first"}, // I, E, X
        {col: "RES_SURNAME",           field: "Resident.Individual.Surname", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_NAME",              field: "Resident.Individual.Name", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_GENDER",            field: "Resident.Individual.Gender", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_DATE_OF_BIRTH",     field: "Resident.Individual.DateOfBirth", type: "date", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_ID_NUMBER",         field: "Resident.Individual.IDNumber", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_TEMP_RES_PERMIT_NUMBER", field: "Resident.Individual.TempResPermitNumber", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_FOREIGN_ID_NUMBER", field: "Resident.Individual.ForeignIDNumber", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_FOREIGN_ID_COUNTRY", field: "Resident.Individual.ForeignIDCountry", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_PASSPORT_NUMBER",   field: "Resident.Individual.PassportNumber", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_PASSPORT_COUNTRY",  field: "Resident.Individual.PassportCountry", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_ACCOUNT_NAME",      field: "Resident.Individual.AccountName", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_ACCOUNT_IDENTIFIER", field: "Resident.Individual.AccountIdentifier", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, // RESIDENT OTHER
        {col: "RES_ACCOUNT_NUMBER", field: "Resident.Individual.AccountNumber", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_CUSTOMS_CLIENT_NUMBER", field: "Resident.Individual.CustomsClientNumber", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_TAX_NUMBER",        field: "Resident.Individual.TaxNumber", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_VAT_NUMBER",        field: "Resident.Individual.VATNumber", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_TAX_CLEARANCE_CERT_IND", field: "Resident.Individual.TaxClearanceCertificateIndicator", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, // N
        {col: "RES_TAX_CLEARANCE_CERT_NO", field: "Resident.Individual.TaxClearanceCertificateReference", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_STR_ADDRESS_LINE1", field: "Resident.Individual.StreetAddress.AddressLine1", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_STR_ADDRESS_LINE2", field: "Resident.Individual.StreetAddress.AddressLine2", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_STR_SUBURB",     field: "Resident.Individual.StreetAddress.Suburb", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_STR_CITY",       field: "Resident.Individual.StreetAddress.City", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, // GAUTENG
        {col: "RES_STR_PROVINCE",   field: "Resident.Individual.StreetAddress.Province", transform: "toUpper", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, // GAUTENG
        {col: "RES_STR_POSTAL_CODE", field: "Resident.Individual.StreetAddress.PostalCode", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, // 1501
        {col: "RES_POSTAL_ADDRESS_LINE1", field: "Resident.Individual.PostalAddress.AddressLine1", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_POSTAL_ADDRESS_LINE2", field: "Resident.Individual.PostalAddress.AddressLine2", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_POSTAL_SUBURB",     field: "Resident.Individual.PostalAddress.Suburb", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, // BENONI SOUTH
        {col: "RES_POSTAL_CITY",       field: "Resident.Individual.PostalAddress.City", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, // GAUTENG
        {col: "RES_POSTAL_PROVINCE",   field: "Resident.Individual.PostalAddress.Province", transform: "toUpper", condition: {RES_CUST_TYPE: "I"}}, // GAUTENG
        {col: "RES_POSTAL_CODE",       field: "Resident.Individual.PostalAddress.PostalCode", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, // 1502
        {col: "RES_CONTACT_SURNAME",   field: "Resident.Individual.ContactDetails.ContactSurname", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_CONTACT_NAME",      field: "Resident.Individual.ContactDetails.ContactName", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_EMAIL",             field: "Resident.Individual.ContactDetails.Email", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_FAX",               field: "Resident.Individual.ContactDetails.Fax", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"}, //
        {col: "RES_TELEPHONE",         field: "Resident.Individual.ContactDetails.Telephone", condition: {RES_CUST_TYPE: "I"}, aggregate: "first"},
        {col: "RES_LEGAL_ENTITY_NAME", field: "Resident.Entity.EntityName", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_TRADING_NAME",      field: "Resident.Entity.TradingName", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_REGISTRATION_NUMBER", field: "Resident.Entity.RegistrationNumber", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_INSTITUTIONAL_SECTOR", field: "Resident.Entity.InstitutionalSector", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // 01
        {col: "RES_INDUSTRIAL_CLASSIFICATION", field: "Resident.Entity.IndustrialClassification", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // 08
        {col: "RES_ACCOUNT_NAME",      field: "Resident.Entity.AccountName", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_ACCOUNT_IDENTIFIER", field: "Resident.Entity.AccountIdentifier", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_ACCOUNT_NUMBER", field: "Resident.Entity.AccountNumber", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_CUSTOMS_CLIENT_NUMBER", field: "Resident.Entity.CustomsClientNumber", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_TAX_NUMBER",        field: "Resident.Entity.TaxNumber", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_VAT_NUMBER",        field: "Resident.Entity.VATNumber", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_TAX_CLEARANCE_CERT_IND", field: "Resident.Entity.TaxClearanceCertificateIndicator", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // N
        {col: "RES_TAX_CLEARANCE_CERT_NO", field: "Resident.Entity.TaxClearanceCertificateReference", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "RES_STR_ADDRESS_LINE1", field: "Resident.Entity.StreetAddress.AddressLine1", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_STR_ADDRESS_LINE2", field: "Resident.Entity.StreetAddress.AddressLine2", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "RES_STR_SUBURB",     field: "Resident.Entity.StreetAddress.Suburb", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // BENONI SOUTH
        {col: "RES_STR_CITY",       field: "Resident.Entity.StreetAddress.City", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // GAUTENG
        {col: "RES_STR_PROVINCE",   field: "Resident.Entity.StreetAddress.Province", transform: "toUpper", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // GAUTENG
        {col: "RES_STR_POSTAL_CODE", field: "Resident.Entity.StreetAddress.PostalCode", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // 1501
        {col: "RES_POSTAL_ADDRESS_LINE1", field: "Resident.Entity.PostalAddress.AddressLine1", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_POSTAL_ADDRESS_LINE2", field: "Resident.Entity.PostalAddress.AddressLine2", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "RES_POSTAL_SUBURB",     field: "Resident.Entity.PostalAddress.Suburb", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // BENONI SOUTH
        {col: "RES_POSTAL_CITY",       field: "Resident.Entity.PostalAddress.City", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // GAUTENG
        {col: "RES_POSTAL_PROVINCE",   field: "Resident.Entity.PostalAddress.Province", transform: "toUpper", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // GAUTENG
        {col: "RES_POSTAL_CODE",       field: "Resident.Entity.PostalAddress.PostalCode", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, // 1502
        {col: "RES_CONTACT_SURNAME",   field: "Resident.Entity.ContactDetails.ContactSurname", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_CONTACT_NAME",      field: "Resident.Entity.ContactDetails.ContactName", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_EMAIL",             field: "Resident.Entity.ContactDetails.Email", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "RES_FAX",               field: "Resident.Entity.ContactDetails.Fax", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"}, //
        {col: "RES_TELEPHONE",         field: "Resident.Entity.ContactDetails.Telephone", condition: {RES_CUST_TYPE: "E"}, aggregate: "first"},
        {col: "RES_EXCEPTION_NAME",    field: "Resident.Exception.ExceptionName", condition: {RES_CUST_TYPE: "X"}, default: "", aggregate: "first"}, //
        {col: "RES_COUNTRY",           field: "Resident.Exception.Country", condition: {RES_CUST_TYPE: "X"}, aggregate: "first"}, //

        {col: "UNIQUE_TRAN_N",         meta : "UNIQUE_TRAN_N", aggregate: "first"},
        {col: "UNIQUE_ENTRY_N",        meta : "UNIQUE_ENTRY_N", aggregate: "first"},
        {col: "UNIQUE_XML_OUT_N",      meta : "UNIQUE_XML_OUT_N", aggregate: "first"},
        {col: "UNIQUE_ACK_N",          meta : "UNIQUE_ACK_N", aggregate: "first"},
        {col: "COMPLETE_IND",          meta : "COMPLETE_IND", aggregate: "first"},
        {col: "COMPLETE_DATE",         meta : "COMPLETE_DATE", type: "date", aggregate: "first"},
        {col: "UNIQUE_XML_IN_N",       meta : "UNIQUE_XML_IN_N", aggregate: "first"}
      ]
    },
    { scope: "money",
      src: "cd3_money",
      fields: [
        {col: "RECEIVED_DATE",             meta : "CREATED_Z"}, // 22/DEC/14
        {col: "SEQUENCE_NUMBER",       field: "SequenceNumber"}, // 1
        {col: "MONEY_TRNSFR_AGENT_IND", field: "MoneyTransferAgentIndicator"}, // AD
        {col: "RAND_VALUE",            field: "RandValue"}, // 454659.56
        {col: "FOREIGN_VALUE",         field: "ForeignValue"}, // 38700.03
        {col: "BOP_CATEGORY",          field: "CategoryCode"}, // 104
        {col: "SUB_BOP_CATEGORY",      field: "CategorySubCode"}, // 01
        {col: "SWIFT_DETAIL",          field: "SWIFTDetails"},
        {col: "STRATE_REF_NUMBER",     field: "StrateRefNumber"},
        {col: "LOAN_REF_NUMBER",       field: "LoanRefNumber"},
        {col: "LOAN_TENOR",            field: "LoanTenor"},
        {col: "LOAN_INTEREST_RATE",    field: "LoanInterestRate"},
        {col: "RULINGS_SECTION",       field: "SARBAuth.RulingsSection"}, // B.1(H)
        {col: "AD_INTERNAL_AUTH_NUMBER", field: "SARBAuth.ADInternalAuthNumber"}, //
        {col: "AD_INTERNAL_AUTH_NUMBER_DATE", field: "SARBAuth.ADInternalAuthNumberDate", type: "date"}, //
        {col: "SARB_AUTH_APPLIC_NUMBER", field: "SARBAuth.SARBAuthAppNumber"}, //
        {col: "SARB_AUTH_REFERENCE_NUMBER", field: "SARBAuth.SARBAuthRefNumber"}, //
        {col: "CANNOT_CATEGORISE",     field: "CannotCategorize"}, //
        {col: "SUBJECT",               field: "AdHocRequirement.Subject"}, //
        {col: "DESCRIPTION",           field: "AdHocRequirement.Description"}, //
        {col: "LOCATION_COUNTRY",      field: "LocationCountry"}, // JO
        {col: "REVERSAL_TRN_REF_NUMBER", field: "ReversalTrnRefNumber"}, //
        {col: "REVERSAL_SEQUENCE",     field: "ReversalTrnSeqNumber"}, //
        {col: "IND_3RD_PRTY_SURNAME", field: "ThirdParty.Individual.Surname"}, //
        {col: "IND_3RD_PRTY_NAME",    field: "ThirdParty.Individual.Name"}, //
        {col: "IND_3RD_PRTY_GENDER",  field: "ThirdParty.Individual.Gender"}, //
        {col: "IND_3RD_PRTY_ID_NUMBER", field: "ThirdParty.Individual.IDNumber"}, //
        {col: "IND_3RD_PRTY_DATE_OF_BIRTH", field: "ThirdParty.Individual.DateOfBirth", type: "date"}, //
        {col: "IND_3RD_PRTY_TEMP_RES_PRMT_N", field: "ThirdParty.Individual.TempResPermitNumber"}, //
        {col: "IND_3RD_PRTY_PASSPORT_NUMBER", field: "ThirdParty.Individual.PassportNumber"}, //
        {col: "IND_3RD_PRTY_PASSPORT_COUNTRY", field: "ThirdParty.Individual.PassportCountry"}, //
        {col: "LEGAL_ENTITY_3RD_PRTY_NAME", field: "ThirdParty.Entity.Name"}, //
        {col: "LEGAL_ENTITY_3RD_PRTY_REG_NO", field: "ThirdParty.Entity.RegistrationNumber"}, //
        {col: "THRD_PRTY_CUSTOMS_CLIENT_CODE", field: "ThirdParty.CustomsClientNumber"}, //
        {col: "THRD_PRTY_TAX_NUMBER",  field: "ThirdParty.TaxNumber"}, //
        {col: "THRD_PRTY_VAT_NUMBER",  field: "ThirdParty.VATNumber"}, //
        {col: "THRD_PRTY_STR_ADDRESS_LINE1", field: "ThirdParty.StreetAddress.AddressLine1"}, //
        {col: "THRD_PRTY_STR_ADDRESS_LINE2", field: "ThirdParty.StreetAddress.AddressLine2"}, //
        {col: "THRD_PRTY_STR_SUBURB", field: "ThirdParty.StreetAddress.Suburb"}, //
        {col: "THRD_PRTY_STR_CITY", field: "ThirdParty.StreetAddress.City"}, //
        {col: "THRD_PRTY_STR_PROVINCE", field: "ThirdParty.StreetAddress.Province", transform: "toUpper"}, //
        {col: "THRD_PRTY_STR_POSTAL_CODE", field: "ThirdParty.StreetAddress.PostalCode"}, //
        {col: "THRD_PRTY_POSTAL_ADDRESS_LINE1", field: "ThirdParty.PostalAddress.AddressLine1"}, //
        {col: "THRD_PRTY_POSTAL_ADDRESS_LINE2", field: "ThirdParty.PostalAddress.AddressLine2"}, //
        {col: "THRD_PRTY_POSTAL_SUBURB", field: "ThirdParty.PostalAddress.Suburb"}, //
        {col: "THRD_PRTY_POSTAL_CITY", field: "ThirdParty.PostalAddress.City"}, //
        {col: "THRD_PRTY_POSTAL_PROVINCE", field: "ThirdParty.PostalAddress.Province", transform: "toUpper"}, //
        {col: "THRD_PRTY_POSTAL_CODE", field: "ThirdParty.PostalAddress.PostalCode"}, //
        {col: "THRD_PRTY_CONTACT_SURNAME", field: "ThirdParty.ContactDetails.ContactSurname"}, //
        {col: "THRD_PRTY_CONTACT_NAME", field: "ThirdParty.ContactDetails.ContactName"}, //
        {col: "THRD_PRTY_EMAIL",       field: "ThirdParty.ContactDetails.Email"}, //
        {col: "THRD_PRTY_FAX",         field: "ThirdParty.ContactDetails.Fax"}, //
        {col: "THRD_PRTY_TELEPHONE",   field: "ThirdParty.ContactDetails.Telephone"} //
      ]
    },
    { scope: "importexport",
      src: "cd3_importexport",
      fields: [
        {col: "IMPORT_CONTROL_N", field: "ImportControlNumber"}, //
        {col: "TRANSPORT_DOCUMENT_N",  field: "TransportDocumentNumber"}, //
        {col: "NOMRNONIVS",            field: "MRNNotOnIVS"}, //
        {col: "UCR",                   field: "UCR"}, //
        {col: "PAYMENT_VALUE",         field: "PaymentValue"}, //
        {col: "PAYMENT_CURR_CODE",     field: "PaymentCurrencyCode"}, //
        {col: "SUB_SEQUENCE",          field: "SubSequence"} //
      ]
    }
  ]
}
