package za.co.synthesis.rule.core;

/**
 * Created by jake on 6/28/16.
 */
public enum CounterpartyInstituition {
  Unknown,
  Offshore,
  OnshoreAD,
  OnshoreOther
}
