package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 2:39 PM
 * Determines if the current value is not a valid nonresident exception name
 */
public class notNonResExceptionName implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    String valStr = value.toString();
    return !context.getLookups().isNonResExceptionName(valStr);
  }
}
/*
function notNonResExceptionName(context, value) {
  if (!value)
    return true;
  return !context.lookups.isNonResExceptionName(value);
}*/