package za.co.synthesis.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.util.Map;

/**
 * User: jake
 * Date: 1/19/16
 * Time: 11:42 AM
 * Test function for validating loan references
 */
public class Validate_LoanRef implements ICustomValidate {
  private Map<String, LoanInfo> loanInfoMap;

  public Validate_LoanRef(Map<String, LoanInfo> loanInfoMap) {
    this.loanInfoMap = loanInfoMap;
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String loanRef = value.toString();

    String errorCode = "374";
    String errorDesc = "Invalid loan reference number";

    if (loanInfoMap.containsKey(loanRef)) {
      LoanInfo loanInfo = loanInfoMap.get(loanRef);

      if (loanInfo.isValidLoanRef())
        return new CustomValidateResult(StatusType.Pass);
      else
        return new CustomValidateResult(StatusType.Fail, errorCode, errorDesc);
    }
    else {
      return new CustomValidateResult(StatusType.Pass);
    }
  }
}
