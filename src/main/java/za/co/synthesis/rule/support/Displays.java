package za.co.synthesis.rule.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.FlowType;
import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.rule.predef.alwaysTrue;
import za.co.synthesis.rule.predef.formatDisplayFunctionResult;
import za.co.synthesis.rule.predef.formatScopedFieldValue;
import za.co.synthesis.rule.predef.useConstant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 4/9/16.
 */
public class Displays {
  Logger logger = LoggerFactory.getLogger(Displays.class);

  private final List<DisplayRuleSet> ruleSets = new ArrayList<DisplayRuleSet>();


  public IDisplayFunction loadApplyFunc(IDisplayFunction left, JSIdentifier apply) throws Exception {
    IDisplayFunction result = left;
    if (left != null && apply != null) {
      if (apply instanceof JSFunctionCall) {
        JSFunctionCall applyFunc = (JSFunctionCall)apply;
        if (applyFunc.getName().equals("then")) {
          result = new DFThen(left, parseDisplayFunction(applyFunc.getParameters().get(0)));
        }

        result = loadApplyFunc(result, applyFunc.getApply());
      }
    }
    return result;
  }

  private IDisplayFunction parseDisplayFunction(Object srcParam) throws Exception {
    IDisplayFunction result = null;
    if (srcParam instanceof JSFunctionCall) {
      JSFunctionCall func = (JSFunctionCall)srcParam;

      if (func.getParameters().size() == 1) {
        Object param = func.getParameters().get(0);
        if (param instanceof JSArray) {
          StringList params = DSLSupport.composeStringList(param);
          result = DisplayFunctionFactory.createFunction(func.getName(), params);
        }
        else {
          if (param instanceof String)
            result = DisplayFunctionFactory.createFunction(func.getName(), (String) param);
          else
          if (param instanceof JSRegExLiteral)
            result = DisplayFunctionFactory.createFunction(func.getName(), (JSRegExLiteral) param);
          else
          if (param instanceof JSIdentifier)
            result = DisplayFunctionFactory.createFunction(func.getName(), parseDisplayFunction(param));
          else {
            result = DisplayFunctionFactory.createFunction(func.getName(), param.toString());
          }
        }
      }
      else
      if (func.getParameters().size() == 2) {
        Object param1 = func.getParameters().get(0);
        Object param2 = func.getParameters().get(1);
        if (param1 instanceof String) {
          if (param2 instanceof JSArray) {
            StringList params = DSLSupport.composeStringList(param2);
            result = DisplayFunctionFactory.createFunction(func.getName(), (String)param1, params);
          }
          else
          if (param2 instanceof JSIdentifier) {
            result = DisplayFunctionFactory.createFunction(func.getName(), (String)param1, parseDisplayFunction(param2));
          }
          else {
            result = DisplayFunctionFactory.createFunction(func.getName(), (String)param1, param2.toString());
          }
        }
        else
        if (param1 instanceof JSRegExLiteral) {
          result = DisplayFunctionFactory.createFunction(func.getName(), (JSRegExLiteral)param1, param2.toString());
        }
      }
      else
      if (func.getParameters().size() == 4) {
        Object param1 = func.getParameters().get(0);
        Object param2 = func.getParameters().get(1);
        Object param3 = func.getParameters().get(2);
        Object param4 = func.getParameters().get(3);
        if (param1 instanceof String && param2 instanceof String && param3 instanceof String && param4 instanceof String)
          result = DisplayFunctionFactory.createFunction(func.getName(), (String)param1, (String)param2, (String)param3, (String)param4);
      }
      result = loadApplyFunc(result, func.getApply());
    }
    else
    if (srcParam instanceof JSIdentifier) {
      JSIdentifier id = (JSIdentifier)srcParam;
      result = DisplayFunctionFactory.createFunction(id.getName());

      result = loadApplyFunc(result, id.getApply());
    }
    else
    if (srcParam instanceof String) {
      result = new useConstant((String)srcParam);
    }

    return result;
  }

  private IDisplayFunction get1stParamStringDisplayFunction(Scope scope, JSFunctionCall jsFunc) throws Exception {
    Object formatParam = jsFunc.getParameters().get(0);
    Object srcParam = null;
    if (jsFunc.getParameters().size() > 1)
      srcParam = jsFunc.getParameters().get(1);

    if (srcParam instanceof JSIdentifier) {
      JSIdentifier idParam = (JSIdentifier)srcParam;
      if (idParam.isKeyword("null")) {
        srcParam = null;
      }
    }

    IDisplayFunction displayFunction = null;
    if (formatParam instanceof String) {
      if (srcParam == null)
        displayFunction = new useConstant((String)formatParam);
      else {
        if (srcParam instanceof String)
          displayFunction = new formatScopedFieldValue((String)formatParam, (String)srcParam);
        else
          displayFunction = new formatDisplayFunctionResult((String)formatParam, parseDisplayFunction(srcParam));
      }
    }
    return displayFunction;
  }

  private IDisplayFunction get1stParamIdentifierDisplayFunction(JSFunctionCall jsFunc) throws Exception {
    Object srcParam = jsFunc.getParameters().get(0);

    IDisplayFunction displayFunction = null;
    if (srcParam instanceof JSIdentifier) {
      displayFunction = parseDisplayFunction(srcParam);
    }
    return displayFunction;
  }


  private DisplayRule loadDisplayRule(Map<String, String> mapping, Scope scope, JSFunctionCall jsFunc) throws Exception {
    DisplayRule result = null;

    IAssertionFunction assertion;
    if (jsFunc.getName().equals("extend")) {
      result = new Extend();
    }
    else
    if (jsFunc.getName().equals("excludeValue")) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 1);
      result = new ExcludeValue(
              DSLSupport.composeStringList(jsFunc.getParameters().get(0)),
              assertion != null ? assertion : new alwaysTrue());
    }
    else
    if (jsFunc.getName().equals("limitValue")) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 1);
      result = new LimitValue(
              DSLSupport.composeStringList(jsFunc.getParameters().get(0)),
              assertion != null ? assertion : new alwaysTrue());
    }
    else
    if (jsFunc.getName().equals("show")) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 0);
      result = new Show(assertion != null ? assertion : new alwaysTrue());
    }
    else
    if (jsFunc.getName().equals("hide")) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 0);
      result = new Hide(assertion != null ? assertion : new alwaysTrue());
    }
    else
    if (jsFunc.getName().equals("enable")) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 0);
      result = new Enable(assertion != null ? assertion : new alwaysTrue());
    }
    else
    if (jsFunc.getName().equals("disable")) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 0);
      result = new Disable(assertion != null ? assertion : new alwaysTrue());
    }
    else
    if (jsFunc.getName().equals("clearValue")) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 0);
      result = new ClearValue(assertion != null ? assertion : new alwaysTrue());
    }
    else
    if (jsFunc.getName().equals("setValue")) {
      Object firstParam = jsFunc.getParameters().get(0);
      if (firstParam instanceof String) {
        assertion = DSLSupport.getAssertion(mapping, jsFunc, 2);

        result = new SetValue(
                get1stParamStringDisplayFunction(scope, jsFunc),
                assertion != null ? assertion : new alwaysTrue());
      }
      else
      if (firstParam instanceof JSIdentifier) {
        assertion = DSLSupport.getAssertion(mapping, jsFunc, 1);

        result = new SetValue(
                get1stParamIdentifierDisplayFunction(jsFunc),
                assertion != null ? assertion : new alwaysTrue());
      }
    }
    else
    if (jsFunc.getName().equals("setField")) {
      Object firstParam = jsFunc.getParameters().get(0);
      Object secondParam = jsFunc.getParameters().get(1);
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 2);
      result = new SetField(
              firstParam.toString(),
              parseDisplayFunction(secondParam),
              assertion != null ? assertion : new alwaysTrue());
    }
    else
    if (jsFunc.getName().equals("appendValue")) {
      Object firstParam = jsFunc.getParameters().get(0);
      if (firstParam instanceof String) {
        assertion = DSLSupport.getAssertion(mapping, jsFunc, 2);

        result = new AppendValue(
                get1stParamStringDisplayFunction(scope, jsFunc),
                assertion != null ? assertion : new alwaysTrue());
      }
      else
      if (firstParam instanceof JSIdentifier) {
        assertion = DSLSupport.getAssertion(mapping, jsFunc, 1);

        result = new AppendValue(
                get1stParamIdentifierDisplayFunction(jsFunc),
                assertion != null ? assertion : new alwaysTrue());
      }
    }
    else
    if (jsFunc.getName().equals("setLabel")) {
      Object firstParam = jsFunc.getParameters().get(0);
      if (firstParam instanceof String) {
        assertion = DSLSupport.getAssertion(mapping, jsFunc, 2);

        result = new SetLabel(
                get1stParamStringDisplayFunction(scope, jsFunc),
                assertion != null ? assertion : new alwaysTrue());
      }
      else
      if (firstParam instanceof JSIdentifier) {
        assertion = DSLSupport.getAssertion(mapping, jsFunc, 1);

        result = new SetLabel(
                get1stParamIdentifierDisplayFunction(jsFunc),
                assertion != null ? assertion : new alwaysTrue());
      }
    }

    if (result != null) {
      DSLSupport.applyFilters(result, jsFunc.getApply());
    }
    return result;
  }

  public void loadCommonFunctions(JSObject functionsObj) throws Exception {
    for (Map.Entry<String, Object> entry : functionsObj.entrySet()) {
      if (entry.getValue() instanceof JSFunctionCall) {
        DisplayFunctionFactory.registerFunction(entry.getKey(),
                parseDisplayFunction(entry.getValue()));
      }
    }
  }

  public void loadRuleSet(Map<String, String> mapping, JSObject jsRuleSet) {
    Object rulesetName = jsRuleSet.get("ruleset");
    Object rulesetScope = jsRuleSet.get("scope");

    if (rulesetName != null && rulesetScope != null) {
      Scope scope = Scope.Transaction;
      if (rulesetScope.equals(Scope.Money.getName()))
        scope = Scope.Money;
      else
      if (rulesetScope.equals(Scope.ImportExport.getName()))
        scope = Scope.ImportExport;
      DisplayRuleSet ruleset = new DisplayRuleSet(rulesetName.toString(), scope);

      JSArray jsFields = (JSArray)jsRuleSet.get("fields");
      if (jsFields != null) {
        for (Object obj : jsFields) {
          if (obj instanceof JSObject) {
            JSObject jsValidation = (JSObject)obj;
            Object fieldName = jsValidation.get("field");

            if (fieldName != null) {
              FieldDisplay fieldDisplay = new FieldDisplay(DSLSupport.composeStringList(fieldName));
              ruleset.addField(fieldDisplay);

              JSArray jsRules = (JSArray)jsValidation.get("display");
              if (jsRules != null) {
                for (Object objRule : jsRules) {
                  if (objRule instanceof JSFunctionCall) {
                    JSFunctionCall jsFunc = (JSFunctionCall)objRule;
                    try {
                      DisplayRule rule = loadDisplayRule(mapping, scope, jsFunc);
                      if (rule != null) {
                        fieldDisplay.addDisplay(rule);
                      }
                    } catch (Exception e) {
                      logger.info("Error loading rule ", e);
                      System.out.println("Error loading rule '"+ obj.toString() +"' on field '" + fieldName.toString() + "': " + e.getMessage());
                    }
                  }
                }
              }
            }
          }
        }
      }
      ruleSets.add(ruleset);
    }
  }

  private void doExtends(DisplayRuleSet ruleset){

    StringList nameList = new StringList();
    HashMap<String,FieldDisplay> ruleMap = new HashMap<String,FieldDisplay>();
      for (FieldDisplay fieldEntry : ruleset.getFields()) {

        for (String field : fieldEntry.getFieldList()) {
          if (!nameList.contains(field)) { // This is the primary override mechanism for display rules...
            nameList.add(field);
            ruleMap.put(field,fieldEntry);
          }else{
            // the order is serial, so we can just carry on until there is no more "EXTEND" rule.
            FieldDisplay prevRule = ruleMap.get(field);
            Boolean extend = false;
            int extendPos = 0;
            List<DisplayRule> displays = prevRule.getDisplay();

            for(DisplayRule rule: displays){
              if(rule.getType()==DisplayRuleType.Extend){
                extend = true;
                break;
              }
              extendPos++;
            }

            if(extend){
              // sandwich in the rules (Leaves take precedence...)
              int i;
              List<DisplayRule> newDisplays = new ArrayList<DisplayRule>();
              for( i =0; i< extendPos; i++){
                newDisplays.add(displays.get(i));
              }
              for(DisplayRule rule: fieldEntry.getDisplay()){
                newDisplays.add(rule);
              }
              for( i = extendPos+1; i< displays.size(); i++){
                newDisplays.add(displays.get(i));
              }
              prevRule.setDisplay(newDisplays);

              ruleMap.put(field,fieldEntry);// store this rule to pass down chain.
            }
          }
        }

      }
  }

  private void insertDisplayRules(List<Display> rulelist, String fieldName, DisplayRuleSet ruleset, DisplayRule rule) {
    rulelist.add(new Display(ruleset.getScope(), fieldName, rule.getType(), rule.getField(), rule.getValues(),
            rule.getDisplayFunction(), rule.getAssertion(), rule.getCategories(), rule.getNotCategories()));
  }

  public List<Display> displayRulesFromRuleSets(FlowType flow, String section,
                                       List<String> categories, List<String> mainCategories, String filter) {
    List<Display> result = new ArrayList<Display>();
    for (DisplayRuleSet ruleset : ruleSets) {
      StringList nameList = new StringList();
      HashMap<String,FieldDisplay> ruleMap = new HashMap<String,FieldDisplay>();
      for (FieldDisplay fieldEntry : ruleset.getFields()) {
        StringList fieldNames = new StringList();
        String sectionField;

        for (String field : fieldEntry.getFieldList()) {
          sectionField = field + '.' + section;
          if (!nameList.contains(sectionField)) { // This is the primary override mechanism for display rules...
            nameList.add(sectionField);
            fieldNames.add(field);
          }
        }

        for (String fieldName : fieldNames) {
          for (DisplayRule displayRule : fieldEntry.getDisplay()) {
            int i;

            if (flow != null && displayRule.getFlow() != null && !displayRule.getFlow().equals(flow))
              continue;

            if(section == null || section.isEmpty()){
              if(displayRule.getSection() != null)
                continue;
            }
            if (section != null && displayRule.getSection() != null && !displayRule.getSection().contains(section))
              continue;

            if (categories != null && displayRule.getCategories() != null) {
              boolean hasCategory = false;
              for (i = 0; i < categories.size(); i++) {
                if (displayRule.getCategories().indexOf(categories.get(i)) >= 0) {
                  hasCategory = true;
                  break;
                }
              }
              if (!hasCategory) {
                for (i = 0; i < mainCategories.size(); i++) {
                  if (displayRule.getCategories().indexOf(mainCategories.get(i)) >= 0) {
                    hasCategory = true;
                    break;
                  }
                }
              }
              if (!hasCategory)
                continue;
            }
            if (categories != null && displayRule.getNotCategories() != null) {
              if (!DSLSupport.categoriesNotInList(categories, mainCategories, displayRule.getNotCategories()))
                continue;
            }

            if (filter != null && !filter.equals(fieldName))
              continue;

            insertDisplayRules(result, fieldName, ruleset, displayRule);
          }
        }
      }
    }

    return result;
  }
}

