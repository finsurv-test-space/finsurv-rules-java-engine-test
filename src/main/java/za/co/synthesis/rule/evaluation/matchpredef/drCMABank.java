package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.EvaluationUtil;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;

public class drCMABank implements IMatchAssertionFunction {
  @Override
  public boolean execute(MatchInfo matchInfo) {
    String BIC = matchInfo.getDrBankBIC();

    return EvaluationUtil.isCMAByBIC(matchInfo.getContext(), BIC);
  }
}