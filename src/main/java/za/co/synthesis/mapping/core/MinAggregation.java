package za.co.synthesis.mapping.core;

import java.util.List;

/**
 * User: jake
 * Date: 3/21/16
 * Time: 11:58 AM
 * Find the minimum value in the set of values
 */
public class MinAggregation implements IAggregation {
  @Override
  public String aggregate(List<String> values) {
    if (values.size() > 0) {
      String minStrValue = null;
      try {
        double minValue = 0.0D;
        for (String valStr : values) {
          if (valStr.length() > 0) {
            double valNum = Double.parseDouble(valStr);
            if (minStrValue == null || valNum < minValue) {
              minValue = valNum;
              minStrValue = valStr;
            }
          }
        }
      }
      catch (NumberFormatException e) {
        minStrValue = values.get(0);
        for (String valStr : values) {
          if (valStr.compareTo(minStrValue) < 0) {
            minStrValue = valStr;
          }
        }
      }
      return minStrValue;
    }
    return null;
  }
}

