package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.Context;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;

public class drThisBank implements IMatchAssertionFunction {
  @Override
  public boolean execute(MatchInfo matchInfo) {
    String BIC = matchInfo.getDrBankBIC();

    if (BIC != null) {
      return BIC.equals(Context.THISBANK) ||  BIC.matches(matchInfo.getContext().getWhoAmIRegex());
    }
    return false;
  }
}