package za.co.synthesis.rule.core.impl;

import za.co.synthesis.rule.core.ICustomValue;

import java.util.HashMap;
import java.util.Map;

/**
 * User: jake
 * Date: 8/11/14
 * Time: 4:20 PM
 * Contains the basic custom values like DealerType and AccountFlow
 */
public class DefaultCustomValues implements ICustomValue {
  protected final Map<String, Object> values = new HashMap<String, Object>();

  @Override
  public Object get(String field) {
    return values.get(field);
  }

  public void setDealerType(String value) {
    values.put("DealerType", value);
  }

  public String getDealerType() {
    return (String)values.get("DealerType");
  }

  public void setAccountFlow(String value) {
    values.put("AccountFlow", value);
  }

  public String getAccountFlow() {
    return (String)values.get("AccountFlow");
  }
}
