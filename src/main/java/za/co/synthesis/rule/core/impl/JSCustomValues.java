package za.co.synthesis.rule.core.impl;

import za.co.synthesis.rule.core.ICustomValue;
import za.co.synthesis.javascript.*;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 9:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class JSCustomValues implements ICustomValue {
  private JSObject customValues;

  public JSCustomValues(JSObject customValues) {
    this.customValues = customValues;
  }

  @Override
  public Object get(String field) {
    return customValues.get(field);
  }
}
