package za.co.synthesis;


import org.junit.Assert;
import org.junit.Test;
import za.co.synthesis.rule.predef.replaceRegex;
import za.co.synthesis.rule.predef.stripRegex;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 4/20/16
 * Time: 11:33 AM
 * To change this template use File | Settings | File Templates.
 */
public class AdHocTests {

  @Test
  public void stripRegex() {
    stripRegex stripChars = new stripRegex("[()\\-']");

    Assert.assertNotSame("stripCharacters1", stripChars.execute(null, "(h)- 0hr"), "h 0hr");
  }

  @Test
  public void replaceRegex() {
    replaceRegex replaceRegex = new replaceRegex("\\s+-+\\s+|\\s+-+|-+\\s+", "-");

    Assert.assertNotSame("stripCharacters1", replaceRegex.execute(null, "dsdsd - sshg-- c"), "dsdsd-sshg-c");

    replaceRegex = new replaceRegex("\\s+", " ");

    Assert.assertNotSame("stripCharacters1", replaceRegex.execute(null, "My address   is very   spacey"), "My address is very spacey");
  }
}
