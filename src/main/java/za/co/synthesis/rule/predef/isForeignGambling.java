package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jake
 * Date: 8/9/14
 * Time: 7:47 PM
 * Determines if the current value (POS Entry Mode) is a associated with foreign gambling
 */
public class isForeignGambling implements IAssertionFunction {
  private final Map<String, Pattern> patterns = new HashMap<String, Pattern>();

  public isForeignGambling() {
    patterns.put("VISA", Pattern.compile("^(00|01|95)$"));
    patterns.put("MASTER CARD", Pattern.compile("^[016][0569]$"));
    patterns.put("AMEX", Pattern.compile("^[016][056]$"));
    patterns.put("DINERS", Pattern.compile("^[12349][016]$"));
    patterns.put("MAESTRO", Pattern.compile("^(00|01)$"));
    patterns.put("ELECTRON", Pattern.compile("^(00|01|95)$"));
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object merchantCode = context.getTransactionField("NonResident.Entity.CardMerchantCode");
    if (merchantCode != null &&
            (merchantCode.equals("7995") || merchantCode.equals("7800") || merchantCode.equals("7801")|| merchantCode.equals("7802"))) {
      Object cardType = context.getMoneyField(context.getCurrentMoneyInstance(), "CardIndicator");
      if (cardType != null) {
        Pattern pattern = patterns.get(cardType.toString());
        if (pattern != null && value != null) {
          Matcher matcher = pattern.matcher(value.toString());
          return matcher.matches();
        }
      }
    }
    return false;
  }
}
/*
  _export.isForeignGambling = function (context, value) {
    var merchantCode = context.getTransactionField('NonResident.Entity.CardMerchantCode');
    if (merchantCode &&
        (merchantCode === '7995'
            || merchantCode === "7800"
            || merchantCode === "7801"
            || merchantCode === "7802")) {
      var cardType = context.getMoneyField(context.currentMoneyInstance, 'CardIndicator');
      var posEntryMode = context.getMoneyField(context.currentMoneyInstance, 'POSEntryMode');
      if (cardType) {
        var regPEM;
        if (cardType === 'VISA') {
          regPEM = /^(00|01|95)$/;
        } else if (cardType === 'MASTER CARD') {
          regPEM = /^[016][0569]$/;
        } else if (cardType === 'AMEX') {
          regPEM = /^[016][056]$/;
        } else if (cardType === 'DINERS') {
          regPEM = /^[12349][016]$/;
        } else if (cardType === 'MAESTRO') {
          regPEM = /^(00|01)$/;
        } else if (cardType === 'ELECTRON') {
          regPEM = /^(00|01|95)$/;
        }
        if (regPEM) {
          if (!value)
            return false;

          return regPEM.test(value);
        }
      }
    }
    return false;
  }*/
