package za.co.synthesis.rule.classification;

import za.co.synthesis.rule.core.ClassificationResult;
import za.co.synthesis.rule.core.DrCr;
import za.co.synthesis.rule.core.IClassificationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jake on 6/7/16.
 */
public class ClassificationContext {
  private static class ObjectMap extends HashMap<String, Object> {
    private ObjectMap(int i, float v) {
      super(i, v);
    }

    private ObjectMap(int i) {
      super(i);
    }

    private ObjectMap() {
    }
  }

  private IClassificationContext context;
  private final List<ClassificationResult> classificationResultList = new ArrayList<ClassificationResult>();
  private final DrCr drcr;

  private final ObjectMap classificationCache = new ObjectMap(50);

  public ClassificationContext(IClassificationContext context) {
    this.context = context;

    Object value = context.getField("DrCr");
    if (value != null) {
      String drCrStr = value.toString().toUpperCase();
      if (drCrStr.equals("D"))
        this.drcr = DrCr.DR;
      else if (drCrStr.equals("C"))
        this.drcr = DrCr.CR;
      else
        this.drcr = null;
    }
    else
      this.drcr = null;
  }

  public DrCr getDrCr() {
    return drcr;
  }

  public String getDrCrAsString() {
    if (drcr == DrCr.DR) {
      return "D";
    }
    else if (drcr == DrCr.CR) {
      return "C";
    }
    else {
      return "-";
    }
  }

  public List<ClassificationResult> getClassificationResultList() {
    return classificationResultList;
  }

  public boolean hasField(Object obj, final String field) {
    return context.hasField(obj, field);
  }

  public Object getField(String field) {
    if (classificationCache.containsKey(field))
      return classificationCache.get(field);
    else {
      Object value = context.getField(field);
      classificationCache.put(field, value);
      return value;
    }
  }

  public void logEvent(String type, String ruleName, String fieldName) {
    classificationResultList.add(new ClassificationResult(type, ruleName, fieldName));

    context.logEvent(type, ruleName, fieldName);
  }
}
