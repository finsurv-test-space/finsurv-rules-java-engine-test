package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 9/6/16.
 */
public class isValidBranchName implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value != null && value instanceof String) {
      String branchCode = (String)context.getTransactionField("BranchCode");
      Map<String, String> key = new HashMap<String, String>();
      key.put("code", branchCode);
      String branchName = context.getLookups().getLookupField("branches", key, "name");
      if (value.equals(branchName))
        return true;
    }
    return false;
  }
}

/*
  _export.isValidBranchName = function (context, value) {
    var branchCode = context.getTransactionField("BranchCode");
    if (branchCode && context.lookups.lookups.branches) {
      var i;
      var branch;
      for (i = 0; i < context.lookups.lookups.branches.length; i++) {
        branch = context.lookups.lookups.branches[i];
        if (branch.code === branchCode) {
          if (branch.name === value)
            return true;
        }
      }
    }
    return false;
  }
*/
