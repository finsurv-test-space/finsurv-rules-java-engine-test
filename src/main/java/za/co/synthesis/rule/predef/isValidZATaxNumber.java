package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

/**
 * Created by jake on 1/24/17.
 */
public class isValidZATaxNumber implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return value != null && value instanceof String && Util.isValidZATaxNumber((String) value);
  }
}