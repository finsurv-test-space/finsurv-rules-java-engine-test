package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 2:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class isExceptionName implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return false;
    String valStr = value.toString();
    return context.getLookups().isNonResExceptionName(valStr.toUpperCase()) || context.getLookups().isResExceptionName(valStr.toUpperCase());
  }
}
/*
  _export.isExceptionName = function (context, value) {
    if (!value)
      return false;
    return context.lookups.isNonResExceptionName(value.toUpperCase()) || context.lookups.isResExceptionName(value.toUpperCase());
  }
*/