package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.Util;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 3:15 PM
 * Determines if the current value is not a valid UCR
 */
public class notValidUCR implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !Util.isValidUCR(value.toString());
  }
}

/*
function notValidUCR(context, value) {
  if (!value)
    return true;
  if (value.length < 12 || value.length > 35)
    return true;
  var regUCR = /^[0-9]ZA[0-9]{8}.+$/;
  return (regUCR.test(value) == false);
}
*/