package za.co.synthesis.mapping.configuration;

import za.co.synthesis.javascript.*;

import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 11:30 AM
 * This is the main object that stores all the mapping configurations defined in a single mapping file
 */
public class MappingConfig {
  private String mappingFileName;
  private List<Source> sources = new ArrayList<Source>();
  private List<Section> sections = new ArrayList<Section>();
//  private List<Field> fields = new ArrayList<Field>();

  public static MappingConfig load(String mappingFileName) throws Exception {
    MappingConfig result = new MappingConfig();
    result.mappingFileName = mappingFileName;

    FileReader fileReader = new FileReader(mappingFileName);
    JSStructureParser parser = new JSStructureParser(fileReader);

    Object obj = parser.parse();

    if (obj instanceof JSObject) {
      JSObject mapping = (JSObject)((JSObject)obj).get("mapping");
      return setup(result, mapping);
    }
    return null;
  }

  public static MappingConfig load(final String name, final Reader reader) throws Exception {
    MappingConfig result = new MappingConfig();
    result.mappingFileName = name;

    JSStructureParser parser = new JSStructureParser(reader);

    Object obj = parser.parse();

    if (obj instanceof JSObject) {
      JSObject mapping = (JSObject)((JSObject)obj).get("mapping");
      return setup(result, mapping);
    }
    return null;
  }

  private static MappingConfig setup(MappingConfig config, JSObject jsMapping) {
    JSObject jsSrcMeta = (JSObject)jsMapping.get("src");
    for(Map.Entry<String, Object> entry : jsSrcMeta.entrySet()) {
      if (entry.getValue() instanceof JSObject) {
        config.sources.add(Source.setup(entry.getKey(), (JSObject)entry.getValue()));
      }
    }

    JSArray jsDestMeta = (JSArray)jsMapping.get("dest");
    for (Object section : jsDestMeta) {
      if (section instanceof JSObject) {
        config.sections.add(Section.setup((JSObject) section));
      }
    }

//    for (Section section : config.sections) {
//      config.fields.addAll(section.getFields());
//    }
    return config;
  }

  public String getMappingFileName() {
    return mappingFileName;
  }

  public List<Source> getSources() {
    return sources;
  }

  public List<Section> getSections() {
    return sections;
  }

//  public List<Field> getFields() {
//    return fields;
//  }
}
