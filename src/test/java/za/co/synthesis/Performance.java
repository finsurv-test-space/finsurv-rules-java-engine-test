package za.co.synthesis;

import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.core.impl.JSCustomValues;
import za.co.synthesis.rule.evaluation.Evaluation;
import za.co.synthesis.rule.support.JSConstant;
import za.co.synthesis.rule.support.Util;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 8/10/14
 * Time: 10:07 AM
 * A small class to test performance of the Java rules engine
 */
public class Performance {
  private static class Test_ValidateRQ implements ICustomValidate {
    @Override
    public CustomValidateResult call(Object value, Object... otherInputs) {
      String flow;
      if (otherInputs.length > 0)
        flow = otherInputs[0].toString();
      else
        flow = "";

      if (flow.equals("IN") && value.equals("TESTIN")) {
        return new CustomValidateResult(StatusType.Pass);
      }
      else {
        return new CustomValidateResult(StatusType.Fail, "XXX", "Can only pass inflow TESTIN reports");
      }
    }
  }

  public static void perfomanceEvaluations(int count) throws Exception {
    EvaluationEngine engine = new EvaluationEngine();

    engine.setupLoadRuleFile("../finsurv-rules/evalRules.js");

    FileReader fileReader = new FileReader("../finsurv-rules/testEvalCases.js");
    JSStructureParser parser = new JSStructureParser(fileReader);

    Object obj = parser.parse();

    List<Evaluation> evaluations = new ArrayList<Evaluation>();

    Object testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
    if (testCaseContainer instanceof JSObject) {
      JSArray cases = (JSArray)((JSObject)testCaseContainer).get("test_cases");

      for (Object objCase : cases) {
        if (objCase instanceof JSFunctionCall) {
          JSFunctionCall jsFunc = (JSFunctionCall)objCase;

          if (jsFunc.getName().equals("assert") && jsFunc.getParameters().size() == 6) {
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(1));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(2));
            ResidenceStatus crResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(3));
            BankAccountType crAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(4));
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(5);
            evaluations.add(new Evaluation(drResStatus, drAccType, crResStatus, crAccType, null, null, null));
          }
        }
      }
    }

    Evaluator evaluator = engine.issueEvaluator();

    int evalCount = 0;
    long startMillis = System.currentTimeMillis();
    for (int i=0; i<count; i++) {
      int data_index = i % evaluations.size();

      Evaluation eval = evaluations.get(data_index);
      evaluator.evaluate(eval.getDrResStatus(), eval.getDrAccType(), eval.getCrResStatus(), eval.getCrAccType());
      evalCount++;
    }
    long endMillis = System.currentTimeMillis();

    double tps = (double)evalCount*1000/(endMillis-startMillis);
    System.out.println("Evaluations: " + evalCount + ", in " + (endMillis-startMillis) + " millis. tp/s: " + (long)tps);
  }

  public static void main( String[] args ) throws Exception {

    //perfomanceEvaluations(1000000);


    ValidationEngine engine = new ValidationEngine();

    engine.setupLoadLookupFile("../finsurv-rules/lookups.js");
    engine.setupLoadRuleFile("../finsurv-rules/transaction.js");
    engine.setupLoadRuleFile("../finsurv-rules/money.js");
    engine.setupLoadRuleFile("../finsurv-rules/importexport.js");

    DefaultSupporting supporting = new DefaultSupporting();
    supporting.setCurrentDate(Util.date("2013-01-20"));
    supporting.setGoLiveDate(Util.date("2013-08-18"));
    engine.setupSupporting(supporting);

    engine.registerCustomValidate("Test_ValidateRQ", new Test_ValidateRQ(), "Flow");

    FileReader fileReader = new FileReader("./src/test/resources/transaction_data.js");
    JSStructureParser parser = new JSStructureParser(fileReader);

    Object obj = parser.parse();

    JSArray data = null;
    if (obj instanceof JSObject) {
      data = (JSArray) ((JSObject) obj).get("data");
    }
    else
    if (obj instanceof JSArray) {
      data = (JSArray)obj;
    }

    if (data != null) {
      Validator validator = engine.issueValidator();
      JSObject customData = new JSObject();
      customData.put("DealerType", "AD");
      customData.put("AccountFlow", "IN");
      JSCustomValues customValues = new JSCustomValues(customData);

      int validationCount = 0;
      long startMillis = System.currentTimeMillis();
      for (int i = 0; i < 50000; i++) {
        System.out.println(i);
        int data_index = i % data.size();

        Object transaction = data.get(data_index);
        if (transaction instanceof JSObject) {
          //Object transaction2 = ((JSObject)transaction).get("transaction");

          if (transaction instanceof JSObject) {
            ValidationStats stats = new ValidationStats();
            validator.validate((JSObject) transaction, customValues, stats);
            validationCount++;
          }
        }
      }
      long endMillis = System.currentTimeMillis();

      double tps = (double) validationCount * 1000 / (endMillis - startMillis);
      System.out.println("Validations: " + validationCount + ", in " + (endMillis - startMillis) + " millis. tp/s: " + (long) tps);
    }
  }
}
