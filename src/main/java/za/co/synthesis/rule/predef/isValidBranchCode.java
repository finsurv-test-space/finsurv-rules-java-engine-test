package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 9/6/16.
 */
public class isValidBranchCode implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value != null && value instanceof String) {
      String branchCode = (String) value;
      Map<String, String> key = new HashMap<String, String>();
      key.put("code", branchCode);
      String lookupCode = context.getLookups().getLookupField("branches", key, "code");
      if (branchCode.equals(lookupCode))
        return true;
    }
    return false;
  }
}

/*
  _export.isValidBranchCode = function (context, value) {
    var branchCode = value;
    if (context.lookups.lookups.branches) {
      var i;
      var branch;
      for (i = 0; i < context.lookups.lookups.branches.length; i++) {
        branch = context.lookups.lookups.branches[i];
        if (branch.code === branchCode) {
          return true;
        }
      }
    }
    return false;
  }
*/