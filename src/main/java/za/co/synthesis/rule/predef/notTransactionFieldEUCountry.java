package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IBAN;
import za.co.synthesis.rule.support.IStringResolver;

public class notTransactionFieldEUCountry implements IAssertionFunction {
  private IStringResolver constant;

  public notTransactionFieldEUCountry(IStringResolver constant) {
    this.constant = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField(constant.resolve(context, value));
    if (fieldValue != null && fieldValue instanceof String) {
      return !IBAN.IsEUCountry((String)fieldValue);
    }
    return true;
  }
}
