package za.co.synthesis.rule.predef;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;

public class isInMetaLookup implements IAssertionFunction {
    private String lookupName;
    private String lookupKey;

    public isInMetaLookup(String lookupName, String lookupKey) {
        this.lookupName = lookupName;
        this.lookupKey = lookupKey;
    }

    public boolean execute(FinsurvContext context, Object value) {
        if (value instanceof String) {
            Object lookupValue = context.getCustomValue().get(lookupName);
            if (lookupValue instanceof JSObject) {
                return (value.equals(((JSObject) lookupValue).get(lookupKey)));
            }
        }
        return false;
    }
}
