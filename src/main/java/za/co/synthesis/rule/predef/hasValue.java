package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * User: jake
 * Date: 8/5/14
 * Time: 10:39 PM
 * returns true if the value is equal to the given constant
 */
public class hasValue implements IAssertionFunction {
  private IStringResolver constant;

  public hasValue(IStringResolver constant) {
    this.constant = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (constant == null) {
      return value == null;
    }
    String constantValue = constant.resolve(context, value);
    return (value != null && value.equals(constantValue));
  }
}
/*
function hasValue(constant) {
  return function(context, value) {
    return value && value === constant;
  }
}
*/