package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.ILookups;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 8/11/14
 * Time: 3:36 PM
 * The lookup aggregator allows multiple lookup objects to be amalgamated into a single lookup engine. The way this
 * is done is allow previous lookups to override the previous lookups by returning a non-null value when the method is
 * called.
 */
public class LookupAggregator implements ILookups {
  private final List<ILookups> lookupList = new ArrayList<ILookups>();
  private ILookups validCountryCode;
  private ILookups validCurrencyCode;
  private ILookups validCategory;
  private ILookups validSubCategory;
  private ILookups reportingQualifier;
  private ILookups nonResExceptionName;
  private ILookups resExceptionName;
  private ILookups moneyTransferAgent;
  private ILookups validProvince;
  private ILookups validCardType;
  private ILookups validInstitutionalSector;
  private ILookups validIndustrialClassification;
  private ILookups validCustomsOfficeCode;

  private ILookups alwaysTrueLookup = new AlwaysTrueLookup();

  public LookupAggregator() {
    setLookups(alwaysTrueLookup, true);
  }

  private void setLookups(ILookups lookups, boolean override) {
    String blank = "";

    if (lookups.isValidCountryCode(blank) != null && (override || validCountryCode == alwaysTrueLookup))
      validCountryCode = lookups;
    if (lookups.isValidCurrencyCode(blank) != null && (override || validCurrencyCode == alwaysTrueLookup))
      validCurrencyCode = lookups;
    if (lookups.isValidCategory(blank, blank) != null && (override || validCategory == alwaysTrueLookup))
      validCategory = lookups;
    if (lookups.isValidSubCategory(blank, blank, blank) != null && (override || validSubCategory == alwaysTrueLookup))
      validSubCategory = lookups;
    if (lookups.isReportingQualifier(blank) != null && (override || reportingQualifier == alwaysTrueLookup))
      reportingQualifier = lookups;
    if (lookups.isNonResExceptionName(blank) != null && (override || nonResExceptionName == alwaysTrueLookup))
      nonResExceptionName = lookups;
    if (lookups.isResExceptionName(blank) != null && (override || resExceptionName == alwaysTrueLookup))
      resExceptionName = lookups;
    if (lookups.isMoneyTransferAgent(blank) != null && (override || moneyTransferAgent == alwaysTrueLookup))
      moneyTransferAgent = lookups;
    if (lookups.isValidProvince(blank) != null && (override || validProvince == alwaysTrueLookup))
      validProvince = lookups;
    if (lookups.isValidCardType(blank) != null && (override || validCardType == alwaysTrueLookup))
      validCardType = lookups;
    if (lookups.isValidInstitutionalSector(blank) != null && (override || validInstitutionalSector == alwaysTrueLookup))
      validInstitutionalSector = lookups;
    if (lookups.isValidIndustrialClassification(blank) != null && (override || validIndustrialClassification == alwaysTrueLookup))
      validIndustrialClassification = lookups;
    if (lookups.isValidCustomsOfficeCode(blank) != null && (override || validCustomsOfficeCode == alwaysTrueLookup))
      validCustomsOfficeCode = lookups;
  }

  public void overrideLookups(ILookups lookups) {
    lookupList.add(0, lookups);

    setLookups(lookups, true);
  }

  public void appendLookups(ILookups lookups) {
    lookupList.add(lookups);

    setLookups(lookups, false);
  }

  @Override
  public Boolean isValidCountryCode(String code) {
    return validCountryCode.isValidCountryCode(code);
  }

  @Override
  public Boolean isValidCurrencyCode(String code) {
    return validCurrencyCode.isValidCurrencyCode(code);
  }

  @Override
  public Boolean isValidCategory(String flow, String code) {
    return validCategory.isValidCategory(flow, code);
  }

  @Override
  public Boolean isValidSubCategory(String flow, String code, String subcode) {
    return validSubCategory.isValidSubCategory(flow, code, subcode);
  }

  @Override
  public Boolean isReportingQualifier(String value) {
    return reportingQualifier.isReportingQualifier(value);
  }

  @Override
  public Boolean isNonResExceptionName(String value) {
    return nonResExceptionName.isNonResExceptionName(value);
  }

  @Override
  public Boolean isResExceptionName(String value) {
    return resExceptionName.isResExceptionName(value);
  }

  @Override
  public Boolean isMoneyTransferAgent(String value) {
    return moneyTransferAgent.isMoneyTransferAgent(value);
  }

  @Override
  public Boolean isValidProvince(String value) {
    return validProvince.isValidProvince(value);
  }

  @Override
  public Boolean isValidCardType(String value) {
    return validCardType.isValidCardType(value);
  }

  @Override
  public Boolean isValidInstitutionalSector(String value) {
    return validInstitutionalSector.isValidInstitutionalSector(value);
  }

  @Override
  public Boolean isValidIndustrialClassification(String value) {
    return validIndustrialClassification.isValidIndustrialClassification(value);
  }

  @Override
  public Boolean isValidCustomsOfficeCode(String value) {
    return validCustomsOfficeCode.isValidCustomsOfficeCode(value);
  }

  @Override
  public String getLookupField(final String lookup, final Map<String,String> key, final String fieldName) {
    String result = null;
    for (ILookups lookups : lookupList) {
      result = lookups.getLookupField(lookup, key, fieldName);
      if (result != null)
        break;
    }
    return result;
  }
}
