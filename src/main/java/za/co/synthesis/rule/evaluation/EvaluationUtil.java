package za.co.synthesis.rule.evaluation;

import za.co.synthesis.rule.core.CounterpartyInstituition;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by jake on 9/11/17.
 */
public class EvaluationUtil {
  public static final String EXISTS = "exists";

  public static CounterpartyInstituition getCounterpartyInstituitionByBIC(Context context, String bic) {
    CounterpartyInstituition cptyType = null;
    if (bic != null) {
      if (bic.matches(context.getOnshoreRegex())) {
        cptyType = CounterpartyInstituition.OnshoreOther;
        for (String mask : context.getOnshoreADRegexList()) {
          if (bic.matches(mask)) {
            cptyType = CounterpartyInstituition.OnshoreAD;
            break;
          }
        }
      }
      else {
        cptyType =  CounterpartyInstituition.Offshore;
      }
    }
    return cptyType;
  }

  public static Object getCrValueForMatchField(MatchInfo matchInfo, String fieldName) {
    Object value = matchInfo.getCrAdditionalParams() != null ? matchInfo.getCrAdditionalParams().get(fieldName) : null;
    if (value == null) {
      if (fieldName.equals("BankBIC")) {
        value = matchInfo.getCrBankBIC();
      }
      else
      if (fieldName.equals("Currency")) {
        value = matchInfo.getCrCurrency();
      }
    }
    return value;
  }

  public static Object getDrValueForMatchField(MatchInfo matchInfo, String fieldName) {
    Object value = matchInfo.getDrAdditionalParams() != null ? matchInfo.getDrAdditionalParams().get(fieldName) : null;
    if (value == null) {
      if (fieldName.equals("BankBIC")) {
        value = matchInfo.getDrBankBIC();
      }
      else
      if (fieldName.equals("Currency")) {
        value = matchInfo.getDrCurrency();
      }
      else
      if (fieldName.equals("Field72")) {
        value = matchInfo.getDrField72();
      }
    }
    return value;
  }

  public static boolean compareNumbers(BigDecimal leftValue, String operation, BigDecimal rightValue) {
    if (leftValue != null && rightValue != null) {
      // -1, 0, or 1 as this fieldValue is numerically
      // less than, equal to, or greater than constantValue

      if (operation.equals(">")) {
        return leftValue.compareTo(rightValue) >= 1; // 1
      }
      if (operation.equals(">=")) {
        return leftValue.compareTo(rightValue) >= 0; // 0, 1
      }
      if (operation.equals("<")) {
        return leftValue.compareTo(rightValue) <= -1; // -1
      }
      if (operation.equals("<=")) {
        return leftValue.compareTo(rightValue) <= 0; // 0, -1
      }
      if (operation.equals("=")) {
        return leftValue.compareTo(rightValue) == 0; // 0
      }
      if (operation.equals("!=")) {
        return leftValue.compareTo(rightValue) != 0; // !0
      }
    }
    return false;
  }

  public static boolean compareValues(Object objValue, String operation, List<String> constants) {
    if (operation.equals(EXISTS)) {
      // Check for existence
      return objValue != null;
    }
    else
    if (Util.isNumeric(objValue)) {
      BigDecimal fieldValue = Util.number(objValue);
      if (fieldValue != null && constants.size() == 1) {
        BigDecimal constantValue = Util.number(constants.get(0));
        return EvaluationUtil.compareNumbers(fieldValue, operation, constantValue);
      }
    }
    else
    if (objValue instanceof String) {
      if (operation.equals("=") && constants.size() == 1) {
        return objValue.equals(constants.get(0));
      }
      if (operation.equals("!=") && constants.size() == 1) {
        return !objValue.equals(constants.get(0));
      }
      if (operation.equals("in")) {
        return constants.contains(objValue.toString());
      }
      if (operation.equals("contains")) {
        for (String constant : constants) {
          if (!objValue.toString().contains(constant))
            return false;
        }
        return true;
      }
    }
    return false;
  }

  public static boolean isCMAByBIC(Context context, String bic) {
    boolean result = false;
    if (bic != null) {
      for (String mask : context.getCmaRegexList()) {
        if (bic.matches(mask)) {
          result = true;
          break;
        }
      }
    }
    return result;
  }
}
