package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 12:36 PM
 * Determine if there are one or more monetary amount entries
 */
public class notEmptyMoneyField implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return context.getMoneySize() > 0;
  }
}
/*
function notEmptyMoneyField(context, value) {
  return context.getMoneySize() > 0;
}
*/
