define(['require', './predef'], function (require,  predef) {
  var matching;

  with (predef) {
    matching = {
      ruleset: "Standard Bank Account Entry Matching",
      match: [
        confidence("Exact match (with cents)",
          1.0,
          equals("ValueDate").and(equals("Currency")).and(equals("Amount")).and(equals("DrCr")).
          and(notValue("Currency", ["JPY"])).and(notRounded("Amount")).
          and(sameFirst("TrnRef", 16).or(valueEquals("RelatedRef")).or(valueEquals("SourceRef")))),

        confidence("Exact match (rounded amount)",
          0.99,
          equals("ValueDate").and(equals("Currency")).and(equals("Amount")).and(equals("DrCr")).
          and(sameFirst("TrnRef", 16).or(valueEquals("RelatedRef")).or(valueEquals("SourceRef")))),

        confidence("Ref match, dates similar, amounts equal (with cents)",
          dateVariance("ValueDate", 0, 16, 0.97, 0.4),
          equals("Currency").and(equals("Amount")).and(equals("DrCr")).
          and(notValue("Currency", ["JPY"])).and(notRounded("Amount")).
          and(sameFirst("TrnRef", 16).or(valueEquals("RelatedRef")).or(valueEquals("SourceRef")))),

        confidence("Ref match, dates similar, amounts equal",
          dateVariance("ValueDate", 0, 16, 0.95, 0.3),
          equals("Currency").and(equals("Amount")).and(equals("DrCr")).
          and(sameFirst("TrnRef", 16).or(valueEquals("RelatedRef")).or(valueEquals("SourceRef")))),

        confidence("Ref match, amounts similar, dates equal",
          numberVariance("Amount", 0, 5, 0.94, 0.1),
          equals("ValueDate").and(equals("Currency")).and(equals("DrCr")).
          and(sameFirst("TrnRef", 16).or(valueEquals("RelatedRef")).or(valueEquals("SourceRef")))),

        confidence("Ref match, amounts similar, dates similar",
          numberVariance("Amount", 0, 5, 0.94, 0.1).multiply(dateVariance("ValueDate", 0, 16, 0.95, 0.3)),
          equals("Currency").and(equals("DrCr")).
          and(sameFirst("TrnRef", 16).or(valueEquals("RelatedRef")).or(valueEquals("SourceRef")))),

        confidence("Amount (with cents) and date match (diff refs)",
          0.7,
          equals("ValueDate").and(equals("Currency")).and(equals("Amount")).and(equals("DrCr")).
          and(notValue("Currency", ["JPY"])).and(notRounded("Amount"))),

        confidence("Amount (rounded) and date match (diff refs)",
          0.6,
          equals("ValueDate").and(equals("Currency")).and(equals("Amount")).and(equals("DrCr")).
          and(rounded("Amount"))),

        confidence("Amounts and dates similar (diff refs)",
          numberVariance("Amount", 0, 5, 0.9, 0.1).multiply(dateVariance("ValueDate", 0, 15, 0.9, 0.1)),
          equals("Currency").and(equals("DrCr")))
      ],
      buckets : [
        {
          name: "FineMesh",
          shard: value("Currency").then(numberSpan("Amount", 6.0)).then(value("DrCr")).then(dateSpan("ValueDate", 16.0))
        }
      ]
    }
  }
  return matching;
});
