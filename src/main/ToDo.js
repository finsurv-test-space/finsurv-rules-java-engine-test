

function isForeignGambling(context, value) {
  var merchantCode = context.getTransactionField('NonResident.Entity.CardMerchantCode');
  if (merchantCode && merchantCode === '7995') {
    var cardType = context.getMoneyField(context.currentMoneyInstance, 'CardIndicator');
    var posEntryMode = context.getMoneyField(context.currentMoneyInstance, 'POSEntryMode');
    if (cardType) {
      var regPEM;
      if (cardType === 'VISA') {
        regPEM = /^(00|01|95)$/;
      } else if (cardType === 'MASTER CARD') {
        regPEM = /^[016][0569]$/;
      } else if (cardType === 'AMEX') {
        regPEM = /^[016][056]$/;
      } else if (cardType === 'DINERS') {
        regPEM = /^[12349][016]$/;
      } else if (cardType === 'MAESTRO') {
        regPEM = /^(00|01)$/;
      } else if (cardType === 'ELECTRON') {
        regPEM = /^(00|01|95)$/;
      }
      if (regPEM) {
        if (!value)
          return false;

        return regPEM.test(value);
      }
    }
  }
  return false;
}


