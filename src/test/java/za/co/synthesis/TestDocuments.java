package za.co.synthesis;

import org.junit.Test;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.core.impl.JSCustomValues;
import za.co.synthesis.rule.support.RulePackage;
import za.co.synthesis.rule.support.Util;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 3:16 PM
 * Runs all the JavaScript test cases
 */
public class TestDocuments {
  List<String> testRuleNames = new ArrayList<String>();
  int testCount = 0;
  int errorCount = 0;

  private static class ExpectedDoc {
    private int money;
    private int importexport;
    private String name;

    public ExpectedDoc(int money, int importexport, String name) {
      this.money = money;
      this.importexport = importexport;
      this.name = name;
    }

    public ExpectedDoc(int money, String name) {
      this.money = money;
      this.importexport = -1;
      this.name = name;
    }

    public ExpectedDoc(String name) {
      this.money = -1;
      this.importexport = -1;
      this.name = name;
    }

    public int getMoney() {
      return money;
    }

    public int getImportexport() {
      return importexport;
    }

    public String getName() {
      return name;
    }

    public boolean compare(ResultEntry result) {
      if (result.getScope() == Scope.Transaction && result.getCode().equals(name)) {
        return true;
      }
      if (result.getScope() == Scope.Money &&
              ((money == -1 && result.getMoneyInstance() == 0) || money == result.getMoneyInstance()+1) &&
              result.getCode().equals(name) ) {
        return true;
      }
      if (result.getScope() == Scope.ImportExport &&
              ((money == -1 && result.getMoneyInstance() == 0) || money == result.getMoneyInstance()+1) &&
              ((importexport == -1 && result.getImportExportInstance() == 0) || importexport == result.getImportExportInstance()+1) &&
              result.getCode().equals(name) ) {
        return true;
      }
      return false;
    }
  }

  private int countRuleInstances(String rulename) {
    int count = 0;
    for (String name : testRuleNames) {
      if (name.equals(rulename)) {
        count++;
      }
    }
    return count;
  }

  private String composeDocs(List<ExpectedDoc> items) {
    StringBuilder result = new StringBuilder();
    result.append("[");
    boolean bFirst = true;
    for (ExpectedDoc item : items) {
      if (bFirst) {
        bFirst = false;
        result.append(item.getName());
      }
      else {
        result.append(", ").append(item.getName());
      }
    }
    result.append("]");
    return result.toString();
  }

  private String composeResults(List<ResultEntry> items) {
    StringBuilder result = new StringBuilder();
    result.append("[");
    boolean bFirst = true;
    for (ResultEntry item : items) {
      if (bFirst) {
        bFirst = false;
      }
      else {
        result.append(", ");
      }
      if (item.getScope() == Scope.Money)
        result.append(item.getMoneyInstance()+1).append(":");
      if (item.getScope() == Scope.ImportExport)
        result.append(item.getMoneyInstance()+1).append(":").append(item.getImportExportInstance()+1).append(":");
      result.append(item.getCode());
    }
    result.append("]");
    return result.toString();
  }

  private boolean compare(List<ResultEntry> results, List<ExpectedDoc> expected) {
    List<ResultEntry> matchedResults = new ArrayList<ResultEntry>();
    List<ExpectedDoc> matchedExpected = new ArrayList<ExpectedDoc>();

    for (ExpectedDoc doc : expected) {
      for (ResultEntry result : results) {
        if (!matchedResults.contains(result)) {
          if (doc.compare(result)) {
            matchedResults.add(result);
            matchedExpected.add(doc);
          }
        }
      }
    }

    boolean pass;
    if (matchedExpected.size() == expected.size() && matchedResults.size() == results.size()) {
      pass = true;
    }
    else {
      pass = false;
    }
    return pass;
  }

  private void assertRuleBase(Validator validator, String rulename, JSObject jsObj, JSObject jsCustomData,
                              List<ExpectedDoc> expectedDocuments) {
    testRuleNames.add(rulename);
    testCount++;

    ValidationStats stats = new ValidationStats();
    List<ResultEntry> fullResults = validator.validate(jsObj, new JSCustomValues(jsCustomData), stats);
    List<ResultEntry> results = new ArrayList<ResultEntry>();
    for (ResultEntry entry : fullResults) {
      if (entry.getType() == ResultType.Document)
        results.add(entry);
    }

    if (expectedDocuments.size() == 0) {
      if (results.size() > 0) {
        errorCount++;
        System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting no documents, but raised " + composeResults(results));
      }
    }
    else {
      if (!compare(results, expectedDocuments)) {
        errorCount++;
        System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting " + composeDocs(expectedDocuments) + ", but instead got " + composeResults(results) );
      }
    }

    if (rulename.endsWith("+")) {
      // The + suffix tells us to print out the details of the specific test (verbode mode)
      StringBuilder sb = new StringBuilder();
      JSWriter writer = new JSWriter();
      writer.setIndent("  ");
      writer.setNewline("\n");
      jsObj.compose(writer);

      sb.append("---- Verbose output for ").append(rulename).append(" ----").append("\n");
      sb.append(writer.toString());
      sb.append("\n---\n");

      for (ResultEntry item : results) {
        if (item.getScope() == Scope.Money)
          sb.append(item.getMoneyInstance()+1).append(":");
        if (item.getScope() == Scope.ImportExport)
          sb.append(item.getMoneyInstance()+1).append(":").append(item.getImportExportInstance()+1).append(":");
        sb.append(item.getCode());
        sb.append(": ");
        sb.append(item.getMessage());
        sb.append("\n");
      }
      System.out.print(sb.toString());
    }
  }

  private void assertDocuments(Validator validator, String rulename, JSObject jsObj, List<ExpectedDoc> expectedDocuments) {
    assertDocuments(validator, rulename, jsObj, expectedDocuments, null);
  }

  private void assertDocuments(Validator validator, String rulename, JSObject jsObj, List<ExpectedDoc> expectedDocuments, JSObject jsMeta) {
    JSObject customData = new JSObject();
    if (jsMeta instanceof Map) {
      customData.putAll(jsMeta);
    }
    customData.put("DealerType", "AD");
    assertRuleBase(validator, rulename, jsObj, customData, expectedDocuments);
  }


  private String[] getPackages(String path) {
    File file = new File(path);
    String[] directories = file.list(new FilenameFilter() {
      @Override
      public boolean accept(File current, String name) {
        return new File(current, name).isDirectory();
      }
    });
    return directories;
  }

/*
assertDocuments(["1:OriginalTransaction"], {
         ReportingQualifier: 'BOPCUS', Flow: 'OUT',
         MonetaryAmount: [{CategoryCode: '100'}]
        })
*/
  private ExpectedDoc getDoc(String docStr) {
    int money = -1;
    int importexport = -1;
    String name = "";

    int posColon1 = docStr.indexOf(":");
    if (posColon1 > -1) {
      money = Integer.parseInt(docStr.substring(0, posColon1));
      posColon1++;
      int posColon2 = docStr.indexOf(":", posColon1);
      if (posColon2 > -1) {
        importexport = Integer.parseInt(docStr.substring(posColon1, posColon2));
        posColon2++;
        name = docStr.substring(posColon2);
      }
      else {
        name = docStr.substring(posColon1);
      }
    }
    else {
      name = docStr;
    }

    return new ExpectedDoc(money, importexport, name);
  }

  private List<ExpectedDoc> getDocList(Object obj) {
    List<ExpectedDoc> result = new ArrayList<ExpectedDoc>();
    if (obj instanceof String) {
      result.add(getDoc((String)obj));
    }
    else
    if (obj instanceof List) {
      for(Object objItem : (List)obj) {
        if (objItem instanceof String) {
          result.add(getDoc((String)objItem));
        }
      }
    }
    return result;
  }

  private boolean applyAssertionFunction(Validator validator, JSFunctionCall jsFunc) {
    String rulename = jsFunc.getParameters().get(0).toString();

    if (jsFunc.getName().equals("assertDocuments") && jsFunc.getParameters().size() == 3) {
      List<ExpectedDoc> docList = getDocList(jsFunc.getParameters().get(1));
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(2);
      assertDocuments(validator, rulename, jsObj, docList);
    }
    else if (jsFunc.getName().equals("assertDocuments") && jsFunc.getParameters().size() == 4) {
      List<ExpectedDoc> docList = getDocList(jsFunc.getParameters().get(1));
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(2);
      JSObject jsMeta = (JSObject)jsFunc.getParameters().get(3);
      assertDocuments(validator, rulename, jsObj, docList, jsMeta);
    }
    else {
      System.out.println("Unknown assertion function '" + jsFunc.getName() + "' with " + jsFunc.getParameters().size() + " parameters");
      return false;
    }
    return true;
  }

  private void runPackageTestCases(String packagePath) throws Exception {
    ValidationEngine engine = new ValidationEngine();

    // Package:
    String testLookups = packagePath + "/tests/testLookups.js";
    File file = new File(testLookups);
    if (file.exists() && file.isFile()) {
      engine.setupLoadLookupFile(testLookups);
    }
    engine.setupLoadPackagePath(packagePath, false, true);
    DefaultSupporting supporting = new DefaultSupporting();
    supporting.setCurrentDate(Util.date("2013-01-20"));
    supporting.setGoLiveDate(Util.date("2013-08-18"));
    engine.setupSupporting(supporting);

    engine.setIgnoreMissingCustomValidate(true);

    FileReader fileReader = new FileReader(packagePath + "/tests/testDocCases.js");
    JSStructureParser parser = new JSStructureParser(fileReader);
    Object obj = parser.parse();

    // Load specific test mappings if they have been provided as part of the testCases
    RulePackage rulePackage = engine.getLoadedPackage();
    if (rulePackage != null) {
      JSObject jsResult = JSUtil.findObjectWith(obj, "mappings");
      if (jsResult != null) {
        Object objMappings = jsResult.get("mappings");
        if (objMappings instanceof JSObject) {
          for (Map.Entry<String, Object> entry : ((JSObject) objMappings).entrySet()) {
            rulePackage.getMapping().put(entry.getKey(), entry.getValue() != null ? entry.getValue().toString() : null);
          }
        }
      }
    }

    // Iterate over the testCases
    JSObject testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
    if (testCaseContainer != null) {
      Validator validator = engine.issueValidator();

      JSArray cases = (JSArray) testCaseContainer.get("test_cases");

      for (Object objCase : cases) {
        if (objCase instanceof JSFunctionCall) {
          JSFunctionCall jsFunc = (JSFunctionCall) objCase;

          applyAssertionFunction(validator, jsFunc);
        }
      }
    }
  }

  private boolean runAllPackageTestCases(String repoPath) throws Exception {
    Exception firstException = null;

    String[] paths = getPackages(repoPath);
    for (String path : paths) {
      if (path.equals("stdBankNBOL")) {
        System.out.println(">>>*** Skipping tests on " + path );
        continue;
      }

      String packagePath = repoPath + "/" + path;
      File file = new File(packagePath + "/tests/testDocCases.js");
      if (file.exists() && file.isFile()) {
        System.out.println(">>>*** Running tests on " + path );
        testCount = 0;
        errorCount = 0;

        long startMillis = System.currentTimeMillis();
        try {
          runPackageTestCases(packagePath);
        }
        catch (Exception e) {
          if (firstException == null)
            firstException = e;
          System.out.println("Exception running tests for " + path);
        }
        long endMillis = System.currentTimeMillis();

        if (testCount == 0) {
          System.out.println("No tests in package" );
        }
        else
        if (errorCount == 0) {
          System.out.println("Completed " + testCount + " tests Successfully :-)" );
          System.out.println("Run Time: " + (endMillis-startMillis) + " millseconds" );
        }
        else {
          if (errorCount == 1)
            System.out.println("Ran " + testCount + " tests with 1 error!");
          else
            System.out.println("Ran " + testCount + " tests with " + errorCount + " errors!");
        }
      }
      else {
        System.out.println("*** No file testDocCases.js for " + path );
      }
    }

    if (firstException != null)
      throw firstException;
    return (errorCount == 0);
  }

  @Test
  public void runAllPackages() throws Exception {
    TestDocuments test = new TestDocuments();

    assertTrue(test.runAllPackageTestCases("../finsurv-rules/reg_rule_repo"));
  }

  public static void main( String[] args ) throws Exception {
    TestDocuments test = new TestDocuments();

    test.runAllPackageTestCases("../finsurv-rules/reg_rule_repo");
  }
}
