package za.co.synthesis.rule.evaluation;

import za.co.synthesis.rule.core.*;

import java.util.List;

/**
 * User: jake
 * Date: 3/2/15
 * Time: 8:59 PM
 * These compound decisions are defined within the configuration file and they get expanded into multiple
 * EvaluationDecision objects
 */
public class CompoundEvaluationDecision {
  private ReportableType reportable;
  private String scenario = "Unknown";
  private FlowType flow;
  private DrCr reportingSide;
  private DrCr nonResSide;
  private List<ReportingAccountType> nonResAccountType;
  private List<String> nonResException;
  private DrCr resSide;
  private List<ReportingAccountType> resAccountType;
  private List<String> resException;
  private List<String> category;
  private List<String> notCategory;
  private String subject;
  private String locationCountry;
  private String manualSection;
  private String drSideSwift;
  private AccountHolderStatus accStatusFilter;

  public ReportableType getReportable() {
    return reportable;
  }

  public void setReportable(ReportableType reportable) {
    this.reportable = reportable;
  }

  public FlowType getFlow() {
    return flow;
  }

  public void setFlow(FlowType flow) {
    this.flow = flow;
  }

  public DrCr getReportingSide() { return reportingSide; }

  public void setReportingSide(DrCr reportingSide) { this.reportingSide = reportingSide; }

  public DrCr getNonResSide() {
    return nonResSide;
  }

  public void setNonResSide(DrCr nonResSide) {
    this.nonResSide = nonResSide;
  }

  public List<ReportingAccountType> getNonResAccountType() {
    return nonResAccountType;
  }

  public void setNonResAccountType(List<ReportingAccountType> nonResAccountType) {
    this.nonResAccountType = nonResAccountType;
  }

  public List<String> getNonResException() {
    return nonResException;
  }

  public void setNonResException(List<String> nonResException) {
    this.nonResException = nonResException;
  }

  public DrCr getResSide() {
    return resSide;
  }

  public void setResSide(DrCr resSide) {
    this.resSide = resSide;
  }

  public List<ReportingAccountType> getResAccountType() {
    return resAccountType;
  }

  public void setResAccountType(List<ReportingAccountType> resAccountType) {
    this.resAccountType = resAccountType;
  }

  public List<String> getResException() {
    return resException;
  }

  public void setResException(List<String> resException) {
    this.resException = resException;
  }

  public List<String> getCategory() {
    return category;
  }

  public void setCategory(List<String> category) {
    this.category = category;
  }

  public List<String> getNotCategory() {
    return notCategory;
  }

  public void setNotCategory(List<String> notCategory) {
    this.notCategory = notCategory;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getLocationCountry() {
    return locationCountry;
  }

  public void setLocationCountry(String locationCountry) {
    this.locationCountry = locationCountry;
  }

  public String getManualSection() { return manualSection; }

  public void setManualSection(String manualSection) { this.manualSection = manualSection; }

  public String getDrSideSwift() {
    return drSideSwift;
  }

  public void setDrSideSwift(String drSideSwift) {
    this.drSideSwift = drSideSwift;
  }


  public AccountHolderStatus getAccStatusFilter() {
    return accStatusFilter;
  }

  public void setAccStatusFilter(AccountHolderStatus accStatusFilter) {
    this.accStatusFilter = accStatusFilter;
  }

  public String getScenario() {
    return this.scenario;
  }

  public void setScenario(String scenario) {
    this.scenario = scenario;
  }
}
