package za.co.synthesis.rule.predef;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.impl.ValidationRuntimeException;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;

public class hasMetaLookupValue implements IAssertionFunction {
    private String lookupName;
    private String lookupKey;
    private String key;
    private String lookupValueStr;

    /*
      Used to test if values match on compound lookups. It does not make sense to use this on a simple lookup.

      @param:lookupName The name of the lookup to search
      @param:lookupKey  The lookup key to use when matching against the field value
      @param:key Once the lookup is found evaluate against this key (Optional)
      @param:lookupValue The value to test against the key of the found lookup item.
       */
    public hasMetaLookupValue(String lookupName, String lookupKey, String key, String lookupValue) {
        this.lookupName = lookupName;
        this.lookupKey = lookupKey;
        this.key = key;
        this.lookupValueStr = lookupValue;
    }

    public hasMetaLookupValue(String lookupName, String lookupKey, String key, Boolean lookupValue) {
        this.lookupName = lookupName;
        this.lookupKey = lookupKey;
        this.key = key;
        this.lookupValueStr = lookupValue.toString();
    }

    public boolean execute(FinsurvContext context, Object value) {
        if (value instanceof String) {
            String testVal = (String) value;
            Map<String, String> key = new HashMap<String, String>();
            key.put(this.lookupKey, testVal);
//            String lookupValue = context.getLookups().getLookupField(this.lookupName, key, this.key);
            String lookupValue = getLookupField((JSObject) context.getCustomValue().get(lookupName), lookupName, key, this.key);
            return lookupValue != null && lookupValue.equals(this.lookupValueStr);
        }
        return false;
    }

    private String getLookupField(JSObject jsLookup, final String lookup, final Map<String,String> key, final String fieldName) {
        try {
            if (jsLookup.containsKey(lookup)) {
                JSArray entries = (JSArray) jsLookup.get(lookup);
                for (Object entry : entries) {
                    if (entry instanceof JSObject) {
                        JSObject jsEntry = (JSObject) entry;

                        boolean bMatch = true;
                        for (Map.Entry<String, String> keyEntry : key.entrySet()) {
                            if (jsEntry.containsKey(keyEntry.getKey())) {
                                if (!jsEntry.get(keyEntry.getKey()).equals(keyEntry.getValue())) {
                                    bMatch = false;
                                    break;
                                }
                            } else {
                                bMatch = false;
                                break;
                            }
                        }
                        if (bMatch) {
                            if (jsEntry.containsKey(fieldName)) {
                                return jsEntry.get(fieldName).toString();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new ValidationRuntimeException("Error validating field=" + fieldName + ", lookup=" + lookup, e );
        }
        return null;
    }
}
