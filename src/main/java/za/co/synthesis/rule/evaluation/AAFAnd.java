package za.co.synthesis.rule.evaluation;

/**
 * Created by jake on 6/28/16.
 */
public class AAFAnd implements IAssumptionAssertionFunction {
  private IAssumptionAssertionFunction left;
  private IAssumptionAssertionFunction right;

  public AAFAnd(IAssumptionAssertionFunction left, IAssumptionAssertionFunction right) {
    this.left = left;
    this.right = right;
  }

  public boolean execute(GivenInfo context) {
    return left.execute(context) && right.execute(context);
  }
}
