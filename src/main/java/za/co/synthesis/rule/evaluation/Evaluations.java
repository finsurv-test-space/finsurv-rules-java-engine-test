package za.co.synthesis.rule.evaluation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.EvaluationDecision;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.support.JSConstant;
import za.co.synthesis.rule.support.StringList;
import za.co.synthesis.rule.support.Util;

import java.util.*;
/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif

/**
 * User: jake
 * Date: 3/3/15
 * Time: 9:32 AM
 * This class contains the all the evaluations. It is responsible for generating these evaluations from a set of
 * JavaScript structures. It provides methods to retrieve a subset of the rules based on filter criteria.
 */
public class Evaluations {
  private static class CategorySpaceResult {
    public boolean clash;
    public String duplicateNote;

    private CategorySpaceResult(boolean clash, String duplicateNote) {
      this.clash = clash;
      this.duplicateNote = duplicateNote;
    }
  }

  public static class DuplicateEvaluation extends Evaluation {
    private final String duplicateNote;

    public DuplicateEvaluation(ResidenceStatus drResStatus, BankAccountType drAccType,
                               ResidenceStatus crResStatus, BankAccountType crAccType,
                               List<String> category, List<String> notCategory, String note) {
      super(drResStatus, drAccType, crResStatus, crAccType, category, notCategory, null);
      this.duplicateNote = note;
    }

    public String getDuplicateNote() {
      return duplicateNote;
    }

    public String prettyRuleName() {
      StringBuilder sb = new StringBuilder();
      sb.append("drResStatus: ").append(getDrResStatus() != null ? getDrResStatus().toString() : "?");
      sb.append(", drAccType: ");
      sb.append(getDrAccType().toString());

      sb.append(", crResStatus: ").append(getCrResStatus() != null ? getCrResStatus().toString() : "?");
      sb.append(", crAccType: ");
      sb.append(getCrAccType().toString());

      if (getCategory() != null && getCategory().size() > 0) {
        sb.append(", category: ");
        if (getCategory().size() == 1) {
          sb.append(getCategory().toString());
        }
        else {
          boolean bFirst = true;
          sb.append("[");
          for (String cat : getCategory()) {
            if (bFirst)
              bFirst = false;
            else
              sb.append(", ");
            sb.append(cat);
          }
          sb.append("]");
        }
      }

      if (getNotCategory() != null && getNotCategory().size() > 0) {
        sb.append(", notCategory: ");
        if (getNotCategory().size() == 1) {
          sb.append(getNotCategory().toString());
        }
        else {
          boolean bFirst = true;
          sb.append("[");
          for (String cat : getNotCategory()) {
            if (bFirst)
              bFirst = false;
            else
              sb.append(", ");
            sb.append(cat);
          }
          sb.append("]");
        }
      }
      return sb.toString();
    }
  }

  Logger logger = LoggerFactory.getLogger(Evaluations.class);

  private final List<CompoundEvaluation> evaluationList = new ArrayList<CompoundEvaluation>();

  public Evaluations() {
  }

  private StringList composeStringList(Object param) {
    if (param instanceof String) {
      StringList result = new StringList(1);
      result.add(param.toString());
      return result;
    }
    else
    if (param instanceof JSArray) {
      StringList result = new StringList(((JSArray)param).size());
      for (Object value : (JSArray)param) {
        result.add(value.toString());
      }
      return result;
    }
    return new StringList();
  }

  private String composeString(Object param) {
    if (param != null) {
      if (param instanceof String) {
        return param.toString();
      }
    }
    return null;
  }


  private List<BankAccountType> bankAccountTypeList(Object obj) {
    List<BankAccountType> result = new ArrayList<BankAccountType>();
    if (obj instanceof JSArray) {
      for (Object value : (JSArray)obj) {
        BankAccountType type = JSConstant.bankAccountType(value);
        if (type != null && type != BankAccountType.Unknown)
          result.add(type);
      }
    }
    else {
      BankAccountType type = JSConstant.bankAccountType(obj);
      if (type != null && type != BankAccountType.Unknown)
        result.add(type);
    }
    return result;
  }

  private List<ReportingAccountType> reportingAccountTypeList(Object obj) {
    List<ReportingAccountType> result = new ArrayList<ReportingAccountType>();
    if (obj instanceof JSArray) {
      for (Object value : (JSArray)obj) {
        ReportingAccountType type = JSConstant.reportingAccountType(value);
        if (type != null && type != ReportingAccountType.Unknown)
          result.add(type);
      }
    }
    else {
      ReportingAccountType type = JSConstant.reportingAccountType(obj);
      if (type != null && type != ReportingAccountType.Unknown)
        result.add(type);
    }
    return result;
  }


  private CompoundEvaluationDecision loadDecision(JSObject jsDecision) {
    CompoundEvaluationDecision result = new CompoundEvaluationDecision();
    Object reportable = jsDecision.get("reportable");
    Object flow = jsDecision.get("flow");
    Object reportingSide = jsDecision.get("reportingSide");
    Object nonResSide = jsDecision.get("nonResSide");
    Object nonResAccountType = jsDecision.get("nonResAccountType");
    Object nonResException = jsDecision.get("nonResException");
    Object resSide = jsDecision.get("resSide");
    Object resAccountType = jsDecision.get("resAccountType");
    Object resException = jsDecision.get("resException");
    Object category = jsDecision.get("category");
    Object notCategory = jsDecision.get("notCategory");
    Object subject = jsDecision.get("subject");
    Object locationCountry = jsDecision.get("locationCountry");
    Object manualSection = jsDecision.get("manualSection");
    Object drSideSwift = jsDecision.get("drSideSwift");
    Object accStatusFilter = jsDecision.get("accStatusFilter");


    result.setReportable(JSConstant.reportableType(reportable));
    result.setFlow(JSConstant.flowType(flow));
    result.setReportingSide(JSConstant.drCr(reportingSide));
    result.setNonResSide(JSConstant.drCr(nonResSide));
    result.setNonResAccountType(reportingAccountTypeList(nonResAccountType));
    result.setNonResException(composeStringList(nonResException));
    result.setResSide(JSConstant.drCr(resSide));
    result.setResAccountType(reportingAccountTypeList(resAccountType));
    result.setResException(composeStringList(resException));
    result.setCategory(composeStringList(category));
    result.setNotCategory(composeStringList(notCategory));
    result.setSubject(composeString(subject));
    result.setLocationCountry(composeString(locationCountry));
    result.setManualSection(composeString(manualSection));
    result.setDrSideSwift(composeString(drSideSwift));
    result.setAccStatusFilter(JSConstant.accountHolderStatus(accStatusFilter));

    return result;
  }

  private void checkSize(StringList errors, CompoundEvaluation evaluation, String fieldName,
                     int expectedSize, int foundSize) {
    if (expectedSize != foundSize) {
      StringBuilder sb = new StringBuilder();
      sb.append("Rule {").append(evaluation.prettyRuleName());
      sb.append("} ").append(fieldName).append(" expected to have ").append(expectedSize).append(" entries");
      if (fieldName.equals("nonResException"))
        sb.append(" (or remove nonResSide entry and define only 1 exception)");
      if (fieldName.equals("resException"))
        sb.append(" (or remove resSide entry and define only 1 exception)");
      errors.add(sb.toString());
    }
  }

  private void validateEvaluationConfiguration(CompoundEvaluation evaluation, CompoundEvaluationDecision dec, StringList errors) {
    // Validate Evaluation File configuration
    if (evaluation.getDrResStatus() == null || evaluation.getDrResStatus() == ResidenceStatus.Unknown) {
      errors.add("Rule {" + evaluation.prettyRuleName() + "} no valid drResStatus has been defined");
    }
    if (evaluation.getCrResStatus() == null || evaluation.getCrResStatus() == ResidenceStatus.Unknown) {
      errors.add("Rule {" + evaluation.prettyRuleName() + "} no valid crResStatus has been defined");
    }
    if (evaluation.getDrAccType().size() == 0) {
      errors.add("Rule {" + evaluation.prettyRuleName() + "} no valid drAccType has been defined");
    }
    if (evaluation.getCrAccType().size() == 0) {
      errors.add("Rule {" + evaluation.prettyRuleName() + "} no valid crAccType has been defined");
    }

    if (dec == null ) {
      errors.add("Rule {" + evaluation.prettyRuleName() + "} no getDecision has been defined");
    }
    else {

      if (dec.getReportable() == ReportableType.Reportable || dec.getReportable() == ReportableType.ZZ1Reportable) {
        if (dec.getReportingSide() == null || dec.getReportingSide() == DrCr.Unknown) {
          errors.add("Rule {" + evaluation.prettyRuleName() + "} no valid reportingSide has been defined");
        }

        if (dec.getNonResSide() != null) {
          if (dec.getNonResSide() == DrCr.DR && dec.getNonResAccountType().size() > 0) {
            checkSize(errors, evaluation, "nonResAccountType", evaluation.getDrAccType().size(), dec.getNonResAccountType().size());
          } else if (dec.getNonResSide() == DrCr.DR && dec.getNonResException().size() > 0) {
            checkSize(errors, evaluation, "nonResException", evaluation.getDrAccType().size(), dec.getNonResException().size());
          } else if (dec.getNonResSide() == DrCr.CR && dec.getNonResAccountType().size() > 0) {
            checkSize(errors, evaluation, "nonResAccountType", evaluation.getCrAccType().size(), dec.getNonResAccountType().size());
          } else if (dec.getNonResSide() == DrCr.CR && dec.getNonResException().size() > 0) {
            checkSize(errors, evaluation, "nonResException", evaluation.getCrAccType().size(), dec.getNonResException().size());
          }
        } else {
          if (dec.getNonResException().size() > 0) {
            checkSize(errors, evaluation, "nonResException", 1, dec.getNonResException().size());
          } else {
            errors.add("Rule {" + evaluation.prettyRuleName() + "} nonResSide must be defined (unless single exception is used)");
          }
        }

        if (dec.getResSide() != null) {
          if (dec.getResSide() == DrCr.DR && dec.getResAccountType().size() > 0) {
            checkSize(errors, evaluation, "resAccountType", evaluation.getDrAccType().size(), dec.getResAccountType().size());
          } else if (dec.getResSide() == DrCr.DR && dec.getResException().size() > 0) {
            checkSize(errors, evaluation, "resException", evaluation.getDrAccType().size(), dec.getResException().size());
          } else if (dec.getResSide() == DrCr.CR && dec.getResAccountType().size() > 0) {
            checkSize(errors, evaluation, "resAccountType", evaluation.getCrAccType().size(), dec.getResAccountType().size());
          } else if (dec.getResSide() == DrCr.CR && dec.getResException().size() > 0) {
            checkSize(errors, evaluation, "resException", evaluation.getCrAccType().size(), dec.getResException().size());
          }
        } else {
          if (dec.getResException().size() > 0) {
            checkSize(errors, evaluation, "resException", 1, dec.getResException().size());
          } else {
            errors.add("Rule {" + evaluation.prettyRuleName() + "} resSide must be defined (unless single exception is used)");
          }
        }
      }
    }
  }

  public void loadEvaluations(JSArray jsEvaluations, boolean raiseExceptionOnDuplicateRules) throws EvaluationException {
    List<CompoundEvaluation> localList = new ArrayList<CompoundEvaluation>();
    StringList errors = new StringList();

    for (Object obj : jsEvaluations) {
      if (obj instanceof JSObject) {
        JSObject jsEvaluation = (JSObject)obj;
        Object drResStatus = jsEvaluation.get("drResStatus");
        Object drAccType = jsEvaluation.get("drAccType");
        Object crResStatus = jsEvaluation.get("crResStatus");
        Object crAccType = jsEvaluation.get("crAccType");
        Object validFrom = jsEvaluation.get("validFrom");
        Object validTo = jsEvaluation.get("validTo");
        Object category = jsEvaluation.get("category");
        Object notCategory = jsEvaluation.get("notCategory");

        Object decision = jsEvaluation.get("decision");
        Object drDecision = jsEvaluation.get("drDecision");
        Object crDecision = jsEvaluation.get("crDecision");

        CompoundEvaluation evaluation = new CompoundEvaluation();
        evaluation.setDrResStatus(JSConstant.residenceStatus(drResStatus));
        evaluation.setDrAccType(bankAccountTypeList(drAccType));
        evaluation.setCrResStatus(JSConstant.residenceStatus(crResStatus));
        evaluation.setCrAccType(bankAccountTypeList(crAccType));
        evaluation.setValidFrom(Util.date(validFrom));
        evaluation.setValidTo(Util.date(validTo));
        evaluation.setCategory(composeStringList(category));
        evaluation.setNotCategory(composeStringList(notCategory));
        if (decision instanceof JSObject) {
          CompoundEvaluationDecision dec = loadDecision((JSObject)decision);
          if (dec.getReportingSide() == DrCr.DR)
            evaluation.setDrDecision(dec);
          else
          if (dec.getReportingSide() == DrCr.CR)
            evaluation.setCrDecision(dec);
          else
            evaluation.setDrDecision(dec);
        }
        if (drDecision instanceof JSObject) {
          CompoundEvaluationDecision dec = loadDecision((JSObject)drDecision);
          dec.setReportingSide(DrCr.DR);
          evaluation.setDrDecision(dec);
        }
        if (crDecision instanceof JSObject) {
          CompoundEvaluationDecision dec = loadDecision((JSObject)crDecision);
          dec.setReportingSide(DrCr.CR);
          evaluation.setCrDecision(dec);
        }

        localList.add(evaluation);

        // Validate Evaluation File configuration
        CompoundEvaluationDecision dec = evaluation.getDrDecision();
        if (dec != null)
          validateEvaluationConfiguration(evaluation, dec, errors);
        dec = evaluation.getCrDecision();
        if (dec != null)
          validateEvaluationConfiguration(evaluation, dec, errors);
      }
    }
    // Raise errors now before looking for duplicates because these configuration errors will cause problem in the next step
    if (errors.size() > 0) {
      StringBuilder sb = new StringBuilder();
      sb.append("Evaluation Rule definition errors:").append("\n");
      for (String error : errors) {
        sb.append(error).append("\n");
      }
      throw new EvaluationException(sb.toString());
    }

    // Look for duplicate rules on the rules for today's date (as a default date. ideally all relevant dates should be checked)
    List<DuplicateEvaluation> duplicateRules = new ArrayList<DuplicateEvaluation>();

    // NOTE: The null context is okay because the return of this function not used and it is only called to get duplicates
    evaluationRulesFromRuleSets(null, localList, LocalDate.now(), duplicateRules);
    if (raiseExceptionOnDuplicateRules) {
      for (DuplicateEvaluation dup : duplicateRules) {
        StringBuilder sb = new StringBuilder();
        sb.append("Duplicate Rule {").append(dup.prettyRuleName());
        sb.append("} ").append(dup.getDuplicateNote());
        errors.add(sb.toString());
      }

      if (errors.size() > 0) {
        StringBuilder sb = new StringBuilder();
        sb.append("Evaluation Rule definition errors:").append("\n");
        for (String error : errors) {
          sb.append(error).append("\n");
        }
        throw new EvaluationException(sb.toString());
      }
    }

    evaluationList.addAll(localList);
  }

  private boolean categoryInList(String category, String mainCategory, List<String> ruleList) {
    boolean result = false;
    for (String ruleItem : ruleList) {
      int pos = ruleItem.indexOf('/');
      if (pos == -1) {
        if (mainCategory.equals(ruleItem)) {
          result = true;
          break;
        }
      } else {
        if (category.equals(ruleItem)) {
          result = true;
          break;
        }
      }
    }
    return result;
  }

  private CategorySpaceResult compareCategorySpace(List<String> ruleCategory, List<String> ruleNotCategory,
                                                   List<String> category, List<String> notCategory) {
    List<String> ruleCategories = (ruleCategory != null ? ruleCategory : new ArrayList<String>());
    List<String> ruleNotCategories = (ruleNotCategory != null ? ruleNotCategory : new ArrayList<String>());

    boolean clash = false;
    String duplicateNote = null;

    List<String> newCategories = (category != null ? category : new ArrayList<String>());

    for (int j = 0; j < newCategories.size(); j++) {
      String fullCategory = newCategories.get(j);
      String mainCategory = fullCategory;
      int posSlash = fullCategory.indexOf('/');
      if (posSlash != -1) {
        mainCategory = fullCategory.substring(0, posSlash);
      }
      if (ruleCategories.indexOf(fullCategory) != -1 || ruleCategories.indexOf(mainCategory) != -1) {
        duplicateNote = "category " + fullCategory + " matches category list";
        clash = true;
      }
      if (!categoryInList(fullCategory, mainCategory, ruleNotCategories)) {
        duplicateNote = "category " + fullCategory + " matches notCategory list";
        clash = true;
      }
    }
    List<String> newNotCategories = (notCategory != null ? notCategory : new ArrayList<String>());

    for (String fullNotCategory : newNotCategories) {
      String mainNotCategory = fullNotCategory;
      int posNotSlash = fullNotCategory.indexOf('/');
      if (posNotSlash != -1) {
        mainNotCategory = fullNotCategory.substring(0, posNotSlash);
      }
      if (!categoryInList(fullNotCategory, mainNotCategory, ruleCategories)) {
        duplicateNote = "notCategory " + fullNotCategory + " matches category list";
        clash = true;
      }
      if (ruleNotCategories.indexOf(fullNotCategory) != -1 || ruleNotCategories.indexOf(mainNotCategory) != -1) {
        duplicateNote = "notCategory " + fullNotCategory + " matches notCategory list";
        clash = true;
      }
    }

    if (newCategories.size() == 0 && ruleCategories.size() == 0 && newNotCategories.size() == 0 && ruleNotCategories.size() == 0) {
      return new CategorySpaceResult(true, "No category or notCategory entries to distinguish between conflicting rules");
    }
    else {
      return new CategorySpaceResult(clash, (duplicateNote != null) ? duplicateNote : "passed");
    }
  }

  private boolean reportingSideMatches(DrCr repSide1, DrCr repSide2) {
    if (repSide1 == null)
      return true;
    if (repSide2 == null)
      return true;
    return repSide1 == repSide2;
  }

  /*
   * Helper function to first check if rule is present before adding it
   */
  private void pushEvaluationRule(Context context, List<Evaluation> rulelist, List<DuplicateEvaluation> duplicateRules,
                                  int drIndex, ResidenceStatus drResStatus, BankAccountType drAccType,
                                  int crIndex, ResidenceStatus crResStatus, BankAccountType crAccType,
                                  List<String> category, List<String> notCategory,
                                  CompoundEvaluationDecision decision) {
    boolean found = false;
    String duplicateNote = "";
    for (Evaluation rule : rulelist) {
      if (drResStatus == rule.getDrResStatus() &&
              drAccType == rule.getDrAccType() &&
              crResStatus == rule.getCrResStatus() &&
              crAccType == rule.getCrAccType() &&
              reportingSideMatches(decision.getReportingSide(), rule.getDecision().getReportingSide())) {

        CategorySpaceResult comp1 = compareCategorySpace(rule.getCategory(), rule.getNotCategory(), category, notCategory);
        CategorySpaceResult comp2 = compareCategorySpace(category, notCategory, rule.getCategory(), rule.getNotCategory());

        if (comp1.clash || comp2.clash) {
          duplicateNote = "-> " + comp1.duplicateNote + " : <- " + comp2.duplicateNote;
          found = true;
          break;
        }
      }
    }
    if (!found) {
      EvaluationDecision ruleDecision = new EvaluationDecision(context, decision, drIndex, crIndex);
      rulelist.add(new Evaluation(drResStatus, drAccType, crResStatus, crAccType, category, notCategory, ruleDecision));
    }
    else {
      duplicateRules.add(new DuplicateEvaluation(drResStatus, drAccType, crResStatus, crAccType, category, notCategory, duplicateNote));
    }
  }

  /*
   * Returns the list of evaluation rules pertinent for the specified date
   */
  private List<Evaluation> evaluationRulesFromRuleSets(Context context, List<CompoundEvaluation> ruleset, LocalDate valueDate,
                                                       List<DuplicateEvaluation> duplicateRules) {
    LocalDate nextValueDate = valueDate.plusDays(1);

    List<Evaluation> result = new ArrayList<Evaluation>();

    for (CompoundEvaluation rule : ruleset) {
      ResidenceStatus drResStatus = rule.getDrResStatus();
      BankAccountType drAccType;
      ResidenceStatus crResStatus = rule.getCrResStatus();
      BankAccountType crAccType;
      LocalDate validFrom = (rule.getValidFrom() != null ? rule.getValidFrom() : valueDate);
      LocalDate validTo = (rule.getValidTo() != null ? rule.getValidTo() : nextValueDate);
      CompoundEvaluationDecision drDecision = rule.getDrDecision();
      CompoundEvaluationDecision crDecision = rule.getCrDecision();

      if ((valueDate.isEqual(validFrom) || valueDate.isAfter(validFrom)) && valueDate.isBefore(validTo)) {
        for (int i = 0; i < rule.getDrAccType().size(); i++) {
          drAccType = rule.getDrAccType().get(i);
          for (int j = 0; j < rule.getCrAccType().size(); j++) {
            crAccType = rule.getCrAccType().get(j);
            if (drDecision != null) {
              pushEvaluationRule(context, result, duplicateRules, i, drResStatus, drAccType, j, crResStatus, crAccType,
                      rule.getCategory(), rule.getNotCategory(), drDecision);
            }
            if (crDecision != null) {
              pushEvaluationRule(context, result, duplicateRules, i, drResStatus, drAccType, j, crResStatus, crAccType,
                      rule.getCategory(), rule.getNotCategory(), crDecision);
            }
          }
        }
      }
    }
    return result;
  }

  public List<Evaluation> evaluationRulesForValueDate(Context context, LocalDate valueDate, List<DuplicateEvaluation> duplicateRules) {
    return evaluationRulesFromRuleSets(context, evaluationList, valueDate, duplicateRules);
  }
}
