package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 12:42 PM
 * Determines if the monetary amount field has been provided and if it is a string does it contain any non-whitespace
 * characters
 */
public class hasMoneyField implements IAssertionFunction {
  private IStringResolver field;

  public hasMoneyField(IStringResolver constant) {
    this.field = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getMoneyField(context.getCurrentMoneyInstance(), field.resolve(context, value));
    if (fieldValue != null && (!(fieldValue instanceof String) || ((String)fieldValue).trim().length() > 0))
      return true;
    else
      return false;
  }
}
/*
function hasMoneyField(field) {
  return function(context, value) {
    var fieldValue = context.getMoneyField(context.currentMoneyInstance, field);
    if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
      return true;
    else
      return false;
  }
}
*/
