package za.co.synthesis.rule.difference.validation;

import za.co.synthesis.rule.core.Scope;

public class FieldSize {
  private final Scope scope;
  private final String field;
  private final Integer minLen;
  private final Integer maxLen;
  private final Integer len;

  public FieldSize(Scope scope, String field, Integer minLen, Integer maxLen, Integer len) {
    this.scope = scope;
    this.field = field;
    this.minLen = minLen;
    this.maxLen = maxLen;
    this.len = len;
  }

  public Scope getScope() {
    return scope;
  }

  public String getField() {
    return field;
  }

  public Integer getMinLen() {
    return minLen;
  }

  public Integer getMaxLen() {
    return maxLen;
  }

  public Integer getLen() {
    return len;
  }
}
