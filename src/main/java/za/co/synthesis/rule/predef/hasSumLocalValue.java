package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 4/13/15
 * Time: 10:38 AM
 * Check to see how the sum of the Rand amounts compares to the given constant value
 */
public class hasSumLocalValue implements IAssertionFunction {
  private String operation;
  private BigDecimal constant;

  public hasSumLocalValue(String operation, String constant) {
    this.operation = operation;
    this.constant = Util.number(constant);
  }

  public boolean execute(FinsurvContext context, Object value) {
    BigDecimal total = new BigDecimal(0);
    for (int i = 0; i < context.getMoneySize(); i++) {
      BigDecimal rvalue = Util.number(context.getMoneyField(i, context.map("LocalValue")));
      if (rvalue != null)
        total = total.add(rvalue);
    }
    BigDecimal oneHundred = new BigDecimal(100);
    long givenValue = constant.multiply(oneHundred).longValue();
    long randSumValue = total.multiply(oneHundred).longValue();
    if (operation.equals(">"))
      return randSumValue > givenValue;
    if (operation.equals(">="))
      return randSumValue >= givenValue;
    if (operation.equals("<"))
      return randSumValue < givenValue;
    if (operation.equals("<="))
      return randSumValue <= givenValue;
    if (operation.equals("="))
      return randSumValue == givenValue;
    return false;
  }
}
/*
function hasSumRandValue(operation, constant) {
  return function(context, value) {
    var total = 0;
    for ( var i = 0; i < context.getMoneySize(); i++) {
      var rvalue = context.getMoneyField(i, "RandValue");
      if (rvalue)
        total += Number(rvalue);
    }
    var givenValue = Math.round(Number(constant) * 100);
    var randSumValue = Math.round(total * 100);
    if (operation === ">")
      return randSumValue > givenValue;
    if (operation === ">=")
      return randSumValue >= givenValue;
    if (operation === "<")
      return randSumValue < givenValue;
    if (operation === "<=")
      return randSumValue <= givenValue;
    if (operation === "=")
      return randSumValue == givenValue;
  }
}
*/
