package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 10:35 AM
 * Determines if the nonresident individual/entity field has been provided and if it is a string does it contain any
 * non-whitespace characters
 */
public class hasNonResidentField implements IAssertionFunction {
  private String constant;

  public hasNonResidentField(String constant) {
    this.constant = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField("NonResident.Individual." + constant);
    if (fieldValue == null || fieldValue instanceof IFinsurvContext.Undefined) {
      fieldValue = context.getTransactionField("NonResident.Individual." + constant);
    }
    if (fieldValue != null && !(fieldValue instanceof IFinsurvContext.Undefined) &&
            (!(fieldValue instanceof String) || ((String)fieldValue).trim().length() > 0))
      return true;
    else
      return false;
  }
}
/*
function hasNonResidentField(field) {
  return function(context, value) {
    var fieldValue = context.getTransactionField("NonResident.Individual." + field);
    if (!fieldValue)
      fieldValue = context.getTransactionField("NonResident.Entity." + field);
    if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
      return true;
    else
      return false;
  }
}
*/