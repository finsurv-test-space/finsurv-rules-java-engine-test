package za.co.synthesis.rule.support;

import java.util.ArrayList;
import java.util.Collection;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 4:50 AM
 * This predefined List of Strings and it is a predominantly used by the pre defined functions and function factory
 */
public class StringList extends ArrayList<String> {
  public StringList(int i) {
    super(i);
  }

  public StringList() {
  }

  public StringList(Collection<? extends String> strings) {
    super(strings);
  }
}
