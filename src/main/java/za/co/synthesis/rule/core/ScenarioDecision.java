package za.co.synthesis.rule.core;

import java.util.ArrayList;
import java.util.List;

public class ScenarioDecision {
  private String scenario;
  private String information;
  private List<IEvaluationDecision> decisions;

  public ScenarioDecision(String scenario, String information, List<IEvaluationDecision> decisions) {
    this.scenario = scenario;
    this.information = information;
    this.decisions = decisions;
  }

  public ScenarioDecision(String scenario, String information, IEvaluationDecision decision) {
    this.scenario = scenario;
    this.information = information;
    this.decisions = new ArrayList<IEvaluationDecision>();
    this.decisions.add(decision);
  }

  public String getScenario() {
    return scenario;
  }

  public String getInformation() {
    return information;
  }

  public List<IEvaluationDecision> getDecisions() {
    return decisions;
  }
}
