package za.co.synthesis.rule.core;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.classification.Classification;
import za.co.synthesis.rule.classification.ClassificationContext;
import za.co.synthesis.rule.classification.Classifications;
import za.co.synthesis.rule.core.impl.JSClassificationContext;
import za.co.synthesis.rule.core.impl.JSFinsurvContext;
import za.co.synthesis.rule.core.impl.PojoFinsurvContext;
import za.co.synthesis.rule.support.*;

import java.util.List;

/**
 * Created by jake on 6/7/16.
 */
public class Classifier {
  private final Classifications classifications;

  public Classifier(Classifications classifications) {
    this.classifications = classifications;
  }

  private void runClassification(ClassificationContext context, List<Classification> rules) {
    for (Classification rule : rules) {
      rule.run(context);
    }
  }

  public List<ClassificationResult> classifyRule(String rulename, JSObject jsObj) {
    JSClassificationContext jsContext = new JSClassificationContext(jsObj);
    ClassificationContext context = new ClassificationContext(jsContext);

    List<Classification> rules = classifications.classificationRules(context.getDrCr(), rulename);
    runClassification(context, rules);
    return context.getClassificationResultList();
  }

  public List<ClassificationResult> classify(JSObject jsObj) {
    JSClassificationContext jsContext = new JSClassificationContext(jsObj);
    ClassificationContext context = new ClassificationContext(jsContext);

    List<Classification> rules = classifications.classificationRules(context.getDrCr(), null);
    runClassification(context, rules);
    return context.getClassificationResultList();
  }

  public List<ClassificationResult> classifyRule(String rulename, IClassificationContext ctx) {
    ClassificationContext context = new ClassificationContext(ctx);

    List<Classification> rules = classifications.classificationRules(context.getDrCr(), rulename);
    runClassification(context, rules);
    return context.getClassificationResultList();
  }

  public List<ClassificationResult> classify(IClassificationContext ctx) {
    ClassificationContext context = new ClassificationContext(ctx);

    List<Classification> rules = classifications.classificationRules(context.getDrCr(), null);
    runClassification(context, rules);
    return context.getClassificationResultList();
  }
}
