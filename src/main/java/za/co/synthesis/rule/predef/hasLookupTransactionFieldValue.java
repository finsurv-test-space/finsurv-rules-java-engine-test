package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 11/3/16.
 */
public class hasLookupTransactionFieldValue implements IAssertionFunction {
  private String lookupName;
  private String lookupKey;
  private String key;
  private String field;

  public hasLookupTransactionFieldValue(String lookupName, String lookupKey,String key, String field) {
    this.lookupName = lookupName;
    this.lookupKey = lookupKey;
    this.key = key;
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (value != null && value instanceof String) {
      Object fieldValue = context.getTransactionField(field);
      String testVal = (String) value;
      Map<String, String> key = new HashMap<String, String>();
      key.put(this.lookupKey, testVal);
      String lookupValue = context.getLookups().getLookupField(this.lookupName, key, this.key);
      if (lookupValue != null && lookupValue.equals(fieldValue))
        return true;
    }
    return false;
  }
}

/*
  _export.hasLookupTransactionFieldValue = function(lookupName, lookupKey, key, field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(field);
      var lookup = context.lookups.lookups[lookupName];
      if (!!lookup) {
        var item = lookup.find(function (item) {
          return  (item[lookupKey] == value)
        })
        if(!!item){
          return (item[key]==fieldValue);
        }
      }
    }
  }
*/