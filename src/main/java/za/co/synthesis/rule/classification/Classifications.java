package za.co.synthesis.rule.classification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.DrCr;
import za.co.synthesis.rule.support.DSLSupport;
import za.co.synthesis.rule.support.StringList;

import java.util.*;

/**
 * Created by jake on 6/7/16.
 */
public class Classifications {
  Logger logger = LoggerFactory.getLogger(Classifications.class);

  private final List<ClassificationRuleSet> ruleSets = new ArrayList<ClassificationRuleSet>();

  public Classifications() {
  }

  private IClassificationFunction loadApplyFunc(IClassificationFunction left, JSIdentifier apply) throws Exception {
    IClassificationFunction result = left;
    if (left != null && apply != null) {
      if (apply instanceof JSFunctionCall) {
        JSFunctionCall applyFunc = (JSFunctionCall)apply;
        if (applyFunc.getName().equals("and")) {
          result = new CFAnd(left, loadAssertion((JSIdentifier)applyFunc.getParameters().get(0)));
        }
        else
        if (applyFunc.getName().equals("or")) {
          result = new CFOr(left, loadAssertion((JSIdentifier)applyFunc.getParameters().get(0)));
        }

        result = loadApplyFunc(result, applyFunc.getApply());
      }
    }
    return result;
  }

  private IClassificationFunction loadAssertion(JSIdentifier identifier) throws Exception {
    IClassificationFunction result = null;
    if (identifier instanceof JSFunctionCall) {
      JSFunctionCall func = (JSFunctionCall)identifier;

      if (func.getParameters().size() == 1) {
        Object param = func.getParameters().get(0);
        if (param instanceof JSArray) {
          StringList params = DSLSupport.composeStringList(param);
          result = ClassificationFunctionFactory.createFunction(identifier.getName(), params);
        }
        else {
          if (param instanceof String)
            result = ClassificationFunctionFactory.createFunction(identifier.getName(), (String) param);
          else
          if (param instanceof JSRegExLiteral)
            result = ClassificationFunctionFactory.createFunction(identifier.getName(), (JSRegExLiteral) param);
          else
          if (param instanceof JSIdentifier)
            result = ClassificationFunctionFactory.createFunction(identifier.getName(), loadAssertion((JSIdentifier) param));
          else {
            result = ClassificationFunctionFactory.createFunction(identifier.getName(), param.toString());
          }
        }
      }

      if (func.getParameters().size() == 2) {
        String param1 = func.getParameters().get(0).toString();
        Object param2 = func.getParameters().get(1);
        if (param2 instanceof JSArray) {
          StringList params = DSLSupport.composeStringList(param2);
          result = ClassificationFunctionFactory.createFunction(identifier.getName(), param1, params);
        }
        else
        if (param2 instanceof JSIdentifier) {
          result = ClassificationFunctionFactory.createFunction(identifier.getName(), param1, loadAssertion((JSIdentifier) param2));
        }
        else {
          result = ClassificationFunctionFactory.createFunction(identifier.getName(), param1, param2.toString());
        }
      }
    }
    else {
      result = ClassificationFunctionFactory.createFunction(identifier.getName());
    }

    result = loadApplyFunc(result, identifier.getApply());
    return result;
  }

  private IClassificationFunction getAssertion(JSFunctionCall jsFunc, int paramIndex) throws Exception {
    if (jsFunc.getParameters().size() > paramIndex) {
      Object objExp = jsFunc.getParameters().get(paramIndex);
      if (objExp instanceof JSIdentifier) {
        return loadAssertion((JSIdentifier)objExp);
      }
    }
    return null;
  }

  private void applyFilters(ClassificationRule rule, JSIdentifier jsId) throws Exception {
    if (jsId != null && jsId instanceof JSFunctionCall) {
      JSFunctionCall jsFunc = (JSFunctionCall)jsId;

      if (jsFunc.getName().equals("onDr") && jsFunc.getParameters().size() == 0) {
        rule.onDr();
      }
      else
      if (jsFunc.getName().equals("onCr") && jsFunc.getParameters().size() == 0) {
        rule.onCr();
      }

      applyFilters(rule, jsFunc.getApply());
    }
  }


  private ClassificationRule loadRule(JSFunctionCall jsFunc) throws Exception {
    ClassificationRule result = null;

    IClassificationFunction assertion;

    if (jsFunc.getParameters().size() == 2) {
      assertion = getAssertion(jsFunc, 1);
      if (assertion != null) {
        result = new ClassificationRule(jsFunc.getName(), jsFunc.getParameters().get(0).toString(), assertion);
      }
    }

    if (result != null) {
      applyFilters(result, jsFunc.getApply());
    }
    return result;
  }

  public void loadRuleSet(JSObject jsRuleSet) {
    Object rulesetName = jsRuleSet.get("ruleset");

    if (rulesetName != null) {
      ClassificationRuleSet ruleset = new ClassificationRuleSet(rulesetName.toString());

      JSArray jsValidations = (JSArray)jsRuleSet.get("classifications");
      for (Object obj : jsValidations) {
        if (obj instanceof JSObject) {
          JSObject jsClassification = (JSObject)obj;
          Object fieldName = jsClassification.get("field");

          if (fieldName != null) {
            FieldClassification fieldClassify = new FieldClassification(DSLSupport.composeStringList(fieldName));
            ruleset.addClassification(fieldClassify);

            JSArray jsRules = (JSArray)jsClassification.get("rules");
            if (jsRules != null) {
              for (Object objRule : jsRules) {
                if (objRule instanceof JSFunctionCall) {
                  JSFunctionCall jsFunc = (JSFunctionCall)objRule;
                  try {
                    ClassificationRule rule = loadRule(jsFunc);
                    if (rule != null) {
                      fieldClassify.addRule(rule);
                    }
                  } catch (Exception e) {
                    logger.info("Error loading rule ", e);
                    System.out.println("Error loading rule '"+ obj.toString() +"' on field '" + fieldName.toString() + "': " + e.getMessage());
                  }
                }
              }
            }
          }
        }
      }
      ruleSets.add(ruleset);
    }
  }

  /*
   * Helper function to expand the rules with an Array of fields...(multi-field-rules)
   */
  private void makeRule(List<Classification> rulelist, String rulename,
                        FieldClassification classification, ClassificationRule rule) {
    for (int j = 0; j < classification.getFieldList().size(); j++) {
      String field = classification.getFieldList().get(j);
      if (rulename != null) {
        String indexedRuleName;
        if (classification.getFieldList().size() == 1)
          indexedRuleName = rule.getName();
        else
          indexedRuleName = rule.getName() + ':' + Integer.toString(j + 1);
        if (rulename.equals(indexedRuleName))
          rulelist.add(new Classification(indexedRuleName, field, rule.getType(), rule.getAssertion()));
      } else {
        rulelist.add(new Classification(rule.getName(), field, rule.getType(), rule.getAssertion()));
      }
    }
  }

  public List<Classification> classificationRules(DrCr drcr, String rulename) {
    List<Classification> result = new ArrayList<Classification>();
    Set<String> nameList = new HashSet<String>();
    for (ClassificationRuleSet ruleset : ruleSets) {
      for (FieldClassification classification : ruleset.getClassifications()) {
        for (ClassificationRule rule : classification.getRules()) {
          if (!nameList.contains(rule.getName())) {
            nameList.add(rule.getName());

            // if there is a rulename, match it with rule.name
            if (rulename != null && rule.getName() != null) {
              if (!rule.getName().equals(rulename)) {
                continue;
              }
            }

            if (drcr != null && rule.getDrCr() != null && !rule.getDrCr().equals(drcr))
              continue;

            makeRule(result, rulename, classification, rule);
          }
        }
      }
    }
    return result;
  }
}
