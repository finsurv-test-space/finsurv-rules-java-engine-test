package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 11:38 AM
 * Determines if the current value is one of the valid SWIFT country codes
 */
public class hasInvalidSWIFTCurrency implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return !context.getLookups().isValidCurrencyCode(value.toString());
  }
}
/*
function hasInvalidSWIFTCurrency(context, value) {
  if (!value)
    return true;
  return !context.lookups.isValidCurrencyCode(value);
}
*/