package za.co.synthesis.mapping.core;

import java.util.List;

/**
 * User: jake
 * Date: 1/27/16
 * Time: 4:31 AM
 * The interface to an object that can aggregate a list of strings
 */
public interface IAggregation {
  String aggregate(List<String> values);
}
