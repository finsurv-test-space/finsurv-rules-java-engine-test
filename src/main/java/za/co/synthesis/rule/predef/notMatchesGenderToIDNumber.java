package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * Created by petruspretorius on 22/09/2016.
 */
public class notMatchesGenderToIDNumber implements IAssertionFunction {

  private String field;

  public notMatchesGenderToIDNumber(String field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    String gender = value.toString();
    Object idObj;
    if (field.contains("Resident")) {
      idObj = context.getTransactionField(this.field);
    } else {
      idObj = context.getMoneyField(context.getCurrentMoneyInstance(), this.field);
    }
    String idNumberStr = idObj.toString();
    if (idNumberStr.length() > 10) {
      Integer midNumber = Integer.parseInt(idNumberStr.substring(6, 10));
      return midNumber < 5000 ? (gender.equals("M")) : (gender.equals("F"));
    }
    return false;
  }
}
