package za.co.synthesis.rule.predef;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jake
 * Date: 4/20/16
 * Time: 11:46 AM
 * Replace a given regex pattern with the given constant
 */
public class replaceRegex implements IDisplayFunction {
  private final String constant;
  private final Pattern pattern;

  public replaceRegex(String regex, final String constant) {
    this.pattern = Pattern.compile(regex);
    this.constant = constant;
  }

  public replaceRegex(JSRegExLiteral regex, final String constant) {
    this.pattern = regex.getPattern();
    this.constant = constant;
  }

  @Override
  public String execute(FinsurvContext context, Object value) {
    if (value != null) {
      Matcher matcher = pattern.matcher(value.toString());
      return matcher.replaceAll(constant);
    }
    return null;
  }
}
