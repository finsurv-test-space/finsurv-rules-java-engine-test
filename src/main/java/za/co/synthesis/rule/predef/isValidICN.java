package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

public class isValidICN implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return false;
    return Util.isValidICN(value.toString(), context.getLookups());
  }
}