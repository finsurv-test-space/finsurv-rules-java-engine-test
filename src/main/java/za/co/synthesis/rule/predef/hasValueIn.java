package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.StringList;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 5:52 AM
 * Checks to see if the value is in the given set of values
 */
public class hasValueIn implements IAssertionFunction {
  private StringList constants = null;

  public hasValueIn(String value) {
    constants = new StringList();
    constants.add(value);
  }

  public hasValueIn(StringList value) {
    this.constants = value;
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (value != null) {
      return constants.contains(value);
    }
    return false;
  }
}
/*
function hasValueIn(constant) {
  return function(context, value) {
    if (value) {
      if (typeof constant === "string") {
        if (value === constant)
          return true;
      } else {
        for ( var i = 0; i < constant.length; i++) {
          if (value === constant[i])
            return true;
        }
      }
    }
    return false;
  }
}
*/