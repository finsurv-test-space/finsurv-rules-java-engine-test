package za.co.synthesis.rule.support;

/**
 * User: jake
 * Date: 4/27/16
 * Time: 4:46 PM
 * This function is used to set the value on a different field other than the current field
 */
public class SetField extends DisplayRule {
  public SetField(final String field, IDisplayFunction displayFunction, IAssertionFunction assertion) {
    super(DisplayRuleType.SetField, field, null, displayFunction, assertion);
  }
}