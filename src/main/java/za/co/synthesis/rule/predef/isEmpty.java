package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 11:01 PM
 * Determines if the given value is empty
 */
public class isEmpty implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null)
      return true;
    if (value instanceof String) {
      if (((String)value).trim().length() == 0)
        return true;
    }
    return false;
  }
}
