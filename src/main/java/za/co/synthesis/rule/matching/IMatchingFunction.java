package za.co.synthesis.rule.matching;

import za.co.synthesis.rule.classification.ClassificationContext;

/**
 * Created by jake on 6/15/16.
 */
public interface IMatchingFunction {
  boolean execute(MatchingContext context);
}
