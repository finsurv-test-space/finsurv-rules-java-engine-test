define(function () {
  return function (predef) {
      var transaction;

      with (predef) {
          transaction = {
              ruleset: "Transaction Rules for Flow form",
              scope: "transaction",
              validations: [{
                  field: "",
                  rules: [
                      message("vd4", null, "Submission Date must be equal to or after the 2013-08-19"),
                      message("ocntry1", null, "If Originating Bank is completed, this must also be completed"),
                      message("ocntry4", null, "SWIFT country code may not be ZA for Inwards")
                  ]
              },
              {
                  field: "ValueDate",
                  rules: [
                      warning("vd2", 216, "Submission date cannot be future-dated further than 30 days", notEmpty.and(isDaysInFuture(30))),
                      warning("vd3", 217, "You are capturing a transaction that occurred in the past", notEmpty.and(hasPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/)).and(isDaysInPast(1))),
                      failure("inv_vd1", "I25", "Must be in a valid date format: YYYY-MM-DD",
                          notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/))).onSection("ABG")
                  ]
              },
              {
                  field: "TrnReference",
                  maxLen: 32,
                  rules: [
                      ignore("tref1")
                  ]
              },
              {
                  field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
                  rules: [
                      ignore("nrian1"),

                      failure("inv_nrian1", "I??", "The beneficiary account number or IBAN is mandatory",
                          isEmpty.and(notTransactionFieldValue("IsInvestecFCA", "Y")))
                          .onSection("ABCDG")
                          .onOutflow(),

                      failure("inv_nrian4", "I??", "Account number is limited to 34 characters",
                          notEmpty.and(isTooLong(34)))
                          .onSection("A")
                          .onOutflow(),

                      failure("inv_nrian5", "I??", "The beneficiary account number/IBAN is not required when paying an internal Investec Foreign Currency account",
                          notEmpty.and(hasTransactionFieldValue("IsInvestecFCA", "Y")))
                          .onSection("A")
                          .onOutflow()
                  ]
              }
              ]
          }
      };

      return transaction;
  }

});

