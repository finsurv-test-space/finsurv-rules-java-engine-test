package za.co.synthesis.javascript;

/**
 * User: jake
 * Date: 8/5/14
 * Time: 4:33 PM
 * A JavaScript identifier is the name of a variable (or a function)
 */
public class JSIdentifier implements IJSSnippetSupport {
  protected final JSSnippetSupport snippets = new JSSnippetSupport();

  protected String name;
  protected JSIdentifier apply;

  public JSIdentifier(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public JSIdentifier getApply() {
    return apply;
  }

  public void setApply(JSIdentifier apply) {
    this.apply = apply;
  }

  public boolean isKeyword(String name) {
    return (apply == null) && this.name.equals(name);
  }

  public JSSnippetSupport getSnippets() {
    return snippets;
  }

  @Override
  public String toString() {
    if (apply != null)
      return name + "." + apply.toString();
    else
      return name;
  }

  public void compose(JSWriter writer) {
    if (apply != null) {
      writer.append(name).append('.');
      apply.compose(writer);
    }
    else
      writer.append(name);
  }

  @Override
  public void setPreceding(JSSnippetSupport preceding, int precedingIndex) {
    snippets.setPreceding(preceding, precedingIndex);
  }

  @Override
  public void addSnippet(JSSnippet snippet) {
    snippets.addSnippet(snippet);
  }

  @Override
  public int currentSnippetIndex() {
    return snippets.currentSnippetIndex();
  }

  @Override
  public void augmentEndSnippet(String extraText) {
    snippets.augmentEndSnippet(extraText);
  }

  @Override
  public void composeSnippets(JSWriter writer) {
    if (apply != null) {
      JSSnippet separator = snippets.getFirstSnippetByType(JSSnippet.Type.Start);
      if (separator != null) {
        writer.append(separator.getText());
      }
      else {
        writer.append(name);
        writer.append('.');
      }
      apply.composeSnippets(writer);
    }
    else {
      writer.append(name);
    }
  }

  @Override
  public void removeExtraWhitespace() {
    snippets.removeExtraWhitespace(this);
  }

  @Override
  public void injectWhitespace(Object parentObject) {
    snippets.injectWhitespace( parentObject, this);
  }
}
