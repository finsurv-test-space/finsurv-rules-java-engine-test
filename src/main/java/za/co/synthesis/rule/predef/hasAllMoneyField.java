package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * Created by jake on 11/2/16.
 */
public class hasAllMoneyField implements IAssertionFunction {
  private IStringResolver field;

  public hasAllMoneyField(IStringResolver field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    for (int moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
      Object fieldValue = context.getMoneyField(moneyInstance, field.resolve(context, value));
      if (!(fieldValue != null && (!(fieldValue instanceof String) || ((String)fieldValue).trim().length() > 0)))
        return false;
    }
    return (context.getMoneySize() > 0);
  }
}
/*
  _export.hasAllMoneyField = function (field) {
    return function (context, value) {
      var moneyInstance;
      for (moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
        var fieldValue = context.getMoneyField(moneyInstance, field);
        if (!(fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0)))
          return false;
      }
      return context.getMoneySize() > 0;
    }
  }
*/
