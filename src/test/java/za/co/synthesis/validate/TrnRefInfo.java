package za.co.synthesis.validate;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:11 PM
 * Transaction reference related information required for testing purposes
 */
public class TrnRefInfo {
  private boolean exists;
  private boolean cancelled;
  private String flow;
  private String category;

  public TrnRefInfo(boolean exists, boolean cancelled, String flow, String category) {
    this.exists = exists;
    this.cancelled = cancelled;
    this.flow = flow;
    this.category = category;
  }

  public TrnRefInfo() {
  }

  public boolean isExists() {
    return exists;
  }

  public void setExists(boolean exists) {
    this.exists = exists;
  }

  public boolean isCancelled() {
    return cancelled;
  }

  public void setCancelled(boolean cancelled) {
    this.cancelled = cancelled;
  }

  public String getFlow() {
    return flow;
  }

  public void setFlow(String flow) {
    this.flow = flow;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }
}
