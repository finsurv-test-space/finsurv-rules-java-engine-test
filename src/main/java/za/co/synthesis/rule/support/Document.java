package za.co.synthesis.rule.support;

/**
 * Created by jake on 9/12/17.
 */
public class Document extends Rule {
  public Document(String name, String type, String description, IAssertionFunction assertion, String assertionDSL, String ruleDSL) {
    super(RuleType.Document, name, type, description, null, assertion, assertionDSL, ruleDSL);
  }
}
