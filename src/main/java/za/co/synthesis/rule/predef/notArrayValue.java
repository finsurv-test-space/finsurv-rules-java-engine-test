package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.List;

public class notArrayValue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null)
      return false;
    return !(value instanceof List);
  }
}