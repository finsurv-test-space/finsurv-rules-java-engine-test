package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.core.ResidenceStatus;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;


public class drNoResStatus implements IMatchAssertionFunction {
  @Override
  public boolean execute(MatchInfo matchInfo) {
    ResidenceStatus resStatus = matchInfo.getDrResStatus();

    return resStatus == null;
  }
}