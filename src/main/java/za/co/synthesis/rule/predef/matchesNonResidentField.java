package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 11:56 AM
 * Determines if the current value matches a NoResident field value
 */
public class matchesNonResidentField implements IAssertionFunction {
  private String field;

  public matchesNonResidentField(String field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField("NonResident.Individual." + field);
    if (fieldValue == null || fieldValue instanceof IFinsurvContext.Undefined) {
      fieldValue = context.getTransactionField("NonResident.Entity." + field);
    }
    if (fieldValue != null)
      return fieldValue.equals(value);
    else
      return false;
  }
}
/*
function matchesNonResidentField(field) {
  return function(context, value) {
    var fieldValue = context.getTransactionField("NonResident.Individual." + field);
    if (!fieldValue)
      fieldValue = context.getTransactionField("NonResident.Entity." + field);
    if (fieldValue) {
      if (fieldValue === value)
        return true;
    }
    return false;
  }
}
*/
