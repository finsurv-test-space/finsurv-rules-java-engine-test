package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

/**
 * User: jake
 * Date: 4/19/16
 * Time: 3:42 PM
 * Determines the gender from the ID Number in the given field value
 */
public class genderFromSAID implements IDisplayFunction {
  private String field;

  public genderFromSAID(String field) {
    this.field = field;
  }

  @Override
  public String execute(FinsurvContext context, Object value) {
    Object idObj;
    if (field.equals("ThirdParty.Individual.IDNumber"))
      idObj = context.getMoneyField(context.getCurrentMoneyInstance(), field);
    else
      idObj = context.getTransactionField(field);

    notEmpty notEmpty = new notEmpty();
    if (notEmpty.execute(context, idObj)) {
      String idStr = idObj.toString();
      idStr = idStr.trim();
      if (idStr.length() >= 7 && Character.isDigit(idStr.charAt(6))) {
        int genderValue = Integer.parseInt(idStr.substring(6, 7));
        if (genderValue >= 5)
          return "M";
        else
          return "F";
      }
    }
    return "";
  }
}

/*
  _export.genderFromSAID = function (field) {
    return function (context) {
      var idStr;
      if (field === "ThirdParty.Individual.IDNumber")
        idStr = context.getMoneyField(context.currentMoneyInstance, field);
      else
        idStr = context.getTransactionField(field);

      if (_export.notEmpty(context, idStr)) {
        var dateStr;
        idStr = idStr.trim();
        if (idStr.length >= 7) {
          var genderValue = Number(idStr[6]);
          if (genderValue >= 5)
            return "M";
          else
            return "F";
        }
      }
      return "";
    }
  }


*/