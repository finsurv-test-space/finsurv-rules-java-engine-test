package za.co.synthesis.rule.core;

/**
 * Created by jake on 6/7/16.
 */
public interface IClassificationContext {
  public static class Undefined {}

  boolean hasField(Object obj, final String field);

  Object getField(final String field);

  void logEvent(final String classificationType, final String ruleName, final String fieldName);
}
