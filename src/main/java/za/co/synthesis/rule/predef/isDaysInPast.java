package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif

/**
 * User: jake
 * Date: 8/8/14
 * Time: 11:30 AM
 * Determines if the current value is more than the given number of days in the past
 */
public class isDaysInPast implements IAssertionFunction {
  private int days;

  public isDaysInPast(String days) {
    this.days = Integer.parseInt(days);
  }

  public isDaysInPast(int days) {
    this.days = days;
  }

  public boolean execute(FinsurvContext context, Object value) {
    LocalDate today = context.getSupporting().getCurrentDate();
    LocalDate pastDate = today.minusDays(days);

    LocalDate date = Util.date(value);
    if (date != null && pastDate != null)
      return date.isBefore(pastDate);
    return false;
  }
}
/*
function isDaysInPast(days) {
  return function(context, value) {
    var today = new Date(rule_parameters.todayDate.getTime());
    var pastDate = new Date(rule_parameters.todayDate.getTime());
    pastDate.setDate(today.getDate() - days);

    if (typeof value === "string") {
      var dateParts = value.split("-");
      var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
      return date < pastDate;
    }
    return value < pastDate;
  }
}
*/