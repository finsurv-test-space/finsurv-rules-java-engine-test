package za.co.synthesis.rule.matching;

/**
 * Created by jake on 6/15/16.
 */
public class MFOr implements IMatchingFunction {
  private IMatchingFunction left;
  private IMatchingFunction right;

  public MFOr(IMatchingFunction left, IMatchingFunction right) {
    this.left = left;
    this.right = right;
  }

  public boolean execute(MatchingContext context) {
    return left.execute(context) || right.execute(context);
  }
}
