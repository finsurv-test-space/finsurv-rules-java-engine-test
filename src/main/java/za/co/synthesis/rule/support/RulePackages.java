package za.co.synthesis.rule.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.EngineVersion;

import java.io.Reader;
import java.util.*;

/**
 * User: jake
 * Date: 3/15/16
 * Time: 10:44 AM
 * Functionality used to read in rule package files
 */
public class RulePackages {
  Logger logger = LoggerFactory.getLogger(RulePackages.class);

  public RulePackages() {
  }

  private static EngineVersion loadEngine(JSObject jsEngine) {
    EngineVersion result = null;
    Object major = jsEngine.get("major");
    Object minor = jsEngine.get("minor");

    if (major != null) {
      result = new EngineVersion(Integer.parseInt(major.toString()),
              minor != null ? Integer.parseInt(minor.toString()) : 0);
    }
    return result;
  }

  public static RulePackage loadRulePackage(JSObject jsPackage) {
    RulePackage result = null;

    Object dependsOn = jsPackage.get("dependsOn");
    Object features = jsPackage.get("features");
    Object engine = jsPackage.get("engine");
    Object evaluationEngine = jsPackage.get("evaluationEngine");
    Object mappings = jsPackage.get("mappings");

    if (engine instanceof JSObject) {
      EngineVersion engineVersion = loadEngine((JSObject)engine);

      List<String> featureList = new ArrayList<String>();
      if (features instanceof String) {
        featureList.add((String)features);
      }
      else
      if (features instanceof JSArray) {
        JSArray jsFeatures = (JSArray)features;
        for (Object objFeature : jsFeatures) {
          if (objFeature instanceof String) {
            featureList.add((String)objFeature);
          }
        }
      }
      Map<String, String> map = new HashMap<String, String>();
      if (mappings instanceof JSObject) {
        JSObject jsMappings = (JSObject)mappings;
        for (Map.Entry<String, Object> entry : jsMappings.entrySet()) {
          if (entry.getValue() != null) {
            map.put(entry.getKey(), entry.getValue().toString());
          }
        }
      }

      result = new RulePackage(engineVersion, dependsOn != null ? dependsOn.toString() : null, featureList, map);
      if (evaluationEngine != null) {
        EngineVersion evaluationVersion = loadEngine((JSObject)evaluationEngine);
        result.setEvaluationEngine(evaluationVersion);
      }
    }
    return result;
  }

  public static RulePackage loadRulePackage(final Reader reader) throws Exception {
    JSStructureParser parser = new JSStructureParser(reader);

    Object obj = parser.parse();

    JSObject packageObj = JSUtil.findObjectWith(obj, "engine");
    if (packageObj != null) {
      return loadRulePackage(packageObj);
    }
    return null;
  }
}
