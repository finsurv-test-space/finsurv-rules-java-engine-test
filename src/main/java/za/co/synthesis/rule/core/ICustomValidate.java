package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 2/25/15
 * Time: 5:27 PM
 * The callback interface to be implemented by all validation callback functions
 */
public interface ICustomValidate {
  CustomValidateResult call(Object value, Object... otherInputs);
}
