package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 11:04 AM
 * Determines if the current value starts with the given transaction field value
 */
public class startsWithTransactionField implements IAssertionFunction {
  private String field;

  public startsWithTransactionField(String field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField(field);
    if (value != null && fieldValue != null) {
      if (value.toString().startsWith(fieldValue.toString()))
        return true;
    }
    return false;
  }
}
/*
function startsWithTransactionField(field) {
  return function(context, value) {
    var fieldValue = context.getTransactionField(field);
    if (value && fieldValue) {
      if (value.indexOf(fieldValue) === 0)
        return true;
    }
    return false;
  }
}
*/