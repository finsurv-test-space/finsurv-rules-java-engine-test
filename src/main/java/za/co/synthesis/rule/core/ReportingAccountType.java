package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 3/4/15
 * Time: 1:20 PM
 * The list of account types expected by the SARB
 */
public enum ReportingAccountType {
  Unknown(""),
  RE_OTH("RESIDENT OTHER"),
  RE_CFC("CFC RESIDENT"),
  RE_CFDC("CFDC RESIDENT"),
  RE_FCA("FCA RESIDENT"),
  RE_FCDA("FCDA RESIDENT"),
  CASH("CASH"),
  VOSTRO("VOSTRO"),
  NR_OTH("NON RESIDENT OTHER"),
  NR_RND("NON RESIDENT RAND"),
  NR_NAD("NON RESIDENT NAD"),
  NR_KWT("NON RESIDENT KWACHA"),
  NR_LSL("NON RESIDENT DOMESTIC CURRENCY"),
  NR_FCA("NON RESIDENT FCA"),
  NR_FCDA("NON RESIDENT FCDA"),
  RE_FBC("RES FOREIGN BANK ACCOUNT");

  private final String name;
  ReportingAccountType(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return name;
  }
}
