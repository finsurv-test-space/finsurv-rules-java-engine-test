package za.co.synthesis.rule.core;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.impl.JSMatchingContext;
import za.co.synthesis.rule.matching.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/16/16.
 */
public class Sharder {
  private final List<MatchBucket> rules;

  public Sharder(List<MatchBucket> rules) {
    this.rules = rules;
  }

  private List<ShardResult> runShard(IMatchingContext context, List<MatchBucket> rules, String rulename) {
    List<ShardResult> result = new ArrayList<ShardResult>();
    for (MatchBucket rule : rules) {
      if (rulename == null || rulename.equals(rule.getName())) {
        IShardFunction shard = rule.getShard();
        if (shard != null) {
          result.add(new ShardResult(rule.getName(), shard.execute(context)));
        }
      }
    }
    return result;
  }

  public List<ShardResult> shardRule(String rulename, JSObject jsObj) {
    JSMatchingContext jsContext = new JSMatchingContext(jsObj);

    return runShard(jsContext, rules, rulename);
  }

  public List<ShardResult> shard(JSObject jsObj) {
    JSMatchingContext jsContext = new JSMatchingContext(jsObj);

    return runShard(jsContext, rules, null);
  }

  public List<ShardResult> shardRule(String rulename, IMatchingContext ctx) {
    return runShard(ctx, rules, rulename);
  }

  public List<ShardResult> shard(IMatchingContext ctx) {
    return runShard(ctx, rules, null);
  }
}
