package za.co.synthesis.rule.predef;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jake
 * Date: 4/20/16
 * Time: 9:37 AM
 * Stripping characters or strings based on a regex pattern
 */
public class stripRegex implements IDisplayFunction {
  private final Pattern pattern;

  public stripRegex(String regex) {
    this.pattern = Pattern.compile(regex);
  }

  public stripRegex(JSRegExLiteral regex) {
    this.pattern = regex.getPattern();
  }

  @Override
  public String execute(FinsurvContext context, Object value) {
    if (value != null) {
      Matcher matcher = pattern.matcher(value.toString());
      return matcher.replaceAll("");
    }
    return null;
  }
}
