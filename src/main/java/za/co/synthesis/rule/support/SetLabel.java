package za.co.synthesis.rule.support;

import java.util.List;

/**
 * Created by jake on 4/11/16.
 */
public class SetLabel extends DisplayRule {
  public SetLabel(IDisplayFunction displayFunction, IAssertionFunction assertion) {
    super(DisplayRuleType.SetLabel, null, null, displayFunction, assertion);
  }
}
