package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 12:34 PM
 * Determine if there are no monetary amount entries
 */
public class emptyMoneyField implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return context.getMoneySize() == 0;
  }
}
/*
function emptyMoneyField(context, value) {
  return context.getMoneySize() == 0;
}
*/
