package za.co.synthesis.rule.evaluation.predef;

import za.co.synthesis.rule.evaluation.GivenInfo;
import za.co.synthesis.rule.evaluation.IAssumptionAssertionFunction;

/**
 * Created by jake on 6/28/16.
 */
public class transferCURR implements IAssumptionAssertionFunction {
  @Override
  public boolean execute(GivenInfo givenInfo) {
    if (givenInfo.getCurrency() != null && givenInfo.getContext().getLocalCurrencyRegex() != null) {
      return !givenInfo.getCurrency().matches(givenInfo.getContext().getLocalCurrencyRegex());
    }
    return false;
  }
}
