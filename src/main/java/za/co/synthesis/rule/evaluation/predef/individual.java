package za.co.synthesis.rule.evaluation.predef;

import za.co.synthesis.rule.core.AccountHolderStatus;
import za.co.synthesis.rule.evaluation.GivenInfo;
import za.co.synthesis.rule.evaluation.IAssumptionAssertionFunction;

/**
 * Created by jake on 6/28/16.
 */
public class individual implements IAssumptionAssertionFunction {
  @Override
  public boolean execute(GivenInfo givenInfo) {
    if (givenInfo.getAccountHolderStatus() != null) {
      return givenInfo.getAccountHolderStatus() == AccountHolderStatus.Individual;
    }
    return false;
  }
}
