package za.co.synthesis.rule.core;

/**
 * Created by jake on 6/7/16.
 */
public class ClassificationException extends Exception {
  public ClassificationException(String message) {
    super(message);
  }

  public ClassificationException(String message, java.lang.Throwable throwable) {
    super(message, throwable);
  }
}
