package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 3:04 PM
 * Determines if the current value is not a valid province name
 */
public class notValidProvince implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !context.getLookups().isValidProvince(value.toString());
  }
}

/*
function notValidProvince(context, value) {
  if (!value)
    return true;
  return !context.lookups.isValidProvince(value);
}
*/