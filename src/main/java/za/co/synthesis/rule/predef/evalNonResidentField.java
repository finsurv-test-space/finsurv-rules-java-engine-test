package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * Created by jake on 11/4/16.
 */
public class evalNonResidentField implements IAssertionFunction {
  private IStringResolver field;
  private IAssertionFunction fEval = null;

  public evalNonResidentField(IStringResolver field, IAssertionFunction fEval) {
    this.field = field;
    this.fEval = fEval;
  }

  public boolean execute(FinsurvContext context, Object value) {
    String fieldStr = field.resolve(context, value);
    Object fieldValue = context.getTransactionField("NonResident.Individual." + fieldStr);
    if (fieldValue == null || fieldValue instanceof IFinsurvContext.Undefined) {
      fieldValue = context.getTransactionField("NonResident.Entity." + fieldStr);
    }
    if (fieldValue != null && fEval != null) {
      return fEval.execute(context, fieldValue);
    }
    return false;
  }
}
