package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 5:38 AM
 * Determines if the given value is greater than or equal to 0
 */
public class isPositive implements IAssertionFunction {
  private BigDecimal zero = new BigDecimal(0);
  public boolean execute(FinsurvContext context, Object value) {
    if (value != null) {
      // BigDecimal.compareTo: -1, 0, or 1 as this BigDecimal is numerically less than, equal to, or greater than val.
      BigDecimal valueNum = Util.number(value);
      if (valueNum != null) {
        int compareToZero = valueNum.compareTo(zero);
        return compareToZero == 1 || compareToZero == 0;
      }
    }
    return false;
  }
}
/*
function isPositive(context, value) {
  var test = Number(value);
  return value && value >= 0;
}
*/