package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

/**
 * User: jake
 * Date: 4/19/16
 * Time: 10:01 PM
 * Formats the value returned by the given display function
 */
public class formatDisplayFunctionResult implements IDisplayFunction {
  private final String format;
  private final IDisplayFunction displayFunction;

  public formatDisplayFunctionResult(String format, IDisplayFunction displayFunction) {
    this.format = format;
    this.displayFunction = displayFunction;
  }

  @Override
  public String execute(FinsurvContext context, Object value) {
    if (format != null && format.contains("%s") && displayFunction != null) {
      Object obj = displayFunction.execute(context, value);
      String fieldValue = obj != null ? obj.toString() : null;
      return format.replace("%s", fieldValue);
    }
    return format;
  }
}
