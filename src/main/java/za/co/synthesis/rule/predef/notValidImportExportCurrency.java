package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 3:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class notValidImportExportCurrency implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    Object firstCurr = context.getImportExportField(context.getCurrentMoneyInstance(), 0, "PaymentCurrencyCode");
    if (firstCurr == null || firstCurr instanceof IFinsurvContext.Undefined)
      return true;

    String randCurr = null;
    String foreignCurr = context.getCurrency();

    Object randValue = context.getMoneyField(context.getCurrentMoneyInstance(), "RandValue");
    if (randValue != null && !(randValue instanceof IFinsurvContext.Undefined)) {
      randCurr = "ZAR";
    }
    if ((foreignCurr != null && firstCurr.equals(foreignCurr)) || (randCurr != null && firstCurr.equals(randCurr))) {
      if (!firstCurr.equals(value))
        return true;
    } else
      return true;

    return false;
  }
}

/*
function notValidImportExportCurrency(context, value) {
  var firstCurr = context.getImportExportField(context.currentMoneyInstance, 0, "PaymentCurrencyCode");
  if (!firstCurr)
    return true;

  var randCurr;
  var foreignCurr = context.getTransactionField("FlowCurrency");

  var randValue = context.getMoneyField(context.currentMoneyInstance, "RandValue");
  if (randValue) {
    randCurr = "ZAR";
  }
  if ((foreignCurr && firstCurr === foreignCurr) || (randCurr && firstCurr === randCurr)) {
    if (firstCurr !== value)
      return true;
  } else
    return true;

  return false;
}
*/