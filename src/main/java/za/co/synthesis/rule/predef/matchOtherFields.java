package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 12:30 PM
 * Determine if the given two transaction fields have matching values
 */
public class matchOtherFields implements IAssertionFunction {
  private String fieldA;
  private String fieldB;

  public matchOtherFields(String fieldA, String fieldB) {
    this.fieldA = fieldA;
    this.fieldB = fieldB;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValueA = context.getTransactionField(fieldA);
    Object fieldValueB = context.getTransactionField(fieldB);
    if (fieldValueA != null)
      return fieldValueA.equals(fieldValueB);
    else
      return false;
  }
}
/*
function matchOtherFields(fieldA, fieldB) {
  return function(context, value) {
    // we won't be using this field's value, we're matching other fields
    var fieldValueA = context.getTransactionField(fieldA);
    var fieldValueB = context.getTransactionField(fieldB);
    return (fieldValueA === fieldValueB);
  }
}
*/