package za.co.synthesis;

import org.junit.Test;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.difference.DSLChannelDifferences;
import za.co.synthesis.rule.difference.DSLDifferenceOutput;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PackageDifferencesTest {
  private static String oldRepoPath = "./src/test/resources/test_repo_old";
  private static String newRepoPath = "./src/test/resources/test_repo_new";
  private static String htmlOutputPath = "./out/testPackageDiffs.html";

  private static void outputHTML(JSObject jsDoc, String channel) throws IOException {
    Files.createDirectories(Paths.get(htmlOutputPath).getParent());
    FileWriter html = new FileWriter(htmlOutputPath);
    html.write("<html>\n");
    html.write("<head><title>Differences between test repo packages</title></head>\n");
    html.write("<style>\n");
    html.write("  del {background-color: pink; text-decoration: line-through;}\n");
    html.write("  ins {background-color: yellow; text-decoration: none;}\n");
    html.write("  pre {\n");
    html.write("    background: #f4f4f4;\n");
    html.write("    border: 1px solid #ddd;\n");
    html.write("  border-left: 3px solid #f36d33;\n");
    html.write("  color: #666;\n");
    html.write("  page-break-inside: avoid;\n");
    html.write("  font-family: monospace;\n");
    html.write("  font-size: 15px;\n");
    html.write("  line-height: 1.6;\n");
    html.write("  margin-bottom: 1.6em;\n");
    html.write("  max-width: 100%;\n");
    html.write("  overflow: auto;\n");
    html.write("  padding: 1em 1.5em;\n");
    html.write("  display: block;\n");
    html.write("  word-wrap: break-word;\n");
    html.write("}\n");
    html.write("</style>\n");
    html.write("<body>\n");
    html.write("<p><b>Package Mapping Changes:</b>\n");
    html.write("<pre>\n");
    JSObject jsChannel = (JSObject)jsDoc.get(channel);
    JSArray jsList = (JSArray)jsChannel.get("package");
    for (Object obj : jsList) {
      if (obj instanceof JSObject) {
        JSObject jsObj = (JSObject)obj;
        html.write(jsObj.get("diffHTML").toString());
        html.write("\n");
      }
    }
    html.write("</pre>\n");
    html.write("<p><b>Message Changes:</b>\n");
    html.write("<pre>\n");
    jsList = (JSArray)jsChannel.get("message");
    for (Object obj : jsList) {
      if (obj instanceof JSObject) {
        JSObject jsObj = (JSObject)obj;
        html.write(jsObj.get("diffHTML").toString());
        html.write("\n");
      }
    }
    html.write("</pre>\n");
    html.write("<p><b>Field Changes:</b>\n");
    html.write("<pre>\n");
    jsList = (JSArray)jsChannel.get("field");
    for (Object obj : jsList) {
      if (obj instanceof JSObject) {
        JSObject jsObj = (JSObject)obj;
        html.write(jsObj.get("diffHTML").toString());
        html.write("\n");
      }
    }
    html.write("</pre>\n");
    html.write("<p><b>Rule Changes:</b>\n");
    html.write("<pre>\n");
    jsList = (JSArray)jsChannel.get("rule");
    String lastHeading = "";
    for (Object obj : jsList) {
      if (obj instanceof JSObject) {
        JSObject jsObj = (JSObject)obj;
        String heading = "Scope: " + JSUtil.getString(jsObj, "newValue.scope") + ",  fields: " + JSUtil.getString(jsObj, "newValue.fields");
        if (!lastHeading.equals(heading)) {
          lastHeading = heading;
          html.write("<b><u>" + heading+ "</u></b>\n");
        }
        html.write(jsObj.get("diffHTML").toString());
        html.write("\n");
      }
    }
    html.write("</pre>\n");
    html.write("<p><b>Document Changes:</b>\n");
    html.write("<pre>\n");
    jsList = (JSArray)jsChannel.get("document");
    lastHeading = "";
    for (Object obj : jsList) {
      if (obj instanceof JSObject) {
        JSObject jsObj = (JSObject)obj;
        String heading = "Scope: " + JSUtil.getString(jsObj, "newValue.scope") + ",  fields: " + JSUtil.getString(jsObj, "newValue.fields");
        if (!lastHeading.equals(heading)) {
          lastHeading = heading;
          html.write("<b><u>" + heading+ "</u></b>\n");
        }
        html.write(jsObj.get("diffHTML").toString());
        html.write("\n");
      }
    }
    html.write("</pre>\n");
    html.write("</body>\n");
    html.write("</html>\n");
    html.close();
  }

  @Test
  public void compareDifferences() throws Exception {
    DSLChannelDifferences diffs = new DSLChannelDifferences(oldRepoPath, newRepoPath);

    JSObject jsDoc = new JSObject();
    String channel = "channel1";
    diffs.addValidationDifferences(channel, jsDoc);
    diffs.addDocumentDifferences(channel, jsDoc);

    JSWriter writer = new JSWriter();
    writer.setIndent("  ");
    writer.setNewline("\n");

    jsDoc.compose(writer);
    System.out.println(writer.toString());

    outputHTML(jsDoc, channel);
  }

  @Test
  public void outputDifferences() throws Exception {
    String templatePath = "./src/main/resources/config/DSLDifferenceTemplate.html"; //"classpath:config/DSLDifferenceTemplate.html";

    DSLDifferenceOutput.repoDifferences(oldRepoPath, newRepoPath, htmlOutputPath, templatePath);
  }

  // @Test // Set scratchPad to a test if you want to debug or develop repoDifferences
  public void scratchPad() throws Exception {
    String templatePath = "./src/main/resources/config/DSLDifferenceTemplate.html"; //"classpath:config/DSLDifferenceTemplate.html";

  //  String oldRepoPath = "/Users/jakeshepherd/Work/Synthesis/rules-management/run/custom/synthesis";
  //    String newRepoPath = "/Users/jakeshepherd/Work/Synthesis/rules-management/run/custom/dev";

    String oldRepoPath = "./temp/previous_release/finsurv-rules/reg_rule_repo";
    String newRepoPath = "./temp/current_release/finsurv-rules/reg_rule_repo";

    DSLDifferenceOutput.repoDifferences(oldRepoPath, newRepoPath, htmlOutputPath, templatePath);
  }
}
