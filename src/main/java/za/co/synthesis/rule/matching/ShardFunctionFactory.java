package za.co.synthesis.rule.matching;

import java.lang.reflect.Constructor;

/**
 * Created by jake on 6/16/16.
 */
public class ShardFunctionFactory {
  private static final String functionPackage = "za.co.synthesis.rule.matching.predef.";

  public static IShardFunction createFunction(final String name) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    return (IShardFunction)cls.newInstance();
  }

  public static IShardFunction createFunction(final String name, final String param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class);
    return (IShardFunction)clsConstructor.newInstance(param1);
  }

  public static IShardFunction createFunction(final String name, final String param1, final String param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class);
    return (IShardFunction)clsConstructor.newInstance(param1, param2);
  }
}
