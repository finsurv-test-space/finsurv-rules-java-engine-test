package za.co.synthesis.rule.support;

import java.util.List;

/**
 * Created by jake on 4/10/16.
 */
public class ExcludeValue extends DisplayRule {
  public ExcludeValue(List<String> values, IAssertionFunction assertion) {
    super(DisplayRuleType.ExcludeValue, null, values, null, assertion);
  }
}
