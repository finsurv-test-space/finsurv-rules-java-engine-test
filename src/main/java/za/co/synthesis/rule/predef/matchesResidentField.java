package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 12:08 PM
 * Determines if the current value matches a NoResident field value
 */
public class matchesResidentField implements IAssertionFunction {
  private String field;

  public matchesResidentField(String field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField("Resident.Individual." + field);
    if (fieldValue == null || fieldValue instanceof IFinsurvContext.Undefined) {
      fieldValue = context.getTransactionField("Resident.Entity." + field);
    }
    if (fieldValue != null)
      return fieldValue.equals(value);
    else
      return false;
  }
}
/*
function matchesResidentField(field) {
  return function(context, value) {
    var fieldValue = context.getTransactionField("Resident.Individual." + field);
    if (!fieldValue)
      fieldValue = context.getTransactionField("Resident.Entity." + field);
    if (fieldValue) {
      if (fieldValue === value)
        return true;
    }
    return false;
  }
}
*/
