package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class notChecksumImportExport implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (context.getImportExportSize(context.getCurrentMoneyInstance()) > 0) {
      BigDecimal total = new BigDecimal(0);
      BigDecimal moneyValue;
      for (int i = 0; i < context.getImportExportSize(context.getCurrentMoneyInstance()); i++) {
        BigDecimal fvalue = Util.number(context.getImportExportField(context.getCurrentMoneyInstance(), i, "PaymentValue"));
        if (fvalue != null)
          total = total.add(fvalue);
      }
      Object payCurrCode = context.getImportExportField(context.getCurrentMoneyInstance(), 0, "PaymentCurrencyCode");
      if (payCurrCode != null && payCurrCode.equals(context.map("LocalCurrency"))) {
        moneyValue = Util.number(context.getMoneyField(context.getCurrentMoneyInstance(),context.map("LocalValue")));
        String flowCurr = context.getCurrency();
        if (moneyValue == null && flowCurr.equals(context.map("LocalCurrency")))
          moneyValue = Util.number(context.getMoneyField(context.getCurrentMoneyInstance(), "ForeignValue"));
      }
      else
        moneyValue = Util.number(context.getMoneyField(context.getCurrentMoneyInstance(), "ForeignValue"));

      if (moneyValue == null)
        moneyValue = new BigDecimal(0);
      return total.subtract(moneyValue).abs().compareTo(moneyValue.divide(new BigDecimal(100))) == 1;
    }
    return false;
  }
}
/*
function notChecksumImportExport(context, value) {
  if (context.getImportExportSize(context.currentMoneyInstance) > 0) {
    var total = 0;
    var moneyValue;
    for ( var i = 0; i < context.getImportExportSize(context.currentMoneyInstance); i++) {
      var fvalue = context.getImportExportField(context.currentMoneyInstance, i, "PaymentValue");
      if (fvalue)
        total += Number(fvalue);
    }
    if (context.getImportExportField(context.currentMoneyInstance, 0, "PaymentCurrencyCode") === "ZAR") {
      moneyValue = context.getMoneyField(context.currentMoneyInstance, "RandValue");
      var flowCurr = context.getTransactionField("FlowCurrency");
      if (!moneyValue && flowCurr === "ZAR")
        moneyValue = context.getMoneyField(context.currentMoneyInstance, "ForeignValue");
    }
    else
      moneyValue = context.getMoneyField(context.currentMoneyInstance, "ForeignValue");

    return Math.abs(total - moneyValue) > (moneyValue / 100);
  }
  return false;
}
*/