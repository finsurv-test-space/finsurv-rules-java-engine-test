package za.co.synthesis.rule.evaluation.predef;

import za.co.synthesis.rule.core.CounterpartyInstituition;
import za.co.synthesis.rule.evaluation.EvaluationUtil;
import za.co.synthesis.rule.evaluation.GivenInfo;
import za.co.synthesis.rule.evaluation.IAssumptionAssertionFunction;

/**
 * Created by jake on 8/29/17.
 */
public class localBankAD implements IAssumptionAssertionFunction {
  @Override
  public boolean execute(GivenInfo givenInfo) {
    CounterpartyInstituition cptyType = givenInfo.getCounterpartyInstituition();
    if (cptyType == null) {
      cptyType = EvaluationUtil.getCounterpartyInstituitionByBIC(givenInfo.getContext(), givenInfo.getCounterpartyInstituitionBIC());
    }

    if (cptyType != null) {
      return cptyType == CounterpartyInstituition.OnshoreAD;
    }
    return false;
  }
}