package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 12:59 PM
 * Determine if there are no import/export entries for the current monetary amount
 */
public class emptyImportExport implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return context.getImportExportSize(context.getCurrentMoneyInstance()) == 0;
  }
}
/*
function emptyImportExport(context, value) {
  return context.getImportExportSize(context.currentMoneyInstance) == 0;
}
*/

