package za.co.synthesis.rule.support;

import za.co.synthesis.rule.predef.alwaysFalse;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 12:28 PM
 * This rule is used when another rule (loaded later in time) with the same name needs to be ignored
 */
public class Ignore extends Rule {
  public Ignore(String name, String ruleDSL) {
    super(RuleType.Ignore, name, "XXX", "Ignore me", null, new alwaysFalse(), null, ruleDSL);
  }
}