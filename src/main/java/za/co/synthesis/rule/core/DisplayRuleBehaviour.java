package za.co.synthesis.rule.core;

/**
 * Created by jake on 6/1/16.
 */
public class DisplayRuleBehaviour {
  private boolean filterUnchangedValues = true;
  private boolean setOriginalValueOnFailedAssertion = false;
  private boolean convertAppendToSet = true;

  public boolean isFilterUnchangedValues() {
    return filterUnchangedValues;
  }

  public void setFilterUnchangedValues(boolean filterUnchangedValues) {
    this.filterUnchangedValues = filterUnchangedValues;
  }

  public boolean isSetOriginalValueOnFailedAssertion() {
    return setOriginalValueOnFailedAssertion;
  }

  public void setSetOriginalValueOnFailedAssertion(boolean setOriginalValueOnFailedAssertion) {
    this.setOriginalValueOnFailedAssertion = setOriginalValueOnFailedAssertion;
  }

  public boolean isConvertAppendToSet() {
    return convertAppendToSet;
  }

  public void setConvertAppendToSet(boolean convertAppendToSet) {
    this.convertAppendToSet = convertAppendToSet;
  }
}

