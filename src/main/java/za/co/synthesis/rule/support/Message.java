package za.co.synthesis.rule.support;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 12:32 PM
 * This rule allows the updating of the message associate with the given rule name. This means that there is no need
 * to refine the rule if just tne message needs to be updated
 */
public class Message extends Rule {
  public Message(String name, String code, String message, String ruleDSL) {
    super(RuleType.Message, name, code, message, null, null, null, ruleDSL);
  }
}
