package za.co.synthesis.rule.support;

public interface ICalculationFunction {
    Object execute(FinsurvContext context, Object value);
}
