package za.co.synthesis.rule.support;

import java.util.List;

/**
 * Created by jake on 4/11/16.
 */
public class AppendValue extends DisplayRule {
  public AppendValue(IDisplayFunction displayFunction, IAssertionFunction assertion) {
    super(DisplayRuleType.AppendValue, null, null, displayFunction, assertion);
  }
}
