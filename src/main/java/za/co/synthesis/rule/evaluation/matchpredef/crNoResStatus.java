package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.core.ResidenceStatus;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;

public class crNoResStatus implements IMatchAssertionFunction {
  @Override
  public boolean execute(MatchInfo matchInfo) {
    ResidenceStatus resStatus = matchInfo.getCrResStatus();

    return resStatus == null;
  }
}