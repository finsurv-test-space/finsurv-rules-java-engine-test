package za.co.synthesis.rule.support;


/**
 * Created by jake on 4/10/16.
 */
public class Enable extends DisplayRule {
  public Enable(IAssertionFunction assertion) {
    super(DisplayRuleType.Enable, null, null, null, assertion);
  }
}
