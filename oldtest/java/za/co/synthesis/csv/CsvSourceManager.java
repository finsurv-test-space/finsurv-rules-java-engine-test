package za.co.synthesis.csv;

import com.opencsv.CSVReader;
import za.co.synthesis.mapping.configuration.Field;
import za.co.synthesis.mapping.configuration.MappingConfig;
import za.co.synthesis.mapping.configuration.MappingScope;
import za.co.synthesis.mapping.configuration.Section;
import za.co.synthesis.mapping.source.ISourceData;
import za.co.synthesis.mapping.source.ISourceManager;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 4:55 PM
 * A CSV-specific implementation of the ISourceManager that is used to manage the access to the rows of source data.
 */
public class CsvSourceManager implements ISourceManager {
  private CSVReader reader;
  private String[] headers;
  private String sourceName = null;

  public void openFile(String csvFileName) throws Exception {
    this.reader = new CSVReader(new FileReader(csvFileName));

    this.headers = reader.readNext();
  }

  public void closeFile() throws Exception {
    this.reader.close();
  }

  @Override
  public void setup(MappingConfig mappingConfig) {
    List<String> errorMappings = new ArrayList<String>();

    if (mappingConfig.getSources().size() == 1)
      sourceName = mappingConfig.getSources().get(0).getCode();

    // setup Column Indexes
    Map<String, Integer> headerMap = new HashMap<String, Integer>();

    for (int i=0; i<headers.length; i++) {
      headerMap.put(headers[i], i);
    }

    for (Section section : mappingConfig.getSections()) {
      for (Field field : section.getFields()) {
        boolean isDate = false;
        if (field.getDataType() != null && field.getDataType().equals("date")) {
          isDate = true;
        }
        if (headerMap.containsKey(field.getSourceColName())) {
          field.setExternalConfig(new CsvExternalConfig(headerMap.get(field.getSourceColName()), isDate));
        }
        else {
          if (!errorMappings.contains(field.getSourceColName()))
            errorMappings.add(field.getSourceColName());
        }
      }
    }

    if (errorMappings.size() > 0) {
      System.out.println("** Configuration Errors: Columns do not exists in source **");
      for (String colName : errorMappings) {
        System.out.println(colName);
      }
    }
  }

  @Override
  public Object getNextEntry() {
    String[] nextLine = null;
    try {
      nextLine = reader.readNext();
    } catch (IOException e) {
    }
    return nextLine;
  }

  @Override
  public ISourceData getSourceData(Object entry, String source, MappingScope mappedScope) {
    if (source.equals(sourceName)) {
      return new CsvSourceData((String[])entry);
    }
    return null;
  }
}
