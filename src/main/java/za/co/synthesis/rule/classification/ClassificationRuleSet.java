package za.co.synthesis.rule.classification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/7/16.
 */
public class ClassificationRuleSet {
  private String ruleset;
  private List<FieldClassification> classifications = new ArrayList<FieldClassification>();

  public ClassificationRuleSet(String ruleset) {
    this.ruleset = ruleset;
  }

  public ClassificationRuleSet addClassification(FieldClassification classification) {
    classifications.add(classification);
    return this;
  }

  public String getRuleset() {
    return ruleset;
  }

  public List<FieldClassification> getClassifications() {
    return classifications;
  }
}
