package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.core.IMatchingContext;
import za.co.synthesis.rule.matching.IShardFunction;

/**
 * Created by jake on 6/16/16.
 */
public class numberSpan implements IShardFunction {
  private final String fieldname;
  private final double span;

  public numberSpan(String fieldname, String span) {
    this.fieldname = fieldname;
    this.span = Double.parseDouble(span);
  }

  @Override
  public String[] execute(IMatchingContext context) {
    String[] result = new String[2];
    Object val = context.getField(fieldname);

    double amount = Double.parseDouble(val.toString());
    long bucket = Math.round((amount-span/2) / span);
    result[0] = Double.toString(bucket);
    result[1] = Double.toString(bucket+1);
    return result;
  }
}
