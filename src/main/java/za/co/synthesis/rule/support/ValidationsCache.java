package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.FlowType;
import za.co.synthesis.rule.core.ValidationSchema;

import java.util.*;

/**
 * User: jake
 * Date: 8/10/14
 * Time: 1:56 PM
 * The validations cache stores the most recently used validation sets so that there is no need to constantly generate
 * validation rule lists for common use cases.
 */
public class ValidationsCache implements IValidationRules {
  private static class CacheEntry {
    private int usageCount;
    private long startMillis;
    private IValidationRules.ValidationList list;

    private CacheEntry(ValidationList list) {
      this.list = list;
      this.usageCount = 1;
      this.startMillis = System.currentTimeMillis();
    }

    public int getUsageCount() {
      return usageCount;
    }

    /**
     * The affinity gives a value of approximately usages per second. The more a validation cache entry is used the
     * higher its affinity
     * @param currentMillis - the current milliseconds as per System.currentTimeMillis()
     * @return
     */
    public double getAffinity(long currentMillis) {
      return (double)usageCount*1000/(currentMillis - startMillis);
    }

    public ValidationList getList() {
      usageCount++;
      return list;
    }
  }

  private final Map<String, CacheEntry> cache = new HashMap<String, CacheEntry>();
  private final IValidationRules parent;
  private final int maxCacheSize;

  public ValidationsCache(IValidationRules validationRules, int maxCacheSize) {
    this.parent = validationRules;
    this.maxCacheSize = maxCacheSize;
  }

  private String getKey(FlowType flow, String section, List<String> categories) {
    StringBuilder sb = new StringBuilder();

    List<String> sortedCategories = new ArrayList<String>();
    if (categories != null) {
      for (String cat : categories) {
        if (cat != null) {
          sortedCategories.add(cat);
        }
      }
      Collections.sort(sortedCategories);

      String lastCat = null;
      for (String cat : sortedCategories) {
        if (lastCat == null || !lastCat.equals(cat)) {
          lastCat = cat;
          sb.append(cat);
          sb.append('|');
        }
      }
    }
    sb.append(flow == FlowType.Inflow ? "I" : "O");
    sb.append('@');
    sb.append(section);
    return sb.toString();
  }

  private void pruneCache() {
    if (cache.size() > maxCacheSize) {
      long currentMillis = System.currentTimeMillis();

      synchronized (cache) {
        double smallestAffinity = 0;
        String saCacheEntry = null;
        for (Map.Entry<String, CacheEntry> entry : cache.entrySet()) {
          if (saCacheEntry == null) {
            smallestAffinity = entry.getValue().getAffinity(currentMillis);
            saCacheEntry = entry.getKey();
          }
          else {
            double affinity = entry.getValue().getAffinity(currentMillis);
            if (affinity < smallestAffinity) {
              smallestAffinity = affinity;
              saCacheEntry = entry.getKey();
            }
          }
        }

        cache.remove(saCacheEntry);
      }
    }
  }

  @Override
  public ValidationList validationRules(Map<String, String> mapping, FlowType flow, String section, List<String> categories, List<String> mainCategories, String rulename) {
    if (rulename != null)
      return parent.validationRules(mapping, flow, section, categories, mainCategories, rulename);

    String key = getKey(flow, section, categories);

    boolean hasKey;
    synchronized (cache) {
      hasKey = cache.containsKey(key);
    }

    if (!hasKey) {
      ValidationList list = parent.validationRules(mapping, flow, section, categories, mainCategories, rulename);

      synchronized (cache) {
        if (!cache.containsKey(key)) {
          cache.put(key, new CacheEntry(list));
        }
      }
      pruneCache();
      return list;
    }
    else {
      synchronized (cache) {
        if (cache.containsKey(key)) {
          return cache.get(key).getList();
        }
        else {
          ValidationList list = parent.validationRules(mapping, flow, section, categories, mainCategories, rulename);
          cache.put(key, new CacheEntry(list));
          pruneCache();
          return list;
        }
      }
    }
  }
}
