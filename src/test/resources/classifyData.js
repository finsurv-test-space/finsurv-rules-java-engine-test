define(['require', './predef'], function (require,  predef) {
  var classify;

  with (predef) {
    classify = {
      ruleset: "Standard Bank Account Entry Classification",
      classifications : [
        {
          field  : "Account.Reference",
          rules: [
            include("inc001", hasFieldValue("Account.System", "FORX").and(hasValue("3012")))
          ]
        },
        {
          field  : "BusinessTrnCode",
          rules: [
            exclude("exc001", hasValue("IDDZ")),
            zz2("nr001", hasValue("NMGO"))
          ]
        }
      ]
    }
  }
});
