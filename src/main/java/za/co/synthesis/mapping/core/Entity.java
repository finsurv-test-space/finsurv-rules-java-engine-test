package za.co.synthesis.mapping.core;

import java.util.Map;
import za.co.synthesis.javascript.*;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 4:38 PM
 * The destination entity with contained JavaScript object and associated meta data
 */
public class Entity {
  private final JSObject jsObject;
  private final JSArray jsResults;
  private final Map<String, String> metaData;

  public Entity(JSObject jsObject, Map<String, String> metaData) {
    this.jsObject = jsObject;
    this.jsResults = null;
    this.metaData = metaData;
  }

  public Entity(JSObject jsObject, JSArray jsResults, Map<String, String> metaData) {
    this.jsObject = jsObject;
    this.jsResults = jsResults;
    this.metaData = metaData;
  }

  public JSObject getJsObject() {
    return jsObject;
  }

  public Object getJsValue(String name) {
    JSObject jsObj = jsObject;
    if (jsObj != null && name != null) {
      String[] parts = name.split("\\.");
      String partName;
      for (int i=0; i<parts.length - 1; i++) {
        partName = parts[i];
        if (jsObj != null)
          jsObj = (JSObject) jsObj.get(partName);
        else
          break;
      }

      if (jsObj != null) {
        partName = parts[parts.length - 1];
        return jsObj.get(partName);
      }
    }
    return null;
  }

  public Object getAnyJsValue(String name1, String name2) {
    Object result = getJsValue(name1);
    if (result != null)
      return result;
    return getJsValue(name2);
  }

  public Map<String, String> getMetaData() {
    return metaData;
  }

  public JSArray getJsResults() {
    return jsResults;
  }
}
