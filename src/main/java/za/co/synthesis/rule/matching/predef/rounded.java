package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.matching.IMatchingFunction;
import za.co.synthesis.rule.matching.MatchingContext;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * Created by jake on 7/21/16.
 */
public class rounded implements IMatchingFunction {
  private final String fieldname;

  public rounded(String fieldname) {
    this.fieldname = fieldname;
  }

  @Override
  public boolean execute(MatchingContext context) {
    Object valueLhs = context.getFieldLhs(fieldname);

    if (valueLhs != null) {
      BigDecimal value = Util.number(valueLhs);
      double dblValue = value.doubleValue();
      double floorValue = Math.floor(dblValue);

      if (dblValue-floorValue < 0.001)
        return true;
    }
    return false;
  }
}

