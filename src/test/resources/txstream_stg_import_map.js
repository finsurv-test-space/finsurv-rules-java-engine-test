var mapping = {
  src: {
    cd3_ack: {
      description: "TxStream Staging Monetary AMount",
      select: "SELECT m.UNIQUE_BOPCUS_N, ack.* FROM CD3_MATRIX m JOIN CD3_ACK ack ON ack.UNIQUE_ACK_N = m.UNIQUE_ACK_N",
      transaction_field: "m.UNIQUE_BOPCUS_N",
      order_by: "m.UNIQUE_BOPCUS_N",
//      select: "SELECT r.* FROM Result r",
//      transaction_field: "r.TRN_REFERENCE",
//      order_by: "r.TRN_REFERENCE",
      master_src: "cd3_tran"
    },
    cd3_tran: {
      description: "Transaction",
      select: "SELECT t.* FROM CD3_BOPCUS_RECORD t",
      transaction_field: "t.UNIQUE_BOPCUS_N",
      order_by: "t.UNIQUE_BOPCUS_N"
//      select: "SELECT t.* FROM CoreTransaction t",
//      transaction_field: "t.TRN_REFERENCE",
//      order_by: "t.TRN_REFERENCE"
    },
    cd3_money: {
      description: "Monetary Amount",
      select: "SELECT m.* FROM CD3_BOPCUS_AMOUNTS m",
      transaction_field: "m.UNIQUE_BOPCUS_N",
      money_field: "m.SEQUENCE_NUMBER",
      order_by: "m.UNIQUE_BOPCUS_N, m.SEQUENCE_NUMBER",
//      select: "SELECT m.* FROM MonetaryAmount m",
//      transaction_field: "m.TRN_REFERENCE",
//      money_field: "m.SEQUENCE_NUMBER",
//      order_by: "m.TRN_REFERENCE, m.SEQUENCE_NUMBER",
      master_src: "cd3_tran"
    },
    cd3_importexport: {
      description: "Import Export entries",
      select: "SELECT ie.* FROM CD3_BOPCUS_IMP_EXP ie",
      transaction_field: "ie.UNIQUE_BOPCUS_N",
      money_field: "ie.TRN_SEQUENCE",
      order_by: "ie.UNIQUE_BOPCUS_N, ie.TRN_SEQUENCE, ie.SUB_SEQUENCE",
//      select: "SELECT ie.* FROM ImportExport ie",
//      transaction_field: "ie.TRN_REFERENCE",
//      money_field: "ie.SEQUENCE_NUMBER",
//      order_by: "ie.TRN_REFERENCE, ie.SEQUENCE_NUMBER",
      master_src: "cd3_money"
    }
  },
  dest: [
    { scope: "result",
      src: "cd3_ack",
      fields: [
        {col: "ERROR_CODE",          field: "ErrorCode"},
        {col: "ERROR_DESCRIPTION",   field: "ErrorDescription"},
        {col: "ATTRIBUTE",           field: "ErrorAttribute"},
        {col: "TRN_REFERENCE",       field: "TrnReference"},
        {col: "UNIQUE_BOPCUS_N",     field : "UNIQUE_BOPCUS_N"}, // 129881782
        {col: "SEQUENCE_NUMBER",     field: "Level1SeqNo"},
        {col: "SEQUENCE_NUMBER",     field: "Level2SeqNo"},
        {col: "TRN_STATUS",          field: "TrnStatus"}
      ]
    },
    { scope: "transaction",
      src: "cd3_tran",
      fields: [
        {col: "TRN_REFERENCE",       field: "TrnReference"},
        {col: "SYSTEM_ID",           meta : "SYSTEM_ID"},
        {col: "IMP_EXP_IND",         meta : "IMP_EXP_IND"},
        {col: "BUSINESS_TRAN_C",     meta : "BUSINESS_TRAN_C"},
        {col: "ATTRIBUTE",           meta : "ATTRIBUTE"},
        {col: "ERROR_CODE",          meta : "ERROR_CODE"},
        {col: "ERROR_DESCRIPTION",   meta : "ERROR_DESCRIPTION"},
        {col: "UNIQUE_BOPCUS_N",     meta : "UNIQUE_BOPCUS_N"}, // 129881782
        {col: "MESSAGE_TYPE",        meta : "MESSAGE_TYPE"}, // 9
        {col: "RECORD_TYPE",         meta : "RECORD_TYPE"}, // V
        {col: "REPORTING_QUALIFIER", field: "ReportingQualifier"}, // BOPCUS
        {col: "FLOW",                field: "Flow"}, // OUT
        {col: "REPLACEMENT_TRANSACTION", field: "ReplacementTransaction"}, // N
        {col: "REPLACEMENT_ORIGINAL_REFERENCE", field: "ReplacementOriginalReference"}, //
        {col: "VALUE_DATE",          field: "ValueDate", type: "date"}, // 22/DEC/14
        {col: "BRANCH_CODE",         field: "BranchCode", default: ""}, // 02015500
        {col: "BRANCH_NAME",         field: "BranchName", default: ""}, // TPS TRADE PROCESSING
        {col: "HUB_CODE",            field: "HubCode"}, //
        {col: "HUB_NAME",            field: "HubName"}, //
        {col: "ORIGINATING_BANK",    field: "OriginatingBank"}, // SBZAZAJJ
        {col: "ORIGINATING_COUNTRY", field: "OriginatingCountry"}, // ZA
        {col: "CORRESPONDENT_BANK",    field: "CorrespondentBank"}, // SCBLUS33
        {col: "CORRESPONDENT_COUNTRY", field: "CorrespondentCountry"}, // US
        {col: "RECEIVING_BANK",      field: "ReceivingBank"}, // BOTKJPJT
        {col: "RECEIVING_COUNTRY",   field: "ReceivingCountry"}, // JP
        {col: "TOTAL_VALUE",   field: "TotalValue"}, // JP
        {src: "cd3_money", col: "FOREIGN_CURRENCY_CODE", field: "FlowCurrency", transform: "first", default: "ZAR"},
        {src: "cd3_money", col: "FOREIGN_CURRENCY_CODE", meta: "FOREIGN_CURRENCY_CODE", transform: "first", default: "ZAR"},
        {src: "cd3_money", col: "FOREIGN_VALUE",       field: "TotalForeignValue", transform: "sum"}, //
        {src: "cd3_money", col: "RAND_VALUE",          field: "TotalForeignValue", transform: "sum", condition: {FOREIGN_CURRENCY_CODE: "ZAR"}}, // 454659.56
        {col: "NON_RES_CUST_TYPE",   meta : "NON_RES_CUST_TYPE"}, // I, E, X
        {col: "NON_RES_SURNAME",     field: "NonResident.Individual.Surname", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_NAME",        field: "NonResident.Individual.Name", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_GENDER",        field: "NonResident.Individual.Gender", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_PASSPORT_NUMBER", field: "NonResident.Individual.PassportNumber", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_PASSPORT_COUNTRY", field: "NonResident.Individual.PassportCountry", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_ADDRESS_LINE1", field: "NonResident.Individual.Address.AddressLine1", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_ADDRESS_LINE2", field: "NonResident.Individual.Address.AddressLine2", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_ADDRESS_LINE3", field: "NonResident.Individual.Address.Suburb", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_CITY",          field: "NonResident.Individual.Address.City", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_STATE",         field: "NonResident.Individual.Address.State", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_ZIP_CODE",      field: "NonResident.Individual.Address.PostalCode", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_COUNTRY",       field: "NonResident.Individual.Address.Country", condition: {NON_RES_CUST_TYPE: "I"}}, //
        {col: "NON_RES_ACCOUNT_IDENTIFIER", field: "NonResident.Individual.AccountIdentifier", condition: {NON_RES_CUST_TYPE: "I"}}, // NON RESIDENT OTHER
        {col: "NON_RES_ACCOUNT_NUMBER", field: "NonResident.Individual.AccountNumber", condition: {NON_RES_CUST_TYPE: "I"}},
        {col: "NON_RES_LEGAL_ENTITY_NAME", field: "NonResident.Entity.EntityName", condition: {NON_RES_CUST_TYPE: "E"}}, //
        {col: "NON_RES_ACCOUNT_IDENTIFIER", field: "NonResident.Entity.AccountIdentifier", condition: {NON_RES_CUST_TYPE: "E"}}, //
        {col: "NON_RES_ACCOUNT_NUMBER", field: "NonResident.Entity.AccountNumber", condition: {NON_RES_CUST_TYPE: "E"}}, //
        {col: "NON_RES_ADDRESS_LINE1", field: "NonResident.Entity.Address.AddressLine1", condition: {NON_RES_CUST_TYPE: "E"}}, //
        {col: "NON_RES_ADDRESS_LINE2", field: "NonResident.Entity.Address.AddressLine2", condition: {NON_RES_CUST_TYPE: "E"}}, //
        {col: "NON_RES_ADDRESS_LINE3", field: "NonResident.Entity.Address.Suburb", condition: {NON_RES_CUST_TYPE: "E"}}, //
        {col: "NON_RES_CITY",          field: "NonResident.Entity.Address.City", condition: {NON_RES_CUST_TYPE: "E"}}, //
        {col: "NON_RES_STATE",         field: "NonResident.Entity.Address.State", condition: {NON_RES_CUST_TYPE: "E"}}, //
        {col: "NON_RES_ZIP_CODE",      field: "NonResident.Entity.Address.PostalCode", condition: {NON_RES_CUST_TYPE: "E"}}, //
        {col: "NON_RES_COUNTRY",       field: "NonResident.Entity.Address.Country", condition: {NON_RES_CUST_TYPE: "E"}}, //
        {col: "NON_RES_EXCEPTION_NAME", field: "NonResident.Exception.ExceptionName", condition: {NON_RES_CUST_TYPE: "X"}, default: ""}, //
        {col: "RES_CUST_TYPE",         meta : "RES_CUST_TYPE"}, // I, E, X
        {col: "RES_SURNAME",           field: "Resident.Individual.Surname", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_NAME",              field: "Resident.Individual.Name", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_GENDER",            field: "Resident.Individual.Gender", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_DATE_OF_BIRTH",     field: "Resident.Individual.DateOfBirth", type: "date", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_ID_NUMBER",         field: "Resident.Individual.IDNumber", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_TEMP_RES_PERMIT_NUMBER", field: "Resident.Individual.TempResPermitNumber", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_FOREIGN_ID_NUMBER", field: "Resident.Individual.ForeignIDNumber", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_FOREIGN_ID_COUNTRY", field: "Resident.Individual.ForeignIDCountry", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_PASSPORT_NUMBER",   field: "Resident.Individual.PassportNumber", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_PASSPORT_COUNTRY",  field: "Resident.Individual.PassportCountry", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_ACCOUNT_NAME",      field: "Resident.Individual.AccountName", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_ACCOUNT_IDENTIFIER", field: "Resident.Individual.AccountIdentifier", condition: {RES_CUST_TYPE: "I"}}, // RESIDENT OTHER
        {col: "RES_ACCOUNT_NUMBER", field: "Resident.Individual.AccountNumber", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_CUSTOMS_CLIENT_NUMBER", field: "Resident.Individual.CustomsClientNumber", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_TAX_NUMBER",        field: "Resident.Individual.TaxNumber", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_VAT_NUMBER",        field: "Resident.Individual.VATNumber", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_TAX_CLEARANCE_CERT_IND", field: "Resident.Individual.TaxClearanceCertificateIndicator", condition: {RES_CUST_TYPE: "I"}}, // N
        {col: "RES_TAX_CLEARANCE_CERT_NUMBER", field: "Resident.Individual.TaxClearanceCertificateReference", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_STREET_ADDRESS_LINE1", field: "Resident.Individual.StreetAddress.AddressLine1", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_STREET_ADDRESS_LINE2", field: "Resident.Individual.StreetAddress.AddressLine2", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_STREET_SUBURB",     field: "Resident.Individual.StreetAddress.Suburb", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_STREET_CITY",       field: "Resident.Individual.StreetAddress.City", condition: {RES_CUST_TYPE: "I"}}, // GAUTENG
        {col: "RES_STREET_PROVINCE",   field: "Resident.Individual.StreetAddress.Province", transform: "toUpper", condition: {RES_CUST_TYPE: "I"}}, // GAUTENG
        {col: "RES_STREET_POSTAL_CODE", field: "Resident.Individual.StreetAddress.PostalCode", condition: {RES_CUST_TYPE: "I"}}, // 1501
        {col: "RES_POSTAL_ADDRESS_LINE1", field: "Resident.Individual.PostalAddress.AddressLine1", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_POSTAL_ADDRESS_LINE2", field: "Resident.Individual.PostalAddress.AddressLine2", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_POSTAL_SUBURB",     field: "Resident.Individual.PostalAddress.Suburb", condition: {RES_CUST_TYPE: "I"}}, // BENONI SOUTH
        {col: "RES_POSTAL_CITY",       field: "Resident.Individual.PostalAddress.City", condition: {RES_CUST_TYPE: "I"}}, // GAUTENG
        {col: "RES_POSTAL_PROVINCE",   field: "Resident.Individual.PostalAddress.Province", transform: "toUpper", condition: {RES_CUST_TYPE: "I"}}, // GAUTENG
        {col: "RES_POSTAL_CODE",       field: "Resident.Individual.PostalAddress.PostalCode", condition: {RES_CUST_TYPE: "I"}}, // 1502
        {col: "RES_CONTACT_SURNAME",   field: "Resident.Individual.ContactDetails.ContactSurname", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_CONTACT_NAME",      field: "Resident.Individual.ContactDetails.ContactName", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_EMAIL",             field: "Resident.Individual.ContactDetails.Email", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_FAX",               field: "Resident.Individual.ContactDetails.Fax", condition: {RES_CUST_TYPE: "I"}}, //
        {col: "RES_TELEPHONE",         field: "Resident.Individual.ContactDetails.Telephone", condition: {RES_CUST_TYPE: "I"}},
        {col: "RES_LEGAL_ENTITY_NAME", field: "Resident.Entity.EntityName", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_TRADING_NAME",      field: "Resident.Entity.TradingName", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_REGISTRATION_NUMBER", field: "Resident.Entity.RegistrationNumber", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_INSTITUTIONAL_SECTOR", field: "Resident.Entity.InstitutionalSector", condition: {RES_CUST_TYPE: "E"}}, // 01
        {col: "RES_INDUSTRIAL_CLASSIFICATION", field: "Resident.Entity.IndustrialClassification", condition: {RES_CUST_TYPE: "E"}}, // 08
        {col: "RES_ACCOUNT_NAME",      field: "Resident.Entity.AccountName", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_ACCOUNT_IDENTIFIER", field: "Resident.Entity.AccountIdentifier", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_ACCOUNT_NUMBER", field: "Resident.Entity.AccountNumber", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_CUSTOMS_CLIENT_NUMBER", field: "Resident.Entity.CustomsClientNumber", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_TAX_NUMBER",        field: "Resident.Entity.TaxNumber", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_VAT_NUMBER",        field: "Resident.Entity.VATNumber", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_TAX_CLEARANCE_CERT_IND", field: "Resident.Entity.TaxClearanceCertificateIndicator", condition: {RES_CUST_TYPE: "E"}}, // N
        {col: "RES_TAX_CLEARANCE_CERT_NUMBER", field: "Resident.Entity.TaxClearanceCertificateReference", condition: {RES_CUST_TYPE: "E"}}, //
        {col: "RES_STREET_ADDRESS_LINE1", field: "Resident.Entity.StreetAddress.AddressLine1", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_STREET_ADDRESS_LINE2", field: "Resident.Entity.StreetAddress.AddressLine2", condition: {RES_CUST_TYPE: "E"}}, //
        {col: "RES_STREET_SUBURB",     field: "Resident.Entity.StreetAddress.Suburb", condition: {RES_CUST_TYPE: "E"}}, // BENONI SOUTH
        {col: "RES_STREET_CITY",       field: "Resident.Entity.StreetAddress.City", condition: {RES_CUST_TYPE: "E"}}, // GAUTENG
        {col: "RES_STREET_PROVINCE",   field: "Resident.Entity.StreetAddress.Province", transform: "toUpper", condition: {RES_CUST_TYPE: "E"}}, // GAUTENG
        {col: "RES_STREET_POSTAL_CODE", field: "Resident.Entity.StreetAddress.PostalCode", condition: {RES_CUST_TYPE: "E"}}, // 1501
        {col: "RES_POSTAL_ADDRESS_LINE1", field: "Resident.Entity.PostalAddress.AddressLine1", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_POSTAL_ADDRESS_LINE2", field: "Resident.Entity.PostalAddress.AddressLine2", condition: {RES_CUST_TYPE: "E"}}, //
        {col: "RES_POSTAL_SUBURB",     field: "Resident.Entity.PostalAddress.Suburb", condition: {RES_CUST_TYPE: "E"}}, // BENONI SOUTH
        {col: "RES_POSTAL_CITY",       field: "Resident.Entity.PostalAddress.City", condition: {RES_CUST_TYPE: "E"}}, // GAUTENG
        {col: "RES_POSTAL_PROVINCE",   field: "Resident.Entity.PostalAddress.Province", transform: "toUpper", condition: {RES_CUST_TYPE: "E"}}, // GAUTENG
        {col: "RES_POSTAL_CODE",       field: "Resident.Entity.PostalAddress.PostalCode", condition: {RES_CUST_TYPE: "E"}}, // 1502
        {col: "RES_CONTACT_SURNAME",   field: "Resident.Entity.ContactDetails.ContactSurname", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_CONTACT_NAME",      field: "Resident.Entity.ContactDetails.ContactName", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_EMAIL",             field: "Resident.Entity.ContactDetails.Email", condition: {RES_CUST_TYPE: "E"}}, //
        {col: "RES_FAX",               field: "Resident.Entity.ContactDetails.Fax", condition: {RES_CUST_TYPE: "E"}}, //
        {col: "RES_TELEPHONE",         field: "Resident.Entity.ContactDetails.Telephone", condition: {RES_CUST_TYPE: "E"}},
        {col: "RES_EXCEPTION_NAME",    field: "Resident.Exception.ExceptionName", condition: {RES_CUST_TYPE: "X"}, default: ""}, //
        {col: "RES_COUNTRY",           field: "Resident.Exception.Country", condition: {RES_CUST_TYPE: "X"}}, //

        {col: "UNIQUE_TRAN_N",         meta : "UNIQUE_TRAN_N"},
        {col: "UNIQUE_ENTRY_N",        meta : "UNIQUE_ENTRY_N"},
        {col: "UNIQUE_XML_OUT_N",      meta : "UNIQUE_XML_OUT_N"},
        {col: "UNIQUE_ACK_N",          meta : "UNIQUE_ACK_N"},
        {col: "COMPLETE_IND",          meta : "COMPLETE_IND"},
        {col: "COMPLETE_DATE",         meta : "COMPLETE_DATE", type: "date"},
        {col: "UNIQUE_XML_IN_N",       meta : "UNIQUE_XML_IN_N"}
      ]
    },
    { scope: "money",
      src: "cd3_money",
      fields: [
        {col: "CREATED_Z",             meta : "CREATED_Z"}, // 22/DEC/14
        {col: "SEQUENCE_NUMBER",       field: "SequenceNumber"}, // 1
        {col: "MONEY_TRANSFER_AGENT_INDICATOR", field: "MoneyTransferAgentIndicator"}, // AD
        {col: "RAND_VALUE",            field: "RandValue"}, // 454659.56
        {col: "FOREIGN_VALUE",         field: "ForeignValue"}, // 38700.03
        {col: "BOP_CATEGORY",          field: "CategoryCode"}, // 104
        {col: "SUB_BOP_CATEGORY",      field: "CategorySubCode"}, // 01
        {col: "SWIFT_DETAIL",          field: "SWIFTDetails"},
        {col: "STRATE_REF_NUMBER",     field: "StrateRefNumber"},
        {col: "LOAN_REF_NUMBER",       field: "LoanRefNumber"},
        {col: "LOAN_TENOR",            field: "LoanTenor"},
        {col: "LOAN_INTEREST_RATE",    field: "LoanInterestRate"},
        {col: "RULINGS_SECTION",       field: "SARBAuth.RulingsSection"}, // B.1(H)
        {col: "AD_INTERNAL_AUTH_NUMBER", field: "SARBAuth.ADInternalAuthNumber"}, //
        {col: "AD_INTERNAL_AUTH_NUMBER_DATE", field: "SARBAuth.ADInternalAuthNumberDate", type: "date"}, //
        {col: "SARB_AUTH_APPLIC_NUMBER", field: "SARBAuth.SARBAuthAppNumber"}, //
        {col: "SARB_AUTH_REFERENCE_NUMBER", field: "SARBAuth.SARBAuthRefNumber"}, //
        {col: "CANNOT_CATEGORISE",     field: "CannotCategorize"}, //
        {col: "SUBJECT",               field: "AdHocRequirement.Subject"}, //
        {col: "DESCRIPTION",           field: "AdHocRequirement.Description"}, //
        {col: "LOCATION_COUNTRY",      field: "LocationCountry"}, // JO
        {col: "REVERSAL_TRN_REF_NUMBER", field: "ReversalTrnRefNumber"}, //
        {col: "REVERSAL_SEQUENCE",     field: "ReversalTrnSeqNumber"}, //
        {col: "IND_THRD_PRTY_SURNAME", field: "ThirdParty.Individual.Surname"}, //
        {col: "IND_THRD_PRTY_NAME",    field: "ThirdParty.Individual.Name"}, //
        {col: "IND_THRD_PRTY_GENDER",  field: "ThirdParty.Individual.Gender"}, //
        {col: "IND_THRD_PRTY_ID_NUMBER", field: "ThirdParty.Individual.IDNumber"}, //
        {col: "IND_THRD_PRTY_DATE_OF_BIRTH", field: "ThirdParty.Individual.DateOfBirth", type: "date"}, //
        {col: "IND_THRD_PRTY_TMP_RES_PERMIT_N", field: "ThirdParty.Individual.TempResPermitNumber"}, //
        {col: "IND_THRD_PRTY_PASSPORT_NUMBER", field: "ThirdParty.Individual.PassportNumber"}, //
        {col: "IND_THRD_PRTY_PASSPORT_COUNTRY", field: "ThirdParty.Individual.PassportCountry"}, //
        {col: "LEGAL_ENTITY_THRD_PRTY_NAME", field: "ThirdParty.Entity.Name"}, //
        {col: "LEGAL_ENTITY_THRD_PRTY_REG_N", field: "ThirdParty.Entity.RegistrationNumber"}, //
        {col: "THRD_PRTY_CUSTOMS_CLIENT_CODE", field: "ThirdParty.CustomsClientNumber"}, //
        {col: "THRD_PRTY_TAX_NUMBER",  field: "ThirdParty.TaxNumber"}, //
        {col: "THRD_PRTY_VAT_NUMBER",  field: "ThirdParty.VATNumber"}, //
        {col: "THRD_PRTY_STREET_ADDRESS_LINE1", field: "ThirdParty.StreetAddress.AddressLine1"}, //
        {col: "THRD_PRTY_STREET_ADDRESS_LINE2", field: "ThirdParty.StreetAddress.AddressLine2"}, //
        {col: "THRD_PRTY_STREET_SUBURB", field: "ThirdParty.StreetAddress.Suburb"}, //
        {col: "THRD_PRTY_STREET_CITY", field: "ThirdParty.StreetAddress.City"}, //
        {col: "THRD_PRTY_STREET_PROVINCE", field: "ThirdParty.StreetAddress.Province", transform: "toUpper"}, //
        {col: "THRD_PRTY_STREET_POSTAL_CODE", field: "ThirdParty.StreetAddress.PostalCode"}, //
        {col: "THRD_PRTY_POSTAL_ADDRESS_LINE1", field: "ThirdParty.PostalAddress.AddressLine1"}, //
        {col: "THRD_PRTY_POSTAL_ADDRESS_LINE2", field: "ThirdParty.PostalAddress.AddressLine2"}, //
        {col: "THRD_PRTY_POSTAL_SUBURB", field: "ThirdParty.PostalAddress.Suburb"}, //
        {col: "THRD_PRTY_POSTAL_CITY", field: "ThirdParty.PostalAddress.City"}, //
        {col: "THRD_PRTY_POSTAL_PROVINCE", field: "ThirdParty.PostalAddress.Province", transform: "toUpper"}, //
        {col: "THRD_PRTY_POSTAL_CODE", field: "ThirdParty.PostalAddress.PostalCode"}, //
        {col: "THRD_PRTY_CONTACT_SURNAME", field: "ThirdParty.ContactDetails.ContactSurname"}, //
        {col: "THRD_PRTY_CONTACT_NAME", field: "ThirdParty.ContactDetails.ContactName"}, //
        {col: "THRD_PRTY_EMAIL",       field: "ThirdParty.ContactDetails.Email"}, //
        {col: "THRD_PRTY_FAX",         field: "ThirdParty.ContactDetails.Fax"}, //
        {col: "THRD_PRTY_TELEPHONE",   field: "ThirdParty.ContactDetails.Telephone"} //
      ]
    },
    { scope: "importexport",
      src: "cd3_importexport",
      fields: [
        {col: "IMPORT_CONTROL_NUMBER", field: "ImportControlNumber"}, //
        {col: "TRANSPORT_DOCUMENT_N",  field: "TransportDocumentNumber"}, //
        {col: "NOMRNONIVS",            field: "MRNNotOnIVS"}, //
        {col: "UCR",                   field: "UCR"}, //
        {col: "PAYMENT_VALUE",         field: "PaymentValue"}, //
        {col: "PAYMENT_CURR_CODE",     field: "PaymentCurrencyCode"}, //
        {col: "SUB_SEQUENCE",          field: "SubSequence"} //
      ]
    }
  ]
}
