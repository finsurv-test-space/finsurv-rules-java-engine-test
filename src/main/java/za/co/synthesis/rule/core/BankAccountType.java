package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 3/2/15
 * Time: 3:48 PM
 * A list of bank account types
 */

/**
 *  Banks account type's are used to identify the source and destination of funds for transaction evaluation
 *  purposes. In this sense the cash entries are not account types, but really define source/destination for
 *  a leg of the transaction. For all the other entries these describe different types of bank accounts.
 *
 */
public enum BankAccountType {
  /**
   * Only used when configured account type cannot be determined
   */
  Unknown,
  /**
   * South African Rand account
   */
  LOCAL_ACC,
  /**
   * Customer Foreign Currency account that is used for offshore business like import/export, services etc.
   */
  CFC,
  /**
   * A foreign currency investment account held in the books of South African banks. Money transferred to this
   * type of account is done so using either the discretionary or foreign investment allowance.
   */
  FCA,
  /**
   * A local bank's foreign currency account in the books of an offshore bank. However in the case of non-residents
   * transferring funds from their own offshore accounts we classify these as NOSTRO accounts
   * (since the transfer goes via the local bank's NOSTRO account)
   */
  NOSTRO,
  /**
   * A non-resident bank's Rand account in the books of a local bank
   */
  VOSTRO,
  /**
   * This used where the funds for a transaction are provided in cash as opposed to an account.
   * In this case the cash provided is in Rand
   */
  CASH_LOCAL,
  /**
   * This used where the funds for a transaction are provided in cash as opposed to an account.
   * In this case the cash provided is in a foreign currency
   */
  CASH_CURR
}
