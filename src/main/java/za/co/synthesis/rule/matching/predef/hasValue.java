package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.matching.IMatchingFunction;
import za.co.synthesis.rule.matching.MatchingContext;
import za.co.synthesis.rule.support.StringList;

/**
 * Created by jake on 6/20/16.
 */
public class hasValue implements IMatchingFunction {
  private final String fieldname;
  private StringList constants = null;

  public hasValue(String fieldname, String value) {
    this.fieldname = fieldname;
    this.constants = new StringList();
    this.constants.add(value);
  }

  public hasValue(String fieldname, StringList value) {
    this.fieldname = fieldname;
    this.constants = value;
  }

  public boolean execute(MatchingContext context) {
    Object valueLhs = context.getFieldLhs(fieldname);

    if (valueLhs != null) {
      return constants.contains(valueLhs.toString());
    }
    return false;
  }
}