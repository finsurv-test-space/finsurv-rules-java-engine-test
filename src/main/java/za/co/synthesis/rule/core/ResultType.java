package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 3:30 PM
 * To change this template use File | Settings | File Templates.
 */
public enum ResultType {
  Error,
  Warning,
  Deprecated,
  Mandatory,
  Document,
  Validate,
  NoValidate
}
