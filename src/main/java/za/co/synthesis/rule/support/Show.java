package za.co.synthesis.rule.support;

/**
 * Created by jake on 4/10/16.
 */
public class Show extends DisplayRule {
  public Show(IAssertionFunction assertion) {
    super(DisplayRuleType.Show, null, null, null, assertion);
  }
}
