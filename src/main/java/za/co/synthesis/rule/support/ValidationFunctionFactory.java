package za.co.synthesis.rule.support;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.predef.literal;
import za.co.synthesis.rule.predef.map;

import java.lang.reflect.Constructor;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 10:59 PM
 * Creates a new function (or logical combination of functions based on a given JavaScript definition)
 */
public class ValidationFunctionFactory {
  private static final String functionPackage = "za.co.synthesis.rule.predef.";

  private static IStringResolver wrap(String value) {
    if (DSLSupport.hasMapping(value)) {
      return new map(value);
    }
    else {
      return new literal(value);
    }
  }

  public static IAssertionFunction createFunction(final String name) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    return (IAssertionFunction)cls.newInstance();
  }

  public static IAssertionFunction createFunction(final String name, final String param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    try {
      Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class);
      return (IAssertionFunction) clsConstructor.newInstance(param1);
    }
    catch (NoSuchMethodException e) {
      try {
        Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class);
        return (IAssertionFunction) clsConstructor.newInstance(wrap(param1));
      }
      catch (NoSuchMethodException eInner) {
        throw e;
      }
    }
  }

  public static IAssertionFunction createFunction(final String name, final IStringResolver param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class);
    return (IAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IAssertionFunction createFunction(final String name, final JSRegExLiteral param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(JSRegExLiteral.class);
    return (IAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IAssertionFunction createFunction(final String name, final StringList param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    try {
      Constructor<?> clsConstructor = cls.getDeclaredConstructor(StringList.class);
      return (IAssertionFunction) clsConstructor.newInstance(param1);
    }
    catch (NoSuchMethodException e) {
      try {
        Constructor<?> clsConstructor = cls.getDeclaredConstructor(StringResolverList.class);
        StringResolverList list = new StringResolverList();
        for (String c : param1) {
          list.add(wrap(c));
        }
        return (IAssertionFunction) clsConstructor.newInstance(list);
      }
      catch (NoSuchMethodException eInner) {
        throw e;
      }
    }
  }

  public static IAssertionFunction createFunction(final String name, final StringResolverList param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(StringResolverList.class);
    return (IAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IAssertionFunction createFunction(final String name, final String param1, final String param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    try {
      Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class);
      return (IAssertionFunction) clsConstructor.newInstance(param1, param2);
    }
    catch (NoSuchMethodException e) {
      try {
        Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IStringResolver.class);
        return (IAssertionFunction) clsConstructor.newInstance(wrap(param1), wrap(param2));
      }
      catch (NoSuchMethodException eInner) {
        throw e;
      }
    }
  }

  public static IAssertionFunction createFunction(final String name, final IStringResolver param1, final IStringResolver param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IStringResolver.class);
    return (IAssertionFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IAssertionFunction createFunction(final String name, final String param1, final String param2, final String param3) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    try {
      Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class, String.class);
      return (IAssertionFunction) clsConstructor.newInstance(param1, param2, param3);
    }
    catch (NoSuchMethodException e) {
      try {
        Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IStringResolver.class, IStringResolver.class);
        return (IAssertionFunction) clsConstructor.newInstance(wrap(param1), wrap(param2), wrap(param3));
      }
      catch (NoSuchMethodException eInner) {
        throw e;
      }
    }
  }

  public static IAssertionFunction createFunction(final String name, final IStringResolver param1, final IStringResolver param2, final IStringResolver param3) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IStringResolver.class, IStringResolver.class);
    return (IAssertionFunction)clsConstructor.newInstance(param1, param2, param3);
  }

  public static IAssertionFunction createFunction(final String name, final String param1, final String param2, final Boolean param3) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    try {
      Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class, Boolean.class);
      return (IAssertionFunction) clsConstructor.newInstance(param1, param2, param3);
    }
    catch (NoSuchMethodException e) {
      try {
        Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IStringResolver.class, Boolean.class);
        return (IAssertionFunction) clsConstructor.newInstance(wrap(param1), wrap(param2), param3);
      }
      catch (NoSuchMethodException eInner) {
        throw e;
      }
    }
  }

  public static IAssertionFunction createFunction(final String name, final IStringResolver param1, final IStringResolver param2, final Boolean param3) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IStringResolver.class, Boolean.class);
    return (IAssertionFunction)clsConstructor.newInstance(param1, param2, param3);
  }

  public static IAssertionFunction createFunction(final String name, final String param1, final String param2, final String param3, final String param4) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    try {
      Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class, String.class, String.class);
      return (IAssertionFunction) clsConstructor.newInstance(param1, param2, param3, param4);
    }
    catch (NoSuchMethodException e) {
      try {
        Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IStringResolver.class, IStringResolver.class, IStringResolver.class);
        return (IAssertionFunction) clsConstructor.newInstance(wrap(param1), wrap(param2), wrap(param3), wrap(param4));
      }
      catch (NoSuchMethodException eInner) {
        throw e;
      }
    }
  }

  public static IAssertionFunction createFunction(final String name,
                                                  final IStringResolver param1, final IStringResolver param2,
                                                  final IStringResolver param3, final IStringResolver param4) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IStringResolver.class,
            IStringResolver.class, IStringResolver.class);
    return (IAssertionFunction)clsConstructor.newInstance(param1, param2, param3, param4);
  }


  public static IAssertionFunction createFunction(final String name, final String param1, final String param2, final String param3, final Boolean param4) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    try {
      Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class, String.class, Boolean.class);
      return (IAssertionFunction) clsConstructor.newInstance(param1, param2, param3, param4);
    }
    catch (NoSuchMethodException e) {
      try {
        Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IStringResolver.class, IStringResolver.class, Boolean.class);
        return (IAssertionFunction) clsConstructor.newInstance(wrap(param1), wrap(param2), wrap(param3), param4);
      }
      catch (NoSuchMethodException eInner) {
        throw e;
      }
    }
  }

  public static IAssertionFunction createFunction(final String name,
                                                  final IStringResolver param1, final IStringResolver param2,
                                                  final IStringResolver param3, final Boolean param4) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IStringResolver.class,
            IStringResolver.class, Boolean.class);
    return (IAssertionFunction)clsConstructor.newInstance(param1, param2, param3, param4);
  }


  public static IAssertionFunction createFunction(final String name, final String param1, final StringList param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    try {
      Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, StringList.class);
      return (IAssertionFunction) clsConstructor.newInstance(param1, param2);
    }
    catch (NoSuchMethodException e) {
      try {
        Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, StringResolverList.class);
        StringResolverList list = new StringResolverList();
        for (String c : param2) {
          list.add(wrap(c));
        }
        return (IAssertionFunction) clsConstructor.newInstance(wrap(param1), list);
      }
      catch (NoSuchMethodException eInner) {
        throw e;
      }
    }
  }

  public static IAssertionFunction createFunction(final String name, final IStringResolver param1, final StringResolverList param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, StringResolverList.class);
    return (IAssertionFunction) clsConstructor.newInstance(param1, param2);
  }

  public static IAssertionFunction createFunction(final String name, final IAssertionFunction param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IAssertionFunction.class);
    return (IAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IAssertionFunction createFunction(final String name, final String param1, final IAssertionFunction param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    try {
      Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, IAssertionFunction.class);
      return (IAssertionFunction) clsConstructor.newInstance(param1, param2);
    }
    catch (NoSuchMethodException e) {
      try {
        Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IAssertionFunction.class);
        return (IAssertionFunction) clsConstructor.newInstance(wrap(param1), param2);
      }
      catch (NoSuchMethodException eInner) {
        throw e;
      }
    }
  }

  public static IAssertionFunction createFunction(final String name, final IStringResolver param1, final IAssertionFunction param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IStringResolver.class, IAssertionFunction.class);
    return (IAssertionFunction) clsConstructor.newInstance(param1, param2);
  }
}
