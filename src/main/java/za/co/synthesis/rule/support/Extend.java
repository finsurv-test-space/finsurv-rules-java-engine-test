package za.co.synthesis.rule.support;

import za.co.synthesis.rule.predef.alwaysFalse;


/**
 * Created by jake on 4/10/16.
 */
public class Extend extends DisplayRule {
  public Extend() {
    super(DisplayRuleType.Extend, null, null, null, new alwaysFalse());
  }
}
