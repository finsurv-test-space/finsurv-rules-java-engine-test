package za.co.synthesis.rule.core;

public enum CachePeriod {
    // Keep the cached entry forever
    Forever,
    // Keep the cached entry for a long period of time (default is 1 week)
    Long,
    // Keep the cached entry for a medium period of time (default is 1 hour)
    Medium,
    // Keep the cached entry for a short period of time (default is 1 minute)
    Short,
    // Don't cache the entry at all
    Never
}
