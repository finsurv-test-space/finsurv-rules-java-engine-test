package za.co.synthesis.rule.doc;

import za.co.synthesis.rule.core.FlowType;
import za.co.synthesis.rule.support.RuleType;

import java.util.List;

public class RuleInfo {
  private RuleType type;
  private String ruleName;
  private SourceStatus ruleStatus;
  private SourceStatus messageStatus;
  private String ruleChannel;
  private String parentRuleChannel;
  private String messageChannel;
  private FieldInfo fieldInfo;
  private String errorCode;
  private String errorMessage;
  private String validation;
  private boolean filterOnCategory;
  private boolean filterOnFlow;
  private boolean filterOnSection;
  private List<String> onCategory;
  private List<String> notOnCategory;
  private String onSection;
  private FlowType onFlow;
  private String assertionDSL;
  private TestStatus testStatus;
  private int positiveTestCount;
  private int negativeTestCount;
  private String ruleDSL;

  public RuleType getType() {
    return type;
  }

  public void setType(RuleType type) {
    this.type = type;
  }

  public String getRuleName() {
    return ruleName;
  }

  public void setRuleName(String ruleName) {
    this.ruleName = ruleName;
  }

  public SourceStatus getRuleStatus() {
    return ruleStatus;
  }

  public void setRuleStatus(SourceStatus ruleStatus) {
    this.ruleStatus = ruleStatus;
  }


  public String getRuleChannel() {
    return ruleChannel;
  }

  public void setRuleChannel(String ruleChannel) {
    this.ruleChannel = ruleChannel;
  }

  public String getParentRuleChannel() {
    return parentRuleChannel;
  }

  public void setParentRuleChannel(String parentRuleChannel) {
    this.parentRuleChannel = parentRuleChannel;
  }

  public SourceStatus getMessageStatus() {
    return messageStatus;
  }

  public void setMessageStatus(SourceStatus messageStatus) {
    this.messageStatus = messageStatus;
  }

  public String getMessageChannel() {
    return messageChannel;
  }

  public void setMessageChannel(String messageChannel) {
    this.messageChannel = messageChannel;
  }

  public FieldInfo getFieldInfo() {
    return fieldInfo;
  }

  public void setFieldInfo(FieldInfo fieldInfo) {
    this.fieldInfo = fieldInfo;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getValidation() {
    return validation;
  }

  public void setValidation(String validation) {
    this.validation = validation;
  }

  public boolean isFilterOnCategory() {
    return filterOnCategory;
  }

  public void setFilterOnCategory(boolean filterOnCategory) {
    this.filterOnCategory = filterOnCategory;
  }

  public boolean isFilterOnFlow() {
    return filterOnFlow;
  }

  public void setFilterOnFlow(boolean filterOnFlow) {
    this.filterOnFlow = filterOnFlow;
  }

  public boolean isFilterOnSection() {
    return filterOnSection;
  }

  public void setFilterOnSection(boolean filterOnSection) {
    this.filterOnSection = filterOnSection;
  }

  public List<String> getOnCategory() {
    return onCategory;
  }

  public void setOnCategory(List<String> onCategory) {
    this.onCategory = onCategory;
  }

  public List<String> getNotOnCategory() {
    return notOnCategory;
  }

  public void setNotOnCategory(List<String> notOnCategory) {
    this.notOnCategory = notOnCategory;
  }

  public String getOnSection() {
    return onSection;
  }

  public void setOnSection(String onSection) {
    this.onSection = onSection;
  }

  public FlowType getOnFlow() {
    return onFlow;
  }

  public void setOnFlow(FlowType onFlow) {
    this.onFlow = onFlow;
  }

  public String getAssertionDSL() {
    return assertionDSL;
  }

  public void setAssertionDSL(String assertionDSL) {
    this.assertionDSL = assertionDSL;
  }

  public TestStatus getTestStatus() {
    return testStatus;
  }

  public void setTestStatus(TestStatus testStatus) {
    this.testStatus = testStatus;
  }

  public int getPositiveTestCount() {
    return positiveTestCount;
  }

  public void setPositiveTestCount(int positiveTestCount) {
    this.positiveTestCount = positiveTestCount;
  }

  public int getNegativeTestCount() {
    return negativeTestCount;
  }

  public void setNegativeTestCount(int negativeTestCount) {
    this.negativeTestCount = negativeTestCount;
  }

  public String getRuleDSL() {
    return ruleDSL;
  }

  public void setRuleDSL(String ruleDSL) {
    this.ruleDSL = ruleDSL;
  }
}
