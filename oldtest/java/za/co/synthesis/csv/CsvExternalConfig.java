package za.co.synthesis.csv;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 4:48 PM
 * The CSV-specific "external" field configuration
 */
public class CsvExternalConfig {
  private int colIndex;
  private boolean isDateFormat;

  public CsvExternalConfig(int colIndex, boolean dateFormat) {
    this.colIndex = colIndex;
    this.isDateFormat = dateFormat;
  }

  public int getColIndex() {
    return colIndex;
  }

  public boolean isDateFormat() {
    return isDateFormat;
  }
}
