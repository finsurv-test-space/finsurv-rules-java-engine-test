package za.co.synthesis.rule.support;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 9:35 PM
 * A failure rule
 */
public class Failure extends Rule {
  public Failure(String name, String code, String message, IAssertionFunction assertion, String assertionDSL, String ruleDSL) {
    super(RuleType.Error, name, code, message, null, assertion, assertionDSL, ruleDSL);
  }
}
