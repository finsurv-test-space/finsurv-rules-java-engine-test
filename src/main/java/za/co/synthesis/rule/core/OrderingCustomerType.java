package za.co.synthesis.rule.core;

public enum OrderingCustomerType {
  NonBank,
  Bank
}
