package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.Util;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 3:34 PM
 * Determines if the current value is not a valid email
 */
public class notValidEmail implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !Util.isValidEmail(value.toString());
  }
}

/*
function notValidEmail(context, value) {
  if (!value)
    return true;
  return !isValidEmail(context, value);
}
*/