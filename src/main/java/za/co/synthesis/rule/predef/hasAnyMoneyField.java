package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * Created by jake on 11/2/16.
 */
public class hasAnyMoneyField implements IAssertionFunction {
  private IStringResolver field;

  public hasAnyMoneyField(IStringResolver field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    String fieldName = field.resolve(context, value);
    for (int moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
      Object fieldValue = context.getMoneyField(moneyInstance, fieldName);
      if (fieldValue != null && (!(fieldValue instanceof String) || ((String)fieldValue).trim().length() > 0))
        return true;
    }
    return false;
  }
}
/*
  _export.hasAnyMoneyField = function hasAnyMoneyField(field) {
    return function (context, value) {
      var moneyInstance;
      for (moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
        var fieldValue = context.getMoneyField(moneyInstance, field);
        if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
          return true;
      }
      return false;
    }
  }
*/

