package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.List;

public class isArrayValue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return value == null || value instanceof List;
  }
}