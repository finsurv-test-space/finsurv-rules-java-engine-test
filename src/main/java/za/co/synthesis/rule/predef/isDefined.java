package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 1/20/16
 * Time: 2:33 PM
 * Returns true if the value has been defined
 */
public class isDefined implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return (value != null);
  }
}
