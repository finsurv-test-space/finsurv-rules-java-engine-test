package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.FlowType;

import java.util.List;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 8:58 PM
 * Rule object associated with a distinct rule in the system
 */
public class Rule {
  private RuleType type;
  private String name;
  private String code;
  private String message;
  private String validation;
  private IAssertionFunction assertion;
  private String assertionDSL;
  private FlowType flow;
  private String section;
  private List<String> categories;
  private List<String> notCategories;
  private String ruleDSL;

  public Rule(final RuleType type, final String name, final String code, final String message,
              final String validation, final IAssertionFunction assertion, final String assertionDSL, final String ruleDSL) {
    this.type = type;
    this.name = name;
    this.code = code;
    this.message = message;
    this.validation = validation;
    this.assertion = assertion;
    this.assertionDSL = assertionDSL;
    this.ruleDSL = ruleDSL;
  }

  public Rule onInflow() {
    flow = FlowType.Inflow;
    return this;
  }

  public Rule onOutflow() {
    flow = FlowType.Outflow;
    return this;
  }

  public Rule onSection(final String sections) {
    section = sections;
    return this;
  }

  public Rule onCategory(final List<String> categories) {
    this.categories = categories;
    return this;
  }

  public Rule notOnCategory(final List<String> categories) {
    this.notCategories = categories;
    return this;
  }

  public RuleType getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public String getValidation() {
    return validation;
  }

  public IAssertionFunction getAssertion() {
    return assertion;
  }

  public String getAssertionDSL() {
    return assertionDSL;
  }

  public FlowType getFlow() {
    return flow;
  }

  public String getSection() {
    return section;
  }

  public List<String> getCategories() {
    return categories;
  }

  public List<String> getNotCategories() {
    return notCategories;
  }

  public String getRuleDSL() {
    return ruleDSL;
  }

  public void setRuleDSL(String ruleDSL) {
    this.ruleDSL = ruleDSL;
  }
}
