package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;


/**
 * User: dylan
 * Date: 30/06/2020
 * Time: 02:35 PM
 * Determines if the Resident IDNumber is under the SDA Limit
 */
public class isValidSDAAmount implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    localAmountForIDNumber localAmountForID = new localAmountForIDNumber();
    double SDALimit = 1000000.00f;
    double localIDAmount = new Double(localAmountForID.execute(context, value).toString());
    return localIDAmount <= SDALimit;
  }
}