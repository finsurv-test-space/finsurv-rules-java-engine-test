package za.co.synthesis.javascript;

import java.util.ArrayList;
import java.util.List;

public class JSSnippetSupport {
  public static int BASE = -1;
  private JSSnippetSupport preceding = null;
  private int precedingIndex = -1;
  private final List<JSSnippet> snippets = new ArrayList<JSSnippet>();

  public JSSnippetSupport getPreceding() {
    return preceding;
  }

  public void setPreceding(JSSnippetSupport parent, int index) {
    this.preceding = parent;
    this.precedingIndex = index;
  }

  public void clearSnippets() {
    snippets.clear();
  }

  public void copy(JSSnippetSupport other) {
    snippets.clear();
    snippets.addAll(other.snippets);
  }

  public int currentSnippetIndex() {
    return snippets.size()-1;
  }

  public void addSnippet(JSSnippet snippet) {
    snippets.add(snippet);
  }

  public JSSnippet getFirstSnippetByType(JSSnippet.Type type) {
    for (JSSnippet snippet : snippets) {
      if (snippet.getType().equals(type)) {
        return snippet;
      }
    }
    return null;
  }

  public JSSnippet getLastSnippetByType(JSSnippet.Type type) {
    for (int i = snippets.size()-1; i>=0; i--) {
      JSSnippet snippet = snippets.get(i);
      if (snippet.getType().equals(type)) {
        return snippet;
      }
    }
    return null;
  }

  public List<JSSnippet> getSnippetsByTypes(JSSnippet.Type ... types) {
    List<JSSnippet> result = new ArrayList<JSSnippet>();
    for (JSSnippet jss : snippets) {
      for (JSSnippet.Type type : types) {
        if (jss.getType().equals(type)){
          result.add(jss);
          break;
        }
      }
    }
    return result;
  }

  public JSSnippet getSeparatorByValue(Object value) {
    for (JSSnippet snippet : snippets) {
      if (snippet.getType().equals(JSSnippet.Type.Separator)) {
        JSSnippet separator = snippet;
        if (separator.getRelatedObject() != null && separator.getRelatedObject().equals(value)) {
          return separator;
        }
      }
    }
    return null;
  }

  public JSSnippet getSeparatorByName(String name) {
    for (JSSnippet snippet : snippets) {
      if (snippet.getType().equals(JSSnippet.Type.Separator)) {
        JSSnippet separator = snippet;
        if (separator.getName() != null && separator.getName().equals(name)) {
          return separator;
        }
      }
    }
    return null;
  }

  public JSSnippet getVariableByName(String name) {
    for (JSSnippet snippet : snippets) {
      if (snippet.getType().equals(JSSnippet.Type.Variable)) {
        JSSnippet variable = snippet;
        if (variable.getName() != null && variable.getName().equals(name)) {
          return variable;
        }
      }
    }
    return null;
  }

  public JSSnippet getSeparatorAfterSnippet(JSSnippet target) {
    if (target != null) {
      boolean bAfterTarget = false;
      for (JSSnippet snippet : snippets) {
        if (snippet == target) {
          bAfterTarget = true;
        }
        if (bAfterTarget && snippet.getType().equals(JSSnippet.Type.Separator)) {
          return snippet;
        }
      }
    }
    return null;
  }

  public JSSnippet getSnippetForValue(Object value) {
    if (value != null) {
      for (JSSnippet snippet : snippets) {
        if (snippet.getType().equals(JSSnippet.Type.Value)) {
          if (snippet.getRelatedObject() == value) {
            return snippet;
          }
        }
      }
    }
    return null;
  }

  public void augmentEndSnippet(String extraText) {
    JSSnippet jssEnd = getLastSnippetByType(JSSnippet.Type.End);
    if (jssEnd != null) {
      jssEnd.appendText(extraText);
    }
    else {
      JSSnippet jssBody = getLastSnippetByType(JSSnippet.Type.Body);
      if (jssBody != null) {
        Object relatedObject = jssBody.getRelatedObject();
        if (IJSSnippetSupport.class.isAssignableFrom(relatedObject.getClass())) {
          JSSnippetSupport jsss = getLastSnippetSupportForObj(relatedObject);
          jsss.augmentEndSnippet(extraText);
        }
      }
    }
  }

  private static String getLeadingWhitespace(JSSnippetSupport jsss) {
    return getLeadingWhitespace(jsss, JSSnippetSupport.BASE, true);
  }

  private static String getLeadingWhitespace(JSSnippetSupport jsss, int precedingIndex, boolean firstCall) {
    String result = "";
    if (jsss != null) {
      boolean bHasNewline = false;
      int fromIndex = jsss.snippets.size();
      if (precedingIndex != JSSnippetSupport.BASE && precedingIndex < fromIndex) {
        fromIndex = precedingIndex;
      }
      for (int i = fromIndex; i-- > 0; ) {
        JSSnippet jss = jsss.snippets.get(i);
        if ((!firstCall && jss.getType() == JSSnippet.Type.End) ||
                jss.getType() == JSSnippet.Type.Separator ||
                jss.getType() == JSSnippet.Type.Start) {
          String text = jss.getText();
          for (int j = 0; j < text.length(); j++) {
            char ch = text.charAt(j);
            if (bHasNewline && Character.isWhitespace(ch)) {
              result += ch;
            }
            if (ch == '\n') {
              bHasNewline = true;
            }
          }
          if (bHasNewline) {
            break;
          }
        }
      }
      if (!bHasNewline) {
        return getLeadingWhitespace(jsss.preceding, jsss.precedingIndex, false);
      }
    }
    return result;
  }

  private static String getIndentBetween(String leadingWS1, String leadingWS2) {
    int lenIndent = leadingWS1.length() - leadingWS2.length();
    if (lenIndent < 0) {
      lenIndent = lenIndent * -1;
    }
    String result = "";
    for (int i=0; i<lenIndent; i++) {
      result += " ";
    }
    return result;
  }

  private static String getIndentBetween(List<String> leadingWhitespaceList) {
    String result = "  ";
    if (leadingWhitespaceList.size() > 1) {
      for (int i=0; i<leadingWhitespaceList.size(); i++) {
        String leadingWS1 = leadingWhitespaceList.get(i);
        String leadingWS2 = leadingWhitespaceList.get(i+1);
        if (leadingWS1.length() != leadingWS2.length()) {
          return getIndentBetween(leadingWS1, leadingWS2);
        }
      }
    }
    return result;
  }

  private static String getStandardIndent(JSSnippetSupport jsss, List<String> leadingWhitespaceList) {
    String leadingWhitespace = "";
    if (jsss != null) {
      boolean bHasNewline = false;
      for (int i = jsss.snippets.size(); i-- > 0; ) {
        JSSnippet jss = jsss.snippets.get(i);
        if (jss.getType() == JSSnippet.Type.End ||
            jss.getType() == JSSnippet.Type.Start) {
          String text = jss.getText();
          for (int j = 0; j < text.length(); j++) {
            char ch = text.charAt(j);
            if (bHasNewline && Character.isWhitespace(ch)) {
              leadingWhitespace += ch;
            }
            if (ch == '\n') {
              bHasNewline = true;
            }
          }
          if (bHasNewline) {
            leadingWhitespaceList.add(leadingWhitespace);
            leadingWhitespace = "";
            bHasNewline = false;
          }
        }
      }
      if (leadingWhitespaceList.size() < 4) {
        return getStandardIndent(jsss.getPreceding(), leadingWhitespaceList);
      }
      else {
        return getIndentBetween(leadingWhitespaceList);
      }
    }
    if (leadingWhitespaceList.size() >= 2) {
      return getIndentBetween(leadingWhitespaceList.get(0), leadingWhitespaceList.get(1));
    }
    return "";
  }

  private JSSnippetSupport getFirstSnippetSupportForObj(Object obj) {
    if (obj instanceof JSObject) {
      return ((JSObject)obj).snippets;
    }
    if (obj instanceof JSArray) {
      return ((JSArray)obj).snippets;
    }
    if (obj instanceof JSIdentifier) {
      return ((JSIdentifier)obj).snippets;
    }
    if (obj instanceof JSRegExLiteral) {
      return ((JSRegExLiteral)obj).snippets;
    }
    return null;
  }

  private JSSnippetSupport getLastSnippetSupportForObj(Object obj) {
    if (obj instanceof JSObject) {
      return ((JSObject)obj).snippets;
    }
    if (obj instanceof JSArray) {
      return ((JSArray)obj).snippets;
    }
    if (obj instanceof JSIdentifier) {
      JSIdentifier jsId = (JSIdentifier)obj;
      JSSnippetSupport result = jsId.snippets;
      while (jsId.getApply() != null) {
        jsId = jsId.getApply();
        result = jsId.snippets;
      }
      return result;
    }
    if (obj instanceof JSRegExLiteral) {
      return ((JSRegExLiteral)obj).snippets;
    }
    return null;
  }

  private static void extractLeadingWhitespaceFromSnippets(JSSnippetSupport jsss, String leadingWhitespace) {
    if (jsss != null) {
      String replaceStr = "\n";
      String findStr = replaceStr + leadingWhitespace;

      for (JSSnippet jss : jsss.snippets) {
        if (jss.getType() == JSSnippet.Type.End ||
                jss.getType() == JSSnippet.Type.Separator ||
                jss.getType() == JSSnippet.Type.Start) {
          String text = jss.getText();
          if (text.contains(findStr)) {
            text = text.replace(findStr, replaceStr);
            jss.setText(text);
          }
        }
      }
    }
  }

  private static void extractLeading(Object obj, String leadingWhitespace) {
    JSSnippetSupport jsss = null;
    if (obj instanceof JSObject) {
      JSObject jsObj = (JSObject) obj;
      jsss = jsObj.snippets;
      for (Object item : jsObj.values()) {
        extractLeading(item, leadingWhitespace);
      }
    } else if (obj instanceof JSArray) {
      JSArray jsArray = (JSArray) obj;
      jsss = jsArray.snippets;
      for (Object item : jsArray) {
        extractLeading(item, leadingWhitespace);
      }
    } else if (obj instanceof JSFunctionDefinition) {
      JSFunctionDefinition jsFunc = (JSFunctionDefinition)obj;
      jsss = jsFunc.snippets;
      for (Object item : jsFunc.getParameters()) {
        extractLeading(item, leadingWhitespace);
      }
    } else if (obj instanceof JSFunctionCall) {
      JSFunctionCall jsFunc = (JSFunctionCall)obj;
      jsss = jsFunc.snippets;
      for (Object item : jsFunc.getParameters()) {
        extractLeading(item, leadingWhitespace);
      }
    } else if (obj instanceof JSIdentifier) {
      JSIdentifier jsId = (JSIdentifier)obj;
      jsss = jsId.snippets;
      extractLeading(jsId.getApply(), leadingWhitespace);
    } else if (obj instanceof JSRegExLiteral) {
      jsss = ((JSRegExLiteral) obj).snippets;
    }
    extractLeadingWhitespaceFromSnippets(jsss, leadingWhitespace);
  }

  /**
   * Removes extra whitespace for the current snippets so that big indents are removed
   */
  public void removeExtraWhitespace(Object obj) {
    // 1. Find out the parent whitespace indent
    JSSnippetSupport jsss = getFirstSnippetSupportForObj(obj);
    String leadingWhitespace = getLeadingWhitespace(jsss.getPreceding());
    // 2. Remove this whitespace as contained within the contained snippets
    extractLeading(obj, leadingWhitespace);
  }


  private static void prefixLeadingWhitespaceToSnippet(JSSnippet jss, String leadingWhitespace) {
    if (jss != null) {
      if (jss.getType() == JSSnippet.Type.Start) {
        String text = jss.getText();
        jss.setText(leadingWhitespace + text);
      }
    }
  }

  private static void appendLeadingWhitespaceToSnippet(JSSnippet jss, String leadingWhitespace) {
    if (jss != null) {
      if (jss.getType() == JSSnippet.Type.End) {
        String text = jss.getText();
        int pos = text.lastIndexOf("\n");
        if (pos == text.length()-1) {
          jss.setText(text + leadingWhitespace);
        }
        else {
          jss.setText(text + "\n" + leadingWhitespace);
        }
      }
    }
  }

  private static void injectLeadingWhitespaceIntoSnippets(JSSnippetSupport jsss, String leadingWhitespace) {
    if (jsss != null) {
      String findStr = "\n";
      String replaceStr = findStr + leadingWhitespace;

      for (JSSnippet jss : jsss.snippets) {
        if (jss.getType() == JSSnippet.Type.End ||
                jss.getType() == JSSnippet.Type.Separator ||
                jss.getType() == JSSnippet.Type.Start) {
          String text = jss.getText();
          if (text.contains(findStr)) {
            text = text.replace(findStr, replaceStr);
            jss.setText(text);
          }
        }
      }
    }
  }

  private static void injectLeading(Object obj, String leadingWhitespace) {
    JSSnippetSupport jsss = null;
    if (obj instanceof JSObject) {
      JSObject jsObj = (JSObject) obj;
      jsss = jsObj.snippets;
      for (Object item : jsObj.values()) {
        injectLeading(item, leadingWhitespace);
      }
    } else if (obj instanceof JSArray) {
      JSArray jsArray = (JSArray) obj;
      jsss = jsArray.snippets;
      for (Object item : jsArray) {
        injectLeading(item, leadingWhitespace);
      }
    } else if (obj instanceof JSFunctionDefinition) {
      JSFunctionDefinition jsFunc = (JSFunctionDefinition)obj;
      jsss = jsFunc.snippets;
      for (Object item : jsFunc.getParameters()) {
        injectLeading(item, leadingWhitespace);
      }
    } else if (obj instanceof JSFunctionCall) {
      JSFunctionCall jsFunc = (JSFunctionCall)obj;
      jsss = jsFunc.snippets;
      for (Object item : jsFunc.getParameters()) {
        injectLeading(item, leadingWhitespace);
      }
    } else if (obj instanceof JSIdentifier) {
      JSIdentifier jsId = (JSIdentifier)obj;
      jsss = jsId.snippets;
      injectLeading(jsId.getApply(), leadingWhitespace);
    } else if (obj instanceof JSRegExLiteral) {
      jsss = ((JSRegExLiteral) obj).snippets;
    }
    injectLeadingWhitespaceIntoSnippets(jsss, leadingWhitespace);
  }

  private boolean areSnippetsMultiline(Object obj) {
    if (obj instanceof IJSSnippetSupport) {
      JSWriter writer = new JSWriter();
      ((IJSSnippetSupport)obj).composeSnippets(writer);
      String text = writer.toString();
      // Long pieces of text deserve their own line
      if (text.length() > 60)
        return true;
      // Else look to see if there are newlines in the text
      for (int j = 0; j < text.length(); j++) {
        char ch = text.charAt(j);
        if (ch == '\n') {
          return true;
        }
      }
    }
    return false;
  }

  private void prependPadding(Object obj, String padding) {
    if (obj instanceof IJSSnippetSupport) {
//      ((IJSSnippetSupport)obj).addSnippet(new JSSnippet(JSSnippet.Type.Prefix, padding));
      JSSnippetSupport jsss = getFirstSnippetSupportForObj(obj);
      JSSnippet jss = jsss.getFirstSnippetByType(JSSnippet.Type.Start);
      if (jss != null) {
        String text = jss.getText();
        jss.setText(padding + text);
      }
    }
  }

  private void insertNewlineToEmptyArrayOrObject(Object parentObject) {
    if (parentObject instanceof JSArray) {
      JSArray jsArray = (JSArray)parentObject;
      if (jsArray.size() == 0) {
        // we are dealing with [] scenario or something similar
        // Let's sort out like [
        //                     ]
        JSSnippetSupport jsss = getFirstSnippetSupportForObj(parentObject);
        JSSnippet jss = jsArray.snippets.getFirstSnippetByType(JSSnippet.Type.Start);
        if (jss != null) {
          String text = jss.getText();
          int pos = text.lastIndexOf("\n");
          if (pos != -1) {
            jss.setText(text.substring(0, pos) + "\n");
          }
          else {
            jss.setText(text + "\n");
          }
        }
      }
    }
    else
    if (parentObject instanceof JSObject) {
      JSObject jsObject = (JSObject)parentObject;
      if (jsObject.size() == 0) {
        // we are dealing with {} scenario or something similar
        // Let's sort out like {
        //                     }
        JSSnippetSupport jsss = getFirstSnippetSupportForObj(parentObject);
        JSSnippet jss = jsObject.snippets.getFirstSnippetByType(JSSnippet.Type.Start);
        if (jss != null) {
          String text = jss.getText();
          int pos = text.lastIndexOf("\n");
          if (pos != -1) {
            jss.setText(text.substring(0, pos) + "\n");
          }
          else {
            jss.setText(text + "\n");
          }
        }
      }
    }
  }

  private boolean needsIndent(Object parentObject, Object childObject) {
    if (parentObject instanceof JSArray) {
      JSArray jsArray = (JSArray)parentObject;
      if (jsArray.contains(childObject)) {
        if (jsArray.get(0).equals(childObject)) {
          return true;
        }
      }
      else {
        if (jsArray.size() == 0) {
          return true;
        }
      }
    }
    else
    if (parentObject instanceof JSObject) {
      JSObject jsObject = (JSObject)parentObject;
      if (jsObject.containsValue(childObject)) {
        if (jsObject.get(0).equals(childObject)) {
          return true;
        }
      }
      else {
        if (jsObject.size() == 0) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Inject extra whitespace based on the preceding snippets
   * @param parentObject
   * @param childObject
   */
  public void injectWhitespace(Object parentObject, Object childObject) {
    // 1. Find out the whitespace indent based on the current context
    // Need the object we are embedding into
    JSSnippetSupport jsss = getFirstSnippetSupportForObj(parentObject);
    String parentLeadingWhitespace = getLeadingWhitespace(jsss);
    String leadingWhitespace = parentLeadingWhitespace;
    boolean bAddedNewlineToEmptyArrayOrObject = false;

    boolean bChildIsMultiline = areSnippetsMultiline(childObject);

      // 2. See if indent is needed because of empty parent container
    if (needsIndent(parentObject, childObject)) {
      // 2.1 Indent extra whitespace on first line of child and update leadingWhitespace to take into account indent
      List<String> leadingWhitespaceList = new ArrayList<String>();
      String standardIndent = getStandardIndent(jsss, leadingWhitespaceList);
      leadingWhitespace = leadingWhitespace + standardIndent;

      // 2.2 Format empty parent appropriately so that child insert looks neat (add extra newline)
      if (bChildIsMultiline) {
        bAddedNewlineToEmptyArrayOrObject = true;
        insertNewlineToEmptyArrayOrObject(parentObject);
      }
      if (parentObject instanceof JSArray) {
        prependPadding(childObject, leadingWhitespace);
      }
    }
    else {
      if (bChildIsMultiline && parentObject instanceof JSArray) {
        prependPadding(childObject, "\n");
      }
    }

    // 3. Inject this whitespace into the contained snippets
    injectLeading(childObject, leadingWhitespace);

    // 4. If we added newline to parent then add preceding parent whitespace onto the end of the last child snippets
    if (bAddedNewlineToEmptyArrayOrObject) {
      JSSnippetSupport childJsss = getLastSnippetSupportForObj(childObject);
      if (childJsss != null && childJsss.snippets.size() > 0) {
        appendLeadingWhitespaceToSnippet(childJsss.snippets.get(childJsss.snippets.size() - 1), parentLeadingWhitespace);
      }
    }

    // 5. Add preceding parent info
    JSSnippetSupport jsssChild = getFirstSnippetSupportForObj(childObject);
    // TODO: Work out where child is beinf inserted so preceding index can be correctly set
    jsssChild.setPreceding(jsss, JSSnippetSupport.BASE);
  }

}
