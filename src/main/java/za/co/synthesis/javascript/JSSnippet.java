package za.co.synthesis.javascript;

public class JSSnippet {
  public static enum Type {
    Start,
    Variable,
    Value,
    Separator,
    Body,
    End
  }

  private final Type type;
  private String name;
  private String text;
  private Object relatedObject;

  public JSSnippet(Type type, String text, Object relatedObject) {
    this.text = text;
    this.type = type;
    this.relatedObject = relatedObject;
  }

  public JSSnippet(Type type, String text) {
    this.text = text;
    this.type = type;
    this.relatedObject = null;
  }

  public String getText() {
    return text;
  }

  public Type getType() {
    return type;
  }

  public JSSnippet setText(String text) {
    this.text = text;
    return this;
  }

  public String getName() {
    return name;
  }

  public JSSnippet setName(String name) {
    this.name = name;
    return this;
  }

  public Object getRelatedObject() {
    return relatedObject;
  }

  public JSSnippet setRelatedObject(Object relatedObject) {
    this.relatedObject = relatedObject;
    return this;
  }

  public void appendText(String extraText) {
    if (text != null) {
      text += extraText;
    }
    else {
      text = extraText;
    }
  }
}
