package za.co.synthesis.rule.difference.validation;

public enum DifferenceType {
  Added,
  Changed,
  Removed
}
