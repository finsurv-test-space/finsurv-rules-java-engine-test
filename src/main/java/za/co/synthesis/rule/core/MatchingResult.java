package za.co.synthesis.rule.core;

/**
 * Created by jake on 6/15/16.
 */
public class MatchingResult {
  private final double confidence;
  private final String matchName;

  public MatchingResult(double confidence, String matchName) {
    this.confidence = confidence;
    this.matchName = matchName;
  }

  public double getConfidence() {
    return confidence;
  }

  public String getMatchName() {
    return matchName;
  }
}
