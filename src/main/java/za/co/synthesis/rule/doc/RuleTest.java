package za.co.synthesis.rule.doc;

public class RuleTest {
  private String ruleName;
  private String fieldName;
  private TestStatus testStatus;
  private String failReason;
  private String assertFunc;
  private String reportData;
  private String customData;

  public RuleTest() {
  }

  public RuleTest(final RuleTest copyFrom) {
    ruleName = copyFrom.ruleName;
    fieldName = copyFrom.fieldName;
    testStatus = copyFrom.testStatus;
    failReason = copyFrom.failReason;
    assertFunc = copyFrom.assertFunc;
    reportData = copyFrom.reportData;
    customData = copyFrom.customData;
  }

  public String getRuleName() {
    return ruleName;
  }

  public void setRuleName(String ruleName) {
    this.ruleName = ruleName;
  }

  public String getFieldName() {
    return fieldName;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }

  public TestStatus getTestStatus() {
    return testStatus;
  }

  public void setTestStatus(TestStatus testStatus) {
    this.testStatus = testStatus;
  }

  public String getFailReason() {
    return failReason;
  }

  public void setFailReason(String failReason) {
    this.failReason = failReason;
  }

  public String getAssertFunc() {
    return assertFunc;
  }

  public void setAssertFunc(String assertFunc) {
    this.assertFunc = assertFunc;
  }

  public String getReportData() {
    return reportData;
  }

  public void setReportData(String reportData) {
    this.reportData = reportData;
  }

  public String getCustomData() {
    return customData;
  }

  public void setCustomData(String customData) {
    this.customData = customData;
  }
}
