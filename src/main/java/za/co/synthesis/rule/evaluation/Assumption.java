package za.co.synthesis.rule.evaluation;

import za.co.synthesis.rule.core.BankAccountType;
import za.co.synthesis.rule.core.DrCr;
import za.co.synthesis.rule.core.ResidenceStatus;
import za.co.synthesis.rule.evaluation.matchpredef.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/28/16.
 */
public class Assumption {
  private final String scenario;
  private final IMatchAssertionFunction matchAssertion;
  private final List<AssumptionRule> rules;

  private final DrCr knownSide;
  private final ResidenceStatus resStatus;
  private final List<BankAccountType> accType;

  public Assumption(String scenario, DrCr knownSide, ResidenceStatus resStatus, List<BankAccountType> accType, List<AssumptionRule> rules) {
    this.scenario = scenario;

    this.matchAssertion = makeMatchAssertionFrom(knownSide, resStatus, accType);
    this.rules = rules;
    for (AssumptionRule rule : rules) {
      rule.updateAssumptionRuleType(knownSide);
    }

    this.knownSide = knownSide;
    this.resStatus = resStatus;
    this.accType = accType;
  }

  public Assumption(String scenario, IMatchAssertionFunction matchAssertion, List<AssumptionRule> rules) {
    this.scenario = scenario;

    this.matchAssertion = matchAssertion;
    this.rules = rules;

    this.knownSide = DrCr.Unknown;
    this.resStatus = ResidenceStatus.Unknown;
    this.accType = new ArrayList<BankAccountType>();
  }

  private IMatchAssertionFunction makeMatchAssertionFrom(DrCr knownSide, ResidenceStatus resStatus, List<BankAccountType> accType) {
    if (knownSide.equals(DrCr.DR)) {
      IMatchAssertionFunction drThisBank = new drThisBank();
      IMatchAssertionFunction notCrThisBank = new not(new crThisBank());
      IMatchAssertionFunction start = new MAFAnd(drThisBank, notCrThisBank);

      drHasResStatus hasResStatus = new drHasResStatus();
      hasResStatus.setConstant(resStatus);
      drHasAccType hasAccType = new drHasAccType();
      hasAccType.setConstants(accType);
      IMatchAssertionFunction end = new MAFAnd(hasResStatus, hasAccType);

      return new MAFAnd(start, end);
    }
    else if (knownSide.equals(DrCr.CR)) {
      IMatchAssertionFunction crThisBank = new crThisBank();
      IMatchAssertionFunction notDrThisBank = new not(new drThisBank());
      IMatchAssertionFunction start = new MAFAnd(crThisBank, notDrThisBank);

      crHasResStatus hasResStatus = new crHasResStatus();
      hasResStatus.setConstant(resStatus);
      crHasAccType hasAccType = new crHasAccType();
      hasAccType.setConstants(accType);
      IMatchAssertionFunction end = new MAFAnd(hasResStatus, hasAccType);

      return new MAFAnd(start, end);
    }
    return new returnFalse();
  }

  public String getScenario() {
    return scenario;
  }

  public IMatchAssertionFunction getMatchAssertion() {
    return matchAssertion;
  }

  public List<AssumptionRule> getRules() {
    return rules;
  }

  public boolean isKnownSide(DrCr drcr) {
    return knownSide.equals(drcr);
  }

  public ResidenceStatus getResStatus() {
    return resStatus;
  }

  public List<BankAccountType> getAccType() {
    return accType;
  }
}
