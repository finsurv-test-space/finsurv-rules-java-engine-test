package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.*;

import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 4/12/16
 * Time: 12:54 PM
 * A validation object contains the rule that will be run in the future. This intermediary object allows different
 * messages and codes to be associated with a validation run and also allows multiple validations to be associated
 * with a single rule (array fields)
 */
public class Display {
  private final Scope scope;
  private final String field;
  private final DisplayRuleType type;
  private final String setField;
  private final List<String> values;
  private final IDisplayFunction displayFunction;
  private final IAssertionFunction assertion;
  private final List<String> categories;
  private final List<String> notCategories;

  public Display(Scope scope, String field, DisplayRuleType type, String setField,
                 List<String> values, IDisplayFunction displayFunction,
                 IAssertionFunction assertion, List<String> categories, List<String> notCategories) {
    this.scope = scope;
    this.field = field;
    this.type = type;
    this.setField = setField;
    this.values = values;
    this.displayFunction = displayFunction;
    this.assertion = assertion;
    this.categories = categories;
    this.notCategories = notCategories;
  }

  private boolean mustRunRule(Scope scope, String field, Object value) {
    return (value == null || !(value instanceof IFinsurvContext.Undefined));
  }

  private void logDisplayEvent(FinsurvContext context, StringList appendedFields,
                               IAssertionFunction assertion, Scope scope, DisplayRuleType type,
                               String field, List<String> settings, IDisplayFunction displayFunction, int moneyInstance, int ieInstance) {

    boolean logEvent = true;

    Object fieldValue = null;
    if (scope == Scope.Transaction) {
      fieldValue = context.getTransactionField(field);
    }
    if (scope == Scope.Money) {
      if (moneyInstance < 0) {
        moneyInstance = 0;
      }
      fieldValue = context.getMoneyField(moneyInstance, field);
    }
    if (scope == Scope.ImportExport) {
      if (moneyInstance < 0) {
        moneyInstance = 0;
      }
      if (ieInstance < 0) {
        ieInstance = 0;
      }
      fieldValue = context.getImportExportField(moneyInstance, ieInstance, field);
    }

    if (mustRunRule(scope, field, fieldValue)) {
      Object newValue = null;
      List<String> newValues = null;
      if (displayFunction != null) {
        newValue = displayFunction.execute(context, fieldValue);

        if (newValue == null) {
          logEvent = false;
        }
        else {
          newValues = new ArrayList<String>();
          newValues.add(newValue.toString());
        }
      }

      // Filter out events where value does not change
      if (type == DisplayRuleType.SetValue &&
              context.getDisplayRuleBehaviour().isFilterUnchangedValues() &&
              fieldValue != null && fieldValue.equals(newValue)) {
        logEvent = false;
      }

      if (logEvent && assertion != null &&
              !assertion.execute(context, fieldValue)) {
        if (type == DisplayRuleType.SetValue && context.getDisplayRuleBehaviour().isSetOriginalValueOnFailedAssertion() && fieldValue != null) {
          newValue = fieldValue;
          newValues = new ArrayList<String>();
          newValues.add(newValue.toString());
        }
        else
          logEvent = false;
      }

      if (logEvent) {
        if (type == DisplayRuleType.AppendValue) {
          if (!appendedFields.contains(field + ":" + moneyInstance + ":" + ieInstance)) {
            type = DisplayRuleType.SetValue;
            appendedFields.add(field + ":" + moneyInstance + ":" + ieInstance);
          }
        }
        else
        if (type == DisplayRuleType.SetField) {
          DSLSupport.ScopedField scopedField = DSLSupport.parseCompoundField(context.getCurrentScope(), setField);
          scope = scopedField.getScope();
          field = scopedField.getField();
          if (scope == Scope.Money) {
            if (moneyInstance < 0) {
              moneyInstance = 0;
            }
          }
          if (scope == Scope.ImportExport) {
            if (moneyInstance < 0) {
              moneyInstance = 0;
            }
            if (ieInstance < 0) {
              ieInstance = 0;
            }
          }
        }

        if (scope == Scope.Transaction) {
          context.logTransactionDisplayEvent(type, field, newValues != null ? newValues : settings);
        }
        if (scope == Scope.Money) {
          context.logMoneyDisplayEvent(type, moneyInstance, field, newValues != null ? newValues : settings);
        }
        if (scope == Scope.ImportExport) {
          context.logImportExportDisplayEvent(type, moneyInstance, ieInstance, field, newValues != null ? newValues : settings);
        }
      }
    }
  }

  public boolean run(FinsurvContext context, StringList appendedFields) {
    boolean matchedRule = false;
    boolean runRule;
    int moneySize;
    String moneyCategory;
    String moneyMainCategory;
    int i;

    context.setCurrentScope(scope);

    if (scope == Scope.Transaction) {
      matchedRule = true;
      logDisplayEvent(context, appendedFields, assertion, scope, type, field, values, displayFunction, -1, -1);
    } else if (scope == Scope.Money) {
      moneySize = context.getMoneySize();
      for (i = 0; i < moneySize; i++) {
        runRule = true;
        moneyCategory = context.getCategories().get(i);
        moneyMainCategory = context.getMainCategories().get(i);
        if (categories != null && !categories.contains(moneyCategory) && !categories.contains(moneyMainCategory))
          runRule = false;
        if (notCategories != null && !DSLSupport.categoryNotInList(moneyCategory, moneyMainCategory, notCategories))
          runRule = false;
        if (runRule) {
          matchedRule = true;
          context.setCurrentMoneyInstance(i);
          logDisplayEvent(context, appendedFields, assertion, scope, type, field, values, displayFunction, i, -1);
        }
      }
    } else if (scope == Scope.ImportExport) {
      moneySize = context.getMoneySize();
      for (i = 0; i < moneySize; i++) {
        runRule = true;
        moneyCategory = context.getCategories().get(i);
        moneyMainCategory = context.getMainCategories().get(i);
        if (categories != null && !categories.contains(moneyCategory) && !categories.contains(moneyMainCategory))
          runRule = false;
        if (notCategories != null && !DSLSupport.categoryNotInList(moneyCategory, moneyMainCategory, notCategories))
          runRule = false;

        if (runRule) {
          int importSize = context.getImportExportSize(i);
          for (int j = 0; j < importSize; j++) {
            matchedRule = true;
            context.setCurrentMoneyInstance(i);
            context.setCurrentImportExportInstance(j);
            logDisplayEvent(context, appendedFields, assertion, scope, type, field, values, displayFunction, i, j);
          }
        }
      }
    }
    return matchedRule;
  }
}
