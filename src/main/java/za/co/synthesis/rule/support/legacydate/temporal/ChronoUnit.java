package za.co.synthesis.rule.support.legacydate.temporal;

import za.co.synthesis.rule.support.legacydate.Temporal;

public enum ChronoUnit {
  SECONDS("Seconds", 1),
  /**
   * Unit that represents the concept of a minute.
   * For the ISO calendar system, it is equal to 60 seconds.
   */
  MINUTES("Minutes", 60),
  /**
   * Unit that represents the concept of an hour.
   * For the ISO calendar system, it is equal to 60 minutes.
   */
  HOURS("Hours", 3600),
  /**
   * Unit that represents the concept of half a day, as used in AM/PM.
   * For the ISO calendar system, it is equal to 12 hours.
   */
  HALF_DAYS("HalfDays", 43200),
  /**
   * Unit that represents the concept of a day.
   * For the ISO calendar system, it is the standard day from midnight to midnight.
   * The estimated duration of a day is {@code 24 Hours}.
   * <p>
   * When used with other calendar systems it must correspond to the day defined by
   * the rising and setting of the Sun on Earth. It is not required that days begin
   * at midnight - when converting between calendar systems, the date should be
   * equivalent at midday.
   */
  DAYS("Days", 86400),
  /**
   * Unit that represents the concept of a week.
   * For the ISO calendar system, it is equal to 7 days.
   * <p>
   * When used with other calendar systems it must correspond to an integral number of days.
   */
  WEEKS("Weeks", 7 * 86400L),
  /**
   * Unit that represents the concept of a month.
   * For the ISO calendar system, the length of the month varies by month-of-year.
   * The estimated duration of a month is one twelfth of {@code 365.2425 Days}.
   * <p>
   * When used with other calendar systems it must correspond to an integral number of days.
   */
  MONTHS("Months", 31556952L / 12),
  /**
   * Unit that represents the concept of a year.
   * For the ISO calendar system, it is equal to 12 months.
   * The estimated duration of a year is {@code 365.2425 Days}.
   * <p>
   * When used with other calendar systems it must correspond to an integral number of days
   * or months roughly equal to a year defined by the passage of the Earth around the Sun.
   */
  YEARS("Years", 31556952L),
  /**
   * Unit that represents the concept of a decade.
   * For the ISO calendar system, it is equal to 10 years.
   * <p>
   * When used with other calendar systems it must correspond to an integral number of days
   * and is normally an integral number of years.
   */
  DECADES("Decades", 31556952L * 10L),
  /**
   * Unit that represents the concept of a century.
   * For the ISO calendar system, it is equal to 100 years.
   * <p>
   * When used with other calendar systems it must correspond to an integral number of days
   * and is normally an integral number of years.
   */
  CENTURIES("Centuries", 31556952L * 100L),
  /**
   * Unit that represents the concept of a millennium.
   * For the ISO calendar system, it is equal to 1000 years.
   * <p>
   * When used with other calendar systems it must correspond to an integral number of days
   * and is normally an integral number of years.
   */
  MILLENNIA("Millennia", 31556952L * 1000L);

  private final String name;
  private final long durationSeconds;

  private ChronoUnit(String name, long durationSeconds) {
    this.name = name;
    this.durationSeconds = durationSeconds;
  }

  //-----------------------------------------------------------------------
  /**
   * Checks if this unit is a date unit.
   *
   * @return true if a date unit, false if a time unit
   */
  public boolean isDateBased() {
    return this.compareTo(DAYS) >= 0;
  }

  /**
   * Checks if this unit is a time unit.
   *
   * @return true if a time unit, false if a date unit
   */
  public boolean isTimeBased() {
    return this.compareTo(DAYS) < 0;
  }

  public long between(Temporal temporal1Inclusive, Temporal temporal2Exclusive) {
    long span = temporal1Inclusive.getCalendar().getTimeInMillis() - temporal2Exclusive.getCalendar().getTimeInMillis();
    if (span < 0L) {
      span *= -1L;
    }
    return span/1000L/durationSeconds;
  }

  @Override
  public String toString() {
    return name;
  }

}
