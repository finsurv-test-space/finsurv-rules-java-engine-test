package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 11:02 AM
 * Determines if the current value does not equal the given transaction field
 */
public class notMatchesTransactionField implements IAssertionFunction {
  private String field;

  public notMatchesTransactionField(String field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField(field);
    if (fieldValue != null) {
      if (fieldValue.equals(value))
        return false;
    }
    return true;
  }
}
/*
function notMatchesTransactionField(field) {
  return function(context, value) {
    var fieldValue = context.getTransactionField(field);
    if (fieldValue) {
      if (fieldValue === value)
        return false;
    }
    return true;
  }
}
*/