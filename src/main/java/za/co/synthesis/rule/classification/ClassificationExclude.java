package za.co.synthesis.rule.classification;

/**
 * Created by jake on 6/7/16.
 */
public class ClassificationExclude extends ClassificationRule {
  public ClassificationExclude(String name, IClassificationFunction assertion) {
    super("exclude", name, assertion);
  }
}
