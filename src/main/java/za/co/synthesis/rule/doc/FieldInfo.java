package za.co.synthesis.rule.doc;

import za.co.synthesis.rule.core.Scope;

import java.util.List;

public class FieldInfo {
  private String channel;
  private Scope scope;
  private List<String> fieldNames;
  private String commonFieldLabel;
  private Integer minLen;
  private Integer maxLen;
  private Integer len;

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public Scope getScope() {
    return scope;
  }

  public void setScope(Scope scope) {
    this.scope = scope;
  }

  public List<String> getFieldNames() {
    return fieldNames;
  }

  public void setFieldNames(List<String> fieldNames) {
    this.fieldNames = fieldNames;
  }

  public String getCommonFieldLabel() {
    return commonFieldLabel;
  }

  public void setCommonFieldLabel(String commonFieldLabel) {
    this.commonFieldLabel = commonFieldLabel;
  }

  public Integer getMinLen() {
    return minLen;
  }

  public void setMinLen(Integer minLen) {
    this.minLen = minLen;
  }

  public Integer getMaxLen() {
    return maxLen;
  }

  public void setMaxLen(Integer maxLen) {
    this.maxLen = maxLen;
  }

  public Integer getLen() {
    return len;
  }

  public void setLen(Integer len) {
    this.len = len;
  }
}
