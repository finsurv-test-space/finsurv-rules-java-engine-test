package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 3/2/15
 * Time: 4:53 PM
 * Used to define the different types of reportable states defined by the evaluation DSL
 */

/**
 * Specifies if the transaction is reportable or not and if reportable then it determines the nature of the
 * ReportingQualifier under which the transaction should be reported.
 */
public enum ReportableType {
  /**
   * No rule has been provided in the configuration to cover this scenario
   */
  Unknown("UNKNOWN", 0),
  /**
   * This is a recognised legal transaction but the system does not support it.
   */
  Unsupported("UNSUPPORTED", 1),
  /**
   * This is an illegal transaction. The the given parameters make no sense
   */
  Illegal("ILLEGAL", 2),
  /**
   * This is a reportable transaction. The ReportingQualifier must be set to BOPCUS
   */
  Reportable("BOPCUS", 10),
  /**
   * This is a reportable transaction. The ReportingQualifier must be set to NON REPORTABLE
   */
  ZZ1Reportable("NON REPORTABLE", 9),
  /**
   * This is a reportable transaction. The ReportingQualifier must be set to BOPCARD RESIDENT
   */
  CARDResReportable("BOPCARD RESIDENT", 8),
  /**
   * This is a reportable transaction. The ReportingQualifier must be set to BOPCARD NON RESIDENT
   */
  CARDNonResReportable("BOPCARD NON RESIDENT", 7),
  /**
   * This transaction need not be reported to the SARB at all. E.g. Resident Rand to Resident Rand
   */
  NotReportable("NOT REPORTABLE", 3);


  private String value;

  private int onerousScore;

  ReportableType(String value, int onerousScore){
    this.value = value;
    this.onerousScore = onerousScore;
  }

  public String toString(){
    return this.value;
  }

  public String getReportingQualifier(){
    return this.value;
  }

  public static ReportableType fromString(String value){
    if(value != null) {
      for(ReportableType reportableType : ReportableType.values()) {
        if (value.equalsIgnoreCase(reportableType.value)) {
          return reportableType;
        }
      }
    }
    return ReportableType.Unknown;
  }

  public int getOnerousScore() {
    return onerousScore;
  }

  public boolean potentialReport() {
    return onerousScore > 3;
  }
}
