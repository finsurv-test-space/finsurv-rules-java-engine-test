package za.co.synthesis;

import org.junit.Assert;
import org.junit.Test;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.FlowType;

import java.io.FileReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SnippetTests {
  private boolean areStringEquals(String str1, String str2) {
    int minLen = str1.length();
    if (str2.length() < minLen) {
      minLen = str2.length();
    }

    boolean result = true;
    String line1 = "";
    String line2 = "";
    for (int i=0; i<minLen; i++) {
      char ch1 = str1.charAt(i);
      char ch2 = str2.charAt(i);
      if (ch1 != ch2) {
        result = false;
      }
      if (!result && (ch1 == '\n' || ch2 == '\n')) {
        line1 += Character.toString(ch1);
        line2 += Character.toString(ch2);
        break;
      }
      if (ch1 == '\n') {
        line1 = "";
      }
      else {
        line1 += Character.toString(ch1);
      }
      if (ch2 == '\n') {
        line2 = "";
      }
      else {
        line2 += Character.toString(ch2);
      }
    }
    if(!result) {
      System.out.println("Line 1:" + line1 + "|");
      System.out.println("Line 2:" + line2 + "|");
    }

    if (result && str1.length() != str2.length()) {
      System.out.println("Strings are of different lengths: " + str1.length() + " versus " + str2.length());
      result = false;
    }
    return result;
  }

  private String composeOriginal(Object obj) {
    JSWriter writer = new JSWriter();
    writer.setIndent("  ");
    writer.setNewline("\n");

    if (obj instanceof JSObject) {
      if (obj instanceof JSScope) {
        JSScope jsScope = (JSScope) obj;
        jsScope.compose(writer);
      }
      else {
        JSObject jsObj = (JSObject) obj;
        jsObj.compose(writer);
      }
      return writer.toString();
    }
    Assert.fail("Cannot compose: Expecting JSObject or JSScope");
    return "ERROR!!!!";
  }

  private String composeViaSnippets(Object obj) {
    JSWriter writer = new JSWriter();
    if (obj instanceof JSObject) {
      if (obj instanceof JSScope) {
        JSScope jsScope = (JSScope) obj;
        jsScope.composeSnippets(writer);
      }
      else {
        JSObject jsObj = (JSObject) obj;
        jsObj.composeSnippets(writer);
      }
      return writer.toString();
    }
    else if (obj instanceof JSFunctionCall) {
      JSFunctionCall jsFunc = (JSFunctionCall) obj;
      jsFunc.composeSnippets(writer);
      return writer.toString();
    }
    Assert.fail("Cannot compose: Expecting JSObject, JSScope or JSFunctionCall");
    return "ERROR!!!!";
  }

  @Test
  public void buildAndComposeJS1() throws Exception {
    JSObject jsObj = new JSObject();
    JSObject jsInnerObj = new JSObject();
    jsObj.put("inner", jsInnerObj);

    JSWriter jsWriter = new JSWriter();
    jsWriter.setIndent("  ");
    jsWriter.setNewline("\n");
    jsObj.compose(jsWriter);
    String original = jsWriter.toString();

    JSStructureParser parser = new JSStructureParser(original);

    Object obj = parser.parse();

    String composed = composeViaSnippets(obj);
    Assert.assertTrue(areStringEquals(original, composed));
  }

  private String testObjectAndArrayJS() {
    return  "{\n" +
            "  ReportingQualifier: 'BOPCUS', Flow: 'OUT',\n" +
            "  TrnReference: 'TestMessageRef', // single line comma\n" +
            "  ValueDate: '2020-12-25',\n" +
            "  ReplacementOriginalReference: 'OrigTrnRef',\n" +
            "  ReplacementTransaction: 'Y', \n" +
            "  Resident: { Individual: { CustomsClientNumber: '12345678' /*An example dummy CCN*/, IDNumber: '6811035039084' }},\n" +
            "  MonetaryAmount: [\n" +
            "    {\n" +
            "       DomesticValue: '20000.10',\n" +
            "       AdHocRequirement: { Subject: 'SDA' }\n" +
            "    },\n" +
            "    {\n" +
            "       DomesticValue: '20000.20',\n" +
            "       MoneyTransferAgent: 'AD'\n" +
            "    }\n" +
            "  ]\n" +
            "}\n";
  }

  @Test
  public void parseAndComposeJS1() throws Exception {
    String original = testObjectAndArrayJS();
    JSStructureParser parser = new JSStructureParser(original);

    Object obj = parser.parse();

    String composed = composeViaSnippets(obj);
    Assert.assertTrue(areStringEquals(original, composed));
  }


  private String testFunctionsJS() {
    return  "define(function () {\n" +
            "  return function (predef) {\n" +
            "    var feature;\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Transaction Checksum Rules\",\n" +
            "        scope: \"transaction\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"TotalValue\",\n" +
            "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")\n" +
            "            ]\n" +
            "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  @Test
  public void parseAndComposeJS2() throws Exception {
    String original = testFunctionsJS();
    JSStructureParser parser = new JSStructureParser(original);

    Object obj = parser.parse();

    String composed = composeViaSnippets(obj);
    Assert.assertTrue(areStringEquals(original, composed));
  }

  private String testRegexJS() {
    return  "define(function () {\n" +
            "  return function (predef) {\n" +
            "    var feature;\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Money Test Rules\",\n" +
            "        scope: \"money\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber\",\n" +
            "            rules : [\n" +
            "              failure(\"za_maian4\", \"IAN\", \"InternalAuthNumber needs to be 8 numbers\",\n" +
            "                  notEmpty.and(hasMoneyFieldValue(map(\"{{Regulator}}Auth.AuthIssuer\"), \"THIS BANK\")\n" +
            "                  .or(hasMoneyFieldValue(map(\"{{Regulator}}Auth.AuthIssuer\"), \"OTHER BANK\"))).and(notPattern(/^\\d{8}$/)))\n" +
            "            ]\n" +
            "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  @Test
  public void parseAndComposeJS3() throws Exception {
    String original = testRegexJS();
    JSStructureParser parser = new JSStructureParser(original);

    Object obj = parser.parse();

    String composed = composeViaSnippets(obj);
    Assert.assertTrue(areStringEquals(original, composed));
  }

  private String testMoreComplexJS() {
    return  "define(function (abc, xyz) {\n" +
            "  return function (predef) {\n" +
            "    var feature, turd;\n" +
            "    turd = { stuff: 'nonsense'};\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Transaction Checksum Rules\",\n" +
            "        scope: \"transaction\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"TotalValue\",\n" +
            "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")\n" +
            "            ]\n" +
            "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    turd = '42';\n" +
            "    turd = '44'; //Komment\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  @Test
  public void parseAndComposeJS4() throws Exception {
    String original = testMoreComplexJS();
    JSStructureParser parser = new JSStructureParser(original);

    Object obj = parser.parse();

    String composed = composeViaSnippets(obj);
    Assert.assertTrue(areStringEquals(original, composed));
  }

  private String expectingMoreComplexEditJS() {
    return  "define(function (abc, xyz) {\n" +
            "  return function (predef) {\n" +
            "    var feature, turd;\n" +
            "    turd = { stuff: 'nonsense'};\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Updated Rules\",\n" +
            "        scope: \"transaction\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"EditedField\",\n" +
            "            rules : [\n" +
            "              failure(\"totv1\", 244, \"Totally new message\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")\n" +
            "            ]\n" +
            "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    turd = '42';\n" +
            "    turd = '44'; //Komment\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  @Test
  public void parseAndEditJS5() throws Exception {
    String original = testMoreComplexJS();
    JSStructureParser parser = new JSStructureParser(original);

    Object obj = parser.parse();

    JSObject jsFeature = JSUtil.findObjectWith(obj, "ruleset");
    jsFeature.put("ruleset", "Updated Rules");

    JSObject jsVal0 = JSUtil.findObjectWith(obj, "rules");
    jsVal0.put("field", "EditedField");

    JSArray jsRules = (JSArray)jsVal0.get("rules");
    JSFunctionCall fsFunc0 = (JSFunctionCall)jsRules.get(0);
    fsFunc0.getParameters().set(2, "Totally new message");
    String composed = composeViaSnippets(obj);
    String expecting = expectingMoreComplexEditJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  private String testReplacementRulesJS() {
    return  "[\n" +
            "  failure(\"totv1\", 100, \"new logic1\",\n" +
            "          isEmpty).onOutflow().onSection(\"ABCDEG\"),\n" +
            "  failure(\"totv2\", 101, \"new logic2\",\n" +
            "          isDefined.and(notSumTotalValue)).onSection(\"A\")\n" +
            "]";
  }

  private String testInjectedReplacementRulesJS() {
    return  "define(function () {\n" +
            "  return function (predef) {\n" +
            "    var feature;\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Transaction Checksum Rules\",\n" +
            "        scope: \"transaction\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"TotalValue\",\n" +
            "            rules : [\n" +
            "  failure(\"totv1\", 100, \"new logic1\",\n" +
            "          isEmpty).onOutflow().onSection(\"ABCDEG\"),\n" +
            "  failure(\"totv2\", 101, \"new logic2\",\n" +
            "          isDefined.and(notSumTotalValue)).onSection(\"A\")\n" +
            "]\n" +
            "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  @Test
  public void parseAndInjectJS6() throws Exception {
    String original = testFunctionsJS();
    JSStructureParser parserOrig = new JSStructureParser(original);

    Object objOrig = parserOrig.parse();

    String inject = testReplacementRulesJS();
    JSStructureParser parserInject = new JSStructureParser(inject);

    Object objInject = parserInject.parse();

    JSObject jsVal0 = JSUtil.findObjectWith(objOrig, "rules");
    jsVal0.put("rules", objInject);
    String composed = composeViaSnippets(objOrig);
    String expecting = testInjectedReplacementRulesJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }


  // --- Helper functions for readParseInjectAndComposeJS1() ---

  private JSArray getRulesForField(JSArray validations, List<String> fieldNames) {
    for (Object objFieldRule : validations) {
      if (objFieldRule instanceof JSObject) {
        boolean bFound = false;
        JSObject fieldRule = (JSObject)objFieldRule;
        Object objField = fieldRule.get("field");
        if (objField instanceof String) {
          if (objField.equals(fieldNames.get(0)) && fieldNames.size() == 1) {
            bFound = true;
          }
        }
        else {
          if( objField instanceof JSArray) {
            JSArray fields = (JSArray)objField;
            if (fields.size() == fieldNames.size()) {
              bFound = true;
              for (int i=0; i<fields.size(); i++) {
                if (!fields.get(i).equals(fieldNames.get(i))) {
                  bFound = false;
                  break;
                }
              }
            }
          }
        }
        if (bFound) {
          Object objRules = fieldRule.get("rules");
          if (objRules instanceof JSArray) {
            return (JSArray) objRules;
          }
        }
      }
    }
    return null;
  }

  private void updateRuleInRules(JSArray rules, JSFunctionCall rule) {
    String editedRuleName = (String)rule.getParameters().get(0);

    for (int i=0; i<rules.size(); i++) {
      Object objCall = rules.get(i);
      if (objCall instanceof JSFunctionCall) {
        JSFunctionCall jsCall = (JSFunctionCall)objCall;
        String ruleName = (String)jsCall.getParameters().get(0);
        if (ruleName.equals(editedRuleName)) {
          rules.set(i, rule);
          break;
        }
      }
    }
  }

  private JSFunctionCall getJSFunctionCall(Reader ruleDSLReader) throws Exception {
    JSStructureParser parser = new JSStructureParser(ruleDSLReader);
    Object objNewRule;
    objNewRule = parser.parse();

    JSFunctionCall jsNewRule = null;
    if (objNewRule instanceof JSFunctionCall) {
      jsNewRule = (JSFunctionCall)objNewRule;
    }
    if (objNewRule instanceof JSScope) {
      JSScope scope = ((JSScope)objNewRule);
      if (scope.size() == 1) {
        for (Object value : scope.values()) {
          if (value instanceof JSFunctionCall)
            jsNewRule = (JSFunctionCall)value;
        }
      }
    }
    return jsNewRule;
  }

  @Test
  public void readParseInjectAndComposeJS1() throws Exception {
    String filenameOrig = "./src/test/resources/testSnippet.js";
    String filenameInject = "./src/test/resources/testSnippetInject.js";
    String filenameExpect = "./src/test/resources/testSnippetExpect.js";
    List<String> fieldNames = new ArrayList<String>();
    fieldNames.add("{{Regulator}}Auth.AuthFacilitator");

    FileReader fileReader = new FileReader(filenameOrig);
    JSStructureParser parser = new JSStructureParser(fileReader);
    parser.setResolveIdentifiers(false);
    Object objOrig = parser.parse();

    fileReader = new FileReader(filenameInject);
    JSFunctionCall jsNewRule = getJSFunctionCall(fileReader);

    JSObject jsRuleset = JSUtil.findObjectWith(objOrig, "validations");
    Object objValidations = jsRuleset.get("validations");
    if (objValidations instanceof JSArray) {
      JSArray validations = (JSArray) objValidations;

      JSArray rules = getRulesForField(validations, fieldNames);

      updateRuleInRules(rules, jsNewRule);
    }
    String composed = composeViaSnippets(objOrig);

    byte[] encoded = Files.readAllBytes(Paths.get(filenameExpect));
    String expecting = new String(encoded, StandardCharsets.UTF_8);

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  // --- Helper functions for readParseInjectAndComposeJS1() ---
  private String baseFunctionPrefixJS() {
    return  "define(function () {\n" +
            "  return function (predef) {\n" +
            "    var feature;\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Transaction Checksum Rules\",\n" +
            "        scope: \"transaction\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"TotalValue\",\n";
  }

  private String baseFunctionJS() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private String baseFunctionSuffixJS() {
    return  "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  private String editCodeFunctionJS1() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", \"I23\", \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private String editCodeFunctionJS2() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 345, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private String editMessageFunctionJS() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"Brand spanking new message\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private JSFunctionCall getRuleByName(Object obj, List<String> fieldNames, String name) {
    JSObject jsRuleset = JSUtil.findObjectWith(obj, "validations");
    Object objValidations = jsRuleset.get("validations");
    if (objValidations instanceof JSArray) {
      JSArray validations = (JSArray) objValidations;

      JSArray rules = getRulesForField(validations, fieldNames);

      for (int i=0; i<rules.size(); i++) {
        Object objCall = rules.get(i);
        if (objCall instanceof JSFunctionCall) {
          JSFunctionCall jsCall = (JSFunctionCall)objCall;
          String ruleName = (String)jsCall.getParameters().get(0);
          if (ruleName.equals(name)) {
            return jsCall;
          }
        }
      }
    }
    return null;
  }

  private JSFunctionCall getTestRule(Object obj) {
    List<String> fieldNames = new ArrayList<String>();
    fieldNames.add("TotalValue");
    String name = "totv1";
    return getRuleByName(obj, fieldNames, name);
  }

  private void editCode(JSFunctionCall jsCall, String code) {
    if (jsCall.getParameters().size() >= 2) {
      if (code == null || !code.equals(jsCall.getParameters().get(1))) {
        jsCall.getParameters().set(1, code);
      }
    }
  }

  @Test
  public void parseAndEditCodeRule1() throws Exception {
    String original = baseFunctionPrefixJS() + baseFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    editCode(jsCall, "I23");
    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + editCodeFunctionJS1() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  private void editCode(JSFunctionCall jsCall, Integer code) {
    if (jsCall.getParameters().size() >= 2) {
      if (code == null || !code.equals(jsCall.getParameters().get(1))) {
        jsCall.getParameters().set(1, code);
      }
    }
  }

  @Test
  public void parseAndEditCodeRule2() throws Exception {
    String original = baseFunctionPrefixJS() + baseFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    editCode(jsCall, 345);
    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + editCodeFunctionJS2() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  private void editMessage(JSFunctionCall jsCall, String message) {
    if (jsCall.getParameters().size() >= 3) {
      if (message == null || !message.equals(jsCall.getParameters().get(2))) {
        jsCall.getParameters().set(2, message);
      }
    }
  }

  @Test
  public void parseAndEditMessageRule() throws Exception {
    String original = baseFunctionPrefixJS() + baseFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    editMessage(jsCall, "Brand spanking new message");
    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + editMessageFunctionJS() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  private JSFunctionCall findApply(JSFunctionCall jsCall, String applyName) {
    JSIdentifier jsId = jsCall.getApply();
    if (jsId instanceof JSFunctionCall) {
      if (jsId.getName().equals(applyName)) {
        return (JSFunctionCall)jsId;
      }
      return findApply((JSFunctionCall)jsId, applyName);
    }

    return null;
  }

  private int findLastPosOfApply(JSFunctionCall jsCall, String... applyNames) {
    JSIdentifier jsId = jsCall.getApply();
    int foundPos = -1;
    int index = 0;
    while(jsId instanceof JSFunctionCall) {
      for (String applyName : applyNames) {
        if (jsId.getName().equals(applyName)) {
          foundPos = index;
        }
      }
      index++;
      jsId = jsId.getApply();
    }

    return foundPos;
  }

  private void removeApply(JSFunctionCall jsCall, String applyName) {
    JSIdentifier jsId = jsCall.getApply();
    if (jsId instanceof JSFunctionCall) {
      if (jsId.getName().equals(applyName)) {
        jsCall.setApply(jsId.getApply());
      }
      removeApply((JSFunctionCall)jsId, applyName);
    }
  }

  private void addApplyAt(JSFunctionCall jsCall, int pos, String applyName, Object parameter) {
    JSIdentifier jsId = jsCall.getApply();
    if (jsId == null || pos == 0) {
      JSFunctionCall newApply = null;
      if (parameter == null) {
        newApply = new JSFunctionCall(applyName);
      }
      else
      if (parameter instanceof JSArray) {
        JSArray params = new JSArray();
        params.add(parameter);
        newApply = new JSFunctionCall(applyName, params);
      }
      else
      if (parameter instanceof String) {
        JSArray params = new JSArray();
        params.add(parameter);
        newApply = new JSFunctionCall(applyName, params);
      }
      if (newApply != null) {
        jsCall.setApply(newApply);
        newApply.setApply(jsId);
      }
    }
    else {
      if (jsId instanceof JSFunctionCall) {
        pos--;
        addApplyAt((JSFunctionCall)jsId, pos, applyName, parameter);
      }
    }
  }

  private void editSection(JSFunctionCall jsCall, List<String> sections) {
    String applyName = "onSection";
    if (sections.size() > 0) {
      //Cannot use lamdas for JDK 1.6
      //String sections_str = sections.stream().reduce("", (acc, s) -> acc += s);
      String sections_str = "";
      for (String s : sections) {
        sections_str += s;
      }
      JSFunctionCall jsOnSection = findApply(jsCall, "onSection");
      if (jsOnSection != null) {
        if(!sections_str.equals(jsOnSection.getParameters().get(0))) {
          jsOnSection.getParameters().set(0, sections_str);
        }
      } else {
        addApplyAt(jsCall, -1, "onSection", sections_str);
      }
    }
    else {
      removeApply(jsCall, "onSection");
    }
  }

  private String editSectionFunctionJS1() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"A\")\n" +
            "            ]\n";
  }

  @Test
  public void parseAndEditSectionRule1() throws Exception {
    String original = baseFunctionPrefixJS() + baseFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    List<String> sections = new ArrayList<String>();
    sections.add("A");
    editSection(jsCall, sections);
    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + editSectionFunctionJS1() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  private String editSectionFunctionJS2() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"])))\n" +
            "            ]\n";
  }

  @Test
  public void parseAndEditSectionRule2() throws Exception {
    String original = baseFunctionPrefixJS() + baseFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    List<String> sections = new ArrayList<String>();
    editSection(jsCall, sections);
    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + editSectionFunctionJS2() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  private String addInflowFunctionJS() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onInflow().onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private void editFlow(JSFunctionCall jsCall, FlowType flowType) {
    if (flowType == FlowType.Inflow) {
      JSFunctionCall jsOnSection = findApply(jsCall, "onInflow");
      if (jsOnSection == null) {
        addApplyAt(jsCall, 0, "onInflow", null);
        removeApply(jsCall, "onOutflow");
      }
    }
    else
    if (flowType == FlowType.Outflow) {
      JSFunctionCall jsOnSection = findApply(jsCall, "onOutflow");
      if (jsOnSection == null) {
        addApplyAt(jsCall, 0, "onOutflow", null);
        removeApply(jsCall, "onInflow");
      }
    }
    else {
      removeApply(jsCall, "onInflow");
      removeApply(jsCall, "onOutflow");
    }
  }

  @Test
  public void parseAndEditInflowRule() throws Exception {
    String original = baseFunctionPrefixJS() + baseFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    editFlow(jsCall, FlowType.Inflow);
    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + addInflowFunctionJS() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  private String addCategoryFunctionJS1() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onCategory([\"401\", \"510\"]).onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private String addCategoryFunctionJS2() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onCategory(\"401\").onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private String baseCategoryFunctionJS() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onCategory(['401', '510']).onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private String editCategoryFunctionJS3() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onCategory([\"401\", \"509\", \"510\"]).onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private void editCategory(JSFunctionCall jsCall, List<String> categories) {
    if (categories.size() > 0) {
      JSFunctionCall jsOn = findApply(jsCall, "onCategory");
      Object parameter;
      if (categories.size() > 1) {
        JSArray jsArray = new JSArray();
        jsArray.addAll(categories);
        parameter = jsArray;
      }
      else {
        parameter = categories.get(0);
      }
      if (jsOn != null) {
        if (!parameter.equals(jsOn.getParameters().get(0))) {
          jsOn.getParameters().set(0, parameter);
        }
      } else {
        int pos = findLastPosOfApply(jsCall, "onInflow", "onOutflow") + 1;
        addApplyAt(jsCall, pos, "onCategory", parameter);
      }
    }
    else {
      removeApply(jsCall, "onCategory");
    }
  }

  @Test
  public void parseAndEditCategory1() throws Exception {
    String original = baseFunctionPrefixJS() + baseFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    List<String> categories = new ArrayList<String>();
    categories.add("401");
    categories.add("510");
    editCategory(jsCall, categories);
    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + addCategoryFunctionJS1() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  @Test
  public void parseAndEditCategory2() throws Exception {
    String original = baseFunctionPrefixJS() + baseFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    List<String> categories = new ArrayList<String>();
    categories.add("401");
    editCategory(jsCall, categories);
    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + addCategoryFunctionJS2() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  @Test
  public void parseAndEditCategory3() throws Exception {
    String original = baseFunctionPrefixJS() + baseCategoryFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    List<String> categories = new ArrayList<String>();
    categories.add("401");
    categories.add("509");
    categories.add("510");
    editCategory(jsCall, categories);
    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + editCategoryFunctionJS3() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  @Test
  public void parseAndEditCategory4() throws Exception {
    // Expecting no change to JS at all!!! The data is the same so no edit. See the single quotes are untouched
    String original = baseFunctionPrefixJS() + baseCategoryFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    List<String> categories = new ArrayList<String>();
    categories.add("401");
    categories.add("510");
    editCategory(jsCall, categories);
    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + baseCategoryFunctionJS() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  private String addNotCategoryFunctionJS() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onCategory([\"101\", \"103\", \"105\", \"106\"]).notOnCategory([\"101/11\", \"103/11\"]).onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private void editNotCategory(JSFunctionCall jsCall, List<String> notCategories) {
    if (notCategories.size() > 0) {
      JSFunctionCall jsOn = findApply(jsCall, "notOnCategory");
      Object parameter;
      if (notCategories.size() > 1) {
        JSArray jsArray = new JSArray();
        jsArray.addAll(notCategories);
        parameter = jsArray;
      }
      else {
        parameter = notCategories.get(0);
      }
      if (jsOn != null) {
        if (!parameter.equals(jsOn.getParameters().get(0))) {
          jsOn.getParameters().set(0, parameter);
        }
      } else {
        int pos = findLastPosOfApply(jsCall, "onInflow", "onOutflow", "onCategory") + 1;
        addApplyAt(jsCall, pos, "notOnCategory", parameter);
      }
    }
    else {
      removeApply(jsCall, "notOnCategory");
    }
  }

  @Test
  public void parseAndEditNotCategory() throws Exception {
    // Expecting the order of onCategory and notOnCategory to be first onCategory and followed by notOnCategory
    String original = baseFunctionPrefixJS() + baseFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);
    List<String> categories = new ArrayList<String>();
    categories.add("101");
    categories.add("103");
    categories.add("105");
    categories.add("106");
    List<String> notCategories = new ArrayList<String>();
    notCategories.add("101/11");
    notCategories.add("103/11");

    // Do the edits
    editCategory(jsCall, categories);
    editNotCategory(jsCall, notCategories);

    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + addNotCategoryFunctionJS() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  private String editAssertionJS() {
    return  "evalResidentField(\"AccountNumber\", hasLookupValue('mtaAccounts', 'accountNumber', 'isADLA', true))\n" +
            "                  .and(evalResidentField(\"AccountNumber\", isInLookup('mtaAccounts', 'accountNumber')))";
  }

  private String editAssertionFunctionJS() {
    return  "            rules : [\n" +
            "              failure(\"totv1\", 244, \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  evalResidentField(\"AccountNumber\", hasLookupValue('mtaAccounts', 'accountNumber', 'isADLA', true))\n" +
            "                  .and(evalResidentField(\"AccountNumber\", isInLookup('mtaAccounts', 'accountNumber')))).onSection(\"ABCDEG\")\n" +
            "            ]\n";
  }

  private JSIdentifier getJSIdentifier(String assertionDSL) throws Exception {
    JSStructureParser parser = new JSStructureParser(assertionDSL);
    Object objNewRule;
    objNewRule = parser.parse();
    JSIdentifier jsAssertion = null;
    if (objNewRule instanceof JSIdentifier) {
      jsAssertion = (JSIdentifier)objNewRule;
    }
    if (objNewRule instanceof JSScope) {
      JSScope scope = ((JSScope)objNewRule);
      if (scope.size() == 1) {
        for (Object value : scope.values()) {
          if (value instanceof JSIdentifier)
            jsAssertion = (JSIdentifier)value;
        }
      }
    }
    return jsAssertion;
  }

  private void editAssertion(JSFunctionCall jsCall, JSIdentifier jsAssertion) {
    if (jsCall.getParameters().size() >= 4) {
      jsCall.getParameters().set(3, jsAssertion);
    }
  }

  @Test
  public void parseAndEditAssertionRule() throws Exception {
    String original = baseFunctionPrefixJS() + baseFunctionJS() + baseFunctionSuffixJS();
    JSStructureParser parser = new JSStructureParser(original);
    Object objOrig = parser.parse();

    JSFunctionCall jsCall = getTestRule(objOrig);

    JSIdentifier jsAssertion = getJSIdentifier(editAssertionJS());
    editAssertion(jsCall, jsAssertion);

    String composed = composeViaSnippets(objOrig);

    String expecting = baseFunctionPrefixJS() + editAssertionFunctionJS() + baseFunctionSuffixJS();

    Assert.assertTrue(areStringEquals(expecting, composed));
  }

  private String baseForExtractWhitespace() {
    return  "define(function () {\n" +
            "  return function (predef) {\n" +
            "    var feature;\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Transaction Checksum Rules\",\n" +
            "        scope: \"transaction\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"TotalValue\",\n" +
            "            rules : [\n" +
            "              failure(\"totv1\", 244,\n" +
            "                  \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")\n" +
            "            ]\n" +
            "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  private String failureWithoutWhitespace() {
    return  "failure(\"totv1\", 244,\n" +
            "    \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "    isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")";
  }

  private String failureWithoutWhitespaceTrailingNewline() {
    return  "failure(\"totv1\", 244,\n" +
            "    \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "    isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")\n";
  }

  @Test
  public void extractFailureSnippet() throws Exception {
    String original = baseForExtractWhitespace();
    JSStructureParser parser = new JSStructureParser(original);

    Object obj = parser.parse();

    JSFunctionCall jsFunc = getTestRule(obj);
    jsFunc.removeExtraWhitespace();

    String composed = composeViaSnippets(jsFunc);
    String expected = failureWithoutWhitespace();
    Assert.assertTrue("Expecting nicely unindented", areStringEquals(expected, composed));
  }

  private String baseForInsertWhitespace() {
    return  "define(function () {\n" +
            "  return function (predef) {\n" +
            "    var feature;\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Transaction Checksum Rules\",\n" +
            "        scope: \"transaction\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"TotalValue\",\n" +
            "            rules : [\n" +
            "            ]\n" +
            "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  private String baseForInsertWhitespaceEmptyArray() {
    return  "define(function () {\n" +
            "  return function (predef) {\n" +
            "    var feature;\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Transaction Checksum Rules\",\n" +
            "        scope: \"transaction\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"TotalValue\",\n" +
            "            rules : []\n" +
            "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  private JSArray getTestRules(Object obj) {
    List<String> fieldNames = new ArrayList<String>();
    fieldNames.add("TotalValue");
    String name = "totv1";
    JSObject jsRuleset = JSUtil.findObjectWith(obj, "validations");
    Object objValidations = jsRuleset.get("validations");
    if (objValidations instanceof JSArray) {
      JSArray validations = (JSArray) objValidations;
      JSArray rules = getRulesForField(validations, fieldNames);
      return rules;
    }
    return null;
  }

  private JSFunctionCall getJSFunctionCall(String ruleDSL) throws Exception {
    JSStructureParser parser = new JSStructureParser(ruleDSL);
    Object objNewRule;
    objNewRule = parser.parse();
    JSFunctionCall jsRule = null;
    if (objNewRule instanceof JSFunctionCall) {
      jsRule = (JSFunctionCall)objNewRule;
    }
    if (objNewRule instanceof JSScope) {
      JSScope scope = ((JSScope)objNewRule);
      if (scope.size() == 1) {
        for (Object value : scope.values()) {
          if (value instanceof JSFunctionCall)
            jsRule = (JSFunctionCall)value;
        }
      }
    }
    return jsRule;
  }

  @Test
  public void insertFailureSnippet1() throws Exception {
    String base = baseForInsertWhitespace();
    JSStructureParser parser = new JSStructureParser(base);

    Object obj = parser.parse();

    JSFunctionCall jsFunc = getJSFunctionCall(failureWithoutWhitespace());

    JSArray rules = getTestRules(obj);
    jsFunc.injectWhitespace(rules);
    rules.add(jsFunc);

    String composed = composeViaSnippets(obj);
    String expected = baseForExtractWhitespace();
    Assert.assertTrue("Expecting nicely indented", areStringEquals(expected, composed));
  }

  @Test
  public void insertFailureSnippet2() throws Exception {
    String base = baseForInsertWhitespace();
    JSStructureParser parser = new JSStructureParser(base);

    Object obj = parser.parse();

    JSFunctionCall jsFunc = getJSFunctionCall(failureWithoutWhitespaceTrailingNewline());

    JSArray rules = getTestRules(obj);
    jsFunc.injectWhitespace(rules);
    rules.add(jsFunc);

    String composed = composeViaSnippets(obj);
    String expected = baseForExtractWhitespace();
    Assert.assertTrue("Expecting nicely indented", areStringEquals(expected, composed));
  }

  @Test
  public void insertFailureSnippetIntoEmptyArray() throws Exception {
    String base = baseForInsertWhitespaceEmptyArray();
    JSStructureParser parser = new JSStructureParser(base);

    Object obj = parser.parse();

    JSFunctionCall jsFunc = getJSFunctionCall(failureWithoutWhitespace());

    JSArray rules = getTestRules(obj);
    jsFunc.injectWhitespace(rules);
    rules.add(jsFunc);

    String composed = composeViaSnippets(obj);
    String expected = baseForExtractWhitespace();
    Assert.assertTrue("Expecting nicely indented", areStringEquals(expected, composed));
  }

  private String baseForObjectExtractWhitespace() {
    return  "define(function () {\n" +
            "  return function (evaluationEx) {\n" +
            "    var evaluations;\n" +
            "    with (evaluationEx) {\n" +
            "      evaluations = [\n" +
            "        {\n" +
            "          drResStatus: resStatus.NONRES,\n" +
            "          drAccType: [accType.NOSTRO],\n" +
            "          crResStatus: resStatus.RESIDENT,\n" +
            "          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA, accType.NOSTRO],\n" +
            "          validFrom: \"2013-07-18\",\n" +
            "          decision: {\n" +
            "            manualSection: \"B and T Section B.1 (A) - page 1\",\n" +
            "            reportable: rep.REPORTABLE,\n" +
            "            flow: flowDir.IN,\n" +
            "            reportingSide: drcr.CR,\n" +
            "            nonResSide: drcr.DR,\n" +
            "            nonResAccountType: [at.NR_OTH],\n" +
            "            resSide: drcr.CR,\n" +
            "            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA, at.RE_CFC]\n" +
            "          }\n" +
            "        }\n" +
            "      ];\n" +
            "    }\n" +
            "    return evaluations;\n" +
            "  }\n" +
            "});";
  }

  private String objectWithoutWhitespace() {
    return  "{\n" +
            "  manualSection: \"B and T Section B.1 (A) - page 1\",\n" +
            "  reportable: rep.REPORTABLE,\n" +
            "  flow: flowDir.IN,\n" +
            "  reportingSide: drcr.CR,\n" +
            "  nonResSide: drcr.DR,\n" +
            "  nonResAccountType: [at.NR_OTH],\n" +
            "  resSide: drcr.CR,\n" +
            "  resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA, at.RE_CFC]\n" +
            "}";
  }

  private JSObject getObjectByName(Object objDoc, String name) throws Exception {
    JSObject jsEval = JSUtil.findObjectWith(objDoc, name);
    if (jsEval != null) {
      Object namedObj = jsEval.get(name);
      if (namedObj instanceof JSObject)
        return (JSObject)namedObj;
    }
    return null;
  }

  @Test
  public void extractObjectSnippet() throws Exception {
    String original = baseForObjectExtractWhitespace();
    JSStructureParser parser = new JSStructureParser(original);

    Object objDoc = parser.parse();

    JSObject jsDecision = getObjectByName(objDoc, "decision");
    if (jsDecision != null) {
      jsDecision.removeExtraWhitespace();
    }

    String composed = composeViaSnippets(jsDecision);
    String expected = objectWithoutWhitespace();
    Assert.assertTrue("Expecting nicely unindented", areStringEquals(expected, composed));
  }

  private String baseForInsertObject() {
    return  "define(function () {\n" +
            "  return function (evaluationEx) {\n" +
            "    var evaluations;\n" +
            "    with (evaluationEx) {\n" +
            "      evaluations = [\n" +
            "        {\n" +
            "          drResStatus: resStatus.NONRES,\n" +
            "          drAccType: [accType.NOSTRO],\n" +
            "          crResStatus: resStatus.RESIDENT,\n" +
            "          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA, accType.NOSTRO],\n" +
            "          validFrom: \"2013-07-18\",\n" +
            "          decision: {}\n" +
            "        }\n" +
            "      ];\n" +
            "    }\n" +
            "    return evaluations;\n" +
            "  }\n" +
            "});";
  }

  @Test
  public void insertObjectSnippet() throws Exception {
    String base = baseForInsertObject();
    JSStructureParser parser = new JSStructureParser(base);

    Object objDoc = parser.parse();

    JSObject jsEvaluation = JSUtil.findObjectWith(objDoc, "decision");

    String objectToInsert = objectWithoutWhitespace();
    parser = new JSStructureParser(objectToInsert);
    Object obj = parser.parse();

    if (obj instanceof JSObject) {
      JSObject jsObj = (JSObject)obj;
      String composedObj = composeViaSnippets(jsObj);
      Assert.assertTrue("Expecting identical", areStringEquals(objectToInsert, composedObj));
      jsObj.injectWhitespace(jsEvaluation);
      jsEvaluation.put("decision", jsObj);
    }

    String composed = composeViaSnippets(objDoc);
    String expected = baseForObjectExtractWhitespace();
    Assert.assertTrue("Expecting nicely indented", areStringEquals(expected, composed));
  }


  private String baseForAddNewRule() {
    return  "define(function () {\n" +
            "  return function (predef) {\n" +
            "    var feature;\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Transaction Checksum Rules\",\n" +
            "        scope: \"transaction\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"TotalValue\",\n" +
            "            rules : [\n" +
            "              failure(\"totv1\", 244,\n" +
            "                  \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\")\n" +
            "            ]\n" +
            "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  private String newRuleToAdd1() {
    return  "failure(\"totv2\", 245,\n" +
            "    \"Description of new rule\",\n" +
            "    isEmpty).onInflow()";
  }

  private String newRuleToAdd2() {
    return  "failure('totv3', 246, 'Description of new rule on one line with single quoted strings', notEmpty).onOutflow()";
  }

  private String expectingUpdatedRule() {
    return  "define(function () {\n" +
            "  return function (predef) {\n" +
            "    var feature;\n" +
            "    with (predef) {\n" +
            "\n" +
            "      feature = {\n" +
            "        ruleset: \"Transaction Checksum Rules\",\n" +
            "        scope: \"transaction\",\n" +
            "        validations: [\n" +
            "          {\n" +
            "            field : \"TotalValue\",\n" +
            "            rules : [\n" +
            "              failure(\"totv1\", 244,\n" +
            "                  \"RandValue + ForeignValue must equal TotalValue\",\n" +
            "                  isDefined.and(notSumTotalValue).and(notValueIn([\"NAMIBIA\", \"LESOTHO\", \"SWAZILAND\"]))).onSection(\"ABCDEG\"), \n" +
            "              failure(\"totv2\", 245,\n" +
            "                  \"Description of new rule\",\n" +
            "                  isEmpty).onInflow(), \n" +
            "              failure('totv3', 246, 'Description of new rule on one line with single quoted strings', notEmpty).onOutflow()\n" +
            "            ]\n" +
            "          }\n" +
            "        ]\n" +
            "      };\n" +
            "    }\n" +
            "    return feature;\n" +
            "  }\n" +
            "});\n";
  }

  @Test
  public void insertNewRule() throws Exception {
    String base = baseForAddNewRule();
    JSStructureParser parser = new JSStructureParser(base);

    Object obj = parser.parse();

    JSFunctionCall jsFunc1 = getJSFunctionCall(newRuleToAdd1());

    JSArray rules = getTestRules(obj);
    jsFunc1.injectWhitespace(rules);
    rules.add(jsFunc1);

    JSFunctionCall jsFunc2 = getJSFunctionCall(newRuleToAdd2());

    jsFunc2.injectWhitespace(rules);
    rules.add(jsFunc2);

    String composed = composeViaSnippets(obj);
    String expected = expectingUpdatedRule();
    Assert.assertTrue("Expecting nicely indented", areStringEquals(expected, composed));
  }
}
