package za.co.synthesis.rule.difference.document;

import za.co.synthesis.rule.difference.DiffUtils;
import za.co.synthesis.rule.difference.FieldRule;
import za.co.synthesis.rule.difference.IDifference;
import za.co.synthesis.rule.difference.RuleMap;
import za.co.synthesis.rule.difference.validation.RuleDifference;
import za.co.synthesis.rule.support.RuleSet;
import za.co.synthesis.rule.support.RuleType;

import java.util.ArrayList;
import java.util.List;

public class DocumentDifferences {
  public static List<IDifference> getRuleDifferences(String oldPackagePath, String newPackagePath) throws Exception {
    List<IDifference> result = new ArrayList<IDifference>();
    List<RuleSet> oldRuleSets = DiffUtils.loadDocumentRulesets(oldPackagePath);
    List<RuleSet> newRuleSets = DiffUtils.loadDocumentRulesets(newPackagePath);

    // Changes to Rules
    RuleMap oldRules = DiffUtils.getRuleMapFromRuleSets(oldRuleSets, RuleType.Document);
    RuleMap newRules = DiffUtils.getRuleMapFromRuleSets(newRuleSets, RuleType.Document);

    for (String name : newRules.ruleList()) {
      FieldRule oldFieldRule = oldRules.get(name);
      FieldRule newFieldRule = newRules.get(name);
      if (oldFieldRule == null) {
        result.add(RuleDifference.createAddedRule(newFieldRule.getScope(), newFieldRule.getFields(), newFieldRule.getRule()));
      }
      else {
        if (!oldFieldRule.equalsRule(newFieldRule.getRule())) {
          result.add(RuleDifference.createChangedRule(
                  oldFieldRule.getScope(), oldFieldRule.getFields(), oldFieldRule.getRule(),
                  newFieldRule.getScope(), newFieldRule.getFields(), newFieldRule.getRule()));
        }
      }
      oldRules.remove(name);
    }
    for (String name : oldRules.ruleList()) {
      FieldRule fieldRule = oldRules.get(name);
      result.add(RuleDifference.createRemovedRule(fieldRule.getScope(), fieldRule.getFields(), fieldRule.getRule()));
    }
    return result;
  }
}
