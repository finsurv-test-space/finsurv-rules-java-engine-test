package za.co.synthesis.rule.support.legacydate;

import za.co.synthesis.rule.support.legacydate.format.DateTimeFormatter;
import za.co.synthesis.rule.support.legacydate.format.DateTimeParseException;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class LocalDate extends Temporal {
  public LocalDate(Date date) {
    this.calendar = new GregorianCalendar();
    this.calendar.setTime(date);
  }

  public LocalDate(Calendar calendar) {
    this.calendar = calendar;
  }

  public LocalDate() {
    this.calendar = GregorianCalendar.getInstance();
  }

  public static LocalDate now() {
    return new LocalDate();
  }

  /**
   * Creates a date based on the year, month and day
   * @param year The 4-digit representation of the year (YYYY)
   * @param month The first month is 1 (Jan) and goes up the 12 (Dec)
   * @param day A value between 1 and 31
   * @return an instance of the LocalDate class containing the provided YYYY, MM & DD values.
   */
  public static LocalDate of(int year, int month, int day) {
    GregorianCalendar cal = new GregorianCalendar(year, month-1, day);
    return new LocalDate(cal);
  }

  public synchronized static LocalDate parse(String value, DateTimeFormatter format) throws DateTimeParseException {
    try {
      return format.parseDate(value);
    } catch (ParseException e) {
      throw new DateTimeParseException(e.getMessage(), e);
    }
  }

  public synchronized static String format(LocalDate value, DateTimeFormatter format) {
    return format.format(value);
  }

  public static long daysBetween(LocalDate date1, LocalDate date2) {
    Calendar dayOne = (Calendar) date1.calendar.clone(),
            dayTwo = (Calendar) date2.calendar.clone();

    if (dayOne.get(Calendar.YEAR) == dayTwo.get(Calendar.YEAR)) {
      return Math.abs(dayOne.get(Calendar.DAY_OF_YEAR) - dayTwo.get(Calendar.DAY_OF_YEAR));
    } else {
      if (dayTwo.get(Calendar.YEAR) > dayOne.get(Calendar.YEAR)) {
        //swap them
        Calendar temp = dayOne;
        dayOne = dayTwo;
        dayTwo = temp;
      }
      int extraDays = 0;

      int dayOneOriginalYearDays = dayOne.get(Calendar.DAY_OF_YEAR);

      while (dayOne.get(Calendar.YEAR) > dayTwo.get(Calendar.YEAR)) {
        dayOne.add(Calendar.YEAR, -1);
        // getActualMaximum() important for leap years
        extraDays += dayOne.getActualMaximum(Calendar.DAY_OF_YEAR);
      }

      return extraDays - dayTwo.get(Calendar.DAY_OF_YEAR) + dayOneOriginalYearDays;
    }
  }

  public LocalDate plusDays(int days) {
    Calendar newCal = (Calendar)calendar.clone();
    newCal.add(Calendar.DAY_OF_YEAR, days);
    return new LocalDate(newCal);
  }

  public LocalDate minusDays(int days) {
    Calendar newCal = (Calendar)calendar.clone();
    newCal.add(Calendar.DAY_OF_YEAR, -days);
    return new LocalDate(newCal);
  }

  public boolean isEqual(LocalDate otherDate) {
    if (calendar.get(Calendar.YEAR) == otherDate.calendar.get(Calendar.YEAR)) {
      return calendar.get(Calendar.DAY_OF_YEAR) == otherDate.calendar.get(Calendar.DAY_OF_YEAR);
    }
    return false;
  }

  public boolean isAfter(LocalDate otherDate) {
    if (calendar.get(Calendar.YEAR) == otherDate.calendar.get(Calendar.YEAR)) {
      return calendar.get(Calendar.DAY_OF_YEAR) > otherDate.calendar.get(Calendar.DAY_OF_YEAR);
    }
    return calendar.get(Calendar.YEAR) > otherDate.calendar.get(Calendar.YEAR);
  }

  public boolean isBefore(LocalDate otherDate) {
    if (calendar.get(Calendar.YEAR) == otherDate.calendar.get(Calendar.YEAR)) {
      return calendar.get(Calendar.DAY_OF_YEAR) < otherDate.calendar.get(Calendar.DAY_OF_YEAR);
    }
    return calendar.get(Calendar.YEAR) < otherDate.calendar.get(Calendar.YEAR);
  }
}
