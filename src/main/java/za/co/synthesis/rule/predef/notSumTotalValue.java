package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 1/28/16
 * Time: 12:53 PM
 * Test to check the sum of the Foreign and Rand values against the total value
 */
public class notSumTotalValue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    BigDecimal total = new BigDecimal(0);
    BigDecimal sumValue = Util.number(context.getTransactionField("TotalValue"));
    for (int i = 0; i < context.getMoneySize(); i++) {
      BigDecimal fvalue = Util.number(context.getMoneyField(i, "ForeignValue"));
      if (fvalue != null)
        total = total.add(fvalue);
      BigDecimal rvalue = Util.number(context.getMoneyField(i, context.map("LocalValue")));
      if (rvalue != null)
        total = total.add(rvalue);
    }
    if (sumValue != null)
      return sumValue.compareTo(total) != 0;
    return true;
  }
}
/*
  _export.notSumTotalValue = function notSumTotalValue(context, value) {
    var total = 0;
    var sumValue = context.getTransactionField("TotalValue");
    for (var i = 0; i < context.getMoneySize(); i++) {
      var fvalue = context.getMoneyField(i, "ForeignValue");
      if (fvalue)
        total += Number(fvalue);
      var rvalue = context.getMoneyField(i, "RandValue");
      if (rvalue)
        total += Number(rvalue);
    }
    var a = Math.round(Number(sumValue) * 100);
    var b = Math.round(total * 100);
    return a != b;
  }
*/
