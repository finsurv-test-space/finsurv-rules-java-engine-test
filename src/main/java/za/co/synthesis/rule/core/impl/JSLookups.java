package za.co.synthesis.rule.core.impl;

import za.co.synthesis.rule.core.ILookups;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.ValidationException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 7:07 AM
 * Implements the ILookups methods against the data provided by the lookups.js file
 */
public class JSLookups implements ILookups {
  private JSObject jsLookup = null;
  private Map<String, JSObject> countries = null;
  private Map<String, JSObject> currencies = null;
  private List<JSObject> categories = null;
  private Map<String, JSObject> institutionalSectors = null;
  private Map<String, JSObject> industrialClassifications = null;
  private Map<String, JSObject> customsOffices = null;
  private JSArray reportingQualifiers = null;
  private JSArray nonResExceptionNames = null;
  private JSArray resExceptionNames = null;
  private JSArray moneyTransferAgents = null;
  private JSArray provinces = null;
  private JSArray cardTypes = null;

  public JSLookups() {
  }

  public JSLookups(JSObject lookup) {
    load(lookup);
  }

  private Map<String, JSObject> loadCodedMap(JSObject lookup, String name) {
    Map<String, JSObject> result = null;
    if (lookup.containsKey(name)) {
      result = new HashMap<String, JSObject>();
      for (Object obj : (JSArray)lookup.get(name)) {
        if (obj instanceof JSObject) {
          JSObject jsObj = (JSObject)obj;
          result.put(jsObj.get("code").toString(), jsObj);
        }
      }
    }
    return result;
  }

  public void load(JSObject lookup) {
    this.jsLookup = lookup;
    if (lookup.containsKey("ReportingQualifier")) {
      reportingQualifiers = (JSArray)lookup.get("ReportingQualifier");
    }
    if (lookup.containsKey("nonResidentExceptions")) {
      nonResExceptionNames = (JSArray)lookup.get("nonResidentExceptions");
    }
    if (lookup.containsKey("residentExceptions")) {
      resExceptionNames = (JSArray)lookup.get("residentExceptions");
    }
    if (lookup.containsKey("moneyTransferAgents")) {
      moneyTransferAgents = (JSArray)lookup.get("moneyTransferAgents");
    }
    if (lookup.containsKey("provinces")) {
      provinces = (JSArray)lookup.get("provinces");
    }
    if (lookup.containsKey("cardTypes")) {
      cardTypes = (JSArray)lookup.get("cardTypes");
    }

    currencies = loadCodedMap(lookup, "currencies");
    countries = loadCodedMap(lookup, "countries");
    institutionalSectors = loadCodedMap(lookup, "institutionalSectors");
    industrialClassifications = loadCodedMap(lookup, "industrialClassifications");
    customsOffices = loadCodedMap(lookup, "customsOffices");

    if (lookup.containsKey("categories")) {
      categories = new ArrayList<JSObject>();
      for (Object obj : (JSArray)lookup.get("categories")) {
        if (obj instanceof JSObject) {
          JSObject jsObj = (JSObject)obj;
          categories.add(jsObj);
        }
      }
    }
  }

  @Override
  public Boolean isValidCountryCode(String code) {
    if (countries == null)
      return null;
    return countries.containsKey(code);
  }

  @Override
  public Boolean isValidCurrencyCode(String code) {
    if (currencies == null)
      return null;
    return currencies.containsKey(code);
  }

  @Override
  public Boolean isValidCategory(String flow, String code) {
    if (categories == null)
      return null;
    for (JSObject jsCat : categories) {
      if (jsCat.get("flow").equals(flow) && jsCat.get("code").toString().indexOf(code) == 0) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Boolean isValidSubCategory(String flow, String code, String subcode) {
    if (categories == null)
      return null;
    List<String> cats = new ArrayList<String>();
    for (JSObject jsCat : categories) {
      if (jsCat.get("flow").equals(flow) && jsCat.get("code").toString().indexOf(code) == 0) {
        cats.add(jsCat.get("code").toString());
      }
    }
    boolean hasValidSubCode = false;
    boolean canHaveNoSubCode = false;

    for (String jsCode : cats) {
      int pos = jsCode.indexOf("/");
      String jsSubCode = null;
      if (pos > -1) {
        jsSubCode = jsCode.substring(pos + 1);
      }
      if (jsSubCode != null) {
        if (jsSubCode.equals(subcode)) {
          hasValidSubCode = true;
          break;
        }
      }
      else {
        canHaveNoSubCode = true;
      }
    }

    if (!hasValidSubCode && (subcode == null || subcode.length() == 0) && canHaveNoSubCode)
      hasValidSubCode = true;
    return hasValidSubCode;
  }

  @Override
  public Boolean isReportingQualifier(String value) {
    if (reportingQualifiers == null)
      return null;
    return reportingQualifiers.contains(value);
  }

  @Override
  public Boolean isNonResExceptionName(String value) {
    if (nonResExceptionNames == null)
      return null;
    return nonResExceptionNames.contains(value);
  }

  @Override
  public Boolean isResExceptionName(String value) {
    if (resExceptionNames == null)
      return null;
    return resExceptionNames.contains(value);
  }

  @Override
  public Boolean isMoneyTransferAgent(String value) {
    if (moneyTransferAgents == null)
      return null;
    return moneyTransferAgents.contains(value);
  }

  @Override
  public Boolean isValidProvince(String value) {
    if (provinces == null)
      return null;
    return provinces.contains(value);
  }

  @Override
  public Boolean isValidCardType(String value) {
    if (cardTypes == null)
      return null;
    return cardTypes.contains(value);
  }

  @Override
  public Boolean isValidInstitutionalSector(String value) {
    if (institutionalSectors == null)
      return null;
    return institutionalSectors.containsKey(value);
  }

  @Override
  public Boolean isValidIndustrialClassification(String value) {
    if (industrialClassifications == null)
      return null;
    return industrialClassifications.containsKey(value);
  }

  @Override
  public Boolean isValidCustomsOfficeCode(String value) {
    if (customsOffices == null)
      return null;
    return customsOffices.containsKey(value);
  }

  @Override
  public String getLookupField(final String lookup, final Map<String,String> key, final String fieldName) {
    try {
      if (jsLookup.containsKey(lookup)) {
        JSArray entries = (JSArray) jsLookup.get(lookup);
        for (Object entry : entries) {
          if (entry instanceof JSObject) {
            JSObject jsEntry = (JSObject) entry;

            boolean bMatch = true;
            for (Map.Entry<String, String> keyEntry : key.entrySet()) {
              if (jsEntry.containsKey(keyEntry.getKey())) {
                if (!jsEntry.get(keyEntry.getKey()).equals(keyEntry.getValue())) {
                  bMatch = false;
                  break;
                }
              } else {
                bMatch = false;
                break;
              }
            }
            if (bMatch) {
              if (jsEntry.containsKey(fieldName)) {
                return jsEntry.get(fieldName).toString();
              }
            }
          }
        }
      }
    } catch (Exception e) {
      throw new ValidationRuntimeException("Error validating field=" + fieldName + ", lookup=" + lookup, e );
    }
    return null;
  }
}
