package za.co.synthesis;

import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import za.co.synthesis.csv.CSVReaderEx;
import za.co.synthesis.csv.CsvOutput;
import za.co.synthesis.validate.ErrorCount;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 1/27/16
 * Time: 6:26 AM
 * Analysing the result of the CSV Output file
 */
public class CsvOutputAnalysis {
  static String[] currentRow = null;

  private static class TranDecision {
    List<CsvOutput.ValidationEntry> oldEntries = new ArrayList<CsvOutput.ValidationEntry>();
    List<CsvOutput.ValidationEntry> newEntries = new ArrayList<CsvOutput.ValidationEntry>();
  }

  private static void setupWeHaveErrorCodes(Map<String, String> weHaveErrorCode) {
//    weHaveErrorCode.put("212", "TRANSACTION REFERENCE/SEQUENCE OF CANCELLED TRANSACTION NOT STORED ON SARB DATABASE."); // Assume that if 212 external validate was operational then we would match SARB
  }

  private static void setupBypassOurErrorCodes(Map<String, String> bypassErrorCode) {
    bypassErrorCode.put("410", "ignore"); // Original transaction and SequenceNumber combination not stored on database; Attrib: ReversalTrnRefNumber
//    bypassErrorCode.put("527", "ignore"); // May not be completed (errors in NON REPORTABLE rules)
//    bypassErrorCode.put("528", "ignore"); // May not be completed (errors in NON REPORTABLE rules)
  }

  private static void setupAlternateErrorCodes(Map<String, String> alternateErrorCode) {
    alternateErrorCode.put("323", "368");
    alternateErrorCode.put("431", "302");
    alternateErrorCode.put("271", "533");  // 271 Invalid account identifier => 533 Type of data reported in all attributes must be correct
    alternateErrorCode.put("338", "533");  // 338 Invalid postal code format => 533 Type of data reported in all attributes must be correct
    alternateErrorCode.put("243", "533");  // 243 If Flow is IN, SWIFT country code must be ZA => 533 Type of data reported in all attributes must be correct
    alternateErrorCode.put("497", "499");  // 497 Flow is OUT and cat is 101/01 to 101/10 the first 3 chars must be INV => 499 Invalid ImportControl Number format
    alternateErrorCode.put("499", "498");  // 499 Invalid ImportControl Number format => 498 INVALID CUSTOMS OFFICE CODE COMPLETED IN THE IMPORTCONTROLNUMBER ATTRIBUTE
    alternateErrorCode.put("393", "400,401,402");  // 393 INVALID SUBJECT => 400 incorrect cat / subject combo, 401 INCORRECT FLOW COMPLETED WITH SUBJECT, 402 DESCRIPTION MUST BE COMPLETED
    alternateErrorCode.put("221", "500");  // 221 NOMRNONIVS MUST HAVE A VALUE Y => 500 MRN NOT STORED ON IVS
    alternateErrorCode.put("416", "419,421,424,426,428");  // 416 If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used and Resident Entity is completed, then must be completed => 419,421,424,426,428 INDIVIDUALTHIRDPARTYNAME MUST BE COMPLETED
    alternateErrorCode.put("543", "209,533");  // 543 ReplacementTransaction must be completed => 209 REPLACEMENTTRANSACTION MUST CONTAIN A VALUE Y OR N  ; 533 Type of data reported in all attributes must be correct
    alternateErrorCode.put("494", "493,514");  // 494 Must contain a sequential SubSequence entries that must start with the value => 493 SUBSEQUENCE MUST START WITH 1; 514 INVALID CCN COMPLETED IN UCR
    alternateErrorCode.put("351", "207");  // 494 Must contain a sequential SubSequence entries that must start with the value => 493 SUBSEQUENCE MUST START WITH 1
    alternateErrorCode.put("L02", "335");  // L02 length of field is too short => 335 MINIMUM AND MAXIMUM LENGTHS OF ALL ATTRIBUTES MUST BE CORRECT
    alternateErrorCode.put("490", "514,504,498,499");  // 490 ImportExport element must be completed => 514 INVALID CCN COMPLETED IN UCR; 504 UCR IN THE INCORRECT FORMAT (since the SARB expects a value to be present)
    alternateErrorCode.put("S09", "514");  // S09 Unless the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, this need not be provided and if invalid will cause the SARB to reject transaction => 514 INVALID CCN COMPLETED IN UCR (since the SARB checks all given values)
//    alternateErrorCode.put("349", "ignore"); //SEQUENCENUMBER NOT IN SEQUENCE
    alternateErrorCode.put("201", "ignore"); //INVALID REFERENCE FORMAT
    alternateErrorCode.put("305", "ignore"); //ORIGINAL TRANSACTION AND/OR SEQUENCE AND FLOW NOT STORED ON DATABASE; Attrib: TRNReference
    alternateErrorCode.put("529", "ignore"); //TOTAL PAYMENTVALUE OF ALL SUBSEQUENCES EXCEEDS A 1% VARIANCE WITH RANDVALUE OR FOREIGNVALUE
    alternateErrorCode.put("220", "ignore"); //TRN REFERENCE ALREADY STORED ON SARB
//    alternateErrorCode.put("212", "ignore:TRANSACTION REFERENCE/SEQUENCE OF CANCELLED TRANSACTION NOT STORED ON SARB DATABASE.");
//    alternateErrorCode.put("494", "ignore:SUBSEQUENCE UNDER IMPORTEXPORTDATA MUST BE SEQUENTIAL");
  }

  private static TranDecision readTranDecision(CSVReaderEx reader) throws IOException {
    TranDecision result = null;

    String currentReference;
    String reference;
    CsvOutput.ValidationType validationType;
    String code;
    String description;
    String attribute;
    int sequence;
    int subSequence;
    String comment = "";

    if (currentRow == null)
      currentRow = reader.readNext();

    if(currentRow != null) {
      result = new TranDecision();
      currentReference = currentRow[0];
      while(currentRow != null) {
        reference = currentRow[0];
        if (!reference.equals(currentReference))
          break;
        validationType = CsvOutput.ValidationEntry.getValidationType(currentRow[1]);
        if (currentRow.length > 3) {
          code = currentRow[2];
          description = currentRow[3];
          attribute = currentRow[4];
          String val = currentRow[5];
          sequence = (val != null && val.length() > 0) ? Integer.parseInt(val) : 0;
          val = currentRow[6];
          subSequence = (val != null && val.length() > 0) ? Integer.parseInt(val) : 0;
        }
        else {
          code = "";
          description = "";
          attribute = "";
          sequence = 0;
          subSequence = 0;
        }

        if (validationType == CsvOutput.ValidationType.NewSuccess ||
            validationType == CsvOutput.ValidationType.NewError ||
            validationType == CsvOutput.ValidationType.NewWarning ) {
          result.newEntries.add(new CsvOutput.ValidationEntry(reference, validationType, code, description, attribute, sequence, subSequence, comment));
        }
        else {
          result.oldEntries.add(new CsvOutput.ValidationEntry(reference, validationType, code, description, attribute, sequence, subSequence, comment));
        }
        currentRow = reader.readNext();
      }
    }
    return result;
  }

  public static void main( String[] args ) throws Exception {
    CsvOutput csvOutput = new CsvOutput();
    Map<String, String> alternateErrorCode = new HashMap<String, String>();
    Map<String, String> bypassErrorCode = new HashMap<String, String>();
    Map<String, String> weHaveErrorCode = new HashMap<String, String>();

    setupAlternateErrorCodes(alternateErrorCode);
    setupBypassOurErrorCodes(bypassErrorCode);
    setupWeHaveErrorCodes(weHaveErrorCode);

    int totalTransactions = 0;
    String lastReference = "";

    // Error Stats
    int totalErrorTransactions = 0;
    int totalErrorTests = 0;
    int notErrors = 0;
    int ignoreErrors = 0;
    int codesMatchSARS = 0;
    int invalidMatchSARS = 0;
    int disagreeWithSARS = 0;
    ErrorCount codesMatchSARSCount = new ErrorCount();
    ErrorCount ignoreErrorCount = new ErrorCount();
    ErrorCount invalidMatchCount = new ErrorCount();
    ErrorCount disagreeWithSARSCount = new ErrorCount();

    // Success Stats
    int totalSuccessTransactions = 0;
    int totalSuccessTests = 0;
    int weHaveErrors = 0;
    int weHaveWarnings = 0;
    int weMatchSuccess = 0;
    ErrorCount weHaveErrorCount = new ErrorCount();
    ErrorCount weHaveWarningCount = new ErrorCount();

    //CSVReaderEx reader = new CSVReaderEx(new FileReader("/Users/jake/Work/Standard Bank/errorlog_2016-02-29.txt"));
    CSVReaderEx reader = new CSVReaderEx(new FileReader("/Users/Synthesis/Desktop/finsurv-rules-java-engine/errorlog.txt"));
    TranDecision entry;
    while ((entry = readTranDecision(reader)) != null) {
      boolean bValidError = true;
      boolean bIgnoreError = false;

      totalTransactions++;
      lastReference = (entry.oldEntries.size() > 0) ? entry.oldEntries.get(0).getReference() : entry.newEntries.get(0).getReference();
      if ((entry.oldEntries.size() == 0) || (entry.oldEntries.get(0).getValidationType() == CsvOutput.ValidationType.OldSuccess))
        totalSuccessTransactions++;
      else
        totalErrorTransactions++;

      List<CsvOutput.ValidationEntry> issues = entry.newEntries;
      for(CsvOutput.ValidationEntry oldEntry : entry.oldEntries)
      {
        bValidError = true;
        bIgnoreError = false;
        if (oldEntry.getValidationType() == CsvOutput.ValidationType.OldError) {
          totalErrorTests++;

          // Error Stats
          String errorCode = oldEntry.getCode();
          String errorDesc = oldEntry.getDescription();
          String attribute = oldEntry.getAttribute();
          String ourErrorCode = null;

          boolean bWeHaveSARBError = false;
          if (issues.size() > 0) {
            for (CsvOutput.ValidationEntry res : issues) {

              String altResCode = alternateErrorCode.containsKey(res.getCode()) ? alternateErrorCode.get(res.getCode()) : "";
              if (res.getCode().equals(errorCode) || altResCode.contains(errorCode)) {
                ourErrorCode = res.getCode();
                bWeHaveSARBError = true;
                break;
              }
            }
          }
          if (!bWeHaveSARBError && weHaveErrorCode.containsKey(errorCode)) {
            if (weHaveErrorCode.get(errorCode).equals(errorDesc)) {
              bWeHaveSARBError = true;
            }
          }

          // Not valid errors
          if (errorCode.equals("") && issues.size() == 0) {
            notErrors++;
            bValidError = false;
          } else
          if (errorDesc.equals("238-error no code details found") && issues.size() == 0) {
            notErrors++;
            bValidError = false;
          } else
          if (errorDesc.equals("313-error no code details found") && issues.size() == 0) {
            notErrors++;
            bValidError = false;
          }

          // Ignore these errors
          if (bValidError && alternateErrorCode.containsKey(errorCode)) {
            String alternate = alternateErrorCode.get(errorCode);
            if (alternate.equals("ignore") ||
                    alternate.equals("ignore:"+errorDesc)) {
              ignoreErrors++;
              bIgnoreError = true;
              ignoreErrorCount.addError(errorCode, errorDesc, attribute, oldEntry.getReference());
            }
          }

          // We have the same error as raised by the SARB
          if (bValidError && !bIgnoreError) {
            if (bWeHaveSARBError) {
              codesMatchSARS++;
              codesMatchSARSCount.addError(errorCode, errorDesc + (ourErrorCode == null || errorCode.equals(ourErrorCode) ? "" : " (Synthesis code " + ourErrorCode + ")"), attribute, oldEntry.getReference());
            }
            else {
              int errorCount = 0;
              for (CsvOutput.ValidationEntry res : issues) {
                if (res.getValidationType() == CsvOutput.ValidationType.NewError)
                  errorCount++;
              }

              if (errorCount > 0) {
                invalidMatchSARS++;
                invalidMatchCount.addError(errorCode, errorDesc, attribute, oldEntry.getReference());
              }
              else {
                disagreeWithSARS++;
                disagreeWithSARSCount.addError(errorCode, errorDesc, attribute, oldEntry.getReference());
              }
            }
          }

          // Sanity check
          if(totalErrorTests - notErrors - ignoreErrors - codesMatchSARS - invalidMatchSARS - disagreeWithSARS != 0) {
            System.out.println();
          }
        }
        else {
          // Success Stats
          for (CsvOutput.ValidationEntry res : issues) {
            totalSuccessTests++;

            boolean bBypass = false;
            if (bypassErrorCode.containsKey(res.getCode())) {
              String bypass = bypassErrorCode.get(res.getCode());
              if (bypass.equals("ignore") ||
                      bypass.equals("ignore:"+res.getDescription())) {
                bBypass = true;
              }
            }

            if (bBypass) {
              weMatchSuccess++;
            }
            else if (res.getValidationType() == CsvOutput.ValidationType.NewError) {
              weHaveErrors++;
              weHaveErrorCount.addError(res.getCode(), res.getDescription(), res.getAttribute(), res.getReference());
            }
            else if (res.getValidationType() == CsvOutput.ValidationType.NewWarning) {
              weHaveWarnings++;
              weHaveWarningCount.addError(res.getCode(), res.getDescription(), res.getAttribute(), res.getReference());
            }
            else {
              weMatchSuccess++;
            }
          }
        }
      }
    }
    System.out.println("------------");
    System.out.println("Total Transactions: " + totalTransactions);
    System.out.println("------------");
    System.out.println("Total Errors      : " + totalErrorTests + " (transactions " + totalErrorTransactions + ")");
    System.out.println("Not Errors        : " + notErrors);
    System.out.println("Ignore Errors     : " + ignoreErrors);
    System.out.println("Codes Match SARB  : " + codesMatchSARS);
    System.out.println("Invalid Match SARB: " + invalidMatchSARS);
    System.out.println("Disagree With SARB: " + disagreeWithSARS);
    System.out.println("Checksum (should be 0): " + (totalErrorTests - notErrors - ignoreErrors - codesMatchSARS - invalidMatchSARS - disagreeWithSARS));
    System.out.println("------------");
    System.out.println("Success Tests     : " + totalSuccessTests + " (transactions " + totalSuccessTransactions + ")");
    System.out.println("Agree with SARB   : " + weMatchSuccess);
    System.out.println("We have Errors    : " + weHaveErrors);
    System.out.println("We have Warnings  : " + weHaveWarnings);
    System.out.println("Checksum (should be 0): " + (totalSuccessTests - weMatchSuccess - weHaveErrors - weHaveWarnings));
    System.out.println("------------");

    System.out.println("** SARB says Error - We say None **");
    disagreeWithSARSCount.printAsDelimited(100, 4, "\t");

    System.out.println("** Different Error Codes **");
    invalidMatchCount.printAsDelimited(100, 4, "\t");

    System.out.println("** Codes Match **");
    codesMatchSARSCount.printAsDelimited(100, 4, "\t");

    System.out.println("** Ignored Codes **");
    ignoreErrorCount.printAsDelimited(100, 4, "\t");

    System.out.println("** We say Error - SARB say None **");
    weHaveErrorCount.printAsDelimited(200, 4, "\t");

    System.out.println("** We warn **");
    weHaveWarningCount.printAsDelimited(100, 4, "\t");

    System.out.println("** Last Reference **");
    System.out.println(lastReference);
  }
}
