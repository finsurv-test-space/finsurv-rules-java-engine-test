package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.DSLSupport;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IStringResolver;

public class map implements IStringResolver {
  private String lookup;

  public map(String lookup) {
    this.lookup = lookup;
  }

  @Override
  public String resolve(FinsurvContext context, Object value) {
    return DSLSupport.map(context.getMappings(), lookup);
  }
}
