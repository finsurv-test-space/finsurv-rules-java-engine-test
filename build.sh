#!/bin/bash

set -eE
ROOT=$PWD

write_metadata(){
   echo "{
                \"artefact\":
                [{
                        \"name\":\"${ROOT}/build/libs/*.jar\",
                        \"destination\":\"JARS\"
                }]
        }" >> ${ROOT}/build/metadata.json
}

# Useage: errExitFunc $?
errExitFunc() {
    if [ $1 == 1 ]
    then
        echo -e ${RED}"ERROR: Build error"${NC}
        exit 1
    fi
}

./gradlew clean
rm -rf ${ROOT}/build

#xxx test disabled 
#gradle test -i --tests za.co.synthesis.TestChannels
#gradle test -i --tests za.co.synthesis.Evaluations 

./gradlew build -x test
errExitFunc $?

./gradlew -b build.gradle build -x test --warning-mode all
errExitFunc $?

./gradlew publishToMavenLocal

write_metadata

# if [ ! -d "BuiltFiles" ]
# then
# 	mkdir "BuiltFiles"
# fi

# rm -f "BuiltFiles/*"
# cp "build/libs/"*.jar "BuiltFiles/"
# rm -f "BuiltFiles/"*source*.jar