package za.co.synthesis.rule.support;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSRegExLiteral;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

/**
 * User: jake
 * Date: 4/19/16
 * Time: 8:31 PM
 * Creates a new function based on a given JavaScript definition
 */
public class DisplayFunctionFactory {
  private static final String functionPackage = "za.co.synthesis.rule.predef.";

  private static final Map<String, IDisplayFunction> functionCache = new HashMap<String, IDisplayFunction>();

  public static void registerFunction(final String name, IDisplayFunction displayFunction) {
    functionCache.put(name, displayFunction);
  }

  public static IDisplayFunction createFunction(final String name) throws Exception {
    if (functionCache.containsKey(name)) {
      return functionCache.get(name);
    }

    Class<?> cls = Class.forName(functionPackage + name);
    return (IDisplayFunction)cls.newInstance();
  }

  public static IDisplayFunction createFunction(final String name, final String param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class);
    return (IDisplayFunction)clsConstructor.newInstance(param1);
  }

  public static IDisplayFunction createFunction(final String name, final JSRegExLiteral param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(JSRegExLiteral.class);
    return (IDisplayFunction)clsConstructor.newInstance(param1);
  }

  public static IDisplayFunction createFunction(final String name, final JSRegExLiteral param1, final String param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(JSRegExLiteral.class, String.class);
    return (IDisplayFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IDisplayFunction createFunction(final String name, final StringList param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(StringList.class);
    return (IDisplayFunction)clsConstructor.newInstance(param1);
  }

  public static IDisplayFunction createFunction(final String name, final String param1, final String param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class);
    return (IDisplayFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IDisplayFunction createFunction(final String name, final String param1, final StringList param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, StringList.class);
    return (IDisplayFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IDisplayFunction createFunction(final String name, final IDisplayFunction param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IDisplayFunction.class);
    return (IDisplayFunction)clsConstructor.newInstance(param1);
  }

  public static IDisplayFunction createFunction(final String name, final String param1, final IDisplayFunction param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, IDisplayFunction.class);
    return (IDisplayFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IDisplayFunction createFunction(final String name, final String param1, final String param2, final String param3, final String param4) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class, String.class, String.class);
    return (IDisplayFunction)clsConstructor.newInstance(param1, param2, param3, param4);
  }

}
