package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 3/21/16
 * Time: 3:40 PM
 * The interface for a class that registers custom validation functions
 */
public interface ICustomValidateFactory {
  void register(ValidationEngine engine);
}
