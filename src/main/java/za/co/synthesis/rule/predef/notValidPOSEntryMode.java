package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jake
 * Date: 8/9/14
 * Time: 7:30 PM
 * Determines if the current value is a valid Point of Sale (POS) entry mode
 */
public class notValidPOSEntryMode implements IAssertionFunction {
  private final Map<String, Pattern> patterns = new HashMap<String, Pattern>();

  public notValidPOSEntryMode() {
    patterns.put("VISA", Pattern.compile("^(0[0-7]|10|84|90|91|95|96|XX)$"));
    patterns.put("MASTER", Pattern.compile("^[019][0126ABCFMNORST]$"));
    patterns.put("AMEX", Pattern.compile("^[0-6][0-6]$"));
    patterns.put("DINERS", Pattern.compile("^[012349S][01234569]$"));
    patterns.put("MAESTRO", Pattern.compile("^(0[0-8]|79|80|81|82|90|91|92)$"));
    patterns.put("ELECTRON", Pattern.compile("^(0[0-7]|10|81|84|86|90|91|95|96|XX)$"));
    patterns.put("BOCEXPRESS", Pattern.compile("^.*$"));
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object cardType = context.getMoneyField(context.getCurrentMoneyInstance(), "CardIndicator");
    if (cardType != null) {
      Pattern pattern = patterns.get(cardType.toString());
      if (pattern != null && value != null) {
        Matcher matcher = pattern.matcher(value.toString());
        return !matcher.matches();
      }
    }
    return false;
  }
}
/*
  _export.notValidPOSEntryMode = function (context, value) {
    var cardType = context.getMoneyField(context.currentMoneyInstance, 'CardIndicator');
    var regPEM;
    if (cardType) {
      if (cardType === 'VISA') {
        regPEM = /^(0[0-7]|10|84|90|91|95|96|XX)$/;
      } else if (cardType === 'MASTER') {
        regPEM = /^[019][0126ABCFMNORST]$/;
      } else if (cardType === 'AMEX') {
        regPEM = /^[0-6][0-6]$/;
      } else if (cardType === 'DINERS') {
        regPEM = /^[012349S][01234569]$/;
      } else if (cardType === 'MAESTRO') {
        regPEM = /^(0[0-8]|79|80|81|82|90|91|92)$/;
      } else if (cardType === 'ELECTRON') {
        regPEM = /^(0[0-7]|81|84|86|90|91|95|96|XX)$/;
      } else if (cardType === 'BOCEXPRESS') {
        regPEM = null;
      }
    }
    if (regPEM) {
      if (!value)
        return false;

      return (regPEM.test(value) == false);
    }
    return false;
  }
*/