package za.co.synthesis;

import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.DisplayEngine;
import za.co.synthesis.rule.core.Displayor;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.support.Util;

import java.io.FileReader;
/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 4/22/16
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class DisplayTest {
  public static DisplayEngine createEngine() throws Exception {
    DisplayEngine engine = new DisplayEngine();

    DefaultSupporting supporting = new DefaultSupporting();
    supporting.setCurrentDate(LocalDate.now());
    supporting.setGoLiveDate(Util.date("2013-08-18"));
    engine.setupSupporting(supporting);

    engine.setupLoadLookupFile("../finsurv-rules/reg_rule_repo/coreSADC/data/lookups.js");

    engine.setupLoadRuleFile("./src/test/resources/autoenrich.js");

    return engine;
  }

  public static void main( String[] args ) throws Exception {
    DisplayEngine engine = createEngine();
    Displayor displayor = engine.issueDisplayor();
    displayor.getDisplayRuleBehaviour().setFilterUnchangedValues(false);
    displayor.getDisplayRuleBehaviour().setSetOriginalValueOnFailedAssertion(true);

    DisplayTestSupport test = new DisplayTestSupport();

    FileReader fileReader = new FileReader("./src/test/resources/enrichTestCases.js");
    JSStructureParser parser = new JSStructureParser(fileReader);

    Object obj = parser.parse();

    JSObject testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
    if (testCaseContainer != null) {
      JSArray cases = (JSArray)testCaseContainer.get("test_cases");

      for (Object objCase : cases) {
        if (objCase instanceof JSFunctionCall) {
          JSFunctionCall jsFunc = (JSFunctionCall) objCase;

          boolean processedTest = false;

          //assertShow("MainTransferPurpose",  {ReportingQualifier: 'BOPCUS'})
          if (jsFunc.getName().equals("assertShpw") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject) jsFunc.getParameters().get(1);
            test.assertShow(displayor, jsFunc.getParameters().get(0).toString(), jsObj);
            processedTest = true;
          }

          //assertHide("MainTransferPurpose",  {ReportingQualifier: 'NON REPORTABLE'})
          if (jsFunc.getName().equals("assertHide") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject) jsFunc.getParameters().get(1);
            test.assertHide(displayor, jsFunc.getParameters().get(0).toString(), jsObj);
            processedTest = true;
          }

          //assertValue("Resident.Description", "Foo, Bar", {Resident: {Individual: {Name:'Foo',Surname:"Bar"}}})
          if (jsFunc.getName().equals("assertValue") && jsFunc.getParameters().size() == 3) {
            JSObject jsObj = (JSObject) jsFunc.getParameters().get(2);
            String value = jsFunc.getParameters().get(1).toString();
            test.assertValue(displayor, jsFunc.getParameters().get(0).toString(), value, jsObj);
            processedTest = true;
          }

          //assertUnchangedValue("Resident.Description", {Resident: {Individual: {Name:'Foo',Surname:"Bar"}}})
          if (jsFunc.getName().equals("assertUnchangedValue") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject) jsFunc.getParameters().get(1);
            test.assertUnchangedValue(displayor, jsFunc.getParameters().get(0).toString(), jsObj);
            processedTest = true;
          }

          //assertFieldValue("RunField", "SetField", "Foo, Bar", {Resident: {Individual: {Name:'Foo',Surname:"Bar"}}})
          if (jsFunc.getName().equals("assertFieldValue") && jsFunc.getParameters().size() == 4) {
            String filterField = jsFunc.getParameters().get(0).toString();
            String setField = jsFunc.getParameters().get(1).toString();
            String value = jsFunc.getParameters().get(2).toString();
            JSObject jsObj = (JSObject) jsFunc.getParameters().get(3);
            test.assertFieldValue(displayor, filterField, setField, value, jsObj);
            processedTest = true;
          }

          //assert2FieldValues("RunField", "SetField1", "Value1", "SetField2", "Value2", {Resident: {Individual: {Name:'Foo',Surname:"Bar"}}})
          if (jsFunc.getName().equals("assert2FieldValues") && jsFunc.getParameters().size() == 6) {
            String filterField = jsFunc.getParameters().get(0).toString();
            String setField1 = jsFunc.getParameters().get(1).toString();
            String value1 = jsFunc.getParameters().get(2).toString();
            String setField2 = jsFunc.getParameters().get(3).toString();
            String value2 = jsFunc.getParameters().get(4).toString();
            JSObject jsObj = (JSObject) jsFunc.getParameters().get(5);
            test.assert2FieldValues(displayor, filterField, setField1, value1, setField2, value2, jsObj);
            processedTest = true;
          }

          if (!processedTest) {
            System.out.println("!! Have not catered for test: " + jsFunc.getName());
          }
        }
      }
    }

    System.out.println("\n*** Ran " + test.getTestCount() + " tests. Errors: " + test.getErrorCount());
  }
}
