package za.co.synthesis.rule.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * User: jake
 * Date: 3/6/16
 * Time: 11:00 AM
 * This stores the version number of the rules engine. By definition major versions are updated on the addition of new
 * predef functions. Minor versions are incremented when fixes are made to the predef functions associated with a major
 * version.
 * I.e. If rules are written for a specific major lease then they should be expected to work on later major versions.
 *
 */
public class EngineVersion {
  private int major;
  private int minor;

  public EngineVersion(int major, int minor) {
    this.major = major;
    this.minor = minor;
  }

  public int getMajor() {
    return major;
  }

  public int getMinor() {
    return minor;
  }

  public boolean isEqualOrLower(EngineVersion that) {
    if (that.getMajor() < major)
      return true;

    if (that.getMajor() == major && that.getMinor() <= minor)
      return true;

    return false;
  }

  public boolean isHigher(EngineVersion that) {
    if (major > that.getMajor())
      return true;

    if (major == that.getMajor() && minor > that.getMinor())
      return true;

    return false;
  }

  private static EngineVersion cachedVersion = null;

  public static EngineVersion getVersion() {
    EngineVersion result = null;
    if (cachedVersion == null) {
      try {
        Properties prop = new Properties();

        InputStream inputStream =
                EngineVersion.class.getClassLoader().getResourceAsStream("synthesis_rulesengine_version.properties");

        if (inputStream != null) {
          prop.load(inputStream);

          String majorVer = prop.getProperty("major");
          String minorVer = prop.getProperty("minor");
          result = new EngineVersion(Integer.parseInt(majorVer), Integer.parseInt(minorVer));
        } else {
          result = new EngineVersion(2,0);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
      cachedVersion = result;
    }
    else
      result = cachedVersion;
    return result;
  }

  public String toString() {
    return "{major=" + major + ", minor=" + minor + "}";
  }
}
