package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 5:01 AM
 * Checks for an exact length
 */
public class isWrongLength implements IAssertionFunction {
  private int len;

  public isWrongLength(int len) {
    this.len = len;
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (value != null && value instanceof String && ((String) value).length() > 0) {
      return ((String)value).trim().length() != len;
    }
    return false;
  }
}
/*
function isWrongLength(constant) {
  return function(context, value) {
    if (value && typeof value === "string")
      return value.trim().length != constant;
    return false;
  };
}
*/