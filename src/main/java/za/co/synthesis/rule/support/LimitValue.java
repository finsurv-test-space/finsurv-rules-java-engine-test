package za.co.synthesis.rule.support;

import java.util.List;

/**
 * Created by jake on 4/10/16.
 */
public class LimitValue extends DisplayRule {
  public LimitValue(List<String> values, IAssertionFunction assertion) {
    super(DisplayRuleType.LimitValue, null, values, null, assertion);
  }
}
