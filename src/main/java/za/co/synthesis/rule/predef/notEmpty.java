package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/5/14
 * Time: 10:35 PM
 * Determines if the given value is not empty
 */
public class notEmpty implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value != null && !(value instanceof IFinsurvContext.Undefined)) {
      if (!(value instanceof String) || ((String)value).trim().length() > 0) {
        return true;
      }
    }
    return false;
  }
}
