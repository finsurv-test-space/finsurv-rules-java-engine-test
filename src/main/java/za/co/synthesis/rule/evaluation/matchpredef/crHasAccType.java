package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.core.BankAccountType;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;
import za.co.synthesis.rule.support.JSConstant;

import java.util.ArrayList;
import java.util.List;


public class crHasAccType implements IMatchAssertionFunction {
  private List<BankAccountType> constants = new ArrayList<BankAccountType>();

  public crHasAccType() {
  }

  public crHasAccType(Object value) {
    if (value != null) {
      constants.add(JSConstant.bankAccountType(value));
    }
  }

  public crHasAccType(List<Object> values) {
    if (values != null) {
      for (Object value : values) {
        constants.add(JSConstant.bankAccountType(value));
      }
    }
  }

  public void setConstants(List<BankAccountType> constants) {
    this.constants = constants;
  }

  @Override
  public boolean execute(MatchInfo matchInfo) {
    BankAccountType accType = matchInfo.getCrAccType();

    if (accType != null) {
      return constants.contains(accType);
    }
    return false;
  }
}