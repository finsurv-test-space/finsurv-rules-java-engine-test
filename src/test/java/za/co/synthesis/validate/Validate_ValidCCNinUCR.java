package za.co.synthesis.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:18 PM
 * A Test implementation of the Validate_ValidCCNinUCR function. Needs to raise the following SARB error:
 * 514, "Invalid customs client number completed in UCR"
 * UCR format: nZA12345678a...24a where n = last digit of the year, ZA = Fixed, 12345678 = Valid Customs Client Number, a...24a = consignment number
 */
public class Validate_ValidCCNinUCR implements ICustomValidate {
  private Map<String, CCNInfo> ccnInfoMap;

  public Validate_ValidCCNinUCR(Map<String, CCNInfo> ccnInfoMap) {
    this.ccnInfoMap = ccnInfoMap;
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String ucr = value.toString();
    String ccn = ucr.substring(3, 11);

    String errorCode = "514";
    String errorDesc = "Invalid customs client number completed in UCR";

    if (ccnInfoMap.containsKey(ccn)) {
      CCNInfo ccnInfo = ccnInfoMap.get(ccn);

      if (ccnInfo.isValidCCN())
        return new CustomValidateResult(StatusType.Pass);
      else
        return new CustomValidateResult(StatusType.Fail, errorCode, errorDesc);
    }
    else {
      return new CustomValidateResult(StatusType.Pass);
    }
  }
}
