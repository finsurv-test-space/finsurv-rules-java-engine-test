package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.DSLSupport;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 9:36 AM
 * Determines if the sum of the monetary rand values do not equal the current value
 */
public class notSumLocalValue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    BigDecimal total = new BigDecimal(0);
    for (int i = 0; i < context.getMoneySize(); i++) {
      BigDecimal rvalue = Util.number(context.getMoneyField(i, context.map("LocalValue")));
      if (rvalue != null)
        total = total.add(rvalue);
    }
    BigDecimal valueNum = Util.number(value);
    if (valueNum != null)
      return valueNum.compareTo(total) != 0;
    return true;
  }
}
/*
function notSumRandValue(context, value) {
  var total = 0;
  for ( var i = 0; i < context.getMoneySize(); i++) {
    var rvalue = context.getMoneyField(i, "RandValue");
    if (rvalue)
      total += Number(rvalue);
  }
  var a = Math.round(Number(value) * 100);
  var b = Math.round(total * 100);
  return a != b;
}
*/