package za.co.synthesis.rule.support;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.core.ILookups;

import java.math.BigDecimal;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
import za.co.synthesis.rule.support.legacydate.LocalDateTime;
import za.co.synthesis.rule.support.legacydate.format.DateTimeFormatter;
import za.co.synthesis.rule.support.legacydate.format.DateTimeParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
#else*/
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
//#endif

/**
 * User: jake
 * Date: 8/8/14
 * Time: 8:30 AM
 * Used for routines that deal with numbers
 */
public class Util {
  private static DateTimeFormatter validDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  private static DateTimeFormatter validDateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
  private static DateTimeFormatter noDashLongDateFormat = DateTimeFormatter.ofPattern("yyyyMMdd");
  private static DateTimeFormatter noDashDateFormat = DateTimeFormatter.ofPattern("yyMMdd");

  private static final Set<Class<?>> WRAPPER_TYPES = getWrapperTypes();

  public static boolean isWrapperType(Class<?> clazz)
  {
    return WRAPPER_TYPES.contains(clazz);
  }

  public static boolean isSimpleType(Class<?> clazz)
  {
    return clazz.isPrimitive() || clazz.isEnum() || WRAPPER_TYPES.contains(clazz);
  }

  private static Set<Class<?>> getWrapperTypes()
  {
    Set<Class<?>> ret = new HashSet<Class<?>>();
    ret.add(String.class);
    ret.add(Boolean.class);
    ret.add(Character.class);
    ret.add(Byte.class);
    ret.add(Short.class);
    ret.add(Integer.class);
    ret.add(Long.class);
    ret.add(Float.class);
    ret.add(Double.class);
    ret.add(Void.class);
    ret.add(JSRegExLiteral.class);
    return ret;
  }

  public static BigDecimal number(Object value) {
    if (value != null) {
      if (value instanceof BigDecimal) {
        return (BigDecimal) value;
      } else {
        try {
          String valueNoCommas = value.toString().replace(",", "");
          return new BigDecimal(valueNoCommas);
        } catch (Exception e) {
          return null;
        }
      }
    }
    return null;
  }

  public static LocalDate date(Object value) {
    if (value != null) {
      if (value instanceof LocalDate) {
        return (LocalDate) value;
      } else if (value instanceof Date) {
/*#if OLDDATE
        return new LocalDate((Date)value);
#else*/
        return (((Date) value).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
//#endif
      } else {
        try {
          return LocalDate.parse(value.toString(), validDateFormat);
        } catch (Exception e) {
          return null;
        }
      }
    }
    return null;
  }

  public static LocalDateTime dateTime(Object value) {
    if (value != null) {
      if (value instanceof LocalDateTime) {
        return (LocalDateTime) value;
      } else if (value instanceof Date) {
/*#if OLDDATE
        return new LocalDateTime((Date)value);
#else*/
        return (((Date) value).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
//#endif
      } else {
        try {
          String str = value.toString();
          if (str.length() > 19)
            str = str.substring(0, 19);
          str = str.replace('T', ' ');
          return LocalDateTime.parse(str, validDateTimeFormat);
        } catch (Exception e) {
          return null;
        }
      }
    }
    return null;
  }

  public static String dateTimeToString(LocalDateTime ldt) {
    if (ldt != null) {
/*#if OLDDATE
        return validDateTimeFormat.format(ldt);
#else*/
        return ldt.format(DateTimeFormatter.ISO_DATE_TIME);
//#endif
    }
    return null;
  }

  public static boolean isNumeric(final String s) {
    if (s == null) return false;
    return s.matches("\\d+(\\.\\d{1,2})?");
  }

  public static boolean isNumeric(final Object o) {
    return (o instanceof String && Util.isNumeric((String) o)) ||
        (o instanceof Number && Number.class.isAssignableFrom(o.getClass()));
  }

  public static boolean validRSAID(final String id) {
    if (id.length() != 13) {
      return false;
    }

    if (!isNumeric(id)) {
      return false;
    }

    if (!id.substring(0, 6).matches("^\\d{2}(0[1-9]|1[0-2])(0[1-9]|1\\d|2\\d|3[0-1])$"))
      return false;

    int[] digits = new int[id.length()];
    for (int i = 0; i < id.length(); i++) {
      digits[i] = Character.digit(id.charAt(i), 10);
    }

    int a = 0;
    for (int i = 0; i < 6; i++) {
      a += digits[i * 2];
    }

    int b = 0;
    for (int i = 0; i < 6; i++) {
      int n = 2 * (digits[i * 2 + 1]);
      b += (n / 10) + (n % 10);
    }

    int total = a + b;
    int tmp = (10 * ((total / 10) + 1)) - total;
    if (tmp < 10) {
      return tmp % 10 == digits[12];
    } else if (tmp == 10) {
      return 0 == digits[12];
    } else {
      return false;
    }
  }

  public static boolean isValidVATNumber(final String str) {
    if ((!str.matches("^\\d+$")) &&
       (str.equals("NO VAT NUMBER") || str.equals("NO VAT NUM"))) return true;
    return true;
  }

  public static boolean isValidZATaxNumber(final String str) {
    boolean isChecksumCorrect = false;

    if (str != null && str.trim().length() > 0) {
      /*
       0254/089/06/3,
       * Ignore all non-numeric values.
       * Take each digit in an ODD position and multiply it by 2.
       * Total A (0x2) + (5x2) +(0x2) + (9x2) + (6x2)
       0    +  1+0   +   0    +  1+8  + 1+2
       Add these values giving Total A.   13

       Add all the digits in EVEN positions giving Total B.
       Total B 2 + 4 + 8 + 0 = 14 (ignore last digit, as that is the check digit)

       Add Total A and Total B together to give you Total C Total C 13 + 14 = 27

       Subtract Total C from the next highest multiple of 10, giving the check digit.

       Check digit 30 % 27 = 3
       The last digit i.e. 3 is the check digit.

       */

      String taxNumber = str.replaceAll("[^0-9]", "");

      if (taxNumber.length() == 10) {
        int oddChecksum = 0;
        int evenChecksum = 0;

        for (int i = 0; i < taxNumber.length() - 1; i++) {
          int digit = Character.digit(taxNumber.charAt(i), 10);
          if (i % 2 == 0) {
            // ODD positions (using zero offset)
            int multiple = digit * 2;
            if (multiple < 10)
              oddChecksum += multiple;
            else
              oddChecksum += (multiple - 10) + 1;
          } else {
            // EVEN positions
            evenChecksum += digit;
          }
        }
        int check_digit = Character.digit(taxNumber.charAt(9), 10);

        int checksum = 10 - (oddChecksum + evenChecksum) % 10;
        isChecksumCorrect = (checksum == check_digit);
      }
    }
    return isChecksumCorrect;
  }

  public static boolean isValidCCN(final String str) {
    if (!str.matches("^\\d{8}$"))
      return false;

    int[] digits = new int[8];
    int i;
    for (i = 0; i < 8; i++) {
      if (i < str.length())
        digits[i] = Integer.parseInt(str.substring(i, i + 1));
      else
        digits[i] = 0;
    }

    int total = 0;
    int multiplier = 9;
    for (i = 0; i < 8; i++) {
      total += digits[i] * multiplier;
      multiplier -= 1;
      if (multiplier == 5)
        multiplier -= 1;
    }
    return (total % 11) == 0 || (total % 10) == 0;
  }

  /*
    if (!value.match("^\\d{8}$"))
      return false;

    var digits = [];
    var i;

    for (i = 0; i < value.length; i++) {
      digits.push(Number(value.charAt(i)));
    }

    var total = 0;
    var multiplier = 9;
    for (i = 0; i < 8; i++) {
      total += digits[i] * multiplier;
      multiplier -= 1;
      if (multiplier == 5)
        multiplier -= 1;
    }
    return (total % 11) == 0 || (total % 10) == 0;
  */

  public static boolean isValidPostalCode(final String str) {
    return str.matches("^([0-9]){1,4}$");
  }

  public synchronized static boolean isValidICN(final String str, ILookups lookups) {
    if (str.length() != 18)
      return false;
    if (!lookups.isValidCustomsOfficeCode(str.substring(0, 3)))
      return false;
    if (!str.substring(3, 11).matches("^(19|20)[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])$"))
      return false;
    try {
      LocalDate.parse(str.substring(3, 11), noDashLongDateFormat);
    } catch (DateTimeParseException e) {
      return false;
    }
    return str.substring(11, 18).matches("^[0-9]*$");
  }

  public static boolean isValidICNCustomsOffice(final String str, ILookups lookups) {
    if (str.length() <= 3)
      return false;
    if (!lookups.isValidCustomsOfficeCode(str.substring(0, 3)))
      return false;
    return true;
  }

  public static boolean isValidUCR(final String str) {
    if (str.length() < 12 || str.length() > 35)
      return false;
    return str.matches("^[0-9]ZA[0-9]{8}[0-9a-zA-Z]{1,24}$");
  }

  public static boolean isValidEmail(final String str) {
    String emailreg = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    return str.matches(emailreg);
  }

  public static boolean isValidECI(final String str, final String cardType) {
    String regECI = null;
    if (cardType != null) {
      if (cardType.equals("VISA") || cardType.equals("ELECTRON")) {
        regECI = "^(0[0-9]|XX)$";
      } else if (cardType.equals("MASTER") || cardType.equals("MAESTRO")) {
        regECI = "^([0-3]|X)$";
      } else if (cardType.equals("AMEX")) {
        regECI = "^0[5-7]$";
      } else if (cardType.equals("DINERS")) {
        regECI = "^0[5-8]$";
      }
    }
    if (regECI != null)
      return str.matches(regECI);
    return false;
  }

  public synchronized static boolean doesDateMatchSAID(final LocalDate date, final String idStr) {
    if (date != null && idStr != null) {
      String dateStr = noDashDateFormat.format(date);
      return idStr.startsWith(dateStr);
    }
    return false;
  }
}
