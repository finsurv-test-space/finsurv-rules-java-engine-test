package za.co.synthesis.mapping.configuration;

import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.javascript.*;

import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 1:12 AM
 * This is a configuration section object that is initialised with data from a JSObject read in from the mapping file.
 * Each section contains a number of fields
 */
public class Section {
  private MappingScope scope;
  private String source;
  private List<Field> fields = new ArrayList<Field>();

  protected static Section setup(JSObject jsScopeMeta) {
    Section result = new Section();
    Object rulesetScope = jsScopeMeta.get("scope");
    Object source = jsScopeMeta.get("src");

    if (source != null)
      result.source = source.toString();

    if (rulesetScope != null) {
      result.scope = null;
      if (rulesetScope.equals("result"))
        result.scope = MappingScope.Results;
      else
      if (rulesetScope.equals("transaction"))
        result.scope = MappingScope.Transaction;
      else
      if (rulesetScope.equals("money"))
        result.scope = MappingScope.Money;
      else
      if (rulesetScope.equals("importexport"))
        result.scope = MappingScope.ImportExport;

      Object fields = jsScopeMeta.get("fields");
      if (fields instanceof JSArray) {
        for (Object field : (JSArray)fields) {
          if (field instanceof JSObject) {
            result.fields.add(Field.setup(result.scope, result.source, (JSObject) field));
          }
        }
      }
    }
    return result;
  }

  public MappingScope getMappingScope() {
    return scope;
  }

  public String getSource() {
    return source;
  }

  public List<Field> getFields() {
    return fields;
  }
}
