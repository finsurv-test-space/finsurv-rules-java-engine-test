package za.co.synthesis.rule.evaluation.predef;

import za.co.synthesis.rule.core.CounterpartyInstituition;
import za.co.synthesis.rule.core.ResidenceStatus;
import za.co.synthesis.rule.evaluation.EvaluationUtil;
import za.co.synthesis.rule.evaluation.GivenInfo;
import za.co.synthesis.rule.evaluation.IAssumptionAssertionFunction;
import za.co.synthesis.rule.support.JSConstant;

import java.util.ArrayList;
import java.util.List;

public class hasResStatus implements IAssumptionAssertionFunction {
  private List<ResidenceStatus> constants = new ArrayList<ResidenceStatus>();

  public hasResStatus() {
  }

  public hasResStatus(Object value) {
    if (value != null) {
      constants.add(JSConstant.residenceStatus(value));
    }
  }

  public hasResStatus(List<Object> values) {
    if (values != null) {
      for (Object value : values) {
        constants.add(JSConstant.residenceStatus(value));
      }
    }
  }

  public void setConstant(ResidenceStatus constant) {
    this.constants.clear();
    this.constants.add(constant);
  }

  public void setConstants(List<ResidenceStatus> constants) {
    this.constants = constants;
  }

  @Override
  public boolean execute(GivenInfo givenInfo) {
    ResidenceStatus resStatus = givenInfo.getResidenceStatus();

    if (resStatus != null) {
      return constants.contains(resStatus);
    }
    return false;
  }
}
