package za.co.synthesis.rule.matching;

/**
 * Created by jake on 6/16/16.
 */
public class CFMultiply implements IConfidenceFunction {
  private IConfidenceFunction left;
  private IConfidenceFunction right;

  public CFMultiply(IConfidenceFunction left, IConfidenceFunction right) {
    this.left = left;
    this.right = right;
  }

  public double execute(MatchingContext context) {
    return left.execute(context) * right.execute(context);
  }
}
