package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 3/2/15
 * Time: 4:04 PM
 * Debit and Credit type
 */

/**
 * This is used to denote which side of the transaction must be used to populate the either the resident or
 * non resident side of the transaction
 */
public enum DrCr {
  Unknown,
  /**
   * Map client information from the debit side of the transaction
   */
  DR,
  /**
   * Map client information from the credit side of the transaction
   */
  CR
}

