package za.co.synthesis.rule.matching;

/**
 * Created by jake on 6/16/16.
 */
public interface IConfidenceFunction {
  double execute(MatchingContext context);
}
