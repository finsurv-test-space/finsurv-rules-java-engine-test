package za.co.synthesis.rule.difference.validation;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.difference.HTMLDiffUtils;
import za.co.synthesis.rule.difference.IDifference;

public class MappingDifference implements IDifference {
  private final String key;
  private final DifferenceType type;
  private final String oldValue;
  private final String newValue;

  public MappingDifference(String key, DifferenceType type, String oldValue, String newValue) {
    this.key = key;
    this.type = type;
    this.oldValue = oldValue;
    this.newValue = newValue;
  }

  public static MappingDifference createAddedMapping(String key, String value) {
    return new MappingDifference(key, DifferenceType.Added, null, value);
  }

  public static MappingDifference createChangedMapping(String key, String oldValue, String newValue) {
    return new MappingDifference(key, DifferenceType.Changed, oldValue, newValue);
  }

  public static MappingDifference createRemovedMapping(String key, String value) {
    return new MappingDifference(key, DifferenceType.Removed, value, null);
  }

  public String getKey() {
    return key;
  }

  @Override
  public DifferenceType getType() {
    return type;
  }

  public String getOldValue() {
    return oldValue;
  }

  public String getNewValue() {
    return newValue;
  }

  @Override
  public String diffHTML() { //LocalCurrencySymbol: "$",
    if (type == DifferenceType.Added) {
      return String.format("<ins>%s: \"%s\"</ins>", key, newValue);
    }
    else
    if (type == DifferenceType.Changed) {
      return String.format("%s: %s", key, HTMLDiffUtils.composeValues(oldValue, newValue));
    }
    else
    if (type == DifferenceType.Removed) {
      return String.format("<del>%s: \"%s\"</del>", key, oldValue);
    }
    return "";
  }

  @Override
  public void addTo(JSArray array) {
    JSObject obj = new JSObject();
    obj.put("key", key);
    obj.put("oldValue", oldValue);
    obj.put("newValue", newValue);
    obj.put("diffHTML", diffHTML());
    array.add(obj);
  }
}
