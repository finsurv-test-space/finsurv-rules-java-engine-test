package za.co.synthesis.rule.difference.test;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.rule.core.FlowType;
import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.rule.difference.HTMLDiffUtils;
import za.co.synthesis.rule.difference.IDifference;
import za.co.synthesis.rule.difference.validation.DifferenceType;
import za.co.synthesis.rule.support.Rule;

import java.util.List;

import static za.co.synthesis.rule.difference.HTMLDiffUtils.composeNumberOrString;

/*
  assertFailure("flow3", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'OUT' }),
  assertSuccess("flow3", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'IN' }),
  //The flow direction of the report data must match the flow on the account entry. Please ask IT support to investigate
  assertSuccessCustom("flow4", { ReportingQualifier: 'BOPCUS', Flow: 'IN' }, {
    DealerType: "AD",
    AccountFlow: "IN"
  }),
*/
public class ValidationTestDifference implements IDifference {
  private final DifferenceType type;
  private final String testRuleName;
  private final int testInstance;
  private final String oldTestFunction;
  private final String newTestFunction;
  private final JSObject oldData;
  private final JSObject newData;
  private final JSObject oldCustom;
  private final JSObject newCustom;

  public ValidationTestDifference(DifferenceType type, String testRuleName, int testInstance,
                                  String oldTestFunction, JSObject oldData, JSObject oldCustom,
                                  String newTestFunction, JSObject newData, JSObject newCustom) {
    this.type = type;
    this.testRuleName = testRuleName;
    this.testInstance = testInstance;
    this.oldTestFunction = oldTestFunction;
    this.oldData = oldData;
    this.oldCustom = oldCustom;
    this.newTestFunction = newTestFunction;
    this.newData = newData;
    this.newCustom = newCustom;
  }

  public static ValidationTestDifference createAddedTest(String testRuleName, int testInstance, String testFunction,
                                                         JSObject data, JSObject custom) {
    return new ValidationTestDifference(DifferenceType.Added, testRuleName, testInstance,
            null, null, null, testFunction, data, custom);
  }

  public static ValidationTestDifference createChangedTest(String testRuleName, int testInstance,
                                                           String oldTestFunction, JSObject oldData, JSObject oldCustom,
                                                           String newTestFunction, JSObject newData, JSObject newCustom) {
    return new ValidationTestDifference(DifferenceType.Changed, testRuleName, testInstance,
            oldTestFunction, oldData, oldCustom, newTestFunction, newData, newCustom);
  }

  public static ValidationTestDifference createRemovedTest(String testRuleName, int testInstance, String testFunction,
                                                           JSObject data, JSObject custom) {
    return new ValidationTestDifference(DifferenceType.Removed, testRuleName, testInstance,
            testFunction, data, custom, null, null, null);
  }

  public String getTestRuleName() {
    return testRuleName;
  }

  public int getTestInstance() {
    return testInstance;
  }

  @Override
  public DifferenceType getType() {
    return type;
  }

  private String jsObjectAsString(JSObject jso) {
    if (jso != null) {
      JSWriter writer = new JSWriter();
      jso.removeExtraWhitespace();
      jso.composeSnippets(writer);
      return writer.toString();
    }
    else {
      return "";
    }
  }

  @Override
  public String diffHTML() {
    /*assertFailure("flow3", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'OUT' })*/
    if (type == DifferenceType.Added) {
      if (newCustom != null) {
        return String.format("<ins>%s(\"%s\", %s, %s)</ins>",
                newTestFunction, testRuleName, jsObjectAsString(newData), jsObjectAsString(newCustom)
        );
      }
      else {
        return String.format("<ins>%s(\"%s\", %s)</ins>",
                newTestFunction, testRuleName, jsObjectAsString(newData)
        );
      }
    }
    else
    if (type == DifferenceType.Changed) {
      if (oldCustom != null && newCustom != null) {
        return String.format("%s(\"%s\", %s, %s)",
                HTMLDiffUtils.composeWholeStrings(oldTestFunction, newTestFunction),
                testRuleName,
                HTMLDiffUtils.composeStrings(jsObjectAsString(oldData), jsObjectAsString(newData)),
                HTMLDiffUtils.composeStrings(jsObjectAsString(oldCustom), jsObjectAsString(newCustom))
        );
      }
      else
      if (oldCustom != null) {
        // New stuff has deleted the custom data
        return String.format("%s(\"%s\", %s<del>, %s</del>)",
                HTMLDiffUtils.composeWholeStrings(oldTestFunction, newTestFunction),
                testRuleName,
                HTMLDiffUtils.composeStrings(jsObjectAsString(oldData), jsObjectAsString(newData)),
                jsObjectAsString(oldCustom)
        );
      }
      else
      if (newCustom != null) {
        // Old test did not have the custom data
        return String.format("%s(\"%s\", %s<ins>, %s</ins>)",
                HTMLDiffUtils.composeWholeStrings(oldTestFunction, newTestFunction),
                testRuleName,
                HTMLDiffUtils.composeStrings(jsObjectAsString(oldData), jsObjectAsString(newData)),
                jsObjectAsString(newCustom)
        );
      }
      else {
        // Neither old or new test has custom data
        return String.format("%s(\"%s\", %s)",
                HTMLDiffUtils.composeWholeStrings(oldTestFunction, newTestFunction),
                testRuleName,
                HTMLDiffUtils.composeStrings(jsObjectAsString(oldData), jsObjectAsString(newData))
        );
      }
    }
    else
    if (type == DifferenceType.Removed) {
      if (oldCustom != null) {
        return String.format("<del>%s(\"%s\", %s, %s)</del>",
                oldTestFunction, testRuleName, jsObjectAsString(oldData), jsObjectAsString(oldCustom)
        );
      }
      else {
        return String.format("<del>%s(\"%s\", %s)</del>",
                newTestFunction, testRuleName, jsObjectAsString(newData)
        );
      }
    }
    return "";
  }

  private static JSObject testToJSObject(String testFunction,
                                         JSObject data, JSObject custom) {
    JSObject jsRule = new JSObject();
    jsRule.put("function", testFunction);

    if (data != null)
      jsRule.put("data", data);
    if (custom != null)
      jsRule.put("custom", custom);
    return jsRule;
  }

  @Override
  public void addTo(JSArray array) {
    JSObject obj = new JSObject();
    obj.put("testRule", testRuleName);
    obj.put("instance", testInstance);
    if (oldData != null) {
      obj.put("oldValue", testToJSObject(oldTestFunction, oldData, oldCustom));
    }
    if (newData != null) {
      obj.put("newValue", testToJSObject(newTestFunction, newData, newCustom));
    }
    obj.put("diffHTML", diffHTML());
    array.add(obj);
  }
}
