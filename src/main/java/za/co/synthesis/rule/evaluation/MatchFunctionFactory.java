package za.co.synthesis.rule.evaluation;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.support.StringList;

import java.lang.reflect.Constructor;
import java.util.List;

public class MatchFunctionFactory {
  private static final String functionPackage = "za.co.synthesis.rule.evaluation.matchpredef.";

  public static IMatchAssertionFunction createFunction(final String name) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    return (IMatchAssertionFunction)cls.newInstance();
  }

  public static IMatchAssertionFunction createFunction(final String name, final String param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class);
    return (IMatchAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IMatchAssertionFunction createFunction(final String name, final Object param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(Object.class);
    return (IMatchAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IMatchAssertionFunction createFunction(final String name, final JSRegExLiteral param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(JSRegExLiteral.class);
    return (IMatchAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IMatchAssertionFunction createFunction(final String name, final List<Object> param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(List.class);
    return (IMatchAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IMatchAssertionFunction createFunction(final String name, final String param1, final String param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class);
    return (IMatchAssertionFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IMatchAssertionFunction createFunction(final String name, final String param1, final String param2, final StringList param3) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class, StringList.class);
    return (IMatchAssertionFunction)clsConstructor.newInstance(param1, param2, param3);
  }

  public static IMatchAssertionFunction createFunction(final String name, final String param1, final String param2, final String param3) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class, String.class);
    return (IMatchAssertionFunction)clsConstructor.newInstance(param1, param2, param3);
  }

  public static IMatchAssertionFunction createFunction(final String name, final String param1, final StringList param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, StringList.class);
    return (IMatchAssertionFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IMatchAssertionFunction createFunction(final String name, final IMatchAssertionFunction param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IMatchAssertionFunction.class);
    return (IMatchAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IMatchAssertionFunction createFunction(final String name, final String param1, final IMatchAssertionFunction param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, IMatchAssertionFunction.class);
    return (IMatchAssertionFunction)clsConstructor.newInstance(param1, param2);
  }
}
