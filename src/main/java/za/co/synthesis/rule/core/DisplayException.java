package za.co.synthesis.rule.core;

/**
 * Created by jake on 4/12/16.
 */
public class DisplayException extends Exception {
  public DisplayException(String message) {
    super(message);
  }

  public DisplayException(String message, java.lang.Throwable throwable) {
    super(message, throwable);
  }
}
