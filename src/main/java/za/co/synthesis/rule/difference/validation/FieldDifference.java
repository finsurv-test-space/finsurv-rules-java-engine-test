package za.co.synthesis.rule.difference.validation;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.rule.difference.HTMLDiffUtils;
import za.co.synthesis.rule.difference.IDifference;

public class FieldDifference implements IDifference {
  private final DifferenceType type;
  private final FieldSize oldValue;
  private final FieldSize newValue;

  public FieldDifference(DifferenceType type, FieldSize oldValue, FieldSize newValue) {
    this.type = type;
    this.oldValue = oldValue;
    this.newValue = newValue;
  }

  public static FieldDifference createAddedField(FieldSize value) {
    return new FieldDifference(DifferenceType.Added, null, value);
  }

  public static FieldDifference createChangedField(FieldSize oldValue, FieldSize newValue) {
    return new FieldDifference(DifferenceType.Changed, oldValue, newValue);
  }

  public static FieldDifference createRemovedField(FieldSize value) {
    return new FieldDifference(DifferenceType.Removed, value, null);
  }

  public Scope getScope() {
    if (oldValue != null) {
      return oldValue.getScope();
    }
    if (newValue != null) {
      return newValue.getScope();
    }
    return null;
  }

  public String getField() {
    if (oldValue != null) {
      return oldValue.getField();
    }
    if (newValue != null) {
      return newValue.getField();
    }
    return null;
  }

  @Override
  public DifferenceType getType() {
    return type;
  }

  @Override
  public String diffHTML() {
    if (type == DifferenceType.Added) {
      StringBuilder sb = new StringBuilder();
      HTMLDiffUtils.appendNameValue(sb, ", ", "minLen", newValue.getMinLen());
      HTMLDiffUtils.appendNameValue(sb, ", ", "maxLen", newValue.getMaxLen());
      HTMLDiffUtils.appendNameValue(sb, ", ", "len", newValue.getLen());

      return String.format("<ins>\"%s\": { %s }</ins>", newValue.getField(), sb.toString());
    }
    else
    if (type == DifferenceType.Changed) {
      StringBuilder sb = new StringBuilder();
      HTMLDiffUtils.appendNameValues(sb, ", ", "minLen", oldValue.getMinLen(), newValue.getMinLen());
      HTMLDiffUtils.appendNameValues(sb, ", ", "maxLen", oldValue.getMaxLen(), newValue.getMaxLen());
      HTMLDiffUtils.appendNameValues(sb, ", ", "len", oldValue.getLen(), newValue.getLen());

      return String.format("\"%s\": { %s }", oldValue.getField(), sb.toString());
    }
    else
    if (type == DifferenceType.Removed) {
      StringBuilder sb = new StringBuilder();
      HTMLDiffUtils.appendNameValue(sb, ", ", "minLen", oldValue.getMinLen());
      HTMLDiffUtils.appendNameValue(sb, ", ", "maxLen", oldValue.getMaxLen());
      HTMLDiffUtils.appendNameValue(sb, ", ", "len", oldValue.getLen());

      return String.format("<del>\"%s\": { %s }</del>", oldValue.getField(), sb.toString());
    }
    return "";
  }

  private static JSObject fieldToJSObject(FieldSize field) {
    JSObject jsField = new JSObject();

    if (field.getMinLen() != null) {
      jsField.put("minLen", field.getMinLen());
    }
    if (field.getMaxLen() != null) {
      jsField.put("maxLen", field.getMaxLen());
    }
    if (field.getLen() != null) {
      jsField.put("len", field.getLen());
    }
    return jsField;
  }

  @Override
  public void addTo(JSArray array) {
    JSObject obj = new JSObject();
    obj.put("scope", getScope().getName());
    obj.put("field", getField());
    if (oldValue != null) {
      obj.put("oldValue", fieldToJSObject(oldValue));
    }
    if (newValue != null) {
      obj.put("newValue", fieldToJSObject(newValue));
    }
    obj.put("diffHTML", diffHTML());
    array.add(obj);
  }
}
