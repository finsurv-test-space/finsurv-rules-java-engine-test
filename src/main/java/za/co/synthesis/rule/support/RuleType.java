package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.Scope;

import javax.print.Doc;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 3:29 PM
 * The type of validation rule
 */
public enum RuleType {
  Error,
  Warning,
  Deprecated,
  Ignore,
  Message,
  Validate,
  Document;

  public static RuleType fromString(String name) {
    RuleType result = null;
    if (name.equals("failure"))
      result = Error;
    else
    if (name.equals("warning"))
      result = Warning;
    else
    if (name.equals("deprecated"))
      result = Deprecated;
    else
    if (name.equals("ignore"))
      result = Ignore;
    else
    if (name.equals("validate"))
      result = Validate;
    else
    if (name.equals("document"))
      result = Document;
    else
    if (name.equals("message"))
      result = Message;
    return result;
  }

  @Override
  public String toString() {
    String result = null;
    if (this == Error)
      result = "failure";
    else
    if (this == Warning)
      result = "warning";
    else
    if (this == Deprecated)
      result = "deprecated";
    else
    if (this == Ignore)
      result = "ignore";
    else
    if (this == Validate)
      result = "validate";
    else
    if (this == Document)
      result = "document";
    else
    if (this == Message)
      result = "message";
    return result;
  }
}
