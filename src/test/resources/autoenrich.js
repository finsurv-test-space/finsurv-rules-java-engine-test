define([ 'require', './predef'], function (require,  predef) {
  var detailTrans, detailMoney, detailImportExport, displayFuncs;

  enrichmentFuncs = {
    cleanName: stripRegex("[\\/().,]").then(replaceRegex(/\s+/, " ")).then(replaceRegex(/\s-+\s|\s-+|-+\s/, "-")).then(trim).then(nullIfEmpty),
    cleanAddress: stripRegex("[\\/().,]").then(replaceRegex(/\s+/, " ")).then(replaceRegex(/\s-+\s|\s-+|-+\s/, "-")).then(trim).then(nullIfEmpty),
    cleanRegistrationNumber: stripRegex(/[\s\-().,]/).then(trim).then(nullIfEmpty),
    lookupBranchName: lookupFieldForKey("Branches", "code", "transaction::BranchCode", "name"),
    lookupHubName: lookupFieldForKey("Hubs", "code", "transaction::HubCode", "name")
  };

  with (predef) { with (enrichmentFuncs) {
    detailTrans = {
      ruleset: "Transaction Enrichment Rules",
      scope  : "transaction",
      fields : [
        {
          field  : "BranchName",
          display: [
            setValue(lookupBranchName, isEmpty)
          ]
        },
        {
          field  : "HubName",
          display: [
            setValue(lookupHubName, isEmpty)
          ]
        },
        {
          field  : "OriginatingCountry",
          display: [
            setValue("ZA").onOutflow().onSection("ABG")
          ]
        },
        {
          field  : "NonResident.Individual",
          display: [
            setField("Resident.Exception.ExceptionName", "MUTUAL PARTY").onInflow().onCategory(["250","251"])
          ]
        },
        {
          field  : "NonResident.Individual.Surname",
          display: [
            setValue(cleanName)
          ]
        },
        {
          field  : "NonResident.Individual.Name",
          display: [
            setValue(cleanName)
          ]
        },
        {
          field  : ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
          display: [
            // A(iv) Non Resident Rand accounts
            setField("NonResident.Individual.AccountIdentifier", "NON RESIDENT RAND",
              hasValue("NON RESIDENT RAND").and(hasTransactionField("NonResident.Individual"))).onInflow().onSection("AC"),
            setField("NonResident.Entity.AccountIdentifier", "NON RESIDENT RAND",
              hasValue("NON RESIDENT RAND").and(hasTransactionField("NonResident.Entity"))).onInflow().onSection("AC"),
            setField("Resident.Exception.ExceptionName", "NON RESIDENT RAND").onInflow().onSection("AC")
          ]
        },
        {
          field  : "NonResident.Exception.ExceptionName",
          display: [
            setValue('MUTUAL PARTY', null, isEmpty).onSection('A').onInflow().onCategory("252")
          ]
        },
        {
          field  : "NonResident.Individual.AccountIdentifier",
          display: [
            setValue("NON RESIDENT RAND", null, isEmpty).onSection("B")
          ]
        },
        {
          field:[

            "NonResident.Individual.Address.AddressLine1",
            "NonResident.Individual.Address.AddressLine2",
            "NonResident.Individual.Address.Suburb",
            "NonResident.Individual.Address.City",
            "NonResident.Individual.Address.State",
            "NonResident.Individual.Address.PostalCode",
            "NonResident.Entity.Address.AddressLine1",
            "NonResident.Entity.Address.AddressLine2",
            "NonResident.Entity.Address.Suburb",
            "NonResident.Entity.Address.City",
            "NonResident.Entity.Address.State",
            "NonResident.Entity.Address.PostalCode"
          ],
          display:[
            setValue(cleanAddress)
          ]
        },
        {
          field  : "Resident.Entity",
          display: [
            setField("NonResident.Exception.ExceptionName", "MUTUAL PARTY",
                notNonResidentFieldValue("AccountIdentifier", "RES FOREIGN BANK ACCOUNT")).onInflow().onCategory("255")
          ]
        },
        {
          field  : "Resident.Entity.EntityName",
          display: [
            setValue(cleanName)
          ]
        },
        {
          field  : "Resident.Entity.TradingName",
          display: [
            setValue(cleanName)
          ]
        },
        {
          field  : "Resident.Entity.RegistrationNumber",
          display: [
            setValue(cleanRegistrationNumber)
          ]
        },
        {
          field  : ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1", "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
          display: [
            setValue(cleanAddress)
          ]
        },
        {
          field  : ["Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2", "Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2"],
          display: [
            setValue(cleanAddress)
          ]
        },
        {
          field  : ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb", "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb"],
          display: [
            setValue(cleanAddress)
          ]
        },
        {
          field  : ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City", "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
          display: [
            setValue(cleanAddress)
          ]
        },
        {
          field  : ["Resident.Individual.ContactDetails.ContactSurname", "Resident.Entity.ContactDetails.ContactSurname", "Resident.Individual.ContactDetails.ContactName", "Resident.Entity.ContactDetails.ContactName"],
          display: [
            setValue(cleanName)
          ]
        },
        {
          field  : "Resident.Individual.DateOfBirth",
          display: [
            setValue(dateOfBirthFromSAID("Resident.Individual.IDNumber"), isEmpty.or(hasValue("1900-01-01")))
          ]
        }
      ]
    };

    detailMoney = {
      ruleset: "Money Enrichment Rules",
      scope  : "money",
      fields : [
        {
          field  : ["ThirdParty.StreetAddress.AddressLine1",
            "ThirdParty.StreetAddress.AddressLine2",
            "ThirdParty.StreetAddress.Suburb",
            "ThirdParty.StreetAddress.City"],
          display: [
            setValue(cleanAddress)
          ]
        },
        {
          field  : ["ThirdParty.PostalAddress.AddressLine1",
            "ThirdParty.PostalAddress.AddressLine2",
            "ThirdParty.PostalAddress.Suburb",
            "ThirdParty.PostalAddress.City"],
          display: [
            setValue(cleanAddress)
          ]
        },
        {
          field  : ["ThirdParty.ContactDetails.ContactSurname", "ThirdParty.ContactDetails.ContactName"],
          display: [
            setValue(cleanName)
          ]
        }
      ]
    };

    detailImportExport = {
      ruleset: "Import/Export Enrichment Rules",
      scope  : "importexport",
      fields : [
      ]
    };

  }}

  return {
    detailTrans       : detailTrans,
    detailMoney       : detailMoney,
    detailImportExport: detailImportExport
  }
});

