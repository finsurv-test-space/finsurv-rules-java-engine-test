package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;

public class drOffshoreBank implements IMatchAssertionFunction {
  @Override
  public boolean execute(MatchInfo matchInfo) {
    String BIC = matchInfo.getDrBankBIC();

    if (BIC != null) {
      return !BIC.matches(matchInfo.getContext().getOnshoreRegex());
    }
    return false;
  }
}