package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 3:06 PM
 * Determines if the current value is not a valid card type
 */
public class notValidCardType implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !context.getLookups().isValidCardType(value.toString());
  }
}

/*
function notValidCardType(context, value) {
  if (!value)
    return true;
  return !context.lookups.isValidCardType(value);
}
*/