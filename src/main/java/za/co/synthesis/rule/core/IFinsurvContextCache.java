package za.co.synthesis.rule.core;

import za.co.synthesis.rule.support.CentralCustomValidationCache.ValidationCacheEntry;
import java.util.List;

public interface IFinsurvContextCache {
    void clearCache();
    void marshalValidate(ValidationCacheEntry entry);
    List<ValidationCacheEntry> unmarshalCache();
}
