package za.co.synthesis.rule.evaluation;

import za.co.synthesis.rule.core.AccountHolderStatus;
import za.co.synthesis.rule.core.CounterpartyInstituition;
import za.co.synthesis.rule.core.ResidenceStatus;

/**
 * Created by jake on 6/28/16.
 */
public class GivenInfo {
  private Context context;
  private String counterpartyInstituitionBIC;
  private CounterpartyInstituition counterpartyInstituition;
  private AccountHolderStatus accountHolderStatus;
  private String currency;
  private String field72;
  private ResidenceStatus residenceStatus;

  public Context getContext() {
    return context;
  }

  public void setContext(Context context) {
    this.context = context;
  }

  public CounterpartyInstituition getCounterpartyInstituition() {
    return counterpartyInstituition;
  }

  public void setCounterpartyInstituition(CounterpartyInstituition counterpartyInstituition) {
    this.counterpartyInstituition = counterpartyInstituition;
  }

  public String getCounterpartyInstituitionBIC() {
    return counterpartyInstituitionBIC;
  }

  public void setCounterpartyInstituitionBIC(String counterpartyInstituitionBIC) {
    this.counterpartyInstituitionBIC = counterpartyInstituitionBIC;
  }

  public AccountHolderStatus getAccountHolderStatus() {
    return accountHolderStatus;
  }

  public void setAccountHolderStatus(AccountHolderStatus accountHolderStatus) {
    this.accountHolderStatus = accountHolderStatus;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getField72() {
    return field72;
  }

  public void setField72(String field72) {
    this.field72 = field72;
  }

  public ResidenceStatus getResidenceStatus() {
    return residenceStatus;
  }

  public void setResidenceStatus(ResidenceStatus residenceStatus) {
    this.residenceStatus = residenceStatus;
  }
}
