package za.co.synthesis.rule.support;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSFunctionCall;
import za.co.synthesis.javascript.JSIdentifier;
import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.rule.core.ValidationException;
import za.co.synthesis.rule.predef.literal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 4/9/16.
 */
public class DSLSupport {
  private static final String openMustache = "{{";
  private static final String closeMustache = "}}";

  public static class ScopedField {
    private final Scope scope;
    private final String field;

    public ScopedField(Scope scope, String field) {
      this.scope = scope;
      this.field = field;
    }

    public Scope getScope() {
      return scope;
    }

    public String getField() {
      return field;
    }
  }

  public static boolean hasMapping(String str) {
    if (str != null && str.contains(openMustache)) {
      int openPos = str.indexOf(openMustache);
      int closePos = str.indexOf(closeMustache, openPos);
      if (closePos != -1) {
        return true;
      }
    }
    return false;
  }

  public static StringList composeStringList(Object param) {
    if (param instanceof String) {
      StringList result = new StringList(1);
      result.add(param.toString());
      return result;
    }
    else
    if (param instanceof JSArray) {
      StringList result = new StringList(((JSArray)param).size());
      for (Object value : (JSArray)param) {
        result.add(value.toString());
      }
      return result;
    }
    return null;
  }

  public static StringResolverList composeStringResolverList(Object param) {
    if (param instanceof String) {
      StringResolverList result = new StringResolverList(1);
      result.add(new literal(param.toString()));
      return result;
    }
    else
    if (param instanceof JSArray) {
      StringResolverList result = new StringResolverList(((JSArray)param).size());
      for (Object value : (JSArray)param) {
        result.add(new literal(value.toString()));
      }
      return result;
    }
    return null;
  }

  public static IAssertionFunction loadApplyFunc(Map<String, String> mapping, IAssertionFunction left, JSIdentifier apply) throws Exception {
    IAssertionFunction result = left;
    if (left != null && apply != null) {
      if (apply instanceof JSFunctionCall) {
        JSFunctionCall applyFunc = (JSFunctionCall)apply;
        if (applyFunc.getName().equals("and")) {
          result = new AFAnd(left, loadAssertion(mapping, (JSIdentifier)applyFunc.getParameters().get(0)));
        }
        else
        if (applyFunc.getName().equals("or")) {
          result = new AFOr(left, loadAssertion(mapping, (JSIdentifier)applyFunc.getParameters().get(0)));
        }

        result = loadApplyFunc(mapping, result, applyFunc.getApply());
      }
    }
    return result;
  }

  public static List<String> getParamsInStr(String value) {
    List<String> result = null;
    int openPos = value != null ? value.indexOf(openMustache) : -1;
    while (openPos > -1) {
      int closePos = value.indexOf(closeMustache, openPos);
      if (closePos != -1) {
        String key = value.substring(openPos + openMustache.length(), closePos);
        if (result == null) {
          result = new ArrayList<String>();
        }
        result.add(key);
        openPos = value.indexOf(openMustache, closePos);
      }
      else
        break;
    }
    return result;
  }

  public static String applyMappingToStr(Map<String, String> mapping, String value) {
    return applyMappingToStr(mapping, value, 0);
  }

  private static String applyMappingToStr(Map<String, String> mapping, String value, int pos) {
    if (mapping != null && value != null) {
      int openPos = value.indexOf(openMustache, pos);
      if (openPos != -1) {
        int closePos = value.indexOf(closeMustache, openPos);
        if (closePos != -1) {
          String key = value.substring(openPos + openMustache.length(), closePos);
          if (mapping.containsKey(key)) {
            String newValue = value.replace(openMustache + key + closeMustache, mapping.get(key));
            return applyMappingToStr(mapping, newValue, openPos);
          } else {
            return applyMappingToStr(mapping, value, closePos + openMustache.length());
          }
        }
      }
    }
    return value;
  }

  public static String map(Map<String, String> mapping, String value) {
    if (value.contains(openMustache))
      return DSLSupport.applyMappingToStr(mapping, value);
    else
      return mapping.get(value);
  }

  /*private static boolean isMapFunction(JSIdentifier jsIdent) {
    if (jsIdent instanceof JSFunctionCall) {
      JSFunctionCall func = (JSFunctionCall)jsIdent;
      if (func.getName().equals("map")) {
        return true;
      }
    }
    return false;
  }

  private static String resolveMapCall(JSIdentifier jsIdent, Map<String, String> mapping) {
    if (jsIdent instanceof JSFunctionCall) {
      JSFunctionCall func = (JSFunctionCall)jsIdent;
      if (func.getName().equals("map") && func.getParameters().size() == 1) {
        Object param = func.getParameters().get(0);
        if (param instanceof String) {
          String value = (String)param;
          return DSLSupport.map(mapping, value);
        }
      }
    }
    return null;
  }*/

  private static IStringResolver getStringResolver(JSIdentifier jsIdent) {
    if (jsIdent instanceof JSFunctionCall) {
      JSFunctionCall func = (JSFunctionCall)jsIdent;
      if (func.getParameters().size() == 1) {
        Object param = func.getParameters().get(0);
        if (param instanceof String) {
          return StringResolverFactory.createFunctionDefault(func.getName(), (String)param, null);
        }
      }
    }
    return null;
  }

  private static IStringResolver getStringResolverFromObject(Object obj) {
    IStringResolver resolver = null;
    if (obj instanceof JSIdentifier)
      resolver = getStringResolver((JSIdentifier) obj);
    else
    if (obj == null) {
      resolver = new literal(null);
    }
    else {
      resolver = new literal(obj.toString());
    }
    return resolver;
  }

/*  private static String resolveStringParam(Map<String, String> mapping, Object param) {
    String strParam = null;
    if (param instanceof String) {
      strParam = param.toString();
    }
    else
    if (param instanceof JSIdentifier) {
      if (isMapFunction((JSIdentifier) param))
        strParam = resolveMapCall((JSIdentifier) param, mapping);
    }
    return strParam;
  }*/

  public static IAssertionFunction loadAssertion(Map<String, String> mapping, JSIdentifier identifier) throws Exception {
    IAssertionFunction result = null;
    try {
      if (identifier instanceof JSFunctionCall) {
        JSFunctionCall func = (JSFunctionCall) identifier;

        if (func.getParameters().size() == 1) {
          Object param = func.getParameters().get(0);
          if (param instanceof JSArray) {
            StringList params = composeStringList(param);
            result = ValidationFunctionFactory.createFunction(identifier.getName(), params);
          } else {
            if (param instanceof String)
              result = ValidationFunctionFactory.createFunction(identifier.getName(), (String) param);
            else if (param instanceof JSRegExLiteral)
              result = ValidationFunctionFactory.createFunction(identifier.getName(), (JSRegExLiteral) param);
            else if (param instanceof JSIdentifier && ((JSIdentifier) param).getName().equals("undefined")) {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), (String) null);
            } else if (param instanceof JSIdentifier) {
              IStringResolver resolver = getStringResolver((JSIdentifier) param);
              if (resolver != null)
                result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver);
              else
                result = ValidationFunctionFactory.createFunction(identifier.getName(), loadAssertion(mapping, (JSIdentifier) param));
            } else {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), param.toString());
            }
          }
        }

        if (func.getParameters().size() == 2) {
          Object param1 = func.getParameters().get(0);
          Object param2 = func.getParameters().get(1);
          if (param1 instanceof String) {
            if (param2 instanceof JSArray) {
              StringList params = composeStringList(param2);
              result = ValidationFunctionFactory.createFunction(identifier.getName(), (String) param1, params);
            } else if (param2 instanceof JSIdentifier) {
              IStringResolver resolver2 = getStringResolver((JSIdentifier) param2);
              if (resolver2 != null)
                result = ValidationFunctionFactory.createFunction(identifier.getName(), new literal((String) param1), resolver2);
              else
                result = ValidationFunctionFactory.createFunction(identifier.getName(), (String) param1, loadAssertion(mapping, (JSIdentifier) param2));
            } else {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), (String) param1, param2.toString());
            }
          } else if (param1 instanceof JSIdentifier) {
            IStringResolver resolver1 = getStringResolver((JSIdentifier) param1);
            if (resolver1 != null) {
              if (param2 instanceof JSArray) {
                StringResolverList params = composeStringResolverList(param2);
                result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver1, params);
              } else if (param2 instanceof JSIdentifier) {
                IStringResolver resolver2 = getStringResolver((JSIdentifier) param2);
                if (resolver2 != null)
                  result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver1, resolver2);
                else
                  result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver1, loadAssertion(mapping, (JSIdentifier) param2));
              } else {
                result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver1, new literal(param2.toString()));
              }
            }
          }
        }

        if (func.getParameters().size() == 3) {
          Object param1 = func.getParameters().get(0);
          Object param2 = func.getParameters().get(1);
          Object param3 = func.getParameters().get(2);
          if (param1 instanceof JSIdentifier || param2 instanceof JSIdentifier) {
            IStringResolver resolver1 = getStringResolverFromObject(param1);
            IStringResolver resolver2 = getStringResolverFromObject(param2);
            if (param3 instanceof JSIdentifier) {
              IStringResolver resolver3 = getStringResolverFromObject(param3);
              result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver1, resolver2, resolver3);
            } else if (param3 instanceof String) {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver1, resolver2, new literal((String) param3));
            } else if (param3 instanceof Boolean) {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver1, resolver2, (Boolean) param3);
            }
          } else {
            if (param3 instanceof JSIdentifier) {
              IStringResolver resolver3 = getStringResolverFromObject(param3);
              result = ValidationFunctionFactory.createFunction(identifier.getName(), new literal((String) param1), new literal((String) param2), resolver3);
            } else if (param3 instanceof String) {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), (String) param1, (String) param2, (String) param3);
            } else if (param3 instanceof Boolean) {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), (String) param1, (String) param2, (Boolean) param3);
            }
          }
        }


        if (func.getParameters().size() == 4) {
          Object param1 = func.getParameters().get(0);
          Object param2 = func.getParameters().get(1);
          Object param3 = func.getParameters().get(2);
          Object param4 = func.getParameters().get(3);
          if (param1 instanceof JSIdentifier || param2 instanceof JSIdentifier || param3 instanceof JSIdentifier) {
            IStringResolver resolver1 = getStringResolverFromObject(param1);
            IStringResolver resolver2 = getStringResolverFromObject(param2);
            IStringResolver resolver3 = getStringResolverFromObject(param3);

            if (param4 instanceof JSIdentifier) {
              IStringResolver resolver4 = getStringResolverFromObject(param4);
              result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver1, resolver2, resolver3, resolver4);
            } else if (param4 instanceof String) {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver1, resolver2, resolver3, new literal((String) param4));
            } else if (param4 instanceof Boolean) {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), resolver1, resolver2, resolver3, (Boolean) param4);
            }
          } else {
            if (param4 instanceof JSIdentifier) {
              IStringResolver resolver4 = getStringResolverFromObject(param4);
              result = ValidationFunctionFactory.createFunction(identifier.getName(), new literal((String) param1), new literal((String) param2), new literal((String) param3), resolver4);
            } else if (param4 instanceof String) {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), (String) param1, (String) param2, (String) param3, (String) param4);
            } else if (param4 instanceof Boolean) {
              result = ValidationFunctionFactory.createFunction(identifier.getName(), (String) param1, (String) param2, (String) param3, (Boolean) param4);
            }
          }
        }
      } else {
        result = ValidationFunctionFactory.createFunction(identifier.getName());
      }

      result = loadApplyFunc(mapping, result, identifier.getApply());
      if (result == null) {
        throw new ValidationException("Cannot resolve function " + identifier.getName());
      }
    } catch (ClassNotFoundException e) {
      throw new ValidationException("The function '" + identifier.getName() + "' in not defined", e);
    }
    return result;
  }

  public static IAssertionFunction getAssertion(Map<String, String> mapping, JSFunctionCall jsFunc, int paramIndex) throws Exception {
    if (jsFunc.getParameters().size() > paramIndex) {
      Object objExp = jsFunc.getParameters().get(paramIndex);
      if (objExp instanceof JSIdentifier) {
        return loadAssertion(mapping, (JSIdentifier)objExp);
      }
    }
    return null;
  }

  public static void applyFilters(Rule rule, JSIdentifier jsId) throws Exception {
    if (jsId != null && jsId instanceof JSFunctionCall) {
      JSFunctionCall jsFunc = (JSFunctionCall)jsId;

      if (jsFunc.getName().equals("onSection") && jsFunc.getParameters().size() == 1) {
        rule.onSection(jsFunc.getParameters().get(0).toString());
      }
      else
      if (jsFunc.getName().equals("onInflow") && jsFunc.getParameters().size() == 0) {
        rule.onInflow();
      }
      else
      if (jsFunc.getName().equals("onOutflow") && jsFunc.getParameters().size() == 0) {
        rule.onOutflow();
      }
      else
      if (jsFunc.getName().equals("onCategory") && jsFunc.getParameters().size() == 1) {
        StringList params = composeStringList(jsFunc.getParameters().get(0));
        if (params != null) {
          rule.onCategory(params);
        }
      }
      else
      if (jsFunc.getName().equals("notOnCategory") && jsFunc.getParameters().size() == 1) {
        StringList params = composeStringList(jsFunc.getParameters().get(0));
        if (params != null) {
          rule.notOnCategory(params);
        }
      }

      applyFilters(rule, jsFunc.getApply());
    }
  }

  public static void applyFilters(DisplayRule rule, JSIdentifier jsId) throws Exception {
    if (jsId != null && jsId instanceof JSFunctionCall) {
      JSFunctionCall jsFunc = (JSFunctionCall)jsId;

      if (jsFunc.getName().equals("onSection") && jsFunc.getParameters().size() == 1) {
        rule.onSection(jsFunc.getParameters().get(0).toString());
      }
      else
      if (jsFunc.getName().equals("onInflow") && jsFunc.getParameters().size() == 0) {
        rule.onInflow();
      }
      else
      if (jsFunc.getName().equals("onOutflow") && jsFunc.getParameters().size() == 0) {
        rule.onOutflow();
      }
      else
      if (jsFunc.getName().equals("onCategory") && jsFunc.getParameters().size() == 1) {
        StringList params = composeStringList(jsFunc.getParameters().get(0));
        if (params != null) {
          rule.onCategory(params);
        }
      }
      else
      if (jsFunc.getName().equals("notOnCategory") && jsFunc.getParameters().size() == 1) {
        StringList params = composeStringList(jsFunc.getParameters().get(0));
        if (params != null) {
          rule.notOnCategory(params);
        }
      }

      applyFilters(rule, jsFunc.getApply());
    }
  }

  public static boolean categoryNotInList(String category, String mainCategory, List<String> ruleList) {
    boolean result = true;
    for (String ruleItem : ruleList) {
      int pos = ruleItem.indexOf('/');
      if (pos == -1) {
        if (ruleItem.equals(mainCategory)) {
          result = false;
          break;
        }
      } else {
        if (ruleItem.equals(category)) {
          result = false;
          break;
        }
      }
    }
    return result;
  }

  public static boolean categoriesNotInList(List<String> categories, List<String> mainCategories, List<String> ruleList) {
    for (int i=0; i<categories.size(); i++) {
      if (categoryNotInList(categories.get(i), mainCategories.get(i), ruleList))
        return true;
    }
    return false;
  }

  public static ScopedField parseCompoundField(Scope defaultScope, String compoundField) {
    Scope targetScope = defaultScope;
    String targetField = compoundField;
    int posScope = compoundField.indexOf("::");
    if (posScope > -1) {
      String scopeStr = compoundField.substring(0, posScope);
      if (scopeStr.equals("transaction"))
        targetScope = Scope.Transaction;
      else
      if (scopeStr.equals("money"))
        targetScope = Scope.Money;
      else
      if (scopeStr.equals("importexport"))
        targetScope = Scope.ImportExport;
      targetField = compoundField.substring(posScope + 2);
    }

    return new ScopedField(targetScope, targetField);
  }

  public static Object lookupScopedFieldValue(FinsurvContext context, String field) {
    int moneyInstance = context.getCurrentMoneyInstance();
    int ieInstance = context.getCurrentImportExportInstance();
    Object lookupValue = null;

    ScopedField scopedField = parseCompoundField(context.getCurrentScope(), field);

    if (scopedField.getScope() == Scope.Transaction) {
      lookupValue = context.getTransactionField(scopedField.getField());
    }
    if (scopedField.getScope() == Scope.Money) {
      if (moneyInstance == -1) {
        moneyInstance = 0;
      }
      lookupValue = context.getMoneyField(moneyInstance, scopedField.getField());
    }
    if (scopedField.getScope() == Scope.ImportExport) {
      if (moneyInstance == -1) {
        moneyInstance = 0;
      }
      if (ieInstance == -1) {
        ieInstance = 0;
      }
      lookupValue = context.getImportExportField(moneyInstance, ieInstance, scopedField.getField());
    }
    return lookupValue;
  }

  public static Object[] lookupScopedFieldValues(FinsurvContext context, String[] fieldList) {
    Object[] result = new Object[fieldList.length];
    for (int i = 0; i < fieldList.length; i++) {
      result[i] = lookupScopedFieldValue(context, fieldList[i]);
    }
    return result;
  }
}
