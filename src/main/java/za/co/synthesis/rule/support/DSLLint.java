package za.co.synthesis.rule.support;

import java.util.ArrayList;

public class DSLLint {
  public enum Type {
    Error,
    Warning
  }
  public static class List extends ArrayList<DSLLint> {}

  private Type type;
  private String message;

  public static DSLLint createError(String message) {
    return new DSLLint().setType(Type.Error).setMessage(message);
  }

  public static DSLLint createWarning(String message) {
    return new DSLLint().setType(Type.Warning).setMessage(message);
  }

  public Type getType() {
    return type;
  }

  public DSLLint setType(Type type) {
    this.type = type;
    return this;
  }

  public String getMessage() {
    return message;
  }

  public DSLLint setMessage(String message) {
    this.message = message;
    return this;
  }
}
