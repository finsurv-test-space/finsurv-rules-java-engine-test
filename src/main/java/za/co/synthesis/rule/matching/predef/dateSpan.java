package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.core.IMatchingContext;
import za.co.synthesis.rule.matching.IShardFunction;
import za.co.synthesis.rule.support.Util;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
import za.co.synthesis.rule.support.legacydate.temporal.ChronoUnit;
#else*/
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
//#endif

/**
 * Created by jake on 6/16/16.
 */
public class dateSpan implements IShardFunction {
  private final String fieldname;
  private final double span;
  private final LocalDate baseDate = LocalDate.of(2000,1,1);

  public dateSpan(String fieldname, String span) {
    this.fieldname = fieldname;
    this.span = Double.parseDouble(span);
  }

  @Override
  public String[] execute(IMatchingContext context) {
    String[] result = new String[2];
    LocalDate val = Util.date(context.getField(fieldname));

    double days = ChronoUnit.DAYS.between(baseDate,val);
    long bucket = Math.round((days-span/2) / span);
    result[0] = Double.toString(bucket);
    result[1] = Double.toString(bucket+1);
    return result;
  }
}
