package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;

public class returnFalse implements IMatchAssertionFunction {
  @Override
  public boolean execute(MatchInfo matchInfo) {
    return false;
  }
}
