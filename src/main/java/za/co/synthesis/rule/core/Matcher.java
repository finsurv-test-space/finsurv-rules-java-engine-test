package za.co.synthesis.rule.core;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.impl.JSMatchingContext;
import za.co.synthesis.rule.matching.*;

import java.util.List;

/**
 * Created by jake on 6/16/16.
 */
public class Matcher {
  private final List<MatchConfidence> rules;

  public Matcher(List<MatchConfidence> rules) {
    this.rules = rules;
  }

  private void runMatching(MatchingContext context, List<MatchConfidence> rules, String rulename) {
    for (MatchConfidence rule : rules) {
      if (rulename == null || rulename.equals(rule.getName())) {
        IMatchingFunction assertion = rule.getAssertion();
        if (assertion != null && assertion.execute(context)) {
          IConfidenceFunction confidence = rule.getConfidence();
          if (confidence != null) {
            double value = confidence.execute(context);
            if (value > 0.01) {
              context.logMatch(value, rule.getName());
            }
          }
        }
      }
    }
  }

  public List<MatchingResult> matchRule(String rulename, JSObject jsObjLhs, JSObject jsObjRhs) {
    JSMatchingContext jsContextLhs = new JSMatchingContext(jsObjLhs);
    JSMatchingContext jsContexRLhs = new JSMatchingContext(jsObjRhs);
    MatchingContext context = new MatchingContext(jsContextLhs, jsContexRLhs);

    runMatching(context, rules, rulename);
    return context.getMatchingResultList();
  }

  public List<MatchingResult> match(JSObject jsObjLhs, JSObject jsObjRhs) {
    JSMatchingContext jsContextLhs = new JSMatchingContext(jsObjLhs);
    JSMatchingContext jsContexRLhs = new JSMatchingContext(jsObjRhs);
    MatchingContext context = new MatchingContext(jsContextLhs, jsContexRLhs);

    runMatching(context, rules, null);
    return context.getMatchingResultList();
  }

  public List<MatchingResult> matchRule(String rulename, IMatchingContext ctxLhs, IMatchingContext ctxRhs) {
    MatchingContext context = new MatchingContext(ctxLhs, ctxRhs);

    runMatching(context, rules, rulename);
    return context.getMatchingResultList();
  }

  public List<MatchingResult> match(IMatchingContext ctxLhs, IMatchingContext ctxRhs) {
    MatchingContext context = new MatchingContext(ctxLhs, ctxRhs);

    runMatching(context, rules, null);
    return context.getMatchingResultList();
  }
}
