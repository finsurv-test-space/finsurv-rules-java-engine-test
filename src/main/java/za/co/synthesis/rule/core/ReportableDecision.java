package za.co.synthesis.rule.core;

public enum ReportableDecision {
  Unknown,
  Illegal,
  DoNotReport,
  ReportToRegulator
}
