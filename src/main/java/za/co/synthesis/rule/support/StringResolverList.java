package za.co.synthesis.rule.support;

import java.util.ArrayList;
import java.util.Collection;

public class StringResolverList extends ArrayList<IStringResolver> {
  public StringResolverList(int i) {
    super(i);
  }

  public StringResolverList() {
  }

  public StringResolverList(Collection<? extends IStringResolver> strings) {
    super(strings);
  }
}