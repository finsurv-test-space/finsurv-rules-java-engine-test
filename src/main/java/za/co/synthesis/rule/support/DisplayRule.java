package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.FlowType;

import java.util.List;

/**
 * Created by jake on 4/9/16.
 */
public class DisplayRule {
  private final DisplayRuleType type;
  private final String field;
  private final List<String> values;
  private final IDisplayFunction displayFunction;
  private final IAssertionFunction assertion;
  private FlowType flow;
  private String section;
  private List<String> categories;
  private List<String> notCategories;

  public DisplayRule(final DisplayRuleType type, final String field,
                     final List<String> values, final IDisplayFunction displayFunction,
                     final IAssertionFunction assertion) {
    this.type = type;
    this.field = field;
    this.values = values;
    this.displayFunction = displayFunction;
    this.assertion = assertion;
  }

  public DisplayRule onInflow() {
    flow = FlowType.Inflow;
    return this;
  }

  public DisplayRule onOutflow() {
    flow = FlowType.Outflow;
    return this;
  }

  public DisplayRule onSection(final String sections) {
    section = sections;
    return this;
  }

  public DisplayRule onCategory(final List<String> categories) {
    this.categories = categories;
    return this;
  }

  public DisplayRule notOnCategory(final List<String> categories) {
    this.notCategories = categories;
    return this;
  }

  public DisplayRuleType getType() {
    return type;
  }

  public String getField() {
    return field;
  }

  public List<String> getValues() {
    return values;
  }

  public IDisplayFunction getDisplayFunction() {
    return displayFunction;
  }

  public IAssertionFunction getAssertion() {
    return assertion;
  }

  public FlowType getFlow() {
    return flow;
  }

  public String getSection() {
    return section;
  }

  public List<String> getCategories() {
    return categories;
  }

  public List<String> getNotCategories() {
    return notCategories;
  }
}
