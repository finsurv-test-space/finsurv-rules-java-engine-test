package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 12:53 PM
 * Determines if the import/export field has not been provided or if it is has is it a string that only contains
 * non-whitespace characters
 */
public class notImportExportField implements IAssertionFunction {
  private String field;

  public notImportExportField(String field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getImportExportField(context.getCurrentMoneyInstance(), context.getCurrentImportExportInstance(), field);
    if (fieldValue == null || fieldValue instanceof IFinsurvContext.Undefined ||
         (fieldValue instanceof String && ((String)fieldValue).trim().length() == 0))
      return true;
    else
      return false;
  }
}
/*
function notImportExportField(field) {
  return function(context, value) {
    var fieldValue = context.getImportExportField(context.currentMoneyInstance, context.currentImportExportInstance, field);
    if (!fieldValue || (typeof fieldValue == "string" && fieldValue.trim().length == 0))
      return true;
    else
      return false;
  }
}
*/


