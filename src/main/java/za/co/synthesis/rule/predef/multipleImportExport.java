package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 1:04 PM
 * Determine if there are multiple import/export entries for the current monetary amount
 */
public class multipleImportExport implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return context.getImportExportSize(context.getCurrentMoneyInstance()) > 1;
  }
}
/*
function multipleImportExport(context, value) {
  return context.getImportExportSize(context.currentMoneyInstance) > 1;
}
*/

