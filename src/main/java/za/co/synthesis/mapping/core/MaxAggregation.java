package za.co.synthesis.mapping.core;

import java.util.List;


/**
 * User: jake
 * Date: 3/21/16
 * Time: 11:58 AM
 * Find the maximum value in the set of values
 */
public class MaxAggregation implements IAggregation {
  @Override
  public String aggregate(List<String> values) {
    if (values.size() > 0) {
      String maxStrValue = null;
      try {
        double maxValue = 0.0D;
        for (String valStr : values) {
          if (valStr.length() > 0) {
            double valNum = Double.parseDouble(valStr);
            if (maxStrValue == null || valNum > maxValue) {
              maxValue = valNum;
              maxStrValue = valStr;
            }
          }
        }
      }
      catch (NumberFormatException e) {
        maxStrValue = values.get(0);
        for (String valStr : values) {
          if (valStr.compareTo(maxStrValue) > 0) {
            maxStrValue = valStr;
          }
        }
      }
      return maxStrValue;
    }
    return null;
  }
}

