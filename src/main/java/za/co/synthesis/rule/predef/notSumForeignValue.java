package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 8:27 AM
 * Determines if the sum of money amount is different to the foreign value
 */
public class notSumForeignValue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    BigDecimal total = new BigDecimal(0);
    BigDecimal sumValue = Util.number(context.getTransactionField("TotalForeignValue"));
    for (int i = 0; i < context.getMoneySize(); i++) {
      BigDecimal fvalue = Util.number(context.getMoneyField(i, "ForeignValue"));
      if (fvalue != null)
        total = total.add(fvalue);
    }
    if (sumValue != null)
      return sumValue.compareTo(total) != 0;
    return true;
  }
}
/*
function notSumForeignValue(context, value) {
  var total = 0;
  var sumValue = context.getTransactionField("TotalForeignValue");
  for ( var i = 0; i < context.getMoneySize(); i++) {
    var fvalue = context.getMoneyField(i, "ForeignValue");
    if (fvalue)
      total += Number(fvalue);
  }
  var a = Math.round(Number(sumValue) * 100);
  var b = Math.round(total * 100);
  return a != b;
}
*/