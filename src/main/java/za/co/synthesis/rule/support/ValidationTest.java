package za.co.synthesis.rule.support;

import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.JSCustomValues;
import za.co.synthesis.rule.doc.RuleTest;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationTest {
  public static enum TestClass {
    Positive,
    Negative
  };

  public static class TestContext {
    private Test_Validate test_validate = new Test_Validate();
    private Map<String, List<RuleTestResult>> testMap = new HashMap<String, List<RuleTestResult>>();
    private List<RuleTestResult> testList = new ArrayList<RuleTestResult>();
    private int testCount = 0;
    private int errorCount = 0;

    public int getTestCount() {
      return testCount;
    }

    public int getErrorCount() {
      return errorCount;
    }

    public List<RuleTestResult> getTestResults() {
      return testList;
    }
  };

  public static class RuleTestResult {
    private final String fullRuleName;
    private final int fieldIndex;
    private final int instance;
    private final TestClass testClass;
    private final String testDSL;
    private final List<String> errorList = new ArrayList<String>();

    public RuleTestResult(String fullRuleName, int fieldIndex, int instance, TestClass testClass, String testDSL) {
      this.fullRuleName = fullRuleName;
      this.fieldIndex = fieldIndex;
      this.instance = instance;
      this.testClass = testClass;
      this.testDSL = testDSL;
    }

    public String getFullRuleName() {
      return fullRuleName;
    }

    public int getFieldIndex() {
      return fieldIndex;
    }

    public int getInstance() {
      return instance;
    }

    public List<String> getErrorList() {
      return errorList;
    }

    public void addError(String error) {
      if (error.contains("{{rule}}")) {
        String composeError = error.replace("{{rule}}", fullRuleName + '#' + instance);
        errorList.add(composeError);
      }
      else {
        errorList.add(error);
      }
    }

    public TestClass getTestClass() {
      return testClass;
    }

    public String getTestDSL() {
      return testDSL;
    }
  }

  private static class Test_Validate implements ICustomValidate {
    private Boolean wasRun = false;

    public void resetRun() {
      wasRun = false;
    }

    public Boolean getWasRun() {
      return wasRun;
    }

    @Override
    public CustomValidateResult call(Object value, Object... otherInputs) {
      this.wasRun = true;
      return new CustomValidateResult(StatusType.Pass);
    }
  }

  private TestContext defaultTestContext = new TestContext();
  long startLoad;
  long endLoad;
  long startRun;
  long endRun;

/*  private Test_Validate test_validate = new Test_Validate();
  private Map<String, List<RuleTestResult>> testMap = new HashMap<String, List<RuleTestResult>>();
  private int testCount = 0;
  private int errorCount = 0;
 */

  private static void assertRuleBase(TestContext testContext, Validator validator, JSObject jsObj, JSObject jsCustomData,
                              ResultType expectType, int number, boolean mustMatchRule,
                              RuleTestResult testResult) {
    //reset the test validator
    testContext.test_validate.resetRun();
    testContext.testCount++;

//    if (testResult.getFullRuleName().equals("ThirdParty.Entity.Name.maxLen") && testResult.getInstance() == 1) {
//      System.out.println("");
//    }
    ValidationStats stats = new ValidationStats();
    List<ResultEntry> fullResults = validator.validateRule(testResult.getFullRuleName(), jsObj, new JSCustomValues(jsCustomData), stats);
    List<ResultEntry> results = new ArrayList<ResultEntry>();
    for (ResultEntry entry : fullResults) {
      if (entry.getType() != ResultType.Mandatory)
        results.add(entry);
    }

    boolean rulesFound = stats.getRunRules() > 0;
    if (!rulesFound && mustMatchRule) {
      testContext.errorCount++;
      testResult.addError("Failed Test: {{rule}} - No matching rule found");
    }
    else {
      if (expectType == null) {
        if (results.size() > 0) {
          testContext.errorCount++;
          testResult.addError("Failed Test: {{rule}} - Expecting Success, but raised " + results.size() + ": " + results.get(0).getType().toString());
        }
      }
      else {
        if (expectType==ResultType.Validate || expectType==ResultType.NoValidate) {
          if (expectType==ResultType.Validate && !(testContext.test_validate.getWasRun() || stats.getValidationCacheHits() > 0)) {
            testContext.errorCount++;
            testResult.addError("Failed Test: {{rule}} - Expecting " + expectType.toString() + ", but did not validate!");
          } else if(expectType==ResultType.NoValidate && (testContext.test_validate.getWasRun() || stats.getValidationCacheHits() > 0)) {
            testContext.errorCount++;
            testResult.addError("Failed Test: {{rule}} - Expecting " + expectType.toString() + ", but validated!");
          }
        } else if (results.size() == 0 || results.get(0).getType() != expectType || results.size() != number) {
          testContext.errorCount++;
          if (results.size() == 0)
            testResult.addError("Failed Test: {{rule}} - Expecting " + expectType.toString() + ", but instead succeeded");
          else
          if (results.get(0).getType() != expectType)
            testResult.addError("Failed Test: {{rule}} - Expecting " + expectType.toString() + ", but instead raised: " + results.get(0).getType().toString());
          else
          if (results.size() != number)
            testResult.addError("Failed Test: {{rule}} - Expecting " + number + 'x' + expectType.toString() + ", but only raised " + results.size());
        }
      }
    }
  }

  private static void assertFailure(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj) {
    JSObject customData = new JSObject();
    customData.put("DealerType", "AD");
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.Error, 1, true, ruleTest);
  }

  private static void assert2Failures(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj) {
    JSObject customData = new JSObject();
    customData.put("DealerType", "AD");
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.Error, 2, true, ruleTest);
  }

  private static void assertWarning(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj) {
    JSObject customData = new JSObject();
    customData.put("DealerType", "AD");
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.Warning, 1, true, ruleTest);
  }

  private static void assertDeprecated(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj) {
    JSObject customData = new JSObject();
    customData.put("DealerType", "AD");
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.Deprecated, 1, true, ruleTest);
  }

  private static void assertSuccess(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj) {
    JSObject customData = new JSObject();
    customData.put("DealerType", "AD");
    assertRuleBase(testContext, validator, jsObj, customData, null, 1, true, ruleTest);
  }

  private static void assertNoRule(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj) {
    JSObject customData = new JSObject();
    customData.put("DealerType", "AD");
    assertRuleBase(testContext, validator, jsObj, customData, null, 1, false, ruleTest);
  }

  private static void assertFailureCustom(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj, JSObject customData) {
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.Error, 1, true, ruleTest);
  }

  private static void assert2FailuresCustom(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj, JSObject customData) {
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.Error, 2, true, ruleTest);
  }

  private static void assertWarningCustom(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj, JSObject customData) {
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.Warning, 1, true, ruleTest);
  }

  private static void assertDeprecatedCustom(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj, JSObject customData) {
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.Deprecated, 1, true, ruleTest);
  }

  private static void assertSuccessCustom(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj, JSObject customData) {
    assertRuleBase(testContext, validator, jsObj, customData, null, 1, true, ruleTest);
  }

  private static void assertNoRuleCustom(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj, JSObject customData) {
    assertRuleBase(testContext, validator, jsObj, customData, null, 1, false, ruleTest);
  }

  private static void assertValidation(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj){
    JSObject customData = new JSObject();
    customData.put("DealerType", "AD");
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.Validate, 1, true, ruleTest);
  }

  private static void assertNoValidation(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj){
    JSObject customData = new JSObject();
    customData.put("DealerType", "AD");
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.NoValidate, 1, false, ruleTest);
  }

  private static void assertCustomValidation(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj, JSObject customData){
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.Validate, 1, true, ruleTest);
  }

  private static void assertCustomNoValidation(TestContext testContext, Validator validator, RuleTestResult ruleTest, JSObject jsObj, JSObject customData){
    assertRuleBase(testContext, validator, jsObj, customData, ResultType.NoValidate, 1, false, ruleTest);
  }

  private static RuleTestResult addNewRuleTest(TestContext testContext, String fullRulename, String funcName, String testDSL) {
    TestClass testClass = (funcName.contains("Success") ? TestClass.Positive : TestClass.Negative);

    String ruleName = fullRulename;
    int fieldIndex = 0;
    if (fullRulename.contains(":")) {
      int pos = fullRulename.lastIndexOf(':');
      if (pos != -1) {
        ruleName = fullRulename.substring(0, pos);
        fieldIndex = Integer.parseInt(fullRulename.substring(pos+1)) - 1;
      }
    }

    if (testContext.testMap.containsKey(ruleName))
    {
      List<RuleTestResult> list = testContext.testMap.get(ruleName);
      RuleTestResult newResult = new RuleTestResult(fullRulename, fieldIndex, list.size()+1, testClass, testDSL);
      list.add(newResult);
      testContext.testList.add(newResult);
      return newResult;
    }
    else {
      List<RuleTestResult> list = new ArrayList<RuleTestResult>();
      RuleTestResult newResult = new RuleTestResult(fullRulename, fieldIndex, 1, testClass, testDSL);
      list.add(newResult);
      testContext.testMap.put(ruleName, list);
      testContext.testList.add(newResult);
      return newResult;
    }
  }

  public static boolean applyAssertionFunction(TestContext testContext, Validator validator, JSFunctionCall jsFunc) {
    String funcName = jsFunc.getName();
    String rulename = jsFunc.getParameters().get(0).toString();

    JSWriter writer = new JSWriter();
    jsFunc.compose(writer);
    RuleTestResult ruleTest = addNewRuleTest(testContext, rulename, funcName, writer.toString());

    if (funcName.equals("assertFailure") && jsFunc.getParameters().size() == 2) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      assertFailure(testContext, validator, ruleTest, jsObj);
    }
    else
    if (funcName.equals("assert2Failures") && jsFunc.getParameters().size() == 2) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      assert2Failures(testContext, validator, ruleTest, jsObj);
    }
    else
    if (funcName.equals("assertWarning") && jsFunc.getParameters().size() == 2) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      assertWarning(testContext, validator, ruleTest, jsObj);
    }
    else
    if (funcName.equals("assertDeprecated") && jsFunc.getParameters().size() == 2) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      assertDeprecated(testContext, validator, ruleTest, jsObj);
    }
    else
    if (funcName.equals("assertNoRule") && jsFunc.getParameters().size() == 2) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      assertNoRule(testContext, validator, ruleTest, jsObj);
    }
    else
    if (funcName.equals("assertSuccess") && jsFunc.getParameters().size() == 2) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      assertSuccess(testContext, validator, ruleTest, jsObj);
    }
    else
    if (funcName.equals("assertFailureCustom") && jsFunc.getParameters().size() == 3) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
      assertFailureCustom(testContext, validator, ruleTest, jsObj, jsCustom);
    }
    else
    if (funcName.equals("assert2FailuresCustom") && jsFunc.getParameters().size() == 3) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
      assert2FailuresCustom(testContext, validator, ruleTest, jsObj, jsCustom);
    }
    else
    if (funcName.equals("assertWarningCustom") && jsFunc.getParameters().size() == 3) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
      assertWarningCustom(testContext, validator, ruleTest, jsObj, jsCustom);
    }
    else
    if (funcName.equals("assertDeprecatedCustom") && jsFunc.getParameters().size() == 3) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
      assertDeprecatedCustom(testContext, validator, ruleTest, jsObj, jsCustom);
    }
    else
    if (funcName.equals("assertSuccessCustom") && jsFunc.getParameters().size() == 3) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
      assertSuccessCustom(testContext, validator, ruleTest, jsObj, jsCustom);
    }
    else
    if (funcName.equals("assertNoRuleCustom") && jsFunc.getParameters().size() == 3) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
      assertNoRuleCustom(testContext, validator, ruleTest, jsObj, jsCustom);
    }
    else
    if (funcName.equals("assertValidation") && jsFunc.getParameters().size() == 2) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
//      JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
      assertValidation(testContext, validator, ruleTest, jsObj);
    }
    else
    if (funcName.equals("assertNoValidation") && jsFunc.getParameters().size() == 2) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
//      JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
      assertNoValidation(testContext, validator, ruleTest, jsObj);
    }
    else
    if (funcName.equals("assertCustomValidation") && jsFunc.getParameters().size() == 3) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
      assertCustomValidation(testContext, validator, ruleTest, jsObj, jsCustom);
    }
    else
    if (funcName.equals("assertCustomNoValidation") && jsFunc.getParameters().size() == 3) {
      JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
      JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
      assertCustomNoValidation(testContext, validator, ruleTest, jsObj, jsCustom);
    }
    else {
      ruleTest.addError("Unknown assertion function '" + jsFunc.getName() +
              "' with " + jsFunc.getParameters().size() + " parameters");
      testContext.testCount++;
      testContext.errorCount++;
      return false;
    }
    return true;
  }

  private static JSFunctionCall composeAssertionFunction(RuleTest ruleTest) throws Exception {
    JSArray params = new JSArray();
    params.add(ruleTest.getRuleName());
    JSStructureParser parserReportData = new JSStructureParser(ruleTest.getReportData());
    params.add(parserReportData.parse());
    if (ruleTest.getCustomData() != null && ruleTest.getCustomData().length() > 0) {
      JSStructureParser parserCustomData = new JSStructureParser(ruleTest.getCustomData());
      params.add(parserCustomData.parse());
    }
    return new JSFunctionCall(ruleTest.getAssertFunc(), params);
  }

  public static ValidationTest createValidationTest(ValidationEngine engine) throws Exception {
    ValidationTest result = new ValidationTest();

    result.startLoad = System.currentTimeMillis();
    FileReader fileReader = new FileReader(engine.getPackagePath() + "/tests/testCases.js");
    JSStructureParser parser = new JSStructureParser(fileReader);
    Object obj = parser.parse();

    // Load specific test mappings if they have been provided as part of the testCases
    RulePackage rulePackage = engine.getLoadedPackage();
    if (rulePackage != null) {
      JSObject jsResult = JSUtil.findObjectWith(obj, "mappings");
      if (jsResult != null) {
        Object objMappings = jsResult.get("mappings");
        if (objMappings instanceof JSObject) {
          for (Map.Entry<String, Object> entry : ((JSObject) objMappings).entrySet()) {
            rulePackage.getMapping().put(entry.getKey(), entry.getValue() != null ? entry.getValue().toString() : null);
          }
        }
      }
    }

    for (String validateName : engine.getCustomValidateList()) {
      engine.registerCustomValidate(validateName, result.defaultTestContext.test_validate);
    }
    result.endLoad = System.currentTimeMillis();

    result.startRun = System.currentTimeMillis();
    // Iterate over the testCases
    JSObject testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
    if (testCaseContainer != null) {
      Validator validator = engine.issueValidator();

      JSArray cases = (JSArray) testCaseContainer.get("test_cases");

      for (Object objCase : cases) {
        if (objCase instanceof JSFunctionCall) {
          JSFunctionCall jsFunc = (JSFunctionCall) objCase;

          applyAssertionFunction(result.defaultTestContext, validator, jsFunc);
        }
      }
    }
    result.endRun = System.currentTimeMillis();
    return result;
  }

  public List<RuleTestResult> getResultsByRulename(String ruleName) {
    return defaultTestContext.testMap.get(ruleName);
  }

  public RuleTestResult runTestFunction(ValidationEngine engine, JSFunctionCall jsFunctionCall) throws ValidationException {
    TestContext testContext = new TestContext();
    Validator validator = engine.issueValidator();
    applyAssertionFunction(testContext, validator, jsFunctionCall);

    return testContext.testList.get(0);
  }


  public RuleTestResult runRuleTest(ValidationEngine engine, RuleTest ruleTest) throws ValidationException {
    TestContext testContext = new TestContext();
    Validator validator = engine.issueValidator();
    try {
      applyAssertionFunction(testContext, validator, composeAssertionFunction(ruleTest));
    } catch (Exception e) {
      throw new ValidationException("Cannot compose Assertion Function", e);
    }

    return testContext.testList.get(0);
  }

  public List<RuleTestResult> runTestFunctions(ValidationEngine engine, List<JSFunctionCall> jsFunctionCalls) throws ValidationException {
    TestContext testContext = new TestContext();
    Validator validator = engine.issueValidator();
    for (JSFunctionCall jsFunctionCall : jsFunctionCalls) {
      applyAssertionFunction(testContext, validator, jsFunctionCall);
    }
    return testContext.testList;
  }

  public List<RuleTestResult> runRuleTests(ValidationEngine engine, List<RuleTest> ruleTests) throws ValidationException {
    TestContext testContext = new TestContext();
    Validator validator = engine.issueValidator();
    for (RuleTest ruleTest : ruleTests) {
      try {
        applyAssertionFunction(testContext, validator, composeAssertionFunction(ruleTest));
      } catch (Exception e) {
        throw new ValidationException("Cannot compose Assertion Function", e);
      }
    }
    return testContext.testList;
  }

  public int getTestCount() {
    return defaultTestContext.testCount;
  }

  public int getErrorCount() {
    return defaultTestContext.errorCount;
  }

  public TestContext getDefaultTestContext() {
    return defaultTestContext;
  }

  public long getStartLoad() {
    return startLoad;
  }

  public long getEndLoad() {
    return endLoad;
  }

  public long getStartRun() {
    return startRun;
  }

  public long getEndRun() {
    return endRun;
  }
}

