package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 5:05 AM
 * Checks to see if associated string is too short
 */
public class isTooShort implements IAssertionFunction {
  private int len;

  public isTooShort(int len) {
    this.len = len;
  }

  public isTooShort(String strLen) {
    this.len = Integer.parseInt(strLen);
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (value != null && value instanceof String && ((String) value).length() > 0) {
      return ((String)value).trim().length() < len;
    }
    return false;
  }
}
/*
function isTooShort(constant) {
  return function(context, value) {
    if (value && typeof value === "string")
      return value.trim().length < constant;
    return false;
  };
}
*/
