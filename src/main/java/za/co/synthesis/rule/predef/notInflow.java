package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.core.FlowType;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 5:30 AM
 * Determines if the transaction is not an inflow
 */
public class notInflow implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return (context.getFlow() == null || !context.getFlow().equals(FlowType.Inflow));
  }
}
/*
function notInflow(context, value) {
  return context.flow !== "IN";
}
*/