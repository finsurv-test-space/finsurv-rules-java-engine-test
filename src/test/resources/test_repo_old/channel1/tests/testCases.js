define(function () {
  return function (testBase) {
    with (testBase) {
      var mappings = {
        LocalCurrencySymbol: "R",
        LocalCurrencyName: "Rand",
        LocalCurrency: "ZAR",
        LocalValue: "RandValue",
        Regulator: "SARB",
        DealerPrefix: "AD",
        RegulatorPrefix: "SARB"
      };

      setMappings(mappings);

      var test_cases = [
        assertSuccess("obank3", {
          ReportingQualifier: 'NON REPORTABLE', Flow: 'IN',
          NonResident: { Entity: { AccountIdentifier: 'CASH' } }
        }),
        assertNoRule("obank3", { ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ' }),

        assertSuccess("obank4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),
        assertFailure("obank4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),
        assertFailure("obank4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          Resident: { Exception: { ExceptionName: 'BULK PENSION' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),

        assertSuccessCustom("ietdn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{ TransportDocumentNumber: 'XYZ' }] }]
        }, { LUClient: 'N' }),
        assertFailureCustom("ietdn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{ TransportDocumentNumber: 'XYZ' }] }]
        }, { LUClient: 'Y' }),
        assertSuccessCustom("ietdn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{ TransportDocumentNumber: '' }] }]
        }, { LUClient: 'N' })
      ]

    }
    return testBase;
  }
})
