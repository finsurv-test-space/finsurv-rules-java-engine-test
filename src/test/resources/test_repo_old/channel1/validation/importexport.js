define(function () {
  return function (predef) {
    var ie;

    with (predef) {
      ie = {
        ruleset: "Import/Export Rules for Flow Form",
        scope: "importexport",
        validations: [
          {
            field: "",
            rules: [
              message("ieicn1", null, "Required field for this category"),
              message("ieicn2", null, "Not required for category chosen")
            ]
          }, {
            field: ["ImportControlNumber", "TransportDocumentNumber", "UCR"],
            rules: [
              failure("inv_ieicn-tdn-ucr1", "INV??", "May not contain special characters",
                notEmpty.and(hasPattern(/[^a-zA-Z0-9]/)))
            ]
          }, {
            field: "TransportDocumentNumber",
            rules: [
            ]
          }, {
            field: "OldField",
            minLen: 4,
            maxLen: 18
          }
        ]
      }
    };

    return ie;
  }
});