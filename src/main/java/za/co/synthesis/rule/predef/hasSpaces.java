package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jake
 * Date: 1/15/16
 * Time: 10:57 AM
 * Returns true if there is a space in the value
 */
public class hasSpaces implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value != null) {
      String s = value.toString();
      return s.contains(" ") || !s.trim().equals(s);
    }
    return false;
  }
}


/*
  _export.hasSpaces = function hasSpaces(context, value) {
    return value && (typeof value == "string" && (value.indexOf(' ') > -1 || value.trim() !== value));
  }
*/
