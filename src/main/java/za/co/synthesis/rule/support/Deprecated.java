package za.co.synthesis.rule.support;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/6/14
 * Time: 12:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class Deprecated extends Rule {
  public Deprecated(String name, String code, String message, IAssertionFunction assertion, String assertionDSL, String ruleDSL) {
    super(RuleType.Deprecated, name, code, message, null, assertion, assertionDSL, ruleDSL);
  }
}