package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 3/2/15
 * Time: 4:05 PM
 * Residence status i.e. is the person a South African resident or a non resident
 */
public enum ResidenceStatus {
  Unknown,
  Resident,
  NonResident,
  IHQ,
  HOLDCO
}

