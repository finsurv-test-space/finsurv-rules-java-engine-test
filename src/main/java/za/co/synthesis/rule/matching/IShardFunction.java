package za.co.synthesis.rule.matching;

import za.co.synthesis.rule.core.IMatchingContext;

/**
 * Created by jake on 6/16/16.
 */
public interface IShardFunction {
  String[] execute(IMatchingContext context);
}
