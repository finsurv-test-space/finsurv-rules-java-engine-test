package za.co.synthesis;

import za.co.synthesis.rule.core.*;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.evaluation.EvaluationTestUtilty;
import za.co.synthesis.rule.support.JSConstant;
import za.co.synthesis.rule.support.Util;

import static org.junit.Assert.assertTrue;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * User: jake
 * Date: 3/5/15
 * Time: 3:18 PM
 * Code to execute the evaluation test cases
 */
public class Evaluations {
  private static class Test_Validate implements ICustomValidate {
    private Boolean wasRun = false;

    public void resetRun() {
      wasRun = false;
    }

    public Boolean getWasRun() {
      return wasRun;
    }

    @Override
    public CustomValidateResult call(Object value, Object... otherInputs) {
      this.wasRun = true;
      return new CustomValidateResult(StatusType.Pass);
    }
  }

  Evaluations.Test_Validate test_validate = new Evaluations.Test_Validate();


  private static String onUsTestCaseName(ResidenceStatus drStatus, BankAccountType drAccountType,
                                         ResidenceStatus crStatus, BankAccountType crAccountType,
                                         String manualSection) {
    String name = JSConstant.jsResidenceStatus(drStatus).getApply().getName() + " " +
            JSConstant.jsBankAccountType(drAccountType).getApply().getName();
    name += " -> " + JSConstant.jsResidenceStatus(crStatus).getApply().getName() + " " +
            JSConstant.jsBankAccountType(crAccountType).getApply().getName() + " (" + manualSection + ")";
    return name;
  }

  private static String nonResResultParams(IEvaluationDecision decision) {
    String result = "";
    if (decision.getNonResException() != null) {
      result += "nonResException: \"" + decision.getNonResException() + "\"";
    } else {
      result += "nonResSide: " + JSConstant.jsDrCr(decision.getNonResSide()).toString() +
              ", nonResAccountType: " + JSConstant.jsReportingAccountType(decision.getNonResAccountType()).toString();
    }
    return result;
  }

  private static String resResultParams(IEvaluationDecision decision) {
    String result = "";
    if (decision.getResException() != null) {
      result += "resException: \"" + decision.getResException() + "\"";
    } else {
      result += "resSide: " + JSConstant.jsDrCr(decision.getResSide()).toString() +
              ", resAccountType: " + JSConstant.jsReportingAccountType(decision.getResAccountType()).toString();
    }
    return result;
  }

  private static String composeCaseOnUsSingleDecision(ResidenceStatus drStatus, BankAccountType drAccountType,
                                                      ResidenceStatus crStatus, BankAccountType crAccountType,
                                                      AccountHolderStatus drAccountHolderStatus,
                                                      IEvaluationDecision decision) {
    String name = onUsTestCaseName(drStatus, drAccountType, crStatus, crAccountType, decision.getManualSection());
    String testCase;
    if (drAccountHolderStatus == AccountHolderStatus.Individual) {
      testCase = "assertOnUsIdv(\"" + name + "\", \n";
    }
    else if (drAccountHolderStatus == AccountHolderStatus.Entity) {
      testCase = "assertOnUsEnt(\"" + name + "\", \n";
    }
    else {
      testCase = "assertOnUs(\"" + name + "\", \n";
    }
    testCase += "  " + JSConstant.jsResidenceStatus(drStatus).toString() +
            ", " + JSConstant.jsBankAccountType(drAccountType).toString() +
            ", " + JSConstant.jsResidenceStatus(crStatus).toString() +
            ", " + JSConstant.jsBankAccountType(crAccountType).toString() + ", \n";
    if (decision.getReportable() == ReportableType.NotReportable || decision.getReportable() == ReportableType.Illegal) {
      testCase += "  { reportable: " + JSConstant.jsReportableType(decision.getReportable()).toString() + " })";

    } else {
      testCase += "  { reportable: " + JSConstant.jsReportableType(decision.getReportable()).toString() +
              ", flow: " + JSConstant.jsFlowType(decision.getFlow()).toString() +
              ", reportingSide: " + JSConstant.jsDrCr(decision.getReportingSide()).toString() +
              ", " + nonResResultParams(decision) +
              ", " + resResultParams(decision) +
              " })";

    }
    return testCase;
  }

  private static String composeCaseOnUsDoubleDecision(ResidenceStatus drStatus, BankAccountType drAccountType,
                                                      ResidenceStatus crStatus, BankAccountType crAccountType,
                                                      AccountHolderStatus drAccountHolderStatus,
                                                      IEvaluationDecision decision1, IEvaluationDecision decision2) {
    String name = onUsTestCaseName(drStatus, drAccountType, crStatus, crAccountType, decision1.getManualSection());
    String testCase;
    if (drAccountHolderStatus == AccountHolderStatus.Individual) {
      testCase = "assertOnUsIdv2Results(\"" + name + "\", \n";
    }
    else if (drAccountHolderStatus == AccountHolderStatus.Entity) {
      testCase = "assertOnUsEnt2Results(\"" + name + "\", \n";
    }
    else {
      testCase = "assertOnUs2Results(\"" + name + "\", \n";
    }
    testCase += "  " + JSConstant.jsResidenceStatus(drStatus).toString() +
            ", " + JSConstant.jsBankAccountType(drAccountType).toString() +
            ", " + JSConstant.jsResidenceStatus(crStatus).toString() +
            ", " + JSConstant.jsBankAccountType(crAccountType).toString() + ", \n";
    if (decision1.getReportable() == ReportableType.NotReportable || decision1.getReportable() == ReportableType.Illegal) {
      testCase += "  { reportable: " + JSConstant.jsReportableType(decision1.getReportable()).toString() + " })";

    } else {
      testCase += "  { reportable: " + JSConstant.jsReportableType(decision1.getReportable()).toString() +
              ", flow: " + JSConstant.jsFlowType(decision1.getFlow()).toString() +
              ", reportingSide: " + JSConstant.jsDrCr(decision1.getReportingSide()).toString() +
              ", " + nonResResultParams(decision1) +
              ", " + resResultParams(decision1) +
              " },\n";
    }
    if (decision2.getReportable() == ReportableType.NotReportable || decision2.getReportable() == ReportableType.Illegal) {
      testCase += "  { reportable: " + JSConstant.jsReportableType(decision2.getReportable()).toString() + " })";

    } else {
      testCase += "  { reportable: " + JSConstant.jsReportableType(decision2.getReportable()).toString() +
              ", flow: " + JSConstant.jsFlowType(decision2.getFlow()).toString() +
              ", reportingSide: " + JSConstant.jsDrCr(decision2.getReportingSide()).toString() +
              ", " + nonResResultParams(decision2) +
              ", " + resResultParams(decision2) +
              " })";
    }
    return testCase;
  }

  private static String testCaseOnUs(ResidenceStatus drStatus, BankAccountType drAccountType,
                              ResidenceStatus crStatus, BankAccountType crAccountType,
                              AccountHolderStatus drAccountHolderStatus,
                              List<IEvaluationDecision> decisions ) {
    if (decisions != null && decisions.size() == 1 && decisions.get(0).getReportable() != ReportableType.Unknown) {

      IEvaluationDecision decision = decisions.get(0);

      return composeCaseOnUsSingleDecision(drStatus, drAccountType, crStatus, crAccountType, drAccountHolderStatus, decision);
    }
    if (decisions != null && decisions.size() == 2) {
      IEvaluationDecision decision1 = decisions.get(0);
      IEvaluationDecision decision2 = decisions.get(0);

      return composeCaseOnUsDoubleDecision(drStatus, drAccountType, crStatus, crAccountType, drAccountHolderStatus, decision1, decision2);
    }
    return null;
  }

  private static String customerPaymentTestCaseName(ResidenceStatus drStatus, BankAccountType drAccountType,
                                            String beneficiaryBIC, String destinationCurrency) {
    String name = "CUS Pay " + JSConstant.jsResidenceStatus(drStatus).getApply().getName() + " " +
            JSConstant.jsBankAccountType(drAccountType).getApply().getName();
    name += " paying " + destinationCurrency + " to " + beneficiaryBIC;
    return name;
  }

  private static String customerReceiptTestCaseName(ResidenceStatus crStatus, BankAccountType crAccountType,
                                            String remitterBIC, String sourceCurrency) {
    String name = "CUS Rec " + JSConstant.jsResidenceStatus(crStatus).getApply().getName() + " " +
            JSConstant.jsBankAccountType(crAccountType).getApply().getName();
    name += " receiving " + sourceCurrency + " from " + remitterBIC;
    return name;
  }

  private static String THIS_BANK = "SBZAZAJJ";

  private static String composeCaseAssumption(String testName,
                                              String drBankBIC, ResidenceStatus drStatus, BankAccountType drAccountType,
                                              String drCurrency, String drField72,
                                              String crBankBIC, ResidenceStatus crStatus, BankAccountType crAccountType,
                                              String crCurrency,
                                              AccountHolderStatus drAccountHolderStatus,
                                              IEvaluationDecision decision) {
    String testCase;
    if (drAccountHolderStatus == AccountHolderStatus.Individual) {
      testCase = "assertAssumptionIdv(\"" + testName + "\", \n";
    }
    else if (drAccountHolderStatus == AccountHolderStatus.Entity) {
      testCase = "assertAssumptionEnt(\"" + testName + "\", \n";
    }
    else {
      testCase = "assertAssumption(\"" + testName + "\", \n";
    }
    testCase += " \"" + drBankBIC +
            "\", " + (drStatus != null ? JSConstant.jsResidenceStatus(drStatus).toString() : "null") +
            ", " + (drAccountType != null ? JSConstant.jsBankAccountType(drAccountType).toString() : "null") +
            ", " + (drCurrency != null ? "\"" + drCurrency + "\"" : "null") +
            ", " + (drField72 != null ? "\"" + drField72 + "\"" : "null") +
            ", \"" + crBankBIC +
            "\", " + (crStatus != null ? JSConstant.jsResidenceStatus(crStatus).toString() : "null") +
            ", " + (crAccountType != null ? JSConstant.jsBankAccountType(crAccountType).toString() : "null") +
            ", " + (crCurrency != null ? "\"" + crCurrency + "\"" : "null") + ", \n";
    if (decision.getReportable() == ReportableType.NotReportable || decision.getReportable() == ReportableType.Illegal) {
      testCase += "  { reportable: " + JSConstant.jsReportableType(decision.getReportable()).toString() + " })";

    } else {
      testCase += "  { reportable: " + JSConstant.jsReportableType(decision.getReportable()).toString() +
              ", flow: " + JSConstant.jsFlowType(decision.getFlow()).toString() +
              ", reportingSide: " + JSConstant.jsDrCr(decision.getReportingSide()).toString() +
              ", " + nonResResultParams(decision) +
              ", " + resResultParams(decision) +
              " })";

    }
    return testCase;
  }

  private static String testCaseCustomerPayment(ResidenceStatus drStatus, BankAccountType drAccountType,
                                        String beneficiaryBIC, String destinationCurrency,
                                        AccountHolderStatus drAccountHolderStatus,
                                        IEvaluationDecision decision ) {
    if (decision != null && decision.getReportable() != ReportableType.Unknown) {
      String name = customerPaymentTestCaseName(drStatus, drAccountType, beneficiaryBIC, destinationCurrency);
      return composeCaseAssumption(name, THIS_BANK, drStatus, drAccountType, null, null,
              beneficiaryBIC, null, null, destinationCurrency, drAccountHolderStatus, decision);
    }
    return null;
  }

  private static String testCaseCustomerReceipt(ResidenceStatus crStatus, BankAccountType crAccountType,
                                        String remitterBIC, String sourceCurrency, String field72,
                                        AccountHolderStatus drAccountHolderStatus,
                                        IEvaluationDecision decision ) {
    if (decision != null && decision.getReportable() != ReportableType.Unknown) {
      String name = customerReceiptTestCaseName(crStatus, crAccountType, remitterBIC, sourceCurrency);
      return composeCaseAssumption(name, remitterBIC, null, null, sourceCurrency, field72,
              THIS_BANK, crStatus, crAccountType, null, drAccountHolderStatus, decision);
    }
    return null;
  }

  private static String testCaseBankPayment(String beneficiaryBIC, String destinationCurrency,
                                            IEvaluationDecision decision ) {
    if (decision != null && decision.getReportable() != ReportableType.Unknown) {
      String name = "Bank Pay to " + beneficiaryBIC + " in " + destinationCurrency;
      return composeCaseAssumption(name, THIS_BANK, null, BankAccountType.NOSTRO, null, null,
              beneficiaryBIC, null, null, destinationCurrency, null, decision);
    }
    return null;
  }

  private static String testCaseBankReceipt(String remitterBIC, String sourceCurrency,
                                            IEvaluationDecision decision ) {
    if (decision != null && decision.getReportable() != ReportableType.Unknown) {
      String name = "Bank Rec from " + remitterBIC + " in " + sourceCurrency;
      return composeCaseAssumption(name, remitterBIC, null, null, sourceCurrency, null,
              THIS_BANK, null, BankAccountType.NOSTRO, null, null, decision);
    }
    return null;
  }


  private static void generateTestCases(String filepath, Evaluator evaluator) throws Exception {
    List<String> rules = new ArrayList<String>();

    // generate all OnUs test cases
    ResidenceStatus statuses[] = new ResidenceStatus[] {ResidenceStatus.Resident, ResidenceStatus.NonResident};//, ResidenceStatus.IHQ, ResidenceStatus.HOLDCO};
    BankAccountType accountTypes[] = new BankAccountType[] {BankAccountType.CASH_CURR,
            BankAccountType.CASH_LOCAL, BankAccountType.CFC, BankAccountType.FCA, BankAccountType.LOCAL_ACC,
            BankAccountType.VOSTRO, BankAccountType.NOSTRO };
    for (ResidenceStatus drStatus : statuses) {
      for (BankAccountType drAccountType : accountTypes) {
        for (ResidenceStatus crStatus : statuses) {
          for (BankAccountType crAccountType : accountTypes) {
            List<IEvaluationDecision> decisionsInv = evaluator.evaluateCustomerOnUsFiltered(drStatus, drAccountType, AccountHolderStatus.Individual,
                    crStatus, crAccountType, null);
            List<IEvaluationDecision> decisionsEnt = evaluator.evaluateCustomerOnUsFiltered(drStatus, drAccountType, AccountHolderStatus.Entity,
                    crStatus, crAccountType, null);

            if (EvaluationTestUtilty.areDecisionsTheSame(decisionsInv, decisionsEnt)) {
              rules.add(testCaseOnUs(drStatus, drAccountType, crStatus, crAccountType, null, decisionsInv));
            } else {
              rules.add(testCaseOnUs(drStatus, drAccountType, crStatus, crAccountType, AccountHolderStatus.Individual, decisionsInv));
              rules.add(testCaseOnUs(drStatus, drAccountType, crStatus, crAccountType, AccountHolderStatus.Entity, decisionsEnt));
            }
          }
        }
      }
    }

    PrintWriter writer = new PrintWriter(filepath, "UTF-8");
    writer.println("define(function() {");
    writer.println("  return function(testBase) {");
    writer.println("    var test_cases;");
    writer.println("    with (testBase) {");
    writer.println("      test_cases = [");
    for (String s : rules) {
      writer.println(s + ",");
    }
    writer.println("      ];");
    writer.println("    }");
    writer.println("    return testBase;");
    writer.println("  }");
    writer.println("})");
    writer.close();
  }

  public static void generateTestCases() throws Exception {
    String packagePath = "../finsurv-rules/reg_rule_repo/coreBON";

    EvaluationEngine engine = new EvaluationEngine();

    engine.setupLoadPackagePath(packagePath);

    Evaluator evaluator = engine.issueEvaluator();

    generateTestCases(packagePath + "/tests/testEvalCases.js", evaluator);
  }

  private static void generateAssumptionTestCases(String filepath, Evaluator evaluator) throws Exception {
    List<String> rules = new ArrayList<String>();

    // generate all CustomerPayment test cases
    ResidenceStatus statuses[] = new ResidenceStatus[] {ResidenceStatus.Resident, ResidenceStatus.NonResident};//, ResidenceStatus.IHQ, ResidenceStatus.HOLDCO};
    BankAccountType accountTypes[] = new BankAccountType[] {BankAccountType.CASH_CURR,
            BankAccountType.CASH_LOCAL, BankAccountType.CFC, BankAccountType.FCA, BankAccountType.LOCAL_ACC,
            BankAccountType.VOSTRO, BankAccountType.NOSTRO };
    String[] counterpartyBICs = new String[] {"CITIUSXXX", "OTHRZAXXX", "ABSAZAJJX"};
    String[] destinationCurrencies = new String[] {"ZAR", "USD"};
    for (ResidenceStatus drStatus : statuses) {
      for (BankAccountType drAccountType : accountTypes) {
        for (String crCPBIC : counterpartyBICs) {
          for (String crCurrency : destinationCurrencies) {
            IEvaluationDecision decisionInv = evaluator.evaluateCustomerPayment(drStatus, drAccountType, crCPBIC, crCurrency, AccountHolderStatus.Individual);
            IEvaluationDecision decisionEnt = evaluator.evaluateCustomerPayment(drStatus, drAccountType, crCPBIC, crCurrency, AccountHolderStatus.Entity);

            if (EvaluationTestUtilty.areDecisionsTheSame(decisionInv, decisionEnt)) {
              rules.add(testCaseCustomerPayment(drStatus, drAccountType, crCPBIC, crCurrency, null, decisionInv));
            } else {
              rules.add(testCaseCustomerPayment(drStatus, drAccountType, crCPBIC, crCurrency, AccountHolderStatus.Individual, decisionInv));
              rules.add(testCaseCustomerPayment(drStatus, drAccountType, crCPBIC, crCurrency, AccountHolderStatus.Entity, decisionEnt));
            }
          }
        }
      }
    }


    // generate all CustomerPayment test cases
    for (ResidenceStatus crStatus : statuses) {
      for (BankAccountType crAccountType : accountTypes) {
        for (String drCPBIC : counterpartyBICs) {
          for (String drCurrency : destinationCurrencies) {
            IEvaluationDecision decisionInvNone = evaluator.evaluateCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, AccountHolderStatus.Individual, null);
            IEvaluationDecision decisionInvDtcus = evaluator.evaluateCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, AccountHolderStatus.Individual, "DTCUS");
            IEvaluationDecision decisionInvNtnrc = evaluator.evaluateCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, AccountHolderStatus.Individual, "NTNRC");

            IEvaluationDecision decisionEntNone = evaluator.evaluateCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, AccountHolderStatus.Entity, null);
            IEvaluationDecision decisionEntDtcus = evaluator.evaluateCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, AccountHolderStatus.Entity, "DTCUS");
            IEvaluationDecision decisionEntNtnrc = evaluator.evaluateCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, AccountHolderStatus.Entity, "NTNRC");

            if (EvaluationTestUtilty.areDecisionsTheSame(decisionInvNone, decisionInvDtcus) && EvaluationTestUtilty.areDecisionsTheSame(decisionInvNone, decisionInvNtnrc)) {
              // field 72 invariant
              if (EvaluationTestUtilty.areDecisionsTheSame(decisionInvNone, decisionEntNone)) {
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, null, null, decisionInvNone));
              } else {
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, null, AccountHolderStatus.Individual, decisionInvNone));
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, null, AccountHolderStatus.Entity, decisionEntNone));
              }
            }
            else {
              // field 72 is important
              if (EvaluationTestUtilty.areDecisionsTheSame(decisionInvNone, decisionEntNone)) {
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, null, null, decisionInvNone));
              } else {
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, null, AccountHolderStatus.Individual, decisionInvNone));
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, null, AccountHolderStatus.Entity, decisionEntNone));
              }

              if (EvaluationTestUtilty.areDecisionsTheSame(decisionInvDtcus, decisionEntDtcus)) {
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, "DTCUS", null, decisionInvDtcus));
              } else {
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, "DTCUS", AccountHolderStatus.Individual, decisionInvDtcus));
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, "DTCUS", AccountHolderStatus.Entity, decisionEntDtcus));
              }

              if (EvaluationTestUtilty.areDecisionsTheSame(decisionInvNtnrc, decisionEntNtnrc)) {
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, "NTNRC", null, decisionInvNtnrc));
              } else {
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, "NTNRC", AccountHolderStatus.Individual, decisionInvNtnrc));
                rules.add(testCaseCustomerReceipt(crStatus, crAccountType, drCPBIC, drCurrency, "NTNRC", AccountHolderStatus.Entity, decisionEntNtnrc));
              }
            }
          }
        }
      }
    }

    for (String cpBIC : counterpartyBICs) {
      for (String currency : destinationCurrencies) {
        IEvaluationDecision decision = evaluator.evaluateBankPayment(cpBIC, currency);
        rules.add(testCaseBankPayment(cpBIC, currency, decision));
      }
    }

    for (String cpBIC : counterpartyBICs) {
      for (String currency : destinationCurrencies) {
        IEvaluationDecision decision = evaluator.evaluateBankReceipt(cpBIC, currency);
        rules.add(testCaseBankReceipt(cpBIC, currency, decision));
      }
    }

    PrintWriter writer = new PrintWriter(filepath, "UTF-8");
    writer.println("define(function() {");
    writer.println("  return function(testBase) {");
    writer.println("    var test_cases;");
    writer.println("    with (testBase) {");
    writer.println("      test_cases = [");
    for (String s : rules) {
      writer.println(s + ",");
    }
    writer.println("      ];");
    writer.println("    }");
    writer.println("    return testBase;");
    writer.println("  }");
    writer.println("})");
    writer.close();

    /*
    // Find all unmapped assumptions for unknown Dr side
    unknownCount = 0;
    String[] swiftCodes = new String[]{"EXCON APPROVAL", "SPOT", "HEDGE", "TRANSFER FROM ABROAD", "RETURN OF FUNDS", "COMMODITIES", "FREIGHT", "DTCUS", "NTNRC", "NTNRB"};
    for (ResidenceStatus crStatus : statuses) {
      for (BankAccountType crAccountType : accountTypes) {
        for (String drCPBIC : counterpartyBICs) {
          for (String crCurrency : destinationCurrencies) {
            for (ACCOUNT_HOLDER_STATUS crAHStatus : accountHolderStatuses) {
              for (String swiftCode : swiftCodes) {
                List<IEvaluationDecision> decisions = evaluator.evaluateUnknownDrSide(crStatus, crAccountType, null, drCPBIC, crCurrency, crAHStatus, swiftCode);
                if (decisions == null || (decisions.size() == 1 && decisions.get(0).getReportable() == ReportableType.Unknown)) {
                  unknownCount++;
                  System.out.println("No Unknown Dr Decision for: Cr: " + crStatus.toString() + ", " + crAccountType.toString() +
                          "; Dr: " + drCPBIC.toString() + ", " + crCurrency + ", " + crAHStatus.toString() + ", " + swiftCode);
                } else {
                  checkCorrectnessOfDecisions(validator, decisions);
                }
              }
            }
          }
        }
      }
    }
    if (unknownCount == 0) {
      System.out.println("Every Unknown Dr getDecision scenario has been mapped in the rules");
    } else {
      System.out.println("Problem: A total of " + unknownCount + " Unknown Dr getDecision scenarios have not been mapped");
    }

    writer = new PrintWriter(filepath, "UTF-8");
    writer.println("define(['testEvalBase'], function (testBase) {");
    writer.println("  with (testBase) {");
    writer.println("    var test_cases = [");
    for (String s : rules) {
      writer.println(s + ",");
    }
    writer.println("    ];");
    writer.println("  }");
    writer.println("  return testBase;");
    writer.println("});");
    writer.close();*/
  }

  public static void generateAssumptionTestCases() throws Exception {
    String packagePath = "../finsurv-rules/reg_rule_repo/coreSARB";

    EvaluationEngine engine = new EvaluationEngine();

    engine.setupLoadPackagePath(packagePath);

    Evaluator evaluator = engine.issueEvaluator();

    generateAssumptionTestCases(packagePath + "/tests/testScenarioEvalCases.js", evaluator);
  }

  /*
  public void runTestCases(String filterTestCase) throws Exception {
    String filterRule = filterTestCase;
    if (filterTestCase != null) {
      int pos = filterTestCase.indexOf('#');
      if (pos != -1) {
        filterRule = filterTestCase.substring(0, pos);
      }
    }

    ValidationEngine validationEngine = new ValidationEngine();

    //validationEngine.setupLoadPackagePath("../finsurv-rules/reg_rule_repo/stdBankNBOL");
    validationEngine.setupLoadPackagePath("../finsurv-rules/reg_rule_repo/coreSARB");
//    validationEngine.setupLoadLookupFile("../finsurv-rules/lookups.js");
//    validationEngine.setupLoadRuleFile("../finsurv-rules/transaction.js");
//    validationEngine.setupLoadRuleFile("../finsurv-rules/money.js");
//    validationEngine.setupLoadRuleFile("../finsurv-rules/importexport.js");

    DefaultSupporting supporting = new DefaultSupporting();
    supporting.setCurrentDate(Util.date("2013-01-20"));
    supporting.setGoLiveDate(Util.date("2013-08-18"));
    validationEngine.setupSupporting(supporting);

    validationEngine.setIgnoreMissingCustomValidate(true);
    Validator validator = validationEngine.issueValidator();


    EvaluationEngine engine = new EvaluationEngine();

  //  engine.setupLoadPackagePath("../finsurv-rules/reg_rule_repo/stdBankNBOL");
    engine.setupLoadPackagePath("../finsurv-rules/reg_rule_repo/coreSARB");
//       engine.setupLoadRuleFile("../finsurv-rules/build/crules/stdBankNBOL/evaluation/evalRules.js");
//       engine.setupLoadAssumptionsFile("../finsurv-rules/build/crules/stdBankNBOL/evaluation/evalAssumptions.js");
    //   engine.setupLoadRuleFile("../finsurv-rules/evalRules.js");
    //   engine.setupLoadAssumptionsFile("../finsurv-rules/evalAssumptions.js");
//    engine.setupLoadRuleFile("../finsurv-rules/customRules/stdBankEval.js");


    Evaluator evaluator = engine.issueEvaluator();

    List<DuplicateEvaluation> duplicates = evaluator.getDuplicateRules();
    if(duplicates.size() > 0) {
      System.out.println("*** Duplicate rules detected:");
      for (DuplicateEvaluation duplicate : duplicates) {
        System.out.println(duplicate.prettyRuleName());
      }
    }

   // List<IEvaluationDecision> result = evaluator.evaluateUnknownCrSide(ResidenceStatus.Resident, BankAccountType.LOCAL_ACC, CounterpartyStatus.Onshore, "USD", ACCOUNT_HOLDER_STATUS.Entity);
    List<IEvaluationDecision> result = evaluator.evaluate(ResidenceStatus.NonResident, BankAccountType.LOCAL_ACC, ResidenceStatus.Resident, BankAccountType.CFC);

    System.out.println("TEST CASES: testEvalCases.js" );
    System.out.println("============================" );
    FileReader fileReader = new FileReader("../finsurv-rules/reg_rule_repo/coreSADC/tests/testEvalCases.js");
    JSStructureParser parser = new JSStructureParser(fileReader);

    Object obj = parser.parse();

    JSObject testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
    if (testCaseContainer != null) {
      JSArray cases = (JSArray)(testCaseContainer).get("test_cases");
      EvaluationTestUtilty.doCases(evaluator, validator, cases, filterRule);
    }

    System.out.println("\nTEST CASES: testScenarioEvalCases.js" );
    System.out.println("======================================" );
    fileReader = new FileReader("../finsurv-rules/reg_rule_repo/coreSADC/tests/testScenarioEvalCases.js");
    parser = new JSStructureParser(fileReader);

    obj = parser.parse();

    testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
    if (testCaseContainer != null) {
      JSArray cases = (JSArray)(testCaseContainer).get("test_cases");
      EvaluationTestUtilty.doCases(evaluator, validator, cases, filterRule);
    }

    // Find all Unknown mappings
    int unknownCount = 0;
    ResidenceStatus statuses[] = new ResidenceStatus[] {ResidenceStatus.Resident, ResidenceStatus.NonResident};//, ResidenceStatus.IHQ, ResidenceStatus.HOLDCO};
    BankAccountType accountTypes[] = new BankAccountType[] {BankAccountType.CASH_CURR,
            BankAccountType.CASH_LOCAL, BankAccountType.CFC, BankAccountType.FCA, BankAccountType.LOCAL_ACC,
            BankAccountType.VOSTRO, BankAccountType.NOSTRO };
    for (ResidenceStatus drStatus : statuses) {
      for (BankAccountType drAccountType : accountTypes) {
        for (ResidenceStatus crStatus : statuses) {
          for (BankAccountType crAccountType : accountTypes) {
            List<IEvaluationDecision> decisions = evaluator.evaluate(drStatus, drAccountType, crStatus, crAccountType);
            if (decisions == null || (decisions.size() == 1 && decisions.get(0).getReportable() == ReportableType.Unknown)) {
              unknownCount++;
              System.out.println("No Decision for: Dr: " + drStatus.toString() + ", " +  drAccountType.toString() +
                      "; Cr: " + crStatus.toString() + ", " + crAccountType.toString() );
            }
            else {
              EvaluationTestUtilty.checkCorrectnessOfDecisions(validator, decisions);
            }
          }
        }
      }
    }
    if (unknownCount == 0) {
      System.out.println("Every getDecision scenario has been mapped in the rules");
    }
    else {
      System.out.println("Problem: A total of " + unknownCount + " scenarios have not been mapped");
    }

    // Find all unmapped assumptions for unknown Cr side
    unknownCount = 0;
    String[] counterpartyBICs = new String[] {"CITIUSXXX", "OTHRZAXXX", "ABSAZAJJX"};
    String[] destinationCurrencies = new String[] {"ZAR", "USD"};
    AccountHolderStatus[] accountHolderStatuses = new AccountHolderStatus[] {AccountHolderStatus.Entity, AccountHolderStatus.Individual};
    for (ResidenceStatus drStatus : statuses) {
      for (BankAccountType drAccountType : accountTypes) {
        for (String crCPBIC : counterpartyBICs) {
          for (String crCurrency : destinationCurrencies) {
            for (AccountHolderStatus crAHStatus : accountHolderStatuses) {
              List<IEvaluationDecision> decisions = evaluator.evaluateUnknownCrSide(drStatus, drAccountType, null, crCPBIC, crCurrency, crAHStatus);
              if (decisions == null || (decisions.size() == 1 && decisions.get(0).getReportable() == ReportableType.Unknown)) {
                unknownCount++;
                System.out.println("No Unknown Cr Decision for: Dr: " + drStatus.toString() + ", " + drAccountType.toString() +
                        "; Cr: " + crCPBIC + ", " + crCurrency +  ", " + crAHStatus.toString());
              } else {
                EvaluationTestUtilty.checkCorrectnessOfDecisions(validator, decisions);
              }
            }
          }
        }
      }
    }
    if (unknownCount == 0) {
      System.out.println("Every Unknown Cr getDecision scenario has been mapped in the rules");
    }
    else {
      System.out.println("Problem: A total of " + unknownCount + " Unknown Cr getDecision scenarios have not been mapped");
    }


    // Find all unmapped assumptions for unknown Dr side
    unknownCount = 0;
    String[] swiftCodes = new String[]{"EXCON APPROVAL", "SPOT", "HEDGE", "TRANSFER FROM ABROAD", "RETURN OF FUNDS", "COMMODITIES", "FREIGHT", "DTCUS", "NTNRC", "NTNRB"};
    for (ResidenceStatus crStatus : statuses) {
      for (BankAccountType crAccountType : accountTypes) {
        for (String drCPBIC : counterpartyBICs) {
          for (String crCurrency : destinationCurrencies) {
            for (AccountHolderStatus crAHStatus : accountHolderStatuses) {
              for (String swiftCode : swiftCodes) {
                List<IEvaluationDecision> decisions = evaluator.evaluateUnknownDrSide(crStatus, crAccountType, null, drCPBIC, crCurrency, crAHStatus, swiftCode);
                if (decisions == null || (decisions.size() == 1 && decisions.get(0).getReportable() == ReportableType.Unknown)) {
                  unknownCount++;
                  System.out.println("No Unknown Dr Decision for: Cr: " + crStatus.toString() + ", " + crAccountType.toString() +
                      "; Dr: " + drCPBIC.toString() + ", " + crCurrency + ", " + crAHStatus.toString() + ", " + swiftCode);
                } else {
                  EvaluationTestUtilty.checkCorrectnessOfDecisions(validator, decisions);
                }
              }
            }
          }
        }
      }
    }
    if (unknownCount == 0) {
      System.out.println("Every Unknown Dr getDecision scenario has been mapped in the rules");
    } else {
      System.out.println("Problem: A total of " + unknownCount + " Unknown Dr getDecision scenarios have not been mapped");
    }
  }
  */

  private void jumboLoadEvaluationEngine(EvaluationEngine engine, String packagePath) throws EvaluationException {
    String packageFileName = packagePath + File.separator + "evaluation" + File.separator + "evalJumbo.js";
    Charset encoding = StandardCharsets.UTF_8;
    String data = null;
    try
    {
      byte[] encoded = Files.readAllBytes(Paths.get(packageFileName));
      data = new String(encoded, encoding);
    }
    catch (IOException e) {
      throw new EvaluationException("Cannot load " + packageFileName, e);
    }
    engine.setupLoadRule("", new StringReader(data));
    engine.setupLoadAssumptions("", new StringReader(data));
    engine.setupLoadContext("", new StringReader(data));
  }

  private void executePackageTestCases(String packagePath, String testFileName, EvaluationTestUtilty.TestOutput output) throws Exception {
    EvaluationEngine evaluationEngine = new EvaluationEngine();
    ValidationEngine validationEngine = new ValidationEngine();

    // Package:
    long startLoad = System.currentTimeMillis();
    evaluationEngine.setupLoadPackagePath(packagePath);

    String testLookups = packagePath + "/tests/testLookups.js";
    File file = new File(testLookups);
    if (file.exists() && file.isFile()) {
      validationEngine.setupLoadLookupFile(testLookups);
    }
    validationEngine.setupLoadPackagePath(packagePath);
    DefaultSupporting supporting = new DefaultSupporting();
    supporting.setCurrentDate(Util.date("2013-01-20"));
    supporting.setGoLiveDate(Util.date("2013-08-18"));
    validationEngine.setupSupporting(supporting);

    //engine.setIgnoreMissingCustomValidate(true);
    for (String validateName : validationEngine.getCustomValidateList()) {
      validationEngine.registerCustomValidate(validateName, test_validate);
    }
    long endLoad = System.currentTimeMillis();

    FileReader fileReader = new FileReader(packagePath + "/tests/" + testFileName);
    JSStructureParser parser = new JSStructureParser(fileReader);
    Object obj = parser.parse();

    long startRun = System.currentTimeMillis();
    // Iterate over the testCases
    JSObject testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
    if (testCaseContainer != null) {
      Evaluator evaluator = evaluationEngine.issueEvaluator();
      Validator validator = validationEngine.issueValidator();

      JSArray cases = (JSArray) testCaseContainer.get("test_cases");

      EvaluationTestUtilty.doCases(evaluator, validator, cases, null, output);
    }
    long endRun = System.currentTimeMillis();
    System.out.println("Load Time: " + (endLoad-startLoad) + "ms , Run Time: " + (endRun-startRun) + "ms");
  }

  private String[] getPackages(String path) {
    File file = new File(path);
    String[] directories = file.list(new FilenameFilter() {
      @Override
      public boolean accept(File current, String name) {
        return new File(current, name).isDirectory();
      }
    });
    return directories;
  }

  private void printTestOutput(String path, EvaluationTestUtilty.TestOutput output, boolean showOnlyErrors) {
    for (EvaluationTestUtilty.TestResult result : output.getResults()) {
      if (!result.hasPassed()) {
        System.out.println(result.getTestName() + ": " + result.getNote());
      }
      else {
        if (!showOnlyErrors || !result.isDecisionValid()) {
          System.out.println(result.getTestName() + ": passed" + (result.getNote().length() > 0 ? ". " + result.getNote() : ""));
        }
      }
      if (!result.isDecisionValid()) {
        System.out.println("Decision Invalid: " + result.getDecisionNote());
      }
    }

    if (output.getTestCount() == 0) {
      System.out.println("No tests in package " + path );
    }
    else {
      if (output.getErrorCount() == 0) {
        System.out.println(path + ": Completed " + output.getTestCount() + " tests Successfully :-)");
      } else {
        if (output.getErrorCount() == 1)
          System.out.println(path + ": Ran " + output.getTestCount() + " tests with 1 error!");
        else
          System.out.println(path + ": Ran " + output.getTestCount() + " tests with " + output.getErrorCount() + " errors!");
      }

      System.out.println();
      System.out.println("**************************************************************************************");
      System.out.println();
    }
  }

  private boolean runAllPackageTestCases(String repoPath) throws Exception {
    Exception firstException = null;
    int outputErrors = 0;
    String[] paths = getPackages(repoPath);
    for (String path : paths) {
      if (path.equals("stdBankNBOL")) {
        System.out.println(">>>*** Skipping tests on " + path );
        continue;
      }

      String packagePath = repoPath + "/" + path;
      File file = new File(packagePath + "/tests/testEvalCases.js");
      if (file.exists() && file.isFile()) {
        System.out.println(">>>*** Running tests on " + path );
        EvaluationTestUtilty.TestOutput output = new EvaluationTestUtilty.TestOutput();

        try {
          executePackageTestCases(packagePath, "testEvalCases.js", output);
        }
        catch (Exception e) {
          if (firstException == null)
            firstException = e;
          System.out.println("Exception running tests for " + path);
        }

        outputErrors += output.getErrorCount();
        printTestOutput(path, output, true);
      }
      else {
        System.out.println("*** No file testEvalCases.js for " + path );
      }

      file = new File(packagePath + "/tests/testScenarioEvalCases.js");
      if (file.exists() && file.isFile()) {
        EvaluationTestUtilty.TestOutput output = new EvaluationTestUtilty.TestOutput();
        System.out.println(">>>*** Running consolidated evaluation tests on " + path );

        try {
          executePackageTestCases(packagePath, "testScenarioEvalCases.js", output);
        }
        catch (Exception e) {
          if (firstException == null)
            firstException = e;
          System.out.println("Exception running consolidated evaluation tests for " + path);
        }

        outputErrors += output.getErrorCount();
        printTestOutput(path, output, true);
      }
      else {
        System.out.println("*** No file testScenarioEvalCases.js for " + path );
      }
    }

    if (firstException != null)
      throw firstException;

    return outputErrors == 0;
  }

  private boolean runSpecificPackageTestCases(String repoPath, String specificPackage) throws Exception {
    Exception firstException = null;
    int outputErrors = 0;

    String packagePath = repoPath + "/" + specificPackage;
    File file = new File(packagePath + "/tests/adhocEvalCases.js");
    if (file.exists() && file.isFile()) {
      System.out.println(">>>*** Running adhoc tests on " + specificPackage );
      EvaluationTestUtilty.TestOutput output = new EvaluationTestUtilty.TestOutput();

      try {
        executePackageTestCases(packagePath, "adhocEvalCases.js", output);
      }
      catch (Exception e) {
        if (firstException == null)
          firstException = e;
        System.out.println("Exception running tests for " + specificPackage);
      }

      outputErrors += output.getErrorCount();
      printTestOutput(specificPackage, output, true);
    }
    else {
      System.out.println("*** No file adhocEvalCases.js for " + specificPackage );
    }

    file = new File(packagePath + "/tests/testEvalCases.js");
    if (file.exists() && file.isFile()) {
      System.out.println(">>>*** Running core evaluation tests on " + specificPackage );
      EvaluationTestUtilty.TestOutput output = new EvaluationTestUtilty.TestOutput();

      try {
        executePackageTestCases(packagePath, "testEvalCases.js", output);
      }
      catch (Exception e) {
        if (firstException == null)
          firstException = e;
        System.out.println("Exception running tests for " + specificPackage);
      }

      outputErrors += output.getErrorCount();
      printTestOutput(specificPackage, output, true);
    }
    else {
      System.out.println("*** No file testEvalCases.js for " + specificPackage );
    }

    file = new File(packagePath + "/tests/testScenarioEvalCases.js");
    if (file.exists() && file.isFile()) {
      EvaluationTestUtilty.TestOutput output = new EvaluationTestUtilty.TestOutput();
      System.out.println(">>>*** Running scenario evaluation tests on " + specificPackage );

      try {
        executePackageTestCases(packagePath, "testScenarioEvalCases.js", output);
      }
      catch (Exception e) {
        if (firstException == null)
          firstException = e;
        System.out.println("Exception running consolidated evaluation tests for " + specificPackage);
      }

      outputErrors += output.getErrorCount();
      printTestOutput(specificPackage, output, true);
    }
    else {
      System.out.println("*** No file testScenarioEvalCases.js for " + specificPackage);
    }

    if (firstException != null)
      throw firstException;

    return outputErrors == 0;
  }

  @Test
  public void TestAllChannels() throws Exception {
    Evaluations testEvaluations = new Evaluations();

    boolean testSuccess = testEvaluations.runAllPackageTestCases("../finsurv-rules/reg_rule_repo");

    assertTrue("Errors in Evaluation Tests", testSuccess);
  }


  public static void main( String[] args ) throws Exception {
    if (args.length > 0 && "GenTests".equals(args[0])) {
      //generateTestCases();
      System.out.println("generateTestCases() has been disabled");
    } else if (args.length > 0 && "GenAssumptionTests".equals(args[0])) {
      //generateAssumptionTestCases();
      System.out.println("generateAssumptionTestCases() has been disabled");
    } else if (args.length > 1 && "EvalPackage".equals(args[0])) {
      Evaluations test = new Evaluations();
      test.runSpecificPackageTestCases("../finsurv-rules/reg_rule_repo", args[1]);
    } else {
      Evaluations test = new Evaluations();
      test.runAllPackageTestCases("../finsurv-rules/reg_rule_repo");
    }
  }
}
