package za.co.synthesis.rule.matching;

import za.co.synthesis.rule.core.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jake on 6/15/16.
 */
public class MatchingContext {
  private IMatchingContext contextLhs;
  private IMatchingContext contextRhs;
  private final List<MatchingResult> matchingResultList = new ArrayList<MatchingResult>();

  public MatchingContext(IMatchingContext contextLhs, IMatchingContext contextRhs) {
    this.contextLhs = contextLhs;
    this.contextRhs = contextRhs;
  }

  public List<MatchingResult> getMatchingResultList() {
    return matchingResultList;
  }

  public Object getFieldLhs(String field) {
    return contextLhs.getField(field);
  }

  public Object getFieldRhs(String field) {
    return contextRhs.getField(field);
  }

  public void logMatch(final double confidence, final String matchName) {
    matchingResultList.add(new MatchingResult(confidence, matchName));

    contextLhs.logMatch(confidence, matchName, contextRhs);
    contextRhs.logMatch(confidence, matchName, contextLhs);
  }
}
