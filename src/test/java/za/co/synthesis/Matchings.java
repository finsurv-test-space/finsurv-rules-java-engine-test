package za.co.synthesis;

import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.Matcher;
import za.co.synthesis.rule.core.MatchingEngine;
import za.co.synthesis.rule.core.MatchingResult;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/18/16.
 */
public class Matchings {
  List<String> testRuleNames = new ArrayList<String>();
  int testCount = 0;
  int errorCount = 0;

  private int countRuleInstances(String rulename) {
    int count = 0;
    for (String name : testRuleNames) {
      if (name.equals(rulename)) {
        count++;
      }
    }
    return count;
  }

  private boolean dblEquals(double v1, double v2) {
    return (Math.abs(v1-v2) < 0.001);
  }

  private MatchingResult bestResult(List<MatchingResult> list) {
    MatchingResult best = null;
    for (MatchingResult mr : list) {
      if (best == null || best.getConfidence() < mr.getConfidence()) {
        best = mr;
      }
    }
    return best;
  }


  private void assertRuleBase(Matcher matcher, String rulename, JSObject jsObjLhs, JSObject jsObjRhs,
                              double expectedConfidence) {
    testRuleNames.add(rulename);
    testCount++;

    List<MatchingResult> results = matcher.matchRule(rulename, jsObjLhs, jsObjRhs);
    if (dblEquals(expectedConfidence, 0)) {
      if (results.size() > 0) {
        errorCount++;
        System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting No Match, but had " + results.size() + ": " + results.get(0).getConfidence());
      }
    }
    else {
      if (results.size() == 0) {
        errorCount++;
        System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting confidence of " + expectedConfidence + ", but instead no match was found");
      }
      else {
        MatchingResult bestResult = bestResult(results);
        if (!dblEquals(expectedConfidence, bestResult.getConfidence())) {
          errorCount++;
          System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting confidence of " + expectedConfidence + ", but instead got " + bestResult.getConfidence());
        }
      }
    }
  }

  private void assertConfidence(Matcher matcher, String rulename, String confdence, JSObject jsObjLhs, JSObject jsObjRhs) {
    assertRuleBase(matcher, rulename, jsObjLhs, jsObjRhs, Double.parseDouble(confdence));
  }

  private void assertNoMatch(Matcher matcher, String rulename, JSObject jsObjLhs, JSObject jsObjRhs) {
    assertRuleBase(matcher, rulename, jsObjLhs, jsObjRhs, 0);
  }

  private void runTestCases(String filterTestCase) throws Exception {
    String filterRule = filterTestCase;
    int filterInstance = -1;
    if (filterTestCase != null) {
      int pos = filterTestCase.indexOf('#');
      if (pos != -1) {
        filterRule = filterTestCase.substring(0, pos);
        filterInstance = Integer.parseInt(filterTestCase.substring(pos+1));
      }
    }

    MatchingEngine engine = new MatchingEngine();

// Package:
//    engine.setupLoadPackagePath("../reg_rule_repo/stdBankCommon");
// File: One by One
    engine.setupLoadRuleFile("./src/test/resources/matchData.js");

    Matcher matcher = engine.issueMatcher();

    FileReader fileReader = new FileReader("./src/test/resources/matchingTestCases.js");
    JSStructureParser parser = new JSStructureParser(fileReader);
    Object obj = parser.parse();

    JSObject testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
    JSArray cases = (JSArray)testCaseContainer.get("test_cases");

    int instance = 0;
    for (Object objCase : cases) {
      if (objCase instanceof JSFunctionCall) {
        JSFunctionCall jsFunc = (JSFunctionCall)objCase;

        String rulename = jsFunc.getParameters().get(0).toString();
        if (filterRule == null || rulename.equals(filterRule)) {
          instance++;
          if (filterInstance != -1 && instance != filterInstance)
            continue;

          if (jsFunc.getName().equals("assertConfidence") && jsFunc.getParameters().size() == 4) {
            String ruleName = (String)jsFunc.getParameters().get(0);
            String confidence = (String)jsFunc.getParameters().get(1);
            JSObject jsObjLhs = (JSObject)jsFunc.getParameters().get(2);
            JSObject jsObjRhs = (JSObject)jsFunc.getParameters().get(3);
            assertConfidence(matcher, ruleName, confidence, jsObjLhs, jsObjRhs);
          }
          else
          if (jsFunc.getName().equals("assertNoMatch") && jsFunc.getParameters().size() == 3) {
            String ruleName = (String)jsFunc.getParameters().get(0);
            JSObject jsObjLhs = (JSObject)jsFunc.getParameters().get(1);
            JSObject jsObjRhs = (JSObject)jsFunc.getParameters().get(2);
            assertNoMatch(matcher, ruleName, jsObjLhs, jsObjRhs);
          }
          else {
            System.out.println("Unknown assertion function '" + jsFunc.getName() + "' with " + jsFunc.getParameters().size() + " parameters");
          }
        }
      }
    }
  }

  public static void main( String[] args ) throws Exception {
    Matchings test = new Matchings();
    long startMillis = System.currentTimeMillis();
    if (args.length == 1)
      test.runTestCases(args[0]);
    else
      test.runTestCases(null);
    long endMillis = System.currentTimeMillis();

    if (test.errorCount == 0) {
      System.out.println("Completed " + test.testCount + " tests Successfully :-)" );
      System.out.println("Run Time: " + (endMillis-startMillis) + " millseconds" );
    }
    else {
      if (test.errorCount == 1)
        System.out.println("Ran " + test.testCount + " tests with 1 error!");
      else
        System.out.println("Ran " + test.testCount + " tests with " + test.errorCount + " errors!");
    }
  }
}
