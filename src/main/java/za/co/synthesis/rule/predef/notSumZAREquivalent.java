package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 8:27 AM
 * Determines if the sum of money rand amounts is different to the ZAR equivalent value
 */
public class notSumZAREquivalent implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    BigDecimal total = new BigDecimal(0);
    BigDecimal sumValue = Util.number(context.getTransactionField("ZAREquivalent"));
    for (int i = 0; i < context.getMoneySize(); i++) {
      BigDecimal fvalue = Util.number(context.getMoneyField(i, "RandValue"));
      if (fvalue != null)
        total = total.add(fvalue);
    }
    return sumValue.compareTo(total) != 0;
  }
}
/*
function notSumZAREquivalent(context, value) {
  var total = 0;
  var sumValue = context.getTransactionField("ZAREquivalent");
  for ( var i = 0; i < context.getMoneySize(); i++) {
    var fvalue = context.getMoneyField(i, "RandValue");
    if (fvalue)
      total += Number(fvalue);
  }
  var a = Math.round(Number(sumValue) * 100);
  var b = Math.round(total * 100);
  return a != b;
}
*/