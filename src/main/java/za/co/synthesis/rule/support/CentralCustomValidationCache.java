package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.CachePeriod;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.support.CustomValidateCache.ValidateInputs;
import za.co.synthesis.rule.support.CustomValidateCache.ResponseCache;

import java.util.*;

public class CentralCustomValidationCache {
    public static class ValidationCacheEntry {
        private String validateName;
        private ValidateInputs validateInputs;
        private CustomValidateResult result;
        private ResponseCache responseCache;
        private Long expiryMillis = null;

        public String getValidateName() {
            return validateName;
        }

        public void setValidateName(String validateName) {
            this.validateName = validateName;
        }

        public ValidateInputs getValidateInputs() {
            return validateInputs;
        }

        public void setValidateInputs(ValidateInputs validateInputs) {
            this.validateInputs = validateInputs;
        }

        public CustomValidateResult getResult() {
            return result;
        }

        public void setResult(CustomValidateResult result) {
            this.result = result;
        }

        public ResponseCache getResponseCache() {
            return responseCache;
        }

        public void setResponseCache(ResponseCache responseCache) {
            this.responseCache = responseCache;
        }

        public Long getExpiryMillis() {
            return expiryMillis;
        }

        public void setExpiryMillis(Long expiryMillis) {
            this.expiryMillis = expiryMillis;
        }

        private boolean areObjectsTheSame(Object obj1, Object obj2) {
            if (obj1 != null) {
                if (!obj1.equals(obj2))
                    return false;
            }
            else {
                // Out validateName is null so if the other.validateName is not null then not the same
                if (obj2 != null)
                    return false;
            }
            return true;
        }

        public boolean equals(ValidationCacheEntry other) {
            if (!areObjectsTheSame(validateName, other.validateName)) {
                return false;
            }
            if (validateInputs != null) {
                if (!validateInputs.equals(other.validateInputs)) {
                    return false;
                }
            }
            else {
                if (validateInputs != null || other.validateInputs != null) {
                    return false;
                }
            }
            return true;
        }

        public int hashCode() {
            return generateHashCode(validateName, validateInputs);
        }

        public static int generateHashCode(String validateName, ValidateInputs validateInputs) {
            return validateName.hashCode() ^ validateInputs.hashCode();
        }
    }

    public static class ExpiryCacheEntry implements Comparable<ExpiryCacheEntry> {
        private long expiryMillis;
        private int hashCode;
        private FinsurvContext context;
        private ValidationCacheEntry validationCacheEntry;

        //Compares the t values using the comparator, if they are equal it uses the
        //node it - older nodes considered to be smaller..
        public int compareTo(ExpiryCacheEntry other){
            int comparison = (this.expiryMillis > other.expiryMillis) ? 1 : -1;
            if (this.expiryMillis == other.expiryMillis)
                comparison = 0;
            return comparison;
        }

        public long getExpiryMillis() {
            return expiryMillis;
        }

        public int getHashCode() {
            return hashCode;
        }

        public FinsurvContext getContext() {
            return context;
        }

        public ValidationCacheEntry getValidationCacheEntry() {
            return validationCacheEntry;
        }
    }


    private final CustomValidateRegistry validateRegistry;

    private long keepLongMillis = 1000 * 60 * 60 * 24 * 7; // 1 week
    private long keepMediumMillis = 1000 * 60 * 60; // 1 hour
    private long keepShortMillis = 1000 * 60; // 1 minute
    private boolean cleanupOnCache = true;

    private final PriorityQueue<ExpiryCacheEntry> expiryEntries = new PriorityQueue<ExpiryCacheEntry>();
    private final Map<Integer, List<ValidationCacheEntry>> cacheMap = new HashMap<Integer, List<ValidationCacheEntry>>();
    private final Map<FinsurvContext, List<ValidationCacheEntry>> contextCacheMap = new HashMap<FinsurvContext, List<ValidationCacheEntry>>();

    public synchronized void clear() {
        cacheMap.clear();
        contextCacheMap.clear();
    }

    public long getKeepLongMillis() {
        return keepLongMillis;
    }

    public void setKeepLongMillis(long keepLongMillis) {
        this.keepLongMillis = keepLongMillis;
    }

    public long getKeepMediumMillis() {
        return keepMediumMillis;
    }

    public void setKeepMediumMillis(long keepMediumMillis) {
        this.keepMediumMillis = keepMediumMillis;
    }

    public long getKeepShortMillis() {
        return keepShortMillis;
    }

    public void setKeepShortMillis(long keepShortMillis) {
        this.keepShortMillis = keepShortMillis;
    }

    public boolean isCleanupOnCache() {
        return cleanupOnCache;
    }

    public void setCleanupOnCache(boolean cleanupOnCache) {
        this.cleanupOnCache = cleanupOnCache;
    }

    public CentralCustomValidationCache(CustomValidateRegistry validateRegistry) {
        this.validateRegistry = validateRegistry;
    }

    private ValidationCacheEntry findMatch(List<ValidationCacheEntry> entryList, String validationName, ValidateInputs inputs) {
        ValidationCacheEntry result = null;
        for (ValidationCacheEntry entry : entryList) {
            if (entry.getValidateName().equals(validationName) && entry.getValidateInputs().equals(inputs)) {
                result = entry;
            }
        }
        return result;
    }

    private void linkContextToCache(FinsurvContext context, ValidationCacheEntry cacheEntry) {
        if (context != null) {
            if (contextCacheMap.containsKey(context)) {
                List<ValidationCacheEntry> entryList = contextCacheMap.get(context);
                ValidationCacheEntry matchedEntry = findMatch(entryList, cacheEntry.validateName, cacheEntry.validateInputs);
                if (matchedEntry != null) {
                    entryList.remove(matchedEntry);
                }
                entryList.add(cacheEntry);
            } else {
                List<ValidationCacheEntry> entryList = new ArrayList<ValidationCacheEntry>();
                entryList.add(cacheEntry);
                contextCacheMap.put(context, entryList);
            }
        }
    }

    private void removeCacheEntries(ExpiryCacheEntry ece) {
        if (cacheMap.containsKey(ece.hashCode)) {
            List<ValidationCacheEntry> entryList = cacheMap.get(ece.hashCode);
            if (entryList.size() > 1) {
                entryList.remove(ece.validationCacheEntry);

            } else {
                cacheMap.remove(ece.hashCode);
            }
        }
        if (ece.context != null) {
            if (contextCacheMap.containsKey(ece.context)) {
                List<ValidationCacheEntry> entryList = contextCacheMap.get(ece.context);
                if (entryList.size() > 1) {
                    entryList.remove(ece.validationCacheEntry);

                } else {
                    contextCacheMap.remove(ece.context);
                }
            }
        }
    }

    private void cleanupExpiredData(long currentMillis) {
        List<ExpiryCacheEntry> list = new ArrayList<ExpiryCacheEntry>();
        do {
            ExpiryCacheEntry ece = expiryEntries.peek();
            if (ece != null && ece.expiryMillis < currentMillis) {
                list.add(ece);
                expiryEntries.poll();
            }
            else {
                break;
            }
        } while(true);

        for (ExpiryCacheEntry ece : list) {
            removeCacheEntries(ece);
        }
    }

    public synchronized void cacheResult(String name, Object value, Object[] otherInputs,
                                         CustomValidateResult result, ResponseCache responseCache,
                                         Long setExpiryMillis) {
        cacheResult(null, name, new ValidateInputs(value, otherInputs), result, responseCache, setExpiryMillis);
    }

    public synchronized void cacheResult(String name, ValidateInputs inputs,
                                         CustomValidateResult result, ResponseCache responseCache,
                                         Long setExpiryMillis) {
        cacheResult(null, name, inputs, result, responseCache, setExpiryMillis);
    }

    public synchronized void cacheResult(FinsurvContext context, String name, ValidateInputs inputs,
                                         CustomValidateResult result, ResponseCache responseCache,
                                         Long setExpiryMillis) {
        CachePeriod cp = CachePeriod.Forever;
        CustomValidateRegistry.Entry entry = validateRegistry.get(name);
        if (entry != null && result != null) {
            cp = entry.getCachePeriodByStatus(result.getStatus());
        }
        if (cp == CachePeriod.Never)
            return;

        int hashCode = ValidationCacheEntry.generateHashCode(name, inputs);
        ValidationCacheEntry validationEntry = null;

        if (cacheMap.containsKey(hashCode)) {
            List<ValidationCacheEntry> entryList = cacheMap.get(hashCode);

            validationEntry = findMatch(entryList, name, inputs);
            if (validationEntry != null) {
                validationEntry.result = result;
                validationEntry.responseCache = responseCache;
                linkContextToCache(context, validationEntry);
            }
            else {
                validationEntry = new ValidationCacheEntry();
                validationEntry.validateName = name;
                validationEntry.validateInputs = new ValidateInputs(inputs.getValue(), inputs.getOtherInputs());
                validationEntry.result = result;
                validationEntry.responseCache = responseCache;
                entryList.add(validationEntry);
                linkContextToCache(context, validationEntry);
            }
        }
        else {
            validationEntry = new ValidationCacheEntry();
            validationEntry.validateName = name;
            validationEntry.validateInputs = new ValidateInputs(inputs.getValue(), inputs.getOtherInputs());
            validationEntry.result = result;
            validationEntry.responseCache = responseCache;

            List<ValidationCacheEntry> entryList = new ArrayList<ValidationCacheEntry>();
            entryList.add(validationEntry);
            cacheMap.put(hashCode, entryList);
            linkContextToCache(context, validationEntry);
        }

        if (cp != CachePeriod.Forever) {
            long currentMillis = System.currentTimeMillis();
            long expiryMillis;
            if (setExpiryMillis != null) {
                expiryMillis = setExpiryMillis;
            }
            else {
                expiryMillis = currentMillis;
                if (cp == CachePeriod.Long)
                    expiryMillis += keepLongMillis;
                else
                if (cp == CachePeriod.Medium)
                    expiryMillis += keepMediumMillis;
                else
                if (cp == CachePeriod.Short)
                    expiryMillis += keepShortMillis;
            }

            ExpiryCacheEntry ece = new ExpiryCacheEntry();
            ece.hashCode = hashCode;
            ece.expiryMillis = expiryMillis;
            ece.context = context;
            ece.validationCacheEntry = validationEntry;
            expiryEntries.add(ece);

            validationEntry.setExpiryMillis(expiryMillis);

            if (cleanupOnCache) {
                cleanupExpiredData(currentMillis);
            }
        }
    }

    public synchronized CustomValidateResult getResult(String name, ValidateInputs inputs) {
        int hashCode = ValidationCacheEntry.generateHashCode(name, inputs);
        if (cacheMap.containsKey(hashCode)) {
            List<ValidationCacheEntry> entryList = cacheMap.get(hashCode);

            ValidationCacheEntry matchedEntry = findMatch(entryList, name, inputs);
            if (matchedEntry != null) {
                return matchedEntry.result;
            }
        }
        return null;
    }

    public synchronized CustomValidateResult getResultAndAssociateContext(FinsurvContext context, String name, ValidateInputs inputs) {
        int hashCode = ValidationCacheEntry.generateHashCode(name, inputs);
        if (cacheMap.containsKey(hashCode)) {
            List<ValidationCacheEntry> entryList = cacheMap.get(hashCode);

            ValidationCacheEntry matchedEntry = findMatch(entryList, name, inputs);
            if (matchedEntry != null) {
                linkContextToCache(context, matchedEntry);
                return matchedEntry.result;
            }
        }
        return null;
    }

    public synchronized CustomValidateResult getResult(String name, Object value, Object[] otherInputs) {
        return getResult(name, new ValidateInputs(value, otherInputs));
    }

    public synchronized CustomValidateResult getResultAndAssociateContext(FinsurvContext context, String name, Object value, Object[] otherInputs) {
        return getResultAndAssociateContext(context, name, new ValidateInputs(value, otherInputs));
    }

    public synchronized ResponseCache getResponse(String name, ValidateInputs inputs) {
        int hashCode = ValidationCacheEntry.generateHashCode(name, inputs);
        if (cacheMap.containsKey(hashCode)) {
            List<ValidationCacheEntry> entryList = cacheMap.get(hashCode);

            ValidationCacheEntry matchedEntry = findMatch(entryList, name, inputs);
            if (matchedEntry != null) {
                return matchedEntry.responseCache;
            }
        }
        return null;
   }

    public synchronized List<ValidationCacheEntry> getTransactionValidationCacheList(FinsurvContext context) {
        return contextCacheMap.get(context);
    }

    public synchronized void expiredCleanup() {
        cleanupExpiredData(System.currentTimeMillis());
    }

    public int size() {
        int size = 0;
        for (List<ValidationCacheEntry> entry : cacheMap.values()) {
            size += entry.size();
        }
        return size;
    }

    public float sparsenessMetric() {
        float size = 0;
        for (List<ValidationCacheEntry> entry : cacheMap.values()) {
            size += entry.size();
        }
        return size/cacheMap.size();
    }
}
