package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 2:19 PM
 * Determines if the current value is not a valid CCN number
 */
public class notValidCCN implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value instanceof String) {
      String valueStr = (String)value;
      return !Util.isValidCCN(valueStr);
    }
    return true;
  }
}