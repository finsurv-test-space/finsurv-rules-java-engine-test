package za.co.synthesis.rule.core.impl;

import za.co.synthesis.rule.core.ISupporting;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif

/**
 * User: jake
 * Date: 8/7/14
 * Time: 1:26 PM
 * The default implementation of the Supporting interface. It returns the current date and time as the current date.
 */
public class DefaultSupporting implements ISupporting {
  private LocalDate currentDate = null;
  private LocalDate goLiveDate;

  public DefaultSupporting() {
    goLiveDate = LocalDate.of(2013, 8, 19);
  }

  public void setCurrentDate(LocalDate currentDate) {
    this.currentDate = currentDate;
  }

  public void setGoLiveDate(LocalDate goLiveDate) {
    this.goLiveDate = goLiveDate;
  }

  public LocalDate getCurrentDate() {
    if (currentDate == null)
      return LocalDate.now();
    else
      return currentDate;
  }

  public LocalDate getGoLiveDate() {
    return goLiveDate;
  }
}
