package za.co.synthesis.rule.evaluation;

/**
 * Created by jake on 6/28/16.
 */
public interface IAssumptionAssertionFunction {
  boolean execute(GivenInfo givenInfo);
}
