package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 1/28/16
 * Time: 12:45 PM
 * Check to see if all the monetary sequence amounts are in order
 */
public class notValidSequenceNumbers implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    boolean validSequenceNumbers = true;
    boolean missingSequenceNumbers = false;
    Object replacementIndicator = context.getTransactionField("ReplacementTransaction");
    if (replacementIndicator == null || !replacementIndicator.equals("Y")) {
      for (int moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
        Object fieldValue = context.getMoneyField(moneyInstance, "SequenceNumber");
        if (fieldValue != null) {
          BigDecimal seqNo = Util.number(fieldValue);
          if (missingSequenceNumbers || seqNo == null || seqNo.intValue() != moneyInstance+1)
            validSequenceNumbers = false;
        }
        else {
          missingSequenceNumbers = true;
        }
      }
    }
    return !validSequenceNumbers;
  }
}
/*
  _export.notValidSequenceNumbers = function notValidSequenceNumbers(context, value) {
    var validSequenceNumbers = true;
    var missingSequenceNumbers = false;
    var replacementIndicator = context.getTransactionField("ReplacementTransaction");
    if (!replacementIndicator || replacementIndicator !== "Y") {
      var moneyInstance;
      for (moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
        var fieldValue = context.getMoneyField(moneyInstance, "SequenceNumber");
        if (fieldValue) {
          if (missingSequenceNumbers || Number(fieldValue) != moneyInstance+1)
            validSequenceNumbers = false;
        }
        else {
          missingSequenceNumbers = true;
        }
      }
    }
    return !validSequenceNumbers;
  }
*/
