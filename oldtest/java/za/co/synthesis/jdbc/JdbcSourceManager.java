package za.co.synthesis.jdbc;

import za.co.synthesis.mapping.configuration.*;
import za.co.synthesis.mapping.source.ISourceData;
import za.co.synthesis.mapping.source.ISourceManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.sql.*;


/**
 * User: jake
 * Date: 1/8/16
 * Time: 9:51 AM
 * Connection to a jdbc data source to retrieve transaction and result data
 */
class JdbcDataSet {
  private final String code;
  private final String masterSrcCode;
  private final String select;
  private final String transaction_link;
  private final String money_link;
  private final String transaction_field;
  private final String money_field;
  private final String order_by;
  private final List<Map<String, String>> dataBuffer = new ArrayList<Map<String, String>>();
  private final DateFormat jsFormatter = new SimpleDateFormat("yyyy-MM-dd");
  //private final DateTimeFormatter jsFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  private static String stripPrefix(String name) {
    if (name != null) {
      int posDot = name.indexOf('.');
      if (posDot > -1) {
        return name.substring(posDot+1);
      }
    }
    return name;
  }

  JdbcDataSet(String code, String masterSrcCode, String select, String transaction_link, String money_link, String order_by) {
    this.code = code;
    this.masterSrcCode = masterSrcCode;
    this.select = select;
    this.transaction_link = transaction_link;
    this.money_link = money_link;
    this.transaction_field = stripPrefix(transaction_link);
    this.money_field = stripPrefix(money_link);
    this.order_by = order_by;
  }

  public void bufferResults(ResultSet rs) {
    try {
      ResultSetMetaData rsmd = rs.getMetaData();
      int columnCount = rsmd.getColumnCount();

      while(rs.next()){
        Map<String, String> rowMap = new HashMap<String, String>();

        for (int i=1; i<=columnCount; i++) {
          String colName = rsmd.getColumnName(i);
          int colType = rsmd.getColumnType(i);
          String value;
          if (colType == Types.TIMESTAMP || colType == Types.DATE) {
            Object objVal = rs.getObject(i);
            if (objVal != null) {
              value = jsFormatter.format(rs.getDate(i));
            }
            else
              value = null;
          }
          else {
            value = rs.getString(i);
          }
          rowMap.put(colName, value);
        }
        dataBuffer.add(rowMap);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void clear() {
    dataBuffer.clear();
  }

  String getCode() {
    return code;
  }

  String getMasterSrcCode() {
    return masterSrcCode;
  }

  String getSelect() {
    return select;
  }

  String getTransaction_link() {
    return transaction_link;
  }

  String getMoney_link() {
    return money_link;
  }

  String getTransaction_field() {
    return transaction_field;
  }

  String getMoney_field() {
    return money_field;
  }

  String getOrder_by() {
    return order_by;
  }

  public int getResultCount() {
    return dataBuffer.size();
  }

  public Map<String, String> getResult(int index) {
    return dataBuffer.get(index);
  }

  public List<Map<String, String>> getFocusedLevel1Results(String transactionValue) {
    List<Map<String, String>> focusBuffer = new ArrayList<Map<String, String>>();
    for (Map<String, String> entry : dataBuffer) {
      if (entry.get(transaction_field).equals(transactionValue)) {
        focusBuffer.add(entry);
      }
    }
    return focusBuffer;
  }

  public List<List<Map<String, String>>> getFocusedLevel2Results(JdbcDataSet level1Dataset, String transactionValue) {
    List<String> moneyOrder = new ArrayList<String>();

    for (Map<String, String> level1Entry : level1Dataset.dataBuffer) {
      if (level1Entry.get(level1Dataset.getTransaction_field()).equals(transactionValue)) {
        String moneyValue = level1Entry.get(level1Dataset.getMoney_field());
        if (!moneyOrder.contains(moneyValue))
          moneyOrder.add(moneyValue);
      }
    }

    Map<String, List<Map<String, String>>> moneyBuffer = new HashMap<String, List<Map<String, String>>>();
    for (Map<String, String> entry : dataBuffer) {
      if (entry.get(transaction_field).equals(transactionValue)) {
        String moneyValue = entry.get(money_field);
        List<Map<String, String>> focusBuffer;
        if (moneyBuffer.containsKey(moneyValue)) {
          focusBuffer = moneyBuffer.get(moneyValue);
        }
        else {
          focusBuffer = new ArrayList<Map<String, String>>();
          moneyBuffer.put(moneyValue, focusBuffer);
        }
        focusBuffer.add(entry);
      }
    }
    List<List<Map<String, String>>> result = new ArrayList<List<Map<String, String>>>();
    for (String money : moneyOrder) {
      result.add(moneyBuffer.get(money));
    }
    return result;
  }
}

class JdbcEntry {
  private String transactionValue;

  JdbcEntry(String transactionValue) {
    this.transactionValue = transactionValue;
  }

  String getTransactionValue() {
    return transactionValue;
  }
}

public class JdbcSourceManager implements ISourceManager {
  public interface PullReference {
    boolean isMore();
    boolean isRefString();
    String getNextReference();
  }

  private String jdbcDriver = "com.mysql.jdbc.Driver";
  private String dbUrl = "jdbc:mysql://localhost/EMP";
  private String dbUser = "username";
  private String dbPassword = "password";
  private int bufferSize = 1;

  private Connection conn = null;
  private PullReference pullReference = null;
  private Map<String, JdbcDataSet> dataSetMap = new HashMap<String, JdbcDataSet>();
  private List<String> pulledReferences = null;
  private int pulledReferencesCursor = 0;
  private JdbcDataSet mainDataSet = null;
  private int readTransactions = 0;

  public JdbcSourceManager() {
  }

  public JdbcSourceManager(String jdbcDriver) {
    this.jdbcDriver = jdbcDriver;
  }

  public JdbcSourceManager(String jdbcDriver, String dbUrl, String dbUser, String dbPassword) {
    this.jdbcDriver = jdbcDriver;
    this.dbUrl = dbUrl;
    this.dbUser = dbUser;
    this.dbPassword = dbPassword;
  }

  public String getJdbcDriver() {
    return jdbcDriver;
  }

  public void setJdbcDriver(String jdbcDriver) {
    this.jdbcDriver = jdbcDriver;
  }

  public String getDbUrl() {
    return dbUrl;
  }

  public void setDbUrl(String dbUrl) {
    this.dbUrl = dbUrl;
  }

  public String getDbUser() {
    return dbUser;
  }

  public void setDbUser(String dbUser) {
    this.dbUser = dbUser;
  }

  public String getDbPassword() {
    return dbPassword;
  }

  public void setDbPassword(String dbPassword) {
    this.dbPassword = dbPassword;
  }

  public int getBufferSize() {
    return bufferSize;
  }

  public void setBufferSize(int bufferSize) {
    this.bufferSize = bufferSize;
  }

  public Connection getConnection() {
    return conn;
  }

  public void open(PullReference pullReference) throws Exception {
    readTransactions = 0;
    this.pullReference = pullReference;
    Statement stmt = null;
    try{
      //STEP 2: Register JDBC driver
      Class.forName(jdbcDriver);

      //STEP 3: Open a connection
      System.out.println("Connecting to database...");
      this.conn = DriverManager.getConnection(dbUrl,dbUser,dbPassword);
    }
    catch(SQLException se) {
      //Handle errors for JDBC
      se.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  public void close() throws Exception {
    this.conn.close();
  }

  @Override
  public void setup(MappingConfig mappingConfig) {
    dataSetMap.clear();
    pulledReferencesCursor = 0;
    for (Source source : mappingConfig.getSources()) {
      Object description = source.configMap().get("description");
      Object master_src = source.configMap().get("master_src");
      Object select = source.configMap().get("select");
      Object transaction_field = source.configMap().get("transaction_field");
      Object money_field = source.configMap().get("money_field");
      Object order_by = source.configMap().get("order_by");

      String sDescription = description != null ? description.toString() : null;
      String sMaster_src = master_src != null ? master_src.toString() : null;
      String sSelect = select != null ? select.toString() : null;
      String sTransaction_field = transaction_field != null ? transaction_field.toString() : null;
      String sMoney_field = money_field != null ? money_field.toString() : null;
      String sOrder_by = order_by != null ? order_by.toString() : null;

      JdbcDataSet dataSet = new JdbcDataSet(source.getCode(), sMaster_src, sSelect, sTransaction_field, sMoney_field, sOrder_by);
      dataSetMap.put(source.getCode(), dataSet);

      if (sMaster_src == null)
        mainDataSet = dataSet;
    }

    for (Section section : mappingConfig.getSections()) {
      for (Field field : section.getFields()) {
        boolean isDate = false;
        if (field.getDataType() != null && field.getDataType().equals("date")) {
          isDate = true;
        }
        field.setExternalConfig(new JdbcExternalConfig(field.getSourceColName(), isDate));
      }
    }
  }

  public static boolean numericTest (Object obj){
    boolean isNumeric = obj.getClass().isPrimitive();
//    try{
//      //Long x = Long((long) obj.getClass().isPrimitive());
//      isNumeric = obj.getClass().isPrimitive();
//    } catch (Exception error){}
    return isNumeric;
  }

  private void loadEntries() {
    pulledReferencesCursor = 0;
    pulledReferences = new ArrayList<String>();

    boolean isRefString = pullReference.isRefString();
    for (int i=0; i<bufferSize; i++) {
      String ref = pullReference.getNextReference();
      if (ref == null)
        break;
      pulledReferences.add(ref);
    }

    for (JdbcDataSet dataSet : dataSetMap.values()) {
      dataSet.clear();
      StringBuilder inClause = new StringBuilder();

      for (String ref : pulledReferences) {
        if (inClause.length() > 0)
          inClause.append(",");
        if (isRefString || (!numericTest(ref))) {
          isRefString = true;
          inClause.append("'").append(ref).append("'");
        } else {
          inClause.append(ref);
        }
      }

      StringBuilder selectClause = new StringBuilder();
      selectClause.append(dataSet.getSelect()).append(" WHERE ").append(dataSet.getTransaction_link());
      selectClause.append(" IN (").append(inClause).append(")");
      selectClause.append(" ORDER BY ").append(dataSet.getOrder_by());

      Statement stmt = null;
      ResultSet rs = null;
      try {
        stmt = conn.createStatement();
        rs = stmt.executeQuery(selectClause.toString());
        dataSet.bufferResults(rs);

      } catch (SQLException e) {
        e.printStackTrace();
        System.out.println("\r\n\r\n"+selectClause.toString()+"\r\n\r\n");
      }
      finally {
        try {
          if(rs!=null)
            rs.close();
          if(stmt!=null)
            stmt.close();
        } catch(SQLException se2){
          // nothing we can do
          se2.printStackTrace();
        }
      }
    }
    readTransactions += pulledReferences.size();
    System.out.println("read " + readTransactions);
  }

  @Override
  public Object getNextEntry() {
    if (pulledReferences == null || (pulledReferencesCursor >= pulledReferences.size() && pullReference.isMore()))
      loadEntries();

    JdbcEntry nextLine = null;
    if (pulledReferencesCursor < pulledReferences.size()) {
      nextLine = new JdbcEntry(pulledReferences.get(pulledReferencesCursor++));
    }
    return nextLine;
  }

  @Override
  public ISourceData getSourceData(Object entry, String source, MappingScope mappingScope) {
    JdbcDataSet dataSet = dataSetMap.get(source);
    JdbcEntry jdbcEntry = (JdbcEntry)entry;

    if (mappingScope == MappingScope.Transaction || mappingScope == MappingScope.Results) {
      return new JdbcSourceData(0, dataSet.getFocusedLevel1Results(jdbcEntry.getTransactionValue()), null);
    }
    if (mappingScope == MappingScope.Money) {
      return new JdbcSourceData(1, dataSet.getFocusedLevel1Results(jdbcEntry.getTransactionValue()), null);
    }
    if (mappingScope == MappingScope.ImportExport) {
      JdbcDataSet masterDataSet = dataSetMap.get(dataSet.getMasterSrcCode());

      return new JdbcSourceData(2, null, dataSet.getFocusedLevel2Results(masterDataSet, jdbcEntry.getTransactionValue()));
    }
    return null;
  }
}

