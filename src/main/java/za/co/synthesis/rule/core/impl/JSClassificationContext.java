package za.co.synthesis.rule.core.impl;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.IClassificationContext;

/**
 * Created by jake on 6/7/16.
 */
public class JSClassificationContext implements IClassificationContext {
  private final JSObject jsObj;

  public JSClassificationContext(JSObject jsObj) {
    this.jsObj = jsObj;
  }

  public Object getDataField(Object objLevel, final String field) {
    String[] fields = field.split("\\.");
    String fieldName = null;
    for (int i = 0; i < fields.length; i++) {
      fieldName = fields[i];
      objLevel = ((JSObject) objLevel).get(fieldName);
      if (objLevel == null) {
        if (i >= fields.length - 1)
          return null;
        else {
          return null;
        }
      }
    }
    return objLevel;
  }

  public boolean hasField(Object obj, final String field) {
    if (obj instanceof JSObject) {
      return ((JSObject) obj).get(field) != null;
    }
    return false;
  }

  public Object getField(final String field) {
    return getDataField(jsObj, field);
  }

  public void logEvent(final String classificationType, final String ruleName, final String fieldName) {
  }
}


