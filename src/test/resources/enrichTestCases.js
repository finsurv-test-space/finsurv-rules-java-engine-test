define(['testDisplayBase'], function (testBase) {
  with (testBase) {
    var test_cases = [
      assertValue("Resident.Entity.EntityName", "Ewfj vt--x+sw AF",
        {ReportingQualifier: "BOPCUS", Resident : {Entity : {EntityName : "Ewfj  vt--x.+s/w AF"}}}),
      assertValue("Resident.Entity.EntityName", "Shepherd Inc",
        {ReportingQualifier: "BOPCUS", Resident : {Entity : {EntityName : "Shepherd  Inc"}}}),
      assertFieldValue("NonResident.Individual", "Resident.Exception.ExceptionName", "MUTUAL PARTY",
        { ReportingQualifier: "BOPCUS", Flow: "IN",
          NonResident : {Individual : {}},
          MonetaryAmount: [{CategoryCode: "250"}]}),
      assertValue("Resident.Individual.DateOfBirth", "1968-11-03",
        {ReportingQualifier: "BOPCUS", Resident : {Individual : {IDNumber : "6811035039084"}}}),
      assertValue("Resident.Individual.DateOfBirth", "1968-11-03",
        {Resident : {Individual : {DateOfBirth: "1900-01-01", IDNumber : "6811035039084"}}}),
      assertValue("Resident.Individual.DateOfBirth", "1922-01-01",
          {Resident : {Individual : {DateOfBirth: "1900-01-01", IDNumber : "2201015153082"}}}),
      assertValue("Resident.Individual.DateOfBirth", "1918-01-01",
          {Resident : {Individual : {DateOfBirth: "1900-01-01", IDNumber : "1801015153082"}}}),
      assertValue("Resident.Individual.DateOfBirth", "2015-01-01",
          {Resident : {Individual : {DateOfBirth: "1900-01-01", IDNumber : "1501015153082"}}}),
      assertValue("Resident.Individual.DateOfBirth", "1968-11-02",
        {ReportingQualifier: "BOPCUS", Resident : {Individual : {DateOfBirth: "1968-11-02", IDNumber : "6811035039084"}}}),
      assert2FieldValues("Resident.Individual.AccountIdentifier",
        "NonResident.Individual.AccountIdentifier", "NON RESIDENT RAND",
        "Resident.Exception.ExceptionName", "NON RESIDENT RAND",
        { ReportingQualifier: "BOPCUS",
          Flow: "IN",
          Resident : {Individual : {AccountIdentifier : "NON RESIDENT RAND"}},
          NonResident : {Individual : {}}
        })
    ]
  }
});
