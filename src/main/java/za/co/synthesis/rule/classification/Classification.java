package za.co.synthesis.rule.classification;

import za.co.synthesis.rule.core.*;

/**
 * Created by jake on 6/7/16.
 */
public class Classification {
  private final String field;
  private final String name;
  private final String type;
  private final IClassificationFunction assertion;

  public Classification(String name, String field, String type, IClassificationFunction assertion) {
    this.name = name;
    this.field = field;
    this.type = type;
    this.assertion = assertion;
  }

  private boolean mustRunRule(String field, Object value) {
    return (value == null || !(value instanceof IClassificationContext.Undefined));
  }

  public boolean run(ClassificationContext context) {
    Object value;

    value = context.getField(field);
    if (mustRunRule(field, value)) {
      if (assertion == null || assertion.execute(context, value)) {
        context.logEvent(type, name, field);
      }
    }
    return true;
  }
}
