package za.co.synthesis.rule.evaluation;

public interface IMatchAssertionFunction {
  boolean execute(MatchInfo matchInfo);
}