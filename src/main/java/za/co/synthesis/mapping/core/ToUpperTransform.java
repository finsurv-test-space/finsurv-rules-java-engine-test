package za.co.synthesis.mapping.core;

/**
 * User: jake
 * Date: 3/19/16
 * Time: 11:26 PM
 * Converts to upper case
 */
public class ToUpperTransform implements ITransform {

  @Override
  public String transform(String value) {
    return value.toUpperCase();
  }
}
