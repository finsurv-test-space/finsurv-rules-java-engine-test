package za.co.synthesis;

import org.junit.Assert;
import org.junit.Test;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.*;
import za.co.synthesis.rule.support.CentralCustomValidationCache;
import za.co.synthesis.rule.support.CustomValidateCache;
import za.co.synthesis.rule.support.CustomValidateRegistry;
import za.co.synthesis.rule.support.Util;
import za.co.synthesis.validate.*;

import java.io.FileReader;
import java.math.BigDecimal;
import java.net.URL;
import java.util.*;

import static org.junit.Assert.assertTrue;


/**
 * User: jake
 * Date: 8/7/14
 * Time: 3:16 PM
 * Runs all the JavaScript test cases
 */
public class TestExternalValidation {
    private static class ExtraAssertion {
        private int expectCachePuts;
        private int expectCacheHits;
        private String expectCode;

        public ExtraAssertion() {
            expectCachePuts = 0;
            expectCacheHits = 0;
            expectCode = null;
        }
    }

    List<String> testRuleNames = new ArrayList<String>();
    int testCount = 0;
    int errorCount = 0;
    boolean debugPrintout = false;

    private int countRuleInstances(String rulename) {
        int count = 0;
        for (String name : testRuleNames) {
            if (name.equals(rulename)) {
                count++;
            }
        }
        return count;
    }

    public TestExternalValidation() {
        TestUtility.setNotLUClientCCN("11111111");
        TestUtility.setLUClientCCN("22222222");
        TestUtility.setInvalidCCN("33333333");
        TestUtility.setInvalidCCN("44444444");

        TestUtility.setTrnRef("ExistingIN101Trn", FlowType.Inflow, "101/01");
        TestUtility.setTrnRef("ExistingOUT101Trn", FlowType.Outflow, "101/01");
        TestUtility.setCancelledTrnRef("ExistingINTrnCancelled", FlowType.Inflow);
        TestUtility.setCancelledTrnRef("ExistingOUTTrnCancelled", FlowType.Outflow);
        TestUtility.setTrnRefNotInDB("MissingTrn");

        TestUtility.setLoanRef("ValidLoan");
        TestUtility.setLoanRef("InvalidLoan");
    }

    private ExtraAssertion getExtraAssertion(JSObject jsCustomData) {
        Object objAssert = jsCustomData.get("Assert");
        if (objAssert instanceof JSObject) {
            ExtraAssertion result = new ExtraAssertion();
            JSObject jsAssert = (JSObject)objAssert;
            Object objCachePuts = jsAssert.get("ValidationCachePuts");
            Object objCacheHits = jsAssert.get("ValidationCacheHits");
            Object objCode = jsAssert.get("Code");

            if (objCachePuts instanceof String) {
                result.expectCachePuts = Integer.parseInt((String)objCachePuts);
            }
            if (objCacheHits instanceof String) {
                result.expectCacheHits = Integer.parseInt((String)objCacheHits);
            }
            if (objCode instanceof String) {
                result.expectCode = (String)objCode;
            }
            return result;
        }
        return null;
    }

    private String stringifyJSObject(JSObject js) {
        JSWriter writer = new JSWriter();
        writer.setIndent("  ");
        writer.setNewline("\n");
        js.compose(writer);
        return writer.toString();
    }

    private void assertRuleBase(Validator validator, String rulename, JSObject jsObj, JSObject jsCustomData,
                                ResultType expectType, int number, boolean mustMatchRule) {

        int oldErrorCount = errorCount;
        testRuleNames.add(rulename);
        testCount++;

        if (debugPrintout) {
            System.out.println("---->>>> Running test: " + rulename + '#' + countRuleInstances(rulename));
            System.out.println("Input Meta: " + stringifyJSObject(jsCustomData));
            System.out.println("Input Report: " + stringifyJSObject(jsObj));
        }

        // The central validation cache stores validation results across validations and therefore there is a
        // need to clear this cache between each new test case
        validator.getCentralValidateCache().clear();

        ExtraAssertion extraAssertion = getExtraAssertion(jsCustomData);

        ValidationStats stats = new ValidationStats();
        List<ResultEntry> fullResults = validator.validateRule(rulename, jsObj, jsCustomData, new JSCustomValues(jsCustomData), stats);
        List<ResultEntry> results = new ArrayList<ResultEntry>();
        for (ResultEntry entry : fullResults) {
            if (entry.getType() != ResultType.Mandatory)
                results.add(entry);
        }

        boolean rulesFound = stats.getRunRules() > 0;
        if (!rulesFound && mustMatchRule) {
            errorCount++;
            System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - No matching rule found");
        }
        else {
            if (expectType == null) {
                if (results.size() > 0) {
                    errorCount++;
                    System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting Success, but raised " + results.size() + ": " + results.get(0).getType().toString());
                }
                else {
                    // Success so far... Now lets check the extra assertions
                    if (extraAssertion != null) {
                        if (extraAssertion.expectCachePuts != stats.getValidationCachePuts()) {
                            errorCount++;
                            System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) +
                                    " - Result Correct, expecting " + extraAssertion.expectCachePuts +
                                    " validation cache puts, but instead got " + stats.getValidationCachePuts());
                        }
                        else
                        if (extraAssertion.expectCacheHits != stats.getValidationCacheHits()) {
                            errorCount++;
                            System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) +
                                    " - Result Correct, expecting " + extraAssertion.expectCacheHits +
                                    " validation cache hits, but instead got " + stats.getValidationCacheHits());
                        }
                    }
                }
            }
            else {
                if (results.size() == 0 || results.get(0).getType() != expectType || results.size() != number) {
                    errorCount++;
                    if (results.size() == 0)
                        System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting " + expectType.toString() + ", but instead succeeded");
                    else
                    if (results.get(0).getType() != expectType)
                        System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting " + expectType.toString() + ", but instead raised: " + results.get(0).getType().toString());
                    else
                    if (results.size() != number)
                        System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting " + number + 'x' + expectType.toString() + ", but only raised " + results.size());
                }
                else {
                    // Success so far... Now lets check the extra assertions
                    String firstCode = null;
                    if (results.size() > 0) {
                        firstCode = results.get(0).getCode();
                    }
                    if (extraAssertion != null) {
                        if (extraAssertion.expectCachePuts != stats.getValidationCachePuts()) {
                            errorCount++;
                            System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) +
                                    " - Result Correct, expecting " + extraAssertion.expectCachePuts +
                                    " validation cache puts, but instead got " + stats.getValidationCachePuts());
                        }
                        else
                        if (extraAssertion.expectCacheHits != stats.getValidationCacheHits()) {
                            errorCount++;
                            System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) +
                                    " - Result Correct, expecting " + extraAssertion.expectCacheHits +
                                    " validation cache hits, but instead got " + stats.getValidationCacheHits());
                        }
                        else
                        if (extraAssertion.expectCode != null && !extraAssertion.expectCode.equals(firstCode)) {
                            errorCount++;
                            System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) +
                                    " - Result Correct, expecting Code '" + extraAssertion.expectCode +
                                    "' , but instead got '" + firstCode + "'");
                        }
                    }
                }
            }
        }
        if (debugPrintout) {
            if (oldErrorCount == errorCount) {
                System.out.println("-->> Test Success");
            }
            System.out.println("Output Meta: " + stringifyJSObject(jsCustomData));
        }
    }

    private void assertFailure(Validator validator, String rulename, JSObject jsObj) {
        JSObject customData = new JSObject();
        customData.put("DealerType", "AD");
        assertRuleBase(validator, rulename, jsObj, customData, ResultType.Error, 1, true);
    }

    private void assert2Failures(Validator validator, String rulename, JSObject jsObj) {
        JSObject customData = new JSObject();
        customData.put("DealerType", "AD");
        assertRuleBase(validator, rulename, jsObj, customData, ResultType.Error, 2, true);
    }

    private void assertWarning(Validator validator, String rulename, JSObject jsObj) {
        JSObject customData = new JSObject();
        customData.put("DealerType", "AD");
        assertRuleBase(validator, rulename, jsObj, customData, ResultType.Warning, 1, true);
    }

    private void assertDeprecated(Validator validator, String rulename, JSObject jsObj) {
        JSObject customData = new JSObject();
        customData.put("DealerType", "AD");
        assertRuleBase(validator, rulename, jsObj, customData, ResultType.Deprecated, 1, true);
    }

    private void assertSuccess(Validator validator, String rulename, JSObject jsObj) {
        JSObject customData = new JSObject();
        customData.put("DealerType", "AD");
        assertRuleBase(validator, rulename, jsObj, customData, null, 1, true);
    }

    private void assertNoRule(Validator validator, String rulename, JSObject jsObj) {
        JSObject customData = new JSObject();
        customData.put("DealerType", "AD");
        assertRuleBase(validator, rulename, jsObj, customData, null, 1, false);
    }

    private void assertFailureCustom(Validator validator, String rulename, JSObject jsObj, JSObject customData) {
        assertRuleBase(validator, rulename, jsObj, customData, ResultType.Error, 1, true);
    }

    private void assert2FailuresCustom(Validator validator, String rulename, JSObject jsObj, JSObject customData) {
        assertRuleBase(validator, rulename, jsObj, customData, ResultType.Error, 2, true);
    }

    private void assertWarningCustom(Validator validator, String rulename, JSObject jsObj, JSObject customData) {
        assertRuleBase(validator, rulename, jsObj, customData, ResultType.Warning, 1, true);
    }

    private void assertDeprecatedCustom(Validator validator, String rulename, JSObject jsObj, JSObject customData) {
        assertRuleBase(validator, rulename, jsObj, customData, ResultType.Deprecated, 1, true);
    }

    private void assertSuccessCustom(Validator validator, String rulename, JSObject jsObj, JSObject customData) {
        assertRuleBase(validator, rulename, jsObj, customData, null, 1, true);
    }

    private void assertNoRuleCustom(Validator validator, String rulename, JSObject jsObj, JSObject customData) {
        assertRuleBase(validator, rulename, jsObj, customData, null, 1, false);
    }

    private boolean applyAssertionFunction(Validator validator, JSFunctionCall jsFunc) {
        String rulename = jsFunc.getParameters().get(0).toString();

        if (jsFunc.getName().equals("assertFailure") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            assertFailure(validator, rulename, jsObj);
        }
        else
        if (jsFunc.getName().equals("assert2Failures") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            assert2Failures(validator, rulename, jsObj);
        }
        else
        if (jsFunc.getName().equals("assertWarning") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            assertWarning(validator, rulename, jsObj);
        }
        else
        if (jsFunc.getName().equals("assertDeprecated") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            assertDeprecated(validator, rulename, jsObj);
        }
        else
        if (jsFunc.getName().equals("assertNoRule") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            assertNoRule(validator, rulename, jsObj);
        }
        else
        if (jsFunc.getName().equals("assertSuccess") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            assertSuccess(validator, rulename, jsObj);
        }
        else
        if (jsFunc.getName().equals("assertFailureCustom") && jsFunc.getParameters().size() == 3) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
            assertFailureCustom(validator, rulename, jsObj, jsCustom);
        }
        else
        if (jsFunc.getName().equals("assert2FailuresCustom") && jsFunc.getParameters().size() == 3) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
            assert2FailuresCustom(validator, rulename, jsObj, jsCustom);
        }
        else
        if (jsFunc.getName().equals("assertWarningCustom") && jsFunc.getParameters().size() == 3) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
            assertWarningCustom(validator, rulename, jsObj, jsCustom);
        }
        else
        if (jsFunc.getName().equals("assertDeprecatedCustom") && jsFunc.getParameters().size() == 3) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
            assertDeprecatedCustom(validator, rulename, jsObj, jsCustom);
        }
        else
        if (jsFunc.getName().equals("assertSuccessCustom") && jsFunc.getParameters().size() == 3) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
            assertSuccessCustom(validator, rulename, jsObj, jsCustom);
        }
        else
        if (jsFunc.getName().equals("assertNoRuleCustom") && jsFunc.getParameters().size() == 3) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            JSObject jsCustom = (JSObject)jsFunc.getParameters().get(2);
            assertNoRuleCustom(validator, rulename, jsObj, jsCustom);
        }
        else {
            System.out.println("Unknown assertion function '" + jsFunc.getName() + "' with " + jsFunc.getParameters().size() + " parameters");
            return false;
        }
        return true;
    }

    private boolean runTestCases() throws Exception {
        ValidationEngine engine = new ValidationEngine();

// Package:
        engine.setupLoadPackagePath("../finsurv-rules/src/tests/reg_rule_tests/testExternal");

        DefaultSupporting supporting = new DefaultSupporting();
        supporting.setCurrentDate(Util.date("2013-01-20"));
        supporting.setGoLiveDate(Util.date("2013-08-18"));
        engine.setupSupporting(supporting);

        TestUtility.registerTestFunctions(engine);

        Validator validator = engine.issueValidator();

        FileReader fileReader = new FileReader("../finsurv-rules/src/tests/reg_rule_tests/testExternal/tests/testCases.js");
        JSStructureParser parser = new JSStructureParser(fileReader);
        Object obj = parser.parse();

        JSObject testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
        if (testCaseContainer != null) {
            JSArray cases = (JSArray)((JSObject)testCaseContainer).get("test_cases");

            for (Object objCase : cases) {
                if (objCase instanceof JSFunctionCall) {
                    JSFunctionCall jsFunc = (JSFunctionCall)objCase;
                    applyAssertionFunction(validator, jsFunc);
                }
            }
        }

        if (errorCount == 0) {
            System.out.println("Completed " + testCount + " tests Successfully :-)" );
        }
        else {
            if (errorCount == 1) {
                System.out.println("Ran " + testCount + " tests with 1 error!");
            }
            else {
                System.out.println("Ran " + testCount + " tests with " + errorCount + " errors!");
            }
        }
        return (errorCount == 0);
    }

    @Test
    public void TestExternals() throws Exception {
        TestExternalValidation testChannels = new TestExternalValidation();

        Assert.assertTrue("Some tests have failed!", testChannels.runTestCases());
    }

    public static void main( String[] args ) throws Exception {
        TestExternalValidation testChannels = new TestExternalValidation();

        testChannels.TestCentralCache();
        testChannels.debugPrintout = false;
        testChannels.runTestCases();
    }

    @Test
    public void TestCentralCache() throws Exception {
        final String PASS = "Pass";
        final String FAIL = "Fail";
        final String ERROR = "ERROR";

        CustomValidateRegistry registry = new CustomValidateRegistry();
        Validate_Dummy pass = new Validate_Dummy(StatusType.Pass);
        Validate_Dummy fail = new Validate_Dummy(StatusType.Fail);
        Validate_Dummy error = new Validate_Dummy(StatusType.Error);
        CacheStrategy cs = CacheStrategy.set(StatusType.Pass, CachePeriod.Long)
                .and(StatusType.Fail, CachePeriod.Medium)
                .and(StatusType.Error, CachePeriod.Short);

        registry.register(PASS, pass, new String[0], cs);
        registry.register(FAIL, fail, new String[0], cs);
        registry.register(ERROR, error, new String[0], cs);

        CentralCustomValidationCache valCache = new CentralCustomValidationCache(registry);

        valCache.setCleanupOnCache(false);
        valCache.setKeepLongMillis(1000);
        valCache.setKeepMediumMillis(500);
        valCache.setKeepShortMillis(100);

        CustomValidateCache.ValidateInputs inputs = new CustomValidateCache.ValidateInputs("MyValue", new Object[0]);
        CustomValidateResult passResult = pass.call(inputs.getValue(), inputs.getOtherInputs());
        CustomValidateResult failResult = fail.call(inputs.getValue(), inputs.getOtherInputs());
        CustomValidateResult errorResult = error.call(inputs.getValue(), inputs.getOtherInputs());

        valCache.cacheResult(PASS, inputs, passResult, null, null);
        valCache.cacheResult(FAIL, inputs, failResult, null, null);
        valCache.cacheResult(ERROR, inputs, errorResult, null, null);

        int blastRadius = 10000;
        // Blast the cache
        for (int i=0; i<blastRadius; i++) {
            Object[] objs = new Object[1];
            objs[0] = i;
            CustomValidateCache.ValidateInputs randomInputs = new CustomValidateCache.ValidateInputs("Value#"+ i, objs);

            valCache.cacheResult(PASS, randomInputs, passResult, null, null);
            valCache.cacheResult(FAIL, randomInputs, failResult, null, null);
            valCache.cacheResult(ERROR, randomInputs, errorResult, null, null);
        }

        int startCacheCount = (valCache.getResult(PASS, inputs) != null) ? 1 : 0;
        startCacheCount += (valCache.getResult(FAIL, inputs) != null) ? 1 : 0;
        startCacheCount += (valCache.getResult(ERROR, inputs) != null) ? 1 : 0;

        int cacheSize = valCache.size();
        System.out.println("At start: Cache Size = " + valCache.size());
        System.out.println("At start: Sparseness Metric = " + valCache.sparsenessMetric());
        Assert.assertEquals("At start: Expecting " + (3*blastRadius + 3) + " cached entries", 3*blastRadius + 3, cacheSize);

        Assert.assertEquals("Expecting to find 3 specific cached entries", 3, startCacheCount);

        Thread.sleep(120);
        valCache.expiredCleanup();
        cacheSize = valCache.size();
        System.out.println("After 120 ms: Cache Size = " + cacheSize);
        Assert.assertTrue("After 120 ms: Expecting PASS to be here", (valCache.getResult(PASS, inputs) != null));
        Assert.assertTrue("After 120 ms: Expecting FAIL to be here", (valCache.getResult(FAIL, inputs) != null));
        Assert.assertFalse("After 120 ms: Expecting ERROR to be missing", (valCache.getResult(ERROR, inputs) != null));
        Assert.assertEquals("After 120 ms: Expecting " + (2*blastRadius + 2) + " cached entries", 2*blastRadius + 2, cacheSize);

        Thread.sleep(400);
        valCache.expiredCleanup();
        cacheSize = valCache.size();
        System.out.println("After 520 ms: Cache Size = " + cacheSize);
        Assert.assertTrue("After 520 ms: Expecting PASS to be here", (valCache.getResult(PASS, inputs) != null));
        Assert.assertFalse("After 520 ms: Expecting FAIL to be missing", (valCache.getResult(FAIL, inputs) != null));
        Assert.assertFalse("After 520 ms: Expecting ERROR to be missing", (valCache.getResult(ERROR, inputs) != null));
        Assert.assertEquals("After 520 ms: Expecting " + (1*blastRadius + 1) + " cached entries", 1*blastRadius + 1, cacheSize);

        Thread.sleep(500);
        valCache.expiredCleanup();
        cacheSize = valCache.size();
        System.out.println("After 1020 ms: Cache Size = " + cacheSize);
        Assert.assertFalse("After 1020 ms: Expecting PASS to be missing", (valCache.getResult(PASS, inputs) != null));
        Assert.assertFalse("After 1020 ms: Expecting FAIL to be missing", (valCache.getResult(FAIL, inputs) != null));
        Assert.assertFalse("After 1020 ms: Expecting ERROR to be missing", (valCache.getResult(ERROR, inputs) != null));
        Assert.assertEquals("After 1020 ms: Expecting 0 cached entries", 0, cacheSize);
    }

    private Validator getValidator() throws Exception {
      ValidationEngine engine = new ValidationEngine();

      engine.setupLoadPackagePath("../finsurv-rules/src/tests/reg_rule_tests/testExternal");
      //engine.setupLoadRuleUrl(new URL("http://127.0.0.1:8081/rules-management/api/rules/coreSARBExternal/validation"));
      //engine.setupLoadLookupUrl(new URL("http://127.0.0.1:8081/rules-management/api/rules/coreSARBExternal/lookups"));

      DefaultSupporting supporting = new DefaultSupporting();
      supporting.setCurrentDate(Util.date("2013-01-20"));
      supporting.setGoLiveDate(Util.date("2013-08-18"));
      engine.setupSupporting(supporting);

      CacheStrategy cacheStrategy = CacheStrategy.set(StatusType.Pass, CachePeriod.Long).
              and(StatusType.Fail, CachePeriod.Medium).
              and(StatusType.Error, CachePeriod.Short);

      engine.registerCustomValidate("Validate_ImportUndertakingCCN", cacheStrategy,
              new Validate_Example("Validate_ImportUndertakingCCN", StatusType.Pass));

      engine.registerCustomValidate("Validate_ValidCCN", cacheStrategy,
              new Validate_Example("Validate_ValidCCN", StatusType.Pass));

      engine.registerCustomValidate("Validate_ValidCCNinUCR", cacheStrategy,
              new Validate_Example("Validate_ValidCCNinUCR", StatusType.Pass));

      engine.registerCustomValidate("Validate_ReversalTrnRef", cacheStrategy,
              new Validate_Example("Validate_ReversalTrnRef", StatusType.Pass),
              "transaction::Flow", "ReversalTrnSeqNumber");

      engine.registerCustomValidate("Validate_ReplacementTrnReference", cacheStrategy,
              new Validate_Example("Validate_ReplacementTrnReference", StatusType.Pass),
              "TrnReference", "ReplacementTransaction", "ReplacementOriginalReference");

      engine.registerCustomValidate("Validate_IndividualSDA", cacheStrategy,
              new Validate_Example("Validate_IndividualSDA", StatusType.Pass),
              "transaction::TrnReference", "transaction::ValueDate", "function::localAmountForIDNumber");

      engine.registerCustomValidate("Validate_LoanRef", cacheStrategy,
              new Validate_Example("Validate_LoanRef", StatusType.Pass));

      engine.registerCustomValidate("Validate_MRNonIVS", cacheStrategy,
              new Validate_Example("Validate_MRNonIVS", StatusType.Pass),
              "ImportControlNumber");

      engine.getCentralValidateCache().setKeepLongMillis(1000*60*60*24); // 1 day
      engine.getCentralValidateCache().setKeepMediumMillis(1000*60*60); // 1 hour
      engine.getCentralValidateCache().setKeepShortMillis(1000); // 1 second

      return engine.issueValidator();
    }

    private String testMessage() {
      return  "{" +
              "  ReportingQualifier: 'BOPCUS', Flow: 'OUT'," +
              "  TrnReference: 'TestMessageRef'," +
              "  ValueDate: '2020-12-25'," +
              "  ReplacementOriginalReference: 'OrigTrnRef'," +
              "  ReplacementTransaction: 'Y', " +
              "  Resident: { Individual: { CustomsClientNumber: '12345678', IDNumber: '6811035039084' }}," +
              "  MonetaryAmount: [" +
              "    {" +
              "       DomesticValue: '20000.10'," +
              "       AdHocRequirement: { Subject: 'SDA' }" +
              "    }" +
              "  ]" +
              "}";
    }

    private void printStats(ValidationStats stats) {
      System.out.println("Validation Stats");
      System.out.println("================");
      System.out.println("Rules run: " + stats.getRunRules());
      System.out.println("Errors: " + stats.getErrorCount());
      System.out.println("Warnings: " + stats.getWarningCount());
      System.out.println("Cache Hits: " + stats.getValidationCacheHits());
      System.out.println("Cache Puts: " + stats.getValidationCachePuts());
    }

    private void printMeta(JSObject meta) {
      JSWriter writer = new JSWriter();
      writer.setNewline("\n");
      writer.setIndent("  ");

      meta.compose(writer);
      System.out.println("Meta");
      System.out.println("====");
      System.out.println(writer.toString());
    }

    @Test
    public void exampleValidations() throws Exception {
      Validator validator = getValidator();

      JSStructureParser parser = new JSStructureParser(testMessage());
      Object obj = parser.parse();

      if (obj instanceof JSObject) {
        JSObject meta = new JSObject();
        JSFinsurvContext jsFinsurvContext = new JSFinsurvContext((JSObject)obj, meta);
        ICustomValue customValues = new DefaultCustomValues();
        ValidationStats stats = new ValidationStats();

        System.out.println("Validating Test Message");
        System.out.println("=======================");

        validator.validate(jsFinsurvContext, customValues, stats);

        printStats(stats);

        System.out.println();
        printMeta(meta);
      }
    }

  @Test
  public void prepopulateCentralCache() throws Exception {
    Validator validator = getValidator();
    CentralCustomValidationCache cache = validator.getCentralValidateCache();

    cache.cacheResult("Validate_ImportUndertakingCCN",
            "12345678", null,
            new CustomValidateResult(StatusType.Pass), null, null);

    cache.cacheResult("Validate_ReplacementTrnReference",
            "OrigTrnRef", new Object[] {"TestMessageRef", "Y", "OrigTrnRef"},
            new CustomValidateResult(StatusType.Pass), null, null);

    cache.cacheResult("Validate_IndividualSDA",
            "6811035039084", new Object[] {"TestMessageRef", "2020-12-25", "20000.10"},
            new CustomValidateResult(StatusType.Pass), null, null);

    JSStructureParser parser = new JSStructureParser(testMessage());
    Object obj = parser.parse();

    if (obj instanceof JSObject) {
      JSObject meta = new JSObject();
      JSFinsurvContext jsFinsurvContext = new JSFinsurvContext((JSObject)obj, meta);
      ICustomValue customValues = new DefaultCustomValues();
      ValidationStats stats = new ValidationStats();

      System.out.println("Validating Test Message");
      System.out.println("=======================");

      validator.validate(jsFinsurvContext, customValues, stats);

      printStats(stats);

      System.out.println();
      printMeta(meta);
    }
  }
}
