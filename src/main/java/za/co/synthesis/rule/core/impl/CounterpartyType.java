package za.co.synthesis.rule.core.impl;

/**
 * Created by jake on 8/29/17.
 */
public enum CounterpartyType {
  Bank,
  NonBank
}
