package za.co.synthesis.rule.classification;

/**
 * Created by jake on 6/7/16.
 */
public class ClassificationZZ2 extends ClassificationRule {
  public ClassificationZZ2(String name, IClassificationFunction assertion) {
    super("zz2", name, assertion);
  }
}
