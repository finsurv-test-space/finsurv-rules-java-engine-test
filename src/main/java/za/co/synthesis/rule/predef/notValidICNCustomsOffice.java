package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.Util;

public class notValidICNCustomsOffice implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !Util.isValidICNCustomsOffice(value.toString(), context.getLookups());
  }
}

//_export.notValidICNCustomsOffice = function (context, value) {
//      if (!value)
//        return true;
//      if (value.length <= 3)
//        return true;
//      if (!context.lookups.isValidCustomsOfficeCode(value.substr(0, 3)))
//        return true;
//      return false;
//}