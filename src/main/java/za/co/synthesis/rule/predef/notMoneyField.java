package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 12:45 PM
 * Determines if the monetary amount field has not been provided or if it is has is it a string that only contains
 * non-whitespace characters
 */
public class notMoneyField implements IAssertionFunction {
  private IStringResolver field;

  public notMoneyField(IStringResolver field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getMoneyField(context.getCurrentMoneyInstance(), field.resolve(context, value));
    if (fieldValue == null || fieldValue instanceof IFinsurvContext.Undefined ||
         (fieldValue instanceof String && ((String)fieldValue).trim().length() == 0))
      return true;
    else
      return false;
  }
}
/*
function notMoneyField(field) {
  return function(context, value) {
    var fieldValue = context.getMoneyField(context.currentMoneyInstance, field);
    if (!fieldValue || (typeof fieldValue == "string" && fieldValue.trim().length == 0))
      return true;
    else
      return false;
  }
}
*/

