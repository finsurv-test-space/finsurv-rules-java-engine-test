package za.co.synthesis.rule.core;

public class StatusCacheStrategy {
    private StatusType status;
    private CachePeriod cachePeriod;

    StatusCacheStrategy(StatusType status, CachePeriod cachePeriod) {
        this.status = status;
        this.cachePeriod = cachePeriod;
    }

    public StatusType getStatus() {
        return status;
    }

    public CachePeriod getCachePeriod() {
        return cachePeriod;
    }
}
