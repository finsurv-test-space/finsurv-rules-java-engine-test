package za.co.synthesis.rule.evaluation.predef;

import za.co.synthesis.rule.core.ResidenceStatus;
import za.co.synthesis.rule.evaluation.GivenInfo;
import za.co.synthesis.rule.evaluation.IAssumptionAssertionFunction;


public class noResStatus implements IAssumptionAssertionFunction {

  @Override
  public boolean execute(GivenInfo givenInfo) {
    ResidenceStatus resStatus = givenInfo.getResidenceStatus();

    return resStatus == null;
  }
}
