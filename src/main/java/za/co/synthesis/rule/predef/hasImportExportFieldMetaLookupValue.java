package za.co.synthesis.rule.predef;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

public class hasImportExportFieldMetaLookupValue implements IAssertionFunction {
    private String lookupName;
    private String lookupKey;
    private String field;

    public hasImportExportFieldMetaLookupValue(String lookupName, String lookupKey, String field) {
        this.lookupName = lookupName;
        this.lookupKey = lookupKey;
        this.field = field;
    }

    public boolean execute(FinsurvContext context, Object value) {
        int moneyIndex = context.getCurrentMoneyInstance();
        int importIndex = context.getCurrentImportExportInstance();
        if (moneyIndex == -1) {
            moneyIndex = 0;
        }
        if (importIndex == -1) {
            importIndex = 0;
        }


        if (value != null && value instanceof String) {
            Object fieldValue = context.getImportExportField(moneyIndex, importIndex, field);
            JSObject lookupValue = (JSObject) context.getCustomValue().get(lookupName);
            return lookupValue != null && lookupValue.get(lookupKey).equals(fieldValue);
        }
        return false;
    }
}
