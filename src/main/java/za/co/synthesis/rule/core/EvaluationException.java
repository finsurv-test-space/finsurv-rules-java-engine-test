package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 3/5/15
 * Time: 2:50 PM
 * This is thrown by the Evaluation classes whenever an error is encountered
 */
public class EvaluationException extends Exception {
  public EvaluationException(String message) {
    super(message);
  }

  public EvaluationException(String message, java.lang.Throwable throwable) {
    super(message, throwable);
  }
}
