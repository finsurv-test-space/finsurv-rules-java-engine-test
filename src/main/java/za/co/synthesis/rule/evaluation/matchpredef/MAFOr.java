package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;

public class MAFOr implements IMatchAssertionFunction {
  private IMatchAssertionFunction left;
  private IMatchAssertionFunction right;

  public MAFOr(IMatchAssertionFunction left, IMatchAssertionFunction right) {
    this.left = left;
    this.right = right;
  }

  public boolean execute(MatchInfo context) {
    return left.execute(context) || right.execute(context);
  }
}
