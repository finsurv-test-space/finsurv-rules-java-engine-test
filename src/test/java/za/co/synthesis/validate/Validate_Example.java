package za.co.synthesis.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

public class Validate_Example implements ICustomValidate {
  private final String name;
  private final StatusType statusType;

  public Validate_Example(String name, StatusType statusType) {
    this.name = name;
    this.statusType = statusType;
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    System.out.println("External Validation Called");
    System.out.println("Name: " + (name != null ? "\"" + name + "\"" : ""));
    System.out.println("Value: " + (value != null ? "\"" + value.toString() + "\"" : "null"));
    System.out.print("OtherInputs: [");
    for (Object input : otherInputs) {
      System.out.print(input != null ? "\"" + input.toString() + "\", " : "null, ");
    }
    System.out.println("]");
    System.out.println();

    if (statusType == StatusType.Fail) {
      return new CustomValidateResult(StatusType.Fail, "001", "Failure Message");
    }
    else
    if (statusType == StatusType.Error) {
      return new CustomValidateResult(StatusType.Error, "002", "Error Message");
    }
    return new CustomValidateResult(StatusType.Pass);
  }
}
