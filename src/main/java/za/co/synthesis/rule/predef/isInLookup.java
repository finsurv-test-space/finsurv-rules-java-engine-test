package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by petruspretorius on 20/10/2016.
 */
public class isInLookup implements IAssertionFunction {
  private String lookupName;
  private String lookupKey;
  /*
    Used to see if the current field value is in a lookup, by optional lookupKey

     @param:lookupName The name of the lookup to search
     @param:lookupKey Optional lookup key if the lookup is not a simple array
     */
  public isInLookup(String lookupName, String lookupKey) {
    this.lookupName = lookupName;
    this.lookupKey = lookupKey;
  }

  public boolean execute(FinsurvContext context, Object value) {

    if (value != null && value instanceof String) {
      String testVal = (String) value;
      Map<String, String> key = new HashMap<String, String>();
      key.put(this.lookupKey, testVal);
      String lookupValue = context.getLookups().getLookupField(this.lookupName, key, this.lookupKey);
      if (lookupValue != null)
        return true;
    }
    return false;
  }

}
