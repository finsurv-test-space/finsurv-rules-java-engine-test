package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.StringList;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 1:55 PM
 * Checks to see if the value is not in the given set of values
 */
public class notValueIn implements IAssertionFunction {
  private StringList constants = null;

  public notValueIn(String value) {
    constants = new StringList();
    constants.add(value);
  }

  public notValueIn(StringList value) {
    this.constants = value;
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (value != null) {
      return !constants.contains(value);
    }
    return true;
  }
}
/*
function notValueIn(constant) {
  return function(context, value) {
    if (value) {
      if (typeof constant === "string") {
        if (value === constant)
          return false;
      } else {
        for ( var i = 0; i < constant.length; i++) {
          if (value === constant[i])
            return false;
        }
      }
    }
    return true;
  }
}
*/