package za.co.synthesis;

import za.co.synthesis.csv.CsvOutput;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.jdbc.JdbcSourceManager;
import za.co.synthesis.mapping.configuration.MappingConfig;
import za.co.synthesis.mapping.core.Entity;
import za.co.synthesis.mapping.core.Mapper;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.core.impl.JSCustomValues;
import za.co.synthesis.rule.core.impl.JSFinsurvContext;
import za.co.synthesis.rule.support.Util;
import za.co.synthesis.validate.*;

import java.sql.*;
/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Synthesis on 2016/04/12.
 */
public class JdbcStagingMigration {
    //public static long startUniqueBopCusN = 132475417;//131476147;
    public static long startUniqueTranN = 135888671;//131476147;
    public static String startTrnRef = "";//131476147;
    public static int STAGING_POOL_ID = -273;

    public static String THIRD_PARTY_INSERT_STR = "INSERT INTO [dbo].[STG_ThirdParty]\n" +
            "           ([EntryID]\n" +
            "           ,[SequenceNumber]\n" +
            "           ,[IndividualSurname]\n" +
            "           ,[IndividualName]\n" +
            "           ,[IndividualGender]\n" +
            "           ,[IndividualIDNumber]\n" +
            "           ,[IndividualDoB]\n" +
            "           ,[IndividualTempResPermitNum]\n" +
            "           ,[IndividualPassportNumber]\n" +
            "           ,[IndividualPassportCountry]\n" +
            "           ,[EntityName]\n" +
            "           ,[EntityRegistrationNumber]\n" +
            "           ,[CustomsClientNumber]\n" +
            "           ,[TaxNumber]\n" +
            "           ,[VatNumber]\n" +
            "           ,[StreetAddressLine1]\n" +
            "           ,[StreetAddressLine2]\n" +
            "           ,[StreetSuburb]\n" +
            "           ,[StreetCity]\n" +
            "           ,[StreetProvince]\n" +
            "           ,[StreetPostalCode]\n" +
            "           ,[PostalAddressLine1]\n" +
            "           ,[PostalAddressLine2]\n" +
            "           ,[PostalSuburb]\n" +
            "           ,[PostalCity]\n" +
            "           ,[PostalProvince]\n" +
            "           ,[PostalCode]\n" +
            "           ,[ContactSurname]\n" +
            "           ,[ContactName]\n" +
            "           ,[Email]\n" +
            "           ,[Fax]\n" +
            "           ,[Telephone])";
    public static String IMP_EXP_INSERT_STR = "INSERT INTO [dbo].[STG_ImportExport]\n" +
            "([EntryID]\n" +
            ",[SequenceNumber]\n" +
            ",[ImportControlNumber]\n" +
            ",[TransportDocumentNumber]\n" +
            ",[UCR]\n" +
            ",[MRNNotOnIVS]\n" +
            ",[PaymentValue]\n" +
            ",[PaymentCurrencyCode])";
    public static String MONEY_INSERT_STR = "INSERT INTO [dbo].[STG_MonetaryAmount]\n" +
            "([EntryID]\n" +
            ",[SequenceNumber]\n" +
            ",[MoneyTransferAgentID]\n" +
            ",[RandValue]\n" +
            ",[ForeignValue]\n" +
            ",[CategoryCode]\n" +
            ",[SwiftDetails]\n" +
            ",[StrateRefNumber]\n" +
            ",[LoanRefNumber]\n" +
            ",[LoanTenor]\n" +
            ",[LoanInterestRate]\n" +
            ",[RulingSection]\n" +
            ",[InternalAuthNumber]\n" +
            ",[InternalAuthDate]\n" +
            ",[SARBAuthAppNumber]\n" +
            ",[SARBAuthReference]\n" +
            ",[CannotCategorize]\n" +
            ",[AdhocSubject]\n" +
            ",[AdhocDescription]\n" +
            ",[LocationCountryCode]\n" +
            ",[ReversalSourceName]\n" +
            ",[ReversalSourceReference]\n" +
            ",[ReversalTrnRefNumber]\n" +
            ",[ReversalSequence]\n" +
            ",[PaymentIdentifier]\n" +
            ",[BOPDIRTrnReference]\n" +
            ",[BOPDIRADCode]\n" +
            ",[CardChargeBack]\n" +
            ",[CardIndicator]\n" +
            ",[ECommerceIndicator]\n" +
            ",[POSEntryMode]\n" +
            ",[CardFraudulentTransactionIndicator]\n" +
            ",[ForeignCardHoldersPurchasesRandValue]\n" +
            ",[ForeignCardHoldersCashWithdrawalsRandValue]\n" +
            ",[TravelSurname]\n" +
            ",[TravelName]\n" +
            ",[TravelIDNumber]\n" +
            ",[TravelDateOfBirth]\n" +
            ",[TravelTempResPermitNumber])\n";

    public static String TRANSACTION_INSERT_STR = "INSERT INTO [dbo].[STG_Transaction]\n" +
            "([StagingPoolID]\n" +
            ",[InQueueMsg_ID]\n" +
            ",[SourceName]\n" +
            ",[SourceReference]\n" +
            ",[TrnReference]\n" +
            ",[Version]\n" +
            ",[ParentSourceName]\n" +
            ",[ParentSourceReference]\n" +
            ",[ParentTrnReference]\n" +
            ",[ReplacementSourceName]\n" +
            ",[ReplacementSourceReference]\n" +
            ",[ReplacementTrnReference]\n" +
            ",[ReportingQualifierID]\n" +
            ",[Flow]\n" +
            ",[ValueDate]\n" +
            ",[CurrencyCode]\n" +
            ",[StateID]\n" +
            ",[BranchCode]\n" +
            ",[BranchName]\n" +
            ",[HubCode]\n" +
            ",[HubName]\n" +
            ",[OriginatingBank]\n" +
            ",[OriginatingCountryCode]\n" +
            ",[CorrespondentBank]\n" +
            ",[CorrespondentCountryCode]\n" +
            ",[ReceivingBank]\n" +
            ",[ReceivingCountryCode]\n" +
            ",[NR_Type]\n" +
            ",[NRI_Surname]\n" +
            ",[NRI_Name]\n" +
            ",[NRI_Gender]\n" +
            ",[NRI_PassportNumber]\n" +
            ",[NRI_PassportCountryCode]\n" +
            ",[NRE_EntityName]\n" +
            ",[NRE_CardMerchantName]\n" +
            ",[NRE_CardMerchantCode]\n" +
            ",[NR_AccountTypeID]\n" +
            ",[NR_AccountNumber]\n" +
            ",[NR_AddressLine1]\n" +
            ",[NR_AddressLine2]\n" +
            ",[NR_Suburb]\n" +
            ",[NR_City]\n" +
            ",[NR_State]\n" +
            ",[NR_ZIPCode]\n" +
            ",[NR_CountryCode]\n" +
            ",[NRX_ExceptionName]\n" +
            ",[R_Type]\n" +
            ",[RI_Surname]\n" +
            ",[RI_Name]\n" +
            ",[RI_Gender]\n" +
            ",[RI_DoB]\n" +
            ",[RI_IDNumber]\n" +
            ",[RI_TempResPermitNumber]\n" +
            ",[RI_ForeignIDNumber]\n" +
            ",[RI_ForeignIDCountry]\n" +
            ",[RI_PassportNumber]\n" +
            ",[RI_PassportCountryCode]\n" +
            ",[RI_BeneficiaryID1]\n" +
            ",[RI_BeneficiaryID2]\n" +
            ",[RI_BeneficiaryID3]\n" +
            ",[RI_BeneficiaryID4]\n" +
            ",[RE_LegalEntityName]\n" +
            ",[RE_TradingName]\n" +
            ",[RE_RegistrationNumber]\n" +
            ",[RE_InstitutionalSector]\n" +
            ",[RE_IndustrialClass]\n" +
            ",[R_AccountName]\n" +
            ",[R_AccountTypeID]\n" +
            ",[R_AccountNumber]\n" +
            ",[R_CustomsClientNumber]\n" +
            ",[R_TaxNumber]\n" +
            ",[R_VATNumber]\n" +
            ",[R_TAXClrCertInd]\n" +
            ",[R_TAXClrCertRef]\n" +
            ",[R_StreetAddressLine1]\n" +
            ",[R_StreetAddressLine2]\n" +
            ",[R_StreetSuburb]\n" +
            ",[R_StreetCity]\n" +
            ",[R_StreetPostalCode]\n" +
            ",[R_StreetProvince]\n" +
            ",[R_PostalAddressLine1]\n" +
            ",[R_PostalAddressLine2]\n" +
            ",[R_PostalSuburb]\n" +
            ",[R_PostalCity]\n" +
            ",[R_PostalProvince]\n" +
            ",[R_PostalCode]\n" +
            ",[R_ContactName]\n" +
            ",[R_ContactSurname]\n" +
            ",[R_ContactEmail]\n" +
            ",[R_ContactFax]\n" +
            ",[R_ContactTelephone]\n" +
            ",[R_CustomerSrc]\n" +
            ",[R_CustomerRef]\n" +
            ",[R_CardNumber]\n" +
            ",[R_SuppCardInd]\n" +
            ",[RX_ExceptionName]\n" +
            ",[RX_CountryCode]\n" +
            ",[UserID]\n" +
            ",[IsAdla]\n" +
            ",[DifferenceRecord]\n" +
            ",[Action]\n" +
            ",[ErrorDescription]" +
            ")\n";

    public static HashMap <String, Integer> LL_REPORTING_QUALIFIERS = new HashMap<String, Integer>();
    public static HashMap <String, Integer> LL_ACCOUNT_IDENTIFIERS = new HashMap<String, Integer>();



    public static HashMap<String, Integer> getLookupList(Connection conn, String sql){
        HashMap <String, Integer> q = new HashMap<String, Integer>();
        try {
             Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Integer rqid = new Integer(rs.getInt(1));
                String rqstr = rs.getString(2);
                q.put(rqstr, rqid);
            }
            rs.close();
            stmt.close();
        }catch(Exception err){
            System.err.println("Error retrieving lookup list: ("+sql+")"+err.getMessage());
        }
        return q;
    }

    public static int lookupList(HashMap<String, Integer> LookupList, String rqstr){
        Integer i = null;
        i = LookupList.get(rqstr);
        return (i != null ? i.intValue() : -1);
    }


    private static Validator issueValidator(ValidationEngine engine, DefaultSupporting supporting) throws ValidationException {
        engine.setupLoadLookupFile("../finsurv-rules/lookups.js");
        engine.setupLoadRuleFile("../finsurv-rules/transaction.js");
        engine.setupLoadRuleFile("../finsurv-rules/exttransaction.js");
        engine.setupLoadRuleFile("../finsurv-rules/money.js");
        engine.setupLoadRuleFile("../finsurv-rules/extmoney.js");
        engine.setupLoadRuleFile("../finsurv-rules/importexport.js");
        engine.setupLoadRuleFile("../finsurv-rules/extimportexport.js");

        supporting.setCurrentDate(LocalDate.now());
        supporting.setGoLiveDate(Util.date("2013-08-18"));
        engine.setupSupporting(supporting);
        engine.setupLookups(new LookupsTest());
        engine.setupCustomValidateFactory(new TestValidateFactory());

        Validator validator = engine.issueValidator();
        return validator;
    }

    //Sets the STAGING_POOL_ID to the next available (negative) integer --> (min(id) - 1)
    public static boolean getNextStagingId(Connection conn, String status){
        int lowestId = -273;
        Integer nextID = null;
        String sql = "SELECT (MIN(StagingPoolID)-1) as 'NextID' FROM STG_StagingPool";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                nextID = rs.getInt(1);
            }
            rs.close();
            stmt.close();
            int currentID = STAGING_POOL_ID;
            STAGING_POOL_ID = (nextID != null && lowestId < nextID)?lowestId:nextID;
            //create the new staging pool id, with the specified status
            if (insertStagingId(conn,status) && (nextID != null)){
                return setStagingIdStatus(conn, currentID, "P");
            }
        }catch(Exception err){
            System.err.println("Error retrieving next staging pool ID: ("+sql+")"+err.getMessage());
            return false;
        }
        return true;
    }

    public static boolean insertStagingId(Connection conn, String status){
        status = (status!=null && status.length()>0)?status:"C";
        try{
            //Use Staging ID -273 and Status 'C' by default.
            CallableStatement cs = conn.prepareCall("insert into STG_StagingPool (StagingPoolID, Status) values (?, ?)");
            cs.setInt(1, STAGING_POOL_ID);
            cs.setString(2, status);
            cs.executeUpdate();
        } catch(Exception error){
            if (error.getMessage().indexOf("duplicate") > 0) {
                System.out.println("STAGING_POOL_ID already inserted : "+STAGING_POOL_ID+" (C)");
            } else {
                System.err.println("Some exception while trying to write the STAGING_POOL_ID to DB: "+error.getMessage());
                return false;
            }
        }
        return true;
    }

    public static boolean setStagingIdStatus(Connection conn, int stagingId, String status){
        status = (status!=null && status.length()>0)?status:"C";
        try{
            //Use Staging ID -273 and Status 'C' by default.
            CallableStatement cs = conn.prepareCall("UPDATE STG_StagingPool SET Status=? WHERE StagingPoolID=?");
            cs.setString(1, status);
            cs.setInt(2, stagingId);
            cs.executeUpdate();
        } catch(Exception error){
                System.err.println("Some exception while trying to set the current STAGING_POOL_ID ("+STAGING_POOL_ID+":"+status+"): "+error.getMessage());
                return false;
        }
        return true;
    }

    public static boolean setCurrentStagingId(Connection conn, String status){
        status = (status!=null && status.length()>0)?status:"C";
        return setStagingIdStatus(conn, STAGING_POOL_ID, status);
    }

    public static void main( String[] args ) throws Exception {
        HashMap<String, Boolean> refHash = new HashMap<String, Boolean>();
        long startMillis = System.currentTimeMillis();
        ValidationEngine engine = new ValidationEngine();
        DefaultSupporting supporting = new DefaultSupporting();

        Validator validator = issueValidator(engine, supporting);


        MappingConfig mappingConfig = MappingConfig.load("./src/test/resources/bopcd3_staging_mapping.js");

        CsvOutput csvOutput = new CsvOutput();
        csvOutput.openForWrite("errorlog.txt");


        // MSSQL target DB
        JdbcSourceManager destManager = new JdbcSourceManager();
        Connection destConn = null;
        try {
            destManager.setJdbcDriver("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            destManager.setDbUrl("jdbc:sqlserver://10.143.224.103:1433;databaseName=tXstream");
//            destManager.setDbUrl("jdbc:sqlserver://127.0.0.1:1433;databaseName=master");
            destManager.setDbUser("sa");
            destManager.setDbPassword("p@ssw0rd");
//            destManager.setDbPassword("Password1");
            destManager.setBufferSize(1000);
            destManager.open(null);
            destConn = destManager.getConnection();
            System.out.println("Connection to TxStream Staging DB successfull.");
        } catch (Exception error) {
            System.err.println("Unable to connect to TxStream Staging DB: " + error.getMessage());
        }

        if (destConn == null) {
            throw new Exception("TxStream DB not connected - check configuration and connectivity status");
        }


        LL_REPORTING_QUALIFIERS = getLookupList(destConn, "SELECT [ReportingQualifierID]\n" +
                "      ,[Name]\n" +
                "  FROM [dbo].[OR_ReportingQualifier]");
        LL_ACCOUNT_IDENTIFIERS = getLookupList(destConn, "SELECT [AccountTypeID]\n" +
                "      ,[Name]\n" +
                "  FROM [dbo].[OR_AccountType]");


        //GET THE LIST OF IMPORTED TRN_REFERENCES AND USE THAT LIST TO EXCLUDE FROM FURTHER SELECTION BELOW...

        String importedTrnRefs = "";
        String sql = "SELECT DISTINCT TRN_REFERENCE FROM [dbo].[ImportedList] ORDER BY TRN_REFERENCE";
//        try {
//            Statement stmt = destConn.createStatement();
//            ResultSet rs = stmt.executeQuery(sql);
//            while (rs.next()) {
////                importedTrnRefs += ((importedTrnRefs.length() > 0 )? ", ": "") + "'" + rs.getString(1) + "'";
//                refHash.put(rs.getString(1), new Boolean(true));
//            }
//            rs.close();
//            stmt.close();
//        }catch(Exception err){
//            System.err.println("Error retrieving imported TRN_REFERENCE list: ("+sql+")"+err.getMessage());
//        }

        JdbcSourceManager sourceManager = new JdbcSourceManager();
        //Oracle
        sourceManager.setJdbcDriver("oracle.jdbc.OracleDriver");
        sourceManager.setDbUrl("jdbc:oracle:thin:@dfmodbr1ldm.standardbank.co.za:1526:xbal");
        sourceManager.setDbUser("C736340");
        sourceManager.setDbPassword("ask4help");

        sourceManager.setBufferSize(1000);

        List<String> references = new ArrayList<String>();
        sourceManager.open(new ProcessReference(sourceManager,

                "SELECT t.TRN_REFERENCE FROM (\n" +
                        "SELECT DISTINCT a.TRN_REFERENCE \n" +
                        "FROM\n" +
                        "CD3_TRAN_STG a\n" +
                        "WHERE \n" +
//                    "a.UNIQUE_TRAN_N >= {StartNo}\n" +
//                    "AND \n" +
//                    "a.TRN_REFERENCE = '1511040155TT5705'\n" +
//                    "AND \n" +
//                        "a.TRN_REFERENCE not like '%CA___'\n" +
//                        "AND \n" +
//                        "a.TRN_REFERENCE not like '%RE___'\n" +
//                        "AND \n" +
                        "a.VALUE_DATE >= TO_DATE('2015-11-01','YYYY-MM-dd')\n" +
//                        ((importedTrnRefs.length() > 0)
//                                ?"AND a.TRN_REFERENCE not in ("+importedTrnRefs+") \n":"") +
                        "ORDER BY \n" +
                        "a.TRN_REFERENCE\n" +
//                    ",a.UNIQUE_TRAN_N\n" +
                        ") t\n" +
                        "WHERE \n" +
                        "ROWNUM <= {Limit}\n" +
                        "", startUniqueTranN, 1000, 1000));
//              "", startUniqueTranN, 10000, 1050000));

        sourceManager.setup(mappingConfig);


        Connection insertBatchConn = null;
        Statement insertBatchStatement = null;

        Entity tran;
        Object entry;
        int processedTransactions = 0;
        int batchCount = 0;
        getNextStagingId(destConn,"C");
        while ((entry = sourceManager.getNextEntry()) != null) {
            if (insertBatchConn == null) {
//                insertBatchConn = sourceManager.getConnection();
                insertBatchConn = destConn;
                insertBatchStatement = insertBatchConn.createStatement();

            }
            tran = Mapper.composeEntity(mappingConfig, sourceManager, entry);

            //add the necessary batch entries...
            String insValues = createTransactionInsert(tran, destConn);
//            createTransactionInsert(tran, sourceManager.getConnection());

            Boolean isInserted = false;
            try {
                isInserted = refHash.get(tran.getJsValue("TrnReference"));
            } catch (Exception err){
                //ignore issues here...
            }
//            if ((isInserted == null) || (!isInserted)) {
//                sql = "INSERT INTO ImportedList (TRN_REFERENCE, IMPORT_DATE, STATUS, ERROR_LOG, TXSTREAM_ID) VALUES (" + insValues + ")";
////            System.out.println(sql);
//                insertBatchStatement.addBatch(sql);
//                batchCount++;
//            }

          processedTransactions++;

            if (processedTransactions % 10 == 0) {
                insertBatchStatement.executeBatch();
                batchCount = 0;
                getNextStagingId(destConn,"C");
            }
        }

        System.out.println(sql);
        if (batchCount > 0)
        try {
            insertBatchStatement.executeBatch();
        } catch(Exception err){
            //this may occur if/when the full 10^x transactions have run as it will have already just run
        }

        csvOutput.close();

        long endMillis = System.currentTimeMillis();
        System.out.println("Run Time: " + (endMillis-startMillis) + " millseconds" );
        System.out.println("Processed Record Count: " + processedTransactions );

    }


    public static String createTransactionInsert(Entity tran, Connection conn) throws Exception {
        Map<String, String> meta = tran.getMetaData();
        long txstreamID = 0;
        JSObject js = tran.getJsObject();
        String flow = (String)js.get("Flow");
        String sql = TRANSACTION_INSERT_STR +
                "     VALUES\n" +
                "(" +
                STAGING_POOL_ID + //[StagingPoolID]  // *** NOT NULL ***
//TODO: We need to add a legitimate looking id here..
                ",-1" + // [InQueueMsg_ID]  // *** NOT NULL ***
                "," + StrOrNull(meta.get("SYSTEM_ID")) +   //[SourceName]
//TODO: must use a proper reference here - Nulls not allowed.
                "," + StrOrNull(js.get("TrnReference")) + //<SourceReference, varchar(30),>\n" +  // *** NOT NULL ***
                ",NULL" +   //<TrnReference, varchar(30),>\n" +
            ",3" + //<Version, smallint,>\n" +
                ",NULL" +  //<ParentSourceName, varchar(40),>\n" +
                ",NULL" +   //<ParentSourceReference, varchar(30),>\n" +
                ",NULL" + //<ParentTrnReference, varchar(30),>\n" +  //STORING THE TX ID IN THIS FIELD FOR BATCH INSERTION?
                ",NULL" + //<ReplacementSourceName, varchar(40),>\n" +
                ",NULL" + //<ReplacementSourceReference, varchar(30),>\n" +
                ",NULL" + //<ReplacementTrnReference, varchar(30),>\n" +

    "," + lookupList(LL_REPORTING_QUALIFIERS, StrOrNull(js.get("ReportingQualifier"), false)) + //<ReportingQualifierID, smallint,>\n" +

                "," + StrOrNull(flow != null?(flow.compareToIgnoreCase("IN")==0?"I":"O"):"U") +  //<Flow, varchar(1),>\n" +
            "," + StrOrNull(js.get("ValueDate")) +  //<ValueDate, datetime,>\n" +
                "," + StrOrNull(js.get("FlowCurrency")) +  //<CurrencyCode, varchar(3),>\n" +
            ",NULL" + //<StateID, smallint,>\n" +
                "," + StrOrNull(js.get("BranchCode")) +  //<BranchCode, varchar(10),>\n" +
                "," + StrOrNull(js.get("BranchName")) +  //<BranchName, varchar(50),>\n" +
                "," + StrOrNull(js.get("HubCode")) +  //<HubCode, varchar(10),>\n" +
                "," + StrOrNull(js.get("HubName")) +  //<HubName, varchar(50),>\n" +
                "," + StrOrNull(js.get("OriginatingBank")) +  //<OriginatingBank, varchar(50),>\n" +
                "," + StrOrNull(js.get("OriginatingCountry")) +  //<OriginatingCountryCode, varchar(2),>\n" +
                "," + StrOrNull(js.get("CorrespondentBank")) +  //<CorrespondentBank, varchar(50),>\n" +
                "," + StrOrNull(js.get("CorrespondentCountry")) +  //<CorrespondentCountryCode, varchar(2),>\n" +
                "," + StrOrNull(js.get("ReceivingBank")) +  //<ReceivingBank, varchar(50),>\n" +
                "," + StrOrNull(js.get("ReceivingCountry")) +  //<ReceivingCountryCode, varchar(2),>\n" +
                "," + StrOrNull(meta.get("NON_RES_CUST_TYPE")) +  //<NR_Type, varchar(1),>\n" +
                "," + StrOrNull(tran.getJsValue("NonResident.Individual.Surname")) +  //<NRI_Surname, varchar(35),>\n" +
                "," + StrOrNull(tran.getJsValue("NonResident.Individual.Name")) +  //<NRI_Name, varchar(50),>\n" +
                "," + StrOrNull(tran.getJsValue("NonResident.Individual.Gender")) +  //<NRI_Gender, varchar(1),>\n" +
                "," + StrOrNull(tran.getJsValue("NonResident.Individual.PassportNumber")) +  //<NRI_PassportNumber, varchar(20),>\n" +
                "," + StrOrNull(tran.getJsValue("NonResident.Individual.PassportCountry")) +  //<NRI_PassportCountryCode, varchar(2),>\n" +
                "," + StrOrNull(tran.getJsValue("NonResident.Entity.EntityName")) +  //<NRE_EntityName, varchar(70),>\n" +
                ",NULL" + //<NRE_CardMerchantName, varchar(70),>\n" +   ???
                ",NULL" + //<NRE_CardMerchantCode, varchar(6),>\n" +   ???

    "," + lookupList(LL_ACCOUNT_IDENTIFIERS, StrOrNull(tran.getAnyJsValue("NonResident.Entity.AccountIdentifier", "NonResident.Individual.AccountIdentifier"), false)) +  //<NR_AccountTypeID, smallint,>\n" +

                "," + StrOrNull(tran.getAnyJsValue("NonResident.Entity.AccountNumber", "NonResident.Individual.AccountNumber")) +  //<NR_AccountNumber, varchar(40),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("NonResident.Entity.StreetAddress.AddressLine1", "NonResident.Individual.StreetAddress.AddressLine1")) +  //<NR_AddressLine1, varchar(50),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("NonResident.Entity.StreetAddress.AddressLine2", "NonResident.Individual.StreetAddress.AddressLine2")) +  //<NR_AddressLine2, varchar(50),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("NonResident.Entity.StreetAddress.Suburb", "NonResident.Individual.StreetAddress.Suburb")) +  //<NR_Suburb, varchar(50),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("NonResident.Entity.StreetAddress.City", "NonResident.Individual.StreetAddress.City")) +  //<NR_City, varchar(35),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("NonResident.Entity.StreetAddress.State", "NonResident.Individual.StreetAddress.State")) +  //<NR_State, varchar(35),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("NonResident.Entity.StreetAddress.PostalCode", "NonResident.Individual.StreetAddress.PostalCode")) +  //<NR_ZIPCode, varchar(10),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("NonResident.Entity.StreetAddress.Country", "NonResident.Individual.StreetAddress.Country")) +  //<NR_CountryCode, varchar(2),>\n" +
                "," + StrOrNull(tran.getJsValue("NonResident.Exception.ExceptionName")) +  //<NRX_ExceptionName, varchar(35),>\n" +
                "," + StrOrNull(meta.get("RES_CUST_TYPE")) +  //<R_Type, varchar(1),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.Surname")) +  //<RI_Surname, varchar(35),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.Name")) +  //<RI_Name, varchar(50),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.Gender")) +  //<RI_Gender, varchar(1),>\n" +
            "," + StrOrNull(tran.getJsValue("Resident.Individual.DateOfBirth")) +  //<RI_DoB, datetime,>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.IDNumber")) +  //<RI_IDNumber, varchar(20),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.TempResPermitNumber")) +  //<RI_TempResPermitNumber, varchar(20),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.ForeignIDNumber")) +  //<RI_ForeignIDNumber, varchar(20),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.ForeignIDCountry")) +  //<RI_ForeignIDCountry, varchar(2),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.PassportNumber")) +  //<RI_PassportNumber, varchar(20),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.PassportCountry")) +  //<RI_PassportCountryCode, varchar(2),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.BeneficiaryID1")) +  //<RI_BeneficiaryID1, varchar(13),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.BeneficiaryID2")) +  //<RI_BeneficiaryID2, varchar(13),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.BeneficiaryID3")) +  //<RI_BeneficiaryID3, varchar(13),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Individual.BeneficiaryID4")) +  //<RI_BeneficiaryID4, varchar(13),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Entity.EntityName")) +  //<RE_LegalEntityName, varchar(70),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Entity.TradingName")) +  //<RE_TradingName, varchar(70),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Entity.RegistrationNumber")) +  //<RE_RegistrationNumber, varchar(30),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Entity.InstitutionalSector")) +  //<RE_InstitutionalSector, varchar(2),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Entity.IndustrialClass")) +  //<RE_IndustrialClass, varchar(2),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.AccountName", "Resident.Entity.AccountName")) +  //<R_AccountName, varchar(70),>\n" +

                "," + lookupList(LL_ACCOUNT_IDENTIFIERS, StrOrNull(tran.getAnyJsValue("Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"), false)) +  //<R_AccountTypeID, smallint,>\n" +

                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber")) +  //<R_AccountNumber, varchar(40),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber")) +  //<R_CustomsClientNumber, varchar(35),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.TaxNumber", "Resident.Entity.TaxNumber")) +  //<R_TaxNumber, varchar(30),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.VATNumber", "Resident.Entity.VATNumber")) +  //<R_VATNumber, varchar(20),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateIndicator")) +  //<R_TAXClrCertInd, varchar(1),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference")) +  //<R_TAXClrCertRef, varchar(30),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1")) +  //<R_StreetAddressLine1, varchar(70),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2")) +  //<R_StreetAddressLine2, varchar(70),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb")) +  //<R_StreetSuburb, varchar(35),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City")) +  //<R_StreetCity, varchar(35),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.StreetAddress.PostalCode", "Resident.Entity.StreetAddress.PostalCode")) +  //<R_StreetPostalCode, varchar(10),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province")) +  //<R_StreetProvince, varchar(35),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1")) +  //<R_PostalAddressLine1, varchar(70),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2")) +  //<R_PostalAddressLine2, varchar(70),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb")) +  //<R_PostalSuburb, varchar(35),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City")) +  //<R_PostalCity, varchar(35),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province")) +  //<R_PostalProvince, varchar(35),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.PostalAddress.PostalCode", "Resident.Entity.PostalAddress.PostalCode")) +  //<R_PostalCode, varchar(10),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.ContactDetails.ContactName", "Resident.Entity.ContactDetails.ContactName")) +  //<R_ContactName, varchar(50),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.ContactDetails.ContactSurname", "Resident.Entity.ContactDetails.ContactSurname")) +  //<R_ContactSurname, varchar(35),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.ContactDetails.Email", "Resident.Entity.ContactDetails.Email")) +  //<R_ContactEmail, varchar(120),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.ContactDetails.Fax", "Resident.Entity.ContactDetails.Fax")) +  //<R_ContactFax, varchar(15),>\n" +
                "," + StrOrNull(tran.getAnyJsValue("Resident.Individual.ContactDetails.Telephone", "Resident.Entity.ContactDetails.Telephone")) +  //<R_ContactTelephone, varchar(15),>\n" +
                ",NULL" + //<R_CustomerSrc, varchar(45),>\n" +
                ",NULL" + //<R_CustomerRef, varchar(45),>\n" +
                ",NULL" + //<R_CardNumber, varchar(20),>\n" +
                ",NULL" + //<R_SuppCardInd, varchar(1),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Exception.ExceptionName")) + //<RX_ExceptionName, varchar(35),>\n" +
                "," + StrOrNull(tran.getJsValue("Resident.Exception.Country")) + //<RX_CountryCode, varchar(2),>\n" +
            ",NULL" + //<UserID, int,>\n" +
                ",NULL" + //<IsAdla, varchar(1),>\n" +
                ",NULL" + //<DifferenceRecord, varchar(1024),>\n" +
                ",NULL" + //<Action, varchar(1),>\n" +
                ",NULL" + //<ErrorDescription, varchar(1024),>)";
                ")";
        try {
            PreparedStatement stgTx = conn.prepareStatement(sql,
                    Statement.RETURN_GENERATED_KEYS);
            int affectedRows = stgTx.executeUpdate();

            if (affectedRows > 0) {
                try {
                    ResultSet generatedKeys = stgTx.getGeneratedKeys();
                    //this should return the EntryID key that was generated...
                    if (generatedKeys.next()) {
                        Statement batchInsert = conn.createStatement();
                        txstreamID = generatedKeys.getLong(1);
                        createMoneyInsert(tran, batchInsert, generatedKeys.getLong(1));
                        batchInsert.executeBatch();
                        batchInsert.close();
//                        System.out.print(".");
//                    } else {
//                        System.err.print(".");
                    }
                } catch (Exception err) {
                    System.err.println("\r\nError inserting Monetary Amount (TRN REF: "+StrOrNull(js.get("TrnReference"))+"): "+err.getMessage());
                    if (err.getMessage().indexOf("duplicate") <0) {
                        err.printStackTrace();
                    }
                }
            } else {
                System.err.print("*");
            }


//        Statement batchInsert = conn.createStatement();
//        createMoneyInsert(tran, batchInsert, 0);

        } catch (Exception err){
            System.out.println("ERROR running query: "+err.getMessage()+"\r\n\r\n"+sql+"\r\n\r\n");
        }
        return StrOrNull(js.get("TrnReference"))  + ", " +
                "GETDATE(), " +
                StrOrNull("SUCCESS") + ", " +
                "'', " +
                "" + txstreamID + "";
    }




    public static void createMoneyInsert(Entity tran, Statement batchInsert, long txID) throws Exception{
//        Map<String, String> meta = tran.getMetaData();
        JSObject js = tran.getJsObject();
        JSArray money = (JSArray) js.get("MonetaryAmount");
        for (int i =0; i < money.size(); i++) {
            JSObject jsObj = (JSObject) money.get(i);
            if (jsObj != null) {
                JSObject sarb = (JSObject) jsObj.get("SARBAuth");
                //get rid of null references...
                sarb = (sarb != null ? sarb : (new JSObject()));

                int seq = StrToIntDef(jsObj.get("SequenceNumber").toString(), i);
                String sql = MONEY_INSERT_STR +
                        "     VALUES\n" +
                        "(" +
                        txID + //<EntryID, bigint,>\n" +
                        "," + seq + //StrOrNull(jsObj.get("SequenceNumber"), false) + //<SequenceNumber, int,>\n" +
                        "," + StrOrNull(jsObj.get("MoneyTransferAgentIndicator")) +  //<MoneyTransferAgentID, varchar(35),>\n" +
                        "," + StrOrNull(jsObj.get("RandValue"), false) + //<RandValue, decimal(20,2),>\n" +
                        "," + StrOrNull(jsObj.get("ForeignValue"), false) + //<ForeignValue, decimal(20,2),>\n" +
                        ",'" + StrOrNull(jsObj.get("CategoryCode"), false, false) +
                                (jsObj.get("CategoryCode") != null && jsObj.get("CategorySubCode") != null ?
                                        "/" + jsObj.get("CategorySubCode")
                                        : "") + "'" + //<CategoryCode, varchar(6),>\n" +
                        "," + StrOrNull(jsObj.get("SwiftDetails")) + //",<SwiftDetails, varchar(100),>\n" +
                        "," + StrOrNull(jsObj.get("StrateRefNumber")) + //",<StrateRefNumber, varchar(30),>\n" +
                        "," + StrOrNull(jsObj.get("LoanRefNumber")) + //",<LoanRefNumber, varchar(20),>\n" +
                        "," + StrOrNull(jsObj.get("LoanTenor")) + //",<LoanTenor, varchar(10),>\n" +
                        "," + StrOrNull(jsObj.get("LoanInterestRate")) + //",<LoanInterestRate, varchar(25),>\n" +
                        "," + StrOrNull(jsObj.get("RulingSection")) + //",<RulingSection, varchar(30),>\n" +
                        "," + StrOrNull(jsObj.get("InternalAuthNumber")) + //",<InternalAuthNumber, varchar(15),>\n" +
                        "," + StrOrNull(jsObj.get("InternalAuthDate")) + //",<InternalAuthDate, datetime,>\n" +
                        ((sarb != null) ?
                                "," + StrOrNull(sarb.get("SARBAuthAppNumber")) + //",<SARBAuthAppNumber, varchar(15),>\n" +
                                "," + StrOrNull(sarb.get("SARBAuthRefNumber")) //",<SARBAuthReference, varchar(15),>\n" +
                                : ",NULL, NULL") +
                        "," + StrOrNull(jsObj.get("CannotCategorize")) + //",<CannotCategorize, varchar(100),>\n" +
                        "," + StrOrNull(jsObj.get("AdHocRequirement.Subject")) + //",<AdhocSubject, varchar(30),>\n" +
                        "," + StrOrNull(jsObj.get("AdHocRequirement.Description")) + //",<AdhocDescription, varchar(100),>\n" +
                        "," + StrOrNull(jsObj.get("LocationCountry")) + //",<LocationCountryCode, varchar(2),>\n" +
                        "," + StrOrNull(jsObj.get("ReversalSourceName")) + //",<ReversalSourceName, varchar(40),>\n" +
                        "," + StrOrNull(jsObj.get("ReversalSourceReference")) + //",<ReversalSourceReference, varchar(30),>\n" +
                        "," + StrOrNull(jsObj.get("ReversalTrnRefNumber")) + //",<ReversalTrnRefNumber, varchar(30),>\n" +
                        "," + ((jsObj.get("ReversalTrnRefNumber") != null) ?
                                    StrOrNull(jsObj.get("ReversalTrnSeqNumber"), false) : "NULL") + //",<ReversalSequence, int,>\n" +
                        "," + StrOrNull(jsObj.get("PaymentIdentifier")) + //",<PaymentIdentifier, varchar(20),>\n" +
                        "," + StrOrNull(jsObj.get("BOPDIRTrnReference")) + //",<BOPDIRTrnReference, varchar(30),>\n" +
                        "," + StrOrNull(jsObj.get("BOPDIRADCode")) + //",<BOPDIRADCode, varchar(3),>\n" +
                        "," + StrOrNull(jsObj.get("CardChargeBack")) + //",<CardChargeBack, varchar(1),>\n" +
                        "," + StrOrNull(jsObj.get("CardIndicator")) + //",<CardIndicator, varchar(20),>\n" +
                        "," + StrOrNull(jsObj.get("ECommerceIndicator")) + //",<ECommerceIndicator, varchar(2),>\n" +
                        "," + StrOrNull(jsObj.get("POSEntryMode")) + //",<POSEntryMode, varchar(2),>\n" +
                        "," + StrOrNull(jsObj.get("CardFraudulentTransactionIndicator")) + //",<CardFraudulentTransactionIndicator, varchar(1),>\n" +
                        "," + StrOrNull(jsObj.get("ForeignCardHoldersPurchasesRandValue"), false) + //",<ForeignCardHoldersPurchasesRandValue, decimal(20,2),>\n" +
                        "," + StrOrNull(jsObj.get("ForeignCardHoldersCashWithdrawalsRandValue"), false) + //",<ForeignCardHoldersCashWithdrawalsRandValue, decimal(20,2),>\n" +
                        "," + StrOrNull(jsObj.get("TravelSurname")) + //",<TravelSurname, varchar(35),>\n" +
                        "," + StrOrNull(jsObj.get("TravelName")) + //",<TravelName, varchar(50),>\n" +
                        "," + StrOrNull(jsObj.get("TravelIDNumber")) + //",<TravelIDNumber, varchar(20),>\n" +
                        "," + StrOrNull(jsObj.get("TravelDateOfBirth")) + //",<TravelDateOfBirth, datetime,>\n" +
                        "," + StrOrNull(jsObj.get("TravelTempResPermitNumber")) + //",<TravelTempResPermitNumber, varchar(20),>)";
                        ")";
                batchInsert.addBatch(sql);
                createImpExpInsert(tran, batchInsert, txID, seq, (JSArray) jsObj.get("ImportExport"));
                createtThirdPartyInsert(tran, batchInsert, txID, seq, (JSObject) jsObj.get("ThirdParty"));
            }
        }
    }


    public static int StrToIntDef(String in, int def){
        int i = def;
        try {
            i = Integer.parseInt(in);
        } catch (Exception err){
            //ignore errors stemming from invalid int strings
        }
        return i;
    }



    public static void createImpExpInsert(Entity tran, Statement batchInsert, long txID, int seq, JSArray impExp) throws Exception{
        Map<String, String> meta = tran.getMetaData();
        if (impExp != null) {
            for (int i = 0; i < impExp.size(); i++) {
                JSObject jsObj = (JSObject) impExp.get(i);
                String sql = IMP_EXP_INSERT_STR +
                        "     VALUES\n" +
                        "(" +
                        txID +  //"<EntryID, bigint,>\n" +
                        "," + seq +           //,<SequenceNumber, int,>\n" +
                        "," + StrOrNull(jsObj.get("ImportControlNumber")) + //  ,<ImportControlNumber, varchar(35),>\n" +
                        "," + StrOrNull(jsObj.get("TransportDocumentNumber")) + //  ,<TransportDocumentNumber, varchar(35),>\n" +
                        "," + StrOrNull(jsObj.get("UCR")) + //  ,<UCR, varchar(35),>\n" +
                        "," + StrOrNull(jsObj.get("MRNNotOnIVS")) + //  ,<MRNNotOnIVS, varchar(1),>\n" +
                        "," + StrOrNull(jsObj.get("PaymentValue"), false) + //  ,<PaymentValue, decimal(20,2),>\n" +
                        "," + StrOrNull(jsObj.get("PaymentCurrencyCode")) + //  ,<PaymentCurrencyCode, varchar(3),>" +
                        ")";
                batchInsert.addBatch(sql);
//            System.out.println(sql);

            }
        }
    }


    public static String StrOrNull(Object objStr){
        return StrOrNull(objStr, true);
    }

    public static String StrOrNull(Object objStr, boolean addQuotes) {
        return StrOrNull(objStr, addQuotes, false);
    }

    public static String StrOrNull(Object objStr, boolean addQuotes, boolean NullAsBlank){
        String str = objStr != null ? objStr.toString() : null;
        if ((str == null) || ("NULL".equalsIgnoreCase(str))){
            return NullAsBlank?"":"NULL";
        }
//        return "'"+ str.replaceAll("\'","\'\'") +"'";
        return (((str.length() == 0 || str.toCharArray()[0] != '\'') && addQuotes) ? "'"+ str.replaceAll("\'","\'\'") +"'" : str);
    }


    public static void createtThirdPartyInsert(Entity tran, Statement batchInsert, long txID, int seq, JSObject thrdPrty) throws Exception{
            if (thrdPrty != null) {
                JSObject jsObj = thrdPrty;
                JSObject postAdd = ((JSObject) jsObj.get("PostalAddress"));
                JSObject strAdd = ((JSObject) jsObj.get("StreetAddress"));
                JSObject contact = ((JSObject) jsObj.get("ContactDetails"));
                JSObject ind = ((JSObject) jsObj.get("Individual"));
                JSObject ent = ((JSObject) jsObj.get("Entity"));

                //get rid of null references...
                postAdd = (postAdd != null ? postAdd : (new JSObject()));
                strAdd = (strAdd != null ? strAdd : (new JSObject()));
                contact =  (contact != null ? contact : (new JSObject()));
                ind =  (ind != null ? ind : (new JSObject()));
                ent =  (ent != null ? ent : (new JSObject()));

                String sql = THIRD_PARTY_INSERT_STR +
                        "     VALUES\n" +
                        "(" +
                        txID +  //"<EntryID, bigint,>\n" +
                        "," + seq +           //,<SequenceNumber, int,>\n" +
                        "," + StrOrNull(ind == null ? null : ind.get("Surname")) + //  ,<IndividualSurname, varchar(35),>\n" +
                        "," + StrOrNull(ind == null ? null : ind.get("Name")) + //  "           ,<IndividualName, varchar(35),>\n" +
                        "," + StrOrNull(ind == null ? null : ind.get("Gender")) + //  "           ,<IndividualGender, varchar(1),>\n" +
                        "," + StrOrNull(ind == null ? null : ind.get("IDNumber")) + //  "           ,<IndividualIDNumber, varchar(20),>\n" +
                        "," + StrOrNull(ind == null ? null : ind.get("DatOfBirth")) + //  "           ,<IndividualDoB, datetime,>\n" +
                        "," + StrOrNull(ind == null ? null : ind.get("TempResPermitNumber")) + //  "           ,<IndividualTempResPermitNum, varchar(20),>\n" +
                        "," + StrOrNull(ind == null ? null : ind.get("PassportNumber")) + //  "           ,<IndividualPassportNumber, varchar(20),>\n" +
                        "," + StrOrNull(ind == null ? null : ind.get("PassportCountry")) + //  "           ,<IndividualPassportCountry, varchar(2),>\n" +
                        "," + StrOrNull(ent == null ? null : ent.get("Name")) + //  "           ,<EntityName, varchar(50),>\n" +
                        "," + StrOrNull(ent == null ? null : ent.get("RegistrationNumber")) + //  "           ,<EntityRegistrationNumber, varchar(30),>\n" +
                        "," + StrOrNull(jsObj.get("CustomsClientNumber")) + //  "           ,<CustomsClientNumber, varchar(20),>\n" +
                        "," + StrOrNull(jsObj.get("TaxNumber")) + //  "           ,<TaxNumber, varchar(20),>\n" +
                        "," + StrOrNull(jsObj.get("VATNumber")) + //  "           ,<VatNumber, varchar(20),>\n" +
                        "," + StrOrNull(strAdd.get("AddressLine1")) + //  "           ,<StreetAddressLine1, varchar(70),>\n" +
                        "," + StrOrNull(strAdd.get("AddressLine2")) + //  "           ,<StreetAddressLine2, varchar(70),>\n" +
                        "," + StrOrNull(strAdd.get("Suburb")) + //  "           ,<StreetSuburb, varchar(35),>\n" +
                        "," + StrOrNull(strAdd.get("City")) + //  "           ,<StreetCity, varchar(35),>\n" +
                        "," + StrOrNull(strAdd.get("Province")) + //  "           ,<StreetProvince, varchar(35),>\n" +
                        "," + StrOrNull(strAdd.get("PostalCode")) + //  "           ,<StreetPostalCode, varchar(10),>\n" +
                        "," + StrOrNull(postAdd.get("AddressLine1")) + //  "           ,<PostalAddressLine1, varchar(70),>\n" +
                        "," + StrOrNull(postAdd.get("AddressLine2")) + //  "           ,<PostalAddressLine2, varchar(70),>\n" +
                        "," + StrOrNull(postAdd.get("Suburb")) + //  "           ,<PostalSuburb, varchar(35),>\n" +
                        "," + StrOrNull(postAdd.get("City")) + //  "           ,<PostalCity, varchar(35),>\n" +
                        "," + StrOrNull(postAdd.get("Province")) + //  "           ,<PostalProvince, varchar(35),>\n" +
                        "," + StrOrNull(postAdd.get("PostalCode")) + //  "           ,<PostalCode, varchar(10),>\n" +
                        "," + StrOrNull(contact.get("ContactSurname")) + //  "           ,<ContactSurname, varchar(35),>\n" +
                        "," + StrOrNull(contact.get("ContactName")) + //  "           ,<ContactName, varchar(35),>\n" +
                        "," + StrOrNull(contact.get("Email")) + //  "           ,<Email, varchar(120),>\n" +
                        "," + StrOrNull(contact.get("Fax")) + //  "           ,<Fax, varchar(15),>\n" +
                        "," + StrOrNull(contact.get("Telephone")) + //  "           ,<Telephone, varchar(15),>" +
                        ")";
                batchInsert.addBatch(sql);
            }
    }





}
