package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

/**
 * User: jake
 * Date: 4/27/16
 * Time: 11:46 AM
 * Returns null if the given value is empty
 */
public class nullIfEmpty implements IDisplayFunction {
  @Override
  public String execute(FinsurvContext context, Object value) {
    if (value != null) {
      return value.toString().length() == 0 ? null : value.toString();
    }
    return null;
  }
}
