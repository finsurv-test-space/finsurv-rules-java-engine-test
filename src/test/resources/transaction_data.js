[ { // #0
  "TrnReference" : "TrnReference0",
  "SplitTrnReference" : "SplitTrnReference0",
  "ReportingQualifier" : "BOPCUS",
  "Flow" : "IN",
  "ValueDate" : "2006-05-04",
  "FlowCurrency" : "FlowCurrency0",
  "TotalForeignValue" : "1255.13",
  "BranchCode" : "BranchCode",
  "BranchName" : "BranchName0",
  "HubCode" : "HubCode0",
  "HubName" : "HubName0",
  "OriginatingBank" : "OriginatingBank0",
  "OriginatingCountry" : "OriginatingCountry0",
  "CorrespondentBank" : "CorrespondentBank0",
  "CorrespondentCountry" : "CorrespondentCountry0",
  "ReceivingBank" : "ReceivingBank0",
  "ReceivingCountry" : "ReceivingCountry0",
  "IsADLA" : "false",
  "Resident" : {
    "Exception" : {
      "ExceptionName" : "MUTUAL PARTY",
      "Country" : "Co",
      "AccountIdentifier" : "RESIDENT OTHER",
      "AccountNumber" : "AccountNumber0"
    }
  },
  "NonResident" : {
    "Exception" : {
      "ExceptionName" : "MUTUAL PARTY",
      "AccountIdentifier" : "NON RESIDENT OTHER",
      "AccountNumber" : "AccountNumber1"
    }
  },
  "MonetaryAmount" : [ {
    "MoneyTransferAgentIndicator" : "AD",
    "RandValue" : "0",
    "ForeignValue" : "0",
    "SARBAuth" : {
      "RulingsSection" : "RulingsSection0",
      "ADInternalAuthNumber" : "ADInternalAuthN",
      "ADInternalAuthNumberDate" : "2006-05-04",
      "SARBAuthAppNumber" : "SARBAuthAppNumber0",
      "SARBAuthRefNumber" : "0"
    },
    "CategoryCode" : "CategoryCode0",
    "CategorySubCode" : "CategorySubCode0",
    "SWIFTDetails" : "SWIFTDetails0",
    "StrateRefNumber" : "StrateRefNumber0",
    "ImportExport" : {
      "ImportControlNumber" : "ImportControlNumber0",
      "TransportDocumentNumber" : "TransportDocumentNumber0",
      "UCR" : "UCR0",
      "MRNNotOnIVS" : "false"
    },
    "ImportExport" : [ {
      "ImportControlNumber" : "ImportControlNumber1",
      "TransportDocumentNumber" : "TransportDocumentNumber1",
      "UCR" : "UCR1",
      "MRNNotOnIVS" : "false"
    } ],
    "AdHocRequirement" : {
      "Subject" : "Subject0",
      "Description" : "Description0"
    },
    "ReversalTrnRefNumber" : "ReversalTrnRefNumber0",
    "ReversalTrnSeqNumber" : "0",
    "BOPDIRTrnReference" : "BOPDIRTrnReference0",
    "BOPDIRADCode" : "BOP",
    "ThirdParty" : {
      "Individual" : {
        "Surname" : "Surname1",
        "Name" : "Name1",
        "Gender" : "M",
        "DateOfBirth" : "2006-05-04",
        "IDNumber" : "IDNumber2",
        "TempResPermitNumber" : "TempResPermitNumber1",
        "PassportNumber" : "PassportNumber0",
        "PassportCountry" : "Pa"
      },
      "TaxNumber" : "TaxNumber0",
      "VATNumber" : "VATNumber0",
      "CustomsClientNumber" : "CustomsClientNumber0",
      "StreetAddress" : {
        "AddressLine1" : "AddressLine10",
        "AddressLine2" : "AddressLine20",
        "Suburb" : "Suburb0",
        "City" : "City0",
        "PostalCode" : "PostalCode0",
        "Province" : "Province0"
      },
      "PostalAddress" : {
        "AddressLine1" : "AddressLine11",
        "AddressLine2" : "AddressLine21",
        "Suburb" : "Suburb1",
        "City" : "City1",
        "PostalCode" : "PostalCode1",
        "Province" : "GAUTENG"
      },
      "ContactDetails" : {
        "ContactName" : "ContactName0",
        "ContactSurname" : "ContactSurname0",
        "Email" : "Email0",
        "Fax" : "Fax1111111",
        "Telephone" : "Telephone0"
      }
    },
    "CannotCategorize" : "CannotCategorize0",
    "LoanRefNumber" : "LoanRefNumber0",
    "LoanTenor" : "LoanTenor0",
    "LoanInterestRate" : "LoanInterestRate0",
    "LocationCountry" : "Lo",
    "CardChargeBack" : "Y",
    "CardIndicator" : "AMEX",
    "ElectronicCommerceIndicator" : "El",
    "POSEntryMode" : "PO",
    "CardFraudulentTransactionIndicator" : "Y",
    "ForeignCardHoldersPurchasesRandValue" : "0",
    "ForeignCardHoldersCashWithdrawalsRandValue" : "0"
  }, {
    "MoneyTransferAgentIndicator" : "AD",
    "RandValue" : "0",
    "ForeignValue" : "0",
    "SARBAuth" : {
      "RulingsSection" : "RulingsSection1",
      "ADInternalAuthNumber" : "ADInternalAuthN",
      "ADInternalAuthNumberDate" : "2006-05-04",
      "SARBAuthAppNumber" : "SARBAuthAppNumber1",
      "SARBAuthRefNumber" : "1"
    },
    "CategoryCode" : "CategoryCode1",
    "CategorySubCode" : "CategorySubCode1",
    "SWIFTDetails" : "SWIFTDetails1",
    "StrateRefNumber" : "StrateRefNumber1",
    "ImportExport" : {
      "ImportControlNumber" : "ImportControlNumber2",
      "TransportDocumentNumber" : "TransportDocumentNumber2",
      "UCR" : "UCR2",
      "MRNNotOnIVS" : "false"
    },
    "ImportExport" : [ {
      "ImportControlNumber" : "ImportControlNumber3",
      "TransportDocumentNumber" : "TransportDocumentNumber3",
      "UCR" : "UCR3",
      "MRNNotOnIVS" : "false"
    } ],
    "Travel" : {
      "Surname" : "Surname2",
      "Name" : "Name2",
      "IDNumber" : "IDNumber44444",
      "DateOfBirth" : "2006-05-04",
      "TempResPermitNumber" : "TempResPermitNumber2"
    },
    "AdHocRequirement" : {
      "Subject" : "Subject1",
      "Description" : "Description1"
    },
    "ReversalTrnRefNumber" : "ReversalTrnRefNumber1",
    "ReversalTrnSeqNumber" : "0",
    "BOPDIRTrnReference" : "BOPDIRTrnReference1",
    "BOPDIRADCode" : "BOP",
    "ThirdParty" : {
      "Individual" : {
        "Surname" : "Surname3",
        "Name" : "Name3",
        "Gender" : "M",
        "DateOfBirth" : "2006-05-04",
        "IDNumber" : "IDNumber5",
        "TempResPermitNumber" : "TempResPermitNumber3",
        "PassportNumber" : "PassportNumber1",
        "PassportCountry" : "Pa"
      },
      "TaxNumber" : "TaxNumber1",
      "VATNumber" : "VATNumber1",
      "CustomsClientNumber" : "CustomsClientNumber1",
      "StreetAddress" : {
        "AddressLine1" : "AddressLine12",
        "AddressLine2" : "AddressLine22",
        "Suburb" : "Suburb2",
        "City" : "City2",
        "PostalCode" : "PostalCode2",
        "Province" : "GAUTENG"
      },
      "PostalAddress" : {
        "AddressLine1" : "AddressLine13",
        "AddressLine2" : "AddressLine23",
        "Suburb" : "Suburb3",
        "City" : "City3",
        "PostalCode" : "PostalCode3",
        "Province" : "GAUTENG"
      },
      "ContactDetails" : {
        "ContactName" : "ContactName1",
        "ContactSurname" : "ContactSurname1",
        "Email" : "Email1",
        "Fax" : "Fax3333333",
        "Telephone" : "Telephone1"
      }
    },
    "CannotCategorize" : "CannotCategorize1",
    "LoanRefNumber" : "LoanRefNumber1",
    "LoanTenor" : "LoanTenor1",
    "LoanInterestRate" : "LoanInterestRate1",
    "LocationCountry" : "Lo",
    "CardChargeBack" : "Y",
    "CardIndicator" : "AMEX",
    "ElectronicCommerceIndicator" : "El",
    "POSEntryMode" : "PO",
    "CardFraudulentTransactionIndicator" : "Y",
    "ForeignCardHoldersPurchasesRandValue" : "0",
    "ForeignCardHoldersCashWithdrawalsRandValue" : "0"
  } ]
},

{ // #1
  "Version" : "BOPCUS2",
  "TrnReference" : "1729892",
  "ReportingQualifier" : "BOPCUS",
  "Flow" : "IN",
  "ValueDate" : "2010-02-17",
  "FlowCurrency" : "GBP",
  "TotalForeignValue" : 150.00,
  "OriginatingBank" : "MIDLGB22",
  "OriginatingCountry" : "GB",
  "ReceivingBank" : "IVESZAJJXXX",
  "ReceivingCountry" : "ZA",
  "IsADLA" : false,
  "Resident" : {
    "Entity" : {
      "EntityName" : "Ewfj  vt--xs/w AF",
      "RegistrationNumber" : "2006/018965/23",
      "AccountIdentifier" : "CASH",
      "AccountNumber" : "123",
      "TaxNumber" : "",
      "VATNumber" : "",
      "CustomsClientNumber" : "",
      "StreetAddress" : {
        "AddressLine1" : "932 Umlp Fsjqii",
        "Suburb" : "Cape Town",
        "City" : "Cape Town"
      },
      "ContactDetails" : {
        "ContactName" : "Yjocoi Wusebixb",
        "Email" : "",
        "Fax" : "0746555249",
        "Telephone" : "7078577995"
      }
    }
  },
  "NonResident" : {
    "Individual" : {
      "Surname" : "MWIPKVZA",
      "Name" : "WKVMWZ",
      "Address" : {
        "Country" : "GB",
        "AddressLine1" : "  "
      }
    }
  },
  "MonetaryAmount" : [ {
    "RandValue" : 1786.52,
    "ForeignValue" : 150.00,
    "SARBAuth" : {
      "SARBAuthRefNumber" : "        "
    },
    "CategoryCode" : "116",
    "CategorySubCode" : "*",
    "ImportExport" : [ {
      "UCR" : "123",
      "MRNNotOnIVS" : false
    } ],
    "CannotCategorize" : "",
    "LocationCountry" : "GB",
    "SequenceNumber" : 1
  } ]
},

{ // #2 (with custom Data)
  customData : {
    DealerType : 'AD'
  },
  transaction : {
    "Version" : "BOPCUS2",
    "TrnReference" : "1725927",
    "ReportingQualifier" : "BOPCUS",
    "Flow" : "OUT",
    "ValueDate" : "2010-02-17",
    "FlowCurrency" : "USD",
    "TotalForeignValue" : 640.90,
    "OriginatingBank" : "IVESZAJJXXX",
    "OriginatingCountry" : "ZA",
    "ReceivingBank" : "SAMBSARIXXX",
    "ReceivingCountry" : "SA",
    "IsADLA" : false,
    "Resident" : {
      "Individual" : {
        "Surname" : "lhv Etbldb",
        "Name" : "Skmwp Yixojb",
        "IDNumber" : "7910125082084",
        "TempResPermitNumber" : "",
        "PassportNumber" : "",
        "AccountName" : "0543844146",
        "AccountIdentifier" : "RESIDENT OTHER",
        "AccountNumber" : "10011135578",
        "TaxNumber" : "0543844146",
        "CustomsClientNumber" : "",
        "StreetAddress" : {
          "AddressLine1" : "4 Yptc Bxf Rffhso",
          "Suburb" : "Secunda",
          "City" : "Secunda"
        },
        "ContactDetails" : {
          "ContactName" : "Hbzjw bfb Qqttip",
          "Email" : "bejvi.eleeesetv4@znagx.sfp",
          "Fax" : "",
          "Telephone" : "3578456881"
        },
        "BeneficiaryID1" : "123",
        "BeneficiaryID2" : "123",
        "BeneficiaryID3" : "123",
        "BeneficiaryID4" : "123"
      }
    },
    "NonResident" : {
      "Individual" : {
        "Surname" : "BHHDDULHFV",
        "Name" : "DKJYJWWL",
        "Address" : {
          "Country" : "SA",
          "AddressLine1" : "  "
        }
      }
    },
    "MonetaryAmount" : [ {
      "RandValue" : 5000.00,
      "ForeignValue" : 640.90,
      "SARBAuth" : {
        "SARBAuthRefNumber" : "B4Di    "
      },
      "CategoryCode" : "501",
      "CategorySubCode" : "",
      "ImportExport" : [ {
        "UCR" : "",
        "MRNNotOnIVS" : false
      } ],
      "CannotCategorize" : "",
      "LocationCountry" : "SA",
      "SequenceNumber" : 1
    } ]
  }
},

{ // #3
  "Version" : "BOPCUS2",
  "TrnReference" : "1725927",
  "ReportingQualifier" : "BOPCUS",
  "Flow" : "OUT",
  "ValueDate" : "2010-02-17",
  "FlowCurrency" : "USD",
  "TotalForeignValue" : 640.90,
  "OriginatingBank" : "IVESZAJJXXX",
  "OriginatingCountry" : "ZA",
  "ReceivingBank" : "SAMBSARIXXX",
  "ReceivingCountry" : "SA",
  "IsADLA" : false,
  "MonetaryAmount" : [ {
    "RandValue" : 5000.00,
    "ForeignValue" : 640.90,
    "SARBAuth" : {
      "SARBAuthRefNumber" : "B4Di    "
    },
    "CategoryCode" : "501",
    "CategorySubCode" : "",
    "ImportExport" : [ {
      "UCR" : "",
      "MRNNotOnIVS" : false
    } ],
    "CannotCategorize" : "",
    "LocationCountry" : "SA",
    "SequenceNumber" : 1
  } ]
},

{ // #4
  "Version" : "BOPCUS2",
  "TrnReference" : "1729892",
  "ReportingQualifier" : "BOPCUS",
  "Flow" : "IN",
  "ValueDate" : "2013-07-03",
  "FlowCurrency" : "GBP",
  "TotalForeignValue" : 150,
  "OriginatingBank" : "MIDLGB22",
  "OriginatingCountry" : "GB",
  "ReceivingBank" : "IVESZAJJXXX",
  "ReceivingCountry" : "ZA",
  "IsADLA" : false,
  "Resident" : {
    "Entity" : {
      "EntityName" : "Ewfjvtxsw AF",
      "RegistrationNumber" : "2006/018965/23",
      "AccountIdentifier" : "CASH",
      "AccountNumber" : "123",
      "TaxNumber" : "123",
      "VATNumber" : "NO VAT NUMBER",
      "CustomsClientNumber" : "12345678",
      "StreetAddress" : {
        "AddressLine1" : "932 Umlp Fsjqii",
        "Suburb" : "Cape Town",
        "City" : "Cape Town",
        "Province" : "WESTERN CAPE"
      },
      "ContactDetails" : {
        "ContactName" : "Yjocoi Wusebixb",
        "Email" : "",
        "Fax" : "0746555249",
        "Telephone" : "7078577995",
        "ContactSurname" : "Groober"
      },
      "TradingName" : "Foo",
      "InstitutionalSector" : "01",
      "IndustrialClassification" : "01",
      "PostalAddress" : {
        "AddressLine1" : "Foo",
        "Suburb" : "Foo",
        "City" : "Foo",
        "Province" : "MPUMALANGA",
        "PostalCode" : "6616"
      }
    }
  },
  "NonResident" : {
    "Individual" : {
      "Surname" : "MWIPKVZA",
      "Name" : "WKVMWZ",
      "Address" : {
        "Country" : "GB",
        "AddressLine1" : "  "
      },
      "AccountIdentifier" : "CASH"
    }
  },
  "MonetaryAmount" : [ {
    "RandValue" : 1786.52,
    "ForeignValue" : 150,
    "SARBAuth" : {
      "SARBAuthRefNumber" : "        "
    },
    "CategoryCode" : "106",
    "CategorySubCode" : "",
    "ImportExport" : [ {
      "UCR" : "7ZA0398346346",
      "MRNNotOnIVS" : false,
      "PaymentCurrencyCode" : "GBP",
      "PaymentValue" : "140"
    }, {
      "UCR" : "4ZA3453453444",
      "PaymentCurrencyCode" : "GBP",
      "PaymentValue" : "10"
    } ],
    "CannotCategorize" : "",
    "LocationCountry" : "GB",
    "SequenceNumber" : 1,
    "MoneyTransferAgentIndicator" : "AD"
  } ]
},

{ // #5 "NEW" transaction
  "TrnReference" : "1725927",
  "Flow" : "OUT",
  "ValueDate" : "2010-02-17",
  "FlowCurrency" : "USD",
  "TotalForeignValue" : 150,
  "MonetaryAmount" : [ {
    "ForeignValue" : 150,
    "SequenceNumber" : 1,
    "MoneyTransferAgentIndicator" : "AD"
  } ]
},

{ // #6 "NEW" transaction2
  "Version" : "FINSURV",
  "TrnReference" : "LEG3369868",
  "ReportingQualifier" : "BOPCUS",
  "Flow" : "OUT",
  "ValueDate" : "2010-02-17",
  "FlowCurrency" : "EUR",
  "TotalForeignValue" : 75.00,
  "IsADLA" : false,
  "Resident" : {},
  "NonResident" : {},
  "MonetaryAmount" : [ {
    "MoneyTransferAgentIndicator" : "AD",
    "ForeignValue" : 75.00,
    "SARBAuth" : {},
    "CategoryCode" : "",
    "CategorySubCode" : "",
    "SequenceNumber" : 1
  } ]
}, { // #7 Capitec as BOPCUS2 when it should have been BOPCARD RESIDENT (error no Money)
  "Version" : "BOPCUS2",
  "TrnReference" : "103201300000683",
  "ReportingQualifier" : "BOPCUS",
  "Flow" : "OUT",
  "ValueDate" : "2013-07-07",
  "FlowCurrency" : "MZN",
  "TotalForeignValue" : 1000.00,
  "BranchCode" : "470010",
  "BranchName" : "CAPITEC BANK CPC",
  "IsADLA" : false,
  "Resident" : {
    "Entity" : {
      "EntityName" : "",
      "TradingName" : "",
      "RegistrationNumber" : "",
      "InstitutionalSector" : "",
      "IndustrialClassification" : "",
      "AccountName" : "SAVINGS ACCOUNT                                             ",
      "AccountIdentifier" : "CASH",
      "AccountNumber" : "1147871459",
      "TaxNumber" : "",
      "VATNumber" : "",
      "StreetAddress" : {
        "Province" : "GAUTENG",
        "AddressLine1" : "415 EXT. 23",
        "AddressLine2" : "",
        "Suburb" : "RATANDA",
        "City" : "RATANDA",
        "PostalCode" : "1441"
      },
      "PostalAddress" : {
        "Province" : "GAUTENG",
        "AddressLine1" : "415 EXT. 23",
        "AddressLine2" : "",
        "Suburb" : "RATANDA",
        "City" : "RATANDA",
        "PostalCode" : "1441"
      },
      "ContactDetails" : {
        "ContactName" : "FACKSON",
        "ContactSurname" : "NTELANE",
        "Email" : "",
        "Fax" : "",
        "Telephone" : "0838984241"
      },
      "CardNumber" : "5284972024346278",
      "SupplementaryCardIndicator" : "N"
    }
  },
  "NonResident" : {
    "Entity" : {
      "CardMerchantName" : "MMN1                   GAZA        ",
      "CardMerchantCode" : "6011",
      "AccountIdentifier" : "CASH",
      "AccountNumber" : "",
      "Address" : {
        "Country" : "MZ"
      }
    }
  }
}, { // #8 Capitec as BOPCARD RESIDENT (error no Money)
  "Version" : "FINSURV",
  "TrnReference" : "103201300000683",
  "ReportingQualifier" : "BOPCARD RESIDENT",
  "Flow" : "OUT",
  "ValueDate" : "2013-07-07",
  "FlowCurrency" : "MZN",
  "TotalForeignValue" : 1000.00,
  "BranchCode" : "470010",
  "BranchName" : "CAPITEC BANK CPC",
  "IsADLA" : false,
  "Resident" : {
    "Entity" : {
      "EntityName" : "",
      "TradingName" : "",
      "RegistrationNumber" : "",
      "InstitutionalSector" : "",
      "IndustrialClassification" : "",
      "AccountName" : "SAVINGS ACCOUNT                                             ",
      "AccountIdentifier" : "CASH",
      "AccountNumber" : "1147871459",
      "TaxNumber" : "",
      "VATNumber" : "",
      "StreetAddress" : {
        "Province" : "GAUTENG",
        "AddressLine1" : "415 EXT. 23",
        "AddressLine2" : "",
        "Suburb" : "RATANDA",
        "City" : "RATANDA",
        "PostalCode" : "1441"
      },
      "PostalAddress" : {
        "Province" : "GAUTENG",
        "AddressLine1" : "415 EXT. 23",
        "AddressLine2" : "",
        "Suburb" : "RATANDA",
        "City" : "RATANDA",
        "PostalCode" : "1441"
      },
      "ContactDetails" : {
        "ContactName" : "FACKSON",
        "ContactSurname" : "NTELANE",
        "Email" : "",
        "Fax" : "",
        "Telephone" : "0838984241"
      },
      "CardNumber" : "5284972024346278",
      "SupplementaryCardIndicator" : "N"
    }
  },
  "NonResident" : {
    "Entity" : {
      "CardMerchantName" : "MMN1                   GAZA        ",
      "CardMerchantCode" : "6011",
      "AccountIdentifier" : "CASH",
      "AccountNumber" : "",
      "Address" : {
        "Country" : "MZ"
      }
    }
  }
}, { // #9 "BOPCARD NON RESIDENT" transaction1
  "Version" : "FINSURV",
  "TrnReference" : "LEG3369868",
  "ReportingQualifier" : "BOPCARD NON RESIDENT",
  "Flow" : "IN",
  "ValueDate" : "2013-02-17",
  "FlowCurrency" : "ZAR",
  "TotalForeignValue" : 75.00,
  "IsADLA" : false,
  "Resident" : {
    "Individual" : {
      "Surname" : "lhv Etbldb",
      "Name" : "Skmwp Yixojb",
      "IDNumber" : "7910125082084",
      "TempResPermitNumber" : "",
      "PassportNumber" : "",
      "AccountName" : "0543844146",
      "AccountIdentifier" : "RESIDENT OTHER",
      "AccountNumber" : "10011135578",
      "TaxNumber" : "0543844146",
      "CustomsClientNumber" : "",
      "StreetAddress" : {
        "AddressLine1" : "4 Yptc Bxf Rffhso",
        "Suburb" : "Secunda",
        "City" : "Secunda"
      },
      "ContactDetails" : {
        "ContactName" : "Hbzjw bfb Qqttip",
        "Email" : "bejvi.eleeesetv4@znagx.sfp",
        "Fax" : "",
        "Telephone" : "3578456881"
      },
      "BeneficiaryID1" : "",
      "BeneficiaryID2" : "",
      "BeneficiaryID3" : "",
      "BeneficiaryID4" : ""
    }
  },
  "NonResident" : {},
  "MonetaryAmount" : [ {
    "MoneyTransferAgentIndicator" : "AD",
    "SARBAuth" : {},
    "CategoryCode" : "",
    "CategorySubCode" : "",
    "SequenceNumber" : 1
  } ]
}, {// #10 "BOPCARD NON RESIDENT" transaction2
  "Version" : "FINSURV",
  "TrnReference" : "LEG3369868",
  "ReportingQualifier" : "BOPCARD NON RESIDENT",
  "Flow" : "IN",
  "ValueDate" : "2013-02-17",
  "FlowCurrency" : "ZAR",
  "TotalForeignValue" : 75.00,
  "IsADLA" : false,
  "MonetaryAmount" : [ {
    "MoneyTransferAgentIndicator" : "AD",
    "SARBAuth" : {},
    "CategoryCode" : "",
    "CategorySubCode" : "",
    "SequenceNumber" : 1
  } ]
}, { // #11
  "Version" : "FINSURV",
  "TrnReference" : "CTID:9471615",
  "ReportingQualifier" : "BOPCUS",
  "Flow" : "OUT",
  "ValueDate" : "2013-07-03",
  "FlowCurrency" : "EUR",
  "TotalForeignValue" : 10000,
  "OriginatingBank" : "IVESZAJJ",
  "OriginatingCountry" : "ZA",
  "ReceivingBank" : "BRESUS3N",
  "ReceivingCountry" : "US",
  "IsADLA" : false,
  "Resident" : {
    "Exception" : {
      "ExceptionName" : "NON RESIDENT RAND"
    }
  },
  "MonetaryAmount" : [ {
    "MoneyTransferAgentIndicator" : "AD",
    "RandValue" : 110000,
    "ForeignValue" : 10000,
    "SARBAuth" : {
      "ADInternalAuthNumber" : "5678",
      "ADInternalAuthNumberDate" : "2013-07-22"
    },
    "CategoryCode" : "230",
    "CategorySubCode" : "",
    "LocationCountry" : "US",
    "SequenceNumber" : 1
  } ],
  "NonResident" : {
    "Individual" : {}
  }
}, { // #12
  "Version" : "FINSURV",
  "TrnReference" : "CTID:8683895",
  "ReportingQualifier" : "BOPCARD NON RESIDENT",
  "Flow" : "IN",
  "ValueDate" : "2013-05-29",
  "FlowCurrency" : "EUR",
  "TotalForeignValue" : 765.1,
  "IsADLA" : false,
  "MonetaryAmount" : [ {
    "MoneyTransferAgentIndicator" : "CARD",
    "SARBAuth" : {},
    "CategoryCode" : "",
    "CategorySubCode" : "",
    "ForeignCardHoldersPurchasesRandValue" : 765.1,
    "ForeignCardHoldersCashWithdrawalsRandValue" : "0",
    "SequenceNumber" : 1
  } ]
}, { // #13
  "Version" : "FINSURV",
  "TrnReference" : "CTID:8683895",
  "ReportingQualifier" : "BOPCARD NON RESIDENT",
  "Flow" : "IN",
  "ValueDate" : "2013-05-29",
  "FlowCurrency" : "EUR",
  "TotalForeignValue" : 765.10,
  "IsADLA" : false,
  "MonetaryAmount" : [ {
    "MoneyTransferAgentIndicator" : "CARD",
    "SARBAuth" : {},
    "CategoryCode" : "",
    "CategorySubCode" : "",
    "ForeignCardHoldersPurchasesRandValue" : 765.10,
    "ForeignCardHoldersCashWithdrawalsRandValue" : 0.00,
    "SequenceNumber" : 1
  } ]
}, { // #14
  "Version" : "FINSURV",
  "TrnReference" : "20130802OPS10280632363YHEA0107",
  "ReportingQualifier" : "BOPCUS",
  "Flow" : "OUT",
  "ValueDate" : "2013-08-02",
  "FlowCurrency" : "EUR",
  "TotalForeignValue" : 152.16,
  "BranchName" : "HSBC JOHANNESBURG",
  "OriginatingBank" : "HSBCZAJJ",
  "OriginatingCountry" : "ZA",
  "CorrespondentBank" : "BOTKBEBX",
  "CorrespondentCountry" : "BE",
  "ReceivingBank" : "BOTKBEBX&#13;",
  "ReceivingCountry" : "BE",
  "IsADLA" : false,
  "Resident" : {
    "Entity" : {
      "EntityName" : "CLIENTNAME",
      "TradingName" : "CLIENTNAME",
      "RegistrationNumber" : "1987/006192/07",
      "IndustrialClassification" : "",
      "AccountName" : "CLIENTNAME",
      "AccountIdentifier" : "RESIDENT OTHER",
      "AccountNumber" : "121-001523-001",
      "TaxNumber" : "0000000000",
      "StreetAddress" : {
        "Province" : "GAUTENG",
        "AddressLine1" : "99999 HENDRIK VERWOERD DRIVE",
        "Suburb" : "SUBURB1",
        "City" : "PRETORIA",
        "PostalCode" : "0250"
      },
      "PostalAddress" : {
        "Province" : "GAUTENG",
        "AddressLine1" : "99999 HENDRIK VERWOERD DRIVE",
        "Suburb" : "SUBURB1",
        "City" : "PRETORIA",
        "PostalCode" : "0250"
      },
      "ContactDetails" : {
        "ContactName" : "Contact1",
        "ContactSurname" : "CSurname1",
        "Email" : ""
      }
    }
  },
  "NonResident" : {
    "Entity" : {
      "EntityName" : "TestName1",
      "Address" : {}
    }
  },
  "MonetaryAmount" : [ {
    "MoneyTransferAgentIndicator" : "AD",
    "RandValue" : 2039.98,
    "ForeignValue" : 152.16,
    "SARBAuth" : {},
    "CategoryCode" : "601",
    "CategorySubCode" : "01",
    "SWIFTDetails" : "HUBOR3H0229772",
    "BOPDIRTrnReference" : "",
    "BOPDIRADCode" : "",
    "LoanRefNumber" : "",
    "LoanTenor" : "",
    "LoanInterestRate" : "",
    "SequenceNumber" : 1
  } ]
} ]
