package za.co.synthesis.mapping.core;

/**
 * User: jake
 * Date: 3/19/16
 * Time: 11:23 PM
 * The interface to an object that can transform values e.g. to upper case
 */
public interface ITransform {
  String transform(String value);
}
