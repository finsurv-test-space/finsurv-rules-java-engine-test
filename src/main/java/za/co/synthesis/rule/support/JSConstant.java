package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.*;
import za.co.synthesis.javascript.JSIdentifier;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 3/10/15
 * Time: 10:19 AM
 * To change this template use File | Settings | File Templates.
 */
public class JSConstant {
  public static ResidenceStatus residenceStatus(Object obj) {
    if (obj != null) {
      if (obj instanceof JSIdentifier) {
        JSIdentifier jsId = (JSIdentifier)obj;
        if (jsId.getName().equals("resStatus")) {
          String appliedId = jsId.getApply().getName();
          if (appliedId.equals("NONRES"))
            return ResidenceStatus.NonResident;
          if (appliedId.equals("RESIDENT"))
            return ResidenceStatus.Resident;
          if (appliedId.equals("HOLDCO"))
            return ResidenceStatus.HOLDCO;
          if (appliedId.equals("IHQ"))
            return ResidenceStatus.IHQ;
        }
      }
      return ResidenceStatus.Unknown;
    }
    return null;
  }

  public static JSIdentifier jsResidenceStatus(ResidenceStatus obj) {
    JSIdentifier result = null;
    if (obj != null && obj != ResidenceStatus.Unknown) {
      result = new JSIdentifier("resStatus");
      if (obj == ResidenceStatus.NonResident)
        result.setApply(new JSIdentifier("NONRES"));
      if (obj == ResidenceStatus.Resident)
        result.setApply(new JSIdentifier("RESIDENT"));
      if (obj == ResidenceStatus.HOLDCO)
        result.setApply(new JSIdentifier("HOLDCO"));
      if (obj == ResidenceStatus.IHQ)
        result.setApply(new JSIdentifier("IHQ"));
    }
    return result;
  }

  public static CounterpartyInstituition counterpartyStatus(Object obj) {
    if (obj != null) {
      if (obj instanceof JSIdentifier) {
        JSIdentifier jsId = (JSIdentifier)obj;
        if (jsId.getName().equals("cpStatus")) {
          String appliedId = jsId.getApply().getName();
          if (appliedId.equals("ONSHORE"))
            return CounterpartyInstituition.OnshoreAD;
          if (appliedId.equals("ONSHOREAD"))
            return CounterpartyInstituition.OnshoreAD;
          if (appliedId.equals("ONSHOREOTHER"))
            return CounterpartyInstituition.OnshoreOther;
          if (appliedId.equals("OFFSHORE"))
            return CounterpartyInstituition.Offshore;
        }
      }
      return CounterpartyInstituition.Unknown;
    }
    return null;
  }

  public static JSIdentifier jsCounterpartyStatus(CounterpartyInstituition obj) {
    JSIdentifier result = null;
    if (obj != null && obj != CounterpartyInstituition.Unknown) {
      result = new JSIdentifier("cpStatus");
      if (obj == CounterpartyInstituition.OnshoreAD)
        result.setApply(new JSIdentifier("ONSHOREAD"));
      if (obj == CounterpartyInstituition.OnshoreOther)
        result.setApply(new JSIdentifier("ONSHOREOTHER"));
      if (obj == CounterpartyInstituition.Offshore)
        result.setApply(new JSIdentifier("OFFSHORE"));
    }
    return result;
  }

  public static AccountHolderStatus accountHolderStatus(Object obj) {
    if (obj != null) {
      if (obj instanceof JSIdentifier) {
        JSIdentifier jsId = (JSIdentifier)obj;
        if (jsId.getName().equals("accStatus")) {
          String appliedId = jsId.getApply().getName();
          if (appliedId.equals("Entity"))
            return AccountHolderStatus.Entity;
          if (appliedId.equals("Individual"))
            return AccountHolderStatus.Individual;
        }
      }
      return AccountHolderStatus.Unknown;
    }
    return null;
  }

  public static JSIdentifier jsAccountHolderStatus(AccountHolderStatus obj) {
    JSIdentifier result = null;
    if (obj != null && obj != AccountHolderStatus.Unknown) {
      result = new JSIdentifier("accStatus");
      if (obj == AccountHolderStatus.Entity)
        result.setApply(new JSIdentifier("Entity"));
      if (obj == AccountHolderStatus.Individual)
        result.setApply(new JSIdentifier("Individual"));
    }
    return result;
  }

  public static BankAccountType bankAccountType(Object obj) {
    if (obj != null) {
      if (obj instanceof JSIdentifier) {
        JSIdentifier jsId = (JSIdentifier)obj;
        if (jsId.getName().equals("accType")) {
          String appliedId = jsId.getApply().getName();
          if (appliedId.equals("LOCAL_ACC") || appliedId.equals("RAND") || appliedId.equals("NAD"))
            return BankAccountType.LOCAL_ACC;
          if (appliedId.equals("CFC"))
            return BankAccountType.CFC;
          if (appliedId.equals("FCA"))
            return BankAccountType.FCA;
          if (appliedId.equals("NOSTRO"))
            return BankAccountType.NOSTRO;
          if (appliedId.equals("VOSTRO"))
            return BankAccountType.VOSTRO;
          if (appliedId.equals("CASH_LOCAL") || appliedId.equals("CASH_ZAR") || appliedId.equals("CASH_NAD"))
            return BankAccountType.CASH_LOCAL;
          if (appliedId.equals("CASH_CURR"))
            return BankAccountType.CASH_CURR;
        }
      }
      return BankAccountType.Unknown;
    }
    return null;
  }

  public static JSIdentifier jsBankAccountType(BankAccountType obj) {
    JSIdentifier result = null;
    if (obj != null && obj != BankAccountType.Unknown) {
      result = new JSIdentifier("accType");
      if (obj == BankAccountType.LOCAL_ACC)
        result.setApply(new JSIdentifier("LOCAL_ACC"));
      if (obj == BankAccountType.CFC)
        result.setApply(new JSIdentifier("CFC"));
      if (obj == BankAccountType.FCA)
        result.setApply(new JSIdentifier("FCA"));
      if (obj == BankAccountType.NOSTRO)
        result.setApply(new JSIdentifier("NOSTRO"));
      if (obj == BankAccountType.VOSTRO)
        result.setApply(new JSIdentifier("VOSTRO"));
      if (obj == BankAccountType.CASH_LOCAL)
        result.setApply(new JSIdentifier("CASH_LOCAL"));
      if (obj == BankAccountType.CASH_CURR)
        result.setApply(new JSIdentifier("CASH_CURR"));
    }
    return result;
  }

  public static DrCr drCr(Object obj) {
    if (obj != null) {
      if (obj instanceof JSIdentifier) {
        JSIdentifier jsId = (JSIdentifier)obj;
        if (jsId.getName().equals("drcr")) {
          String appliedId = jsId.getApply().getName();
          if (appliedId.equals("DR"))
            return DrCr.DR;
          if (appliedId.equals("CR"))
            return DrCr.CR;
        }
      }
      return DrCr.Unknown;
    }
    return null;
  }

  public static JSIdentifier jsDrCr(DrCr obj) {
    JSIdentifier result = null;
    if (obj != null && obj != DrCr.Unknown) {
      result = new JSIdentifier("drcr");
      if (obj == DrCr.DR)
        result.setApply(new JSIdentifier("DR"));
      if (obj == DrCr.CR)
        result.setApply(new JSIdentifier("CR"));
    }
    return result;
  }

  public static ReportingAccountType reportingAccountType(Object obj) {
    if (obj != null) {
      if (obj instanceof JSIdentifier) {
        JSIdentifier jsId = (JSIdentifier)obj;
        if (jsId.getName().equals("at")) {
          String appliedId = jsId.getApply().getName();
          if (appliedId.equals("RE_OTH"))
            return ReportingAccountType.RE_OTH;
          if (appliedId.equals("RE_CFC"))
            return ReportingAccountType.RE_CFC;
          if (appliedId.equals("RE_CFDC"))
            return ReportingAccountType.RE_CFDC;
          if (appliedId.equals("RE_FCA"))
            return ReportingAccountType.RE_FCA;
          if (appliedId.equals("RE_FCDA"))
            return ReportingAccountType.RE_FCDA;
          if (appliedId.equals("_CASH_"))
            return ReportingAccountType.CASH;
          if (appliedId.equals("VOSTRO"))
            return ReportingAccountType.VOSTRO;
          if (appliedId.equals("NR_OTH"))
            return ReportingAccountType.NR_OTH;
          if (appliedId.equals("NR_RND"))
            return ReportingAccountType.NR_RND;
          if (appliedId.equals("NR_NAD"))
            return ReportingAccountType.NR_NAD;
          if (appliedId.equals("NR_KWT"))
            return ReportingAccountType.NR_KWT;
          if (appliedId.equals("NR_LSL"))
            return ReportingAccountType.NR_LSL;
          if (appliedId.equals("NR_FCA"))
            return ReportingAccountType.NR_FCA;
          if (appliedId.equals("NR_FCDA"))
            return ReportingAccountType.NR_FCDA;
          if (appliedId.equals("RE_FBC"))
            return ReportingAccountType.RE_FBC;
        }
      }
      return ReportingAccountType.Unknown;
    }
    return null;
  }

  public static JSIdentifier jsReportingAccountType(ReportingAccountType obj) {
    JSIdentifier result = null;
    if (obj != null && obj != ReportingAccountType.Unknown) {
      result = new JSIdentifier("at");
      if (obj == ReportingAccountType.RE_OTH)
        result.setApply(new JSIdentifier("RE_OTH"));
      if (obj == ReportingAccountType.RE_CFC)
        result.setApply(new JSIdentifier("RE_CFC"));
      if (obj == ReportingAccountType.RE_CFDC)
        result.setApply(new JSIdentifier("RE_CFDC"));
      if (obj == ReportingAccountType.RE_FCA)
        result.setApply(new JSIdentifier("RE_FCA"));
      if (obj == ReportingAccountType.RE_FCDA)
        result.setApply(new JSIdentifier("RE_FCDA"));
      if (obj == ReportingAccountType.CASH)
        result.setApply(new JSIdentifier("_CASH_"));
      if (obj == ReportingAccountType.VOSTRO)
        result.setApply(new JSIdentifier("VOSTRO"));
      if (obj == ReportingAccountType.NR_OTH)
        result.setApply(new JSIdentifier("NR_OTH"));
      if (obj == ReportingAccountType.NR_RND)
        result.setApply(new JSIdentifier("NR_RND"));
      if (obj == ReportingAccountType.NR_NAD)
        result.setApply(new JSIdentifier("NR_NAD"));
      if (obj == ReportingAccountType.NR_KWT)
        result.setApply(new JSIdentifier("NR_KWT"));
      if (obj == ReportingAccountType.NR_LSL)
        result.setApply(new JSIdentifier("NR_LSL"));
      if (obj == ReportingAccountType.NR_FCA)
        result.setApply(new JSIdentifier("NR_FCA"));
      if (obj == ReportingAccountType.NR_FCDA)
        result.setApply(new JSIdentifier("NR_FCDA"));
      if (obj == ReportingAccountType.RE_FBC)
        result.setApply(new JSIdentifier("RE_FBC"));
    }
    return result;
  }


  public static FlowType flowType(Object obj) {
    if (obj != null) {
      if (obj instanceof JSIdentifier) {
        JSIdentifier jsId = (JSIdentifier)obj;
        if (jsId.getName().equals("flowDir")) {
          String appliedId = jsId.getApply().getName();
          if (appliedId.equals("IN"))
            return FlowType.Inflow;
          if (appliedId.equals("OUT"))
            return FlowType.Outflow;
        }
      }
      return FlowType.Unknown;
    }
    return null;
  }

  public static JSIdentifier jsFlowType(FlowType obj) {
    JSIdentifier result = null;
    if (obj != null && obj != FlowType.Unknown) {
      result = new JSIdentifier("flowDir");
      if (obj == FlowType.Inflow)
        result.setApply(new JSIdentifier("IN"));
      if (obj == FlowType.Outflow)
        result.setApply(new JSIdentifier("OUT"));
    }
    return result;
  }

  public static ReportableType reportableType(Object obj) {
    if (obj != null) {
      if (obj instanceof JSIdentifier) {
        JSIdentifier jsId = (JSIdentifier)obj;
        if (jsId.getName().equals("rep")) {
          String appliedId = jsId.getApply().getName();
          if (appliedId.equals("UNSUPPORTED"))
            return ReportableType.Unsupported;
          if (appliedId.equals("ILLEGAL"))
            return ReportableType.Illegal;
          if (appliedId.equals("REPORTABLE"))
            return ReportableType.Reportable;
          if (appliedId.equals("ZZ1REPORTABLE"))
            return ReportableType.ZZ1Reportable;
          if (appliedId.equals("NONREPORTABLE"))
            return ReportableType.NotReportable;
        }
      }
      return ReportableType.Unknown;
    }
    return null;
  }

  public static JSIdentifier jsReportableType(ReportableType obj) {
    JSIdentifier result = null;
    if (obj != null && obj != ReportableType.Unknown) {
      result = new JSIdentifier("rep");
      if (obj == ReportableType.Unsupported)
        result.setApply(new JSIdentifier("UNSUPPORTED"));
      else
      if (obj == ReportableType.Illegal)
        result.setApply(new JSIdentifier("ILLEGAL"));
      else
      if (obj == ReportableType.Reportable)
        result.setApply(new JSIdentifier("REPORTABLE"));
      else
      if (obj == ReportableType.ZZ1Reportable)
        result.setApply(new JSIdentifier("ZZ1REPORTABLE"));
      else
      if (obj == ReportableType.NotReportable)
        result.setApply(new JSIdentifier("NONREPORTABLE"));
    }
    return result;
  }

  public static ReportableDecision reportableDecision(Object obj) {
    if (obj != null) {
      if (obj instanceof JSIdentifier) {
        JSIdentifier jsId = (JSIdentifier)obj;
        if (jsId.getName().equals("dec")) {
          String appliedId = jsId.getApply().getName();
          if (appliedId.equals("Unknown"))
            return ReportableDecision.Unknown;
          if (appliedId.equals("Illegal"))
            return ReportableDecision.Illegal;
          if (appliedId.equals("ReportToRegulator"))
            return ReportableDecision.ReportToRegulator;
          if (appliedId.equals("DoNotReport"))
            return ReportableDecision.DoNotReport;
        }
      }
      return ReportableDecision.Unknown;
    }
    return null;
  }

  public static JSIdentifier jsReportableDecision(ReportableDecision obj) {
    JSIdentifier result = null;
    if (obj != null && obj != ReportableDecision.Unknown) {
      result = new JSIdentifier("dec");
      if (obj == ReportableDecision.Unknown)
        result.setApply(new JSIdentifier("Unknown"));
      else
      if (obj == ReportableDecision.Illegal)
        result.setApply(new JSIdentifier("Illegal"));
      else
      if (obj == ReportableDecision.ReportToRegulator)
        result.setApply(new JSIdentifier("ReportToRegulator"));
      else
      if (obj == ReportableDecision.DoNotReport)
        result.setApply(new JSIdentifier("DoNotReport"));
    }
    return result;
  }
}
