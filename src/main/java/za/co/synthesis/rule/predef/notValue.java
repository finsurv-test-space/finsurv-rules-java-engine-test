package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * User: jake
 * Date: 8/5/14
 * Time: 10:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class notValue implements IAssertionFunction {
  private IStringResolver constant;

  public notValue(IStringResolver constant) {
    this.constant = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    String constantValue = constant.resolve(context, value);
    return (value == null || !value.equals(constantValue));
  }
}
/*
function notValue(constant) {
  return function(context, value) {
    return !value || value != constant;
  }
}
*/