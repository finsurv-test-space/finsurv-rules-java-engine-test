package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 1:00 PM
 * Determine if there are are import/export entries for the current monetary amount
 */
public class notEmptyImportExport implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return context.getImportExportSize(context.getCurrentMoneyInstance()) > 0;
  }
}
/*
function notEmptyImportExport(context, value) {
  return context.getImportExportSize(context.currentMoneyInstance) > 0;
}
*/

