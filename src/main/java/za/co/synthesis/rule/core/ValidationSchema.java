package za.co.synthesis.rule.core;

import java.util.ArrayList;

/**
 * Created by jake on 4/3/16.
 */
public class ValidationSchema {
  public enum NodeType {
    Field,
    Structure,
    StructureList
  }

  public static class Node {
    private final String name;
    protected final NodeType nodeType;
    protected boolean deprecated;

    public Node(NodeType nodeType, String name, boolean deprecated) {
      this.nodeType = nodeType;
      this.name = name;
      this.deprecated = deprecated;
    }

    public final String getName() {
      return name;
    }

    public final NodeType getNodeType() {
      return nodeType;
    }

    public boolean isDeprecated() {
      return deprecated;
    }
  }

  public static class NodeList extends ArrayList<Node> {}

  public static class FieldNode extends Node {
    private final Integer minLen;
    private final Integer maxLen;
    private final Integer len;

    public FieldNode(String name, Integer minLen, Integer maxLen, Integer len, boolean deprecated) {
      super(NodeType.Field, name, deprecated);
      this.minLen = minLen;
      this.maxLen = maxLen;
      this.len = len;
    }

    public Integer getMinLen() {
      return minLen;
    }

    public Integer getMaxLen() {
      return maxLen;
    }

    public Integer getLen() {
      return len;
    }
  }

  public static class Structure extends Node {
    private final NodeList fields = new NodeList();

    public Structure(String name, boolean isList) {
      super(isList ? NodeType.StructureList : NodeType.Structure, name, false);
    }

    public final void setDeprecated(boolean deprecated) {
      this.deprecated = deprecated;
    }

    public final NodeList getNodes() {
      return fields;
    }
  }
}
