package za.co.synthesis.rule.core.impl;

import za.co.synthesis.rule.core.IFinsurvContextCache;
import za.co.synthesis.rule.support.CentralCustomValidationCache.ValidationCacheEntry;

import java.util.ArrayList;
import java.util.List;

public class DefaultFinsurvContextCache implements IFinsurvContextCache {
    private final List<ValidationCacheEntry> cache = new ArrayList<ValidationCacheEntry>();

    @Override
    public void clearCache() {
      cache.clear();
    }

    @Override
    public void marshalValidate(ValidationCacheEntry entry) {
        cache.add(entry);
    }

    @Override
    public List<ValidationCacheEntry> unmarshalCache() {
        return cache;
    }
}
