package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 12/17/15
 * Time: 9:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class evalMoneyField implements IAssertionFunction {
  private IStringResolver field;
  private IAssertionFunction fEval = null;

  public evalMoneyField(IStringResolver field, IAssertionFunction fEval) {
    this.field = field;
    this.fEval = fEval;
  }

  public boolean execute(FinsurvContext context, Object value) {
    int index = context.getCurrentMoneyInstance();
    if (index == -1)
      index = 0;
    Object fieldValue = context.getMoneyField(index, field.resolve(context, value));
    if (fieldValue != null && fEval != null) {
      return fEval.execute(context, fieldValue);
    }
    return false;
  }
}


/*
    _export.evalMoneyField = function (field, fEval) {
        return function(context, value) {
            var index = context.currentMoneyInstance;
            if (index == -1)
                index = 0;

            var fieldValue = context.getMoneyField(index, field);

            if (fieldValue) {
                return fEval(context, fieldValue);
            }
            return false;
        };
    }
*/