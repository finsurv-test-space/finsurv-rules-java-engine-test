package za.co.synthesis.rule.core;

/**
 * Created by jake on 6/28/16.
 */
public enum AccountHolderStatus {
  Unknown,
  Individual,
  Entity
}
