package za.co.synthesis.rule.difference.validation;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.difference.HTMLDiffUtils;
import za.co.synthesis.rule.difference.IDifference;

public class CodeMessageDifference implements IDifference {
  private final DifferenceType type;
  private final String ruleName;
  private final String oldCode;
  private final String oldMessage;
  private final String newCode;
  private final String newMessage;

  public CodeMessageDifference(String ruleName, DifferenceType type,
                               String oldCode, String oldMessage,
                               String newCode, String newMessage) {
    this.ruleName = ruleName;
    this.type = type;
    this.oldCode = oldCode;
    this.oldMessage = oldMessage;
    this.newCode = newCode;
    this.newMessage = newMessage;
  }

  public static CodeMessageDifference createAddedCodeMessage(String ruleName, String code, String message) {
    return new CodeMessageDifference(ruleName, DifferenceType.Added, null, null, code, message);
  }

  public static CodeMessageDifference createChangedCodeMessage(String ruleName, String oldCode, String oldMessage,
                                                               String newCode, String newMessage) {
    return new CodeMessageDifference(ruleName, DifferenceType.Changed, oldCode, oldMessage, newCode, newMessage);
  }

  public static CodeMessageDifference createRemovedCodeMessage(String ruleName, String oldCode, String oldMessage) {
    return new CodeMessageDifference(ruleName, DifferenceType.Removed, oldCode, oldMessage, null, null);
  }

  public String getRuleName() {
    return ruleName;
  }

  public String getOldCode() {
    return oldCode;
  }

  public String getOldMessage() {
    return oldMessage;
  }

  public String getNewCode() {
    return newCode;
  }

  public String getNewMessage() {
    return newMessage;
  }

  @Override
  public DifferenceType getType() {
    return type;
  }

  @Override
  public String diffHTML() {
    if (type == DifferenceType.Added) {
      if (newCode != null && newMessage != null)
        return String.format("<ins>message('%s', '%s', '%s')</ins>",
                ruleName, newCode, newMessage);
      if (newCode != null)
        return String.format("<ins>message('%s', '%s', null)</ins>", ruleName, newCode);
      if (newMessage != null)
        return String.format("<ins>message('%s', null, '%s')</ins>", ruleName, newMessage);
    }
    else
    if (type == DifferenceType.Changed) {
      if (oldMessage != null && newMessage != null) {
        return String.format("message('%s', %s, \"%s\")",
                ruleName, HTMLDiffUtils.composeValues(oldCode, newCode),
                HTMLDiffUtils.composeStrings(oldMessage, newMessage));
      }
      else {
        return String.format("message('%s', %s, %s)",
                ruleName, HTMLDiffUtils.composeValues(oldCode, newCode),
                HTMLDiffUtils.composeValues(oldMessage, newMessage));
      }
    }
    else
    if (type == DifferenceType.Removed) {
      if (oldCode != null && oldMessage != null)
        return String.format("<del>message('%s', '%s', '%s')</del>",
                ruleName, oldCode, oldMessage);
      if (oldCode != null)
        return String.format("<del>message('%s', '%s', null)</del>", oldCode, oldMessage);
      if (oldMessage != null)
        return String.format("<del>message('%s', null, '%s')</del>", oldCode, oldMessage);
    }
    return "";
  }

  private static JSObject fieldToJSObject(String code, String message) {
    JSObject jsMessage = new JSObject();

    if (code != null) {
      jsMessage.put("code", code);
    }
    if (message != null) {
      jsMessage.put("message", message);
    }
    return jsMessage;
  }

  @Override
  public void addTo(JSArray array) {
    JSObject obj = new JSObject();
    obj.put("ruleName", ruleName);
    obj.put("oldValue", fieldToJSObject(oldCode, oldMessage));
    obj.put("newValue", fieldToJSObject(newCode, newMessage));
    obj.put("diffHTML", diffHTML());
    array.add(obj);
  }
}
