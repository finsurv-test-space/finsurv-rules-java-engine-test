package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.matching.IMatchingFunction;
import za.co.synthesis.rule.matching.MatchingContext;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * Created by jake on 6/16/16.
 */
public class notRounded implements IMatchingFunction {
  private final String fieldname;

  public notRounded(String fieldname) {
    this.fieldname = fieldname;
  }

  @Override
  public boolean execute(MatchingContext context) {
    Object valueLhs = context.getFieldLhs(fieldname);

    if (valueLhs != null) {
      BigDecimal value = Util.number(valueLhs);
      double dblValue = value.doubleValue();
      double floorValue = Math.floor(dblValue);

      if (dblValue-floorValue < 0.001)
        return false;
    }
    return true;
  }
}
