package za.co.synthesis.rule.support;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.predef.literal;
import za.co.synthesis.rule.predef.map;

import java.lang.reflect.Constructor;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 10:59 PM
 * Creates a new function (or logical combination of functions based on a given JavaScript definition)
 */
public class CalculationFunctionFactory {
    private static final String functionPackage = "za.co.synthesis.rule.predef.";

    public static ICalculationFunction createFunction(final String name) throws Exception {
        Class<?> cls = Class.forName(functionPackage + name);

        return (ICalculationFunction) cls.newInstance();
    }
}
