package za.co.synthesis.rule.support;

/**
 * Created by jake on 4/10/16.
 */
public class ClearValue extends DisplayRule {
  public ClearValue(IAssertionFunction assertion) {
    super(DisplayRuleType.ClearValue, null, null, null, assertion);
  }
}
