package za.co.synthesis.rule.classification.predef;

import za.co.synthesis.rule.classification.ClassificationContext;
import za.co.synthesis.rule.classification.IClassificationFunction;
import za.co.synthesis.rule.core.IClassificationContext;
import za.co.synthesis.rule.support.StringList;

/**
 * Created by jake on 6/7/16.
 */
public class hasValue implements IClassificationFunction {
  private StringList constants = null;

  public hasValue(String constant) {
    this.constants = new StringList();
    this.constants.add(constant);
  }

  public hasValue(StringList constant) {
    this.constants = constant;
  }

  public boolean execute(ClassificationContext context, Object value) {
    return (value != null && constants.contains(value.toString()));
  }
}
