 define([ 'require', './predef'], function (require,  predef) {
  var detailTrans, detailMoney, detailImportExport, displayFuncs;

  displayFuncs = {
    cleanName: stripRegex(/[()']/).then(replaceRegex(/\s+-+\s+|\s+-+|-+\s+/, "-")),
    cleanRegistrationNumber: stripRegex(/[()']/).then(replaceRegex(/\s+-+\s+|\s+-+|-+\s+/, "-"))
  };

  with (predef) { with (displayFuncs) {
    detailTrans = {
      ruleset: "Transaction \"Enrichment Rules",
      scope  : "transaction"
    }
  }
  }
  return detailTrans;
});

