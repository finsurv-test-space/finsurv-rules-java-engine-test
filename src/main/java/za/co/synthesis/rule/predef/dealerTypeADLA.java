package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 5:21 AM
 * Is this an ADLA transaction (extra custom data needed to be provided)
 */
public class dealerTypeADLA implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    Object cv = context.getCustomValue().get("DealerType");
    return (cv != null && "ADLA".equals(cv));
  }
}

/*
function dealerTypeADLA(context, value) {
  return (context.getCustomValue("DealerType") === "ADLA");
}
*/
