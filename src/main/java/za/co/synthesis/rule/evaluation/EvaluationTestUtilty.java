package za.co.synthesis.rule.evaluation;

import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.JSCustomValues;
import za.co.synthesis.rule.support.JSConstant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EvaluationTestUtilty {
  public static class TestResult {
    private final String testName;
    private final boolean passed;
    private final String note;
    private boolean decisionValid = true;
    private String decisionNote;
    private final List<IEvaluationDecision> decisions;

    private TestResult(String testName, boolean passed, String note, List<IEvaluationDecision> decisions) {
      this.testName = testName;
      this.passed = passed;
      this.note = note;
      this.decisions = decisions;
    }

    private TestResult(String testName, boolean passed, List<IEvaluationDecision> decisions) {
      this.testName = testName;
      this.passed = passed;
      this.note = "";
      this.decisions = decisions;
    }

    public void setDecisionValid(boolean decisionValid, String decisionNote) {
      this.decisionValid = decisionValid;
      this.decisionNote = decisionNote;
    }

    public String getDecisionNote() {
      return decisionNote;
    }

    public String getTestName() {
      return testName;
    }

    public boolean hasPassed() {
      return passed;
    }

    public String getNote() {
      return note;
    }

    public List<IEvaluationDecision> getDecisions() {
      return decisions;
    }

    public boolean isDecisionValid() {
      return decisionValid;
    }
  }

  public static class TestOutput {
    private int testCount = 0;
    private int errorCount = 0;
    private int invalidDecisionCount = 0;
    private List<TestResult> results = new ArrayList<TestResult>();

    public TestResult addSuccessResult(String testName, List<IEvaluationDecision> decisions) {
      TestResult result = new TestResult(testName, true, decisions);
      testCount++;
      results.add(result);
      return result;
    }

    public TestResult addSuccessResult(String testName, String note, List<IEvaluationDecision> decisions) {
      TestResult result = new TestResult(testName, true, note, decisions);
      testCount++;
      results.add(result);
      return result;
    }

    public TestResult addErrorResult(String testName, String note, List<IEvaluationDecision> decisions) {
      TestResult result = new TestResult(testName, false, note, decisions);
      testCount++;
      errorCount++;
      results.add(result);
      return result;
    }

    public TestResult addErrorResult(String testName, String note) {
      TestResult result = new TestResult(testName, false, note, new ArrayList<IEvaluationDecision>());
      testCount++;
      errorCount++;
      results.add(result);
      return result;
    }

    public int getTestCount() {
      return testCount;
    }

    public int getErrorCount() {
      return errorCount;
    }

    public int getInvalidDecisionCount() {
      return invalidDecisionCount;
    }

    public List<TestResult> getResults() {
      return results;
    }
  }

  private static class Comparison {
    public boolean failed;
    public String note;

    private Comparison(boolean failed, String note) {
      this.failed = failed;
      this.note = note;
    }
  }

  private static Comparison compareProperties(JSObject expected_obj, JSObject got_obj, String property) {
    boolean failed = false;
    String note = null;
    String exp_value = null;
    String got_value = null;

    if (expected_obj.containsKey(property))
      exp_value = expected_obj.get(property).toString();
    if (got_obj.containsKey(property))
      got_value = got_obj.get(property).toString();
    if (exp_value != null && got_value != null) {
      if (!exp_value.equals(got_value)) {
        failed = true;
        note = property + ": expecting " + exp_value + ", but instead got " + got_value;
      }
    }
    else {
      if (exp_value != null) {
        failed = true;
        note = property + ": expecting " + exp_value + ", but got nothing";
      }
      if (got_value != null) {
        failed = true;
        note = property + ": expecting nothing, but got " + got_value;
      }
    }
    return new Comparison(failed, note);
  }

  private static Comparison compareDecision(String test, JSObject res, JSObject testDecision) {
    boolean failed = false;
    String failedReason = "Failed Test: " + test;

    Comparison comp = compareProperties(testDecision, res, "decision");
    failed = failed || comp.failed;
    failedReason += (comp.failed ? "; " + comp.note : "");
    comp = compareProperties(testDecision, res, "reportingQualifier");
    failed = failed || comp.failed;
    failedReason += (comp.failed ? "; " + comp.note : "");
    comp = compareProperties(testDecision, res, "flow");
    failed = failed || comp.failed;
    failedReason += (comp.failed ? "; " + comp.note : "");
    comp = compareProperties(testDecision, res, "reportingSide");
    failed = failed || comp.failed;
    failedReason += (comp.failed ? "; " + comp.note : "");
    comp = compareProperties(testDecision, res, "nonResSide");
    failed = failed || comp.failed;
    failedReason += (comp.failed ? "; " + comp.note : "");
    comp = compareProperties(testDecision, res, "nonResAccountType");
    failed = failed || comp.failed;
    failedReason += (comp.failed ? "; " + comp.note : "");
    comp = compareProperties(testDecision, res, "nonResException");
    failed = failed || comp.failed;
    failedReason += (comp.failed ? "; " + comp.note : "");
    comp = compareProperties(testDecision, res, "resSide");
    failed = failed || comp.failed;
    failedReason += (comp.failed ? "; " + comp.note : "");
    comp = compareProperties(testDecision, res, "resAccountType");
    failed = failed || comp.failed;
    failedReason += (comp.failed ? "; " + comp.note : "");
    comp = compareProperties(testDecision, res, "resException");
    failed = failed || comp.failed;
    failedReason += (comp.failed ? "; " + comp.note : "");

    return new Comparison(failed, failed ? failedReason : "passed");
  }

  public static JSObject convertDecisionToJS(IEvaluationDecision decision) {
    JSObject result = new JSObject();
    JSIdentifier jsId = JSConstant.jsReportableDecision(decision.getDecision());
    if (jsId != null)
      result.put("decision", jsId);
    String qualifier = decision.getReportingQualifier();
    if (qualifier != null)
      result.put("reportingQualifier", qualifier);

    if (decision.getReportable().potentialReport()) {
      jsId = JSConstant.jsFlowType(decision.getFlow());
      if (jsId != null)
        result.put("flow", jsId);
      jsId = JSConstant.jsDrCr(decision.getReportingSide());
      if (jsId != null)
        result.put("reportingSide", jsId);
      jsId = JSConstant.jsDrCr(decision.getNonResSide());
      if (jsId != null)
        result.put("nonResSide", jsId);
      jsId = JSConstant.jsReportingAccountType(decision.getNonResAccountType());
      if (jsId != null)
        result.put("nonResAccountType", jsId);
      String exp = decision.getNonResException();
      if (exp != null)
        result.put("nonResException", exp);

      jsId = JSConstant.jsDrCr(decision.getResSide());
      if (jsId != null)
        result.put("resSide", jsId);
      jsId = JSConstant.jsReportingAccountType(decision.getResAccountType());
      if (jsId != null)
        result.put("resAccountType", jsId);
      exp = decision.getResException();
      if (exp != null)
        result.put("resException", exp);
    }
    return result;
  }

  public static boolean areDecisionsTheSame(List<IEvaluationDecision> decisions1, List<IEvaluationDecision> decisions2) {
    if (decisions1 != null && decisions2 != null) {
      if (decisions1.size() != decisions2.size()) {
        return false;
      }
      if (decisions1.size() == 1) {
        JSObject decision1 = convertDecisionToJS(decisions1.get(0));
        JSObject decision2 = convertDecisionToJS(decisions2.get(0));
        Comparison comparison = compareDecision("", decision1,decision2);
        if (comparison.failed) {
          return false;
        }
      }
      if (decisions1.size() == 2) {
        JSObject decision11 = convertDecisionToJS(decisions1.get(0));
        JSObject decision21 = convertDecisionToJS(decisions2.get(0));
        Comparison comparison = compareDecision("", decision11, decision21);
        if (comparison.failed) {
          return false;
        }
        JSObject decision12 = convertDecisionToJS(decisions1.get(1));
        JSObject decision22 = convertDecisionToJS(decisions2.get(1));
        comparison = compareDecision("", decision12, decision22);
        if (comparison.failed) {
          return false;
        }
      }
      return true;
    }
    else {
      if(decisions1 != decisions2) {
        return false;
      }
      return true;
    }
  }

  public static boolean areDecisionsTheSame(IEvaluationDecision decision1, IEvaluationDecision decision2) {
    if (decision1 != null && decision2 != null) {
      JSObject jsDecision1 = convertDecisionToJS(decision1);
      JSObject jsDecision2 = convertDecisionToJS(decision2);
      Comparison comparison = compareDecision("", jsDecision1, jsDecision2);
      if (comparison.failed) {
        return false;
      }
      return true;
    }
    else {
      if(decision1 != decision2) {
        return false;
      }
      return true;
    }
  }

  private static TestResult assertOnUs(Evaluator evaluator, String test,
                                       ResidenceStatus drResStatus, BankAccountType drAccType,
                                       ResidenceStatus crResStatus, BankAccountType crAccType, JSObject testDecision,
                                       TestOutput output) {
    List<IEvaluationDecision> res = evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, null,
            crResStatus, crAccType, null);

    if (res.size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, comp.note, res);
      }
    }
    else {
      return output.addErrorResult(test, "Expecting 1 result!", res);
    }

    res = evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, AccountHolderStatus.Entity,
            crResStatus, crAccType, AccountHolderStatus.Entity);

    if (res.size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, "special case Ent -> Ent. " + comp.note, res);
      }
    }
    else {
      return output.addErrorResult(test, "special case Ent -> Ent. Expecting 1 result!", res);
    }

    res = evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, AccountHolderStatus.Entity,
            crResStatus, crAccType, AccountHolderStatus.Individual);
    if (res.size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, "special case Ent -> Idv. " + comp.note, res);
      }
    }
    else {
      return output.addErrorResult(test, "special case Ent -> Idv. Expecting 1 result!", res);
    }

    res = evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, AccountHolderStatus.Individual,
            crResStatus, crAccType, AccountHolderStatus.Entity);
    if (res.size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, "special case Idv -> Ent. " + comp.note, res);
      }
    }
    else {
      return output.addErrorResult(test, "special case Idv -> Ent. Expecting 1 result!", res);
    }

    res = evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, AccountHolderStatus.Individual,
            crResStatus, crAccType, AccountHolderStatus.Individual);
    if (res.size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, "special case Idv -> Idv. " + comp.note, res);
      }
    }
    else {
      return output.addErrorResult(test, "special case Idv -> Idv. Expecting 1 result!", res);
    }

    return output.addSuccessResult(test, res);
  }

  private static TestResult assertOnUsIdv(Evaluator evaluator, String test,
                                                  ResidenceStatus drResStatus, BankAccountType drAccType,
                                                  ResidenceStatus crResStatus, BankAccountType crAccType,
                                                  JSObject testDecision,
                                                  TestOutput output) {
    List<IEvaluationDecision> res = evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, AccountHolderStatus.Individual,
            crResStatus, crAccType, null);

    if (res.size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, comp.note, res);
      }
      else {
        return output.addSuccessResult(test, res);
      }
    }
    else {
      return output.addErrorResult(test, "Expecting 1 result!", res);
    }
  }

  private static TestResult assertOnUsEnt(Evaluator evaluator, String test,
                                                  ResidenceStatus drResStatus, BankAccountType drAccType,
                                                  ResidenceStatus crResStatus, BankAccountType crAccType,
                                                  JSObject testDecision,
                                                  TestOutput output) {
    List<IEvaluationDecision> res = evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, AccountHolderStatus.Entity,
            crResStatus, crAccType, null);

    if (res.size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, comp.note, res);
      }
      else {
        return output.addSuccessResult(test, res);
      }
    }
    else {
      return output.addErrorResult(test, "Expecting 1 result!", res);
    }
  }

  private static TestResult assertUnknownCr1Result(Evaluator evaluator, String test,
                                                           ResidenceStatus drResStatus, BankAccountType drAccType,
                                                           String counterpartyInstituitionBIC, String currency,
                                                           AccountHolderStatus accountHolderStatus,
                                                           JSObject testDecision,
                                                           TestOutput output) {
    List<IEvaluationDecision> res = evaluator.evaluateUnknownCrSide(drResStatus, drAccType,
            null, counterpartyInstituitionBIC, currency, accountHolderStatus);

    if (res.size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, comp.note, res);
      }
      else {
        return output.addSuccessResult(test, res);
      }
    }
    else {
      return output.addErrorResult(test, "Expecting 1 result!", res);
    }
  }

  private static TestResult assertUnknownCr2Result(Evaluator evaluator, String test,
                                                           ResidenceStatus drResStatus, BankAccountType drAccType,
                                                           String counterpartyInstituitionBIC, String currency,
                                                           AccountHolderStatus accountHolderStatus,
                                                           JSObject testDecision1, JSObject testDecision2,
                                                           TestOutput output) {
    List<IEvaluationDecision> res = evaluator.evaluateUnknownCrSide(drResStatus, drAccType,
            null, counterpartyInstituitionBIC, currency, accountHolderStatus);

    if (res.size() == 2) {
      Comparison comp1 = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision1);
      Comparison comp2 = compareDecision(test, convertDecisionToJS(res.get(1)), testDecision2);

      if (comp1.failed || comp2.failed) {
        return output.addErrorResult(test, (comp1.note != null ? comp1.note : "") + " | " + comp2.note, res);
      }
      else {
        return output.addSuccessResult(test, res);
      }
    }
    else {
      return output.addErrorResult(test, "Expecting 2 results!", res);
    }
  }

  private static TestResult assertUnknownDr1Result(Evaluator evaluator, String test,
                                                           ResidenceStatus drResStatus, BankAccountType drAccType,
                                                           String counterpartyInstituitionBIC, String currency,
                                                           AccountHolderStatus accountHolderStatus,
                                                           JSObject testDecision,
                                                           TestOutput output) {
    List<IEvaluationDecision> res = evaluator.evaluateUnknownDrSide(drResStatus, drAccType,
            null, counterpartyInstituitionBIC, currency, accountHolderStatus, null);

    if (res.size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, comp.note, res);
      }
      else {
        return output.addSuccessResult(test, res);
      }
    }
    else {
      return output.addErrorResult(test, "Expecting 1 result!", res);
    }
  }

  private static TestResult assertOnUs2Results(Evaluator evaluator, String test,
                                                       ResidenceStatus drResStatus, BankAccountType drAccType,
                                                       ResidenceStatus crResStatus, BankAccountType crAccType,
                                                       JSObject testDecision1, JSObject testDecision2,
                                                       TestOutput output) {
    List<IEvaluationDecision> res = evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, null,
            crResStatus, crAccType, null);

    if (res.size() == 2) {
      Comparison comp1 = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision1);
      Comparison comp2 = compareDecision(test, convertDecisionToJS(res.get(1)), testDecision2);

      if (comp1.failed || comp2.failed) {
        return output.addErrorResult(test, "1: " + comp1.note + " & 2:" + comp2.note, res);
      }
      else {
        return output.addSuccessResult(test, res);
      }
    }
    else {
      return output.addErrorResult(test, "Expecting 2 results!", res);
    }
  }

  private static TestResult assert2Results(Evaluator evaluator, String test,
                                                   ResidenceStatus drResStatus, BankAccountType drAccType,
                                                   ResidenceStatus crResStatus, BankAccountType crAccType,
                                                   JSObject testDecision1, JSObject testDecision2,
                                                   TestOutput output) {
    List<IEvaluationDecision> res = evaluator.evaluate(drResStatus, drAccType, crResStatus, crAccType);

    if (res.size() == 2) {
      Comparison comp1 = compareDecision(test, convertDecisionToJS(res.get(0)), testDecision1);
      Comparison comp2 = compareDecision(test, convertDecisionToJS(res.get(1)), testDecision2);

      if (comp1.failed || comp2.failed) {
        return output.addErrorResult(test, "1: " + comp1.note + " & 2:" + comp2.note, res);
      }
      else {
        return output.addSuccessResult(test, res);
      }
    }
    else {
      return output.addErrorResult(test, "Expecting 2 results!", res);
    }
  }

  private static TestResult assertAssumption(Evaluator evaluator, String test,
                                                     String drBankBIC, ResidenceStatus drResStatus, BankAccountType drAccType,
                                                     String drCurrency, String drField72,
                                                     String crBankBIC, ResidenceStatus crResStatus, BankAccountType crAccType, String crCurrency,
                                                     AccountHolderStatus accountHolderStatus,
                                                     JSObject testDecision,
                                                     TestOutput output) {
    Map<String, Object> drAdditionalParams = null;
    Map<String, Object> crAdditionalParams = null;
    if (drResStatus != null && accountHolderStatus != null) {
      drAdditionalParams = new HashMap<String, Object>();
      drAdditionalParams.put(Evaluator.ACCOUNT_HOLDER_STATUS, accountHolderStatus);
    }
    if (crResStatus != null && accountHolderStatus != null) {
      crAdditionalParams = new HashMap<String, Object>();
      crAdditionalParams.put(Evaluator.ACCOUNT_HOLDER_STATUS, accountHolderStatus);
    }
    IEvaluationScenarioDecision res = evaluator.consolidatedEvaluation(drBankBIC, drResStatus, drAccType,
            drCurrency, drField72, drAdditionalParams ,
            crBankBIC, crResStatus, crAccType, crCurrency, crAdditionalParams, true, true);

    if (res.getDecisions().size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.getDecisions().get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, comp.note, res.getDecisions());
      }
      else {
        return output.addSuccessResult(test, res.getInformation(), res.getDecisions());
      }
    }
    else {
      return output.addErrorResult(test, "Expecting 1 result!", res.getDecisions());
    }
  }

  private static TestResult assertAssumption(Evaluator evaluator, String test,
                                             String drBankBIC, ResidenceStatus drResStatus, BankAccountType drAccType,
                                             String drCurrency, String drField72,
                                             Map<String, Object> drAdditionalParams,
                                             String crBankBIC, ResidenceStatus crResStatus, BankAccountType crAccType, String crCurrency,
                                             Map<String, Object> crAdditionalParams,
                                             JSObject testDecision,
                                             TestOutput output) {
    IEvaluationScenarioDecision res = evaluator.consolidatedEvaluation(drBankBIC, drResStatus, drAccType,
            drCurrency, drField72, drAdditionalParams ,
            crBankBIC, crResStatus, crAccType, crCurrency, crAdditionalParams, true, true);

    if (res.getDecisions().size() == 1) {
      Comparison comp = compareDecision(test, convertDecisionToJS(res.getDecisions().get(0)), testDecision);

      if (comp.failed) {
        return output.addErrorResult(test, comp.note, res.getDecisions());
      }
      else {
        return output.addSuccessResult(test, res.getInformation(), res.getDecisions());
      }
    }
    else {
      return output.addErrorResult(test, "Expecting 1 result!", res.getDecisions());
    }
  }

  private static JSObject convertDecisionToTransactionJS(IEvaluationDecision decision) {
    JSObject result = null;
    if (decision.getDecision() == ReportableDecision.ReportToRegulator) {
      result = new JSObject();
      result.put("ReportingQualifier", decision.getReportingQualifier());

      if (decision.getFlow() == FlowType.Inflow)
        result.put("Flow", "IN");
      if (decision.getFlow() == FlowType.Outflow)
        result.put("Flow", "OUT");

      JSObject nonRes = new JSObject();
      result.put("NonResident", nonRes);
      if (decision.getNonResException() != null) {
        JSObject nonResException = new JSObject();
        nonRes.put("Exception", nonResException);
        nonResException.put("ExceptionName", decision.getNonResException());
      }
      else {
        JSObject nonResIndividual = new JSObject();
        nonRes.put("Individual", nonResIndividual);
        nonResIndividual.put("AccountIdentifier", decision.getNonResAccountType().getName());
      }

      JSObject res = new JSObject();
      result.put("Resident", res);
      if (decision.getResException() != null) {
        JSObject resException = new JSObject();
        res.put("Exception", resException);
        resException.put("ExceptionName", decision.getResException());
      }
      else {
        JSObject resIndividual = new JSObject();
        res.put("Individual", resIndividual);
        resIndividual.put("AccountIdentifier", decision.getResAccountType().getName());
      }

      if (decision.getCategory() != null && decision.getCategory().size() > 0) {
        JSArray moneyList = new JSArray();
        result.put("MonetaryAmount", moneyList);
        for (String cat : decision.getCategory()) {
          JSObject money = new JSObject();
          if (cat.contains("/")) {
            int pos = cat.indexOf("/");
            money.put("CategoryCode", cat.substring(0, pos));
            money.put("CategorySubCode", cat.substring(pos+1));
          }
          else
            money.put("CategoryCode", cat);

          if (decision.getLocationCountry() != null)
            money.put("LocationCountry", decision.getLocationCountry());

          moneyList.add(money);
        }
      }
    }
    return result;
  }

  private static void checkCorrectnessOfDecisions(Validator validator, TestResult testResult) {
    if (validator != null) {
      boolean isValid = true;
      for (IEvaluationDecision decision : testResult.getDecisions()) {
        //if (decision.getReportable() != ReportableType.ZZ1Reportable || decision.isZZ1Reportable()) {
          JSObject jsObj = convertDecisionToTransactionJS(decision);
          if (jsObj != null) {
            isValid = isValid && validateEvalDecision(validator, jsObj, testResult);
          }
        //}
      }
    }
  }

  private static boolean validateEvalDecision(Validator validator, JSObject jsObj, TestResult result) {
    JSObject customData = new JSObject();
    customData.put("DealerType", "AD");
    JSCustomValues customValues = new JSCustomValues(customData);
    ValidationStats stats = new ValidationStats();

    List<ResultEntry> validationResults = new ArrayList<ResultEntry>();
    validationResults.addAll(validator.validateRule("nriaid1:1", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nriaid4:1", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nriaid7:1", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nriaid9:1", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nriaid10:1", jsObj, customValues, stats));

    validationResults.addAll(validator.validateRule("nrexn1", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn2", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn3", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn4", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn5", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn6", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn7", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn9", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn10", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn11", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn12", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn13", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn14", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn15", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn16", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn17", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn18", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn19", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn20", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn21", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn22", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn23", jsObj, customValues, stats));
    //validationResults.addAll(validator.validateRule("nrexn24", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("nrexn25", jsObj, customValues, stats));

    validationResults.addAll(validator.validateRule("accid1:1", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("accid2:1", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("accid5:1", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("accid6:1", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("accid7:1", jsObj, customValues, stats));

    validationResults.addAll(validator.validateRule("ren1", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren2", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren3", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren4", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren5", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren6", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren7", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren9", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren10", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren11", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren12", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren13", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren14", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren15", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren16", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren17", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren18", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren19", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren20", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren21", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren22", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren23", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren24", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren25", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren26", jsObj, customValues, stats));
    validationResults.addAll(validator.validateRule("ren27", jsObj, customValues, stats));

    if (validationResults.size() > 0) {
      String decisionNote = "";
      boolean inError = false;
      for (ResultEntry entry : validationResults) {
        if (entry.getType() == ResultType.Error) {
          inError = true;
          decisionNote += "[" + entry.getName() + "] " + entry.getCode() + ": " + entry.getMessage() + "\n";
        }
      }
      if (inError) {
        JSWriter writer = new JSWriter();
        writer.setNewline("\n");
        writer.setIndent("  ");
        jsObj.compose(writer);
        decisionNote += writer.toString();
        result.setDecisionValid(false, decisionNote);
        return false;
      }
    }
    return true;
  }

  private static String paramAsString(Object param) {
    if (param != null) {
      return param.toString();
    }
    return null;
  }

  public static void doCases(Evaluator evaluator, Validator validator, JSArray cases, String filterRule, TestOutput output) {
    for (Object objCase : cases) {
      if (objCase instanceof JSFunctionCall) {
        JSFunctionCall jsFunc = (JSFunctionCall)objCase;

        String rulename = paramAsString(jsFunc.getParameters().get(0));
        if (filterRule == null || rulename.equals(filterRule)) {
          if (jsFunc.getName().equals("assertOnUs") && jsFunc.getParameters().size() == 6) {
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(1));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(2));
            ResidenceStatus crResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(3));
            BankAccountType crAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(4));
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(5);

            checkCorrectnessOfDecisions(validator,
                    assertOnUs(evaluator, rulename, drResStatus, drAccType, crResStatus, crAccType, jsObj, output));
          }
          else
          if (jsFunc.getName().equals("assertOnUsIdv") && jsFunc.getParameters().size() == 6) {
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(1));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(2));
            ResidenceStatus crResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(3));
            BankAccountType crAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(4));
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(5);

            checkCorrectnessOfDecisions(validator,
                    assertOnUsIdv(evaluator, rulename, drResStatus, drAccType, crResStatus, crAccType, jsObj, output));
          }
          else
          if (jsFunc.getName().equals("assertOnUsEnt") && jsFunc.getParameters().size() == 6) {
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(1));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(2));
            ResidenceStatus crResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(3));
            BankAccountType crAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(4));
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(5);

            checkCorrectnessOfDecisions(validator,
                    assertOnUsEnt(evaluator, rulename, drResStatus, drAccType, crResStatus, crAccType, jsObj, output));
          }
          else
          if (jsFunc.getName().equals("assertOnUs2Results") && jsFunc.getParameters().size() == 7) {
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(1));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(2));
            ResidenceStatus crResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(3));
            BankAccountType crAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(4));
            JSObject jsObj1 = (JSObject)jsFunc.getParameters().get(5);
            JSObject jsObj2 = (JSObject)jsFunc.getParameters().get(6);
            checkCorrectnessOfDecisions(validator,
                    assertOnUs2Results(evaluator, rulename, drResStatus, drAccType, crResStatus, crAccType, jsObj1, jsObj2, output));
          }
          else
          if (jsFunc.getName().equals("assertUnknownCrSide") && jsFunc.getParameters().size() == 7) {
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(1));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(2));
            String counterpartyInstituitionBIC = paramAsString(jsFunc.getParameters().get(3));
            String currency = paramAsString(jsFunc.getParameters().get(4));
            AccountHolderStatus accountHolderStatus = JSConstant.accountHolderStatus(jsFunc.getParameters().get(5));
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(6);

            checkCorrectnessOfDecisions(validator,
                    assertUnknownCr1Result(evaluator, rulename, drResStatus, drAccType,
                            counterpartyInstituitionBIC, currency, accountHolderStatus, jsObj, output));
          }
          else
          if (jsFunc.getName().equals("assert2UnknownCrSide") && jsFunc.getParameters().size() == 8) {
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(1));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(2));
            String counterpartyInstituitionBIC = paramAsString(jsFunc.getParameters().get(3));
            String currency = paramAsString(jsFunc.getParameters().get(4));
            AccountHolderStatus accountHolderStatus = JSConstant.accountHolderStatus(jsFunc.getParameters().get(5));
            JSObject jsObj1 = (JSObject)jsFunc.getParameters().get(6);
            JSObject jsObj2 = (JSObject)jsFunc.getParameters().get(7);

            checkCorrectnessOfDecisions(validator,
                    assertUnknownCr2Result(evaluator, rulename, drResStatus, drAccType,
                            counterpartyInstituitionBIC, currency, accountHolderStatus, jsObj1, jsObj2, output));
          }
          else
          if (jsFunc.getName().equals("assertUnknownDrSide") && jsFunc.getParameters().size() == 7) {
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(1));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(2));
            String counterpartyInstituitionBIC = paramAsString(jsFunc.getParameters().get(3));
            String currency = paramAsString(jsFunc.getParameters().get(4));
            AccountHolderStatus accountHolderStatus = JSConstant.accountHolderStatus(jsFunc.getParameters().get(5));
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(6);

            checkCorrectnessOfDecisions(validator,
                    assertUnknownDr1Result(evaluator, rulename, drResStatus, drAccType,
                            counterpartyInstituitionBIC, currency, accountHolderStatus, jsObj, output));
          }
          else
          if (jsFunc.getName().equals("assert2Results") && jsFunc.getParameters().size() == 7) {
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(1));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(2));
            ResidenceStatus crResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(3));
            BankAccountType crAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(4));
            JSObject jsObj1 = (JSObject)jsFunc.getParameters().get(5);
            JSObject jsObj2 = (JSObject)jsFunc.getParameters().get(6);
            checkCorrectnessOfDecisions(validator,
                    assert2Results(evaluator, rulename, drResStatus, drAccType, crResStatus, crAccType, jsObj1, jsObj2, output));
          }
          else
          if ((jsFunc.getName().equals("assertAssumption") || jsFunc.getName().equals("assertAssumptionIdv") || jsFunc.getName().equals("assertAssumptionEnt")) &&
                  jsFunc.getParameters().size() == 11) {
            String drBankBIC = paramAsString(jsFunc.getParameters().get(1));
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(2));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(3));
            String drCurrency = paramAsString(jsFunc.getParameters().get(4));
            String drField72 = paramAsString(jsFunc.getParameters().get(5));

            String crBankBIC = paramAsString(jsFunc.getParameters().get(6));
            ResidenceStatus crResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(7));
            BankAccountType crAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(8));
            String crCurrency = paramAsString(jsFunc.getParameters().get(9));

            JSObject jsRequiredDecision = null;
            Object obj = jsFunc.getParameters().get(10);
            if (obj instanceof JSObject) {
              jsRequiredDecision = (JSObject) obj;
            }

            AccountHolderStatus accountHolderStatus = null;
            if (jsFunc.getName().equals("assertAssumptionIdv")) {
              accountHolderStatus = AccountHolderStatus.Individual;
            } else if (jsFunc.getName().equals("assertAssumptionEnt")) {
              accountHolderStatus = AccountHolderStatus.Entity;
            }

            checkCorrectnessOfDecisions(validator,
                    assertAssumption(evaluator, rulename, drBankBIC, drResStatus, drAccType, drCurrency, drField72,
                            crBankBIC, crResStatus, crAccType, crCurrency, accountHolderStatus, jsRequiredDecision, output));
          }
          else
          if (jsFunc.getName().equals("assertAssumptionAdditionalParams") &&
              jsFunc.getParameters().size() == 13) {
            String drBankBIC = paramAsString(jsFunc.getParameters().get(1));
            ResidenceStatus drResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(2));
            BankAccountType drAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(3));
            String drCurrency = paramAsString(jsFunc.getParameters().get(4));
            String drField72 = paramAsString(jsFunc.getParameters().get(5));
            JSObject drAdditionalParams = (JSObject) jsFunc.getParameters().get(6);

            String crBankBIC = paramAsString(jsFunc.getParameters().get(7));
            ResidenceStatus crResStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(8));
            BankAccountType crAccType = JSConstant.bankAccountType(jsFunc.getParameters().get(9));
            String crCurrency = paramAsString(jsFunc.getParameters().get(10));
            JSObject crAdditionalParams = (JSObject) jsFunc.getParameters().get(11);

            JSObject jsRequiredDecision = null;
            Object obj = jsFunc.getParameters().get(12);
            if (obj instanceof JSObject) {
              jsRequiredDecision = (JSObject) obj;
            }

            checkCorrectnessOfDecisions(validator,
                    assertAssumption(evaluator, rulename, drBankBIC, drResStatus, drAccType, drCurrency, drField72, drAdditionalParams,
                            crBankBIC, crResStatus, crAccType, crCurrency, crAdditionalParams, jsRequiredDecision, output));
          }
          else {
            output.addErrorResult(rulename, "**** NO TEST SUPPORT FOR: " + jsFunc.getName() + " *****");
          }
        }
      }
    }
  }
}
