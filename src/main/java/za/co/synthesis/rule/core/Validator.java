package za.co.synthesis.rule.core;

import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.rule.core.impl.DefaultFinsurvContextCache;
import za.co.synthesis.rule.core.impl.JSFinsurvContext;
import za.co.synthesis.rule.core.impl.JSFinsurvContextCache;
import za.co.synthesis.rule.core.impl.PojoFinsurvContext;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.support.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 3:20 PM
 * This is the class that actually brings together the finsurv data together with the validations to actually
 * validate the data and return the errors and warnings.
 */
public class Validator {
  private final Map<String, String> mappings;
  private final ILookups lookups;
  private final ISupporting supporting;
  private final IValidationRules validations;
  private final CustomValidateRegistry customValidateRegistry;
  private final CentralCustomValidationCache centralValidateCache;

  public Validator(Map<String, String> mappings,
                   ILookups lookups, ISupporting supporting, IValidationRules validations,
                   CustomValidateRegistry customValidateRegistry) {
    this.mappings = mappings;
    this.lookups = lookups;
    this.supporting = supporting;
    this.validations = validations;
    this.customValidateRegistry = customValidateRegistry;
    this.centralValidateCache = new CentralCustomValidationCache(customValidateRegistry);
  }

  public Validator(Map<String, String> mappings,
                   ILookups lookups, ISupporting supporting, IValidationRules validations,
                   CentralCustomValidationCache centralCache, CustomValidateRegistry customValidateRegistry) {
    this.mappings = mappings;
    this.lookups = lookups;
    this.supporting = supporting;
    this.validations = validations;
    this.centralValidateCache = centralCache;
    this.customValidateRegistry = customValidateRegistry;
  }

  public CentralCustomValidationCache getCentralValidateCache() {
    return centralValidateCache;
  }

  private void runValidation(FinsurvContext context, List<Validation> rules, ValidationStats stats) {
    assert(context != null);
    CustomValidateCache customValidateCache = new CustomValidateCache(centralValidateCache);
    customValidateCache.setContext(context);
    IFinsurvContextCache validationCache = context.getValidationCache();
    if (validationCache != null) {
      List<CentralCustomValidationCache.ValidationCacheEntry> unmarshalledCache = validationCache.unmarshalCache();
      if (unmarshalledCache != null)
      for (CentralCustomValidationCache.ValidationCacheEntry entry : unmarshalledCache) {
        customValidateCache.cacheResult(entry.getValidateName(), entry.getValidateInputs(),
                entry.getResult(), entry.getResponseCache(), entry.getExpiryMillis());
      }
    }
    // Set the validation stats after installing any existing stats so that stats reflect correctly
    customValidateCache.setStats(stats);

    int rulesRun = 0;
    for (Validation rule : rules) {
      if (rule.run(context))
        rulesRun++;
    }
    stats.setFilteredRules(rules.size());
    stats.setRunRules(rulesRun);
  }

  private void updateResultStats(List<ResultEntry> resultList, ValidationStats stats) {
    int errorCount = 0;
    int warningCount = 0;
    int deprecatedCount = 0;

    for (ResultEntry entry : resultList) {
      switch (entry.getType()) {
        case Error: errorCount++; break;
        case Warning: warningCount++; break;
        case Deprecated: deprecatedCount++; break;
      }
    }
    stats.setErrorCount(errorCount);
    stats.setWarningCount(warningCount);
    stats.setDeprecatedCount(deprecatedCount);
  }

  public void registerCustomValidate(String name, ICustomValidate callback) {
    customValidateRegistry.register(name, callback);
  }

  public void clearCustomValidate() {
    customValidateRegistry.clear();
  }

  public List<ResultEntry> validateRule(String rulename, Object dto, ICustomValue customValues, ValidationStats stats) throws Exception {
    return validateRule(rulename, dto, new DefaultFinsurvContextCache(), customValues, stats);
  }

  public List<ResultEntry> validateRule(String rulename, Object dto, IFinsurvContextCache contextCache, ICustomValue customValues, ValidationStats stats) throws Exception {
    PojoFinsurvContext context = new PojoFinsurvContext(dto, contextCache);
    CustomValidateCache customValidateCache = new CustomValidateCache(centralValidateCache);
    customValidateCache.setStats(stats);
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    List<Validation> rules = validations.validationRules(mappings,
            fc.getFlow(), fc.getSection(), fc.getCategories(), fc.getMainCategories(), rulename);
    runValidation(fc, rules, stats);
    updateResultStats(fc.getValidationResultList(), stats);
    customValidateCache.updateContextCache(); // Update the Finsurv context cache with the most recent cached validations
    return fc.getValidationResultList();
  }

  public List<ResultEntry> validate(Object dto, ICustomValue customValues, ValidationStats stats) throws Exception {
    return validate(dto, new DefaultFinsurvContextCache(), customValues, stats);
  }

  /**
   * Validates the given Finsurv/BoP context and custom values against the rules and lookups loaded into the Validator instance.
   *
   * Where reportData, a JSObject instance containing the parsed JSON data from the "Report" or "transaction" data returned from the Finsurv Bop form,
   * and metaData, a JSObject instance containing the parsed JSON from the "Meta" or "customData" section of the JSON returned from the Finsurv Bop Form.
   * The example below makes use of such instances and helper classes to perform a validation call.
   * Example usage:
   *     validate(new MapFinsurvContext(reportData), new MapCustomValues(metaData) ,stats);
   *
   * @param dto
   * @param customValues
   * @param stats
   * @return
   * @throws Exception
   */
  public List<ResultEntry> validate(Object dto, IFinsurvContextCache contextCache, ICustomValue customValues, ValidationStats stats) throws Exception {
    PojoFinsurvContext context = new PojoFinsurvContext(dto, contextCache);
    CustomValidateCache customValidateCache = new CustomValidateCache(centralValidateCache);
    customValidateCache.setStats(stats);
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    List<Validation> rules = validations.validationRules(mappings,
            fc.getFlow(), fc.getSection(), fc.getCategories(), fc.getMainCategories(), null);
    runValidation(fc, rules, stats);
    updateResultStats(fc.getValidationResultList(), stats);
    customValidateCache.updateContextCache(); // Update the Finsurv context cache with the most recent cached validations
    return fc.getValidationResultList();
  }

  /**
   * Simplified BOP validation function, which takes in the JSON BOP report object.
   * Expected structure:
   *   {
   *     "Report":{...},
   *     "Meta":{...}
   *   }
   * OR
   *   {
   *     "transaction":{...},
   *     "customData":{...}
   *   }
   * @param jsonData containing the Stringified JSON data to be validated
   * @return ValidationResultsAndStats containing a List of all validation warnings and errors, as well as a ValidationStats object.
   * @throws Exception if the data provided is not parsable
   */
  public ValidationResultsAndStats validateJson(String jsonData) throws Exception {
    return validateJson(jsonData, null);
  }

    /**
     * Simplified BOP validation function, which takes in the JSON BOP report object.
     * Expected structure:
     *   {
     *     "Report":{...},
     *     "Meta":{...}
     *   }
     * OR
     *   {
     *     "transaction":{...},
     *     "customData":{...}
     *   }
     * @param jsonData containing the Stringified JSON data to be validated
     * @param stats a ValidationStats instance
     * @return ValidationResultsAndStats containing a List of all validation warnings and errors, as well as a ValidationStats object.
     * @throws Exception if the data provided is not parsable
     */
  public ValidationResultsAndStats validateJson(String jsonData, ValidationStats stats) throws Exception {
    stats = stats==null?new ValidationStats():stats;
    JSStructureParser structureParser = new JSStructureParser(jsonData);
    JSObject jso = (JSObject)structureParser.parse();
    List<ResultEntry> validationResults = new ArrayList<ResultEntry>();
    try {
      //fetch the "Report" and "Meta" data sections from the JSON Object
      JSObject reportData = null;
      JSObject metaData = null;
      if (jso.containsKey("Report") && jso.get("Report") instanceof JSObject) {
        reportData = (JSObject) jso.get("Report");
      }
      if (jso.containsKey("Meta") && jso.get("Meta") instanceof JSObject) {
        metaData = (JSObject) jso.get("Meta");
      }
      //the FINSURV form sometimes uses "transaction" instead of "Report"...
      if (reportData == null && jso.containsKey("transaction") && jso.get("transaction") instanceof JSObject) {
        reportData = (JSObject) jso.get("transaction");
      }
      //the FINSURV form sometimes uses "customData" instead of "Meta"...
      if (metaData == null && jso.containsKey("customData") && jso.get("customData") instanceof JSObject) {
        metaData = (JSObject) jso.get("customData");
      }

      //do a report validation...
      validationResults = validate(new MapFinsurvContext(reportData, new MapFinsurvContextCache(metaData)), new MapCustomValues(metaData) ,stats);

    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("Unable to validate transaction - "+e.getMessage());
    }
    return new ValidationResultsAndStats(stats, validationResults);

  }

  public List<ResultEntry> validateRule(String rulename, JSObject jsObj, ICustomValue customValues, ValidationStats stats) {
    return validateRule(rulename, jsObj, (JSObject)null, customValues, stats);
  }

  public List<ResultEntry> validateRule(String rulename, JSObject jsObj, JSObject jsMeta, ICustomValue customValues, ValidationStats stats) {
    JSFinsurvContext context;
    if (jsMeta != null)
      context = new JSFinsurvContext(jsObj, new JSFinsurvContextCache(jsMeta));
    else
      context = new JSFinsurvContext(jsObj);
    CustomValidateCache customValidateCache = new CustomValidateCache(centralValidateCache);
    customValidateCache.setStats(stats);
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    List<Validation> rules = validations.validationRules(mappings,
            fc.getFlow(), fc.getSection(), fc.getCategories(), fc.getMainCategories(), rulename);
    runValidation(fc, rules, stats);
    updateResultStats(fc.getValidationResultList(), stats);
    customValidateCache.updateContextCache(); // Update the jsMeta object with the most recent cached validations
    return fc.getValidationResultList();
  }

  public List<ResultEntry> validate(JSObject jsObj, ICustomValue customValues, ValidationStats stats) {
    return validate(jsObj, (JSObject)null, customValues, stats);
  }

  public List<ResultEntry> validate(JSObject jsObj, JSObject jsMeta, ICustomValue customValues, ValidationStats stats) {
    JSFinsurvContext context;
    if (jsMeta != null)
      context = new JSFinsurvContext(jsObj, new JSFinsurvContextCache(jsMeta));
    else
      context = new JSFinsurvContext(jsObj);
    CustomValidateCache customValidateCache = new CustomValidateCache(centralValidateCache);
    customValidateCache.setStats(stats);
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    List<Validation> rules = validations.validationRules(mappings,
            fc.getFlow(), fc.getSection(), fc.getCategories(), fc.getMainCategories(), null);
    runValidation(fc, rules, stats);
    updateResultStats(fc.getValidationResultList(), stats);
    customValidateCache.updateContextCache(); // Update the jsMeta object with the most recent cached validations
    return fc.getValidationResultList();
  }

  public List<ResultEntry> validateRule(String rulename, IFinsurvContext context, ICustomValue customValues, ValidationStats stats) {
    CustomValidateCache customValidateCache = new CustomValidateCache(centralValidateCache);
    customValidateCache.setStats(stats);
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    List<Validation> rules = validations.validationRules(mappings,
            fc.getFlow(), fc.getSection(), fc.getCategories(), fc.getMainCategories(), rulename);
    runValidation(fc, rules, stats);
    updateResultStats(fc.getValidationResultList(), stats);
    customValidateCache.updateContextCache(); // Update the Finsurv context cache with the most recent cached validations
    return fc.getValidationResultList();
  }

  public List<ResultEntry> validate(IFinsurvContext context, ICustomValue customValues, ValidationStats stats) {
    CustomValidateCache customValidateCache = new CustomValidateCache(centralValidateCache);
    customValidateCache.setStats(stats);
    FinsurvContext fc = new FinsurvContext(context, mappings, supporting, lookups, customValidateRegistry, customValidateCache, customValues);

    List<Validation> rules = validations.validationRules(mappings,
            fc.getFlow(), fc.getSection(), fc.getCategories(), fc.getMainCategories(), null);
    runValidation(fc, rules, stats);
    updateResultStats(fc.getValidationResultList(), stats);
    customValidateCache.updateContextCache(); // Update the Finsurv context cache with the most recent cached validations
    return fc.getValidationResultList();
  }
}
