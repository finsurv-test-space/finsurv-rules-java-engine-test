package za.co.synthesis.rule.predef;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

public class hasMoneyFieldMetaLookupValue implements IAssertionFunction {
    private String lookupName;
    private String lookupKey;
    private String field;

    public hasMoneyFieldMetaLookupValue(String lookupName, String lookupKey, String field) {
        this.lookupName = lookupName;
        this.lookupKey = lookupKey;
        this.field = field;
    }

    public boolean execute(FinsurvContext context, Object value) {
        int moneyIndex = context.getCurrentMoneyInstance();
        if (moneyIndex == -1) {
            moneyIndex = 0;
        }
        if (value != null && value instanceof String) {
            Object fieldValue = context.getMoneyField(moneyIndex, field);
            JSObject lookupValue = (JSObject) context.getCustomValue().get(lookupName);
            return (lookupValue != null && lookupValue.size() > 0) && lookupValue.get(lookupKey).equals(fieldValue);
        }
        return false;
    }
}
