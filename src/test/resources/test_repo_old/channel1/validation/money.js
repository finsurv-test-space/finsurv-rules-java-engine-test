define(function () {
  return function (predef) {
    var money;

    with (predef) {
      money = {
        ruleset: "Money Rules for Flow Form",
        scope: "money",
        validations: [
          {
            field: "",
            rules: [
              message("mrv3", null, "ZAR equivalent and Foreign Currency Amount cannot be the same"),
              message("mrv4", null, "The reported Rand value must be within a certain variance to the Foreign value")
            ]
          },
          {
            field: "SequenceNumber",
            rules: [
            ignore("mseq1")
            ]
          },
          {
            field: "LocationCountry",
            rules: [
            ignore("mlc1"),
            ignore("mlc2"),
            ignore("mlc3"),
            ignore("mlc4")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            rules: [
            ignore("masan1"),
            failure("inv_masan1", "I33", "If the SARB Auth Date is captured then SARB Auth Number must be provided",
            isEmpty.and(hasMoneyField("{{Regulator}}Auth.SARBAuthDate"))).onSection("ABG"),
            failure("inv_masan2", "I??", "The SARB Auth Number must be provided for category 105 and 106",
            isEmpty).onOutflow().onSection("ABG").onCategory(["105", "106"])
            ]
          }
        ]
      }
    };

    return money;
  }
});
