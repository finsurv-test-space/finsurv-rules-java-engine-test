package za.co.synthesis.rule.core;

import za.co.synthesis.rule.evaluation.Context;

import java.util.List;

/**
 * User: jake
 * Date: 4/7/15
 * Time: 11:05 AM
 * An interface to an evaluation getDecision
 */
public interface IEvaluationDecision {
  boolean equals(IEvaluationDecision other);


  /**
   * In a scenario where an evaluation is done with an Unknown debit account type then the returned evaluation getDecision
   * will show the possible debit account types that would match this getDecision. In a case where a specific credit
   * account type is passed in then this list contains the specified value.
   * @return a list of possible debit account types that match this getDecision
   */
  List<BankAccountType> getPossibleDrAccountTypes();

  /**
   * In a scenario where an evaluation is done with an Unknown credit account type then the returned evaluation getDecision
   * will show the possible credit account types that would match this getDecision. In a case where a specific credit
   * account type is passed in then this list contains the specified value.
   * @return a list of possible credit account types that match this getDecision
   */
  List<BankAccountType> getPossibleCrAccountTypes();

  /**
   * The reportability of the getDecision i.e. report to regulator or not.
   * @return
   */
  ReportableDecision getDecision();

  /**
   * The reporting qualifier to use on the BOP report
   * @return
   */
  String getReportingQualifier();

  /**
   * This returns if the transaction is reportable or not and if reportable then it determines the nature of the
   * ReportingQualifier under which the transaction should be reported.
   * @return @see za.co.synthesis.rule.core.ReportableType for what the various values mean
   */
  ReportableType getReportable();

  /**
   * If reportable this returns the directional flow to use for the transaction
   * @return null if not reportable or the inflow.outflow direction for reporting
   */
  FlowType getFlow();

  /**
   * This defines which side of the transaction is responsible for reporting the transaction:
   * + Dr: The Debited party is responsible for providing the needed information
   * + Cr: The Credited party is responsible for providing the needed information
   * @return null if no particular party is responsible for supplying the needed information and the
   * Banking instituition will need to provide the information (if this turns out to be a reportable transaction)
   */
  DrCr getReportingSide();

  /**
   * This defines which side of the transaction must use to populate the Non Resident information:
   * + Dr: Map non resident client information from the debit side of the transaction
   * + Cr: Map non resident client information from the credit side of the transaction
   * @return null if no mapping is required (like in the case where an exception will be used) or the side of the
   * transaction to map the non resident client information from
   */
  DrCr getNonResSide();

  /**
   * This is the account type to use for the non resident (using SARB reporting account types) based on the given
   * bank account type and associated residence status
   * @return null if no account type is required (like in the case where an exception will be used) or the SARB
   * account type to use for the non resident
   */
  ReportingAccountType getNonResAccountType();

  /**
   * This is set to a valid SARB exception if the non resident exception should be used. If the exception is set then
   * both the side and account type will not be specified
   * @return The name of the exception or null if no exception should be used
   */
  String getNonResException();

  /**
   * This defines which side of the transaction must use to populate the Resident information:
   * + Dr: Map resident client information from the debit side of the transaction
   * + Cr: Map resident client information from the credit side of the transaction
   * @return null if no mapping is required (like in the case where an exception will be used) or the side of the
   * transaction to map the resident client information from
   */
  DrCr getResSide();

  /**
   * This is the account type to use for the resident (using SARB reporting account types) based on the given
   * bank account type and associated residence status
   * @return null if no account type is required (like in the case where an exception will be used) or the SARB
   * account type to use for the non resident
   */
  ReportingAccountType getResAccountType();

  /**
   * This is set to a valid SARB exception if the resident exception should be used. If the exception is set then
   * both the side and account type will not be specified
   * @return The name of the exception or null if no exception should be used
   */
  String getResException();

  /**
   * This is set to a valid adhoc subject that must be used for the monetary amount on the transaction
   * @return The name of the adhoc subject that must be used or null if no specific adhoc subject need be used
   */
  String getSubject();

  /**
   * This is set to a location country that must be used for the monetary amount on the transaction
   * @return The name of the location country that must be used or null if any location country may be used
   */
  String getLocationCountry();

  /**
   * This is the reference to the section in the SARB manual that contains this ruling
   * @return The reference to the section in the SARB manual
   */
  String getManualSection();

  /**
   * This is the SWIFT instruction to be used by the debiting instituition on field 72 of the SWIFT message
   * @return The field 72 SWIFT instruction
   */
  String getDrSideSwift();

  /**
   * The account holder status filter is used to see if s desicion applies to a entity or an individual.
   * @return the account holder status (Entity or Individual)
   */
  AccountHolderStatus getAccStatusFilter();

  /**
   * This is a restricted list of category codes that must be used for this transaction. These categories can be used
   * to determine which evaluation getDecision to use for a given transaction where multiple evaluation decisions are
   * returned.
   * Any returned category implies all contained categories unless a sub category is specified e.g. '101' implies
   * 101/01, 101/02 ... 101/11
   * @return a list of categories applicable to this getDecision. If no categories are returned (an empty list) that all
   * categories are applicable
   */
  List<String> getCategory();

  /**
   * This is a list of category codes that must NOT be used for this transaction. These categories can be used to
   * determine which evaluation getDecision to use for a given transaction where multiple evaluation decisions are returned.
   * Any returned category implies all contained categories unless a sub category is specified e.g. '101' implies
   * 101/01, 101/02 ... 101/11
   * @return a list of categories that are not applicable to this getDecision. If no entries are returned (an empty list)
   * than no categories are excluded
   */
  List<String> getNotCategory();

  boolean isZZ1Reportable();

  /**
   * The evaluation context object that provides the configuration context for all evaluation decisions
   * @return
   */
  Context getContext();
}
