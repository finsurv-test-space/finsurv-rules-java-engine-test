package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.StringList;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 10:56 AM
 * Determines if the transaction field has been provided and if it is not equal to the given constant value
 */
public class notTransactionFieldValue implements IAssertionFunction {
  private String field;
  private StringList constants = null;

  public notTransactionFieldValue(String field, String constant) {
    this.field = field;
    this.constants = new StringList();
    this.constants.add(constant);
  }

  public notTransactionFieldValue(String field, StringList constant) {
    this.field = field;
    this.constants = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField(field);
    if (fieldValue != null) {
      for (String constant : constants) {
        if (fieldValue.equals(constant))
          return false;
      }
    }
    return true;
  }
}
/*
function notTransactionFieldValue(field, constant) {
  return function(context, value) {
    var fieldValue = context.getTransactionField(field);
    if (fieldValue) {
      if (typeof constant === "string") {
        if (fieldValue === constant)
          return false;
      } else {
        for ( var i = 0; i < constant.length; i++) {
          if (fieldValue === constant[i])
            return false;
        }
      }
    }
    return true;
  }
}
*/