package za.co.synthesis.rule.classification;

import za.co.synthesis.rule.core.DrCr;

/**
 * Created by jake on 6/7/16.
 */
public class ClassificationRule {
  private String type;
  private String name;
  private DrCr drcr;
  private IClassificationFunction assertion;

  public ClassificationRule(final String type, final String name, final IClassificationFunction assertion) {
    this.type = type;
    this.name = name;
    this.assertion = assertion;
  }

  public ClassificationRule onDr() {
    drcr = DrCr.DR;
    return this;
  }

  public ClassificationRule onCr() {
    drcr = DrCr.CR;
    return this;
  }

  public String getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public DrCr getDrCr() {
    return drcr;
  }

  public IClassificationFunction getAssertion() {
    return assertion;
  }
}
