package za.co.synthesis.rule.support.legacydate.format;

import za.co.synthesis.rule.support.legacydate.LocalDate;
import za.co.synthesis.rule.support.legacydate.LocalDateTime;
import za.co.synthesis.rule.support.legacydate.Temporal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateTimeFormatter {
  private SimpleDateFormat sdf;

  private DateTimeFormatter(SimpleDateFormat sdf) {
    this.sdf = sdf;
  }

  public synchronized static DateTimeFormatter ofPattern(String format) {
    return new DateTimeFormatter(new SimpleDateFormat(format, Locale.ENGLISH));
  }

  public synchronized LocalDateTime parse(String value) throws ParseException {
    return new LocalDateTime(sdf.parse(value));
  }

  public synchronized LocalDate parseDate(String value) throws ParseException {
    return new LocalDate(sdf.parse(value));
  }

  public synchronized String format(Temporal value) {
    return sdf.format(value.getCalendar().getTime());
  }
}

