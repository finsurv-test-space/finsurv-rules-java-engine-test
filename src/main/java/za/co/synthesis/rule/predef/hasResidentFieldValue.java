package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.*;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 12:11 PM
 * Determines if the given resident field has been provided and if it is equal to the given constant value
 */
public class hasResidentFieldValue implements IAssertionFunction {
  private IStringResolver field;
  private StringResolverList constants = null;

  public hasResidentFieldValue(IStringResolver field, IStringResolver constant) {
    this.field = field;
    this.constants = new StringResolverList();
    this.constants.add(constant);
  }

  public hasResidentFieldValue(IStringResolver field, StringResolverList constant) {
    this.field = field;
    this.constants = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    String fieldName = field.resolve(context, value);
    Object fieldValue = context.getTransactionField("Resident.Individual." + fieldName);
    if (fieldValue == null || fieldValue instanceof IFinsurvContext.Undefined) {
      fieldValue = context.getTransactionField("Resident.Entity." + fieldName);
    }
    if (fieldValue != null) {
      for (IStringResolver constant : constants) {
        String constantStr = constant.resolve(context, value);
        if (fieldValue.equals(constantStr))
          return true;
      }
    }
    return false;
  }
}
/*
function hasResidentFieldValue(field, constant) {
  return function(context, value) {
    var fieldValue = context.getTransactionField("Resident.Individual." + field);
    if (!fieldValue)
      fieldValue = context.getTransactionField("Resident.Entity." + field);
    if (fieldValue) {
      if (typeof constant === "string") {
        if (fieldValue === constant)
          return true;
      } else {
        for ( var i = 0; i < constant.length; i++) {
          if (fieldValue === constant[i])
            return true;
        }
      }
    }
    return false;
  }
}
*/
