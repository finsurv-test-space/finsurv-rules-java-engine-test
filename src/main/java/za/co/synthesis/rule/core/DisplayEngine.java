package za.co.synthesis.rule.core;

import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.core.impl.JSLookups;
import za.co.synthesis.rule.support.Displays;
import za.co.synthesis.rule.support.LookupAggregator;
import za.co.synthesis.rule.support.RulePackage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 4/12/16.
 */
public class DisplayEngine {
  private boolean setupTime = true;
  private LookupAggregator lookups = new LookupAggregator();
  private ISupporting supporting;
  private Displays displays;
  private RulePackage loadedPackage = null;
  private Map<String, String> mappings = null;

  public DisplayEngine() {
    this.supporting = new DefaultSupporting();
  }

  private void checkSetup() throws DisplayException {
    if (!setupTime) {
      throw new DisplayException("Displayors have already been issued. You cannot make any further call to setup functions.");
    }
  }

  /***
   * Retrieves the mappings to use from either the loaded package data or the inline mappings provided in the rules file.
   * The Loaded Packae mapping take precidence if supplied.
   * @return
   */
  private Map<String, String> getMappings() {
    return loadedPackage != null ? loadedPackage.getMapping() : mappings;
  }

  public static EngineVersion getVersion() {
    return EngineVersion.getVersion();
  }

  /**
   * This will load rules from a JavaScript file (In the evaluation rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once evaluators are issued
   * @param name
   * @param reader
   * @throws Exception
   */
  public synchronized void setupLoadRule(final String name, final Reader reader) throws DisplayException {
    checkSetup();

    if (displays == null) {
      displays = new Displays();
    }
    try {
      JSStructureParser parser = new JSStructureParser(reader);
      Object obj = parser.parse();

      List<JSFunctionDefinition> withFuncs = new ArrayList<JSFunctionDefinition>();
      JSUtil.findFunctionDefinitions(obj, "with", withFuncs);
      for (JSFunctionDefinition withFunc : withFuncs) {
        if (withFunc.getParameters() != null && withFunc.getParameters().size() == 1) {
          Object withParam = withFunc.getParameters().get(0);
          if (withParam instanceof JSIdentifier) {
            String idName = ((JSIdentifier) withParam).getName();
            List<Object> results = new ArrayList<Object>();
            JSUtil.findByName(obj, idName, results);

            for (Object commonFunc : results) {
              if (commonFunc instanceof JSObject)
              displays.loadCommonFunctions((JSObject)commonFunc);
            }
          }
        }
      }

      List<JSObject> rulesetObjs = new ArrayList<JSObject>();
      JSUtil.findObjectsWith(obj, "ruleset", rulesetObjs);

      JSObject jsResult = JSUtil.findObjectWith(obj, "mappings");
      if (jsResult != null) {
        Object objMappings = jsResult.get("mappings");
        if (objMappings instanceof JSObject) {
          mappings = new HashMap<String, String>();
          for (Map.Entry<String, Object> entry : ((JSObject)objMappings).entrySet()) {
            mappings.put(entry.getKey(), entry.getValue() != null ? entry.getValue().toString() : null);
          }
        }
      }

      for (JSObject rulesetObj : rulesetObjs) {
        displays.loadRuleSet(loadedPackage != null ? loadedPackage.getMapping() : null, rulesetObj);
      }



    }
    catch (Exception e) {
      throw new DisplayException("Could not parse '" + name + "' rule file", e);
    }
  }

  /**
   * This will load rules from a JavaScript file (In the evaluation rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once evaluators are issued
   * @param filename
   * @throws Exception
   */
  public void setupLoadRuleFile(final String filename) throws DisplayException {
    try {
      FileReader fileReader = new FileReader(filename);
      setupLoadRule(filename, fileReader);
    }
    catch (Exception e) {
      throw new DisplayException("Could not load '" + filename + "' rule file", e);
    }
  }

  /**
   * This will load rules from a JavaScript resource (In the evaluation rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once evaluators are issued
   * @param fileUrl
   * @throws Exception
   */
  public void setupLoadRuleUrl(final URL fileUrl) throws DisplayException {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));
      setupLoadRule(fileUrl.getPath(), buffReader);
    }
    catch (Exception e) {
      throw new DisplayException("Could not load '" + fileUrl.getPath() + "' rule file", e);
    }
  }

  /**
   * This will load lookups from a JavaScript file. Later lookup instances will replace earlier lookups in the event
   * that a second lookup file is used. Therefore custom lookup list must be loaded after the standard lookups.
   * This is a setup function and cannot be called once validators are issued
   * @param reader
   * @throws Exception
   */
  public synchronized void setupLoadLookup(final String name, final Reader reader) throws DisplayException {
    checkSetup();

    ILookups jsLookups = new JSLookups();

    try {
      JSStructureParser parser = new JSStructureParser(reader);

      Object obj = parser.parse();

      JSObject lookupObj = JSUtil.findObjectWith(obj, "ReportingQualifier");
      if (lookupObj != null) {
        ((JSLookups)jsLookups).load(lookupObj);
      }
    }
    catch (Exception e) {
      throw new DisplayException("Could not parse '" + name + "' lookup file", e);
    }
    lookups.overrideLookups(jsLookups);
  }

  /**
   * This will load lookups from a JavaScript file. Later lookup instances will replace earlier lookups in the event
   * that a second lookup file is used. Therefore custom lookup list must be loaded after the standard lookups.
   * This is a setup function and cannot be called once validators are issued
   * @param filename
   * @throws Exception
   */
  public void setupLoadLookupFile(final String filename) throws DisplayException {
    try {
      FileReader fileReader = new FileReader(filename);
      setupLoadLookup(filename, fileReader);
    }
    catch (Exception e) {
      throw new DisplayException("Could not load '" + filename + "' lookup file", e);
    }
  }

  /**
   * This will load lookups from a JavaScript file. Later lookup instances will replace earlier lookups in the event
   * that a second lookup file is used. Therefore custom lookup list must be loaded after the standard lookups.
   * This is a setup function and cannot be called once validators are issued
   * @param fileUrl
   * @throws Exception
   */
  public void setupLoadLookupUrl(final URL fileUrl) throws DisplayException {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));
      setupLoadLookup(fileUrl.getPath(), buffReader);
    }
    catch (Exception e) {
      throw new DisplayException("Could not load '" + fileUrl.getPath() + "' lookup file", e);
    }
  }

  public synchronized void setupLookups(ILookups lookups) throws DisplayException {
    checkSetup();

    this.lookups.overrideLookups(lookups);
  }

  public synchronized void setupSupporting(ISupporting supporting) throws DisplayException {
    checkSetup();

    this.supporting = supporting;
  }


  public synchronized Displayor issueDisplayor() throws DisplayException {
    setupTime = false;

    if (displays != null) {
      return new Displayor(getMappings(), lookups, supporting, displays);
    }
    throw new DisplayException("No Display rules have been loaded. Call setupLoadRule methods before issueing Displayor");
  }
}
