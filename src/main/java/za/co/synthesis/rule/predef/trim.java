package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

/**
 * User: jake
 * Date: 4/27/16
 * Time: 10:53 AM
 * This is a display function that is used to trim space characters from the start and end of the current field
 */
public class trim implements IDisplayFunction {
  @Override
  public String execute(FinsurvContext context, Object value) {
    if (value != null) {
      return value.toString().trim();
    }
    return null;
  }
}

