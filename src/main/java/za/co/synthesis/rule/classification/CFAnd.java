package za.co.synthesis.rule.classification;

/**
 * Created by jake on 6/7/16.
 */
public class CFAnd implements IClassificationFunction {
  private IClassificationFunction left;
  private IClassificationFunction right;

  public CFAnd(IClassificationFunction left, IClassificationFunction right) {
    this.left = left;
    this.right = right;
  }

  public boolean execute(ClassificationContext context, Object value) {
    return left.execute(context, value) && right.execute(context, value);
  }
}
