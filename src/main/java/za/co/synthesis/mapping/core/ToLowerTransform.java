package za.co.synthesis.mapping.core;

/**
 * User: jake
 * Date: 3/21/16
 * Time: 11:55 AM
 * Convert a given string to lower case
 */
public class ToLowerTransform  implements ITransform {

  @Override
  public String transform(String value) {
    return value.toLowerCase();
  }
}
