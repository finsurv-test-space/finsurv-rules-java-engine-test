package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.rule.support.DSLSupport;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

/**
 * User: jake
 * Date: 4/19/16
 * Time: 3:42 PM
 * Returns the value in the scoped field name. The given scope is the scope of the field that we are currently
 * processing and is considered to be the default scope if none is supplied.
 */
public class formatScopedFieldValue implements IDisplayFunction {
  private final String field;
  private final String format;

  public formatScopedFieldValue(String format, String field) {
    this.format = format;
    this.field = field;
  }

  @Override
  public String execute(FinsurvContext context, Object value) {
    if (format != null && format.contains("%s") && field != null) {
      Object obj = DSLSupport.lookupScopedFieldValue(context, field);
      String fieldValue = obj != null ? obj.toString() : null;
      return format.replace("%s", fieldValue);
    }
    return format;
  }
}
