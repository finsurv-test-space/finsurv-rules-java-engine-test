package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 9:40 AM
 * Determines if the sum of the monetary ForeignCardHoldersPurchasesRandValue and ForeignCardHoldersCashWithdrawalsRandValue
 * values do not equal the current value
 */
public class notSumCardValue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    BigDecimal total = new BigDecimal(0);
    for (int i = 0; i < context.getMoneySize(); i++) {
      BigDecimal c1value = Util.number(context.getMoneyField(i, context.map("ForeignCardHoldersPurchases{{LocalValue}}")));
      BigDecimal c2value = Util.number(context.getMoneyField(i, context.map("ForeignCardHoldersCashWithdrawals{{LocalValue}}")));
      if (c1value != null)
        total = total.add(c1value);
      if (c2value != null)
        total = total.add(c2value);
    }
    BigDecimal valueNum = Util.number(value);
    if (valueNum != null)
      return valueNum.compareTo(total) != 0;
    return true;
  }
}
/*
function notSumCardValue(context, value) {
  var total = 0;
  for ( var i = 0; i < context.getMoneySize(); i++) {
    var c1value = context.getMoneyField(i, "ForeignCardHoldersPurchasesRandValue");
    var c2value = context.getMoneyField(i, "ForeignCardHoldersCashWithdrawalsRandValue");
    if (c1value)
      total += Number(c1value);
    if (c2value)
      total += Number(c2value);
  }
  var a = Math.round(Number(value) * 100);
  var b = Math.round(total * 100);
  return a != b;
}
*/