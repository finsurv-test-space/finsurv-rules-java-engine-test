package za.co.synthesis.mapping.core;

import za.co.synthesis.mapping.configuration.*;
import za.co.synthesis.mapping.source.ISourceData;
import za.co.synthesis.mapping.source.ISourceManager;
import za.co.synthesis.javascript.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 4:36 PM
 * This is the core class that contains the functionality to produce entities using the source data and
 * mapping configuration.
 */
public class Mapper {
  private final static Map<String, IAggregation> aggregationMap = new HashMap<String, IAggregation>();
  private final static Map<String, ITransform> transformMap = new HashMap<String, ITransform>();

  static {
    registerAggregation("sum", new SumAggregation());
    registerAggregation("min", new MinAggregation());
    registerAggregation("max", new MaxAggregation());
    registerAggregation("first", new FirstAggregation());
    registerAggregation("populated", new FirstPopulatedAggregation());
    registerAggregation("last", new LastAggregation());

    registerTransform("toUpper", new ToUpperTransform());
    registerTransform("toLower", new ToLowerTransform());
  }

  public static boolean hasAggregation(String name) {
    return aggregationMap.containsKey(name);
  }

  public static void registerAggregation(String name, IAggregation aggregation) {
    if (aggregationMap.containsKey(name)) {
      aggregationMap.remove(name);
      aggregationMap.put(name, aggregation);
    }
    else {
      aggregationMap.put(name, aggregation);
    }
  }

  public static boolean hasTransform(String name) {
    return transformMap.containsKey(name);
  }

  public static void registerTransform(String name, ITransform transform) {
    if (transformMap.containsKey(name)) {
      transformMap.remove(name);
      transformMap.put(name, transform);
    }
    else {
      transformMap.put(name, transform);
    }
  }

  private static void addFieldToObject(JSObject jsTran, Field field, String value, int moneyIndex, int importExportIndex) {
    boolean bForceAdd = false;
    if (value == null || value.length() == 0) {
      value = field.getDefaultValue();
      if (value != null && value.length() == 0)
        bForceAdd = true;
    }
    if (bForceAdd || (value != null && value.length() > 0)) {
      JSObject jsObj = jsTran;
      if (field.getMappingScope() == MappingScope.Money || field.getMappingScope() == MappingScope.ImportExport) {
        JSArray jsArray = (JSArray) jsTran.get("MonetaryAmount");
        if (jsArray == null) {
          jsArray = new JSArray();
          jsTran.put("MonetaryAmount", jsArray);
        }
        if (moneyIndex >= 0) {
          if (moneyIndex < jsArray.size()) {
            jsObj = (JSObject) jsArray.get(moneyIndex);
          }
          else
          if (moneyIndex == jsArray.size()) {
            jsObj = new JSObject();
            jsArray.add(jsObj);
          }
          else {
            jsObj = null;
          }
        }
      }
      if (jsObj != null && field.getMappingScope() == MappingScope.ImportExport) {
        JSArray jsArray = (JSArray) jsObj.get("ImportExport");
        if (jsArray == null) {
          jsArray = new JSArray();
          jsObj.put("ImportExport", jsArray);
        }
        if (importExportIndex >= 0) {
          if (importExportIndex < jsArray.size()) {
            jsObj = (JSObject) jsArray.get(importExportIndex);
          }
          else
          if (importExportIndex == jsArray.size()) {
            jsObj = new JSObject();
            jsArray.add(jsObj);
          }
          else {
            jsObj = null;
          }
        }
      }

      if (jsObj != null) {
        String partName;
        for (int i=0; i<field.getFieldParts().length - 1; i++) {
          partName = field.getFieldParts()[i];
          JSObject jsPart = (JSObject) jsObj.get(partName);
          if (jsPart == null) {
            jsPart = new JSObject();
            jsObj.put(partName, jsPart);
            jsObj = jsPart;
          }
          else
            jsObj = jsPart;
        }

        partName = field.getFieldParts()[field.getFieldParts().length - 1];

        jsObj.put(partName, value);
      }
    }
  }

  private static void addFieldToMeta(Map<String, String> metaData, Field field, String value) {
    if ((value == null || value.length() == 0) && field.getDefaultValue() != null) {
      value = field.getDefaultValue();
    }
    metaData.put(field.getMetaName(), value);
  }

  private static String transformValue(Field field, String value) {
    if (field.getTransform() != null && transformMap.containsKey(field.getTransform()) && value != null) {
      return transformMap.get(field.getTransform()).transform(value);
    }
    return value;
  }

  private static String transformedValueFromLevel_0(Field field, ISourceData srcData) {
    if (field.getAggregate() != null && aggregationMap.containsKey(field.getAggregate())) {
      List<String> values = new ArrayList<String>();
      if (srcData.getLevel() == 0) {
        int count = srcData.getRowCount();
        for (int i=0; i<count; i++) {
          String val = srcData.getDataPoint(i, field.getExternalConfig());
          if (val != null)
            values.add(val);
        }
      }
      else
      if (srcData.getLevel() == 1) {
        int count = srcData.getRowCount();
        for (int i=0; i<count; i++) {
          String val = srcData.getDataPoint(i, field.getExternalConfig());
          if (val != null)
            values.add(val);
        }
      }
      else
      if (srcData.getLevel() == 2) {
        int count1 = srcData.getRowCount();
        for (int i=0; i<count1; i++) {
          int count2 = srcData.getRowCount(i);
          for (int j=0; j<count2; j++) {
            String val = srcData.getDataPoint(i, j, field.getExternalConfig());
            if (val != null)
              values.add(val);
          }
        }
      }

      IAggregation aggregation = aggregationMap.get(field.getAggregate());
      return transformValue(field, aggregation.aggregate(values));

    }
    return transformValue(field, srcData.getDataPoint(field.getExternalConfig()));
  }

  private static void mapLevel_0(Section section, Map<String, ISourceData> srcDataMap, Map<String, String> metaData, JSObject jsEntity) {
    for (Field field : section.getFields()) {
      ISourceData srcData = srcDataMap.get(field.getSource());

      if (field.isMeta()) {
        addFieldToMeta(metaData, field, transformedValueFromLevel_0(field, srcData));
      }
    }

    for (Field field : section.getFields()) {
      ISourceData srcData = srcDataMap.get(field.getSource());

      if (!field.isMeta()) {
        if (field.isCondition() ) {
          if (field.getConditionFieldValue().equals(metaData.get(field.getConditionFieldName()))) {
            addFieldToObject(jsEntity, field, transformedValueFromLevel_0(field, srcData), 0, 0);
          }
        }
        else
          addFieldToObject(jsEntity, field, transformedValueFromLevel_0(field, srcData), 0, 0);
      }
    }
  }

  private static String transformedValueFromLevel_1(Field field, ISourceData srcData, int moneyIndex) {
    if (field.getAggregate() != null && aggregationMap.containsKey(field.getAggregate())) {
      List<String> values = new ArrayList<String>();
      if (srcData.getLevel() == 2) {
        int count = srcData.getRowCount(moneyIndex);
        for (int i=0; i<count; i++) {
          String val = srcData.getDataPoint(moneyIndex, i, field.getExternalConfig());
          if (val != null)
            values.add(val);
        }
      }

      IAggregation aggregation = aggregationMap.get(field.getAggregate());
      return transformValue(field, aggregation.aggregate(values));

    }
    return transformValue(field, srcData.getDataPoint(moneyIndex, field.getExternalConfig()));
  }

  private static void mapLevel_1(Section section, Map<String, ISourceData> srcDataMap, Map<String, String> metaData, int moneyIndex, JSObject jsEntity) {
    for (Field field : section.getFields()) {
      ISourceData srcData = srcDataMap.get(field.getSource());

      if (field.isMeta()) {
        addFieldToMeta(metaData, field, transformedValueFromLevel_1(field, srcData, moneyIndex));
      }
    }

    for (Field field : section.getFields()) {
      ISourceData srcData = srcDataMap.get(field.getSource());

      if (!field.isMeta()) {
        if (field.isCondition() ) {
          if (field.getConditionFieldValue().equals(metaData.get(field.getConditionFieldName()))) {
            addFieldToObject(jsEntity, field, transformedValueFromLevel_1(field, srcData, moneyIndex), moneyIndex, 0);
          }
        }
        else
          addFieldToObject(jsEntity, field, transformedValueFromLevel_1(field, srcData, moneyIndex), moneyIndex, 0);
      }
    }
  }

  private static void mapLevel_2(Section section, Map<String, ISourceData> srcDataMap, Map<String, String> metaData, int moneyIndex, int importExportIndex, JSObject jsEntity) {
    for (Field field : section.getFields()) {
      ISourceData srcData = srcDataMap.get(field.getSource());

      if (field.isMeta()) {
        addFieldToMeta(metaData, field,
                transformValue(field, srcData.getDataPoint(moneyIndex, importExportIndex, field.getExternalConfig())));
      }
    }

    for (Field field : section.getFields()) {
      ISourceData srcData = srcDataMap.get(field.getSource());

      if (!field.isMeta()) {
        if (field.isCondition() ) {
          if (field.getConditionFieldValue().equals(metaData.get(field.getConditionFieldName()))) {
            addFieldToObject(jsEntity, field,
                    transformValue(field, srcData.getDataPoint(moneyIndex, importExportIndex, field.getExternalConfig())),
                    moneyIndex, importExportIndex);
          }
        }
        else
          addFieldToObject(jsEntity, field,
                  transformValue(field, srcData.getDataPoint(moneyIndex, importExportIndex, field.getExternalConfig())),
                  moneyIndex, importExportIndex);
      }
    }
  }

  private static MappingScope getMappedScope(MappingConfig mappingConfig, String source) {
    for (Section section : mappingConfig.getSections()) {
      if (section.getSource().equals(source)) {
        return section.getMappingScope();
      }
    }
    return null;
  }

  public static Entity composeEntity(MappingConfig mappingConfig, ISourceManager sourceManager, Object entry) {
    JSObject jsTran = new JSObject();
    JSArray jsResults = new JSArray();
    Map<String, ISourceData> srcDataMap = new HashMap<String, ISourceData>();
    Map<String, String> metaData = new HashMap<String, String>();

    for (Source source : mappingConfig.getSources()) {
      srcDataMap.put(source.getCode(), sourceManager.getSourceData(entry, source.getCode(), getMappedScope(mappingConfig,source.getCode())));
    }

    ISourceData resultsSrcData = null;
    ISourceData moneySrcData = null;
    ISourceData ieSrcData = null;
    for (Section section : mappingConfig.getSections()) {
      if (section.getMappingScope() == MappingScope.Results) {
        resultsSrcData = srcDataMap.get(section.getSource());
      }
      else
      if (section.getMappingScope() == MappingScope.Money) {
        moneySrcData = srcDataMap.get(section.getSource());
      }
      else
      if (section.getMappingScope() == MappingScope.ImportExport) {
        ieSrcData = srcDataMap.get(section.getSource());
      }
    }

    for (Section section : mappingConfig.getSections()) {
      if (section.getMappingScope() == MappingScope.Results) {
        int resultsCount = resultsSrcData != null ? resultsSrcData.getRowCount() : 0;
        for (int resultIndex = 0; resultIndex < resultsCount; resultIndex++) {
          JSObject jsResult = new JSObject();
          mapLevel_1(section, srcDataMap, metaData, resultIndex, jsResult);
          jsResults.add(jsResult);
        }
      }
      else
      if (section.getMappingScope() == MappingScope.Transaction) {
        mapLevel_0(section, srcDataMap, metaData, jsTran);
      }
      else
      if (section.getMappingScope() == MappingScope.Money) {
        if (moneySrcData != null) {
          int moneyCount = moneySrcData.getRowCount();
          for (int moneyIndex = 0; moneyIndex < moneyCount; moneyIndex++) {
            mapLevel_1(section, srcDataMap, metaData, moneyIndex, jsTran);
          }
        }
      }
      else
      if (section.getMappingScope() == MappingScope.ImportExport) {
        if (moneySrcData != null) {
          int moneyCount = moneySrcData.getRowCount();
          for (int moneyIndex = 0; moneyIndex < moneyCount; moneyIndex++) {
            if (ieSrcData != null) {
              int ieCount = 0;
              try {
                ieCount = ieSrcData.getRowCount(moneyIndex);
              } catch(Exception err) {
                System.err.println("Error on UNIQUE_TRN_N: "+metaData.get("UNIQUE_TRAN_N")+" -- Error: "+err.getMessage());
              }
              for (int ieIndex = 0; ieIndex < ieCount; ieIndex++) {
                mapLevel_2(section, srcDataMap, metaData, moneyIndex, ieIndex, jsTran);
              }
            }
          }
        }
      }
    }
    return new Entity(jsTran, jsResults, metaData);
  }
}
