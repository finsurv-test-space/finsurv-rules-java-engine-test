package za.co.synthesis;

import za.co.synthesis.csv.CsvOutput;
import za.co.synthesis.csv.CsvSourceManager;
import za.co.synthesis.mapping.configuration.MappingConfig;
import za.co.synthesis.mapping.core.Entity;
import za.co.synthesis.mapping.core.Mapper;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.core.impl.JSCustomValues;
import za.co.synthesis.rule.core.impl.JSFinsurvContext;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.support.Util;
import za.co.synthesis.validate.*;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 7/23/15
 * Time: 2:21 PM
 * A CSV based testing application that leverages off information supplied in spreadsheets
 */
public class CsvTest {
  private static Validator issueValidator(ValidationEngine engine, DefaultSupporting supporting) throws ValidationException {
    engine.setupLoadLookupFile("../finsurv-rules/lookups.js");
    engine.setupLoadRuleFile("../finsurv-rules/transaction.js");
    engine.setupLoadRuleFile("../finsurv-rules/exttransaction.js");
    engine.setupLoadRuleFile("../finsurv-rules/money.js");
    engine.setupLoadRuleFile("../finsurv-rules/extmoney.js");
    engine.setupLoadRuleFile("../finsurv-rules/importexport.js");
    engine.setupLoadRuleFile("../finsurv-rules/extimportexport.js");

    supporting.setCurrentDate(LocalDate.now());
    supporting.setGoLiveDate(Util.date("2013-08-18"));
    engine.setupSupporting(supporting);
    engine.setupLookups(new LookupsTest());

    TestUtility.registerTestFunctions(engine);

    Validator validator = engine.issueValidator();
    return validator;
  }

  private static void setupAlternateErrorCodes(Map<String, String> alternateErrorCode) {
    alternateErrorCode.put("323", "368");
    alternateErrorCode.put("431", "302");
    alternateErrorCode.put("271", "533");  // 271 Invalid account identifier => 533 Type of data reported in all attributes must be correct
    alternateErrorCode.put("338", "533");  // 338 Invalid postal code format => 533 Type of data reported in all attributes must be correct
    alternateErrorCode.put("243", "533");  // 243 If Flow is IN, SWIFT country code must be ZA => 533 Type of data reported in all attributes must be correct
    alternateErrorCode.put("497", "499");  // 497 Flow is OUT and cat is 101/01 to 101/10 the first 3 chars must be INV => 499 Invalid ImportControl Number format
    alternateErrorCode.put("499", "498");  // 499 Invalid ImportControl Number format => 498 INVALID CUSTOMS OFFICE CODE COMPLETED IN THE IMPORTCONTROLNUMBER ATTRIBUTE
    alternateErrorCode.put("393", "400,401,402");  // 393 INVALID SUBJECT => 400 incorrect cat / subject combo, 401 INCORRECT FLOW COMPLETED WITH SUBJECT, 402 DESCRIPTION MUST BE COMPLETED
    alternateErrorCode.put("221", "500");  // 221 NOMRNONIVS MUST HAVE A VALUE Y => 500 MRN NOT STORED ON IVS
    alternateErrorCode.put("416", "419,421,424,426,428");  // 416 If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used and Resident Entity is completed, then must be completed => 419,421,424,426,428 INDIVIDUALTHIRDPARTYNAME MUST BE COMPLETED
    alternateErrorCode.put("543", "209,533");  // 543 ReplacementTransaction must be completed => 209 REPLACEMENTTRANSACTION MUST CONTAIN A VALUE Y OR N  ; 533 Type of data reported in all attributes must be correct
    alternateErrorCode.put("349", "ignore"); //SEQUENCENUMBER NOT IN SEQUENCE
    alternateErrorCode.put("244", "ignore"); //THE SUM OF RANDVALUE AND FOREIGNVALUE MUST EQUAL TOTALVALUE
    alternateErrorCode.put("529", "ignore"); //TOTAL PAYMENTVALUE OF ALL SUBSEQUENCES EXCEEDS A 1% VARIANCE WITH RANDVALUE OR FOREIGNVALUE
    alternateErrorCode.put("212", "ignore:TRANSACTION REFERENCE/SEQUENCE OF CANCELLED TRANSACTION NOT STORED ON SARB DATABASE.");
    alternateErrorCode.put("494", "ignore:SUBSEQUENCE UNDER IMPORTEXPORTDATA MUST BE SEQUENTIAL");
  }

  public static void main( String[] args ) throws Exception {
    ValidationEngine engine = new ValidationEngine();
    DefaultSupporting supporting = new DefaultSupporting();
    Map<String, String> alternateErrorCode = new HashMap<String, String>();

    setupAlternateErrorCodes(alternateErrorCode);

    Validator validator = issueValidator(engine, supporting);


    MappingConfig mappingConfig = MappingConfig.load("./src/test/resources/stdbank_mapping.js");
    //createTableScripts(mappingConfig);

    CsvSourceManager sourceManager = new CsvSourceManager();
    sourceManager.openFile("/Users/jake/Downloads/BOPCD_QUEUES/SARB_REJECTIONS.csv");
//    sourceManager.openFile("/Users/jake/Downloads/BOPCD_QUEUES/INVALIDS.csv");

    sourceManager.setup(mappingConfig);


    CsvOutput csvOutput = new CsvOutput();
    csvOutput.openForWrite("errorlog.txt");

    Entity tran;
    int count = 1000;
    Object entry;

    int totalTransactions = 0;
    int totalTests = 0;
    int notErrors = 0;
    int ignoreErrors = 0;
    int codesMatchSARS = 0;
    int invalidMatchSARS = 0;
    int disagreeWithSARS = 0;
    ErrorCount ignoreErrorCount = new ErrorCount();
    ErrorCount invalidMatchCount = new ErrorCount();
    ErrorCount disagreeWithSARSCount = new ErrorCount();

    while ((entry = sourceManager.getNextEntry()) != null) {
      boolean bValidError = true;
      boolean bIgnoreError = false;

      tran = Mapper.composeEntity(mappingConfig, sourceManager, entry);
      totalTransactions++;

      JSObject customData = new JSObject();
      customData.put("DealerType", "AD");
      customData.put("AccountFlow", tran.getJsObject().get("Flow"));

      Object ccnObj = tran.getAnyJsValue("Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber");
      //printUCR(tran.getJsObject(), ccnObj, errorCode, errorDesc);

      JSArray oldResults = tran.getJsResults();
      for(Object oldResult : oldResults)
      {
        totalTests++;

        JSObject oldJsResult = (JSObject)oldResult;
        Object val = oldJsResult.get("ErrorCode");
        String errorCode = (val != null ? val.toString() : "");
        val = oldJsResult.get("ErrorDescription");
        String errorDesc = (val != null ? val.toString() : "");
        val = oldJsResult.get("ErrorAttribute");
        String errorAttribute = (val != null ? val.toString() : "");
        val = oldJsResult.get("TrnReference");
        String trnReference = (val != null ? val.toString() : "");
        val = oldJsResult.get("Level1SeqNo");
        int seqNo = (val != null ? Integer.parseInt(val.toString()) : 0);
        val = oldJsResult.get("Level2SeqNo");
        int subSeqNo = (val != null ? Integer.parseInt(val.toString()) : 0);

        csvOutput.addOldError(trnReference, errorCode, errorDesc, errorAttribute, seqNo, subSeqNo, "");

        TestUtility.populateTestInfo(tran, errorCode, errorDesc, seqNo, subSeqNo, ccnObj);

        TestUtility.populateCustomData(ccnObj, customData);

        JSCustomValues customValues = new JSCustomValues(customData);

        supporting.setCurrentDate(Util.date(tran.getJsObject().get("ValueDate")));
        IFinsurvContext context = new JSFinsurvContext(tran.getJsObject());
        ValidationStats stats = new ValidationStats();

        List<ResultEntry> issues = validator.validate(context, customValues, stats);

        boolean bWeHaveSARBError = false;
        if (issues.size() > 0) {
          for (ResultEntry res : issues) {
            csvOutput.addNewError(trnReference, res, "");

            String altResCode = alternateErrorCode.containsKey(res.getCode()) ? alternateErrorCode.get(res.getCode()) : "";
            if (res.getCode().equals(errorCode) || altResCode.contains(errorCode)) {
              bWeHaveSARBError = true;
              break;
            }
          }
        }

        // Not valid errors
        if (errorCode.equals("") && issues.size() == 0) {
          notErrors++;
          bValidError = false;
        }
        if (errorDesc.equals("238-error no code details found") && issues.size() == 0) {
          notErrors++;
          bValidError = false;
        }
        if (errorDesc.equals("313-error no code details found") && issues.size() == 0) {
          notErrors++;
          bValidError = false;
        }

        // Ignore these errors
        if (bValidError && alternateErrorCode.containsKey(errorCode)) {
          String alternate = alternateErrorCode.get(errorCode);
          if (alternate.equals("ignore") ||
              alternate.equals("ignore:"+errorDesc)) {
            ignoreErrors++;
            bIgnoreError = true;
            ignoreErrorCount.addError(errorCode, errorDesc, errorAttribute, trnReference);
          }
        }

        // We have the same error as raised by the SARB
        if (bValidError && !bIgnoreError) {
          if (bWeHaveSARBError)
            codesMatchSARS++;
          else {
            int errorCount = 0;
            for (ResultEntry res : issues) {
              if (res.getType() == ResultType.Error)
                errorCount++;
            }

            if (errorCount > 0) {
              invalidMatchSARS++;
              invalidMatchCount.addError(errorCode, errorDesc, errorAttribute, trnReference);
            }
            else {
              disagreeWithSARS++;
              disagreeWithSARSCount.addError(errorCode, errorDesc, errorAttribute, trnReference);
            }
          }
        }

        if (bValidError && !bIgnoreError && !bWeHaveSARBError && errorCode.equals("221")) { // NOMRNONIVS MUST HAVE A VALUE Y
          System.out.print(tran.getMetaData().get("CREATED_Z"));
          System.out.println("|" + tran.getJsObject().toString());
          System.out.println("Agree: " + (bWeHaveSARBError ? "Y" : "N") + "; No of issues: " + issues.size());
        }
      }
      if (--count <= 0)
        break;
    }
    System.out.println("------------");
    System.out.println("Total             : " + totalTests + " (transactions " + totalTransactions + ")");
    System.out.println("Not Errors        : " + notErrors);
    System.out.println("Ignore Errors     : " + ignoreErrors);
    System.out.println("Codes Match SARB  : " + codesMatchSARS);
    System.out.println("Invalid Match SARB: " + invalidMatchSARS);
    System.out.println("Disagree With SARB: " + disagreeWithSARS);
    System.out.println("Checksum (should be 0): " + (totalTests - notErrors - ignoreErrors - codesMatchSARS - invalidMatchSARS - disagreeWithSARS));
    System.out.println("------------");

    System.out.println("** SARB says Error - We say None **");
    disagreeWithSARSCount.print(6, 3);

    System.out.println("** Different Error Codes **");
    invalidMatchCount.print(10, 3);

    System.out.println("** Ignored Codes **");
    ignoreErrorCount.print(6, 3);

    csvOutput.close();
  }
}
