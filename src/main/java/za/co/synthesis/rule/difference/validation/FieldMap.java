package za.co.synthesis.rule.difference.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FieldMap {
  private List<String> fieldOrder = new ArrayList<String>();
  private Map<String, FieldSize> fieldMap = new HashMap<String, FieldSize>();

  public void clear() {
    fieldOrder.clear();
    fieldMap.clear();
  }

  private static String fieldKey(FieldSize field) {
    String key = field.getScope().getName().substring(0, 2);
    key += "::";
    key += field.getField();
    return key;
  }

  public void add(FieldSize field) {
    String key = fieldKey(field);
    FieldSize toAdd = field;
    FieldSize old = fieldMap.get(key);
    if (old != null) {
      toAdd = new FieldSize(field.getScope(), field.getField(),
              field.getMinLen() != null ? field.getMinLen() : old.getMinLen(),
              field.getMaxLen() != null ? field.getMaxLen() : old.getMaxLen(),
              field.getLen() != null ? field.getLen() : old.getLen());
    }
    if (fieldMap.put(key, toAdd) == null) {
      fieldOrder.add(key);
    }
  }

  public FieldSize get(String key) {
    return fieldMap.get(key);
  }

  public void remove(String key) {
    if (fieldMap.remove(key) != null) {
      fieldOrder.remove(key);
    }
  }

  public List<String> fieldKeys() {
    return fieldOrder;
  }
}
