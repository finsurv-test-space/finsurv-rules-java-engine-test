package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.*;

import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 7:20 AM
 * Determines if the value is in the given the list of currencies
 */
public class isCurrencyIn implements IAssertionFunction {
  private StringResolverList constants = null;

  public isCurrencyIn(IStringResolver value) {
    constants = new StringResolverList();
    constants.add(value);
  }

  public isCurrencyIn(StringResolverList value) {
    this.constants = value;
  }

  public boolean execute(FinsurvContext context, Object value) {
    String currency = context.getCurrency();
    if (currency != null) {
      for (IStringResolver c : constants) {
        if (c != null) {
          String str = c.resolve(context, value);
          if (currency.equals(str))
            return true;
        }
      }
    }
    return false;
  }
}
/*
function isCurrencyIn(currencies) {
  return function(context, value) {
    var currency = context.getTransactionField("FlowCurrency");
    if (currency) {
      if (typeof currencies === "string") {
        if (currency === currencies)
          return true;
      } else {
        for ( var i = 0; i < currencies.length; i++) {
          if (currency === currencies[i])
            return true;
        }
      }
    }
    return false;
  }
}
*/
