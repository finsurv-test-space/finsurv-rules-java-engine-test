package za.co.synthesis.javascript;

import java.util.*;

/**
 * User: jake
 * Date: 8/3/14
 * Time: 9:24 AM
 * Essentially a JavaScript Object is just a map of Objects
 */
public class JSObject extends HashMap<String, Object> implements IJSSnippetSupport {
  protected final JSSnippetSupport snippets = new JSSnippetSupport();
  private final List<String> putOrder = new ArrayList<String>();

  @Override
  public Object put(String key, Object value) {
    if (!putOrder.contains(key)) {
      putOrder.add(key);
    }
    return super.put(key, value);
  }

  @Override
  public Collection<Object> values() {
    if (putOrder.size() > 0) {
      List<Object> values = new ArrayList<Object>();
      for (String key : putOrder) {
        values.add(get(key));
      }
      return values;
    }
    else {
      return super.values();
    }
  }

  @Override
  public void clear() {
    putOrder.clear();
    super.clear();
  }

  @Override
  public Object remove(Object key) {
    putOrder.remove(key.toString());
    return super.remove(key);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("{");

    for (String key : putOrder) {
      if (containsKey(key)) {
        if (sb.length() > 1)
          sb.append(", ");
        sb.append(key).append(": ");
        Object obj = get(key);
        if (obj != null) {
          if (obj instanceof String)
            sb.append('"').append(JSUtil.escapeJavaScriptString(obj.toString(), false)).append('"');
          else
            sb.append(obj.toString());
        } else {
          sb.append("null");
        }
      }
    }
    sb.append("}");

    return sb.toString();
  }

  public void compose(JSWriter writer) {
    writer.append("{");
    writer.incIndent();
    writer.appendNewLine();
    boolean bFirst = true;
    for (String key : putOrder) {
      if (containsKey(key)) {
        if (!bFirst) {
          writer.append(", ");
          writer.appendNewLine();
        }
        else
          bFirst = false;
        if (writer.isQuoteAttributes()) {
          writer.append('"');
          JSUtil.escapeJavaScriptString(key, false, writer);
          writer.append('"');
          writer.append(": ");
        }
        else {
          writer.append(key).append(": ");
        }
        Object obj = get(key);
        if (obj != null) {
          if (obj instanceof String) {
            writer.append('"');
            JSUtil.escapeJavaScriptString(obj.toString(), false, writer);
            writer.append('"');
          } else if (obj instanceof JSObject) {
            ((JSObject) obj).compose(writer);
          } else if (obj instanceof JSArray) {
            ((JSArray) obj).compose(writer);
          } else if (obj instanceof JSFunctionDefinition) {
            ((JSFunctionDefinition) obj).compose(writer);
          } else if (obj instanceof JSFunctionCall) {
            ((JSFunctionCall) obj).compose(writer);
          } else if (obj instanceof JSIdentifier) {
            ((JSIdentifier) obj).compose(writer);
          } else if (obj instanceof JSRegExLiteral) {
            ((JSRegExLiteral) obj).compose(writer);
          } else if (obj instanceof Boolean) {
            writer.append(((Boolean)obj)?"true":"false");
          } else if (obj instanceof Number || Number.class.isAssignableFrom(obj.getClass())) {
            writer.append(obj.toString());
          } else {
            writer.append('"');
            writer.append(obj.toString());
            writer.append('"');
          }
        } else {
          writer.append("null");
        }
      }
    }
    writer.decIndent();
    writer.appendNewLine();
    writer.append("}");
  }

  @Override
  public void setPreceding(JSSnippetSupport preceding, int precedingIndex) {
    snippets.setPreceding(preceding, precedingIndex);
  }

  @Override
  public void addSnippet(JSSnippet snippet) {
    snippets.addSnippet(snippet);
  }

  @Override
  public int currentSnippetIndex() {
    return snippets.currentSnippetIndex();
  }

  @Override
  public void augmentEndSnippet(String extraText) {
    snippets.augmentEndSnippet(extraText);
  }

  @Override
  public void composeSnippets(JSWriter writer) {
    boolean bIndentWriter = false;
    JSSnippet snippet = snippets.getFirstSnippetByType(JSSnippet.Type.Start);
    if (snippet != null) {
      writer.append(snippet.getText());
    }
    else {
      writer.append("{");

      writer.incIndent();
      writer.appendNewLine();
      bIndentWriter = true;
    }

    boolean bFirst = true;
    for (String key : putOrder) {
      Object obj = get(key);

      if (containsKey(key)) {
        if (!bFirst) {
          snippet = snippets.getSeparatorByName(key);
          if (snippet != null) {
            writer.append(snippet.getText());
          }
          else {
            writer.append(", ");
          }
        } else {
          bFirst = false;
        }

        snippet = snippets.getVariableByName(key);
        if (snippet != null) {
          writer.append(snippet.getText());
        }
        else {
          if (writer.isQuoteAttributes()) {
            writer.append('"');
            JSUtil.escapeJavaScriptString(key, false, writer);
            writer.append('"');
            writer.append(": ");
          } else {
            writer.append(key).append(": ");
          }
        }

        snippet = snippets.getSnippetForValue(obj);

        if (obj != null) {
          if (obj instanceof String) {
            if (snippet != null) {
              writer.append(snippet.getText());
            }
            else {
              writer.append('"');
              JSUtil.escapeJavaScriptString(obj.toString(), false, writer);
              writer.append('"');
            }
          } else if (obj instanceof JSObject) {
            ((JSObject) obj).composeSnippets(writer);
          } else if (obj instanceof JSArray) {
            ((JSArray) obj).composeSnippets(writer);
          } else if (obj instanceof JSFunctionDefinition) {
            ((JSFunctionDefinition) obj).composeSnippets(writer);
          } else if (obj instanceof JSFunctionCall) {
            ((JSFunctionCall) obj).composeSnippets(writer);
          } else if (obj instanceof JSIdentifier) {
            ((JSIdentifier) obj).composeSnippets(writer);
          } else if (obj instanceof JSRegExLiteral) {
            ((JSRegExLiteral) obj).composeSnippets(writer);
          } else if (obj instanceof Boolean) {
            if (snippet != null) {
              writer.append(snippet.getText());
            }
            else {
              writer.append(((Boolean) obj) ? "true" : "false");
            }
          } else if (obj instanceof Number || Number.class.isAssignableFrom(obj.getClass())) {
            if (snippet != null) {
              writer.append(snippet.getText());
            }
            else {
              writer.append(obj.toString());
            }
          } else {
            if (snippet != null) {
              writer.append(snippet.getText());
            }
            else {
              writer.append('"');
              writer.append(obj.toString());
              writer.append('"');
            }
          }
        } else {
          writer.append("null");
        }
      }
    }

    snippet = snippets.getFirstSnippetByType(JSSnippet.Type.End);
    if (snippet != null) {
      writer.append(snippet.getText());
    }
    else {
      if (bIndentWriter) {
        writer.decIndent();
        writer.appendNewLine();
      }

      writer.append("}");
    }
  }

  @Override
  public void removeExtraWhitespace() {
    snippets.removeExtraWhitespace(this);
  }

  @Override
  public void injectWhitespace(Object parentObject) {
    snippets.injectWhitespace(parentObject, this);
  }
}
