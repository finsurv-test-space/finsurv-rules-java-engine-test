package za.co.synthesis;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultCustomValues;
import za.co.synthesis.rule.core.impl.JSCustomValues;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jake on 5/31/16.
 */
public class DisplayTestSupport {
  public static class DisplayCheck {
    private String checkType;
    private String fieldName;
    private DisplayEntry.Visibility visibility;
    private DisplayEntry.Availability availability;
    private String fieldLabel;
    private String fieldValue;
    private List<String> limitValues = null;
    private List<String> excludeValues = null;

    private DisplayCheck(String checkType, String fieldName) {
      this.checkType = checkType;
      this.fieldName = fieldName;
    }

    public static DisplayCheck check(String fieldName, DisplayEntry.Visibility visibility) {
      DisplayCheck result = new DisplayCheck("Visibility", fieldName);
      result.setVisibility(visibility);
      return result;
    }

    public static DisplayCheck check(String fieldName, DisplayEntry.Availability availability) {
      DisplayCheck result = new DisplayCheck("Availability", fieldName);
      result.setAvailability(availability);
      return result;
    }

    public static DisplayCheck checkValue(String fieldName, String fieldValue) {
      DisplayCheck result = new DisplayCheck("Value", fieldName);
      result.setFieldValue(fieldValue);
      return result;
    }

    public static DisplayCheck checkFieldValue(String fieldName, String fieldValue) {
      DisplayCheck result = new DisplayCheck("FieldValue", fieldName);
      result.setFieldValue(fieldValue);
      return result;
    }

    public String getCheckType() {
      return checkType;
    }

    public String getFieldName() {
      return fieldName;
    }

    public DisplayEntry.Visibility getVisibility() {
      return visibility;
    }

    public DisplayEntry.Availability getAvailability() {
      return availability;
    }

    public String getFieldValue() {
      return fieldValue;
    }

    public void setVisibility(DisplayEntry.Visibility visibility) {
      this.visibility = visibility;
    }

    public void setAvailability(DisplayEntry.Availability availability) {
      this.availability = availability;
    }

    public void setFieldValue(String fieldValue) {
      this.fieldValue = fieldValue;
    }

    public String getFieldLabel() {
      return fieldLabel;
    }

    public void setFieldLabel(String fieldLabel) {
      this.fieldLabel = fieldLabel;
    }

    public List<String> getLimitValues() {
      return limitValues;
    }

    public void setLimitValues(List<String> limitValues) {
      this.limitValues = limitValues;
    }

    public List<String> getExcludeValues() {
      return excludeValues;
    }

    public void setExcludeValues(List<String> excludeValues) {
      this.excludeValues = excludeValues;
    }
  }


  private List<String> testFieldNames = new ArrayList<String>();
  private int testCount = 0;
  private int errorCount = 0;

  public int getTestCount() {
    return testCount;
  }

  public int getErrorCount() {
    return errorCount;
  }

  private void assertRuleBase(Displayor displayor, String fieldname, JSObject jsObj, ICustomValue customData,
                              DisplayCheck expected) {
    List<DisplayCheck> expectedList = new ArrayList<DisplayCheck>();
    if (expected != null)
      expectedList.add(expected);
    assertRuleBase(displayor, fieldname, jsObj, customData, expectedList);
  }

  private void assertRuleBase(Displayor displayor, String fieldname, JSObject jsObj, ICustomValue customData,
                              List<DisplayCheck> expectedList) {
    boolean isNoEventExpected = true;
    String checkNames = null;
    for (DisplayCheck check : expectedList) {
      if (check != null) {
        isNoEventExpected = false;
        if (checkNames == null)
          checkNames = check.getCheckType();
        else
          checkNames += ", " + check.getCheckType();
      }
    }
    String testName = fieldname + " [" + checkNames +  "]";
    testFieldNames.add(fieldname);
    testCount++;

    Collection<DisplayEntry> entries;
    entries = displayor.runDisplayRule(fieldname, jsObj, customData);
    boolean rulesFound = entries.size() > 0;
    if (!rulesFound) {
      errorCount++;
      System.out.println("Failed Test: " + testName +  " - No matching rule found");
    }
    else {
      if (expectedList == null || isNoEventExpected) {
        if (entries.size() > 0) {
          errorCount++;
          System.out.println("Failed Test: " + testName + " - Expecting No Outcome, but rules were fired");
        }
      }
      else {
        if (entries.size() == expectedList.size()) {
          Iterator<DisplayEntry> entryIter = entries.iterator();
          Iterator<DisplayCheck> expectedIter = expectedList.iterator();
          while (entryIter.hasNext()) {
            DisplayEntry entry = entryIter.next();
            DisplayCheck expected = expectedIter.next();

            if (expected.getAvailability() != null && !expected.getAvailability().equals(entry.getAvailability())) {
              errorCount++;
              System.out.println("Failed Test: " + testName + " - Expecting " + expected.getAvailability().toString() + ", but instead got " + entry.getAvailability());
            }
            if (expected.getVisibility() != null && !expected.getVisibility().equals(entry.getVisibility())) {
              errorCount++;
              System.out.println("Failed Test: " + testName + " - Expecting " + expected.getVisibility().toString() + ", but instead got " + entry.getVisibility());
            }
            if (expected.getFieldName() != null && !expected.getFieldName().equals(entry.getFieldName())) {
              errorCount++;
              System.out.println("Failed Test: " + testName + " - Expecting field " + expected.getFieldName() + ", but instead field " + entry.getFieldName() + " was affected");
            }
            if (expected.getFieldValue() != null && !expected.getFieldValue().equals(entry.getFieldValue())) {
              errorCount++;
              System.out.println("Failed Test: " + testName + " - Expecting value '" + expected.getFieldValue() + "', but instead got value '" + entry.getFieldValue() + "'");
            }
          }
        }
        else {
          errorCount++;
          System.out.println("Failed Test: " + testName + " - Expecting " + expectedList.size() + " events, but instead got " + entries.size());
        }
      }
    }
  }

  public void assertShow(Displayor displayor, String fieldFilter, JSObject jsObj) {
    DefaultCustomValues customValues = new DefaultCustomValues();
    customValues.setDealerType("AD");

    assertRuleBase(displayor, fieldFilter, jsObj, customValues, DisplayCheck.check(fieldFilter, DisplayEntry.Visibility.Show));
  }

  public void assertHide(Displayor displayor, String fieldFilter, JSObject jsObj) {
    DefaultCustomValues customValues = new DefaultCustomValues();
    customValues.setDealerType("AD");

    assertRuleBase(displayor, fieldFilter, jsObj, customValues, DisplayCheck.check(fieldFilter, DisplayEntry.Visibility.Hide));
  }

  public void assertEnabled(Displayor displayor, String fieldFilter, JSObject jsObj) {
    DefaultCustomValues customValues = new DefaultCustomValues();
    customValues.setDealerType("AD");

    assertRuleBase(displayor, fieldFilter, jsObj, customValues, DisplayCheck.check(fieldFilter, DisplayEntry.Availability.Enabled));
  }

  public void assertDisabled(Displayor displayor, String fieldFilter, JSObject jsObj) {
    DefaultCustomValues customValues = new DefaultCustomValues();
    customValues.setDealerType("AD");

    assertRuleBase(displayor, fieldFilter, jsObj, customValues, DisplayCheck.check(fieldFilter, DisplayEntry.Availability.Disabled));
  }

  public void assertValue(Displayor displayor, String fieldFilter, String fieldValue, JSObject jsObj) {
    DefaultCustomValues customValues = new DefaultCustomValues();
    customValues.setDealerType("AD");

    assertRuleBase(displayor, fieldFilter, jsObj, customValues, DisplayCheck.checkValue(fieldFilter, fieldValue));
  }


  public void assertUnchangedValue(Displayor displayor, String fieldFilter, JSObject jsObj) {
    DefaultCustomValues customValues = new DefaultCustomValues();
    customValues.setDealerType("AD");

    assertRuleBase(displayor, fieldFilter, jsObj, customValues, (DisplayCheck)null);
  }

  public void assertFieldValue(Displayor displayor, String fieldFilter, String setFieldName, String fieldValue, JSObject jsObj) {
    DefaultCustomValues customValues = new DefaultCustomValues();
    customValues.setDealerType("AD");

    assertRuleBase(displayor, fieldFilter, jsObj, customValues, DisplayCheck.checkFieldValue(setFieldName, fieldValue));
  }

  public void assert2FieldValues(Displayor displayor, String fieldFilter, String setFieldName1, String fieldValue1,
                                 String setFieldName2, String fieldValue2, JSObject jsObj) {
    DefaultCustomValues customValues = new DefaultCustomValues();
    customValues.setDealerType("AD");

    List<DisplayCheck> checkList = new ArrayList<DisplayCheck>();
    checkList.add(DisplayCheck.checkFieldValue(setFieldName1, fieldValue1));
    checkList.add(DisplayCheck.checkFieldValue(setFieldName2, fieldValue2));
    assertRuleBase(displayor, fieldFilter, jsObj, customValues, checkList);
  }
}
