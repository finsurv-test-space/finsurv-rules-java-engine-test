package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.EvaluationUtil;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;

public class crCMABank implements IMatchAssertionFunction {
  @Override
  public boolean execute(MatchInfo matchInfo) {
    String BIC = matchInfo.getCrBankBIC();

    return EvaluationUtil.isCMAByBIC(matchInfo.getContext(), BIC);
  }
}