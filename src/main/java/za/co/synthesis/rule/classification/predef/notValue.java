package za.co.synthesis.rule.classification.predef;

import za.co.synthesis.rule.classification.ClassificationContext;
import za.co.synthesis.rule.classification.IClassificationFunction;
import za.co.synthesis.rule.core.IClassificationContext;
import za.co.synthesis.rule.support.StringList;

/**
 * Created by jake on 6/7/16.
 */
public class notValue implements IClassificationFunction {
  private StringList constants = null;

  public notValue(String value) {
    constants = new StringList();
    constants.add(value);
  }

  public notValue(StringList value) {
    this.constants = value;
  }

  public boolean execute(ClassificationContext context, Object value) {
    if (value != null) {
      return !constants.contains(value);
    }
    return true;
  }
}
