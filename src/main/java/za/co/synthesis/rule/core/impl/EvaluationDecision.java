package za.co.synthesis.rule.core.impl;

import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.evaluation.CompoundEvaluationDecision;
import za.co.synthesis.rule.evaluation.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 3/2/15
 * Time: 4:47 PM
 * The evaluation getDecision object that denotes a single evaluation getDecision
 */
public class EvaluationDecision implements IEvaluationDecision {
  private String reportingQualifier;
  private ReportableDecision decision;
  private ReportableType reportable;
  private FlowType flow;
  private DrCr reportingSide;
  private DrCr nonResSide;
  private ReportingAccountType nonResAccountType;
  private String nonResException;
  private DrCr resSide;
  private ReportingAccountType resAccountType;
  private String resException;
  private String subject;
  private String locationCountry;
  private String manualSection;
  private String drSideSwift;
  private AccountHolderStatus accStatusFilter;
  private List<String> category;
  private List<String> notCategory;
  private final List<BankAccountType> drAccountTypes = new ArrayList<BankAccountType>();
  private final List<BankAccountType> crAccountTypes = new ArrayList<BankAccountType>();
  private Context context;

  public EvaluationDecision(Context context, ReportableType reportable) {
    this.context = context;
    this.reportable = reportable;
    setDecisionAndQualifier(context, reportable);
  }

  public EvaluationDecision(Context context, ReportableType reportable, DrCr reportingSide) {
    this.context = context;
    this.reportable = reportable;
    this.reportingSide = reportingSide;
    setDecisionAndQualifier(context, reportable);
  }

  public EvaluationDecision(Context context, ReportableType reportable, DrCr reportingSide, FlowType flow,
                            DrCr resSide, ReportingAccountType resAccountType, String resException,
                            DrCr nonResSide, ReportingAccountType nonResAccountType, String nonResException,
                            String manualSection) {
    this.context = context;
    this.reportable = reportable;
    setDecisionAndQualifier(context, reportable);
    this.reportingSide = reportingSide;
    this.flow = flow;
    this.resSide = resSide;
    this.resAccountType = resAccountType;
    this.resException = resException;
    this.nonResSide = nonResSide;
    this.nonResAccountType = nonResAccountType;
    this.nonResException = nonResException;
    this.manualSection = manualSection;
  }

  public EvaluationDecision(Context context, EvaluationDecision decision, List<String> category, List<String> notCategory) {
    this.context = context;
    this.reportable = decision.reportable;
    setDecisionAndQualifier(context, reportable);
    this.flow = decision.flow;
    this.reportingSide = decision.reportingSide;
    this.nonResSide = decision.nonResSide;
    this.nonResAccountType = decision.nonResAccountType;
    this.nonResException = decision.nonResException;
    this.resSide = decision.resSide;
    this.resAccountType = decision.resAccountType;
    this.resException = decision.resException;
    this.subject = decision.subject;
    this.locationCountry = decision.locationCountry;
    this.manualSection = decision.manualSection;
    this.drSideSwift = decision.drSideSwift;
    this.accStatusFilter = decision.accStatusFilter;
    if (decision.category != null && category != null) {
      this.category = new ArrayList<String>();
      this.category.addAll(decision.category);
      this.category.addAll(category);
    }
    else {
      if (category != null)
        this.category = category;
      else
        this.category = decision.category;
    }
    if (decision.notCategory != null && notCategory != null) {
      this.notCategory = new ArrayList<String>();
      this.notCategory.addAll(decision.notCategory);
      this.notCategory.addAll(notCategory);
    }
    else {
      if (notCategory != null)
        this.notCategory = notCategory;
      else
        this.notCategory = decision.notCategory;
    }
  }

  public EvaluationDecision(Context context, CompoundEvaluationDecision decision, int drIndex, int crIndex) {
    this.context = context;
    reportable = decision.getReportable();
    setDecisionAndQualifier(context, decision.getReportable());
    reportingSide = decision.getReportingSide();
    category = decision.getCategory();
    notCategory = decision.getNotCategory();
    flow = decision.getFlow();
    subject = decision.getSubject();
    locationCountry = decision.getLocationCountry();
    manualSection = decision.getManualSection();
    drSideSwift = decision.getDrSideSwift();

    accStatusFilter = decision.getAccStatusFilter();

    if (decision.getNonResSide() != null) {
      if (decision.getNonResSide() == DrCr.DR && decision.getNonResAccountType().size() > 0) {
        nonResAccountType = decision.getNonResAccountType().get(drIndex);
      }
      else
      if (decision.getNonResSide() == DrCr.DR && decision.getNonResException().size() > 0) {
        nonResException = decision.getNonResException().get(drIndex);
      }
      else
      if (decision.getNonResSide() == DrCr.CR && decision.getNonResAccountType().size() > 0) {
        nonResAccountType = decision.getNonResAccountType().get(crIndex);
      }
      else
      if (decision.getNonResSide() == DrCr.CR && decision.getNonResException().size() > 0) {
        nonResException = decision.getNonResException().get(crIndex);
      }

      if (nonResException != null) {
        nonResSide = null;
      }
      else {
        nonResSide = decision.getNonResSide();
      }
    }
    else {
      if (decision.getNonResAccountType().size() == 1)
        nonResAccountType = decision.getNonResAccountType().get(0);
      if (decision.getNonResException().size() == 1)
        nonResException = decision.getNonResException().get(0);
    }

    if (decision.getResSide() != null) {
      if (decision.getResSide() == DrCr.DR && decision.getResAccountType().size() > 0) {
        resAccountType = decision.getResAccountType().get(drIndex);
      }
      else
      if (decision.getResSide() == DrCr.DR && decision.getResException().size() > 0) {
        resException = decision.getResException().get(drIndex);
      }
      else
      if (decision.getResSide() == DrCr.CR && decision.getResAccountType().size() > 0) {
        resAccountType = decision.getResAccountType().get(crIndex);
      }
      else
      if (decision.getResSide() == DrCr.CR && decision.getResException().size() > 0) {
        resException = decision.getResException().get(crIndex);
      }

      if (resException != null) {
        resSide = null;
      }
      else {
        resSide = decision.getResSide();
      }
    }
    else {
      if (decision.getResAccountType().size() == 1)
        resAccountType = decision.getResAccountType().get(0);
      if (decision.getResException().size() == 1)
        resException = decision.getResException().get(0);
    }
  }

  private void setDecisionAndQualifier(Context context, ReportableType reportable) {
    decision = ReportableDecision.Unknown;

    if (ReportableType.Reportable.equals(reportable)) {
      decision = ReportableDecision.ReportToRegulator;
      reportingQualifier = "BOPCUS";
    }
    else
    if (ReportableType.ZZ1Reportable.equals(reportable)) {
      decision = (context == null || context.getZz1Reportability()) ? ReportableDecision.ReportToRegulator : ReportableDecision.DoNotReport;
      reportingQualifier = "NON REPORTABLE";
    }
    else
    if (ReportableType.CARDResReportable.equals(reportable)) {
      decision = ReportableDecision.ReportToRegulator;
      reportingQualifier = "BOPCARD RESIDENT";
    }
    else
    if (ReportableType.CARDNonResReportable.equals(reportable)) {
      decision = ReportableDecision.ReportToRegulator;
      reportingQualifier = "BOPCARD NON RESIDENT";
    }
    else
    if (ReportableType.NotReportable.equals(reportable)) {
      decision = ReportableDecision.DoNotReport;
    }
    if (ReportableType.Illegal.equals(reportable)) {
      decision = ReportableDecision.Illegal;
    }
  }

  @Override
  public ReportableDecision getDecision() {
    return decision;
  }

  @Override
  public String getReportingQualifier() {
    return reportingQualifier;
  }

  private boolean objectsEqual(Object obj1, Object obj2) {
    if (obj1 == null && obj2 == null) {
      return true;
    }
    else {
      if (obj1 != null) {
        return obj1.equals(obj2);
      }
    }
    return false;
  }

  @Override
  public boolean equals(IEvaluationDecision other) {
    boolean result = false;

    if (//objectsEqual(scenario, other.getScenario()) &&
        objectsEqual(reportable, other.getReportable()) &&
        objectsEqual(flow, other.getFlow()) &&
        objectsEqual(reportingSide, other.getReportable()) &&
        objectsEqual(nonResSide, other.getNonResSide()) &&
        objectsEqual(nonResAccountType, other.getNonResAccountType()) &&
        objectsEqual(nonResException, other.getNonResException()) &&
        objectsEqual(resSide, other.getResSide()) &&
        objectsEqual(resAccountType, other.getResAccountType()) &&
        objectsEqual(resException, other.getResException()) &&
        objectsEqual(subject, other.getSubject()) &&
        objectsEqual(locationCountry, other.getLocationCountry())) {
      result = true;
    }
    return result;
  }

  @Override
  public List<BankAccountType> getPossibleDrAccountTypes() {
    return drAccountTypes;
  }

  @Override
  public List<BankAccountType> getPossibleCrAccountTypes() {
    return crAccountTypes;
  }

  /**
   * This returns if the transaction is reportable or not and if reportable then it determines the nature of the
   * ReportingQualifier under which the transaction should be reported.
   * @return @see za.co.synthesis.rule.core.ReportableType for what the various values mean
   */
  @Override
  public ReportableType getReportable() {
    return reportable;
  }

  /**
   * If reportable this returns the directional flow to use for the transaction
   * @return null if not reportable or the inflow.outflow direction for reporting
   */
  @Override
  public FlowType getFlow() {
    return flow;
  }

  /**
   * This defines which side of the transaction is responsible for reporting the transaction:
   * + Dr: The Debited party is responsible for providing the needed information
   * + Cr: The Credited party is responsible for providing the needed information
   * @return null if no particular party is responsible for supplying the needed information and the
   * Banking instituition will need to provide the information (if this turns out to be a reportable transaction)
   */
  @Override
  public DrCr getReportingSide() {
    return reportingSide;
  }

  /**
   * This defines which side of the transaction must use to populate the Non Resident information:
   * + Dr: Map non resident client information from the debit side of the transaction
   * + Cr: Map non resident client information from the credit side of the transaction
   * @return null if no mapping is required (like in the case where an exception will be used) or the side of the
   * transaction to map the non resident client information from
   */
  @Override
  public DrCr getNonResSide() {
    return nonResSide;
  }

  /**
   * This is the account type to use for the non resident (using SARB reporting account types) based on the given
   * bank account type and associated residence status
   * @return null if no account type is required (like in the case where an exception will be used) or the SARB
   * account type to use for the non resident
   */
  @Override
  public ReportingAccountType getNonResAccountType() {
    return nonResAccountType;
  }

  /**
   * This is set to a valid SARB exception if the non resident exception should be used. If the exception is set then
   * both the side and account type will not be specified
   * @return The name of the exception or null if no exception should be used
   */
  @Override
  public String getNonResException() {
    return nonResException;
  }

  /**
   * This defines which side of the transaction must use to populate the Resident information:
   * + Dr: Map resident client information from the debit side of the transaction
   * + Cr: Map resident client information from the credit side of the transaction
   * @return null if no mapping is required (like in the case where an exception will be used) or the side of the
   * transaction to map the resident client information from
   */
  @Override
  public DrCr getResSide() {
    return resSide;
  }

  /**
   * This is the account type to use for the resident (using SARB reporting account types) based on the given
   * bank account type and associated residence status
   * @return null if no account type is required (like in the case where an exception will be used) or the SARB
   * account type to use for the non resident
   */
  @Override
  public ReportingAccountType getResAccountType() {
    return resAccountType;
  }

  /**
   * This is set to a valid SARB exception if the resident exception should be used. If the exception is set then
   * both the side and account type will not be specified
   * @return The name of the exception or null if no exception should be used
   */
  @Override
  public String getResException() {
    return resException;
  }

  /**
   * This is set to a valid adhoc subject that must be used for the monetary amount on the transaction
   * @return The name of the adhoc subject that must be used or null if no specific adhoc subject need be used
   */
  @Override
  public String getSubject() {
    return subject;
  }

  /**
   * This is set to a location country that must be used for the monetary amount on the transaction
   * @return The name of the location country that must be used or null if any location country may be used
   */
  @Override
  public String getLocationCountry() {
    return locationCountry;
  }

  /**
   * This is the reference to the section in the SARB manual that contains this ruling
   * @return The reference to the section in the SARB manual
   */
  @Override
  public String getManualSection() { return manualSection; }

  @Override
  public String getDrSideSwift() { return drSideSwift; }

  public void setDrSideSwift(String drSideSwift) {
    this.drSideSwift = drSideSwift;
  }

  @Override
  public AccountHolderStatus getAccStatusFilter() {
    return accStatusFilter;
  }

  /**
   * This is a restricted list of category codes that must be used for this transaction. These categories can be used
   * to determine which evaluation getDecision to use for a given transaction where multiple evaluation decisions are
   * returned.
   * Any returned category implies all contained categories unless a sub category is specified e.g. '101' implies
   * 101/01, 101/02 ... 101/11
   * @return a list of categories applicable to this getDecision. If no categories are returned (an empty list) that all
   * categories are applicable
   */
  @Override
  public List<String> getCategory() {
    return category;
  }

  /**
   * This is a list of category codes that must NOT be used for this transaction. These categories can be used to
   * determine which evaluation getDecision to use for a given transaction where multiple evaluation decisions are returned.
   * Any returned category implies all contained categories unless a sub category is specified e.g. '101' implies
   * 101/01, 101/02 ... 101/11
   * @return a list of categories that are not applicable to this getDecision. If no entries are returned (an empty list)
   * than no categories are excluded
   */
  @Override
  public List<String> getNotCategory() {
    return notCategory;
  }

  @Override
  public boolean isZZ1Reportable() {
    return context !=null ? context.getZz1Reportability(): true;
  }

  @Override
  public Context getContext() {
    return context;
  }
}
