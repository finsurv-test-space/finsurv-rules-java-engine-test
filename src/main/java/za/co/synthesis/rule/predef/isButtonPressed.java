package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;

public class isButtonPressed implements IAssertionFunction {
  private IStringResolver field;

  public isButtonPressed(IStringResolver field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    return true;
  }
}