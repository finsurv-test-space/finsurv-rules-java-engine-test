package za.co.synthesis.rule.core;

/**
 * Created by jake on 6/16/16.
 */
public class MatchingException extends Exception {
  public MatchingException(String message) {
    super(message);
  }

  public MatchingException(String message, java.lang.Throwable throwable) {
    super(message, throwable);
  }
}

