package za.co.synthesis.rule.core;

import za.co.synthesis.rule.core.impl.EvaluationDecision;
import za.co.synthesis.rule.core.impl.EvaluationScenarioDecision;
import za.co.synthesis.rule.evaluation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 3/2/15
 * Time: 10:27 AM
 * This is the class that actually brings together the debit/credit data together with the evaluation rules to actually
 * evaluate the transaction to determine if a particular transaction is reportable or not.
 */
public class Evaluator {
  public final static String ACCOUNT_HOLDER_STATUS = "AccountHolderStatus";
  public final static String DOMESTIC_AMOUNT = "DomesticAmount";

  private final Context context;
  private final List<Evaluation> evaluations;
  private final List<Assumption> assumptions;
  private final List<Evaluations.DuplicateEvaluation> duplicateRules;

  public Evaluator(Context context, List<Evaluation> evaluations, List<Assumption> assumptions, List<Evaluations.DuplicateEvaluation> duplicateRules) {
    this.context = context;
    this.evaluations = evaluations;
    this.assumptions = assumptions;
    this.duplicateRules = duplicateRules;
  }

  public List<Evaluations.DuplicateEvaluation> getDuplicateRules() {
    return duplicateRules;
  }

  private List<IEvaluationDecision> reduceDecisionList(List<IEvaluationDecision> list) {
    List<IEvaluationDecision> result = new ArrayList<IEvaluationDecision>();

    for (IEvaluationDecision decList : list) {
      boolean bDuplicateFound = false;
      for (IEvaluationDecision decResult : result) {
        if (decResult.equals(decList)) {
          bDuplicateFound = true;
          for (BankAccountType bat : decList.getPossibleDrAccountTypes()) {
            if (!decResult.getPossibleDrAccountTypes().contains(bat))
              decResult.getPossibleDrAccountTypes().add(bat);
          }
          for (BankAccountType bat : decList.getPossibleCrAccountTypes()) {
            if (!decResult.getPossibleCrAccountTypes().contains(bat))
              decResult.getPossibleCrAccountTypes().add(bat);
          }

          for (String cat : decList.getCategory()) {
            if (!decResult.getCategory().contains(cat))
              decResult.getCategory().add(cat);
          }
          for (String notCat : decList.getNotCategory()) {
            if (!decResult.getNotCategory().contains(notCat))
              decResult.getNotCategory().add(notCat);
          }
          break;
        }
      }
      if (!bDuplicateFound) {
        result.add(decList);
      }
    }
    return result;
  }


  private IEvaluationDecision reduceDecisionsBasedOnMostOnerous(List<IEvaluationDecision> decisions) {
    IEvaluationDecision watermarkDecision = null;
    if (decisions != null) {
      int watermark = -1;
      for (IEvaluationDecision decision : decisions) {
        if (decision.getReportable().getOnerousScore() > watermark) {
          watermark = decision.getReportable().getOnerousScore();
          watermarkDecision = decision;
        }
      }
    }
    return watermarkDecision==null?new EvaluationDecision(context, ReportableType.NotReportable):watermarkDecision;
  }


  public List<IEvaluationDecision> evaluateRaw(ResidenceStatus drResStatus, BankAccountType drAccType,
                                            ResidenceStatus crResStatus, BankAccountType crAccType) {
    List<IEvaluationDecision> result = new ArrayList<IEvaluationDecision>();
    for (Evaluation rule : evaluations) {
      if (drResStatus == rule.getDrResStatus() &&
              (drAccType == BankAccountType.Unknown || drAccType == rule.getDrAccType()) &&
              crResStatus == rule.getCrResStatus() &&
              (crAccType == BankAccountType.Unknown || crAccType == rule.getCrAccType())) {
        EvaluationDecision decision = new EvaluationDecision(context, rule.getDecision(), rule.getCategory(), rule.getNotCategory());
        decision.getPossibleDrAccountTypes().add(rule.getDrAccType());
        decision.getPossibleCrAccountTypes().add(rule.getCrAccType());
        result.add(decision);
      }
    }
    if (result.size() == 0) {
      result.add(new EvaluationDecision(context, ReportableType.Unknown));
    }
    return reduceDecisionList(result);
  }

  private IEvaluationDecision firstDecisionOnDrCrSide(List<IEvaluationDecision> decisions, DrCr drCr) {
    assert(decisions!=null);
    if (drCr != null) {
      for (IEvaluationDecision decision : decisions) {
        if (decision.getReportingSide() != null && decision.getReportingSide() != DrCr.Unknown && drCr.equals(decision.getReportingSide())) {
          return decision;
        }
      }
    }
    return null;
  }

  private List<IEvaluationDecision> filterOnDrCrSide(List<IEvaluationDecision> decisions, DrCr drCr) {
    assert(decisions!=null);// should never be null, at least an unknown expected...
    List<IEvaluationDecision> result = new ArrayList<IEvaluationDecision>();
    result.addAll(decisions);
    List<IEvaluationDecision> removeDecisions = new ArrayList<IEvaluationDecision>();
    if (drCr != null) {
      for (IEvaluationDecision decision : result) {
        if (decision.getReportingSide() != null && decision.getReportingSide() != DrCr.Unknown && !drCr.equals(decision.getReportingSide())) {
          removeDecisions.add(decision);
        }
      }
      result.removeAll(removeDecisions);
    }
    // at this point, unknowns should have been have been dealt with, so we can assume full coverage - we need to generate the doNotReport
    if(result.size()==0){
      EvaluationDecision dnr = new EvaluationDecision(context, ReportableType.NotReportable);
      result.add(dnr);
    }
    return result;
  }

  private List<IEvaluationDecision> filterOnAccStatus(List<IEvaluationDecision> decisions, AccountHolderStatus accountHolderStatus) {
    List<IEvaluationDecision> result = new ArrayList<IEvaluationDecision>();
    result.addAll(decisions);
    List<IEvaluationDecision> removeDecisions = new ArrayList<IEvaluationDecision>();
    for(IEvaluationDecision decision: result) {
      if (accountHolderStatus != null && !accountHolderStatus.equals(AccountHolderStatus.Unknown)) {
        if (decision.getAccStatusFilter() != null &&
            !decision.getAccStatusFilter().equals(AccountHolderStatus.Unknown) &&
            !decision.getAccStatusFilter().equals(accountHolderStatus)) {
          removeDecisions.add(decision);
        }
      }
    }
    result.removeAll(removeDecisions);
    return result;
  }

  public CounterpartyInstituition getCounterpartyInstituitionByBIC(String bic) {
    return EvaluationUtil.getCounterpartyInstituitionByBIC(context, bic);
  }

  public List<IEvaluationDecision> evaluate(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                ResidenceStatus crResStatus, BankAccountType crAccType) {
    List<IEvaluationDecision> result = evaluateRaw(drResStatus,drAccType,crResStatus,crAccType);

    //In A scenario where the regulations require us to report a transaction as ZZ1 because the other
    // institution is responsible for doing the BOPCUS reporting then if we are in an OnUs scenario
    // there is no need to send the ZZ1 i.e. we have to send the BOPCUS
    List<IEvaluationDecision> removeDecisions = new ArrayList<IEvaluationDecision>();
    for(IEvaluationDecision decision: result) {
      if(decision.getReportable().equals(ReportableType.ZZ1Reportable)&&(decision.getDrSideSwift()!=null)){
        removeDecisions.add(decision);
      }
    }
    result.removeAll(removeDecisions);
    return result;
  }

  public List<IEvaluationDecision> evaluateOnUs(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                AccountHolderStatus drAccountHolderStatus,
                                                ResidenceStatus crResStatus, BankAccountType crAccType,
                                                AccountHolderStatus crAccountHolderStatus) {

    AccountHolderStatus accountHolderStatus = drAccountHolderStatus;
    if(crAccountHolderStatus != null &&
            (crAccType.equals(BankAccountType.CASH_LOCAL)||crAccType.equals(BankAccountType.LOCAL_ACC)))
      accountHolderStatus = crAccountHolderStatus;

    List<IEvaluationDecision> decisions = filterOnAccStatus(evaluate(drResStatus, drAccType, crResStatus, crAccType), accountHolderStatus);
    boolean bHasDr = false;
    boolean bHasCr = false;

    // If the transaction is Illegal then don't bother filling in NotReportable other side
    for (IEvaluationDecision decision : decisions) {
      if (decision.getReportable() != ReportableType.Illegal) {
        if (decision.getReportingSide() == DrCr.DR)
          bHasDr = true;
        if (decision.getReportingSide() == DrCr.CR)
          bHasCr = true;
      }
    }
    for (IEvaluationDecision decision : decisions) {
      if (decision.getReportable() == ReportableType.Illegal) {
        if (!bHasDr && !bHasCr) {
          bHasDr = true;
          bHasCr = true;
        }
      }
    }

    if(!(bHasCr||bHasDr)){
      decisions.add(new EvaluationDecision(context, ReportableType.NotReportable));
    } else {
      if (!bHasCr) {
        decisions.add(new EvaluationDecision(context, ReportableType.NotReportable, DrCr.CR));
      }
      if (!bHasDr) {
        decisions.add(new EvaluationDecision(context, ReportableType.NotReportable, DrCr.DR));
      }
    }

    return decisions;
  }

  private AccountHolderStatus getAccountHolderStatus(ResidenceStatus resStatus, BankAccountType accountHolderAccType,
                                                     Map<String, Object> additionalParams) {
    if (additionalParams != null) {
      if (additionalParams.containsKey(ACCOUNT_HOLDER_STATUS)) {
        Object objAccHolderStatus = additionalParams.get(ACCOUNT_HOLDER_STATUS);
        if (objAccHolderStatus instanceof AccountHolderStatus)
          return (AccountHolderStatus) objAccHolderStatus;
        if (objAccHolderStatus instanceof String) {
          String strAccHolderStatus = (String) objAccHolderStatus;
          if (strAccHolderStatus.startsWith("I") || strAccHolderStatus.startsWith("i"))
            return AccountHolderStatus.Individual;
          if (strAccHolderStatus.startsWith("E") || strAccHolderStatus.startsWith("e"))
            return AccountHolderStatus.Entity;
        }
      }
    }
    if (resStatus != null && (resStatus.equals(ResidenceStatus.HOLDCO) || resStatus.equals(ResidenceStatus.IHQ)))
      return AccountHolderStatus.Entity;
    if (accountHolderAccType != null && accountHolderAccType.equals(BankAccountType.CFC))
      return AccountHolderStatus.Entity;
    if (resStatus != null && accountHolderAccType != null &&
            resStatus.equals(ResidenceStatus.Resident) && accountHolderAccType.equals(BankAccountType.FCA))
      return AccountHolderStatus.Individual;
    return AccountHolderStatus.Unknown;
  }

  public IEvaluationScenarioDecision consolidatedEvaluation(
          String drBankBIC, ResidenceStatus drResStatus, BankAccountType drAccType,
          String drCurrency, String drField72,
          Map<String, Object> drAdditionalParams,
          String crBankBIC, ResidenceStatus crResStatus, BankAccountType crAccType, String crCurrency,
          Map<String, Object> crAdditionalParams,
          boolean sideFiltered, boolean mostOnerousFiltered) {
    MatchInfo matchInfo = new MatchInfo();
    matchInfo.setContext(context);
    matchInfo.setDrBankBIC(drBankBIC);
    matchInfo.setDrResStatus(drResStatus);
    matchInfo.setDrAccType(drAccType);
    matchInfo.setDrCurrency(drCurrency);
    matchInfo.setDrField72(drField72);
    matchInfo.setDrAdditionalParams(drAdditionalParams);

    matchInfo.setCrBankBIC(crBankBIC);
    matchInfo.setCrResStatus(crResStatus);
    matchInfo.setCrAccType(crAccType);
    matchInfo.setCrCurrency(crCurrency);
    matchInfo.setCrAdditionalParams(crAdditionalParams);

    for (Assumption assume : assumptions) {
      if (assume.getMatchAssertion().execute(matchInfo)) {
        for (AssumptionRule rule : assume.getRules()) {

          // Use underlying regulator evaluations rules: Use the provided parameters
          if (rule.getType().equals(AssumptionRuleType.UseBoth)) {
            AccountHolderStatus drAccountHolderStatus = getAccountHolderStatus(drResStatus, drAccType, drAdditionalParams);
            AccountHolderStatus crAccountHolderStatus = getAccountHolderStatus(crResStatus, crAccType, crAdditionalParams);
            List<IEvaluationDecision> decisions = evaluateOnUs(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
            if (mostOnerousFiltered) {
              List<IEvaluationDecision> filteredDecisions = new ArrayList<IEvaluationDecision>();
              IEvaluationDecision drRes = reduceDecisionsBasedOnMostOnerous(filterOnDrCrSide(decisions, DrCr.DR));
              IEvaluationDecision crRes = reduceDecisionsBasedOnMostOnerous(filterOnDrCrSide(decisions, DrCr.CR));
              if (drRes.getReportable().potentialReport()) {
                filteredDecisions.add(drRes);
              }
              if (crRes.getReportable().potentialReport()) {
                filteredDecisions.add(crRes);
              }
              if (filteredDecisions.size() == 0 && drRes != null) {
                filteredDecisions.add(drRes);
              }
              if (filteredDecisions.size() == 0 && crRes != null) {
                filteredDecisions.add(crRes);
              }
              return new EvaluationScenarioDecision(assume.getScenario(), "", filteredDecisions);
            }
            else {
              return new EvaluationScenarioDecision(assume.getScenario(), "", decisions);
            }
          }

          // Use underlying regulator evaluations rules: Use the CR side parameters provided in the rules
          if (rule.getType().equals(AssumptionRuleType.UseCr)) {
            AccountHolderStatus accountHolderStatus = getAccountHolderStatus(drResStatus, drAccType, drAdditionalParams);
            GivenInfo givenInfo = new GivenInfo();
            givenInfo.setContext(context);
            givenInfo.setCounterpartyInstituition(null);
            givenInfo.setCounterpartyInstituitionBIC(crBankBIC);
            givenInfo.setAccountHolderStatus(accountHolderStatus);
            givenInfo.setCurrency(crCurrency);
            givenInfo.setResidenceStatus(crResStatus);

            if (rule.getAssertion().execute(givenInfo)) {
              ResidenceStatus assumedCrResStatus = rule.getResStatus();
              BankAccountType assumedCrAccType = rule.getAccType();

              if (assumedCrResStatus == ResidenceStatus.Unknown) {
                List<IEvaluationDecision> decisions = new ArrayList<IEvaluationDecision>();
                decisions.add(new EvaluationDecision(context, ReportableType.Unknown));
                return new EvaluationScenarioDecision(assume.getScenario(), "No Cr side Residence Status defined", decisions);
              }
              List<IEvaluationDecision> decisions = filterOnAccStatus(evaluateRaw(drResStatus, drAccType, assumedCrResStatus, assumedCrAccType), accountHolderStatus);
              if (sideFiltered) {
                String information = "";
                IEvaluationDecision ignoredDecision = firstDecisionOnDrCrSide(decisions, DrCr.CR);
                if (ignoredDecision != null) {
                  if (ignoredDecision.getDecision() == ReportableDecision.ReportToRegulator) {
                    information = "The other bank must report this as '" + ignoredDecision.getReportable().getReportingQualifier() + "'";
                  }
                }
                List<IEvaluationDecision> sideFileredDecisions = filterOnDrCrSide(decisions, DrCr.DR);
                if (mostOnerousFiltered) {
                  return new EvaluationScenarioDecision(assume.getScenario(), information, reduceDecisionsBasedOnMostOnerous(sideFileredDecisions));
                }
                else {
                  return new EvaluationScenarioDecision(assume.getScenario(), information, sideFileredDecisions);
                }
              }
              else {
                if (mostOnerousFiltered) {
                  return new EvaluationScenarioDecision(assume.getScenario(), "", reduceDecisionsBasedOnMostOnerous(decisions));
                }
                else {
                  return new EvaluationScenarioDecision(assume.getScenario(), "", decisions);
                }
              }
            }
          }

          // Use underlying regulator evaluations rules: Use the DR side parameters provided in the rules
          if (rule.getType().equals(AssumptionRuleType.UseDr)) {
            AccountHolderStatus accountHolderStatus = getAccountHolderStatus(crResStatus, crAccType, crAdditionalParams);

            GivenInfo givenInfo = new GivenInfo();
            givenInfo.setContext(context);
            givenInfo.setCounterpartyInstituition(null);
            givenInfo.setCounterpartyInstituitionBIC(drBankBIC);
            givenInfo.setAccountHolderStatus(accountHolderStatus);
            givenInfo.setField72(drField72);
            givenInfo.setCurrency(drCurrency);
            givenInfo.setResidenceStatus(drResStatus);

            if (rule.getAssertion().execute(givenInfo)) {
              ResidenceStatus assumedDrResStatus = rule.getResStatus();
              BankAccountType assumedDrAccType = rule.getAccType();

              if (assumedDrResStatus == ResidenceStatus.Unknown) {
                List<IEvaluationDecision> decisions = new ArrayList<IEvaluationDecision>();
                decisions.add(new EvaluationDecision(context, ReportableType.Unknown));
                return new EvaluationScenarioDecision(assume.getScenario(), "No Dr side Residence Status defined", decisions);
              }
              List<IEvaluationDecision> decisions = filterOnAccStatus(evaluateRaw(assumedDrResStatus, assumedDrAccType, crResStatus, crAccType), accountHolderStatus);
              if (sideFiltered) {
                String information = "";
                IEvaluationDecision ignoredDecision = firstDecisionOnDrCrSide(decisions, DrCr.DR);
                if (ignoredDecision != null) {
                  if (ignoredDecision.getDecision() == ReportableDecision.ReportToRegulator) {
                    information = "The other bank must report this as '" + ignoredDecision.getReportable().getReportingQualifier() + "'";
                  }
                }
                List<IEvaluationDecision> sideFileredDecisions = filterOnDrCrSide(decisions, DrCr.CR);
                if (mostOnerousFiltered) {
                  return new EvaluationScenarioDecision(assume.getScenario(), information, reduceDecisionsBasedOnMostOnerous(sideFileredDecisions));
                }
                else {
                  return new EvaluationScenarioDecision(assume.getScenario(), information, sideFileredDecisions);
                }
              }
              else {
                if (mostOnerousFiltered) {
                  return new EvaluationScenarioDecision(assume.getScenario(), "", reduceDecisionsBasedOnMostOnerous(decisions));
                }
                else {
                  return new EvaluationScenarioDecision(assume.getScenario(), "", decisions);
                }
              }
            }
          }

          // Use given getDecision if the CR side parameters provided to the rules is evaluated as true
          if (rule.getType().equals(AssumptionRuleType.DecideCr)) {
            AccountHolderStatus accountHolderStatus = getAccountHolderStatus(drResStatus, drAccType, drAdditionalParams);

            GivenInfo givenInfo = new GivenInfo();
            givenInfo.setContext(context);
            givenInfo.setCounterpartyInstituition(null);
            givenInfo.setCounterpartyInstituitionBIC(crBankBIC);
            givenInfo.setAccountHolderStatus(accountHolderStatus);
            givenInfo.setCurrency(crCurrency);

            if (rule.getAssertion().execute(givenInfo)) {
              List<IEvaluationDecision> decisions = new ArrayList<IEvaluationDecision>();
              decisions.add(new EvaluationDecision(context, rule.getDecision(), null, null));
              return new EvaluationScenarioDecision(assume.getScenario(), "", decisions);
            }
          }

          // Use given getDecision if the DR side parameters provided to the rules is evaluated as true
          if (rule.getType().equals(AssumptionRuleType.DecideDr)) {
            AccountHolderStatus accountHolderStatus = getAccountHolderStatus(crResStatus, crAccType, crAdditionalParams);

            GivenInfo givenInfo = new GivenInfo();
            givenInfo.setContext(context);
            givenInfo.setCounterpartyInstituition(null);
            givenInfo.setCounterpartyInstituitionBIC(drBankBIC);
            givenInfo.setAccountHolderStatus(accountHolderStatus);
            givenInfo.setField72(drField72);
            givenInfo.setCurrency(drCurrency);

            if (rule.getAssertion().execute(givenInfo)) {
              List<IEvaluationDecision> decisions = new ArrayList<IEvaluationDecision>();
              decisions.add(new EvaluationDecision(context, rule.getDecision(), null, null));
              return new EvaluationScenarioDecision(assume.getScenario(), "", decisions);
            }
          }
        }
      }
    }
    return new EvaluationScenarioDecision("Unknown", "No scenario matches with the given parameters", new ArrayList<IEvaluationDecision>());
  }

  public List<IEvaluationDecision> evaluateUnknownCrSide(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                         CounterpartyInstituition counterpartyInstituition, String counterpartyInstituitionBIC,
                                                         String destinationCurrency,
                                                         AccountHolderStatus accountHolderStatus) {
    Map<String, Object> drAdditionalParams = new HashMap<String, Object>();
    if (accountHolderStatus != null) {
      drAdditionalParams.put(ACCOUNT_HOLDER_STATUS, accountHolderStatus);
    }
    IEvaluationScenarioDecision scenario = consolidatedEvaluation(
            Context.THISBANK, drResStatus, drAccType,
            null, null,
            drAdditionalParams,
            counterpartyInstituitionBIC, null, null, destinationCurrency,
            null,
            true, false);
    return scenario.getDecisions();

    /* Old Implementation
    ResidenceStatus crResStatus = ResidenceStatus.Unknown;
    BankAccountType crAccType = BankAccountType.Unknown;
    GivenInfo givenInfo = new GivenInfo();
    givenInfo.setContext(context);
    givenInfo.setCounterpartyInstituition(counterpartyInstituition);
    givenInfo.setCounterpartyInstituitionBIC(counterpartyInstituitionBIC);
    givenInfo.setAccountHolderStatus(accountHolderStatus);
    givenInfo.setCurrency(destinationCurrency);

    for (Assumption assume : assumptions) {
      if (assume.isKnownSide(DrCr.DR)) {
        if (assume.getResStatus().equals(drResStatus) && assume.getAccType().contains(drAccType)) {
          // We now need to run all the rules to see what we can assume about the CR side
          for (AssumptionRule rule : assume.getRules()) {
            if (rule.getAssertion().execute(givenInfo)) {
              if (crResStatus == ResidenceStatus.Unknown &&
                      (rule.getType() == AssumptionRuleType.Use || rule.getType() == AssumptionRuleType.UseCr))
                crResStatus = rule.getResStatus();

              if (crAccType == BankAccountType.Unknown &&
                      (rule.getType() == AssumptionRuleType.Use || rule.getType() == AssumptionRuleType.UseCr))
                crAccType = rule.getAccType();
            }
          }
        }
      }
    }

    if (crResStatus == ResidenceStatus.Unknown) {
      List<IEvaluationDecision> result = new ArrayList<IEvaluationDecision>();
      result.add(new EvaluationDecision(context, ReportableType.Unknown));
      return result;
    }

    List<IEvaluationDecision> decisions = filterOnAccStatus(evaluateRaw(drResStatus, drAccType, crResStatus, crAccType), accountHolderStatus);
    if (decisions != null && decisions.size() > 1) {
      IEvaluationDecision removeDecision = null;
      for (IEvaluationDecision getDecision : decisions) {
        if (getDecision.getReportingSide() == DrCr.CR)
          removeDecision = getDecision;
      }
      if (removeDecision != null) {
        decisions.remove(removeDecision);
      }
    }
    return decisions;*/
  }


  public List<IEvaluationDecision> evaluateUnknownDrSide(ResidenceStatus crResStatus, BankAccountType crAccType,
                                                         CounterpartyInstituition counterpartyInstituition,  String counterpartyInstituitionBIC,
                                                         String sourceCurrency,
                                                         AccountHolderStatus accountHolderStatus,
                                                         String field72) {
    Map<String, Object> crAdditionalParams = new HashMap<String, Object>();
    if (accountHolderStatus != null) {
      crAdditionalParams.put(ACCOUNT_HOLDER_STATUS, accountHolderStatus);
    }
    IEvaluationScenarioDecision scenario = consolidatedEvaluation(
            counterpartyInstituitionBIC, null, null,
            sourceCurrency, field72,
            null,
            Context.THISBANK, crResStatus, crAccType, null,
            crAdditionalParams,
            true, false);
    return scenario.getDecisions();
/* Old Implementation
    ResidenceStatus drResStatus = ResidenceStatus.Unknown;
    BankAccountType drAccType = BankAccountType.Unknown;
    GivenInfo givenInfo = new GivenInfo();
    givenInfo.setContext(context);
    givenInfo.setCounterpartyInstituition(counterpartyInstituition);
    givenInfo.setCounterpartyInstituitionBIC(counterpartyInstituitionBIC);
    givenInfo.setAccountHolderStatus(accountHolderStatus);
    givenInfo.setCurrency(sourceCurrency);
    givenInfo.setField72(field72);

    for (Assumption assume : assumptions) {
      if (assume.isKnownSide(DrCr.CR)) {
        if (assume.getResStatus().equals(crResStatus) && assume.getAccType().contains(crAccType)) {
          // We now need to run all the rules to see what we can assume about the CR side
          for (AssumptionRule rule : assume.getRules()) {
            if (rule.getAssertion().execute(givenInfo)) {
              if (drResStatus == ResidenceStatus.Unknown &&
                      (rule.getType() == AssumptionRuleType.Use || rule.getType() == AssumptionRuleType.UseDr))
                drResStatus = rule.getResStatus();

              if (drAccType == BankAccountType.Unknown &&
                      (rule.getType() == AssumptionRuleType.Use || rule.getType() == AssumptionRuleType.UseDr))
                drAccType = rule.getAccType();
            }
          }
        }
      }
    }

    if (drResStatus == ResidenceStatus.Unknown) {
      List<IEvaluationDecision> result = new ArrayList<IEvaluationDecision>();
      result.add(new EvaluationDecision(context, ReportableType.Unknown));
      return result;
    }

    List<IEvaluationDecision> decisions = filterOnAccStatus(evaluateRaw(drResStatus, drAccType, crResStatus, crAccType), accountHolderStatus);
    if (decisions != null && decisions.size() > 1) {
      IEvaluationDecision removeDecision = null;
      for (IEvaluationDecision getDecision : decisions) {
        if (getDecision.getReportingSide() == DrCr.DR)
          removeDecision = getDecision;
      }
      if (removeDecision != null) {
        decisions.remove(removeDecision);
      }
    }
    return decisions;
*/
  }


  public List<IEvaluationDecision> evaluateCustomerOnUs(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                        AccountHolderStatus drAccountHolderStatus,
                                                        ResidenceStatus crResStatus, BankAccountType crAccType,
                                                        AccountHolderStatus crAccountHolderStatus) {
    return evaluateOnUs(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
  }



  public List<IEvaluationDecision> evaluateCustomerOnUsFiltered(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                        AccountHolderStatus drAccountHolderStatus,
                                                        ResidenceStatus crResStatus, BankAccountType crAccType,
                                                        AccountHolderStatus crAccountHolderStatus) {
    List<IEvaluationDecision> decisions = evaluateOnUs(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    List<IEvaluationDecision> result = new ArrayList<IEvaluationDecision>();
    IEvaluationDecision drRes = reduceDecisionsBasedOnMostOnerous(filterOnDrCrSide(decisions, DrCr.DR));
    IEvaluationDecision crRes = reduceDecisionsBasedOnMostOnerous(filterOnDrCrSide(decisions, DrCr.CR));
    if (drRes.getReportable().potentialReport()){
      result.add(drRes);
    }
    if (crRes.getReportable().potentialReport()){
      result.add(crRes);
    }
    if (result.size() == 0 && drRes!= null) {
      result.add(drRes);
    }
    if (result.size() == 0 && crRes!= null) {
      result.add(crRes);
    }
    return result;
  }


  public IEvaluationDecision evaluateCustomerOnUsDrOnerous(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                        AccountHolderStatus drAccountHolderStatus,
                                                        ResidenceStatus crResStatus, BankAccountType crAccType,
                                                        AccountHolderStatus crAccountHolderStatus) {
    List<IEvaluationDecision> decisions = evaluateOnUs(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    IEvaluationDecision result = reduceDecisionsBasedOnMostOnerous(filterOnDrCrSide(decisions, DrCr.DR));
    return result;
  }


  public IEvaluationDecision evaluateCustomerOnUsCrOnerous(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                        AccountHolderStatus drAccountHolderStatus,
                                                        ResidenceStatus crResStatus, BankAccountType crAccType,
                                                        AccountHolderStatus crAccountHolderStatus) {
    List<IEvaluationDecision> decisions = evaluateOnUs(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    IEvaluationDecision result = reduceDecisionsBasedOnMostOnerous(filterOnDrCrSide(decisions, DrCr.CR));
    return result;
  }


  public List<IEvaluationDecision> evaluateCustomerOnUsDrFiltered(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                        AccountHolderStatus drAccountHolderStatus,
                                                        ResidenceStatus crResStatus, BankAccountType crAccType,
                                                        AccountHolderStatus crAccountHolderStatus) {
    List<IEvaluationDecision> decisions = evaluateOnUs(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    return filterOnDrCrSide(decisions, DrCr.DR);
  }

  public List<IEvaluationDecision> evaluateCustomerOnUsCrFiltered(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                        AccountHolderStatus drAccountHolderStatus,
                                                        ResidenceStatus crResStatus, BankAccountType crAccType,
                                                        AccountHolderStatus crAccountHolderStatus) {
    List<IEvaluationDecision> decisions = evaluateOnUs(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    return filterOnDrCrSide(decisions, DrCr.CR);
  }

  public IEvaluationDecision evaluateCustomerPayment(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                     CounterpartyInstituition beneficiaryBankStatus, String destinationCurrency,
                                                     AccountHolderStatus accountHolderStatus) {
    List<IEvaluationDecision> decisions = evaluateUnknownCrSide(drResStatus, drAccType, beneficiaryBankStatus, null, destinationCurrency, accountHolderStatus);
    return reduceDecisionsBasedOnMostOnerous(filterOnDrCrSide(decisions, DrCr.DR));
  }

  public List<IEvaluationDecision> evaluateCustomerPaymentEx(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                     CounterpartyInstituition beneficiaryBankStatus, String destinationCurrency,
                                                     AccountHolderStatus accountHolderStatus) {
    List<IEvaluationDecision> decisions = evaluateUnknownCrSide(drResStatus, drAccType, beneficiaryBankStatus, null, destinationCurrency, accountHolderStatus);
    return decisions;
  }

  public List<IEvaluationDecision> evaluateCustomerPaymentDrFiltered(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                     CounterpartyInstituition beneficiaryBankStatus, String destinationCurrency,
                                                     AccountHolderStatus accountHolderStatus) {
    List<IEvaluationDecision> decisions = evaluateUnknownCrSide(drResStatus, drAccType, beneficiaryBankStatus, null, destinationCurrency, accountHolderStatus);
    return filterOnDrCrSide(decisions, DrCr.DR);
  }

  public IEvaluationDecision evaluateCustomerPayment(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                     String beneficiaryBIC, String destinationCurrency,
                                                     AccountHolderStatus accountHolderStatus) {
    List<IEvaluationDecision> decisions = evaluateUnknownCrSide(drResStatus, drAccType, null, beneficiaryBIC, destinationCurrency, accountHolderStatus);

    return reduceDecisionsBasedOnMostOnerous(filterOnDrCrSide(decisions, DrCr.DR));
  }

  public List<IEvaluationDecision> evaluateCustomerPaymentEx(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                     String beneficiaryBIC, String destinationCurrency,
                                                     AccountHolderStatus accountHolderStatus) {
    return evaluateUnknownCrSide(drResStatus, drAccType, null, beneficiaryBIC, destinationCurrency, accountHolderStatus);
  }

  public List<IEvaluationDecision> evaluateCustomerPaymentDrFiltered(ResidenceStatus drResStatus, BankAccountType drAccType,
                                                     String beneficiaryBIC, String destinationCurrency,
                                                     AccountHolderStatus accountHolderStatus) {
    return filterOnDrCrSide(evaluateUnknownCrSide(drResStatus, drAccType, null, beneficiaryBIC, destinationCurrency, accountHolderStatus), DrCr.DR);
  }

  public IEvaluationDecision evaluateCustomerReceipt(ResidenceStatus crResStatus, BankAccountType crAccType,
                                                     CounterpartyInstituition orderingBankStatus, String sourceCurrency,
                                                     AccountHolderStatus accountHolderStatus,
                                                     String field72) {
    List<IEvaluationDecision> decisions = evaluateUnknownDrSide(crResStatus, crAccType, orderingBankStatus, null, sourceCurrency, accountHolderStatus, field72);
    return reduceDecisionsBasedOnMostOnerous(filterOnDrCrSide(decisions, DrCr.CR));
  }

  public List<IEvaluationDecision> evaluateCustomerReceiptEx(ResidenceStatus crResStatus, BankAccountType crAccType,
                                                     CounterpartyInstituition orderingBankStatus, String sourceCurrency,
                                                     AccountHolderStatus accountHolderStatus,
                                                     String field72) {
    return evaluateUnknownDrSide(crResStatus, crAccType, orderingBankStatus, null, sourceCurrency, accountHolderStatus, field72);
  }

  public List<IEvaluationDecision> evaluateCustomerReceiptCrFiltered(ResidenceStatus crResStatus, BankAccountType crAccType,
                                                     CounterpartyInstituition orderingBankStatus, String sourceCurrency,
                                                     AccountHolderStatus accountHolderStatus,
                                                     String field72) {
    return filterOnDrCrSide(evaluateUnknownDrSide(crResStatus, crAccType, orderingBankStatus, null, sourceCurrency, accountHolderStatus, field72), DrCr.CR);
  }

  public IEvaluationDecision evaluateCustomerReceipt(ResidenceStatus crResStatus, BankAccountType crAccType,
                                                     String orderingBIC, String sourceCurrency,
                                                     AccountHolderStatus accountHolderStatus,
                                                     String field72) {
    List<IEvaluationDecision> decisions = evaluateUnknownDrSide(crResStatus, crAccType, null, orderingBIC, sourceCurrency, accountHolderStatus, field72);
    return reduceDecisionsBasedOnMostOnerous(filterOnDrCrSide(decisions, DrCr.CR));
  }
  
  public List<IEvaluationDecision> evaluateCustomerReceiptEx(ResidenceStatus crResStatus, BankAccountType crAccType,
                                                     String orderingBIC, String sourceCurrency,
                                                     AccountHolderStatus accountHolderStatus,
                                                     String field72) {
    return evaluateUnknownDrSide(crResStatus, crAccType, null, orderingBIC, sourceCurrency, accountHolderStatus, field72);
  }

  public List<IEvaluationDecision> evaluateCustomerReceiptCrFiltered(ResidenceStatus crResStatus, BankAccountType crAccType,
                                                     String orderingBIC, String sourceCurrency,
                                                     AccountHolderStatus accountHolderStatus,
                                                     String field72) {
    return filterOnDrCrSide(evaluateUnknownDrSide(crResStatus, crAccType, null, orderingBIC, sourceCurrency, accountHolderStatus, field72), DrCr.CR);
  }

  public IEvaluationDecision evaluateBankPayment(String beneficiaryBIC, String destinationCurrency) {
    IEvaluationScenarioDecision scenario = consolidatedEvaluation(
            Context.THISBANK, null, BankAccountType.NOSTRO,
            null, null,
            null,
            beneficiaryBIC, null, null, destinationCurrency,
            null,
            false, false);

    if (scenario.getDecisions().size() == 1) {
      return scenario.getDecisions().get(0);
    }
    else {
      return new EvaluationDecision(context, ReportableType.NotReportable, null, null,
              null, null, null,
              null, null, null,
              scenario.getScenario());
    }

    /* Old Implementation
    if (beneficiaryBIC.matches(context.getWhoAmIRegex())) {
      return new EvaluationDecision(context, ReportableType.NotReportable, DrCr.DR, null,
              null, null, null,
              null, null, null,
              "Banking payments made to self are not reportable");
    }

    if (!beneficiaryBIC.matches(context.getOnshoreRegex()) ||
        !destinationCurrency.matches(context.getLocalCurrencyRegex())) {
      return new EvaluationDecision(context, ReportableType.ZZ1Reportable, DrCr.DR, FlowType.Outflow,
              DrCr.DR, ReportingAccountType.RE_OTH, null,
              DrCr.CR, ReportingAccountType.NR_OTH, null,
              "All interbank transactions made to foreign banks or foreign currency transactions are reportable");
    }
    else {
      return new EvaluationDecision(context, ReportableType.NotReportable, DrCr.DR, null,
              null, null, null,
              null, null, null,
              "Banking payments made to local banks in local currencies are not reportable");
    }
    */
  }

  public IEvaluationDecision evaluateBankReceipt(String orderingBIC, String sourceCurrency) {
    IEvaluationScenarioDecision scenario = consolidatedEvaluation(
            orderingBIC, null, BankAccountType.NOSTRO,
            sourceCurrency, null,
            null,
            Context.THISBANK, null, BankAccountType.NOSTRO, null,
            null,
            false, false);

    if (scenario.getDecisions().size() == 1) {
      return scenario.getDecisions().get(0);
    }
    else {
      return new EvaluationDecision(context, ReportableType.NotReportable, null, null,
              null, null, null,
              null, null, null,
              scenario.getScenario());
    }

    /* Old Implementation
    if (orderingBIC.matches(context.getWhoAmIRegex())) {
      return new EvaluationDecision(context, ReportableType.NotReportable, DrCr.CR, null,
              null, null, null,
              null, null, null,
              "Banking payments received from self are not reportable");
    }

    if (!orderingBIC.matches(context.getOnshoreRegex()) ||
            !sourceCurrency.matches(context.getLocalCurrencyRegex())) {
      return new EvaluationDecision(context, ReportableType.ZZ1Reportable, DrCr.CR, FlowType.Inflow,
              DrCr.CR, ReportingAccountType.RE_OTH, null,
              DrCr.DR, ReportingAccountType.NR_OTH, null,
              "All interbank transactions received from foreign banks or in foreign currency are reportable");
    }
    else {
      return new EvaluationDecision(context, ReportableType.NotReportable, DrCr.CR, null,
              null, null, null,
              null, null, null,
              "Banking payments received from local banks in local currencies are not reportable");
    }
    */
  }

  public IEvaluationDecision evaluateCorrespondentBank(String beneficiaryInstitutionBIC,
                                                       String orderingInstitutionBIC,
                                                       OrderingCustomerType orderingCustomer) {
    if (beneficiaryInstitutionBIC.matches(context.getWhoAmIRegex()) ||
        orderingInstitutionBIC.matches(context.getWhoAmIRegex()) ) {
      return new EvaluationDecision(context, ReportableType.Illegal, null, null,
              null, null, null,
              null, null, null,
              "Correspondent banking transactions must involve other banking institutions (i.e. not us)");
    }

    if (!orderingInstitutionBIC.matches(context.getOnshoreRegex()) &&
         beneficiaryInstitutionBIC.matches(context.getOnshoreRegex())) { // Overseas Bank to Local Bank (via us)
      EvaluationDecision eval = new EvaluationDecision(context, ReportableType.ZZ1Reportable, DrCr.DR, FlowType.Inflow,
              DrCr.CR, ReportingAccountType.RE_OTH, null,
              DrCr.DR, ReportingAccountType.NR_OTH, null,
              "Instruct beneficiary bank to report appropriately based on type of customer in field 72");
      if (orderingCustomer == OrderingCustomerType.Bank)
        eval.setDrSideSwift("NTNRB");
      if (orderingCustomer == OrderingCustomerType.NonBank)
        eval.setDrSideSwift("NTNRC");
      return eval;
    }

    String reason;
    if (!beneficiaryInstitutionBIC.matches(context.getOnshoreRegex())) {
      reason = "Ordering institution is required to do the reporting and not us";
    }
    else {
      // Ordering institution is Local
      reason = "Correspondent banking transactions involving local institutions need not be reported";
    }

    return new EvaluationDecision(context, ReportableType.NotReportable, null, null,
            null, null, null,
            null, null, null,
            reason);
  }

  public IEvaluationDecision evaluateCARD(String issuingCountry, String useCountry) {
    if (issuingCountry == null || useCountry == null) {
      return new EvaluationDecision(context, ReportableType.Unknown, null, null,
              null, null, null,
              null, null, null,
              "Information not provided");
    }
    boolean bIssuingLocal = issuingCountry.matches(context.getLocalCountryRegex());
    boolean bUseLocal = useCountry.matches(context.getLocalCountryRegex());

    if (bIssuingLocal && !bUseLocal) {
      return new EvaluationDecision(context, ReportableType.CARDResReportable, DrCr.DR, FlowType.Outflow,
              null, null, null,
              null, null, null,
              "Locally issued card used overseas");
    }
    else
    if (!bIssuingLocal && bUseLocal) {
      return new EvaluationDecision(context, ReportableType.CARDNonResReportable, DrCr.CR, FlowType.Inflow,
              null, null, null,
              null, null, null,
              "Overseas issued card used locally");
    }
    else
    if (bIssuingLocal) {
      return new EvaluationDecision(context, ReportableType.NotReportable, null, null,
              null, null, null,
              null, null, null,
              "Locally issued card used locally");
    }
    return new EvaluationDecision(context, ReportableType.NotReportable, null, null,
            null, null, null,
            null, null, null,
            "Overseas issued card used overseas");
  }
}
