package za.co.synthesis.rule.difference;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.rule.support.ResourceUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DSLDifferenceOutput {
  private static final String CHANNEL_LIST = "_channels";
  private static final String CHANNEL_START = "<!--Start Channel-->";
  private static final String CHANNEL_END = "<!--End Channel-->";
  private static final String CHANNEL_NAME = "[[Channel.Name]]";
  private static final String CHANNEL_BODY = "[[Channel Body]]";

  private static final String FIELD_CHANGES_HEADER = "Field Changes";
  private static final String RULE_CHANGES_HEADER = "Rule Changes";
  private static final String MAPPING_CHANGES_HEADER = "Package Mapping Changes";
  private static final String MESSAGE_CHANGES_HEADER = "Message Changes";
  private static final String DOCUMENT_CHANGES_HEADER = "Document Changes";
  private static final String VAL_TEST_CHANGES_HEADER = "Test Changes - Validation";


  private static enum Part {
    Prefix,
    Channel,
    Suffix
  }

  private static String getHTMLTemplatePart(final String template, final Part part) {
    int posStart = template.indexOf(CHANNEL_START);
    int posEnd = -1;
    if (posStart >= 0) {
      posEnd = template.indexOf(CHANNEL_END, posStart);
      if (posEnd == -1) {
        posStart = -1;
      }
    }

    if (part == Part.Prefix) {
      if (posStart >= 0) {
        return template.substring(0, posStart);
      }
    }
    else if (part == Part.Channel) {
      if (posStart >= 0) {
        return template.substring(posStart + CHANNEL_START.length(), posEnd);
      }
    }
    else if (part == Part.Suffix) {
      if (posEnd >= 0) {
        return template.substring(posEnd + CHANNEL_END.length());
      }
    }
    return "";
  }

  private static void outputHTML(String templatePath, String htmlOutputPath, JSObject jsDoc) throws IOException {
    String template = ResourceUtils.getResourceAsString(templatePath);
    String htmlOutputDir = Paths.get(htmlOutputPath).getParent().toString().replace("\\", "/");

    Files.createDirectories(Paths.get(htmlOutputDir));
    FileWriter html = new FileWriter(htmlOutputPath);
    html.write(getHTMLTemplatePart(template, Part.Prefix));

    JSArray channelList = (JSArray) jsDoc.get(CHANNEL_LIST);
    String channelPart = getHTMLTemplatePart(template, Part.Channel);

    for (Object channelName : channelList) {
      JSObject jsChannel = (JSObject) jsDoc.get(channelName.toString());
      String rule_changes = getRuleChanges(jsChannel);
      String mapping_changes = getChanges(jsChannel, "package");
      String message_changes = getChanges(jsChannel, "message");
      String field_changes = getChanges(jsChannel, "field");
      String document_changes = getDocumentChanges(jsChannel);
      String test_validation_changes = getChanges(jsChannel, "validation_test");

      if (hasChanges(rule_changes, mapping_changes, message_changes, field_changes, document_changes)) {
        String channelHTML = channelPart.replace(CHANNEL_NAME, channelName.toString());
        String channelBodyHtml = "";

        if (!rule_changes.isEmpty())
          channelBodyHtml +=  getHtmlBody(RULE_CHANGES_HEADER, rule_changes);
        if (!mapping_changes.isEmpty())
          channelBodyHtml +=  getHtmlBody(MAPPING_CHANGES_HEADER, mapping_changes);
        if (!message_changes.isEmpty())
          channelBodyHtml +=  getHtmlBody(MESSAGE_CHANGES_HEADER, message_changes);
        if (!field_changes.isEmpty())
          channelBodyHtml +=  getHtmlBody(FIELD_CHANGES_HEADER, field_changes);
        if (!document_changes.isEmpty())
          channelBodyHtml +=  getHtmlBody(DOCUMENT_CHANGES_HEADER, document_changes);
        if (!test_validation_changes.isEmpty())
          channelBodyHtml +=  getHtmlBody(VAL_TEST_CHANGES_HEADER, test_validation_changes);

        channelHTML = channelHTML.replace(CHANNEL_BODY, channelBodyHtml);
        html.write(channelHTML.replace(CHANNEL_BODY, channelBodyHtml));
      }
    }

    html.write(getHTMLTemplatePart(template, Part.Suffix));
    html.close();
  }

  private static boolean hasChanges(String...changes) {
      for (String change : changes) {
          if (!change.isEmpty())
              return true;
      }
      return false;
  }

  private static String getHtmlBody(String sectionHeader, String changeDetail) {
    return "<p><b>" + sectionHeader + ":</b>" + "<pre>" + changeDetail + "</pre>";
  }

  private static String getDocumentChanges(JSObject jsChannel) {
    JSArray jsList;
    String lastHeading;
    StringBuilder changes = new StringBuilder();
    jsList = (JSArray) jsChannel.get("document");
    lastHeading = "";
    if (jsList != null) {
      for (Object obj : jsList) {
        if (obj instanceof JSObject) {
          JSObject jsObj = (JSObject) obj;
          String heading = "Scope: " + JSUtil.getString(jsObj, "newValue.scope") + ",  fields: " + JSUtil.getString(jsObj, "newValue.fields");
          if (!lastHeading.equals(heading)) {
            lastHeading = heading;
            changes.append("<b><u>").append(heading).append("</u></b>\n");
          }
          changes.append(jsObj.get("diffHTML").toString());
          changes.append("\n");
        }
      }
    }
    return changes.toString();
  }

  private static String getRuleChanges(JSObject jsChannel) {
    StringBuilder changes = new StringBuilder();
    JSArray jsList = (JSArray) jsChannel.get("rule");
    String lastHeading = "";
    if (jsList != null) {
      for (Object obj : jsList) {
        if (obj instanceof JSObject) {
          JSObject jsObj = (JSObject) obj;
          String heading = "Scope: " + JSUtil.getString(jsObj, "newValue.scope") + ",  fields: " + JSUtil.getString(jsObj, "newValue.fields");
          if (!lastHeading.equals(heading)) {
            lastHeading = heading;
            changes.append("<b><u>").append(heading).append("</u></b>\n");
          }
          changes.append(jsObj.get("diffHTML").toString());
          changes.append("\n");
        }
      }
    }
    return changes.toString();
  }

  private static String getChanges(JSObject jsChannel, String type) {
    StringBuilder changes = new StringBuilder();
    JSArray jsList = (JSArray) jsChannel.get(type);
    if (jsList != null) {
      for (Object obj : jsList) {
        if (obj instanceof JSObject) {
          JSObject jsObj = (JSObject) obj;
          changes.append(jsObj.get("diffHTML").toString());
          changes.append("\n");
        }
      }
    }
    return changes.toString();
  }

  public static void main (String[] args) throws Exception {
    String oldRepoPath;
    String newRepoPath;
    String htmlOutputPath;
    String templatePath;

    if (args.length < 3) {
      System.out.println("Usage: oldRepoPath newRepoPath htmlOutputPath [templatePath]");
      System.out.println("Where:");
      System.out.println(" - oldRepoPath: the path to the old rule repo");
      System.out.println(" - newRepoPath: the path to the new rule repo");
      System.out.println(" - htmlOutputPath: the output file to write out the html difference document to");
      System.out.println(" - [templatePath]: optional. (Default value: classpath:config/DSLDifferenceTemplate.html)");
      return;
    }
    oldRepoPath = args[0];
    newRepoPath = args[1];
    htmlOutputPath = args[2];
    templatePath = "classpath:config/DSLDifferenceTemplate.html";
    if (args.length >= 4) {
      templatePath = args[3];
    }

    repoDifferences(oldRepoPath, newRepoPath, htmlOutputPath, templatePath);
  }

  private static String[] getChannels(String path) {
    File file = new File(path);
    String[] directories = file.list(new FilenameFilter() {
      @Override
      public boolean accept(File current, String name) {
        if (name.startsWith(".")) // e.g. .git
          return false;
        return new File(current, name).isDirectory();
      }
    });
    return directories;
  }

  public static JSObject channelDifferences(String oldRepoPath, String newRepoPath, String channel) throws Exception {
    DSLChannelDifferences diffs = new DSLChannelDifferences(oldRepoPath, newRepoPath);

    JSObject jsDoc = new JSObject();
    JSArray jsChannels = new JSArray();
    jsDoc.put(CHANNEL_LIST, jsChannels);

    jsChannels.add(channel);
    try {
      diffs.addValidationDifferences(channel, jsDoc);
      diffs.addDocumentDifferences(channel, jsDoc);
    }
    catch (Exception e) {
      // Ignore channels that have errors loading their rules
    }
    return jsDoc;
  }

  public static JSObject repoDifferences(String oldRepoPath, String newRepoPath) throws Exception {
    DSLChannelDifferences diffs = new DSLChannelDifferences(oldRepoPath, newRepoPath);

    JSObject jsDoc = new JSObject();
    JSArray jsChannels = new JSArray();
    jsDoc.put(CHANNEL_LIST, jsChannels);

    String[] channels = getChannels(oldRepoPath);
    for (String channel : channels) {
      jsChannels.add(channel);

      try {
        diffs.addValidationDifferences(channel, jsDoc);
        diffs.addDocumentDifferences(channel, jsDoc);
        diffs.addValidationTestDifferences(channel, jsDoc);
      }
      catch (Exception e) {
        // Ignore channels that have errors loading their rules
      }
    }

    return jsDoc;
  }

  public static void repoDifferences(String oldRepoPath, String newRepoPath, String htmlOutputPath, String templatePath) throws Exception {
    DSLChannelDifferences diffs = new DSLChannelDifferences(oldRepoPath, newRepoPath);

    JSObject jsDoc = DSLDifferenceOutput.repoDifferences(oldRepoPath, newRepoPath);
    JSWriter writer = new JSWriter();
    writer.setIndent("  ");
    writer.setNewline("\n");

    jsDoc.compose(writer);
    System.out.println(writer);

    outputHTML(templatePath, htmlOutputPath, jsDoc);
  }
}
