package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 11:40 AM
 * Determines if the nonresident exception name matches the given constant
 */
public class hasNonResException implements IAssertionFunction {
  private String constant;

  public hasNonResException(String constant) {
    this.constant = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object exception = context.getTransactionField("NonResident.Exception.ExceptionName");
    if (exception != null)
      return exception.equals(constant);
    return false;
  }
}
/*
function hasNonResException(type) {
  return function(context, value) {
    var exception = context.getTransactionField("NonResident.Exception.ExceptionName");
    if (exception)
      return exception === type;
    return false;
  }
}
*/