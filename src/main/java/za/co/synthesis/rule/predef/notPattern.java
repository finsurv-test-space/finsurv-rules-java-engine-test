package za.co.synthesis.rule.predef;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jake
 * Date: 8/5/14
 * Time: 11:04 PM
 * Checks to see that the value does not have the given pattern
 */
public class notPattern implements IAssertionFunction {
  private Pattern pattern;

  public notPattern(String regex) {
    this.pattern = Pattern.compile(regex);
  }

  public notPattern(JSRegExLiteral regex) {
    this.pattern = regex.getPattern();
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (value == null)
      return true;

    Matcher matcher = pattern.matcher(value.toString());
    return !matcher.find();
  }
}
/*
function notPattern(pattern) {
  return function(context, value) {
    return !value || !value.match(pattern);
  }
}
*/