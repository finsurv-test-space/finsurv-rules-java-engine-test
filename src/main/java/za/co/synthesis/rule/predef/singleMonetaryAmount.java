package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jaco
 * Date: 4/30/21
 * Time: 17:20 PM
 * Determine if there is a single import/export entries for the current monetary amount
 */
public class singleMonetaryAmount implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return context.getMoneySize() == 1;
  }
}
/*
function singleMonetaryAmount(context, value) {
  return context.getMoneySize() == 1;
}
*/


