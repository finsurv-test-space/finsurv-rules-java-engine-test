package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jake
 * Date: 8/5/14
 * Time: 10:56 PM
 * Checks to see that the value has a valid date pattern
 */
public class notDatePattern implements IAssertionFunction {
  private Pattern pattern;

  public notDatePattern() {
    this.pattern = Pattern.compile("^(19|20|21)\\d{2}-(0\\d|10|11|12)-(0[1-9]|1\\d|2\\d|3[01])$");
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (value == null)
      return true;

    Matcher matcher = pattern.matcher(value.toString());
    return !matcher.matches();
  }
}
/*
function notDatePattern(context, value) {
  return !value || !value.match(/^(19|20|21)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/);
}
*/