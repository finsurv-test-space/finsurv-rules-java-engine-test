package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.matching.IMatchingFunction;
import za.co.synthesis.rule.matching.MatchingContext;
import za.co.synthesis.rule.support.StringList;

/**
 * Created by jake on 6/16/16.
 */
public class notValue implements IMatchingFunction {
  private final String fieldname;
  private StringList constants = null;

  public notValue(String fieldname, String value) {
    this.fieldname = fieldname;
    this.constants = new StringList();
    this.constants.add(value);
  }

  public notValue(String fieldname, StringList value) {
    this.fieldname = fieldname;
    this.constants = value;
  }

  public boolean execute(MatchingContext context) {
    Object valueLhs = context.getFieldLhs(fieldname);

    if (valueLhs != null) {
      return !constants.contains(valueLhs.toString());
    }
    return true;
  }
}
