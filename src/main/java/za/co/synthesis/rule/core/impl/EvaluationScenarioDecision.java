package za.co.synthesis.rule.core.impl;

import za.co.synthesis.rule.core.IEvaluationDecision;
import za.co.synthesis.rule.core.IEvaluationScenarioDecision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EvaluationScenarioDecision implements IEvaluationScenarioDecision {
  private final List<IEvaluationDecision> decisions;
  private final String scenario;
  private final String information;

  public EvaluationScenarioDecision(String scenario, String information, List<IEvaluationDecision> decisions) {
    this.decisions = decisions;
    this.scenario = scenario;
    this.information = information;
  }

  public EvaluationScenarioDecision(String scenario, String information, IEvaluationDecision decision) {
    this.decisions = new ArrayList<IEvaluationDecision>();
    this.decisions.add(decision);
    this.scenario = scenario;
    this.information = information;
  }

  public EvaluationScenarioDecision(String scenario, String information) {
    this.decisions = new ArrayList<IEvaluationDecision>();
    this.scenario = scenario;
    this.information = information;
  }

  public List<IEvaluationDecision> getDecisions() {
    return decisions;
  }

  public String getScenario() {
    return scenario;
  }

  public String getInformation() {
    return information;
  }
}
