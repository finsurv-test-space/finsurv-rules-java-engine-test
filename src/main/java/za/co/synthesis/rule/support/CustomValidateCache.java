package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.IFinsurvContextCache;
import za.co.synthesis.rule.core.ValidationStats;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * User: jake
 * Date: 2/26/15
 * Time: 10:42 AM
 * A cache of all the user validate results based on the given input parameters. This is NOT a synchronised object
 * and therefore the cache MUST only be accessed by a single thread at a time. It is envisioned that the cache will
 * be attached to a Validator object.
 */
public class CustomValidateCache {
  public static class ResponseCache extends HashMap<String, Object> {}

  public static class ValidateInputs {
    private final int hashCode;
    private final Object value;
    private final Object[] otherInputs;

    private Object stringifyInput(Object input) {
      if (input instanceof String)
        return input;

      if (input instanceof BigDecimal)
        return ((BigDecimal)input).toPlainString();

      if (input instanceof Boolean)
        return ((Boolean)input).toString();

      return input;
    }

    public ValidateInputs(Object value, Object[] otherInputs) {
      this.value = value;
      if (otherInputs != null) {
        this.otherInputs = new Object[otherInputs.length];
        for (int i = 0; i < otherInputs.length; i++) {
          this.otherInputs[i] = stringifyInput(otherInputs[i]);
        }
      }
      else {
        this.otherInputs = null;
      }
      this.hashCode = hashInputs(this.value, this.otherInputs);
    }

    public Object getValue() {
      return value;
    }

    public Object[] getOtherInputs() {
      return otherInputs;
    }

    public int hashCode() {
      return hashCode;
    }

    private int hashInputs (Object value, Object[] inputs) {
      int result = value != null ? value.hashCode() : 0;
      if (inputs != null) {
        for (int i = 0; i < inputs.length; i++) {
          if (inputs[i] != null) {
            result = result ^ inputs[i].hashCode();
          }
        }
      }
      return result;
    }

    private boolean areObjectsTheSame(Object obj1, Object obj2) {
      if (obj1 != null) {
        if (!obj1.equals(obj2))
          return false;
      }
      else {
        // Out validateName is null so if the other.validateName is not null then not the same
        if (obj2 != null)
          return false;
      }
      return true;
    }

    public boolean equals(ValidateInputs other) {
      if (other != null) {
        if (!areObjectsTheSame(value, other.value)) {
          return false;
        }
        int myInputSize = (otherInputs != null) ? otherInputs.length : 0;
        int otherInputSize = (other.otherInputs != null) ? other.otherInputs.length : 0;
        if (myInputSize != otherInputSize) {
          return false;
        }
        if (myInputSize > 0) {
          for (int i = 0; i<myInputSize; i++) {
            if (!areObjectsTheSame(otherInputs[i], other.otherInputs[i])) {
              return false;
            }
          }
        }
      }
      else {
        return false;
      }
      return true;
    }
  }

  private final CentralCustomValidationCache centralCache;
  private ValidationStats stats;
  private FinsurvContext context;

  public CustomValidateCache(CentralCustomValidationCache centralCache) {
    this.centralCache = centralCache;
  }

  public CustomValidateCache(CustomValidateRegistry registry) {
    this.centralCache = new CentralCustomValidationCache(registry);
  }
  public FinsurvContext getContext() {
    return context;
  }

  public void setContext(FinsurvContext context) {
    this.context = context;
  }

  public ValidationStats getStats() {
    return stats;
  }

  public void setStats(ValidationStats stats) {
    this.stats = stats;
  }

  public void updateContextCache() {
    IFinsurvContextCache contextCache = context.getValidationCache();
    List<CentralCustomValidationCache.ValidationCacheEntry> entries =
            centralCache.getTransactionValidationCacheList(context);
    if (entries != null && contextCache != null) {
      contextCache.clearCache();
      for (CentralCustomValidationCache.ValidationCacheEntry entry : entries) {
        contextCache.marshalValidate(entry);
      }
    }
  }

  public void cacheResult(String name, ValidateInputs inputs, CustomValidateResult result, ResponseCache responseCache, Long setExpiryMillis) {
    centralCache.cacheResult(context, name, inputs, result, responseCache, setExpiryMillis);
    if (stats != null) {
      stats.setValidationCachePuts(stats.getValidationCachePuts() + 1);
    }
  }

  public CustomValidateResult getResult(String name, ValidateInputs inputs) {
    CustomValidateResult result = centralCache.getResultAndAssociateContext(context, name, inputs);
    if (stats != null && result != null) {
      stats.setValidationCacheHits(stats.getValidationCacheHits()+1);
    }
    return result;
  }

  public CustomValidateResult getResult(String name, Object value, Object[] otherInputs) {
    CustomValidateResult result = centralCache.getResultAndAssociateContext(context, name, value, otherInputs);
    if (stats != null && result != null) {
      stats.setValidationCacheHits(stats.getValidationCacheHits()+1);
    }
    return result;
  }

  public ResponseCache getResponse(String name, ValidateInputs inputs) {
    return centralCache.getResponse(name, inputs);
  }
}
