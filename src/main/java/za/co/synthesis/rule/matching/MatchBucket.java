package za.co.synthesis.rule.matching;

import java.util.List;

/**
 * Created by jake on 6/16/16.
 */
public class MatchBucket {
  private final String name;
  private final IShardFunction shard;

  public MatchBucket(String name, IShardFunction shard) {
    this.name = name;
    this.shard = shard;
  }

  public String getName() {
    return name;
  }

  public IShardFunction getShard() {
    return shard;
  }
}
