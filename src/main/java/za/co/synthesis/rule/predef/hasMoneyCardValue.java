package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 1:36 PM
 * Determines if the transaction has a card value
 */
public class hasMoneyCardValue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    BigDecimal total = new BigDecimal(0);
    BigDecimal c1value = Util.number(context.getMoneyField(context.getCurrentMoneyInstance(), context.map("ForeignCardHoldersPurchases{{LocalValue}}")));
    BigDecimal c2value = Util.number(context.getMoneyField(context.getCurrentMoneyInstance(), context.map("ForeignCardHoldersCashWithdrawals{{LocalValue}}")));
    if (c1value != null)
      total = total.add(c1value);
    if (c2value != null)
      total = total.add(c2value);

    total = total.multiply(new BigDecimal(100));
    return total.longValue() != 0;
  }
}
/*
function hasMoneyCardValue(context, value) {
  var total = 0;
  var c1value = context.getMoneyField(context.currentMoneyInstance, "ForeignCardHoldersPurchasesRandValue");
  var c2value = context.getMoneyField(context.currentMoneyInstance, "ForeignCardHoldersCashWithdrawalsRandValue");
  if (c1value)
    total += Number(c1value);
  if (c2value)
    total += Number(c2value);
  var cents = Math.round(total * 100);
  return cents != 0;
}
*/