package za.co.synthesis;

import org.junit.Test;
import za.co.synthesis.javascript.JSIdentifier;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSWriter;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class ParseInterestingJS {
  @Test
  public void parseAssertion() throws Exception {
    String snippet = "notEmpty.and(hasMoneyFieldValue(map(\"{{Regulator}}Auth.AuthIssuer\"), \"THIS BANK\").or(hasMoneyFieldValue(map(\"{{Regulator}}Auth.AuthIssuer\"), \"OTHER BANK\")))";
    //String snippet = "notEmpty";
    JSStructureParser parser = new JSStructureParser(snippet);

    Object obj = parser.parseValue();

    JSWriter writer = new JSWriter();
    if (obj instanceof JSIdentifier) {
      JSIdentifier jsObj = (JSIdentifier)obj;
      jsObj.compose(writer);

      System.out.println(writer.toString());
    }
  }


  @Test
  public void parseExtraCommas() throws Exception {
    FileReader fileReader = new FileReader("./src/test/resources/extraCommas.js");
    JSStructureParser parser = new JSStructureParser(fileReader);

    Object obj = parser.parse();

    JSWriter writer = new JSWriter();
    if (obj instanceof JSObject) {
      JSObject jsObj = (JSObject)obj;
      jsObj.compose(writer);

      System.out.println(writer.toString());
    }
  }

  @Test
  public void parseComment() throws Exception {
    FileReader fileReader = new FileReader("./src/test/resources/comments.js");
    JSStructureParser parser = new JSStructureParser(fileReader);

    Object obj = parser.parse();

    JSWriter writer = new JSWriter();
    if (obj instanceof JSObject) {
      JSObject jsObj = (JSObject)obj;
      jsObj.compose(writer);

      System.out.println(writer.toString());
    }
  }

  @Test
  public void parseExample() throws Exception {
    FileReader fileReader = new FileReader("./src/test/resources/example.json");
    JSStructureParser parser = new JSStructureParser(fileReader);

    Object obj = parser.parse();

    JSWriter writer = new JSWriter();
    if (obj instanceof JSObject) {
      JSObject jsObj = (JSObject)obj;
      jsObj.compose(writer);

      System.out.println(writer.toString());
    }
  }
}
