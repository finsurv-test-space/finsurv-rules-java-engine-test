package za.co.synthesis.rule.classification;

/**
 * Created by jake on 6/7/16.
 */
public class ClassificationInclude extends ClassificationRule {
  public ClassificationInclude(String name, IClassificationFunction assertion) {
    super("include", name, assertion);
  }
}
