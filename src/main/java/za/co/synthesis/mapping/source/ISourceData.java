package za.co.synthesis.mapping.source;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 11:33 AM
 * All source data objects are required to implement this interface. The external configuration objects are set up by
 * the source manager and the format is implementation dependant. E.g. in the case of a CSV file or database table
 * the external configuration may simply be the column index. Since the mapping will be to JSON objects all data must
 * be converted to a string representation
 */
public interface ISourceData {
  int getRowCount();
  int getRowCount(int indexLvl1);
  int getLevel();

  String getDataPoint(Object externalConfig);
  String getDataPoint(int indexLvl1, Object externalConfig);
  String getDataPoint(int indexLvl1, int indexLvl2, Object externalConfig);
}
