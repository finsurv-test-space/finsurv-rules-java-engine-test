package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.Util;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 3:12 PM
 * Determines if the current value is not a valid postal code
 */
public class notValidPostalCode implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !Util.isValidPostalCode(value.toString());
  }
}

/*
function notValidPostalCode(context, value) {
  if (!value)
    return true;

  var regPostcode = /^([0-9]){1,4}$/;

  if (regPostcode.test(value) == true) {
    return false;
  }
  return true;
}
*/