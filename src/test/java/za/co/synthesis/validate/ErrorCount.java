package za.co.synthesis.validate;

import java.util.*;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 9:40 PM
 * This class is used to keep track of the errors that the SARB is reporting that Synthesis is not. The point is to
 * highlight the areas that Synthesis must concentrate on to bridge the gap in the rule implementation
 */
public class ErrorCount {
  public static class Entry {
    private String errorCode;
    private String errorDesc;
    private String attribute;
    private Integer count;

    public Entry(String errorCode, String errorDesc, String attribute) {
      this.errorCode = errorCode;
      this.errorDesc = errorDesc;
      this.attribute = attribute;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Entry entry = (Entry) o;

      if (errorCode != null ? !errorCode.equals(entry.errorCode) : entry.errorCode != null) return false;
      if (errorDesc != null ? !errorDesc.equals(entry.errorDesc) : entry.errorDesc != null) return false;
      if (attribute != null ? !attribute.equals(entry.attribute) : entry.attribute != null) return false;

      return true;
    }

    @Override
    public int hashCode() {
      int result = errorCode != null ? errorCode.hashCode() : 0;
      result = 31 * result + (errorDesc != null ? errorDesc.hashCode() : 0);
      result = 31 * result + (attribute != null ? attribute.hashCode() : 0);
      return result;
    }

    public String getErrorCode() {
      return errorCode;
    }

    public String getErrorDesc() {
      return errorDesc;
    }

    public String getAttribute() {
      return attribute;
    }

    public Integer getCount() {
      return count;
    }

    public void setCount(Integer count) {
      this.count = count;
    }
  }

  private final List<Entry> entries = new ArrayList<Entry>();
  private final Map<Entry, List<String>> entryReferences = new HashMap<Entry, List<String>>();

  public void addError(String errorCode, String errorDesc, String attribute, String reference) {
    Entry entry = new Entry(errorCode, errorDesc, attribute);

    int index = entries.indexOf(entry);
    if (index >= 0) {
      entry = entries.get(index);
      entry.setCount(entry.getCount() + 1);
    }
    else {
      entry.setCount(1);
      entries.add(entry);
    }
    if(entryReferences.containsKey(entry)) {
      List<String> list = entryReferences.get(entry);
      if (list.size() < 10) {
        list.add(reference);
      }
    }
    else {
      List<String> list = new ArrayList<String>();
      list.add(reference);
      entryReferences.put(entry, list);
    }
  }

  public void print(int top, int examples) {
    Collections.sort(entries, new Comparator<Entry>() {
      @Override
      public int compare(Entry o1, Entry o2) {
        return o2.getCount().compareTo(o1.getCount());
      }
    });

    for (Entry entry : entries) {
      StringBuilder sb = new StringBuilder();
      sb.append("Count: ").append(entry.getCount()).append("; Code: ").append(entry.getErrorCode());
      sb.append("; Desc: ").append(entry.getErrorDesc()).append("; Attrib: ").append(entry.getAttribute());
      sb.append("; Examples: ");
      List<String> references = entryReferences.get(entry);
      int i = examples;
      for (String ref : references) {
        if (i < examples)
          sb.append(",");
        sb.append(ref);
        if (i-- <= 0)
          break;
      }

      System.out.println(sb.toString());
      if (--top == 0)
        break;
    }
  }

  public void printAsDelimited(int top, int examples, String delimiter) {
    Collections.sort(entries, new Comparator<Entry>() {
      @Override
      public int compare(Entry o1, Entry o2) {
        return o2.getCount().compareTo(o1.getCount());
      }
    });

    StringBuilder sb = new StringBuilder();
    sb.append("Count").append(delimiter).append("Code").append(delimiter);
    sb.append("Description").append(delimiter).append("Field").append(delimiter);
    sb.append("Example(s)");
    System.out.println(sb.toString());

    for (Entry entry : entries) {
      sb = new StringBuilder();
      sb.append(entry.getCount()).append(delimiter).append(entry.getErrorCode());
      sb.append(delimiter).append(entry.getErrorDesc());
      sb.append(delimiter).append(entry.getAttribute().replace(".", " "));
      sb.append(delimiter);
      List<String> references = entryReferences.get(entry);
      int i = examples;
      for (String ref : references) {
        if (i < examples)
          sb.append(", ");
        sb.append(ref);
        if (i-- <= 0)
          break;
      }

      System.out.println(sb.toString());
      if (--top == 0)
        break;
    }
  }
}
