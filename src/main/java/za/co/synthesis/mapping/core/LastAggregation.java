package za.co.synthesis.mapping.core;

import java.util.List;

/**
 * User: james
 * Date: 12/19/16
 * Time: 11:48 AM
 * Finds the last value in the list
 */
public class LastAggregation implements IAggregation {
  @Override
  public String aggregate(List<String> values) {
    if (values.size() > 0) {
      return values.get(values.size()-1);
    }
    return null;
  }
}
