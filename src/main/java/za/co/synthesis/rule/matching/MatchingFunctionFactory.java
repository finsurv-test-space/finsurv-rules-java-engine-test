package za.co.synthesis.rule.matching;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.support.StringList;

import java.lang.reflect.Constructor;

/**
 * Created by jake on 6/16/16.
 */
public class MatchingFunctionFactory {
  private static final String functionPackage = "za.co.synthesis.rule.matching.predef.";

  public static IMatchingFunction createFunction(final String name) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    return (IMatchingFunction)cls.newInstance();
  }

  public static IMatchingFunction createFunction(final String name, final String param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class);
    return (IMatchingFunction)clsConstructor.newInstance(param1);
  }

  public static IMatchingFunction createFunction(final String name, final JSRegExLiteral param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(JSRegExLiteral.class);
    return (IMatchingFunction)clsConstructor.newInstance(param1);
  }

  public static IMatchingFunction createFunction(final String name, final StringList param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(StringList.class);
    return (IMatchingFunction)clsConstructor.newInstance(param1);
  }

  public static IMatchingFunction createFunction(final String name, final String param1, final String param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class);
    return (IMatchingFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IMatchingFunction createFunction(final String name, final String param1, final StringList param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, StringList.class);
    return (IMatchingFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IMatchingFunction createFunction(final String name, final IMatchingFunction param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IMatchingFunction.class);
    return (IMatchingFunction)clsConstructor.newInstance(param1);
  }

  public static IMatchingFunction createFunction(final String name, final String param1, final IMatchingFunction param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, IMatchingFunction.class);
    return (IMatchingFunction)clsConstructor.newInstance(param1, param2);
  }
}
