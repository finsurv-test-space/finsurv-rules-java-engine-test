package za.co.synthesis.csv;

import za.co.synthesis.rule.core.ResultEntry;
import za.co.synthesis.rule.core.ResultType;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * User: jake
 * Date: 1/26/16
 * Time: 3:28 AM
 * Store the output of the validation engine and the original validation feedback for later analysis. The output is
 * stored in a csv file where with the following columns:
 * reference, type [O]ld;[N]ew, code, description, attribute, sequence, sub sequence
 *
 */
public class CsvOutput {
  public static enum ValidationType {
    OldError,
    NewError,
    OldWarning,
    NewWarning,
    OldSuccess,
    NewSuccess
  }

  public static class ValidationEntry {
    private String reference;
    private ValidationType validationType;
    private String code;
    private String description;
    private String attribute;
    private int sequence;
    private int subSequence;
    private String comment;

    public ValidationEntry(String reference, ValidationType validationType, String code, String description, String attribute, int sequence, int subSequence, String comment) {
      this.reference = reference;
      this.validationType = validationType;
      this.code = code;
      this.description = description;
      this.attribute = attribute;
      this.sequence = sequence;
      this.subSequence = subSequence;
      this.comment = comment;
    }

    public ValidationEntry(String reference, ValidationType validationType, String code, String description, String attribute, String comment) {
      this.reference = reference;
      this.validationType = validationType;
      this.code = code;
      this.description = description;
      this.attribute = attribute;
      this.sequence = 0;
      this.subSequence = 0;
      this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      ValidationEntry that = (ValidationEntry) o;

      if (sequence != that.sequence) return false;
      if (subSequence != that.subSequence) return false;
      if (attribute != null ? !attribute.equals(that.attribute) : that.attribute != null) return false;
      if (!code.equals(that.code)) return false;
      if (!description.equals(that.description)) return false;
      if (!reference.equals(that.reference)) return false;
      if (validationType != that.validationType) return false;

      return true;
    }

    @Override
    public int hashCode() {
      int result = reference.hashCode();
      result = 31 * result + validationType.hashCode();
      result = 31 * result + code.hashCode();
      result = 31 * result + description.hashCode();
      result = 31 * result + (attribute != null ? attribute.hashCode() : 0);
      result = 31 * result + sequence;
      result = 31 * result + subSequence;
      return result;
    }

    public String getReference() {
      return reference;
    }

    public ValidationType getValidationType() {
      return validationType;
    }

    public String getCode() {
      return code;
    }

    public String getDescription() {
      return description;
    }

    public String getAttribute() {
      return attribute;
    }

    public int getSequence() {
      return sequence;
    }

    public int getSubSequence() {
      return subSequence;
    }

    public String getComment() {
      return comment;
    }

    public static String getValidationTypeStr(ValidationType type) {
      String result = "";
      if (type != null) {
        if (type == ValidationType.OldError)
          result = "OE";
        else if (type == ValidationType.OldWarning)
          result = "OW";
        else if (type == ValidationType.NewError)
          result = "NE";
        else if (type == ValidationType.NewWarning)
          result = "NW";
        else if (type == ValidationType.OldSuccess)
          result = "OS";
        else if (type == ValidationType.NewSuccess)
          result = "NS";
      }
      return result;
    }

    public static ValidationType getValidationType(String type) {
      ValidationType result = null;
      if (type != null) {
        if (type.equals("OE"))
          result = ValidationType.OldError;
        else if (type.equals("OW"))
          result = ValidationType.OldWarning;
        else if (type.equals("NE"))
          result = ValidationType.NewError;
        else if (type.equals("NW"))
          result = ValidationType.NewWarning;
        else if (type.equals("OS"))
          result = ValidationType.OldSuccess;
        else if (type.equals("NS"))
          result = ValidationType.NewSuccess;
      }
      return result;
    }
  }

  private PrintWriter filePrinter = null;
  private String referenceForBuffer;
  private final List<ValidationEntry> entryBuffer = new ArrayList<ValidationEntry>();

  public void openForWrite(String fileName) throws IOException {
    filePrinter = new PrintWriter(new BufferedWriter(new FileWriter(fileName, false)));
  }

  public void close() {
    if (filePrinter != null) {
      flushBuffer();
      filePrinter.close();
      filePrinter = null;
    }
  }

  public void flushBuffer() {
    if (entryBuffer.size() > 0) {
      Collections.sort(entryBuffer, new Comparator<ValidationEntry>() {
        @Override
        public int compare(ValidationEntry o1, ValidationEntry o2) {
          if (o1.validationType != o2.validationType) {
            return o1.validationType.compareTo(o2.validationType);
          }
          if (o1.equals(o2))
            return 0;
          return (o1.hashCode() > o2.hashCode() ? 1 : -1);
        }
      });

      for (ValidationEntry entry : entryBuffer) {
        StringBuilder sb = new StringBuilder();
        sb.append('"').append(entry.getReference()).append('"').append(",");
        sb.append('"').append(ValidationEntry.getValidationTypeStr(entry.getValidationType())).append('"').append(",");
        if ((entry.getValidationType() != ValidationType.OldSuccess) && (entry.getValidationType() != ValidationType.NewSuccess)) {
          sb.append('"').append(entry.getCode()).append('"').append(",");
          sb.append('"').append(entry.getDescription()).append('"').append(",");
          sb.append('"').append(entry.getAttribute()).append('"').append(",");
          sb.append('"').append(entry.getSequence()).append('"').append(",");
          sb.append('"').append(entry.getSubSequence()).append('"').append(",");
        }
        sb.append('"').append(entry.getComment()).append('"');

        filePrinter.println(sb.toString());
      }

      filePrinter.flush();
      entryBuffer.clear();
    }
  }

  private void addEntry(ValidationEntry entry) {
    if (referenceForBuffer != null && !referenceForBuffer.equals(entry.getReference())) {
      // write out buffer to file
      flushBuffer();
    }

    referenceForBuffer = entry.getReference();
    if (!entryBuffer.contains(entry)) {
      entryBuffer.add(entry);
    }
  }

  public void addOldError(String reference, String code, String description, String attribute, int sequence, int subSequence, String comment) {
    addEntry(new ValidationEntry(reference, ValidationType.OldError, code, description, attribute, sequence, subSequence, comment));
  }

  public void addOldSuccess(String reference, String comment) {
    addEntry(new ValidationEntry(reference, ValidationType.OldSuccess, "", "", "", 0, 0, comment));
  }

  public void addNewError(String reference, ResultEntry resEntry, String comment) {
    ValidationType type = ValidationType.NewError;
    if (resEntry.getType() == ResultType.Warning)
      type = ValidationType.NewWarning;
    else if (resEntry.getType() == ResultType.Deprecated)
      type = ValidationType.NewWarning;
    addEntry(new ValidationEntry(reference, type, resEntry.getCode(),
            resEntry.getMessage(), resEntry.getName(), resEntry.getMoneyInstance() + 1, resEntry.getImportExportInstance() + 1, comment));
  }

  public void addNewSuccess(String reference, String comment) {
    addEntry(new ValidationEntry(reference, ValidationType.NewSuccess, "", "", "", 0, 0, comment));
  }
}