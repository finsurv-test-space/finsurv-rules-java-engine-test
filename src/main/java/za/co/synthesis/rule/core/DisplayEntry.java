package za.co.synthesis.rule.core;

import java.util.List;

/**
 * User: jake
 * Date: 4/19/16
 * Time: 9:23 AM
 * A display entry that is generated for a given field
 */
public class DisplayEntry {
  public static enum Visibility {
    Hide,
    Show
  }
  public static enum Availability {
    Enabled,
    Disabled
  }

  private String fieldName;
  private Visibility visibility;
  private Availability availability;
  private String fieldLabel;
  private String fieldValue;
  private List<String> limitValues = null;
  private List<String> excludeValues = null;
  private Scope scope;
  private int moneyInstance;
  private int importExportInstance;

  public DisplayEntry(String fieldName, Scope scope, int moneyInstance, int importExportInstance) {
    this.fieldName = fieldName;
    this.scope = scope;
    this.moneyInstance = moneyInstance;
    this.importExportInstance = importExportInstance;
  }

  public String getFieldName() {
    return fieldName;
  }

  public Visibility getVisibility() {
    return visibility;
  }

  public Availability getAvailability() {
    return availability;
  }

  public String getFieldValue() {
    return fieldValue;
  }

  public void setVisibility(Visibility visibility) {
    this.visibility = visibility;
  }

  public void setAvailability(Availability availability) {
    this.availability = availability;
  }

  public void setFieldValue(String fieldValue) {
    this.fieldValue = fieldValue;
  }

  public String getFieldLabel() {
    return fieldLabel;
  }

  public void setFieldLabel(String fieldLabel) {
    this.fieldLabel = fieldLabel;
  }

  public List<String> getLimitValues() {
    return limitValues;
  }

  public void setLimitValues(List<String> limitValues) {
    this.limitValues = limitValues;
  }

  public List<String> getExcludeValues() {
    return excludeValues;
  }

  public void setExcludeValues(List<String> excludeValues) {
    this.excludeValues = excludeValues;
  }

  public Scope getScope() {
    return scope;
  }

  public int getMoneyInstance() {
    return moneyInstance;
  }

  public int getImportExportInstance() {
    return importExportInstance;
  }
}
