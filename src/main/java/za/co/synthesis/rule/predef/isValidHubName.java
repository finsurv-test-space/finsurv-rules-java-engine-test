package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 9/6/16.
 */
public class isValidHubName implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value != null && value instanceof String) {
      String hubCode = (String)context.getTransactionField("HubCode");
      Map<String, String> key = new HashMap<String, String>();
      key.put("hubCode", hubCode);
      String hubName = context.getLookups().getLookupField("branches", key, "hubName");
      if (value.equals(hubName))
        return true;
    }
    return false;
  }
}

/*
  _export.isValidHubName = function (context, value) {
    var hubCode = context.getTransactionField("HubCode");
    if (hubCode && context.lookups.lookups.branches) {
      var i;
      var hub;
      for (i = 0; i < context.lookups.lookups.branches.length; i++) {
        hub = context.lookups.lookups.branches[i];
        if (hub.hubCode === hubCode) {
          if (hub.hubName === value)
            return true;
        }
      }
    }
    return false;
  }
*/
