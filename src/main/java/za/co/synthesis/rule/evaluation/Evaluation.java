package za.co.synthesis.rule.evaluation;

import za.co.synthesis.rule.core.BankAccountType;
import za.co.synthesis.rule.core.impl.EvaluationDecision;
import za.co.synthesis.rule.core.ResidenceStatus;

import java.util.List;

/**
 * User: jake
 * Date: 3/2/15
 * Time: 3:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class Evaluation {
  private final ResidenceStatus drResStatus;
  private final BankAccountType drAccType;
  private final ResidenceStatus crResStatus;
  private final BankAccountType crAccType;
  private final List<String> category;
  private final List<String> notCategory;
  private final EvaluationDecision decision;

  public Evaluation(ResidenceStatus drResStatus, BankAccountType drAccType, ResidenceStatus crResStatus, BankAccountType crAccType,
                    List<String> category, List<String> notCategory, EvaluationDecision decision) {
    this.drResStatus = drResStatus;
    this.drAccType = drAccType;
    this.crResStatus = crResStatus;
    this.crAccType = crAccType;
    this.category = category;
    this.notCategory = notCategory;
    this.decision = decision;
  }

  public ResidenceStatus getDrResStatus() {
    return drResStatus;
  }

  public BankAccountType getDrAccType() {
    return drAccType;
  }

  public ResidenceStatus getCrResStatus() {
    return crResStatus;
  }

  public BankAccountType getCrAccType() {
    return crAccType;
  }

  public List<String> getCategory() {
    return category;
  }

  public List<String> getNotCategory() {
    return notCategory;
  }

  public EvaluationDecision getDecision() {
    return decision;
  }
}
