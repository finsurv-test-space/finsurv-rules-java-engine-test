package za.co.synthesis.rule.support;

/**
 * Resolves the value of the string
 */
public interface IStringResolver {
  String resolve(FinsurvContext context, Object value);
}
