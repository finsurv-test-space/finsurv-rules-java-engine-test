package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 2:14 PM
 * Determines if the current value is not a valid VAT number
 */
public class notValidVATNumber implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return (value == null) || !(value instanceof String) || !Util.isValidVATNumber((String) value);
  }
}