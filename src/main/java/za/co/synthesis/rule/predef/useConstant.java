package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

/**
 * User: jake
 * Date: 4/19/16
 * Time: 9:45 PM
 * Returns the given constant. This is used when setting or appending a fixed constant value
 */
public class useConstant implements IDisplayFunction {
  private final String constant;

  public useConstant(String constant) {
    this.constant = constant;
  }

  @Override
  public String execute(FinsurvContext context, Object value) {
    return constant;
  }
}
