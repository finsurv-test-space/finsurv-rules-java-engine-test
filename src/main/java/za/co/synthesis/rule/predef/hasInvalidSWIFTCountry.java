package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 11:36 AM
 * Determines if the current value is not one of the valid SWIFT country codes
 */
public class hasInvalidSWIFTCountry implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return !context.getLookups().isValidCountryCode(value.toString());
  }
}
/*
function hasInvalidSWIFTCountry(context, value) {
  if (!value)
    return true;
  return !context.lookups.isValidCountryCode(value);
}
*/