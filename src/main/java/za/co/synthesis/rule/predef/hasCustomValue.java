package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 5:15 AM
 * Determines if the specified custom value entry has been provided with the given value
 */
public class hasCustomValue implements IAssertionFunction {
  private String fieldname;
  private String constant;

  public hasCustomValue(String fieldname, String constant) {
    this.fieldname = fieldname;
    this.constant = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object cv = context.getCustomValue().get(fieldname);
    return (cv != null && constant.equals(cv));
  }
}

/*
function hasCustomValue(field,_value){
  return function(context, value) {
    return (context.getCustomValue(field) === _value);
  }
}
*/