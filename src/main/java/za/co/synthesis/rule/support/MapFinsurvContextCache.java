package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.IFinsurvContextCache;
import za.co.synthesis.rule.core.StatusType;
import za.co.synthesis.rule.support.CentralCustomValidationCache;
import za.co.synthesis.rule.support.CustomValidateCache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 The following is the format to be used by this component to store validations
 "ValidationCache": [
   {
     "Name": "",
     "Inputs": {
       "Value": "",
       "Other": [ "", "", "" ]
     "Result": {
       "Status": "", //Pass, Fail, Error
       "Code": "",
       "Message": ""
     },
     "Response": {...}
   }
 ]
 */
public class MapFinsurvContextCache implements IFinsurvContextCache {
    private final Map<String, Object> mapCache;

    public MapFinsurvContextCache(Map<String, Object> cache) {
        this.mapCache = cache;
    }

    public Map<String, Object> getMapCache() {
        return mapCache;
    }

    @Override
    public void clearCache() {
        mapCache.remove("ValidationCache");
    }

    private String objToString(Object value) {
        if (value != null) {
            if (value instanceof String)
                return (String) value;
            if (value instanceof Boolean)
                return ((Boolean) value) ? "true" : "false";
            return value.toString();
        }
        return null;
    }

    private String statusToString(StatusType value) {
        if (value != null) {
            if (value.equals(StatusType.Pass))
                return "Pass";
            if (value.equals(StatusType.Error))
                return "Error";
            if (value.equals(StatusType.Fail))
                return "Fail";
        }
        return null;
    }

    @Override
    public void marshalValidate(CentralCustomValidationCache.ValidationCacheEntry entry) {
        Object objCache = mapCache.get("ValidationCache");

        if (objCache == null || !(objCache instanceof List)) {
            objCache = new ArrayList();
            mapCache.put("ValidationCache", objCache);
        }

        List arrayCache = (List)objCache;

        Map<String, Object> jsVal = new HashMap<String, Object>();
        jsVal.put("Name", entry.getValidateName());

        Map<String, Object> jsInputs = new HashMap<String, Object>();
        jsInputs.put("Value", objToString(entry.getValidateInputs().getValue()));
        if (entry.getValidateInputs().getOtherInputs() != null) {
            List jsOtherInputs = new ArrayList();
            jsInputs.put("Other", jsOtherInputs);
            for (Object objInput : entry.getValidateInputs().getOtherInputs()) {
                jsOtherInputs.add(objToString(objInput));
            }
        }
        jsVal.put("Inputs", jsInputs);

        Map<String, Object> jsResult = new HashMap<String, Object>();
        jsResult.put("Status", statusToString(entry.getResult().getStatus()));
        if (entry.getResult().getCode() != null) {
            jsResult.put("Code", entry.getResult().getCode());
        }
        if (entry.getResult().getMessage() != null) {
            jsResult.put("Message", entry.getResult().getMessage());
        }
        jsVal.put("Result", jsResult);

        if (entry.getResponseCache() != null) {
            Map<String, Object> jsResponse = new HashMap<String, Object>();
            jsResponse.putAll(entry.getResponseCache());
            jsVal.put("Response", jsResult);
        }

        arrayCache.add(jsVal);
    }

    private CustomValidateCache.ValidateInputs readInputs(Object objInputs) {
        if (objInputs instanceof Map) {
            Object value;
            Object[] otherInputs = null;

            Map jsInputs = (Map)objInputs;
            value = jsInputs.get("Value");
            Object objOther = jsInputs.get("Other");
            if (objOther instanceof List) {
                List arrayInputs = (List) objOther;
                otherInputs = new Object[arrayInputs.size()];
                int i = 0;
                for (Object objInput : arrayInputs) {
                    otherInputs[i++] = objInput;
                }
            }
            return new CustomValidateCache.ValidateInputs(value, otherInputs);
        }
        return null;
    }

    private CustomValidateResult readResult(Object objResult) {
        if (objResult instanceof Map) {
            Map<String, Object> jsResult = (Map)objResult;

            StatusType status = null;
            String code;
            String message;

            String strStatus = (String)jsResult.get("Status");
            if (strStatus != null) {
                if (strStatus.equals("Pass")) {
                    status = StatusType.Pass;
                }
                else
                if (strStatus.equals("Error")) {
                    status = StatusType.Error;
                }
                else
                if (strStatus.equals("Fail")) {
                    status = StatusType.Fail;
                }
            }
            code = (String)jsResult.get("Code");
            message = (String)jsResult.get("Message");

            if (status != null) {
                return new CustomValidateResult(status, code, message);
            }
        }
        return null;
    }

    private CustomValidateCache.ResponseCache readResponseCache(Object objResponse) {
        if (objResponse instanceof Map) {
            Map<String, Object> jsResponse = (Map)objResponse;

            CustomValidateCache.ResponseCache value = new CustomValidateCache.ResponseCache();
            value.putAll(jsResponse);
            return value;
        }
        return null;
    }

    @Override
    public List<CentralCustomValidationCache.ValidationCacheEntry> unmarshalCache() {
        List<CentralCustomValidationCache.ValidationCacheEntry> result = new ArrayList<CentralCustomValidationCache.ValidationCacheEntry>();
        Object obj = mapCache.get("ValidationCache");
        if (obj instanceof List) {
            List array = (List) obj;

            for (Object objVal : array) {
                if (objVal instanceof Map) {
                    Map<String, Object> jsVal = (Map)objVal;
                    CentralCustomValidationCache.ValidationCacheEntry entry = new CentralCustomValidationCache.ValidationCacheEntry();
                    entry.setValidateName((String)jsVal.get("Name"));
                    entry.setValidateInputs(readInputs(jsVal.get("Inputs")));
                    entry.setResult(readResult(jsVal.get("Result")));
                    entry.setResponseCache(readResponseCache(jsVal.get("Response")));

                    if (entry.getValidateName() != null && entry.getValidateInputs() != null && entry.getResult() != null) {
                        result.add(entry);
                    }
                }
            }
        }
        return result;
    }
}
