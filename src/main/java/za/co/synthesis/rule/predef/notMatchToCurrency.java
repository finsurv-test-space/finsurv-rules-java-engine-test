package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 8:12 AM
 * Determines in the value (expected to be a country code) matches the currency
 */
public class notMatchToCurrency implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if ((value instanceof String && ((String)value).length() == 2)) {
      String curr = context.getCurrency();
      if (curr.startsWith((String)value))
        return false;
    }
    return true;
  }
}
/*
function notMatchToCurrency(context, value) {
  if ((typeof value == "string" && value.length == 2)) {
    var curr = context.getTransactionField("FlowCurrency");
    if (curr.substr(0, 2) === value)
      return false;
  }
  return true;
}
*/