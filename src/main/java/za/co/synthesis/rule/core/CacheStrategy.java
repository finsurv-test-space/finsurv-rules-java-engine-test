package za.co.synthesis.rule.core;

import java.util.ArrayList;
import java.util.List;

public class CacheStrategy {
    private final List<StatusCacheStrategy> strategy = new ArrayList<StatusCacheStrategy>();

    public CacheStrategy(StatusType status, CachePeriod cachePeriod) {
        strategy.add(new StatusCacheStrategy(status, cachePeriod));
    }

    public static CacheStrategy set(StatusType status, CachePeriod cachePeriod) {
        return new CacheStrategy(status, cachePeriod);
    }

    public CacheStrategy and(StatusType status, CachePeriod cachePeriod) {
        strategy.add(new StatusCacheStrategy(status, cachePeriod));
        return this;
    }

    public StatusCacheStrategy getCacheStrategyByStatus(StatusType status) {
        for (StatusCacheStrategy sct : strategy) {
            if (sct.getStatus().equals(status)) {
                return sct;
            }
        }
        return null;
    }
}
