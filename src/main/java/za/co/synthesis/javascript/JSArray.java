package za.co.synthesis.javascript;

import java.util.ArrayList;

/**
 * User: jake
 * Date: 8/3/14
 * Time: 9:26 AM
 * Essentially a JavaScript Object is just a list of Objects
 */
public class JSArray extends ArrayList<Object> implements IJSSnippetSupport {
  protected final JSSnippetSupport snippets;

  public JSArray() {
    this.snippets = new JSSnippetSupport();
  }

  public JSArray(JSSnippetSupport jss) {
    this.snippets = jss;
  }

  @Override
  public String toString() {
    StringBuilder array = new StringBuilder();
    array.append("[");
    for (Object obj : this) {
      if (array.length() > 1)
        array.append(", ");
      if (obj instanceof String)
        array.append('"').append(JSUtil.escapeJavaScriptString(obj.toString(), false)).append('"');
      else if (obj == null)
        array.append("null");
      else
        array.append(obj.toString());
    }
    array.append("]");

    return array.toString();
  }

  public void compose(JSWriter writer) {
    writer.append("[");

    boolean bComplexContent = false;
    if (this.size() > 0) {
      Object obj = this.get(0);
      if (obj instanceof JSObject || obj instanceof JSArray ||
          obj instanceof JSFunctionDefinition || obj instanceof JSFunctionCall) {
        bComplexContent = true;
      }
    }
    if (bComplexContent) {
      writer.incIndent();
      writer.appendNewLine();
    }
    boolean bFirst = true;
    for (Object obj : this) {
      if (!bFirst) {
        if (bComplexContent) {
          writer.append(",");
          writer.appendNewLine();
        } else
          writer.append(", ");
      } else
        bFirst = false;
      if (obj == null) {
        writer.append("null");
      } else if (obj instanceof String) {
        writer.append('"');
        JSUtil.escapeJavaScriptString(obj.toString(), false, writer);
        writer.append('"');
        bComplexContent = false;
      } else if (obj instanceof JSObject) {
        ((JSObject) obj).compose(writer);
      } else if (obj instanceof JSArray) {
        ((JSArray) obj).compose(writer);
      } else if (obj instanceof JSFunctionDefinition) {
        ((JSFunctionDefinition) obj).compose(writer);
      } else if (obj instanceof JSFunctionCall) {
        ((JSFunctionCall) obj).compose(writer);
      } else if (obj instanceof JSIdentifier) {
        ((JSIdentifier) obj).compose(writer);
      } else if (obj instanceof JSRegExLiteral) {
        ((JSRegExLiteral) obj).compose(writer);
      } else if (obj instanceof Boolean) {
        writer.append(((Boolean) obj) ? "true" : "false");
      } else if (obj instanceof Number || Number.class.isAssignableFrom(obj.getClass())) {
        writer.append(obj.toString());
      } else {
        writer.append('"');
        writer.append(obj.toString());
        writer.append('"');
      }
    }
    if (bComplexContent) {
      writer.decIndent();
      writer.appendNewLine();
    }
    writer.append("]");
  }

  @Override
  public void setPreceding(JSSnippetSupport preceding, int precedingIndex) {
    snippets.setPreceding(preceding, precedingIndex);
  }

  @Override
  public void addSnippet(JSSnippet snippet) {
    snippets.addSnippet(snippet);
  }

  @Override
  public int currentSnippetIndex() {
    return snippets.currentSnippetIndex();
  }

  @Override
  public void augmentEndSnippet(String extraText) {
    snippets.augmentEndSnippet(extraText);
  }

  @Override
  public void composeSnippets(JSWriter writer)
  {
    boolean bComplexContent = false;
    boolean bIndentWriter = false;

    JSSnippet snippet = snippets.getFirstSnippetByType(JSSnippet.Type.Start);
    if (snippet != null) {
      writer.append(snippet.getText());
    }
    else {
      writer.append("[");

      if (this.size() > 0) {
        Object obj = this.get(0);
        if (obj instanceof JSObject || obj instanceof JSArray ||
                obj instanceof JSFunctionDefinition || obj instanceof JSFunctionCall) {
          bComplexContent = true;
        }
      }
      if (bComplexContent) {
        bIndentWriter = true;
        writer.incIndent();
        writer.appendNewLine();
      }
    }

    int index = 0;
    for (Object obj : this) {
      if (index > 0) {
        snippet = snippets.getSeparatorByValue(index);
        if (snippet != null) {
          writer.append(snippet.getText());
        }
        else {
          if (bComplexContent) {
            writer.append(",");
            writer.appendNewLine();
          } else
            writer.append(", ");
        }
      }
      index++;

      snippet = snippets.getSnippetForValue(obj);

      if (obj == null) {
        writer.append("null");
      } else if (obj instanceof String) {
        if (snippet != null) {
          writer.append(snippet.getText());
        }
        else {
          writer.append('"');
          JSUtil.escapeJavaScriptString(obj.toString(), false, writer);
          writer.append('"');
          bComplexContent = false;
        }
      } else if (obj instanceof JSObject) {
        ((JSObject) obj).composeSnippets(writer);
      } else if (obj instanceof JSArray) {
        ((JSArray) obj).composeSnippets(writer);
      } else if (obj instanceof JSFunctionDefinition) {
        ((JSFunctionDefinition) obj).composeSnippets(writer);
      } else if (obj instanceof JSFunctionCall) {
        ((JSFunctionCall) obj).composeSnippets(writer);
      } else if (obj instanceof JSIdentifier) {
        ((JSIdentifier) obj).composeSnippets(writer);
      } else if (obj instanceof JSRegExLiteral) {
        ((JSRegExLiteral) obj).composeSnippets(writer);
      } else if (obj instanceof Boolean) {
        if (snippet != null) {
          writer.append(snippet.getText());
        }
        else {
          writer.append(((Boolean) obj) ? "true" : "false");
        }
      } else if (obj instanceof Number || Number.class.isAssignableFrom(obj.getClass())) {
        if (snippet != null) {
          writer.append(snippet.getText());
        }
        else {
          writer.append(obj.toString());
        }
      } else {
        if (snippet != null) {
          writer.append(snippet.getText());
        }
        else {
          writer.append('"');
          writer.append(obj.toString());
          writer.append('"');
        }
      }
    }

    snippet = snippets.getFirstSnippetByType(JSSnippet.Type.End);
    if (snippet != null) {
      writer.append(snippet.getText());
    }
    else {
      if (bIndentWriter) {
        writer.decIndent();
        writer.appendNewLine();
      }

      writer.append("]");
    }
  }

  @Override
  public void removeExtraWhitespace() {
    snippets.removeExtraWhitespace(this);
  }

  @Override
  public void injectWhitespace(Object parentObject) {
    snippets.injectWhitespace(parentObject, this);
  }
}
