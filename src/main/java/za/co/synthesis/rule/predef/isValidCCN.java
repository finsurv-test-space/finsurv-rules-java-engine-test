package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 2:17 PM
 * Determines if the current value is a valid CCN number
 */
public class isValidCCN implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return value != null && value instanceof String && Util.isValidCCN((String) value);
  }
}