define({
  engine: {major: "1", minor: "0"},
  mappings: {
    LocalCurrencySymbol: "R",
    LocalCurrencyName: "Rand",
    LocalCurrency: "ZAR",
    Locale: "ZA",
    LocalValue: "RandValue",
    Regulator: "Regulator",
    DealerPrefix: "RE",
    RegulatorPrefix: "CB",
    BrandNew: "SHINY"
  }
})