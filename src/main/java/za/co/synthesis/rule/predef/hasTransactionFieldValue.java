package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;
import za.co.synthesis.rule.support.StringList;

import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 10:50 AM
 * Determines if the given transaction field has been provided and if it is equal to the given constant value
 */
public class hasTransactionFieldValue implements IAssertionFunction {
  private IStringResolver field;
  private List<IStringResolver> constants = null;

  public hasTransactionFieldValue(IStringResolver field, IStringResolver constant) {
    this.field = field;
    this.constants = new ArrayList<IStringResolver>();
    this.constants.add(constant);
  }

  public hasTransactionFieldValue(String field, StringList constants) {
    this.field = new literal(field);
    this.constants = new ArrayList<IStringResolver>();
    for (String constant : constants) {
      this.constants.add(new literal(constant));
    }
  }

  public boolean execute(FinsurvContext context, Object value) {
    String fieldName = field.resolve(context, value);
    Object fieldValue = context.getTransactionField(fieldName);
    if (fieldValue != null) {
      for (IStringResolver constant : constants) {
        String constantValue = constant.resolve(context, value);
        if (fieldValue.equals(constantValue))
          return true;
      }
    }
    return false;
  }
}
/*
function hasTransactionFieldValue(field, constant) {
  return function(context, value) {
    var fieldValue = context.getTransactionField(field);
    if (fieldValue) {
      if (typeof constant === "string") {
        if (fieldValue === constant)
          return true;
      } else {
        for ( var i = 0; i < constant.length; i++) {
          if (fieldValue === constant[i])
            return true;
        }
      }
    }
    return false;
  }
}
*/