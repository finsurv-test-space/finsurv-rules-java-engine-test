define(['testDisplayBase'], function (testBase) {
  with (testBase) {
    var test_cases = [
      assertInclude("inc001", {Account: {System: 'FORX', Reference: '3012'}}),
      assertExclude("exc001", {BusinessTrnCode: 'IDDZ'}),
      assertZZ2("nr001", {BusinessTrnCode: 'NMGO'})
    ]
  }
})
