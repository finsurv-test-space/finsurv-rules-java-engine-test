package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.MatchInfo;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;

public class MAFAnd implements IMatchAssertionFunction {
  private IMatchAssertionFunction left;
  private IMatchAssertionFunction right;

  public MAFAnd(IMatchAssertionFunction left, IMatchAssertionFunction right) {
    this.left = left;
    this.right = right;
  }

  public boolean execute(MatchInfo context) {
    return left.execute(context) && right.execute(context);
  }
}