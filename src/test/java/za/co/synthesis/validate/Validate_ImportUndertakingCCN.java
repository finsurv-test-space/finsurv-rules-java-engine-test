package za.co.synthesis.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:12 PM
 * A Test implementation of the Validate_ImportUndertakingCCN function. Needs to raise the following SARB error:
 * 323, "Not a registered import undertaking client"
 * Should only be run when: the Flow is OUT and category is 102/01 to 102/10 or 104/01 to 104/10 is used
 */
public class Validate_ImportUndertakingCCN implements ICustomValidate {
  private Map<String, CCNInfo> ccnInfoMap;

  public Validate_ImportUndertakingCCN(Map<String, CCNInfo> ccnInfoMap) {
    this.ccnInfoMap = ccnInfoMap;
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String ccn = value.toString();
    if (ccnInfoMap.containsKey(ccn)) {
      CCNInfo ccnInfo = ccnInfoMap.get(ccn);

      if (ccnInfo.isValidCCN()) {
        if (ccnInfo.isImportUndertaking())
          return new CustomValidateResult(StatusType.Pass);
        else
          return new CustomValidateResult(StatusType.Fail, "323", "Not a registered import undertaking client");
      }
      else {
        return new CustomValidateResult(StatusType.Fail, "322", "This is an invalid customs client number");
      }
    }
    else {
      return new CustomValidateResult(StatusType.Pass);
    }
  }
}
