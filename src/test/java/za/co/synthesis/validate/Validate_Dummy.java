package za.co.synthesis.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

public class Validate_Dummy implements ICustomValidate {
    private StatusType statusType;

    public Validate_Dummy(StatusType statusType) {
        this.statusType = statusType;
    }

    @Override
    public CustomValidateResult call(Object value, Object... otherInputs) {
        if (statusType == StatusType.Fail) {
            return new CustomValidateResult(StatusType.Fail, "001", "Failure Message");
        }
        else
        if (statusType == StatusType.Error) {
            return new CustomValidateResult(StatusType.Error, "002", "Error Message");
        }
        return new CustomValidateResult(StatusType.Pass);
    }
}
