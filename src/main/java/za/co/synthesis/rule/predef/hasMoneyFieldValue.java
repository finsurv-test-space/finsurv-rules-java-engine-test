package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.*;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 1:09 PM
 * Determines if the given monetary amount field has been provided and if it is equal to the given constant value
 */
public class hasMoneyFieldValue implements IAssertionFunction {
  private IStringResolver field;
  private StringResolverList constants = null;

  public hasMoneyFieldValue(IStringResolver field, IStringResolver constant) {
    this.field = field;
    this.constants = new StringResolverList();
    this.constants.add(constant);
  }

  public hasMoneyFieldValue(IStringResolver field, StringResolverList constant) {
    this.field = field;
    this.constants = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    int index = context.getCurrentMoneyInstance();
    if (index == -1)
      index = 0;
    Object fieldValue = context.getMoneyField(index, field.resolve(context, value));
    if (fieldValue != null) {
      for (IStringResolver constant : constants) {
        if (constant != null) {
          String str = constant.resolve(context, value);
          if (fieldValue.equals(str))
            return true;
        }
      }
    }
    return false;
  }
}
/*
function hasMoneyFieldValue(field, constant) {
  return function(context, value) {
    var index = context.currentMoneyInstance;
    if (index == -1)
      index = 0;
    var fieldValue = context.getMoneyField(index, field);
    if (fieldValue) {
      if (typeof constant === "string") {
        if (fieldValue === constant)
          return true;
      } else {
        for ( var i = 0; i < constant.length; i++) {
          if (fieldValue === constant[i])
            return true;
        }
      }

    }
    return false;
  }
}*/