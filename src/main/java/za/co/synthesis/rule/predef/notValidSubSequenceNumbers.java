package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 1/28/16
 * Time: 4:37 PM
 * Check to see if all the import export sub sequence amounts are in order
 */
public class notValidSubSequenceNumbers implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    boolean validSequenceNumbers = true;
    boolean missingSequenceNumbers = false;
    int moneyIndex = context.getCurrentMoneyInstance();
    if (moneyIndex == -1)
      moneyIndex = 0;

    for (int ieInstance = 0; ieInstance < context.getImportExportSize(moneyIndex); ieInstance++) {
      Object fieldValue = context.getImportExportField(moneyIndex, ieInstance, "SubSequence");
      if (fieldValue != null) {
        BigDecimal seqNo = Util.number(fieldValue);
        if (missingSequenceNumbers || seqNo == null || seqNo.intValue() != ieInstance+1)
          validSequenceNumbers = false;
      }
      else {
        missingSequenceNumbers = true;
      }
    }
    return !validSequenceNumbers;
  }
}
/*
  _export.notValidSubSequenceNumbers = function notValidSubSequenceNumbers(context, value) {
    var validSequenceNumbers = true;
    var missingSequenceNumbers = false;
    var ieInstance;
    var moneyIndex = context.currentMoneyInstance;
    if (moneyIndex == -1)
      moneyIndex = 0;

    for (ieInstance = 0; ieInstance < context.getImportExportSize(moneyIndex); ieInstance++) {
      var fieldValue = context.getImportExportField(moneyIndex, ieInstance, "SubSequence");
      if (fieldValue) {
        if (missingSequenceNumbers || Number(fieldValue) != ieInstance+1)
          validSequenceNumbers = false;
      }
      else {
        missingSequenceNumbers = true;
      }
    }
    return !validSequenceNumbers;
  }
*/

