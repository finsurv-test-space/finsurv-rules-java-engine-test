package za.co.synthesis.rule.evaluation;

import za.co.synthesis.rule.core.AccountHolderStatus;
import za.co.synthesis.rule.core.BankAccountType;
import za.co.synthesis.rule.core.CounterpartyInstituition;
import za.co.synthesis.rule.core.ResidenceStatus;

import java.util.Map;

public class MatchInfo {
  private Context context;

  private String drBankBIC;
  private ResidenceStatus drResStatus;
  private BankAccountType drAccType;
  private String drCurrency;
  private String drField72;
  private Map<String, Object> drAdditionalParams;

  private String crBankBIC;
  private ResidenceStatus crResStatus;
  private BankAccountType crAccType;
  private String crCurrency;
  private Map<String, Object> crAdditionalParams;


  public Context getContext() {
    return context;
  }

  public void setContext(Context context) {
    this.context = context;
  }

  public String getDrBankBIC() {
    return drBankBIC;
  }

  public void setDrBankBIC(String drBankBIC) {
    this.drBankBIC = drBankBIC;
  }

  public ResidenceStatus getDrResStatus() {
    return drResStatus;
  }

  public void setDrResStatus(ResidenceStatus drResStatus) {
    this.drResStatus = drResStatus;
  }

  public BankAccountType getDrAccType() {
    return drAccType;
  }

  public void setDrAccType(BankAccountType drAccType) {
    this.drAccType = drAccType;
  }

  public String getDrCurrency() {
    return drCurrency;
  }

  public void setDrCurrency(String drCurrency) {
    this.drCurrency = drCurrency;
  }

  public String getDrField72() {
    return drField72;
  }

  public void setDrField72(String drField72) {
    this.drField72 = drField72;
  }

  public Map<String, Object> getDrAdditionalParams() {
    return drAdditionalParams;
  }

  public void setDrAdditionalParams(Map<String, Object> drAdditionalParams) {
    this.drAdditionalParams = drAdditionalParams;
  }

  public String getCrBankBIC() {
    return crBankBIC;
  }

  public void setCrBankBIC(String crBankBIC) {
    this.crBankBIC = crBankBIC;
  }

  public ResidenceStatus getCrResStatus() {
    return crResStatus;
  }

  public void setCrResStatus(ResidenceStatus crResStatus) {
    this.crResStatus = crResStatus;
  }

  public BankAccountType getCrAccType() {
    return crAccType;
  }

  public void setCrAccType(BankAccountType crAccType) {
    this.crAccType = crAccType;
  }

  public String getCrCurrency() {
    return crCurrency;
  }

  public void setCrCurrency(String crCurrency) {
    this.crCurrency = crCurrency;
  }

  public Map<String, Object> getCrAdditionalParams() {
    return crAdditionalParams;
  }

  public void setCrAdditionalParams(Map<String, Object> crAdditionalParams) {
    this.crAdditionalParams = crAdditionalParams;
  }
}
