package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 12:29 PM
 * A function that always returns false
 */
public class alwaysFalse implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return false;
  }
}