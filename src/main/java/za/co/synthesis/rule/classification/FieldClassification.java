package za.co.synthesis.rule.classification;

import za.co.synthesis.rule.support.StringList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/7/16.
 */
public class FieldClassification {
  private StringList fieldList;
  private List<ClassificationRule> rules;

  public FieldClassification(String field) {
    this.fieldList = new StringList();
    this.fieldList.add(field);
    this.rules = new ArrayList<ClassificationRule>();
  }

  public FieldClassification(StringList fieldList) {
    this.fieldList = fieldList;
    this.rules = new ArrayList<ClassificationRule>();
  }

  public FieldClassification addRule(ClassificationRule rule) {
    this.rules.add(rule);
    return this;
  }

  public List<String> getFieldList() {
    return fieldList;
  }

  public List<ClassificationRule> getRules() {
    return rules;
  }
}
