package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * Created by nishaal on 11/11/16.
 */
public class isLessThan implements IAssertionFunction {
    private BigDecimal constant;

    public isLessThan(String constant) {
        this.constant = Util.number(constant);
    }

    public boolean execute(FinsurvContext context, Object value) {
        BigDecimal numValue = Util.number(value);
        // BigDecimal.compareTo: -1, 0, or 1 as this BigDecimal is numerically less than, equal to, or greater than val.
        if (numValue != null)
            return numValue.compareTo(constant) == -1;
        return false;
    }
}
/*
  _export.isLessThan = function (amount) {
    return function (context, value) {
      return !(value > amount);
    }
  }

*/