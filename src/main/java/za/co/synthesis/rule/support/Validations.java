package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.FlowType;
import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.ValidationSchema;
import za.co.synthesis.rule.predef.isTooLong;
import za.co.synthesis.rule.predef.isTooShort;
import za.co.synthesis.rule.predef.isWrongLength;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 8:29 AM
 * This class contains the all the validations. It is responsible for generating these validations from a set of
 * JavaScript structures. It provides methods to retrieve a subset of the rules based on filter criteria.
 */
public class Validations implements IValidationRules {
//  Logger logger = LoggerFactory.getLogger(Validations.class);

  private static class CodeMessage {
    public String code;
	  public String message;
	  public String section;
  }

  private final List<RuleSet> ruleSets = new ArrayList<RuleSet>();

  public Validations() {
  }

  public List<RuleSet> getRuleSets() {
    return ruleSets;
  }

  private ArrayList<Rule> getRuleListOfType(RuleType type){
    ArrayList<Rule> list = new ArrayList<Rule>();

    for (RuleSet ruleset : ruleSets) {
      for (FieldValidation validation : ruleset.getValidations()) {
        for (Rule rule : validation.getRules()) {
          if (rule.getType() == type) {
            if (!list.contains(rule)) {
              list.add(rule);
            }
          }
        }
      }
    }
    return list;
  }

  public List<Rule> getDocumentRules() {
    return getRuleListOfType(RuleType.Document);
  }

  public List<String> customValidateList() {
    StringList out = new StringList();
    ArrayList<Rule> extValidations = getRuleListOfType(RuleType.Validate);
    for(Rule rule : extValidations){
      out.add(rule.getValidation());
    }
    return out;
  }

  private static String getParameterValue(JSFunctionCall jsFunc, int paramIndex) {
    Object objParam = jsFunc.getParameters().get(paramIndex);
    return objParam != null ? objParam.toString() : null;
  }

  private static String getRuleDSL(JSFunctionCall jsFunc) {
    JSWriter writer = new JSWriter();
    jsFunc.composeSnippets(writer);
    return writer.toString();
  }

  private static String getAssertionDSL(JSFunctionCall jsFunc, int paramIndex) {
    JSWriter writer = new JSWriter();
    Object param = jsFunc.getParameters().get(paramIndex);
    if (param instanceof JSIdentifier) {
      ((JSIdentifier) param).composeSnippets(writer);
      return writer.toString();
    }
    return null;
  }

  private Rule loadRule(Map<String, String> mapping, JSFunctionCall jsFunc) throws Exception {
    DSLLint.List lint = new DSLLint.List();
    return loadRule(mapping, jsFunc, lint);
  }

  private static boolean matchFunction(JSFunctionCall jsFunc, String funcName, int paramCount, DSLLint.List lint) {
    if (jsFunc.getName().equals(funcName)) {
      if (jsFunc.getParameters().size() >= paramCount) {
        if (jsFunc.getParameters().size() > 3) {
          lint.add(DSLLint.createWarning("The " + funcName + " function has too many parameters (has " + jsFunc.getParameters().size() + "expecting " + paramCount + "). Extra parameters ignored"));
        }
        return true;
      } else {
        lint.add(DSLLint.createError("The " + funcName + " function has too few parameters (has " + jsFunc.getParameters().size() + "expecting " + paramCount + ")"));
      }
    }
    return false;
  }

  public static Rule loadRule(Map<String, String> mapping, JSFunctionCall jsFunc, DSLLint.List lint) throws Exception {
    Rule result = null;

    IAssertionFunction assertion;

    String ruleDSL = getRuleDSL(jsFunc);

    if (matchFunction(jsFunc, RuleType.Error.toString(), 4, lint)) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 3);
      if (assertion != null) {
        result = new Failure(getParameterValue(jsFunc,0),
                getParameterValue(jsFunc,1),
                getParameterValue(jsFunc,2),
                assertion, getAssertionDSL(jsFunc, 3),
                ruleDSL);
      }
    }
    else
    if (matchFunction(jsFunc, RuleType.Warning.toString(), 4, lint)) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 3);
      if (assertion != null) {
        result = new Warning(getParameterValue(jsFunc,0),
                getParameterValue(jsFunc,1),
                getParameterValue(jsFunc,2),
                assertion, getAssertionDSL(jsFunc, 3),
                ruleDSL);
      }
    }
    else
    if (matchFunction(jsFunc, RuleType.Deprecated.toString(), 4, lint)) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 3);
      if (assertion != null) {
        result = new Deprecated(getParameterValue(jsFunc,0),
                getParameterValue(jsFunc,1),
                getParameterValue(jsFunc,2),
                assertion, getAssertionDSL(jsFunc, 3),
                ruleDSL);
      }
    }
    else
    if (matchFunction(jsFunc, RuleType.Validate.toString(), 3, lint)) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 2);
      result = new Validate(getParameterValue(jsFunc,0),
              getParameterValue(jsFunc,1),
              assertion, getAssertionDSL(jsFunc, 2),
              ruleDSL);
    }
    else
    if (matchFunction(jsFunc, RuleType.Document.toString(), 4, lint)) {
      assertion = DSLSupport.getAssertion(mapping, jsFunc, 3);
      if (assertion != null) {
        result = new Document(getParameterValue(jsFunc,0),
                getParameterValue(jsFunc,1),
                getParameterValue(jsFunc,2),
                assertion, getAssertionDSL(jsFunc, 3),
                ruleDSL);
      }
    }
    else
    if (matchFunction(jsFunc, RuleType.Ignore.toString(), 0, lint)) {
      result = new Ignore(getParameterValue(jsFunc,0), ruleDSL);
    }
    else
    if (matchFunction(jsFunc, RuleType.Message.toString(), 3, lint)) {
      result = new Message(getParameterValue(jsFunc,0),
              getParameterValue(jsFunc,1),
              getParameterValue(jsFunc,2),
              ruleDSL);
    }
    else {
      lint.add(DSLLint.createError("Unknown function name '" + jsFunc.getName() + "' provided"));
    }

    if (result != null) {
      DSLSupport.applyFilters(result, jsFunc.getApply());
    }
    return result;
  }

  public void loadRuleSet(String packageName, Map<String, String> mapping, JSObject jsRuleSet) {
    Object rulesetName = jsRuleSet.get("ruleset");
    Object rulesetScope = jsRuleSet.get("scope");

    if (rulesetName != null && rulesetScope != null) {
      Scope scope = Scope.Transaction;
      if (rulesetScope.equals("money"))
        scope = Scope.Money;
      else
      if (rulesetScope.equals("importexport"))
        scope = Scope.ImportExport;
      RuleSet ruleset = new RuleSet(packageName, rulesetName.toString(), scope);

      JSArray jsValidations = (JSArray)jsRuleSet.get("validations");
      for (Object obj : jsValidations) {
        if (obj instanceof JSObject) {
          JSObject jsValidation = (JSObject)obj;
          Object fieldName = jsValidation.get("field");

          if (fieldName != null) {
            FieldValidation fieldVal = new FieldValidation(DSLSupport.composeStringList(fieldName));

            Object data = jsValidation.get("minLen");
            if (data != null)
              fieldVal.setMinLen(Integer.parseInt(data.toString()));
            data = jsValidation.get("maxLen");
            if (data != null)
              fieldVal.setMaxLen(Integer.parseInt(data.toString()));
            data = jsValidation.get("len");
            if (data != null)
              fieldVal.setLen(Integer.parseInt(data.toString()));

            JSArray jsRules = (JSArray)jsValidation.get("rules");
            if (jsRules != null) {
              for (Object objRule : jsRules) {
                if (objRule instanceof JSFunctionCall) {
                  JSFunctionCall jsFunc = (JSFunctionCall)objRule;
                  try {
                    Rule rule = loadRule(mapping, jsFunc);
                    if (rule != null) {
                      fieldVal.addRule(rule);
                    }
                  } catch (Exception e) {
//                    logger.info("Error loading rule ", e);
                    System.out.println("Error loading rule '"+ obj.toString() +"' on field '" + fieldName.toString() + "': " + e.getMessage());
                  }
                }
              }
            }
            ruleset.addValidation(fieldVal);
          }
        }
      }
      ruleSets.add(ruleset);
    }
  }

  public boolean categoryNotInList(String category, String mainCategory, List<String> ruleList) {
    boolean result = true;
    for (String ruleItem : ruleList) {
      int pos = ruleItem.indexOf('/');
      if (pos == -1) {
        if (ruleItem.equals(mainCategory)) {
          result = false;
          break;
        }
      } else {
        if (ruleItem.equals(category)) {
          result = false;
          break;
        }
      }
    }
    return result;
  }

  private boolean categoriesNotInList(List<String> categories, List<String> mainCategories, List<String> ruleList) {
    for (int i=0; i<categories.size(); i++) {
      if (categoryNotInList(categories.get(i), mainCategories.get(i), ruleList))
        return true;
    }
    return false;
  }

  private CodeMessage resolveMessageAndSection(String rulename, String field,
                                     Map<String, Message> msgRuleMap, Map<String, Message> msgFieldLenMap,
                                     String defCode, String defMessage, String defSection) {
    if (rulename != null) {
      String name = rulename;
      boolean isFieldRule = false;
      if (rulename.startsWith("field.")) {
        isFieldRule = true;
        name = field + rulename.substring(5);
      }
      if (isFieldRule) {
        Message msg = msgFieldLenMap.get(name);
        if (msg != null) {
          CodeMessage cm = new CodeMessage();
          cm.code = (msg.getCode() != null) ? msg.getCode() : defCode;
          cm.message = (msg.getMessage() != null) ? msg.getMessage() : defMessage;
          return cm;
        }
      }
      else {
        Message msg = msgRuleMap.get(name);
        if (msg != null) {
          CodeMessage cm = new CodeMessage();
          cm.code = (msg.getCode() != null) ? msg.getCode() : defCode;
          cm.message = (msg.getMessage() != null) ? msg.getMessage() : defMessage;
          cm.section = (msg.getSection() != null) ? msg.getSection() : defSection;
          return cm;
        }
      }
    }
    CodeMessage cm = new CodeMessage();
    cm.code = defCode;
    cm.message = defMessage;
    cm.section = defSection;
    return cm;
  }

  /*
   * Helper function to expand the rules with an Array of fields...(multi-field-rules)
   */
  private void makeRule(List<Validation> rulelist, Map<String, String> mapping, String rulename, RuleSet ruleset,
      FieldValidation validation, Rule rule, Map<String, Message> msgRuleMap, Map<String, Message> msgFieldLenMap,
      String section) {
    for (int j = 0; j < validation.getFieldList().size(); j++) {
      String field = validation.getFieldList().get(j);

      CodeMessage m = new CodeMessage();
      // Added extra check so that if the rule is a length rule the
      // resolveMessageAndSection method does not need to be called unnecessarily.
      // Just sets the correct values
      if (rule.getName().endsWith(".len") || rule.getName().endsWith(".minLen") || rule.getName().endsWith(".maxLen")) {
        m.code = rule.getCode();
        m.section = rule.getSection();
        m.message = rule.getMessage();
      } else {
        m = resolveMessageAndSection(rule.getName(), field, msgRuleMap, msgFieldLenMap, rule.getCode(),
            rule.getMessage(), rule.getSection());
      }

      if ((section != null && m.section != null && m.section.contains(section)) || // Ensure no sections are null and
                                                                                   // that the current rule section is
                                                                                   // applicable (i.e. in OnSection tag)
          (m.section == null) // If the current rule has no onSection -> should always apply (i.e. Section H)
      ) {
        if (rulename != null) {
          String indexedRuleName;
          if (validation.getFieldList().size() == 1)
            indexedRuleName = rule.getName();
          else
            indexedRuleName = rule.getName() + ':' + Integer.toString(j + 1);
          if (rulename.equals(indexedRuleName))
            rulelist.add(new Validation(mapping, ruleset.getScope(), field, rule.getType(), m.code, m.message,
                rule.getValidation(), rule.getAssertion(), rule.getCategories(), rule.getNotCategories()));
        } else {
          rulelist.add(new Validation(mapping, ruleset.getScope(), field, rule.getType(), m.code, m.message,
              rule.getValidation(), rule.getAssertion(), rule.getCategories(), rule.getNotCategories()));
        }
      }
    }
  }

  private RuleType getRuleType(Map<String, String> mapping, String ruleConfig, RuleType ruleDefault) {
    RuleType result = ruleDefault;
    if (mapping.containsKey(ruleConfig)) {
      String errorType = mapping.get(ruleConfig);
      if (errorType != null) {
        if (errorType.equals("ERROR"))
          result = RuleType.Error;
        if (errorType.equals("WARNING"))
          result = RuleType.Warning;
      }
    }
    return result;
  }

  private Rule createRuleForErrorType(RuleType ruleType, String name, String code, String message,
                                      IAssertionFunction assertion, String assertionDSL) {
    if (ruleType.equals(RuleType.Error)) {
      return new Failure(
              name,
              code,
              message,
              assertion, assertionDSL, null);
    }
    else
    if (ruleType.equals(RuleType.Warning)) {
      return new Warning(
              name,
              code,
              message,
              assertion, assertionDSL, null);
    }
    return new Ignore(name, null);
  }

  private void setLenRule(Map<String, String> mapping, FieldValidation validation, RuleSet ruleset,
                          Set<String> nameList, Map<String, Message> msgRuleMap, Map<String, Message> msgFieldLenMap,
                          String fieldName, String ruleNameFilter,
                          IValidationRules.ValidationList validationList) {
    if (validation.getLen() != null) {
      String newRuleName = fieldName + ".len";
      if(!nameList.contains(newRuleName) && (ruleNameFilter == null || ruleNameFilter.equals(newRuleName))) {
        RuleType ruleType = getRuleType(mapping, "_lenErrorType", RuleType.Error);
        Rule lenRule = createRuleForErrorType(
                ruleType, newRuleName,
                "L01",
                "The field " + fieldName + " length is incorrect and must be " + validation.getLen().toString() + " characters in length",
                new isWrongLength(validation.getLen()),
                "isWrongLength(" + validation.getLen() + ")");
        makeRule(validationList, mapping, null, ruleset, new FieldValidation(fieldName), lenRule, msgRuleMap, msgFieldLenMap, null);
      }
    }
    if (validation.getMinLen() != null && validation.getMinLen() > 1) {
      String newRuleName = fieldName + ".minLen";
      if(!nameList.contains(newRuleName) && (ruleNameFilter == null || ruleNameFilter.equals(newRuleName))) {
        RuleType ruleType = getRuleType(mapping, "_minLenErrorType", RuleType.Error);
        Rule lenRule = createRuleForErrorType(
                ruleType, newRuleName,
                "L02",
                "The field " + fieldName + " is too short, it must be at least " + validation.getMinLen().toString() + " characters in length",
                new isTooShort(validation.getMinLen()),
                "isTooShort(" + validation.getMinLen() + ")");

        makeRule(validationList, mapping, null, ruleset, new FieldValidation(fieldName), lenRule, msgRuleMap, msgFieldLenMap, null);
      }
    }
    if (validation.getMaxLen() != null) {
      String newRuleName = fieldName + ".maxLen";
      if(!nameList.contains(newRuleName) && (ruleNameFilter == null || ruleNameFilter.equals(newRuleName))) {
        RuleType ruleType = getRuleType(mapping, "_maxLenErrorType", RuleType.Warning);
        String message = "The field " + fieldName;
        if (ruleType.equals(RuleType.Error)) {
          message += " is too long and must be shortened to ";
        }
        else {
          message += " is too long and will be shortened to ";
        }
        message += validation.getMaxLen().toString() + " characters";
        Rule lenRule = createRuleForErrorType(
                ruleType, newRuleName,
                "L03",
                message,
                new isTooLong(validation.getMaxLen()),
                "isTooLong(" + validation.getMaxLen() + ")");

        makeRule(validationList, mapping, null, ruleset, new FieldValidation(fieldName), lenRule, msgRuleMap, msgFieldLenMap, null);
      }
    }
  }

  private String getRulenameWithoutColon(String rulename) {
    String rulenameWithoutColon = null;
    if (rulename != null) {
      if (rulename.indexOf(':') >= 0) {
        rulenameWithoutColon = rulename.substring(0, rulename.indexOf(':'));
      }
    }
    return rulenameWithoutColon;
  }

  private List<FieldValidation> getValidationsForRulename(RuleSet ruleset, String rulename, String rulenameWithoutColon)  {
    List<FieldValidation> validationList;
    if (rulenameWithoutColon != null) {// compound rules...
      validationList = ruleset.getValidationsForName(rulenameWithoutColon);
    } else {
      String suffix = ".len";
      if (rulename.endsWith(suffix)) {
        validationList = ruleset.getValidationsForName(rulename.substring(0, rulename.length()-suffix.length()));
      }
      else {
        suffix = ".minLen";
        if (rulename.endsWith(suffix)) {
          validationList = ruleset.getValidationsForName(rulename.substring(0, rulename.length()-suffix.length()));
        }
        else {
          suffix = ".maxLen";
          if (rulename.endsWith(suffix)) {
            validationList = ruleset.getValidationsForName(rulename.substring(0, rulename.length()-suffix.length()));
          }
          else {
            validationList = ruleset.getValidationsForName(rulename);
          }
        }
      }
    }
    return validationList;
  }


  @Override
  public IValidationRules.ValidationList validationRules(Map<String, String> mapping, FlowType flow, String section, List<String> categories, List<String> mainCategories, String rulename) {
    IValidationRules.ValidationList result = new IValidationRules.ValidationList();
    Set<String> nameList = new HashSet<String>();
    Map<String, Message> msgRuleMap = new HashMap<String, Message>();
    Map<String, Message> msgFieldLenMap = new HashMap<String, Message>();
    for (RuleSet ruleset : ruleSets) {
      List<FieldValidation> validationList;
      String rulenameWithoutColon = getRulenameWithoutColon(rulename);
      if (rulename != null) {
        validationList = getValidationsForRulename(ruleset, rulename, rulenameWithoutColon);
      }
      else {
        validationList = ruleset.getValidations();
      }

      if (validationList != null) {
        for (FieldValidation validation : ruleset.getValidations()) {
          for (Rule rule : validation.getRules()) {
            if (!nameList.contains(rule.getName())) {
              if (rule.getType() == RuleType.Message) {
                if (flow != null && rule.getFlow() != null && !rule.getFlow().equals(flow))
                  continue;

                if (rule.getName().endsWith(".len") ||
                        rule.getName().endsWith(".minLen") ||
                        rule.getName().endsWith(".maxLen")) {
                  msgFieldLenMap.put(rule.getName(), (Message) rule);
                } else {
                  msgRuleMap.put(rule.getName(), (Message) rule);
                }
                continue;
              } else
                nameList.add(rule.getName());

              // if there is a rulename, match it with rule.name, or validation.field.
              if (rulename != null && rule.getName() != null) {
                if (rulenameWithoutColon != null) {// compound rules...
                  if (!rule.getName().equals(rulenameWithoutColon))
                    continue;
                } else {
                  if (!rule.getName().equals(rulename)) {
                    if (!validation.getFieldList().contains(rulename))
                      continue;
                  }
                }
              }

              if (flow != null && rule.getFlow() != null && !rule.getFlow().equals(flow))
                continue;
            //   if (section != null && rule.getSection() != null && !rule.getSection().contains(section))
            //     continue;
              if (categories != null && rule.getCategories() != null) {
                boolean hasCategory = false;
                for (String cat : categories) {
                  if (rule.getCategories().contains(cat)) {
                    hasCategory = true;
                    break;
                  }
                }
                if (!hasCategory) {
                  for (String cat : mainCategories) {
                    if (rule.getCategories().contains(cat)) {
                      hasCategory = true;
                      break;
                    }
                  }
                }
                if (!hasCategory)
                  continue;
              }
              if (categories != null && rule.getNotCategories() != null) {
                if (!categoriesNotInList(categories, mainCategories, rule.getNotCategories()))
                  continue;
              }

              makeRule(result, mapping, rulename, ruleset, validation, rule, msgRuleMap, msgFieldLenMap, section);
            }
          }

          for (String fieldName : validation.getFieldList()) {
            setLenRule(mapping, validation, ruleset, nameList, msgRuleMap, msgFieldLenMap, fieldName, rulename, result);
          }
        }
      }
    }
    return result;
  }

  private ValidationSchema.Structure getScopeStructure(Scope scope, ValidationSchema.Structure structure) {
    if (structure == null && scope == Scope.Transaction) {
      return new ValidationSchema.Structure("Transaction", false);
    }
    if (structure != null) {
      if (scope == Scope.Transaction) {
        return structure;
      }
      else
      if (scope == Scope.Money || scope == Scope.ImportExport) {
        ValidationSchema.Structure moneyStructure = null;
        for (ValidationSchema.Node node : structure.getNodes()) {
          if (node instanceof ValidationSchema.Structure && node.getName().equals("MonetaryAmount")) {
            moneyStructure = (ValidationSchema.Structure)node;
            break;
          }
        }

        if (moneyStructure == null) {
          moneyStructure = new ValidationSchema.Structure("MonetaryAmount", true);
          structure.getNodes().add(moneyStructure);
        }

        if (scope == Scope.Money) {
          return moneyStructure;
        }
        else {
          ValidationSchema.Structure ieStructure = null;
          for (ValidationSchema.Node node : moneyStructure.getNodes()) {
            if (node instanceof ValidationSchema.Structure && node.getName().equals("ImportExport")) {
              ieStructure = (ValidationSchema.Structure)node;
              break;
            }
          }

          if (ieStructure == null) {
            ieStructure = new ValidationSchema.Structure("ImportExport", true);
            moneyStructure.getNodes().add(ieStructure);
          }
          return ieStructure;
        }
      }
    }
    return null;
  }

  private void addFieldToStructure(FieldValidation fieldVal, ValidationSchema.Structure structure) {
    for (String compoundFieldName : fieldVal.getFieldList()) {
      String[] fieldParts = compoundFieldName.split("\\.");

      ValidationSchema.Structure structPart = structure;
      for (int i=0; i<fieldParts.length-1; i++) {
        boolean bExists = false;
        for (ValidationSchema.Node node : structPart.getNodes()) {
          if (node instanceof ValidationSchema.Structure && node.getName().equals(fieldParts[i])) {
            bExists = true;
            structPart = (ValidationSchema.Structure)node;
            break;
          }
        }
        if (!bExists) {
          ValidationSchema.Structure newStruct = new ValidationSchema.Structure(fieldParts[i], false);
          structPart.getNodes().add(newStruct);
          structPart = newStruct;
        }
      }

      String fieldName = fieldParts[fieldParts.length-1];
      boolean bExists = false;
      for (ValidationSchema.Node node : structPart.getNodes()) {
        if (node instanceof ValidationSchema.FieldNode && node.getName().equals(fieldName)) {
          bExists = true;
          break;
        }
      }
      if (!bExists) {
        boolean deprecated = false;
        for (Rule rule : fieldVal.getRules()) {
          if (rule.getType() == RuleType.Deprecated) {
            deprecated = true;
            break;
          }
        }
        ValidationSchema.FieldNode fieldNode = new ValidationSchema.FieldNode(
                fieldName, fieldVal.getMinLen(), fieldVal.getMaxLen(), fieldVal.getLen(), deprecated);
        structPart.getNodes().add(fieldNode);
      }
    }
  }

  private void pruneRedundantFields(ValidationSchema.Structure structure) {
    List<String> structureNameList = new ArrayList<String>();
    List<ValidationSchema.Node> redundantNodeList = new ArrayList<ValidationSchema.Node>();

    for (ValidationSchema.Node node : structure.getNodes()) {
      if (node instanceof ValidationSchema.Structure) {
        pruneRedundantFields((ValidationSchema.Structure)node);
        structureNameList.add(node.getName());
      }
    }

    for (ValidationSchema.Node node : structure.getNodes()) {
      if (node instanceof ValidationSchema.FieldNode) {
        if (structureNameList.contains(node.getName())) {
          redundantNodeList.add(node);
        }
      }
    }
    for (ValidationSchema.Node node : redundantNodeList) {
      structure.getNodes().remove(node);
    }
  }

  private void markDeprecatedStructures(ValidationSchema.Structure structure) {
    for (ValidationSchema.Node node : structure.getNodes()) {
      if (node instanceof ValidationSchema.Structure) {
        markDeprecatedStructures((ValidationSchema.Structure)node);
      }
    }

    boolean hasDeprecated = false;
    boolean hasCurrent = false;
    for (ValidationSchema.Node node : structure.getNodes()) {
      if (node instanceof ValidationSchema.FieldNode) {
        ValidationSchema.FieldNode field = (ValidationSchema.FieldNode)node;
        if (field.getNodeType() == ValidationSchema.NodeType.Field) {
          hasCurrent = true;
        }
        else
        if (field.isDeprecated()) {
          hasDeprecated = true;
        }
      }
    }
    if (hasDeprecated && !hasCurrent) {
      structure.setDeprecated(true);
    }
  }

  public ValidationSchema.Structure getStructure() {
    ValidationSchema.Structure result = getScopeStructure(Scope.Transaction, null);

    for (RuleSet ruleset : ruleSets) {
      ValidationSchema.Structure structure = getScopeStructure(ruleset.getScope(), result);
      for (FieldValidation validation : ruleset.getValidations()) {
        addFieldToStructure(validation, structure);
      }
    }
    pruneRedundantFields(result);
    markDeprecatedStructures(result);
    return result;
  }
}
