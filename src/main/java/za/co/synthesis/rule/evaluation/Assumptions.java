package za.co.synthesis.rule.evaluation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.EvaluationDecision;
import za.co.synthesis.rule.evaluation.matchpredef.MAFAnd;
import za.co.synthesis.rule.evaluation.matchpredef.MAFOr;
import za.co.synthesis.rule.evaluation.predef.all;
import za.co.synthesis.rule.support.DSLSupport;
import za.co.synthesis.rule.support.JSConstant;
import za.co.synthesis.rule.support.StringList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/28/16.
 */
public class Assumptions {
  Logger logger = LoggerFactory.getLogger(Assumptions.class);

  private final List<Assumption> assumptions = new ArrayList<Assumption>();

  public Assumptions() {
  }

  private IAssumptionAssertionFunction loadApplyFunc(IAssumptionAssertionFunction left, JSIdentifier apply) throws Exception {
    IAssumptionAssertionFunction result = left;
    if (left != null && apply != null) {
      if (apply instanceof JSFunctionCall) {
        JSFunctionCall applyFunc = (JSFunctionCall)apply;
        if (applyFunc.getName().equals("and")) {
          result = new AAFAnd(left, loadAssertion((JSIdentifier)applyFunc.getParameters().get(0)));
        }
        else
        if (applyFunc.getName().equals("or")) {
          result = new AAFOr(left, loadAssertion((JSIdentifier)applyFunc.getParameters().get(0)));
        }

        result = loadApplyFunc(result, applyFunc.getApply());
      }
    }
    return result;
  }

  private IAssumptionAssertionFunction loadAssertion(JSIdentifier identifier) throws Exception {
    IAssumptionAssertionFunction result = null;
    if (identifier instanceof JSFunctionCall) {
      JSFunctionCall func = (JSFunctionCall)identifier;

      if (func.getParameters().size() == 1) {
        Object param = func.getParameters().get(0);
        if (param instanceof JSArray) {
          result = AssumptionFunctionFactory.createFunction(identifier.getName(), (List<Object>)param);
        }
        else {
          if (param instanceof String)
            result = AssumptionFunctionFactory.createFunction(identifier.getName(), (String) param);
          else
          if (param instanceof JSRegExLiteral)
            result = AssumptionFunctionFactory.createFunction(identifier.getName(), (JSRegExLiteral) param);
          else
          if (param instanceof JSIdentifier) {
            JSIdentifier jsParamIdentifier = (JSIdentifier)param;
            if (jsParamIdentifier.getApply() == null || param instanceof JSFunctionCall) {
              // We assume that anything that is a function call or has no trailing . is a function
              result = AssumptionFunctionFactory.createFunction(identifier.getName(), loadAssertion(jsParamIdentifier));
            }
            else {
              result = AssumptionFunctionFactory.createFunction(identifier.getName(), jsParamIdentifier);
            }
          }
          else {
            result = AssumptionFunctionFactory.createFunction(identifier.getName(), param.toString());
          }
        }
      }

      if (func.getParameters().size() == 2) {
        String param1 = func.getParameters().get(0).toString();
        Object param2 = func.getParameters().get(1);
        if (param2 instanceof JSArray) {
          StringList params = DSLSupport.composeStringList(param2);
          result = AssumptionFunctionFactory.createFunction(identifier.getName(), param1, params);
        }
        else
        if (param2 instanceof JSIdentifier) {
          result = AssumptionFunctionFactory.createFunction(identifier.getName(), param1, loadAssertion((JSIdentifier) param2));
        }
        else {
          result = AssumptionFunctionFactory.createFunction(identifier.getName(), param1, param2.toString());
        }
      }
    }
    else {
      result = AssumptionFunctionFactory.createFunction(identifier.getName());
    }

    result = loadApplyFunc(result, identifier.getApply());
    return result;
  }

  private IAssumptionAssertionFunction getAssertion(JSFunctionCall jsFunc, int paramIndex) throws Exception {
    if (jsFunc.getParameters().size() > paramIndex) {
      Object objExp = jsFunc.getParameters().get(paramIndex);
      if (objExp instanceof JSIdentifier) {
        return loadAssertion((JSIdentifier)objExp);
      }
    }
    return null;
  }

  private IMatchAssertionFunction loadMatchApplyFunc(IMatchAssertionFunction left, JSIdentifier apply) throws Exception {
    IMatchAssertionFunction result = left;
    if (left != null && apply != null) {
      if (apply instanceof JSFunctionCall) {
        JSFunctionCall applyFunc = (JSFunctionCall)apply;
        if (applyFunc.getName().equals("and")) {
          result = new MAFAnd(left, loadMatchAssertion((JSIdentifier)applyFunc.getParameters().get(0)));
        }
        else
        if (applyFunc.getName().equals("or")) {
          result = new MAFOr(left, loadMatchAssertion((JSIdentifier)applyFunc.getParameters().get(0)));
        }

        result = loadMatchApplyFunc(result, applyFunc.getApply());
      }
    }
    return result;
  }

  private IMatchAssertionFunction loadMatchAssertion(JSIdentifier identifier) throws Exception {
    IMatchAssertionFunction result = null;
    if (identifier instanceof JSFunctionCall) {
      JSFunctionCall func = (JSFunctionCall) identifier;

      if (func.getParameters().size() == 0) {
        result = MatchFunctionFactory.createFunction(identifier.getName());
      }
      else
      if (func.getParameters().size() == 1) {
        Object param = func.getParameters().get(0);
        if (param instanceof JSArray) {
          result = MatchFunctionFactory.createFunction(identifier.getName(), (List<Object>)param);
        } else {
          if (param instanceof String)
            result = MatchFunctionFactory.createFunction(identifier.getName(), (String) param);
          else if (param instanceof JSRegExLiteral)
            result = MatchFunctionFactory.createFunction(identifier.getName(), (JSRegExLiteral) param);
          else if (param instanceof JSIdentifier) {
            JSIdentifier jsParamIdentifier = (JSIdentifier)param;
            if (jsParamIdentifier.getApply() == null || param instanceof JSFunctionCall) {
              // We assume that anything that is a function call or has no trailing . is a function
              result = MatchFunctionFactory.createFunction(identifier.getName(), loadMatchAssertion(jsParamIdentifier));
            }
            else {
              result = MatchFunctionFactory.createFunction(identifier.getName(), jsParamIdentifier);
            }
          }
          else {
            result = MatchFunctionFactory.createFunction(identifier.getName(), param.toString());
          }
        }
      }
      else if (func.getParameters().size() == 3) {
        Object param1 = func.getParameters().get(0);
        Object param2 = func.getParameters().get(1);
        Object param3 = func.getParameters().get(2);
        if (param1 instanceof String && param2 instanceof String) {
          if (param3 instanceof JSArray) {
            result = MatchFunctionFactory.createFunction(identifier.getName(), (String)param1, (String)param2, DSLSupport.composeStringList(param3));
          } else if (param3 instanceof String) {
            result = MatchFunctionFactory.createFunction(identifier.getName(), (String) param1, (String) param2, (String) param3);
          }
        }
      }
    }
    else {
      result = MatchFunctionFactory.createFunction(identifier.getName());
    }

    result = loadMatchApplyFunc(result, identifier.getApply());
    return result;
  }

  private IMatchAssertionFunction getMatch(JSFunctionCall jsFunc, int paramIndex) throws Exception {
    if (jsFunc.getParameters().size() > paramIndex) {
      Object objExp = jsFunc.getParameters().get(paramIndex);
      if (objExp instanceof JSIdentifier) {
        return loadMatchAssertion((JSIdentifier)objExp);
      }
    }
    return null;
  }

  private EvaluationDecision loadDecision(JSObject jsDecision) {
    Object reportable = jsDecision.get("reportable");
    Object flow = jsDecision.get("flow");
    Object reportingSide = jsDecision.get("reportingSide");
    Object nonResSide = jsDecision.get("nonResSide");
    Object nonResAccountType = jsDecision.get("nonResAccountType");
    Object nonResException = jsDecision.get("nonResException");
    Object resSide = jsDecision.get("resSide");
    Object resAccountType = jsDecision.get("resAccountType");
    Object resException = jsDecision.get("resException");
    Object manualSection = jsDecision.get("manualSection");
    Object drSideSwift = jsDecision.get("drSideSwift");

    EvaluationDecision result = new EvaluationDecision(null,
            JSConstant.reportableType(reportable),
            JSConstant.drCr(reportingSide),
            JSConstant.flowType(flow),
            JSConstant.drCr(resSide), JSConstant.reportingAccountType(resAccountType), resException != null ? resException.toString() : null,
            JSConstant.drCr(nonResSide), JSConstant.reportingAccountType(nonResAccountType), nonResException != null ? nonResException.toString() : null,
            manualSection != null ? manualSection.toString() : null);

    result.setDrSideSwift(drSideSwift != null ? drSideSwift.toString() : null);
    return result;
  }

  private AssumptionRule loadRule(JSFunctionCall jsFunc) throws Exception {
    AssumptionRule result = null;

    if (jsFunc.getName().equals("use") && jsFunc.getParameters().size() == 3) {
      ResidenceStatus resStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(0));
      BankAccountType accType = JSConstant.bankAccountType(jsFunc.getParameters().get(1));
      IAssumptionAssertionFunction assertion = getAssertion(jsFunc, 2);
      if (assertion != null) {
        result = new AssumptionRule(AssumptionRuleType.Use, resStatus, accType, assertion);
      }
    }
    else
    if (jsFunc.getName().equals("useCr") && jsFunc.getParameters().size() == 3) {
      ResidenceStatus resStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(0));
      BankAccountType accType = JSConstant.bankAccountType(jsFunc.getParameters().get(1));
      IAssumptionAssertionFunction assertion = getAssertion(jsFunc, 2);
      if (assertion != null) {
        result = new AssumptionRule(AssumptionRuleType.UseCr, resStatus, accType, assertion);
      }
    }
    else
    if (jsFunc.getName().equals("useDr") && jsFunc.getParameters().size() == 3) {
      ResidenceStatus resStatus = JSConstant.residenceStatus(jsFunc.getParameters().get(0));
      BankAccountType accType = JSConstant.bankAccountType(jsFunc.getParameters().get(1));
      IAssumptionAssertionFunction assertion = getAssertion(jsFunc, 2);
      if (assertion != null) {
        result = new AssumptionRule(AssumptionRuleType.UseDr, resStatus, accType, assertion);
      }
    }
    else
    if (jsFunc.getName().equals("useBoth") && jsFunc.getParameters().size() == 0) {
      IAssumptionAssertionFunction assertion = new all();
      result = new AssumptionRule(AssumptionRuleType.UseBoth, null, null, assertion);
    }
    else
    if (jsFunc.getName().equals("useBoth") && jsFunc.getParameters().size() == 1) {
      IAssumptionAssertionFunction assertion = getAssertion(jsFunc, 0);
      if (assertion != null) {
        result = new AssumptionRule(AssumptionRuleType.UseBoth, null, null, assertion);
      }
    }
    else
    if (jsFunc.getName().equals("decideCr") && jsFunc.getParameters().size() == 2) {
      Object param1 = jsFunc.getParameters().get(0);
      if (param1 instanceof JSObject) {
        EvaluationDecision decision = loadDecision((JSObject)param1);
        IAssumptionAssertionFunction assertion = getAssertion(jsFunc, 1);
        if (assertion != null) {
          result = new AssumptionRule(AssumptionRuleType.DecideCr, decision, assertion);
        }
      }
    }
    else
    if (jsFunc.getName().equals("decideDr") && jsFunc.getParameters().size() == 2) {
      Object param1 = jsFunc.getParameters().get(0);
      if (param1 instanceof JSObject) {
        EvaluationDecision decision = loadDecision((JSObject)param1);
        IAssumptionAssertionFunction assertion = getAssertion(jsFunc, 1);
        if (assertion != null) {
          result = new AssumptionRule(AssumptionRuleType.DecideDr, decision, assertion);
        }
      }
    }
    else
    if (jsFunc.getName().equals("decide") && jsFunc.getParameters().size() == 1) {
      Object param1 = jsFunc.getParameters().get(0);
      if (param1 instanceof JSObject) {
        EvaluationDecision decision = loadDecision((JSObject)param1);
        result = new AssumptionRule(AssumptionRuleType.DecideCr, decision, new all());
      }
    }
    return result;
  }

  private List<AssumptionRule> loadRules(JSArray jsRules) {
    List<AssumptionRule> result = new ArrayList<AssumptionRule>();
    if (jsRules != null) {
      for (Object objRule : jsRules) {
        if (objRule instanceof JSFunctionCall) {
          JSFunctionCall jsFunc = (JSFunctionCall)objRule;
          try {
            AssumptionRule rule = loadRule(jsFunc);
            if (rule != null) {
              result.add(rule);
            }
          } catch (Exception e) {
            logger.info("Error loading rule ", e);
            System.out.println("Error loading rule: " + e.getMessage());
          }
        }
      }
    }
    return result;
  }

  private List<BankAccountType> bankAccountTypeList(Object obj) {
    List<BankAccountType> result = new ArrayList<BankAccountType>();
    if (obj instanceof JSArray) {
      for (Object value : (JSArray)obj) {
        BankAccountType type = JSConstant.bankAccountType(value);
        if (type != null && type != BankAccountType.Unknown)
          result.add(type);
      }
    }
    else {
      BankAccountType type = JSConstant.bankAccountType(obj);
      if (type != null && type != BankAccountType.Unknown)
        result.add(type);
    }
    return result;
  }

  public void loadAssumptions(JSArray jsAssumptions) {
    List<Assumption> localList = new ArrayList<Assumption>();

    for (Object obj : jsAssumptions) {
      if (obj instanceof JSObject) {
        JSObject jsEvaluation = (JSObject) obj;
        Object jsScenario = jsEvaluation.get("scenario");
        String scenarioStr = jsScenario != null ? jsScenario.toString() : "";

        Object jsRules = jsEvaluation.get("rules");
        List<AssumptionRule> rules = null;

        if (jsRules instanceof JSArray) {
          rules = loadRules((JSArray) jsRules);
        }

        if (rules != null) {
          Object jsMatch = jsEvaluation.get("match");

          if (jsMatch != null) {
            if (jsMatch instanceof JSIdentifier) {
              // Load NEW style assumption rules with match criteria
              try {
                Assumption assumption = new Assumption(jsScenario != null ? jsScenario.toString() : "",
                        loadMatchAssertion((JSIdentifier) jsMatch),
                        rules);
                localList.add(assumption);
              }
              catch (Exception e) {
                logger.error("Error loading assumption match rule for scenario '" + scenarioStr + "'", e);
                System.out.println("Error loading assumption match rule for scenario '" + scenarioStr + "'" + e.getMessage());
              }
            }
          }
          else {
            // Load OLD style assumption rules with a known side definition
            Object jsKnownSide = jsEvaluation.get("knownSide");
            Object jsResStatus = jsEvaluation.get("resStatus");
            Object jsAccType = jsEvaluation.get("accType");

            if (jsKnownSide != null) {
              DrCr knownSide = JSConstant.drCr(jsKnownSide);

              Assumption assumption = new Assumption(jsScenario != null ? jsScenario.toString() : "",
                      knownSide,
                      JSConstant.residenceStatus(jsResStatus),
                      bankAccountTypeList(jsAccType),
                      rules);
              localList.add(assumption);
            }
          }
        }
      }
    }
    assumptions.addAll(localList);
  }

  public List<Assumption> assumptionRules() {
    return assumptions;
  }
}
