package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 2/25/15
 * Time: 5:31 PM
 * The potential status return types for validation functions
 */
public enum StatusType {
  Pass,
  Fail,
  Error
}
