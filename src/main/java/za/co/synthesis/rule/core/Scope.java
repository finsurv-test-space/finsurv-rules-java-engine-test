package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 3:46 PM
 * The scope under which the rule will run
 */
public enum Scope {
  Transaction("transaction"),
  Money("money"),
  ImportExport("importexport");

  private final String name;
  Scope(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return name;
  }
}
