package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 9:46 AM
 * Determines if the resident exception name does not match the given constant
 */
public class notResException implements IAssertionFunction {
  private String constant;

  public notResException(String constant) {
    this.constant = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object exception = context.getTransactionField("Resident.Exception.ExceptionName");
    if (exception != null)
      return !exception.equals(constant);
    return true;
  }
}
/*
function notResException(type) {
  return function(context, value) {
    var exception = context.getTransactionField("Resident.Exception.ExceptionName");
    if (exception)
      return exception != type;
    return true;
  }
}
*/