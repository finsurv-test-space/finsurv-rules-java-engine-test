package za.co.synthesis.rule.doc;

import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.ValidationEngine;
import za.co.synthesis.rule.support.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationDocs {
  private static class PackageMessage extends Message {
    private final String packageName;

    public PackageMessage(String packageName, String name, String code, String message, String ruleDSL) {
      super(name, code, message, ruleDSL);
      this.packageName = packageName;
    }

    public String getPackageName() {
      return packageName;
    }
  }

  private final ValidationEngine engine;
  private final ValidationTest engineTests;

  public ValidationDocs(ValidationEngine engine, ValidationTest engineTests) {
    this.engine = engine;
    this.engineTests = engineTests;
  }

  private String commonFieldLabel(Map<String, String> mapping, List<String> configuredFields) {
    List<String> fields = new ArrayList<String>();
    for (String fieldName : configuredFields) {
      fields.add(DSLSupport.applyMappingToStr(mapping, fieldName));
    }
    if (fields.size() == 1) {
      return fields.get(0);
    }

    int commonFromStart = -1;
    int commonFromEnd = -1;

    String refStr = fields.get(0);
    int refLen = refStr.length();

    // Find the commonStart
    for (int i = 0; i < refLen; i++) {
      boolean common = true;
      char refChar = refStr.charAt(i);
      for (int j=1; j<fields.size() && common; j++) {
        String otherStr = fields.get(j);
        if (i < otherStr.length()) {
          char otherChar = otherStr.charAt(i);
          if (refChar != otherChar) {
            common = false;
          }
        } else {
          common = false;
        }
      }
      if (!common) {
        break;
      }
      commonFromStart = i;
    }

    // Find the commonFromEnd
    for (int i = 0; i < refLen; i++) {
      boolean common = true;
      char refChar = refStr.charAt(refLen - i - 1);
      for (int j=1; j<fields.size() && common; j++) {
        String otherStr = fields.get(j);
        int otherLen = otherStr.length();
        if (i < otherLen) {
          char otherChar = otherStr.charAt(otherLen - i - 1);
          if (refChar != otherChar) {
            common = false;
          }
        }
        else {
          common = false;
        }
      }
      if (!common) {
        break;
      }
      commonFromEnd = i;
    }

    String result = null;
    if (commonFromStart != -1) {
      result = refStr.substring(0, commonFromStart+1);
    }
    if (commonFromEnd != -1) {
      if (result != null) {
        result += " ... ";
      }
      else {
        result = "... ";
      }
      result += refStr.substring(refLen-commonFromEnd-1, refLen);
    }
    else {
      if (result != null) {
        result += " ...";
      }
      else {
        result = refStr;
      }
    }
    return result;
  }

  private SourceStatus getSourceStatus(final String enginePackage, final String sourcePackage) {
    if(sourcePackage.equals(enginePackage)) {
      return SourceStatus.Defined;
    }
    else {
      return SourceStatus.Inherited;
    }
  }

  /***
   * Error Code and Error Message on specific rules can be overidden and this is then injected into the rule information.
   * This update must reflect that the code/message has been updated
   * @param ruleInfo - an existing rule that needs to be updated
   * @param message
   */
  private void mergeMessageIntoRuleInfo(RuleInfo ruleInfo, PackageMessage message) {
    if (message.getCode() != null) {
      ruleInfo.setErrorCode(message.getCode());
    }
    if (message.getMessage() != null) {
      ruleInfo.setErrorMessage(message.getMessage());
    }
    ruleInfo.setMessageChannel(message.getPackageName());
    ruleInfo.setMessageStatus(SourceStatus.Overriden);
  }

  private RuleInfo composeNewRuleInfo(final String enginePackage, RuleSet ruleSet, Rule rule, FieldInfo fieldInfo,
                                      List<ValidationTest.RuleTestResult> testResults) {
    RuleInfo ruleInfo = new RuleInfo();
    ruleInfo.setType(rule.getType());
    ruleInfo.setRuleName(rule.getName());
    ruleInfo.setRuleStatus(getSourceStatus(enginePackage, ruleSet.getPackageName()));
    ruleInfo.setRuleChannel(ruleSet.getPackageName());
    ruleInfo.setFieldInfo(fieldInfo);

    if (rule.getType() == RuleType.Error ||
            rule.getType() == RuleType.Warning ||
            rule.getType() == RuleType.Deprecated ||
            rule.getType() == RuleType.Validate ) {
      ruleInfo.setMessageStatus(getSourceStatus(enginePackage, ruleSet.getPackageName()));
      ruleInfo.setMessageChannel(ruleSet.getPackageName());
      if (rule.getType() == RuleType.Validate) {
        ruleInfo.setValidation(rule.getValidation());
      }
      else {
        ruleInfo.setErrorCode(rule.getCode());
        ruleInfo.setErrorMessage(rule.getMessage());
      }
      ruleInfo.setFilterOnCategory(
              (rule.getCategories() != null && rule.getCategories().size() > 0) ||
                      (rule.getNotCategories() != null && rule.getNotCategories().size() > 0));
      ruleInfo.setOnCategory(rule.getCategories());
      ruleInfo.setNotOnCategory(rule.getNotCategories());
      ruleInfo.setFilterOnFlow(rule.getFlow() != null);
      ruleInfo.setOnFlow(rule.getFlow());
      ruleInfo.setFilterOnSection(rule.getSection() != null && rule.getSection().length() > 0);
      ruleInfo.setOnSection(rule.getSection());
      ruleInfo.setAssertionDSL(rule.getAssertionDSL());
      ruleInfo.setRuleDSL(rule.getRuleDSL());
    }

    int testsCount = testResults == null ? 0 : testResults.size();
    int testsPassed = 0;
    int testsPositive = 0;
    if (testResults != null) {
      for (ValidationTest.RuleTestResult testResult : testResults) {
        if (testResult.getTestClass().equals(ValidationTest.TestClass.Positive)) {
          testsPositive++;
        }
        if (testResult.getErrorList().size() == 0) {
          testsPassed++;
        }
      }
    }
    ruleInfo.setTestStatus(testsCount == 0 ?
            TestStatus.NoTestDefined : (testsCount - testsPassed == 0) ? TestStatus.Passed : TestStatus.Failed);
    ruleInfo.setPositiveTestCount(testsPositive);
    ruleInfo.setNegativeTestCount(testsCount - testsPositive);

    return ruleInfo;
  }

  private void mergeOveriddenRuleInfo(RuleInfo ruleInfo, final String enginePackage, RuleSet ruleSet, Rule rule, FieldInfo fieldInfo) {
    if (ruleInfo.getRuleStatus() == SourceStatus.Defined) {
      // Update the underlying data that is being overidden
      ruleInfo.setRuleStatus(SourceStatus.Overriden);
      ruleInfo.setParentRuleChannel(ruleSet.getPackageName());
      if (ruleInfo.getType() == RuleType.Ignore) {
        // If a rule is ignored then fill in the underlying detail that is being squashed
        ruleInfo.setMessageStatus(getSourceStatus(enginePackage, ruleSet.getPackageName()));
        ruleInfo.setMessageChannel(ruleSet.getPackageName());
        ruleInfo.setFieldInfo(fieldInfo);
        if (rule.getType() == RuleType.Validate) {
          ruleInfo.setValidation(rule.getValidation());
        }
        else {
          ruleInfo.setErrorCode(rule.getCode());
          ruleInfo.setErrorMessage(rule.getMessage());
        }
        ruleInfo.setFilterOnCategory(
                (rule.getCategories() != null && rule.getCategories().size() > 0) ||
                        (rule.getNotCategories() != null && rule.getNotCategories().size() > 0));
        ruleInfo.setOnCategory(rule.getCategories());
        ruleInfo.setNotOnCategory(rule.getNotCategories());
        ruleInfo.setFilterOnFlow(rule.getFlow() != null);
        ruleInfo.setOnFlow(rule.getFlow());
        ruleInfo.setFilterOnSection(rule.getSection() != null && rule.getSection().length() > 0);
        ruleInfo.setOnSection(rule.getSection());
        ruleInfo.setAssertionDSL(rule.getAssertionDSL());
        ruleInfo.setRuleDSL(rule.getRuleDSL());
      }
    }
  }

  private FieldInfo composeNewFieldInfo(Map<String, String> mapping, RuleSet ruleSet, FieldValidation field) {
    FieldInfo fieldInfo = new FieldInfo();
    fieldInfo.setScope(ruleSet.getScope());
    fieldInfo.setChannel(ruleSet.getPackageName());
    fieldInfo.setFieldNames(field.getFieldList());
    fieldInfo.setCommonFieldLabel(commonFieldLabel(mapping, field.getFieldList()));
    fieldInfo.setMinLen(field.getMinLen());
    fieldInfo.setMaxLen(field.getMaxLen());
    fieldInfo.setLen(field.getLen());

    return fieldInfo;
  }

  private void mergeOveriddenFieldInfo(FieldInfo fieldInfo, RuleSet ruleSet, FieldValidation field) {
    if(fieldInfo.getLen() == null && fieldInfo.getMinLen() == null && fieldInfo.getMaxLen() == null) {
      if (field.getLen() != null || field.getMinLen() != null || field.getMaxLen() != null) {
        fieldInfo.setChannel(ruleSet.getPackageName());
        fieldInfo.setMinLen(field.getMinLen());
        fieldInfo.setMaxLen(field.getMaxLen());
        fieldInfo.setLen(field.getLen());
      }
    }
  }

  public List<RuleInfo> getRuleInfoList() {
    List<RuleInfo> result = new ArrayList<RuleInfo>();
    Map<String, RuleInfo> ruleMap = new HashMap<String, RuleInfo>();
    Map<String, PackageMessage> msgRuleMap = new HashMap<String, PackageMessage>();

    for (RuleSet ruleSet : engine.getRuleSets()) {
      for (FieldValidation field : ruleSet.getValidations()) {
        FieldInfo fieldInfo = composeNewFieldInfo(engine.getMappings(), ruleSet, field);

        for (Rule rule : field.getRules()) {
          if (rule.getType() == RuleType.Message) {
            if (!msgRuleMap.containsKey(rule.getName())) {
              msgRuleMap.put(rule.getName(),
                      new PackageMessage(ruleSet.getPackageName(),
                              rule.getName(), rule.getCode(), rule.getMessage(), rule.getRuleDSL()));
            }
          } else if (rule.getType() == RuleType.Error ||
                  rule.getType() == RuleType.Warning ||
                  rule.getType() == RuleType.Validate ||
                  rule.getType() == RuleType.Ignore ||
                  rule.getType() == RuleType.Deprecated) {
            String ruleName = rule.getName();
            if (ruleMap.containsKey(ruleName)) {
              RuleInfo ruleInfo = ruleMap.get(ruleName);
              mergeOveriddenFieldInfo(ruleInfo.getFieldInfo(), ruleSet, field);
              mergeOveriddenRuleInfo(ruleInfo, engine.getPackageName(), ruleSet, rule, fieldInfo);
            } else {
              RuleInfo ruleInfo = composeNewRuleInfo(engine.getPackageName(), ruleSet, rule,
                      fieldInfo, engineTests.getResultsByRulename(ruleName));
              ruleMap.put(ruleName, ruleInfo);
              result.add(ruleInfo);
            }
          }
        }
      }
    }

    for(PackageMessage packageMessage : msgRuleMap.values()) {
      RuleInfo ruleInfo = ruleMap.get(packageMessage.getName());

      mergeMessageIntoRuleInfo(ruleInfo, packageMessage);
    }
    return result;
  }

  private RuleTest createRuleTest(String ruleName, FieldValidation field, ValidationTest.RuleTestResult testResult) {
    RuleTest test = new RuleTest();
    test.setRuleName(ruleName);
    test.setFieldName(field.getFieldList().get(testResult.getFieldIndex()));
    test.setTestStatus(testResult.getErrorList().size() == 0 ? TestStatus.Passed : TestStatus.Failed);
    if (testResult.getErrorList().size() > 0) {
      test.setFailReason(testResult.getErrorList().get(0));
    }

    String dsl = testResult.getTestDSL();
    JSStructureParser parser = new JSStructureParser(dsl);
    try {
      Object obj = parser.parse();
      if (obj instanceof JSScope) {
        JSScope jsScope = (JSScope)obj;
        JSFunctionCall jsFunc = null;
        for (Object var : jsScope.values()) {
          if (var instanceof JSFunctionCall) {
            jsFunc = (JSFunctionCall)var;
          }
        }

        test.setAssertFunc(jsFunc.getName());
        if (jsFunc.getParameters().size() >= 2) {
          JSObject jsObj = (JSObject) jsFunc.getParameters().get(1);
          JSWriter write = new JSWriter();
          jsObj.removeExtraWhitespace();
          jsObj.composeSnippets(write);
          test.setReportData(write.toString());
        }
        if (jsFunc.getParameters().size() >= 3) {
          JSObject jsObj = (JSObject) jsFunc.getParameters().get(2);
          JSWriter write = new JSWriter();
          jsObj.removeExtraWhitespace();
          jsObj.composeSnippets(write);
          test.setCustomData(write.toString());
        }
      }
    } catch (Exception e) {
      test.setAssertFunc(dsl);
    }
    return test;
  }

  public List<RuleTest> getRuleTestList() {
    List<RuleTest> result = new ArrayList<RuleTest>();

    for (RuleSet ruleSet : engine.getRuleSets()) {
      for (FieldValidation field : ruleSet.getValidations()) {
        for (Rule rule : field.getRules()) {
          String ruleName = rule.getName();
          List<ValidationTest.RuleTestResult> testResults = engineTests.getResultsByRulename(ruleName);
          if (testResults != null) {
            for (ValidationTest.RuleTestResult testResult : testResults) {
              result.add(createRuleTest(ruleName, field, testResult));
            }
          }
        }
      }
    }
    return result;
  }

  public List<RuleTest> getRuleTestListByRulename(String ruleName) {
    List<RuleTest> result = new ArrayList<RuleTest>();

    FieldValidation ruleField = null;
    for (RuleSet ruleSet : engine.getRuleSets()) {
      for (FieldValidation field : ruleSet.getValidations()) {
        for (Rule rule : field.getRules()) {
          if (rule.getName().equals(ruleName)) {
            ruleField = field;
            break;
          }
        }
        if (ruleField != null)
          break;
      }
      if (ruleField != null)
        break;
    }

    List<ValidationTest.RuleTestResult> testResults = engineTests.getResultsByRulename(ruleName);
    if (testResults != null) {
      for (ValidationTest.RuleTestResult testResult : testResults) {
        result.add(createRuleTest(ruleName, ruleField, testResult));
      }
    }
    return result;
  }
}
