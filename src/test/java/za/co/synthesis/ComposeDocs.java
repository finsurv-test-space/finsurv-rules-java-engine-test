package za.co.synthesis;

import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.doc.RuleInfo;
import za.co.synthesis.rule.doc.RuleTest;
import za.co.synthesis.rule.doc.ValidationDocs;
import za.co.synthesis.rule.support.Util;
import za.co.synthesis.rule.support.ValidationTest;
import za.co.synthesis.validate.Validate_Example;

import java.util.List;

public class ComposeDocs {
  private ValidationEngine getEngine() throws Exception {
    ValidationEngine engine = new ValidationEngine();

    engine.setupLoadPackagePath("../finsurv-rules/reg_rule_repo/coreSARB");
    //engine.setupLoadRuleUrl(new URL("http://127.0.0.1:8081/rules-management/api/rules/coreSARBExternal/validation"));
    //engine.setupLoadLookupUrl(new URL("http://127.0.0.1:8081/rules-management/api/rules/coreSARBExternal/lookups"));

    DefaultSupporting supporting = new DefaultSupporting();
    supporting.setCurrentDate(Util.date("2013-01-20"));
    supporting.setGoLiveDate(Util.date("2013-08-18"));
    engine.setupSupporting(supporting);

    CacheStrategy cacheStrategy = CacheStrategy.set(StatusType.Pass, CachePeriod.Long).
            and(StatusType.Fail, CachePeriod.Medium).
            and(StatusType.Error, CachePeriod.Short);

    engine.registerCustomValidate("Validate_ImportUndertakingCCN", cacheStrategy,
            new Validate_Example("Validate_ImportUndertakingCCN", StatusType.Pass));

    engine.registerCustomValidate("Validate_ValidCCN", cacheStrategy,
            new Validate_Example("Validate_ValidCCN", StatusType.Pass));

    engine.registerCustomValidate("Validate_ValidCCNinUCR", cacheStrategy,
            new Validate_Example("Validate_ValidCCNinUCR", StatusType.Pass));

    engine.registerCustomValidate("Validate_ReversalTrnRef", cacheStrategy,
            new Validate_Example("Validate_ReversalTrnRef", StatusType.Pass),
            "transaction::Flow", "ReversalTrnSeqNumber");

    engine.registerCustomValidate("Validate_ReplacementTrnReference", cacheStrategy,
            new Validate_Example("Validate_ReplacementTrnReference", StatusType.Pass),
            "TrnReference", "ReplacementTransaction", "ReplacementOriginalReference");

    engine.registerCustomValidate("Validate_IndividualSDA", cacheStrategy,
            new Validate_Example("Validate_IndividualSDA", StatusType.Pass),
            "transaction::TrnReference", "transaction::ValueDate", "function::localAmountForIDNumber");

    engine.registerCustomValidate("Validate_LoanRef", cacheStrategy,
            new Validate_Example("Validate_LoanRef", StatusType.Pass));

    engine.registerCustomValidate("Validate_MRNonIVS", cacheStrategy,
            new Validate_Example("Validate_MRNonIVS", StatusType.Pass),
            "ImportControlNumber");

    engine.getCentralValidateCache().setKeepLongMillis(1000*60*60*24); // 1 day
    engine.getCentralValidateCache().setKeepMediumMillis(1000*60*60); // 1 hour
    engine.getCentralValidateCache().setKeepShortMillis(1000); // 1 second

    return engine;
  }

  private static void printRuleInfoHeaders() {
    String compose = "";
    compose += "RuleName, ";
    compose += "Status, ";
    compose += "RuleType, ";
    compose += "Channel, ";
    compose += "FieldName, ";
    compose += "Code, ";
    compose += "Message, ";
    compose += "TestStatus, ";
    compose += "PosTests, ";
    compose += "NegTests, ";
    compose += "AssertionDSL";
    System.out.println(compose);
  }

  private static void printRuleInfo(RuleInfo ruleInfo) {
    String compose = "";
    compose += ruleInfo.getRuleName() + ", ";
    compose += ruleInfo.getRuleStatus().toString() + ", ";
    compose += ruleInfo.getType().toString() + ", ";
    compose += ruleInfo.getRuleChannel() + ", ";
    compose += ruleInfo.getFieldInfo().getCommonFieldLabel() + ", ";
    compose += ruleInfo.getErrorCode() + ", ";
    compose += ruleInfo.getErrorMessage() + ", ";
    compose += ruleInfo.getTestStatus().toString() + ", ";
    compose += ruleInfo.getPositiveTestCount() + ", ";
    compose += ruleInfo.getNegativeTestCount() + ", ";
    compose += ruleInfo.getAssertionDSL();
    System.out.println(compose);
  }

  private static void printRuleTestHeaders() {
    String compose = "";
    compose += "RuleName, ";
    compose += "Status, ";
    compose += "FieldName, ";
    compose += "AssertFunc, ";
    compose += "ReportData";
    System.out.println(compose);
  }

  private static void printRuleTest(RuleTest ruleTest) {
    String compose = "";
    compose += ruleTest.getRuleName() + ", ";
    compose += ruleTest.getTestStatus().toString() + ", ";
    compose += ruleTest.getFieldName() + ", ";
    compose += ruleTest.getAssertFunc() + ", ";
    compose += ruleTest.getReportData();
    System.out.println(compose);
  }

  public static void main( String[] args ) throws Exception {
    ComposeDocs compose = new ComposeDocs();

    ValidationEngine engine = compose.getEngine();
    ValidationTest engineTests = ValidationTest.createValidationTest(engine);
    ValidationDocs docs = new ValidationDocs(engine, engineTests);
    List<RuleInfo> list = docs.getRuleInfoList();
    printRuleInfoHeaders();
    for (RuleInfo ruleInfo : list) {
      printRuleInfo(ruleInfo);
    }

    System.out.println("------------------------------------------------------------------");
    List<RuleTest> tests = docs.getRuleTestList();
    printRuleTestHeaders();
    for (RuleTest ruleTest : tests) {
      printRuleTest(ruleTest);
    }

  }
}
