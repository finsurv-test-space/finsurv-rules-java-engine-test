package za.co.synthesis.rule.support;

import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 9:39 PM
 * A validation
 */
public class FieldValidation {
  private StringList fieldList;
  private Integer minLen;
  private Integer maxLen;
  private Integer len;
  private List<Rule> rules;

  public FieldValidation(String field) {
    this.fieldList = new StringList();
    this.fieldList.add(field);
    this.minLen = null;
    this.maxLen = null;
    this.len = null;
    this.rules = new ArrayList<Rule>();
  }

  public FieldValidation(StringList fieldList) {
    this.fieldList = fieldList;
    this.minLen = null;
    this.maxLen = null;
    this.len = null;
    this.rules = new ArrayList<Rule>();
  }

  public FieldValidation setMinLen(int len) {
    this.minLen = len;
    return this;
  }

  public FieldValidation setMaxLen(int len) {
    this.maxLen = len;
    return this;
  }

  public FieldValidation setLen(int len) {
    this.len = len;
    return this;
  }

  public FieldValidation addRule(Rule rule) {
    this.rules.add(rule);
    return this;
  }

  public List<String> getFieldList() {
    return fieldList;
  }

  public Integer getMinLen() {
    return minLen;
  }

  public Integer getMaxLen() {
    return maxLen;
  }

  public Integer getLen() {
    return len;
  }

  public List<Rule> getRules() {
    return rules;
  }
}
