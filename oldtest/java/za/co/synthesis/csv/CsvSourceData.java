package za.co.synthesis.csv;

import za.co.synthesis.mapping.source.ISourceData;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
import za.co.synthesis.rule.support.legacydate.format.DateTimeFormatter;
#else*/
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
//#endif

/**
 * User: jake
 * Date: 12/20/15
 * Time: 4:44 PM
 * A CSV-specific Implementation of the ISourceData interface. This is basically a wrapper around a string array.
 */
public class CsvSourceData implements ISourceData {
  private String[] rowData;

  private static String convertDateToJS(String csvDate) {
    DateTimeFormatter csvFormatter = DateTimeFormatter.ofPattern("dd/MMM/yy");
    LocalDate dt = LocalDate.parse(csvDate, csvFormatter);
    DateTimeFormatter jsFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    return jsFormatter.format(dt);
  }

  public CsvSourceData(String[] rowData) {
    this.rowData = rowData;
  }

  @Override
  public int getRowCount() {
    return 1;
  }

  @Override
  public int getRowCount(int indexLvl1) {
    return 1;
  }

  @Override
  public int getLevel() {
    return 0;
  }

  @Override
  public String getDataPoint(Object externalConfig) {
    CsvExternalConfig config = (CsvExternalConfig)externalConfig;

    String value = rowData[config.getColIndex()];
    if (value != null && value.length() > 0) {
      if (config.isDateFormat())
        return convertDateToJS(value);
    }
    return value;
  }

  @Override
  public String getDataPoint(int indexLvl1, Object externalConfig) {
    return getDataPoint(externalConfig);
  }

  @Override
  public String getDataPoint(int indexLvl1, int indexLvl2, Object externalConfig) {
    return getDataPoint(externalConfig);
  }
}
