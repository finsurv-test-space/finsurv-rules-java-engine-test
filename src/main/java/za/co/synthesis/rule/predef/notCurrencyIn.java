package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.*;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 8:09 AM
 * Determines if the value is not in the given the list of currencies
 */
public class notCurrencyIn implements IAssertionFunction {
  private StringResolverList constants = null;

  public notCurrencyIn(IStringResolver value) {
    constants = new StringResolverList();
    constants.add(value);
  }

  public notCurrencyIn(StringResolverList value) {
    this.constants = value;
  }

  public boolean execute(FinsurvContext context, Object value) {
    String currency = context.getCurrency();
    if (currency != null) {
      for (IStringResolver c : constants) {
        if (c != null) {
          String str = c.resolve(context, value);
          if (currency.equals(str))
            return false;
        }
      }
    }
    return true;
  }
}
/*
function notCurrencyIn(currencies) {
  return function(context, value) {
    var currency = context.getTransactionField("FlowCurrency");
    if (currency) {
      if (typeof currencies === "string") {
        if (currency === currencies)
          return false;
      } else {
        for ( var i = 0; i < currencies.length; i++) {
          if (currency === currencies[i])
            return false;
        }
      }
    }
    return true;
  }
}
*/

