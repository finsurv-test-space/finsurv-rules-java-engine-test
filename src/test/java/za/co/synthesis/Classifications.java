package za.co.synthesis;

import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.*;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/7/16.
 */
public class Classifications {
  List<String> testRuleNames = new ArrayList<String>();
  int testCount = 0;
  int errorCount = 0;

  private int countRuleInstances(String rulename) {
    int count = 0;
    for (String name : testRuleNames) {
      if (name.equals(rulename)) {
        count++;
      }
    }
    return count;
  }

  private void assertRuleBase(Classifier classifier, String rulename, JSObject jsObj,
                              String expectType, int number) {
    testRuleNames.add(rulename);
    testCount++;

    List<ClassificationResult> results = classifier.classifyRule(rulename, jsObj);
    if (expectType == null) {
      if (results.size() > 0) {
        errorCount++;
        System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting Success, but raised " + results.size() + ": " + results.get(0).getType());
      }
    }
    else {
      if (results.size() == 0 || !results.get(0).getType().equals(expectType) || results.size() != number) {
        errorCount++;
        if (results.size() == 0)
          System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting " + expectType + ", but instead succeeded");
        else
        if (!results.get(0).getType().equals(expectType))
          System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting " + expectType + ", but instead raised: " + results.get(0).getType().toString());
        else
        if (results.size() != number)
          System.out.println("Failed Test: " + rulename + '#' + countRuleInstances(rulename) + " - Expecting " + number + 'x' + expectType + ", but only raised " + results.size());
      }
    }
  }

  private void assertExclude(Classifier classifier, String rulename, JSObject jsObj) {
    assertRuleBase(classifier, rulename, jsObj, "exclude", 1);
  }

  private void assertInclude(Classifier classifier, String rulename, JSObject jsObj) {
    assertRuleBase(classifier, rulename, jsObj, "include", 1);
  }

  private void assertZZ2(Classifier classifier, String rulename, JSObject jsObj) {
    assertRuleBase(classifier, rulename, jsObj, "zz2", 1);
  }

  private void runTestCases(String filterTestCase) throws Exception {
    String filterRule = filterTestCase;
    int filterInstance = -1;
    if (filterTestCase != null) {
      int pos = filterTestCase.indexOf('#');
      if (pos != -1) {
        filterRule = filterTestCase.substring(0, pos);
        filterInstance = Integer.parseInt(filterTestCase.substring(pos+1));
      }
    }

    ClassificationEngine engine = new ClassificationEngine();

// Package:
//    engine.setupLoadPackagePath("../reg_rule_repo/stdBankCommon");
// File: One by One
    engine.setupLoadRuleFile("./src/test/resources/classifyData.js");

    Classifier classifier = engine.issueClassifier();

    FileReader fileReader = new FileReader("./src/test/resources/classificationTestCases.js");
    JSStructureParser parser = new JSStructureParser(fileReader);
    Object obj = parser.parse();

    JSObject testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
    JSArray cases = (JSArray)testCaseContainer.get("test_cases");

    int instance = 0;
    for (Object objCase : cases) {
      if (objCase instanceof JSFunctionCall) {
        JSFunctionCall jsFunc = (JSFunctionCall)objCase;

        String rulename = jsFunc.getParameters().get(0).toString();
        if (filterRule == null || rulename.equals(filterRule)) {
          instance++;
          if (filterInstance != -1 && instance != filterInstance)
            continue;

          if (jsFunc.getName().equals("assertExclude") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            assertExclude(classifier, rulename, jsObj);
          }
          else
          if (jsFunc.getName().equals("assertInclude") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            assertInclude(classifier, rulename, jsObj);
          }
          else
          if (jsFunc.getName().equals("assertZZ2") && jsFunc.getParameters().size() == 2) {
            JSObject jsObj = (JSObject)jsFunc.getParameters().get(1);
            assertZZ2(classifier, rulename, jsObj);
          }
          else {
            System.out.println("Unknown assertion function '" + jsFunc.getName() + "' with " + jsFunc.getParameters().size() + " parameters");
          }
        }
      }
    }
  }

  public static void main( String[] args ) throws Exception {
    Classifications test = new Classifications();
    long startMillis = System.currentTimeMillis();
    if (args.length == 1)
      test.runTestCases(args[0]);
    else
      test.runTestCases(null);
    long endMillis = System.currentTimeMillis();

    if (test.errorCount == 0) {
      System.out.println("Completed " + test.testCount + " tests Successfully :-)" );
      System.out.println("Run Time: " + (endMillis-startMillis) + " millseconds" );
    }
    else {
      if (test.errorCount == 1)
        System.out.println("Ran " + test.testCount + " tests with 1 error!");
      else
        System.out.println("Ran " + test.testCount + " tests with " + test.errorCount + " errors!");
    }
  }
}
