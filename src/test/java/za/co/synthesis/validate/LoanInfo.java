package za.co.synthesis.validate;

/**
 * User: jake
 * Date: 1/19/16
 * Time: 11:43 AM
 * Loan reference related information required for testing purposes
 */
public class LoanInfo {
  private boolean validLoanRef;

  public LoanInfo(boolean validLoanRef) {
    this.validLoanRef = validLoanRef;
  }

  public LoanInfo() {
  }

  public boolean isValidLoanRef() {
    return validLoanRef;
  }

  public void setValidLoanRef(boolean validLoanRef) {
    this.validLoanRef = validLoanRef;
  }
}
