package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 10:09 AM
 * Determines if the transaction field has been provided and if it is a string does it contain any non-whitespace
 * characters
 */
public class hasTransactionField implements IAssertionFunction {
  private String field;

  public hasTransactionField(String constant) {
    this.field = constant;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField(field);
    if (fieldValue != null && !(fieldValue instanceof IFinsurvContext.Undefined) &&
            (!(fieldValue instanceof String) || ((String)fieldValue).trim().length() > 0))
      return true;
    else
      return false;
  }
}
/*
function hasTransactionField(field) {
  return function(context, value) {
    var fieldValue = context.getTransactionField(field);
    if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
      return true;
    else
      return false;
  }
}
*/