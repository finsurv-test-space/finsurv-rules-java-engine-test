package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 3/2/15
 * Time: 5:00 PM
 */

/**
 * The flow of funds into or out of the country as defined by the SARB
 */
public enum FlowType {
  Unknown("Unknown"),
  Inflow("IN"),
  Outflow("OUT");

  private String value;
  FlowType(String value) {
    this.value=value;
  }

  public String toString(){
    return this.value;
  }

  public static FlowType fromString(String in){
    if(in != null) {
      for(FlowType eventType : FlowType.values()) {
        if (in.equalsIgnoreCase(eventType.value)) {
          return eventType;
        }
      }
    }
    return FlowType.Unknown;
  }
}
