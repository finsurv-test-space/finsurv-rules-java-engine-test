package za.co.synthesis.rule.core;

/**
 * Created by jake on 6/7/16.
 */
public class ClassificationResult {
  private final String type;
  private final String name;
  private final String field;

  public ClassificationResult(String type, String name, String field) {
    this.type = type;
    this.name = name;
    this.field = field;
  }

  public String getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getField() {
    return field;
  }
}
