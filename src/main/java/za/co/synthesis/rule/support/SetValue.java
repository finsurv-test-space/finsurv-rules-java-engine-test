package za.co.synthesis.rule.support;

import java.util.List;

/**
 * Created by jake on 4/11/16.
 */
public class SetValue extends DisplayRule {
  public SetValue(IDisplayFunction displayFunction, IAssertionFunction assertion) {
    super(DisplayRuleType.SetValue, null, null, displayFunction, assertion);
  }
}
