package za.co.synthesis.rule.evaluation;

import za.co.synthesis.rule.core.BankAccountType;
import za.co.synthesis.rule.core.ResidenceStatus;

import java.util.List;
/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif

/**
 * User: jake
 * Date: 3/3/15
 * Time: 9:45 AM
 * The configuration file stores a list of compound evaluation objects together with their associated compound decisions
 */
public class CompoundEvaluation {
  private ResidenceStatus drResStatus;
  private List<BankAccountType> drAccType;
  private ResidenceStatus crResStatus;
  private List<BankAccountType> crAccType;
  private CompoundEvaluationDecision drDecision;
  private CompoundEvaluationDecision crDecision;
  private LocalDate validFrom;
  private LocalDate validTo;
  private List<String> category;
  private List<String> notCategory;

  public ResidenceStatus getDrResStatus() {
    return drResStatus;
  }

  public void setDrResStatus(ResidenceStatus drResStatus) {
    this.drResStatus = drResStatus;
  }

  public List<BankAccountType> getDrAccType() {
    return drAccType;
  }

  public void setDrAccType(List<BankAccountType> drAccType) {
    this.drAccType = drAccType;
  }

  public ResidenceStatus getCrResStatus() {
    return crResStatus;
  }

  public void setCrResStatus(ResidenceStatus crResStatus) {
    this.crResStatus = crResStatus;
  }

  public List<BankAccountType> getCrAccType() {
    return crAccType;
  }

  public void setCrAccType(List<BankAccountType> crAccType) { this.crAccType = crAccType; }

  public CompoundEvaluationDecision getDrDecision() {
    return drDecision;
  }

  public void setDrDecision(CompoundEvaluationDecision drDecision) {
    this.drDecision = drDecision;
  }

  public CompoundEvaluationDecision getCrDecision() {
    return crDecision;
  }

  public void setCrDecision(CompoundEvaluationDecision crDecision) {
    this.crDecision = crDecision;
  }

  public LocalDate getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(LocalDate validFrom) {
    this.validFrom = validFrom;
  }

  public LocalDate getValidTo() {
    return validTo;
  }

  public void setValidTo(LocalDate validTo) {
    this.validTo = validTo;
  }

  public List<String> getCategory() {
    return category;
  }

  public void setCategory(List<String> category) {
    this.category = category;
  }

  public List<String> getNotCategory() {
    return notCategory;
  }

  public void setNotCategory(List<String> notCategory) {
    this.notCategory = notCategory;
  }

  public String prettyRuleName() {
    StringBuilder sb = new StringBuilder();
    sb.append("drResStatus: ").append(getDrResStatus() != null ? getDrResStatus().toString() : "?");
    sb.append(", drAccType: ");
    if (getDrAccType().size() == 1) {
      sb.append(getDrAccType().toString());
    }
    else {
      boolean bFirst = true;
      sb.append("[");
      for (BankAccountType bat : getDrAccType()) {
        if (bFirst)
          bFirst = false;
        else
          sb.append(", ");
        sb.append(bat.toString());
      }
      sb.append("]");
    }

    sb.append(", crResStatus: ").append(getCrResStatus() != null ? getCrResStatus().toString() : "?");
    sb.append(", crAccType: ");
    if (getCrAccType().size() == 1) {
      sb.append(getCrAccType().toString());
    }
    else {
      boolean bFirst = true;
      sb.append("[");
      for (BankAccountType bat : getCrAccType()) {
        if (bFirst)
          bFirst = false;
        else
          sb.append(", ");
        sb.append(bat.toString());
      }
      sb.append("]");
    }

    if (getCategory() != null && getCategory().size() > 0) {
      sb.append(", category: ");
      if (getCategory().size() == 1) {
        sb.append(getCategory().toString());
      }
      else {
        boolean bFirst = true;
        sb.append("[");
        for (String cat : getCategory()) {
          if (bFirst)
            bFirst = false;
          else
            sb.append(", ");
          sb.append(cat);
        }
        sb.append("]");
      }
    }

    if (getNotCategory() != null && getNotCategory().size() > 0) {
      sb.append(", notCategory: ");
      if (getNotCategory().size() == 1) {
        sb.append(getNotCategory().toString());
      }
      else {
        boolean bFirst = true;
        sb.append("[");
        for (String cat : getNotCategory()) {
          if (bFirst)
            bFirst = false;
          else
            sb.append(", ");
          sb.append(cat);
        }
        sb.append("]");
      }
    }
    return sb.toString();
  }
}
