package za.co.synthesis.rule.classification;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.support.StringList;

import java.lang.reflect.Constructor;

/**
 * Created by jake on 6/7/16.
 */
public class ClassificationFunctionFactory {
  private static final String functionPackage = "za.co.synthesis.rule.classification.predef.";

  public static IClassificationFunction createFunction(final String name) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    return (IClassificationFunction)cls.newInstance();
  }

  public static IClassificationFunction createFunction(final String name, final String param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class);
    return (IClassificationFunction)clsConstructor.newInstance(param1);
  }

  public static IClassificationFunction createFunction(final String name, final JSRegExLiteral param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(JSRegExLiteral.class);
    return (IClassificationFunction)clsConstructor.newInstance(param1);
  }

  public static IClassificationFunction createFunction(final String name, final StringList param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(StringList.class);
    return (IClassificationFunction)clsConstructor.newInstance(param1);
  }

  public static IClassificationFunction createFunction(final String name, final String param1, final String param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class);
    return (IClassificationFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IClassificationFunction createFunction(final String name, final String param1, final StringList param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, StringList.class);
    return (IClassificationFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IClassificationFunction createFunction(final String name, final IClassificationFunction param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IClassificationFunction.class);
    return (IClassificationFunction)clsConstructor.newInstance(param1);
  }

  public static IClassificationFunction createFunction(final String name, final String param1, final IClassificationFunction param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, IClassificationFunction.class);
    return (IClassificationFunction)clsConstructor.newInstance(param1, param2);
  }
}
