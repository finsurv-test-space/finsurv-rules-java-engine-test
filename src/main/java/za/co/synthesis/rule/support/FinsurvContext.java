package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.*;

import java.util.*;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 6:50 AM
 * This is a wrapper for the Finsurv Context and adds a small layer of functionality on top of the context
 */
public class FinsurvContext {
  private static class ObjectMap extends HashMap<String, Object> {
    private ObjectMap(int i, float v) {
      super(i, v);
    }

    private ObjectMap(int i) {
      super(i);
    }

    private ObjectMap() {
    }
  }

  private IFinsurvContext context;
  private Map<String, String> mappings;
  private ISupporting supporting;
  private ILookups lookups;
  private CustomValidateRegistry customValidateRegistry;
  private CustomValidateCache customValidateCache;
  private ICustomValue customValue;
  private Scope currentScope = Scope.Transaction;
  private int currentMoneyInstance = 0;
  private int currentImportExportInstance = 0;
  private final List<ResultEntry> validationResultList = new ArrayList<ResultEntry>();
  private final Map<String, DisplayEntry> displayResultMap = new HashMap<String, DisplayEntry>();
  private final String section;
  private final FlowType flow;
  private final String currency;
  private final List<String> categories = new ArrayList<String>();
  private final List<String> mainCategories = new ArrayList<String>();
  private DisplayRuleBehaviour displayRuleBehaviour = new DisplayRuleBehaviour();

  private final ObjectMap transactionCache = new ObjectMap(50);

  private void buildCategories() {
    for (int i = 0; i < context.getMoneySize(); i++) {
      Object cat = context.getMoneyField(i, "CategoryCode");
      if (cat != null) {
        String fullcat = cat.toString();
        mainCategories.add(fullcat);

        Object subcat = context.getMoneyField(i, "CategorySubCode");
        if (subcat != null)
          fullcat += "/" + subcat.toString();
        categories.add(fullcat);
      }
      else {
        mainCategories.add(null);
        categories.add(null);
      }
    }
  }

  public FinsurvContext(IFinsurvContext context,
                        Map<String, String> mappings,
                        ISupporting supporting, ILookups lookups,
                        CustomValidateRegistry customValidateRegistry, CustomValidateCache customValidateCache,
                        ICustomValue customValue) {
    this.context = context;
    this.mappings = mappings;
    this.supporting = supporting;
    this.lookups = lookups;
    this.customValidateRegistry = customValidateRegistry;
    this.customValidateCache = customValidateCache;
    this.customValidateCache.setContext(this);
    this.customValue = customValue;

    Object value = context.getTransactionField("ReportingQualifier");
    if (value != null) {
      String qualifier = value.toString();
      if (qualifier.equals("BOPCUS"))
        this.section = "A";
      else if (qualifier.equals("NON RESIDENT RAND"))
        this.section = "B";
      else if (qualifier.equals("NON REPORTABLE"))
        this.section = "C";
      else if (qualifier.equals("INTERBANK"))
        this.section = "D";
      else if (qualifier.equals("BOPCARD RESIDENT"))
        this.section = "E";
      else if (qualifier.equals("BOPCARD NON RESIDENT"))
        this.section = "F";
      else if (qualifier.equals("BOPDIR"))
        this.section = "G";
      else
        this.section = null;
    }
    else
      this.section = null;

    value = context.getTransactionField("Flow");
    if (value != null) {
      String s = value.toString().toUpperCase();
      if (s.startsWith("I"))
        this.flow = FlowType.Inflow;
      else
      if (s.startsWith("O"))
        this.flow = FlowType.Outflow;
      else
        this.flow = null;
    }
    else
      this.flow = null;

    value = context.getTransactionField("FlowCurrency");
    if (value != null)
      this.currency = value.toString();
    else
      this.currency = null;

    buildCategories();
  }

  public Map<String, String> getMappings() {
    return mappings;
  }

  public IFinsurvContextCache getValidationCache() {
    return context.getFinsurvContextCache();
  }

  public String map(String str) {
    return DSLSupport.map(mappings, str);
  }

  public void setDisplayRuleBehaviour(DisplayRuleBehaviour displayRuleBehaviour) {
    if (displayRuleBehaviour != null) {
      this.displayRuleBehaviour = displayRuleBehaviour;
    }
  }

  public DisplayRuleBehaviour getDisplayRuleBehaviour() {
    return displayRuleBehaviour;
  }

  public List<ResultEntry> getValidationResultList() {
    return validationResultList;
  }

  public Collection<DisplayEntry> getDisplayResultList() {
    return displayResultMap.values();
  }

  public ISupporting getSupporting() {
    return supporting;
  }

  public ILookups getLookups() {
    return lookups;
  }

  public CustomValidateRegistry getCustomValidateRegistry() {
    return customValidateRegistry;
  }

  public CustomValidateCache getCustomValidateCache() {
    return customValidateCache;
  }

  public ICustomValue getCustomValue() {
    return customValue;
  }

  public String getSection() {
    return section;
  }

  public FlowType getFlow() {
    return flow;
  }

  public String getFlowAsString() {
    return flow == FlowType.Inflow ? "IN" : "OUT";
  }

  public String getCurrency() {
    return currency;
  }

  public List<String> getCategories() {
    return categories;
  }

  public List<String> getMainCategories() {
    return mainCategories;
  }

  public boolean hasField(Object obj, final String field) {
    return context.hasField(obj, field);
  }

  public Object getTransactionField(String field) {
    if (transactionCache.containsKey(field))
      return transactionCache.get(field);
    else {
      Object value = context.getTransactionField(field);
      transactionCache.put(field, value);
      return value;
    }
  }

  public int getMoneySize() {
    return context.getMoneySize();
  }

  public Object getMoneyField(int instance, String field) {
    return context.getMoneyField(instance, field);
  }

  public int getImportExportSize(int moneyInstance) {
    return context.getImportExportSize(moneyInstance);
  }

  public Object getImportExportField(int moneyInstance, int instance, String field) {
    return context.getImportExportField(moneyInstance, instance, field);
  }

  public Scope getCurrentScope() {
    return currentScope;
  }

  public void setCurrentScope(Scope currentScope) {
    this.currentScope = currentScope;
  }

  public int getCurrentMoneyInstance() {
    return currentMoneyInstance;
  }

  public void setCurrentMoneyInstance(int currentMoneyInstance) {
    this.currentMoneyInstance = currentMoneyInstance;
  }

  public int getCurrentImportExportInstance() {
    return currentImportExportInstance;
  }

  public void setCurrentImportExportInstance(int currentImportExportInstance) {
    this.currentImportExportInstance = currentImportExportInstance;
  }

  public void logTransactionEvent(ResultType type, String field, String code, String msg) {
    validationResultList.add(new ResultEntry(type, field, code, msg, Scope.Transaction, -1, -1));

    context.logTransactionEvent(type, field, code, msg);
  }

  public void logMoneyEvent(ResultType type, int instance, String field, String code, String msg) {
    validationResultList.add(new ResultEntry(type, field, code, msg, Scope.Money, instance, -1));

    context.logMoneyEvent(type, instance, field, code, msg);
  }

  public void logImportExportEvent(ResultType type, int moneyInstance, int instance, String field, String code, String msg) {
    validationResultList.add(new ResultEntry(type, field, code, msg, Scope.ImportExport, moneyInstance, instance));

    context.logImportExportEvent(type, moneyInstance, instance, field, code, msg);
  }

  private String displayMapKey(final String field, int moneyInstance, int instance) {
    return field + (moneyInstance != -1 ? ":" + moneyInstance : "") + "," + (instance != -1 ? ":" + instance : "");
  }

  private void updateDisplayEntry(DisplayEntry entry, final DisplayRuleType type, final List<String> values) {
    if (type == DisplayRuleType.Hide) {
      entry.setVisibility(DisplayEntry.Visibility.Hide);
    }
    else
    if (type == DisplayRuleType.Show) {
      entry.setVisibility(DisplayEntry.Visibility.Show);
    }
    else
    if (type == DisplayRuleType.Enable) {
      entry.setAvailability(DisplayEntry.Availability.Enabled);
    }
    else
    if (type == DisplayRuleType.Disable) {
      entry.setAvailability(DisplayEntry.Availability.Disabled);
    }
    else
    if (type == DisplayRuleType.ClearValue) {
      entry.setFieldValue(null);
    }
    else
    if (type == DisplayRuleType.SetValue) {
      String value = (values != null && values.size() > 0) ? values.get(0) : "";
      entry.setFieldValue(value);
    }
    else
    if (type == DisplayRuleType.SetField) {
      String value = (values != null && values.size() > 0) ? values.get(0) : "";
      entry.setFieldValue(value);
    }
    else
    if (type == DisplayRuleType.AppendValue) {
      String value = (values != null && values.size() > 0) ? values.get(0) : "";
      if (entry.getFieldValue() != null)
        entry.setFieldValue(entry.getFieldValue() + value);
      else
        entry.setFieldValue(value);
    }
    else
    if (type == DisplayRuleType.LimitValue) {
      entry.setLimitValues(values);
    }
    else
    if (type == DisplayRuleType.ExcludeValue) {
      entry.setExcludeValues(values);
    }
  }

  public void logTransactionDisplayEvent(final DisplayRuleType type, final String field, final List<String> values) {
    String key = displayMapKey(field, -1, -1);
    DisplayEntry entry;
    if (displayResultMap.containsKey(key)) {
      entry = displayResultMap.get(key);
    }
    else {
      entry = new DisplayEntry(field, Scope.Transaction, -1, -1);
      displayResultMap.put(key, entry);
    }
    updateDisplayEntry(entry, type, values);
    context.logTransactionDisplayEvent(type, field, values);
  }

  public void logMoneyDisplayEvent(final DisplayRuleType type, int instance, final String field, final List<String> values) {
    String key = displayMapKey(field, instance, -1);
    DisplayEntry entry;
    if (displayResultMap.containsKey(key)) {
      entry = displayResultMap.get(key);
    }
    else {
      entry = new DisplayEntry(field, Scope.Transaction, instance, -1);
      displayResultMap.put(key, entry);
    }
    updateDisplayEntry(entry, type, values);
    context.logMoneyDisplayEvent(type, instance, field, values);
  }

  public void logImportExportDisplayEvent(final DisplayRuleType type, int moneyInstance, int instance, final String field, final List<String> values) {
    String key = displayMapKey(field, moneyInstance, instance);
    DisplayEntry entry;
    if (displayResultMap.containsKey(key)) {
      entry = displayResultMap.get(key);
    }
    else {
      entry = new DisplayEntry(field, Scope.Transaction, moneyInstance, instance);
      displayResultMap.put(key, entry);
    }
    updateDisplayEntry(entry, type, values);
    context.logImportExportDisplayEvent(type, moneyInstance, instance, field, values);
  }
}
