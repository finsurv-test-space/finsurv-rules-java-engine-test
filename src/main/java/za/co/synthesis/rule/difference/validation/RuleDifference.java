package za.co.synthesis.rule.difference.validation;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.FlowType;
import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.rule.difference.HTMLDiffUtils;
import za.co.synthesis.rule.difference.IDifference;
import za.co.synthesis.rule.support.Rule;

import java.util.List;

import static za.co.synthesis.rule.difference.HTMLDiffUtils.composeNumberOrString;

public class RuleDifference implements IDifference {
  private final DifferenceType type;
  private final String ruleName;
  private final Scope oldScope;
  private final Scope newScope;
  private final List<String> oldFields;
  private final List<String> newFields;
  private final Rule oldRule;
  private final Rule newRule;

  public RuleDifference(DifferenceType type, String ruleName,
                        Scope oldScope, List<String> oldFields, Rule oldRule,
                        Scope newScope, List<String> newFields, Rule newRule) {
    this.type = type;
    this.ruleName = ruleName;
    this.oldScope = oldScope;
    this.oldFields = oldFields;
    this.oldRule = oldRule;
    this.newScope = newScope;
    this.newFields = newFields;
    this.newRule = newRule;
  }

  public static RuleDifference createAddedRule(Scope scope, List<String> fields, Rule rule) {
    return new RuleDifference(DifferenceType.Added, rule.getName(),
            null, null, null, scope, fields, rule);
  }

  public static RuleDifference createChangedRule(Scope oldScope, List<String> oldFields, Rule oldRule,
                                                 Scope newScope, List<String> newFields, Rule newRule) {
    return new RuleDifference(DifferenceType.Changed, oldRule.getName(),
            oldScope, oldFields, oldRule,
            newScope, newFields, newRule);
  }

  public static RuleDifference createRemovedRule(Scope scope, List<String> fields, Rule rule) {
    return new RuleDifference(DifferenceType.Removed, rule.getName(),
            scope, fields, rule, null,null, null);
  }

  public String getRuleName() {
    return ruleName;
  }

  @Override
  public DifferenceType getType() {
    return type;
  }

  private String onFlow(FlowType flow) {
    if (flow != null) {
      if (flow == FlowType.Inflow) {
        return ".onInflow()";
      }
      else
      if (flow == FlowType.Outflow) {
        return ".onOutflow()";
      }
    }
    return "";
  }

  private String onFlowUpdate(FlowType oldFlow, FlowType newFlow) {
    return HTMLDiffUtils.composeWholeStrings(onFlow(oldFlow), onFlow(newFlow));
  }

  private String onSection(String section) {
    if (section != null) {
      return ".onSection(\"" + section + "\")";
    }
    return "";
  }

  private String onSectionUpdate(String oldSection, String newSection) {
    return HTMLDiffUtils.composeStrings(onSection(oldSection), onSection(newSection));
  }

  private String composeList(List<String> categories) {
    if (categories.size() == 1) {
      return "\"" + categories.get(0) + "\"";
    }
    StringBuilder result = new StringBuilder();
    for (String cat : categories) {
      if (result.length() == 0) {
        result.append("[");
      }
      else {
        result.append(", ");
      }
      result.append("\"").append(cat).append("\"");
    }
    result.append("]");
    return result.toString();
  }

  private String onCategory(List<String> categories) {
    if (categories != null && categories.size() > 0) {
      return ".onCategory("+ composeList(categories) + ")";
    }
    return "";
  }

  private String onCategoryUpdate(List<String> oldCategories, List<String> newCategories) {
    return HTMLDiffUtils.composeStrings(onCategory(oldCategories), onCategory(newCategories));
  }

  private String notOnCategory(List<String> categories) {
    if (categories != null && categories.size() > 0) {
      return ".notOnCategory("+ composeList(categories) + ")";
    }
    return "";
  }

  private String notOnCategoryUpdate(List<String> oldCategories, List<String> newCategories) {
    return HTMLDiffUtils.composeStrings(notOnCategory(oldCategories), notOnCategory(newCategories));
  }

  @Override
  public String diffHTML() {
    /*failure('ls_cntft3', 302, 'Must be in a 2 to 15 digit format',
                notEmpty.and(notPattern(/^\d{2,15}$/))).onSection("AE")*/
    if (type == DifferenceType.Added) {
      if (newRule != null) {
        return String.format("<ins>%s(\"%s\", %s, \"%s\", \n  %s)%s%s%s%s</ins>",
                newRule.getType().toString(), newRule.getName(),
                composeNumberOrString(newRule.getCode()), newRule.getMessage(),
                newRule.getAssertionDSL(),
                onFlow(newRule.getFlow()),
                onCategory(newRule.getCategories()),
                notOnCategory(newRule.getNotCategories()),
                onSection(newRule.getSection())
                );
      }
    }
    else
    if (type == DifferenceType.Changed) {
      return String.format("%s(\"%s\", \"%s\", \"%s\", \n  %s)%s%s%s%s",
              HTMLDiffUtils.composeWholeStrings(oldRule.getType().toString(), newRule.getType().toString()),
              ruleName,
              HTMLDiffUtils.composeValues(oldRule.getCode(), newRule.getCode()),
              HTMLDiffUtils.composeStrings(oldRule.getMessage(), newRule.getMessage()),
              HTMLDiffUtils.composeStrings(oldRule.getAssertionDSL(), newRule.getAssertionDSL()),
              onFlowUpdate(oldRule.getFlow(), newRule.getFlow()),
              onCategoryUpdate(oldRule.getCategories(), newRule.getCategories()),
              notOnCategoryUpdate(oldRule.getNotCategories(), newRule.getNotCategories()),
              onSection(newRule.getSection())
      );
    }
    else
    if (type == DifferenceType.Removed) {
      if (oldRule != null) {
        return String.format("<del>%s(\"%s\", \"%s\", \"%s\", \n  %s)%s%s%s%s</del>",
                oldRule.getType().toString(), oldRule.getName(),
                composeNumberOrString(oldRule.getCode()), oldRule.getMessage(),
                oldRule.getAssertionDSL(),
                onFlow(oldRule.getFlow()),
                onCategory(oldRule.getCategories()),
                notOnCategory(oldRule.getNotCategories()),
                onSection(oldRule.getSection())
        );
      }
    }
    return "";
  }

  private static JSObject ruleToJSObject(Scope scope, List<String> fields, Rule rule) {
    JSObject jsRule = new JSObject();

    jsRule.put("scope", scope.getName());

    JSArray jsFields = new JSArray();
    jsFields.addAll(fields);
    jsRule.put("fields", jsFields);

    jsRule.put("function", rule.getType().toString());
    jsRule.put("code", rule.getCode());
    jsRule.put("message", rule.getMessage());
    if (rule.getSection() != null)
      jsRule.put("onSection", rule.getSection());
    if (rule.getFlow() != null)
      jsRule.put("onFlow", rule.getFlow());
    if (rule.getCategories() != null) {
      JSArray jsArray = new JSArray();
      jsArray.addAll(rule.getCategories());
      jsRule.put("onCategories", jsArray);
    }
    if (rule.getNotCategories() != null) {
      JSArray jsArray = new JSArray();
      jsArray.addAll(rule.getNotCategories());
      jsRule.put("notOnCategory", jsArray);
    }
    jsRule.put("assertion", rule.getAssertionDSL());
    return jsRule;
  }

  @Override
  public void addTo(JSArray array) {
    JSObject obj = new JSObject();
    obj.put("ruleName", ruleName);
    if (oldRule != null) {
      obj.put("oldValue", ruleToJSObject(oldScope, oldFields, oldRule));
    }
    if (newRule != null) {
      obj.put("newValue", ruleToJSObject(newScope, newFields, newRule));
    }
    obj.put("diffHTML", diffHTML());
    array.add(obj);
  }
}
