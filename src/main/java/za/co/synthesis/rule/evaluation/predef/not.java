package za.co.synthesis.rule.evaluation.predef;

import za.co.synthesis.rule.evaluation.GivenInfo;
import za.co.synthesis.rule.evaluation.IAssumptionAssertionFunction;

/**
 * Created by jake on 9/11/17.
 */
public class not implements IAssumptionAssertionFunction {
  private final IAssumptionAssertionFunction func;

  public not(IAssumptionAssertionFunction func) {
    this.func = func;
  }

  @Override
  public boolean execute(GivenInfo givenInfo) {
    return !func.execute(givenInfo);
  }
}