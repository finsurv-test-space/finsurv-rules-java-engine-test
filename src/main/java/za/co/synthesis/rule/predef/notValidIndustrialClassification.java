package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 3:08 PM
 * Determines if the current value is not a valid Industrial Classification
 */
public class notValidIndustrialClassification implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !context.getLookups().isValidIndustrialClassification(value.toString());
  }
}

/*
function notValidIndustrialClassification(context, value) {
  return !context.lookups.isValidIndustrialClassification(value);
}
*/