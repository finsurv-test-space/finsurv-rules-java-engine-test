package za.co.synthesis.rule.classification;

import za.co.synthesis.rule.core.IClassificationContext;

/**
 * Created by jake on 6/7/16.
 */
public interface IClassificationFunction {
  boolean execute(ClassificationContext context, Object value);
}
