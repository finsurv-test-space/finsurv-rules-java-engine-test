package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * Created by petruspretorius on 27/09/2016.
 */
public class notMatchCCNinUCR implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value != null) {
      String CCNinUCR = null;
      String ucr = value.toString();
      if (ucr.length() >= 11) {
        CCNinUCR = ucr.substring(3, 11);

        Object ResidentCCN = context.getTransactionField("Resident.Individual.CustomsClientNumber");
        if (ResidentCCN instanceof IFinsurvContext.Undefined)
          ResidentCCN = null;
        if (ResidentCCN == null) {
          ResidentCCN = context.getTransactionField("Resident.Entity.CustomsClientNumber");
          if (ResidentCCN instanceof IFinsurvContext.Undefined)
            ResidentCCN = null;
        }
        Object ThirdPartyCCN = context.getMoneyField(context.getCurrentMoneyInstance(), "ThirdParty.CustomsClientNumber");
        if (ThirdPartyCCN instanceof IFinsurvContext.Undefined)
          ThirdPartyCCN = null;

        boolean isCCNinUCR = false;
        if (ResidentCCN != null && CCNinUCR.equals(ResidentCCN.toString()))
          isCCNinUCR = true;
        else if (ThirdPartyCCN != null && CCNinUCR.equals(ThirdPartyCCN.toString()))
          isCCNinUCR = true;

        return !isCCNinUCR;
      }
    }
    return true;
  }
}

