package za.co.synthesis.rule.core;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.rule.classification.Classifications;
import za.co.synthesis.rule.support.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/7/16.
 */
public class ClassificationEngine {
  private boolean setupTime = true;
  private Classifications classifications;

  public ClassificationEngine() {}

  private void checkSetup() throws ClassificationException {
    if (!setupTime) {
      throw new ClassificationException("Validators have already been issued. You cannot make any further call to setup functions.");
    }
  }

  public static EngineVersion getVersion() {
    return EngineVersion.getVersion();
  }

  /**
   * This will load classification rules from a JavaScript source (In the classification rule DSL format).
   * Earlier rules loaded first will take mwb950493
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once validators are issued
   * @param name
   * @param reader
   * @throws Exception
   */
  public synchronized void setupLoadRule(final String name, final Reader reader) throws ClassificationException {
    checkSetup();

    if (classifications == null) {
      classifications = new Classifications();
    }
    try {
      JSStructureParser parser = new JSStructureParser(reader);

      Object obj = parser.parse();

      List<JSObject> rulesetObjs = new ArrayList<JSObject>();
      JSUtil.findObjectsWith(obj, "ruleset", rulesetObjs);
      for (JSObject rulesetObj : rulesetObjs) {
        classifications.loadRuleSet(rulesetObj);
      }
    }
    catch (Exception e) {
      throw new ClassificationException("Could not parse '" + name + "' rule file", e);
    }
  }

  /**
   * This will load rules from a JavaScript file (In the rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once validators are issued
   * @param filename
   * @throws Exception
   */
  public void setupLoadRuleFile(final String filename) throws ClassificationException {
    try {
      FileReader fileReader = new FileReader(filename);
      setupLoadRule(filename, fileReader);
    }
    catch (Exception e) {
      throw new ClassificationException("Could not load '" + filename + "' rule file", e);
    }
  }

  /**
   * This will load rules from a JavaScript file (In the rule DSL format). Earlier rules loaded first will take
   * precedence to rules loaded later. This means that custom rules should be loaded first with standard rulesets being
   * loaded later.
   * This is a setup function and cannot be called once validators are issued
   * @param fileUrl
   * @throws Exception
   */
  public void setupLoadRuleUrl(final URL fileUrl) throws ClassificationException {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));
      setupLoadRule(fileUrl.getPath(), buffReader);
    }
    catch (Exception e) {
      throw new ClassificationException("Could not load '" + fileUrl.getPath() + "' rule file", e);
    }
  }

  private void setupLoadClassificationsFromPath(final String packagepath) throws ClassificationException {
    String validationPath = packagepath + File.separator + "classification";
    File file = new File(validationPath);
    if (file.isDirectory()) {
      String nameClassification = validationPath + File.separator + "classification.js";
      File fileClassification = new File(nameClassification);
      if (fileClassification.isFile())
        setupLoadRuleFile(nameClassification);
    }
  }

  /**
   * This will load rules and lookups from a package path. This is the simplest way to load all appropriate
   * configurations for a specific application.
   * @param filepath
   * @return the engine version required to execute this package of rules
   * @throws Exception
   */
  public EngineVersion setupLoadPackagePath(final String filepath) throws ClassificationException {
    EngineVersion highestVersion = null;
    String highestVersionPackageName = null;
    try {
      File file = new File(filepath);
      if (file.isDirectory()) {
        String packageRoot = file.getParent();
        String packagePath = file.getAbsolutePath();
        String packageFileName = packagePath + File.separator + "package.js";
        FileReader fileReader = new FileReader(packageFileName);

        RulePackage rule_package = RulePackages.loadRulePackage(fileReader);
        if (rule_package == null) {
          throw new ValidationException("Could not correctly parse '" + filepath + "' package. Has it been properly defined?");
        }
        highestVersion = rule_package.getValidationEngine();
        highestVersionPackageName = file.getName();
        setupLoadClassificationsFromPath(packagePath);

        if (rule_package.getDependsOn() != null) {
          EngineVersion reqVersion = setupLoadPackagePath(packageRoot + File.separator + rule_package.getDependsOnName());
          if (reqVersion.isHigher(highestVersion)) {
            highestVersionPackageName = rule_package.getDependsOnName();
            highestVersion = reqVersion;

          }
        }
      }
    }
    catch (Exception e) {
      throw new ClassificationException("Could not load '" + filepath + "' package", e);
    }

    if (highestVersion != null) {
      if (highestVersion.isHigher(getVersion())) {
        throw new ClassificationException("The '" + highestVersionPackageName + "' package requires version " + highestVersion.toString() +
                " and this engine version is " + getVersion().toString());
      }
    }
    return highestVersion;
  }

  public synchronized Classifier issueClassifier() throws ClassificationException {
    setupTime = false;

    return new Classifier(classifications);
  }
}
