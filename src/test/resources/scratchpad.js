define(function () {
  return function (predef) {
    var feature;
    with (predef) {

      feature = {
        ruleset: "Transaction Checksum Rules",
        scope: "transaction",
        validations: [
          {
            field : "TotalValue",
            rules : [
              failure("totv1", 244,
                  "RandValue + ForeignValue must equal TotalValue",
                  isDefined.and(notSumTotalValue).and(notValueIn(["NAMIBIA", "LESOTHO", "SWAZILAND"]))).onSection("ABCDEG"),
              failure("totv2", 245,
                  "Description of new rule",
                  isEmpty).onInflow(),
              failure('totv3', 246, 'Description of new rule on one line with single quoted strings', notEmpty).onOutflow()
            ]
          }
        ]
      };
    }
    return feature;
  }
});
