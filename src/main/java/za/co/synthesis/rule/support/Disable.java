package za.co.synthesis.rule.support;

/**
 * Created by jake on 4/10/16.
 */
public class Disable extends DisplayRule {
  public Disable(IAssertionFunction assertion) {
    super(DisplayRuleType.Disable, null, null, null, assertion);
  }
}
