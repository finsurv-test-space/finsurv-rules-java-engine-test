package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.matching.IConfidenceFunction;
import za.co.synthesis.rule.matching.MatchingContext;

/**
 * Created by jake on 6/16/16.
 */
public class constantConfidence implements IConfidenceFunction {
  private double confidence;

  public constantConfidence(String confidence) {
    this.confidence = Double.parseDouble(confidence);
  }

  @Override
  public double execute(MatchingContext context) {
    return confidence;
  }
}
