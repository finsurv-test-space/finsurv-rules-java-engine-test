package za.co.synthesis;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSUtil;

import java.io.FileReader;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 4/24/16
 * Time: 8:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class RequireWithJS {
  public void Read() throws Exception {
    FileReader fileReader = new FileReader("./src/test/resources/test.js");
//    FileReader fileReader = new FileReader("/Users/jake/Work/Synthesis/finsurv-rules-java-engine/src/test/resources/test.js");
    JSStructureParser parser = new JSStructureParser(fileReader);
    parser.setResolveIdentifiers(true);
    Object obj = parser.parse();

    JSObject jsObj = JSUtil.findObjectWith(obj, "ruleset");
    String str = jsObj.toString();
    System.out.println(str);
  }

  public static void main( String[] args ) throws Exception {
    RequireWithJS test = new RequireWithJS();
    test.Read();
  }

}
