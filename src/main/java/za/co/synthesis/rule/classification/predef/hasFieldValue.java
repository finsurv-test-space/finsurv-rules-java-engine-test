package za.co.synthesis.rule.classification.predef;

import za.co.synthesis.rule.classification.ClassificationContext;
import za.co.synthesis.rule.classification.IClassificationFunction;
import za.co.synthesis.rule.support.StringList;

/**
 * Created by jake on 6/7/16.
 */
public class hasFieldValue implements IClassificationFunction {
  private String field;
  private StringList constants = null;

  public hasFieldValue(String field, String constant) {
    this.field = field;
    this.constants = new StringList();
    this.constants.add(constant);
  }

  public hasFieldValue(String field, StringList constant) {
    this.field = field;
    this.constants = constant;
  }

  public boolean execute(ClassificationContext context, Object value) {
    Object fieldValue = context.getField(field);
    if (fieldValue != null) {
      for (String constant : constants) {
        if (fieldValue.equals(constant))
          return true;
      }
    }
    return false;
  }
}
