package za.co.synthesis.rule.support;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 9:36 PM
 * A warning rule
 */
public class Warning extends Rule {
  public Warning(String name, String code, String message, IAssertionFunction assertion, String assertionDSL, String ruleDSL) {
    super(RuleType.Warning, name, code, message, null, assertion, assertionDSL, ruleDSL);
  }
}
