package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

/**
 * User: jake
 * Date: 3/21/16
 * Time: 4:24 PM
 * A default implementation of the custom validate object that passes all. This is used when ignoring missing
 * custom validate functions
 */
public class Validate_PassAll implements ICustomValidate {

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    return new CustomValidateResult(StatusType.Pass);
  }
}
