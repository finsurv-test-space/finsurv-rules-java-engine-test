package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.util.Map;

public class notComplexValue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return !(value == null || !Util.isSimpleType(value.getClass()));
  }
}