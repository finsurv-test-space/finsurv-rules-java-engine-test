package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class notValidECI implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    Object cardType = context.getMoneyField(context.getCurrentMoneyInstance(), "CardIndicator");
    return (value == null) || !(value instanceof String) || !Util.isValidECI(value.toString(), cardType.toString());
  }
}
/*
  _export.notValidECI = function (context, value) {
    var cardType = context.getMoneyField(context.currentMoneyInstance, 'CardIndicator');
    var regECI;
    if (cardType) {
      if (cardType === 'VISA' || cardType === 'ELECTRON') {
        regECI = /^(0[0-9]|XX)$/;
      } else if (cardType === 'MASTER' || cardType === 'MAESTRO') {
        regECI = /^([0-3]|X)$/;
      } else if (cardType === 'AMEX') {
        regECI = /^0[5-7]$/;
      } else if (cardType === 'DINERS') {
        regECI = /^0[5-8]$/;
      }
    }
    if (regECI)
      return (regECI.test(value) == false);
    return false;
  }
*/