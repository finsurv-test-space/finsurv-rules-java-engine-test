package za.co.synthesis.javascript;

/**
 * User: jake
 * Date: 4/25/16
 * Time: 9:42 AM
 * This is simple extension of the JSObject class together with a pointer to a parent scope.
 */
public class JSScope extends JSObject {
  protected final JSScope parentScope;

  public JSScope(JSScope parentScope) {
    this.parentScope = parentScope;
  }

  public JSScope getParentScope() {
    return parentScope;
  }

  public void setPreceding(JSSnippetSupport preceding, int precedingIndex) {
    snippets.setPreceding(preceding, precedingIndex);
  }

  @Override
  public void addSnippet(JSSnippet snippet) {
    snippets.addSnippet(snippet);
  }

  @Override
  public int currentSnippetIndex() {
    return snippets.currentSnippetIndex();
  }

  @Override
  public void augmentEndSnippet(String extraText) {
    snippets.augmentEndSnippet(extraText);
  }

  @Override
  public void composeSnippets(JSWriter writer) {
    JSSnippet snippet = snippets.getFirstSnippetByType(JSSnippet.Type.Start);
    if (snippet != null) {
      writer.append(snippet.getText());
    }

    for (JSSnippet jss : snippets.getSnippetsByTypes(JSSnippet.Type.Variable, JSSnippet.Type.Value, JSSnippet.Type.Body)) {
      String var = jss.getText();
      writer.append(var);

      if (jss.getType().equals(JSSnippet.Type.Variable) && var.contains("=")) {
        Object obj = jss.getRelatedObject();

        if (obj != null) {
          if (obj instanceof String) {
            writer.append('"');
            JSUtil.escapeJavaScriptString(obj.toString(), false, writer);
            writer.append('"');
          } else if (obj instanceof JSObject) {
            ((JSObject) obj).composeSnippets(writer);
          } else if (obj instanceof JSArray) {
            ((JSArray) obj).composeSnippets(writer);
          } else if (obj instanceof JSFunctionDefinition) {
            ((JSFunctionDefinition) obj).composeSnippets(writer);
          } else if (obj instanceof JSFunctionCall) {
            ((JSFunctionCall) obj).composeSnippets(writer);
          } else if (obj instanceof JSIdentifier) {
            ((JSIdentifier) obj).composeSnippets(writer);
          } else if (obj instanceof JSRegExLiteral) {
            ((JSRegExLiteral) obj).composeSnippets(writer);
          } else if (obj instanceof Boolean) {
            writer.append(((Boolean) obj) ? "true" : "false");
          } else if (obj instanceof Number || Number.class.isAssignableFrom(obj.getClass())) {
            writer.append(obj.toString());
          } else {
            writer.append('"');
            writer.append(obj.toString());
            writer.append('"');
          }
        }
      }

      if (jss.getType().equals(JSSnippet.Type.Body) && jss.getRelatedObject() != null) {
        if (IJSSnippetSupport.class.isAssignableFrom(jss.getRelatedObject().getClass())) {
          ((IJSSnippetSupport)jss.getRelatedObject()).composeSnippets(writer);
        }
      }
    }

    snippet = snippets.getFirstSnippetByType(JSSnippet.Type.End);
    if (snippet != null) {
      writer.append(snippet.getText());
    }
  }

  @Override
  public void removeExtraWhitespace() {
    snippets.removeExtraWhitespace(this);
  }

  @Override
  public void injectWhitespace(Object parentObject) {
    snippets.injectWhitespace(parentObject, this);
  }
}
