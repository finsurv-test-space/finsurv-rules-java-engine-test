package za.co.synthesis.rule.matching;

/**
 * Created by jake on 6/16/16.
 */
public class MatchConfidence {
  private final String name;
  private final IConfidenceFunction confidence;
  private final IMatchingFunction assertion;

  public MatchConfidence(final String name, final IConfidenceFunction confidence, final IMatchingFunction assertion) {
    this.name = name;
    this.confidence = confidence;
    this.assertion = assertion;
  }

  public String getName() {
    return name;
  }

  public IConfidenceFunction getConfidence() {
    return confidence;
  }

  public IMatchingFunction getAssertion() {
    return assertion;
  }
}
