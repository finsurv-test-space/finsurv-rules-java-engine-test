package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.core.ILookups;
import za.co.synthesis.rule.support.DSLSupport;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IDisplayFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * User: jake
 * Date: 4/27/16
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class lookupFieldForKey implements IDisplayFunction {
  private final String lookup;
  private final String keyName;
  private final String keyField;
  private final String lookupField;

  public lookupFieldForKey(String lookup, String keyName, String keyField, String lookupField) {
    this.lookup = lookup;
    this.keyName = keyName;
    this.keyField = keyField;
    this.lookupField = lookupField;
  }

  @Override
  public String execute(FinsurvContext context, Object value) {
    Object keyValue = DSLSupport.lookupScopedFieldValue(context, keyField);
    if (keyValue != null) {
      Map<String, String> key = new HashMap<String, String>();
      key.put(keyName, keyValue.toString());

      ILookups lookups = context.getLookups();
      return lookups.getLookupField(lookup, key, lookupField);
    }
    return null;
  }
}