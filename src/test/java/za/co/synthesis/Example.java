package za.co.synthesis;

import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.support.Util;

import java.net.URL;
/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif
import java.util.List;

public class Example {
/*  public static void main( String[] args ) throws Exception {
    ValidationEngine engine = createEngine();

    engine.getStructure();
    Validator validator = engine.issueValidator();

    // use validator to validate transaction data here
  }
*/
  public static void main( String[] args ) throws Exception {
    EvaluationEngine engine = createEvaluationEngine();
    Evaluator evaluator = engine.issueEvaluator();

    // use evaluator to evaluate transaction data here
    /*List<IEvaluationDecision> result = evaluator.evaluateOnUs(
            ResidenceStatus.Resident, BankAccountType.LOCAL_ACC, ACCOUNT_HOLDER_STATUS.Individual,
            ResidenceStatus.Resident, BankAccountType.CFC, ACCOUNT_HOLDER_STATUS.Entity);*/

    List<IEvaluationDecision> result = evaluator.evaluateUnknownCrSide(
            ResidenceStatus.Resident, BankAccountType.LOCAL_ACC,
            null,"CITIUSXXX", "USD",
            AccountHolderStatus.Individual);

    for (IEvaluationDecision decision : result) {
      System.out.println("-----");
      System.out.printf("Reportable: %s\n", decision.getReportable().toString());
      if (decision.getFlow() != null)
        System.out.printf("Flow: %s\n", decision.getFlow().toString());

      boolean printedCat = false;
      if (decision.getCategory().size() > 0) {
        printedCat = true;
        System.out.print("Category: ");
        for (String cat : decision.getCategory()) {
          System.out.printf("%s ", cat);
        }
      }
      if (decision.getNotCategory().size() > 0) {
        if (printedCat)
          System.out.print("; ");
        printedCat = true;
        System.out.print("Not on Category: ");
        for (String cat : decision.getNotCategory()) {
          System.out.printf("%s ", cat);
        }
      }
      if (printedCat)
        System.out.println();

      if (decision.getNonResSide() != null)
        System.out.printf("Non Resident - Map from %s side, Account Type: %s\n", decision.getNonResSide().toString(), decision.getNonResAccountType().getName());
      if (decision.getNonResException() != null)
        System.out.printf("Non Resident - Exception: %s\n", decision.getNonResException());
      if (decision.getResSide() != null)
        System.out.printf("Resident - Map from %s side, Account Type: %s\n", decision.getResSide().toString(), decision.getResAccountType().getName());
      if (decision.getResException() != null)
        System.out.printf("Resident - Exception: %s\n", decision.getResException());
    }
  }

  public static EvaluationEngine createEvaluationEngine() throws Exception {
    EvaluationEngine engine = new EvaluationEngine();

    engine.setupLoadRuleUrl(new URL("http://127.0.0.1:8082/rules-management/api/rules/stdBankCommon/evaluation"));

    return engine;
  }

  public static ValidationEngine createEngine() throws Exception {
    ValidationEngine engine = new ValidationEngine();

    DefaultSupporting supporting = new DefaultSupporting();
    supporting.setCurrentDate(LocalDate.now());
    supporting.setGoLiveDate(Util.date("2013-08-18"));
    engine.setupSupporting(supporting);

    engine.setupLoadLookupFile("../finsurv-rules/lookups.js");

    engine.setupLoadRuleFile("../finsurv-rules/transaction.js");
    engine.setupLoadRuleFile("../finsurv-rules/money.js");
    engine.setupLoadRuleFile("../finsurv-rules/importexport.js");

    return engine;
  }

}
