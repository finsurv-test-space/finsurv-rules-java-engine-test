package za.co.synthesis.rule.matching;

import java.lang.reflect.Constructor;

/**
 * Created by jake on 6/16/16.
 */
public class ConfidenceFunctionFactory {
  private static final String functionPackage = "za.co.synthesis.rule.matching.predef.";

  public static IConfidenceFunction createFunction(final String name) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    return (IConfidenceFunction)cls.newInstance();
  }

  public static IConfidenceFunction createFunction(final String name, final String param1, final String param2,
                                                   final String param3, final String param4, final String param5) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class, String.class, String.class, String.class);
    return (IConfidenceFunction)clsConstructor.newInstance(param1, param2, param3, param4, param5);
  }
}
