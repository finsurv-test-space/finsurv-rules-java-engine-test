package za.co.synthesis.javascript;

/**
 * Created by jake on 5/17/16.
 */
public class JSWriter {
  private final StringBuilder sb;

  private boolean startOfLine;
  private boolean quoteAttributes;
  private String newline = "";
  private String linePrefix = "";
  private String indent = "";
  private int indentLevel;

  public JSWriter() {
    this.sb = new StringBuilder();
    this.indentLevel = 0;
    this.startOfLine = false;
    this.quoteAttributes = false;
  }

  public String getIndent() {
    return indent;
  }

  public void setIndent(String indent) {
    this.indent = indent;
  }

  public boolean isQuoteAttributes() {
    return quoteAttributes;
  }

  public void setQuoteAttributes(boolean quoteAttributes) {
    this.quoteAttributes = quoteAttributes;
  }

  public String getNewline() {
    return newline;
  }

  public void setNewline(String newline) {
    this.newline = newline;
  }

  public String getLinePrefix() {
    return linePrefix;
  }

  public void setLinePrefix(String linePrefix) {
    this.linePrefix = linePrefix;
  }

  public void incIndent() {
    indentLevel++;
  }

  public void decIndent() {
    if (indentLevel > 0)
      indentLevel--;
  }

  public JSWriter append(String s) {
    if (startOfLine) {
      sb.append(linePrefix);
      for (int i=0; i<indentLevel; i++) {
        sb.append(indent);
      }
      startOfLine = false;
    }
    sb.append(s);
    return this;
  }

  public JSWriter append(char ch) {
    if (startOfLine) {
      sb.append(linePrefix);
      for (int i=0; i<indentLevel; i++) {
        sb.append(indent);
      }
      startOfLine = false;
    }
    sb.append(ch);
    return this;
  }

  public JSWriter appendNewLine() {
    sb.append(newline);
    startOfLine = true;
    return this;
  }

  @Override
  public String toString() {
    return sb.toString();
  }
}
