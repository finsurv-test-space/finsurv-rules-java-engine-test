package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by petruspretorius on 20/10/2016.
 */
public class hasLookupValue implements IAssertionFunction {
  private String lookupName;
  private String lookupKey;
  private String key;
  private String lookupValueStr;

  /*
    Used to test if values match on compound lookups. It does not make sense to use this on a simple lookup.

    @param:lookupName The name of the lookup to search
    @param:lookupKey  The lookup key to use when matching against the field value
    @param:key Once the lookup is found evaluate against this key (Optional)
    @param:lookupValue The value to test against the key of the found lookup item.
     */
  public hasLookupValue(String lookupName, String lookupKey,String key, String lookupValue) {
    this.lookupName = lookupName;
    this.lookupKey = lookupKey;
    this.key = key;
    this.lookupValueStr = lookupValue;
  }

  public hasLookupValue(String lookupName, String lookupKey,String key, Boolean lookupValue) {
    this.lookupName = lookupName;
    this.lookupKey = lookupKey;
    this.key = key;
    this.lookupValueStr = lookupValue.toString();
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (value != null && value instanceof String) {
      String testVal = (String) value;
      Map<String, String> key = new HashMap<String, String>();
      key.put(this.lookupKey, testVal);
      String lookupValue = context.getLookups().getLookupField(this.lookupName, key, this.key);
      if (lookupValue != null && lookupValue.equals(this.lookupValueStr))
        return true;
    }
    return false;
  }
}
