package za.co.synthesis.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;
import za.co.synthesis.rule.support.Util;

public class Validate_IndividualSDA implements ICustomValidate {
    @Override
    public CustomValidateResult call(Object value, Object... otherInputs) {
        String IDNumber = value.toString();

        String trnReference = "";
        String valueDate = "";
        double SDATotal = 0;
        double SDALimit = 1000000.00;
        if (otherInputs.length > 0)
            trnReference = otherInputs[0].toString();
        if (otherInputs.length > 1 && otherInputs[1] != null) {
            valueDate = otherInputs[1].toString();
        }
        if (otherInputs.length > 2 && otherInputs[2] != null) {
            SDATotal = Util.number(otherInputs[2]).doubleValue();
        }
        else {
            SDATotal = 0;
        }

        double totalValueForYear = 0;
        if (trnReference.equals("OverTheLimit"))
            totalValueForYear = SDALimit + 1.00;
        if (trnReference.equals("JustUnderLimit"))
            totalValueForYear = SDALimit - 1.00;
        if (trnReference.equals("TenThousandUnderLimit"))
            totalValueForYear = SDALimit - 10000.00;
        if(totalValueForYear + SDATotal > SDALimit) {
            return new CustomValidateResult(StatusType.Fail, "DAL", "You have exceeded the discretionary amount limit of R" + SDALimit + " (for " + IDNumber + " used during 2020)");
        } else {
            return new CustomValidateResult(StatusType.Pass);
        }
    }
}