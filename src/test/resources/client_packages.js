{
  core: {
    package: ["coreSADC", "featureBranchHub", "featureChecksum", "featureNoEditResident", "featureSchema"],
    regulator: [
      {
        name: "South African Reserve Bank",
        package: ["coreSARB", "coreSARBExternal", "flow", "flow_MD",
                  "featureEntity511", "featureHOLDCO", "featureLimit", "featureMTAAccounts", "featureSARBManB4",
                  "featureTravel"]
      },
      {
        name: "Bank of Namibia",
        package: ["coreBON", "coreBONExternal",
                  "featureBONSchemaNumericID"]
      },
      {
        name: "Royal Bank of Malawi",
        package: ["coreRBM", "coreRBMExternal"]
      },
      {
        name: "Central Bank of Lesotho",
        package: ["coreCBL", "coreCBLExternal"]
      }
  },
  clients: [
    {
      client: "Albaraka",
      regulator: "South African Reserve Bank",
      package: ["albaraka"]
    },
    {
      client: "Investec",
      regulator: "South African Reserve Bank",
      package: ["invBMP", "invCCM", "investec", "investecPL", "invFBCC", "invFlow", "invFXM", "invUXP"]
    },
    {
      client: "HSBC",
      regulator: "South African Reserve Bank",
      package: ["hsbc"]
    },
    {
      client: "Sasfin",
      regulator: "South African Reserve Bank",
      package: ["safin"]
    },
    {
      client: "Standard Bank South Africa",
      regulator: "South African Reserve Bank",
      package: ["stdSARB", "sbIBR1", "sbRAVN", "sbTradeSuite", "sbZA", "stdBankCommon", "stdBankLibra", "stdBankNBOL"]
    },
    {
      client: "Standard Bank Lesotho",
      regulator: "Central Bank of Lesotho",
      package: ["sbLS", "sbLSEBank", "sbLSFin"]
    },
    {
      client: "Standard Bank Malawi",
      regulator: "Royal Bank of Malawi",
      package: ["sbMW"]
    },
    {
      client: "Standard Bank Namibia",
      regulator: "Bank of Namibia",
      package: ["sbNA", "stdBankLibraNA"]
    }
  ]
}