package za.co.synthesis.mapping.core;

import java.util.List;

/**
 * User: jake
 * Date: 1/27/16
 * Time: 4:42 AM
 * Finds the first value in the list
 */
public class FirstPopulatedAggregation implements IAggregation {
  @Override
  public String aggregate(List<String> values) {
    if (values.size() > 0) {
      for (String val : values) {
        if (val != null && val.trim().length() > 0) {
          return val;
        }
      }
    }
    return null;
  }
}
