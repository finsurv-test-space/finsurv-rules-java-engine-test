package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.Scope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 10:40 PM
 * A ruleset is associated with a scope (defines the level in the data heirarchy) and contains a list of field
 * validations for the defined scope
 */
public class RuleSet {
  private final String packageName;
  private final String ruleset;
  private final Scope scope;
  private List<FieldValidation> validations = new ArrayList<FieldValidation>();
  private Map<String, List<FieldValidation>> ruleValidationMap = new HashMap<String, List<FieldValidation>>();

  public RuleSet(String packageName, String ruleset, Scope scope) {
    this.packageName = packageName;
    this.ruleset = ruleset;
    this.scope = scope;
  }

  public RuleSet addValidation(FieldValidation validation) {
    validations.add(validation);
    for (Rule rule : validation.getRules()) {
      List<FieldValidation> fieldValList = ruleValidationMap.get(rule.getName());
      if (fieldValList != null) {
        fieldValList.add(validation);
      }
      else {
        fieldValList = new ArrayList<FieldValidation>();
        fieldValList.add(validation);
        ruleValidationMap.put(rule.getName(), fieldValList);
      }
    }
    for (String field : validation.getFieldList()) {
      List<FieldValidation> fieldValList = ruleValidationMap.get(field);
      if (fieldValList != null) {
        fieldValList.add(validation);
      }
      else {
        fieldValList = new ArrayList<FieldValidation>();
        fieldValList.add(validation);
        ruleValidationMap.put(field, fieldValList);
      }
    }
    return this;
  }

  public String getPackageName() {
    return packageName;
  }

  public String getRuleset() {
    return ruleset;
  }

  public Scope getScope() {
    return scope;
  }

  public List<FieldValidation> getValidations() {
    return validations;
  }

  public List<FieldValidation> getValidationsForName(String name) {
    return ruleValidationMap.get(name);
  }
}
