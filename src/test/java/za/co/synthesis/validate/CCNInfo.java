package za.co.synthesis.validate;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:10 PM
 * Customs Client Number related information required for testing purposes
 */
public class CCNInfo {
  private boolean importUndertaking;
  private boolean validCCN;

  public CCNInfo(boolean importUndertaking, boolean validCCN) {
    this.importUndertaking = importUndertaking;
    this.validCCN = validCCN;
  }

  public CCNInfo() {
  }

  public boolean isValidCCN() {
    return validCCN;
  }

  public void setValidCCN(boolean validCCN) {
    this.validCCN = validCCN;
  }

  public boolean isImportUndertaking() {
    return importUndertaking;
  }

  public void setImportUndertaking(boolean importUndertaking) {
    this.importUndertaking = importUndertaking;
  }
}
