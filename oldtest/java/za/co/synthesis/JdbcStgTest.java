package za.co.synthesis;

import za.co.synthesis.csv.CsvOutput;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.jdbc.JdbcSourceManager;
import za.co.synthesis.mapping.configuration.MappingConfig;
import za.co.synthesis.mapping.core.Entity;
import za.co.synthesis.mapping.core.Mapper;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.core.impl.JSCustomValues;
import za.co.synthesis.rule.core.impl.JSFinsurvContext;
import za.co.synthesis.rule.support.Util;
import za.co.synthesis.validate.*;

import java.sql.*;
/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif
import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 1/18/16
 * Time: 9:08 PM
 * Testing the validation rules against data from a Jdbc datasource
 */
class ProcessReference implements JdbcSourceManager.PullReference {
  private List<String> references;
  private JdbcSourceManager sourceManager;
  private String query;
  int index;
  long lastUniqueNo;
  long startUniqueNo;
  String lastTrnRef = "";
  String startTrnRef = "";
  int bufferSize;
  int recordCount;
  int recordLimit;
  private boolean isString;
  private boolean bopData = false;

  ProcessReference(List<String> references, boolean isString) {
    this.sourceManager = null;
    this.query = null;
    this.references = references;
    this.index = 0;
    this.isString = isString;
  }

  ProcessReference(JdbcSourceManager sourceManager, String query, long startUniqueNo, int bufferSize, int recordLimit) {
    this.sourceManager = sourceManager;
    this.query = query;
    this.references = null;
    this.index = 0;
    this.isString = false;
    this.startUniqueNo = startUniqueNo;
    this.lastUniqueNo = startUniqueNo;
    this.bufferSize = bufferSize;
    this.recordLimit = recordLimit;
    bopData = true;
  }

  ProcessReference(JdbcSourceManager sourceManager, String query, String startTrnRef, int bufferSize, int recordLimit) {
    this.sourceManager = sourceManager;
    this.query = query;
    this.references = null;
    this.index = 0;
    this.isString = false;
    this.startTrnRef = startTrnRef;
    this.lastTrnRef = startTrnRef;
    this.bufferSize = bufferSize;
    this.recordLimit = recordLimit;
  }

  private List<String> selectTransactionList(Connection conn, String query, long startUniqueNo, int bufferSize) {
    List<String> result = new ArrayList<String>();
    Statement stmt = null;
    ResultSet rs = null;

    query = query.replace("{StartNo}", Long.toString(startUniqueNo));
    query = query.replace("{Limit}", Integer.toString(bufferSize));
    try {
      stmt = conn.createStatement();
      rs = stmt.executeQuery(query);
      while(rs.next()){
        Object objVal = rs.getObject(1);
        if (objVal instanceof String)
          isString = true;
        else
          isString = false;
        result.add(objVal.toString());
      }

    } catch (SQLException e) {
      System.out.println("Main SQL: \r\n"+query+"\r\n\r\n");
      System.out.flush();
      e.printStackTrace();
    }
    finally {
      try {
        if(rs!=null)
          rs.close();
        if(stmt!=null)
          stmt.close();
      } catch(SQLException se2){
        // nothing we can do
        se2.printStackTrace();
      }
    }
    return result;
  }

  private List<String> selectTransactionList(Connection conn, String query, String startTrnRef, int bufferSize) {
    List<String> result = new ArrayList<String>();
    Statement stmt = null;
    ResultSet rs = null;

    query = query.replace("{StartNo}", startTrnRef);
    query = query.replace("{Limit}", Integer.toString(bufferSize));
    try {
      stmt = conn.createStatement();
      rs = stmt.executeQuery(query);
      while(rs.next()){
        Object objVal = rs.getObject(1);
        if (objVal instanceof String)
          isString = true;
        else
          isString = false;
        result.add(objVal.toString());
      }

    } catch (SQLException e) {
      System.out.println("Main SQL: \r\n"+query+"\r\n\r\n");
      System.out.flush();
      e.printStackTrace();
    }
    finally {
      try {
        if(rs!=null)
          rs.close();
        if(stmt!=null)
          stmt.close();
      } catch(SQLException se2){
        // nothing we can do
        se2.printStackTrace();
      }
    }
    return result;
  }



  @Override
  public boolean isMore() {
    return (references != null && index < references.size()) || (recordCount < recordLimit);
  }

  @Override
  public boolean isRefString() {
    return isString;
  }

  @Override
  public String getNextReference() {
    if (references == null) {
      int constrainedSize = bufferSize;
      if (constrainedSize > recordLimit-recordCount)
        constrainedSize = recordLimit-recordCount;
      index = 0;
      if (bopData) {
        references = selectTransactionList(sourceManager.getConnection(), query, lastUniqueNo, constrainedSize);
      } else {
        references = selectTransactionList(sourceManager.getConnection(), query, lastTrnRef, constrainedSize);
      }
      if(references.size() == 0) {
        System.out.println();
      }
    }

    if (index < references.size()) {
      String val = references.get(index++);
      if (sourceManager != null && bopData) try {
        lastUniqueNo = Long.parseLong(val);
      } catch (Exception err){
        //System.out.println("val is not a long: "+val);
      }
      lastTrnRef = val;
      recordCount++;
      return val;
    }
    else {
      if(references.size() > 0 && recordCount < recordLimit) {
        references = null;
        return getNextReference();
      }
    }
    return null;
  }
}

public class JdbcStgTest {
  //public static long startUniqueBopCusN = 132475417;//131476147;
  public static long startUniqueTranN = 135888671;//131476147;
  public static String startTrnRef = "";//131476147;

  private static Validator issueValidator(ValidationEngine engine, DefaultSupporting supporting) throws ValidationException {
    engine.setupLoadLookupFile("../finsurv-rules/lookups.js");
    engine.setupLoadRuleFile("../finsurv-rules/transaction.js");
    engine.setupLoadRuleFile("../finsurv-rules/exttransaction.js");
    engine.setupLoadRuleFile("../finsurv-rules/money.js");
    engine.setupLoadRuleFile("../finsurv-rules/extmoney.js");
    engine.setupLoadRuleFile("../finsurv-rules/importexport.js");
    engine.setupLoadRuleFile("../finsurv-rules/extimportexport.js");

    supporting.setCurrentDate(LocalDate.now());
    supporting.setGoLiveDate(Util.date("2013-08-18"));
    engine.setupSupporting(supporting);
    engine.setupLookups(new LookupsTest());
    engine.setupCustomValidateFactory(new TestValidateFactory());

    Validator validator = engine.issueValidator();
    return validator;
  }

  public static void main( String[] args ) throws Exception {
    long startMillis = System.currentTimeMillis();
    ValidationEngine engine = new ValidationEngine();
    DefaultSupporting supporting = new DefaultSupporting();

    Validator validator = issueValidator(engine, supporting);


//    MappingConfig mappingConfig = MappingConfig.load("./src/test/resources/testdb_mapping.js");
    MappingConfig mappingConfig = MappingConfig.load("./src/test/resources/bopcd3_staging_mapping.js");

    CsvOutput csvOutput = new CsvOutput();
    csvOutput.openForWrite("errorlog.txt");

    JdbcSourceManager sourceManager = new JdbcSourceManager();
    // MSSQL
//    sourceManager.setJdbcDriver("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//    sourceManager.setDbUrl("jdbc:sqlserver://10.211.55.3:1433;databaseName=BOPCD");
//    sourceManager.setDbUser("jake");
//    sourceManager.setDbPassword("Marshah");

    //Oracle
    sourceManager.setJdbcDriver("oracle.jdbc.OracleDriver");
    sourceManager.setDbUrl("jdbc:oracle:thin:@dfmodbr1ldm.standardbank.co.za:1526:xbal");
    sourceManager.setDbUser("C736340");
    sourceManager.setDbPassword("ask4help");

    sourceManager.setBufferSize(1000);

    List<String> references = new ArrayList<String>();
//    references.add("1309139205TT2513");
//    references.add("130493784");
//    references.add("130493783");
//    references.add("130534491");
//    sourceManager.open(new ProcessReference(references, true));
//  Getting the list from the database
//    sourceManager.open(new ProcessReference(sourceManager,
//      "SELECT t.UNIQUE_BOPCUS_N FROM " +
//              "CD3_BOPCUS_RECORD t " +
//              "join CD3_BOPCUS_AMOUNTS m " +
//              "on m.UNIQUE_BOPCUS_N = t.UNIQUE_BOPCUS_N " +
//              "WHERE t.UNIQUE_BOPCUS_N > {StartNo} " +
//              "AND ROWNUM <= {Limit} " +
//              //"AND t.UNIQUE_BOPCUS_N >= 132797121" +
//              "", startUniqueBopCusN, 10000, 1050000));

    sourceManager.open(new ProcessReference(sourceManager,

            "SELECT t.TRN_REFERENCE FROM (\n" +
                    "SELECT DISTINCT a.TRN_REFERENCE \n" +
                    "FROM\n" +
                    "CD3_TRAN_STG a\n" +
                    "WHERE \n" +
//                    "a.UNIQUE_TRAN_N >= {StartNo}\n" +
//                    "AND \n" +
//                    "a.TRN_REFERENCE = '1511024055SC1895'\n" +
//                    "AND \n" +
                    "a.VALUE_DATE >= TO_DATE('2015-11-01','YYYY-MM-dd')\n" +
                    "ORDER BY \n" +
                    "a.TRN_REFERENCE\n" +
//                    ",a.UNIQUE_TRAN_N\n" +
                    ") t\n" +
                    "WHERE \n" +
                    "ROWNUM <= {Limit}\n" +
              "", startUniqueTranN, 100000, 100000));
//              "", startUniqueTranN, 10000, 1050000));

    sourceManager.setup(mappingConfig);

    Entity tran;
    Object entry;
    String oldTrnReference = "";
    int processedTransactions = 0;
    while ((entry = sourceManager.getNextEntry()) != null) {
      tran = Mapper.composeEntity(mappingConfig, sourceManager, entry);

      JSObject customData = new JSObject();
      customData.put("DealerType", "AD");
      customData.put("AccountFlow", tran.getJsObject().get("Flow"));

      Object ccnObj = tran.getAnyJsValue("Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber");
      //printUCR(tran.getJsObject(), ccnObj, errorCode, errorDesc);

      String uniqueBopCusNumber = null;
      Object numObj = tran.getMetaData().get("UNIQUE_BOPCUS_N");
      uniqueBopCusNumber = (numObj != null ? numObj.toString() : "");

      String trnReference = null;
      Object trnRefObj = tran.getJsObject().get("TrnReference");
      trnReference = (trnRefObj != null ? trnRefObj.toString() : "");

      Object valueDateObj = tran.getJsValue("ValueDate");
      String comment = (valueDateObj != null) ? valueDateObj.toString() : "";
      comment += " - " + trnReference;

      JSArray oldResults = tran.getJsResults();

      processedTransactions++;
      TestUtility.clearTestInfo();
      if (oldResults.size() > 0){
        for(Object oldResult : oldResults) {
          JSObject oldJsResult = (JSObject) oldResult;
          Object val = oldJsResult.get("ErrorCode");
          String errorCode = (val != null ? val.toString() : "");
          val = oldJsResult.get("ErrorDescription");
          String errorDesc = (val != null ? val.toString() : "");
          val = oldJsResult.get("ErrorAttribute");
          String errorAttribute = (val != null ? val.toString() : "");
          val = oldJsResult.get("Level1SeqNo");
          int seqNo = (val != null ? Integer.parseInt(val.toString()) : 0);
          val = oldJsResult.get("Level2SeqNo");
          int subSeqNo = (val != null ? Integer.parseInt(val.toString()) : 0);
          val = oldJsResult.get("TrnStatus");
          String trnStatus = (val != null ? val.toString() : "");

          if ( trnStatus.equalsIgnoreCase("ACCEPTED")) {
            csvOutput.addOldSuccess(uniqueBopCusNumber, comment);
          } else {
            csvOutput.addOldError(uniqueBopCusNumber, errorCode, errorDesc, errorAttribute, seqNo, subSeqNo, comment);
          }

          TestUtility.populateTestInfo(tran, errorCode, errorDesc, seqNo, subSeqNo, ccnObj);
        }
      } else {
        csvOutput.addOldSuccess(trnReference, comment);
      }

      TestUtility.populateCustomData(ccnObj, customData);

      JSCustomValues customValues = new JSCustomValues(customData);

      supporting.setCurrentDate(Util.date(tran.getJsObject().get("ValueDate")));
      IFinsurvContext context = new JSFinsurvContext(tran.getJsObject());
      ValidationStats stats = new ValidationStats();

      List<ResultEntry> issues = validator.validate(context, customValues, stats);

      if (issues.size() > 0) {
        for (ResultEntry res : issues) {
//          csvOutput.addNewError(uniqueBopCusNumber, res, comment);
          csvOutput.addNewError(trnReference, res, comment);
        }
      } else {
//        csvOutput.addNewSuccess(uniqueBopCusNumber, comment);
        csvOutput.addNewSuccess(trnReference, comment);
      }
      //csvOutput.flushBuffer();

    }

    csvOutput.close();

    long endMillis = System.currentTimeMillis();
    System.out.println("Run Time: " + (endMillis-startMillis) + " millseconds" );
    System.out.println("Processed Record Count: " + processedTransactions );

  }
}
