package za.co.synthesis.rule.core.impl;

import za.co.synthesis.rule.core.IFinsurvContextCache;
import za.co.synthesis.rule.support.CentralCustomValidationCache.ValidationCacheEntry;

import java.util.ArrayList;
import java.util.List;

public class NoopFinsurvContextCache implements IFinsurvContextCache {
    @Override
    public void clearCache() {
    }

    @Override
    public void marshalValidate(ValidationCacheEntry entry) {
    }

    @Override
    public List<ValidationCacheEntry> unmarshalCache() {
        return new ArrayList<ValidationCacheEntry>();
    }
}
