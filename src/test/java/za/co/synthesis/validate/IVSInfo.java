package za.co.synthesis.validate;

/**
 * User: jake
 * Date: 1/20/16
 * Time: 1:02 PM
 * Test IVS information used to support MRN not on IVS
 */
public class IVSInfo {
  private String MRN;
  private boolean onIVS;

  public IVSInfo(String MRN, boolean onIVS) {
    this.MRN = MRN;
    this.onIVS = onIVS;
  }

  public IVSInfo() {
  }

  public String getMRN() {
    return MRN;
  }

  public void setMRN(String MRN) {
    this.MRN = MRN;
  }

  public boolean isOnIVS() {
    return onIVS;
  }

  public void setOnIVS(boolean onIVS) {
    this.onIVS = onIVS;
  }
}
