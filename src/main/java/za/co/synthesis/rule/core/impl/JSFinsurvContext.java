package za.co.synthesis.rule.core.impl;

import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.core.IFinsurvContextCache;
import za.co.synthesis.rule.core.ResultType;
import za.co.synthesis.javascript.*;
import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.rule.support.CentralCustomValidationCache;
import za.co.synthesis.rule.support.DisplayRuleType;

import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 12:00 PM
 * This wraps a JSObject and exposes the Finsurv related functionality needed to validate the transaction
 */
public class JSFinsurvContext implements IFinsurvContext {
  private final JSObject trx;
  private final IFinsurvContextCache validationCache;
  private final JSArray monetaryAmountList;
  private final List<JSArray> importExportLists;

  public JSFinsurvContext(JSObject trn) {
    this(trn, new JSFinsurvContextCache(new JSObject()));
  }

  public JSFinsurvContext(JSObject trn, JSObject meta) {
    this(trn, meta != null ? new JSFinsurvContextCache(meta) : new JSFinsurvContextCache(new JSObject()));
  }

  public JSFinsurvContext(JSObject trn, IFinsurvContextCache validationCache) {
    this.trx = trn;
    this.validationCache = validationCache;
    Object obj = trx.get("MonetaryAmount");
    JSArray array = null;
    if (obj instanceof JSArray) {
      array = (JSArray) obj;
    }
    if (array != null)
      this.monetaryAmountList = array;
    else
      this.monetaryAmountList = new JSArray();
    this.importExportLists = new ArrayList<JSArray>();
    for (Object maObj : this.monetaryAmountList) {
      if (maObj instanceof JSObject) {
        Object ieList = ((JSObject)maObj).get("ImportExport");
        if (ieList instanceof JSArray)
          this.importExportLists.add((JSArray)ieList);
        else
          this.importExportLists.add(new JSArray());
      }
    }
  }

  public IFinsurvContextCache getValidationCache() {
    return validationCache;
  }

  public JSObject getMeta() {
    if (validationCache instanceof JSFinsurvContextCache) {
      return ((JSFinsurvContextCache) validationCache).getJsCache();
    }
    else {
      JSFinsurvContextCache cache = new JSFinsurvContextCache(new JSObject());
      List<CentralCustomValidationCache.ValidationCacheEntry> list = validationCache.unmarshalCache();
      for (CentralCustomValidationCache.ValidationCacheEntry entry : list) {
        cache.marshalValidate(entry);
      }
      return cache.getJsCache();
    }
  }

  public Object getDataField(Scope scope, Object objLevel, final String field) {
    String[] fields = field.split("\\.");
    String fieldName = null;
    for (int i=0; i<fields.length; i++) {
      fieldName = fields[i];
      objLevel = ((JSObject)objLevel).get(fieldName);
      if (objLevel == null) {
        if (i >= fields.length-1)
          return null;
        else {
          if (scope == Scope.Transaction) {

            if (i < 2 && (field.startsWith("Resident.") || field.startsWith("NonResident.")))
              return new IFinsurvContext.Undefined();
          }
          return null;
        }
      }
    }
    return objLevel;
  }

  private Object createFieldPath(Object objBase, Object objLevel, String[] fields, boolean clearPath){
    Object level1Obj = ((JSObject)objBase).get(fields[0]);
    if (level1Obj == null) {
      JSObject newJsObj = new JSObject();
      ((JSObject)objBase).put(fields[0], newJsObj);
      level1Obj = newJsObj;
    }
    objLevel = level1Obj;
    for (int j=1; j<fields.length-1; j++) {
      String fieldName = fields[j];
      if (objLevel instanceof JSObject) {
        JSObject jsObj = (JSObject) objLevel;
        if (clearPath) {
          jsObj.clear();
        }
        JSObject newJsObj = new JSObject();
        jsObj.put(fieldName, newJsObj);
        objLevel = newJsObj;
      }
    }
    return objLevel;
  }

  public void setFieldValue(Scope scope, Object objLevel, final String field, final String value) {
    Object objBase = objLevel;
    String[] fields = field.split("\\.");
    String fieldName;
    for (int i=0; i<fields.length-1; i++) {
      fieldName = fields[i];
      objLevel = ((JSObject)objLevel).get(fieldName);
      if (objLevel == null) {
        if (i >= fields.length-1)
          return;
        else {
          if (scope == Scope.Transaction) {
            if (i < 2 && (field.startsWith("Resident.") || field.startsWith("NonResident."))) {
              objLevel = createFieldPath(objBase, objLevel, fields, true);
              /*
              Object level1Obj = ((JSObject)objBase).get(fields[0]);
              if (level1Obj == null) {
                JSObject newJsObj = new JSObject();
                ((JSObject)objBase).put(fields[0], newJsObj);
                level1Obj = newJsObj;
              }
              objLevel = level1Obj;
              for (int j=1; j<fields.length-1; j++) {
                fieldName = fields[j];
                if (objLevel instanceof JSObject) {
                  JSObject jsObj = (JSObject) objLevel;
                  jsObj.clear();
                  JSObject newJsObj = new JSObject();
                  jsObj.put(fieldName, newJsObj);
                  objLevel = newJsObj;
                }
              }
              */
              break;
            } else {
              objLevel = createFieldPath(objBase, objLevel, fields, false);
              break;
            }
          } else {
            objLevel = createFieldPath(objBase, objLevel, fields, false);
            break;
          }
          //return;
        }
      }
    }
    fieldName = fields.length > 1 ? fields[fields.length-1] : field;
    ((JSObject)objLevel).put(fieldName, value);
  }

  @Override
  public IFinsurvContextCache getFinsurvContextCache() {
    return validationCache;
  }

  public boolean hasField(Object obj, final String field) {
    if (obj instanceof JSObject) {
      return ((JSObject)obj).get(field) != null;
    }
    return false;
  }

  public Object getTransactionField(final String field) {
    return getDataField(Scope.Transaction, trx, field);
  }

  public int getMoneySize() {
    return monetaryAmountList != null ? monetaryAmountList.size() : 0;
  }

  public Object getMoneyField(final int instance, final String field) {
    if (instance >= 0 && instance < monetaryAmountList.size())
      return getDataField(Scope.Money, monetaryAmountList.get(instance), field);
    else
      return new IFinsurvContext.Undefined();
  }

  public int getImportExportSize(final int moneyInstance) {
    if (moneyInstance >= 0 && moneyInstance < monetaryAmountList.size()) {
      List list = importExportLists.get(moneyInstance);
      if (list != null)
        return list.size();
    }
    return 0;
  }

  public Object getImportExportField(final int moneyInstance, final int instance, final String field) {
    if (moneyInstance >= 0 && moneyInstance < monetaryAmountList.size()) {
      List list = importExportLists.get(moneyInstance);
      if (list != null)
        if (instance >= 0 && instance < list.size()) {
          return getDataField(Scope.ImportExport, list.get(instance), field);
        }
    }
    return null;
  }

  public void logTransactionEvent(final ResultType type, final String field, final String code, final String msg) {
  }

  public void logMoneyEvent(final ResultType type, final int instance, final String field, final String code, final String msg) {
  }

  public void logImportExportEvent(final ResultType type, final int moneyInstance, final int instance, final String field, final String code, final String msg) {
  }

  private String resolveValueFromList(final List<String> values) {
    String result = null;
    if (values != null && values.size() > 0)
      result = values.get(0);
    return result;
  }
  public void logTransactionDisplayEvent(final DisplayRuleType type, final String field, final List<String> values) {
    if (type == DisplayRuleType.SetValue || type == DisplayRuleType.SetField) {
      setFieldValue(Scope.Transaction, trx, field, resolveValueFromList(values));
    }
  }

  public void logMoneyDisplayEvent(final DisplayRuleType type, int instance, final String field, final List<String> values) {
    if (type == DisplayRuleType.SetValue) {
      if (instance >= 0 && instance < monetaryAmountList.size()) {
        Object money = monetaryAmountList.get(instance);
        setFieldValue(Scope.Money, money, field, resolveValueFromList(values));
      }
    }
  }

  public void logImportExportDisplayEvent(final DisplayRuleType type, int moneyInstance, int instance, final String field, final List<String> values) {
    if (type == DisplayRuleType.SetValue) {
      if (moneyInstance >= 0 && moneyInstance < monetaryAmountList.size()) {
        List list = importExportLists.get(moneyInstance);
        if (list != null) {
          if (instance >= 0 && instance < list.size()) {
            setFieldValue(Scope.ImportExport, list.get(instance), field, resolveValueFromList(values));
          }
        }
      }
    }
  }
}
