package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 1:02 PM
 * Determine if there is a single import/export entries for the current monetary amount
 */
public class singleImportExport implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return context.getImportExportSize(context.getCurrentMoneyInstance()) == 1;
  }
}
/*
function singleImportExport(context, value) {
  return context.getImportExportSize(context.currentMoneyInstance) == 1;
}
*/


