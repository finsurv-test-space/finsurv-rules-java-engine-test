package za.co.synthesis.rule.core;

/**
 * Created by jake on 6/17/16.
 */
public class ShardResult {
  private final String name;
  private final String[] shards;

  public ShardResult(String name, String[] shards) {
    this.name = name;
    this.shards = shards;
  }

  public String getName() {
    return name;
  }

  public String[] getShards() {
    return shards;
  }
}
