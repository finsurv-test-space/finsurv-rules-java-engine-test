package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.EngineVersion;

import java.util.*;

/**
 * User: jake
 * Date: 3/15/16
 * Time: 10:41 AM
 * This contains the rule package information based on the package.js file
 */
public class RulePackage {
  private EngineVersion engine;
  private EngineVersion evaluationEngine;
  private String dependsOn;
  private List<String> features;
  private Map<String, String> mapping;

  public RulePackage(EngineVersion engine) {
    this.engine = engine;
    this.dependsOn = null;
    this.features = new ArrayList<String>();
    this.mapping = new HashMap<String, String>();
  }

  public RulePackage(RulePackage rulePackage) {
    this.engine = rulePackage.engine;
    this.dependsOn = rulePackage.dependsOn;
    this.features = rulePackage.features != null ? rulePackage.features : new ArrayList<String>();
    this.mapping = rulePackage.mapping != null ? rulePackage.mapping : new HashMap<String, String>();
  }

  public RulePackage(EngineVersion engine, String dependsOn, List<String> features, Map<String, String> mapping) {
    this.engine = engine;
    this.dependsOn = dependsOn;
    this.features = features != null ? features : new ArrayList<String>();
    this.mapping = mapping != null ? mapping : new HashMap<String, String>();
  }

  public void mergePriorPackage(RulePackage dependsOnPackage) {
    EngineVersion version = dependsOnPackage.getValidationEngine();
    if (version != null && version.isHigher(engine)) {
      engine = version;
    }
    version = dependsOnPackage.getEvaluationEngine();
    if (version != null && (evaluationEngine == null || version.isHigher(evaluationEngine))) {
      evaluationEngine = version;
    }
    for (Map.Entry<String, String> entry : dependsOnPackage.getMapping().entrySet()) {
      if (!mapping.containsKey(entry.getKey())) {
        mapping.put(entry.getKey(), entry.getValue());
      }
    }
  }

  public void setEvaluationEngine(EngineVersion engine) {
    this.evaluationEngine = engine;
  }

  public EngineVersion getValidationEngine() {
    return engine;
  }

  public EngineVersion getEvaluationEngine() {
    return evaluationEngine != null ? evaluationEngine : engine;
  }

  public String getDependsOn() {
    return dependsOn;
  }

  public String getDependsOnName() {
    if (dependsOn != null && dependsOn.contains("@")) {
      int pos = dependsOn.indexOf("@");
      return dependsOn.substring(0, pos);
    }
    return dependsOn;
  }

  public String getDependsOnVersion() {
    if (dependsOn != null && dependsOn.contains("@")) {
      int pos = dependsOn.indexOf("@");
      return dependsOn.substring(pos+1);
    }
    return null;
  }

  public int getFeatureSize() {
    return features.size();
  }

  public String getFeature(int index) {
    return features.get(index);
  }

  public String getFeatureName(int index) {
    String feature = features.get(index);
    if (feature != null && feature.contains("@")) {
      int pos = feature.indexOf("@");
      return feature.substring(0, pos);
    }
    return feature;
  }

  public List<String> getFeatures() {
    return features;
  }

  public String getFeatureVersion(int index) {
    String feature = features.get(index);
    if (feature != null && feature.contains("@")) {
      int pos = feature.indexOf("@");
      return feature.substring(pos+1);
    }
    return null;
  }

  public Map<String, String> getMapping() {
    return mapping;
  }
}
