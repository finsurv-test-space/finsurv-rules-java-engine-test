package za.co.synthesis.mapping.configuration;

import za.co.synthesis.mapping.core.Mapper;
import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.javascript.*;

import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 1:06 AM
 * This is a configuration field object that is initialised with data from a JSObject read in from the mapping file.
 * Each field object is a mapping from a source data point to a destination mapping point (or meta data point that
 * is populated and potentially used during the mapping, but is not part of the destination output) and
 * has a number of associated configuration items:
 * + The scope of the field i.e. transaction, money or importexport
 * + The data source and its associated data point name
 * + The destination data point name / or meta data point name
 * + The default value to be used if the source data point is empty
 * + Conditional meta field and associated value that must match for the mapping
 * + Optional external configuration for the field
 */
public class Field {
  private MappingScope scope;
  private Object externalConfig;
  private String source;
  private String sourceColName;
  private String fieldName;
  private String[] fieldParts;
  private String metaName;
  private String defaultValue;
  private String dataType;
  private String dataFormat;
  private String aggregate;
  private String transform;
  private String conditionFieldName;
  private String conditionFieldValue;

  private static String composeString(Object param) {
    if (param != null) {
      if (param instanceof String) {
        return param.toString();
      }
    }
    return null;
  }

  protected static Field setup(MappingScope colScope, String source, JSObject jsFieldMeta) {
    Field result = new Field();
    Object src = jsFieldMeta.get("src");
    Object col = jsFieldMeta.get("col");
    Object field = jsFieldMeta.get("field");
    Object meta = jsFieldMeta.get("meta");
    Object type = jsFieldMeta.get("type");
    Object format = jsFieldMeta.get("format");
    Object aggregate = jsFieldMeta.get("aggregate");
    Object transform = jsFieldMeta.get("transform");
    Object condition = jsFieldMeta.get("condition");
    Object defaultValue = jsFieldMeta.get("default");

    result.source = (src != null) ? composeString(src) : source;
    result.scope = colScope;
    result.sourceColName = composeString(col);
    result.fieldName = composeString(field);
    if (result.fieldName != null) {
      result.fieldParts = result.fieldName.split("\\.");
    }
    result.metaName = composeString(meta);
    result.defaultValue = composeString(defaultValue);

    result.dataType = composeString(type);
    result.dataFormat = composeString(format);
    result.aggregate = composeString(aggregate);
    result.transform = composeString(transform);

    if (result.aggregate == null && result.transform != null && Mapper.hasAggregation(result.transform)) {
      result.aggregate = result.transform;
      result.transform = null;
    }

    if (condition instanceof JSObject) {
      for (Map.Entry<String, Object> entry : ((JSObject) condition).entrySet()) {
        result.conditionFieldName = entry.getKey();
        result.conditionFieldValue = composeString(entry.getValue());
      }
    }

    return result;
  }

  public String getSource() {
    return source;
  }

  public MappingScope getMappingScope() {
    return scope;
  }

  public String getSourceColName() {
    return sourceColName;
  }

  public Object getExternalConfig() {
    return externalConfig;
  }

  public void setExternalConfig(Object externalConfig) {
    this.externalConfig = externalConfig;
  }

  public String getFieldName() {
    return fieldName;
  }

  public String[] getFieldParts() {
    return fieldParts;
  }

  public boolean isMeta() {
    return metaName != null;
  }

  public String getMetaName() {
    return metaName;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

  public String getDataType() {
    return dataType;
  }

  public String getDataFormat() {
    return dataFormat;
  }

  public String getAggregate() {
    return aggregate;
  }

  public String getTransform() {
    return transform;
  }

  public boolean isCondition() {
    return conditionFieldName != null;
  }

  public String getConditionFieldName() {
    return conditionFieldName;
  }

  public String getConditionFieldValue() {
    return conditionFieldValue;
  }
}
