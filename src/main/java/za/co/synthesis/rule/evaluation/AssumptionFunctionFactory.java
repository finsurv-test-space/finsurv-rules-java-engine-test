package za.co.synthesis.rule.evaluation;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.support.StringList;

import java.lang.reflect.Constructor;
import java.util.List;

/*
 * User: jake
 * Date: 6/28/16
 * Time: 10:59 PM
 * Creates a new function (or logical combination of functions based on a given JavaScript definition)
 */
public class AssumptionFunctionFactory {
  private static final String functionPackage = "za.co.synthesis.rule.evaluation.predef.";

  public static IAssumptionAssertionFunction createFunction(final String name) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    return (IAssumptionAssertionFunction)cls.newInstance();
  }

  public static IAssumptionAssertionFunction createFunction(final String name, final String param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class);
    return (IAssumptionAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IAssumptionAssertionFunction createFunction(final String name, final JSRegExLiteral param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(JSRegExLiteral.class);
    return (IAssumptionAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IAssumptionAssertionFunction createFunction(final String name, final List<Object> param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(List.class);
    return (IAssumptionAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IAssumptionAssertionFunction createFunction(final String name, final String param1, final String param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, String.class);
    return (IAssumptionAssertionFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IAssumptionAssertionFunction createFunction(final String name, final String param1, final StringList param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, StringList.class);
    return (IAssumptionAssertionFunction)clsConstructor.newInstance(param1, param2);
  }

  public static IAssumptionAssertionFunction createFunction(final String name, final IAssumptionAssertionFunction param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(IAssumptionAssertionFunction.class);
    return (IAssumptionAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IAssumptionAssertionFunction createFunction(final String name, final Object param1) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(Object.class);
    return (IAssumptionAssertionFunction)clsConstructor.newInstance(param1);
  }

  public static IAssumptionAssertionFunction createFunction(final String name, final String param1, final IAssumptionAssertionFunction param2) throws Exception {
    Class<?> cls = Class.forName(functionPackage + name);

    Constructor<?> clsConstructor = cls.getDeclaredConstructor(String.class, IAssumptionAssertionFunction.class);
    return (IAssumptionAssertionFunction)clsConstructor.newInstance(param1, param2);
  }
}
