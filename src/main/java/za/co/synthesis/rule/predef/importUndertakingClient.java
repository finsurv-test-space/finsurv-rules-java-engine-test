package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 12/10/15
 * Time: 3:39 PM
 * Returns true if there is an ImportUndertakingClient custom value and this value is set to Y
 */
public class importUndertakingClient implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    Object cv = context.getCustomValue().get("LUClient");
    return (cv != null && "Y".equals(cv));
  }
}

/*
   _export.importUndertakingClient = function(context, value) {
    var iuc = context.getCustomValue("ImportUndertakingClient");
    return iuc && (iuc === "Y");
  }
*/
