package za.co.synthesis.rule.support;

/**
 * User: jake
 * Date: 4/19/16
 * Time: 3:38 PM
 * This is the interface for all context functions leveraged by display rules. A display function is any
 * function that takes a FinsurvContext object and returns a string
 */
public interface IDisplayFunction {
  String execute(FinsurvContext context, Object value);
}
