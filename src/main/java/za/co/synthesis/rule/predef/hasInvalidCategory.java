package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 8/8/14
 * Time: 2:23 PM
 * Determines if the current value is an invalid category code
 */
public class hasInvalidCategory implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !context.getLookups().isValidCategory(context.getFlowAsString(), value.toString());
  }
}
/*
function hasInvalidCategory(context, value) {
  if (!value)
    return true;
  var flow = context.flow;
  return !context.lookups.isValidCategory(flow, value);
}
*/