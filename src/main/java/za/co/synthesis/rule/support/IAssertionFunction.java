package za.co.synthesis.rule.support;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 8:06 PM
 * This is the interface for all context functions leveraged by all the finsurv rules. A context function is any
 * function that takes two parameters and returns a boolean value
 * parameters:
 * 1. A data context that allows access to all data entries and
 * 2. the specific value that is being evaluated
 */
public interface IAssertionFunction {
   boolean execute(FinsurvContext context, Object value);
}
