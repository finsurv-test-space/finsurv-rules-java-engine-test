package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 8/7/14
 * Time: 9:03 PM
 * Stores validation statistics from a validation run
 */
public class ValidationStats {
  private int filteredRules;
  private int runRules;
  private int errorCount;
  private int warningCount;
  private int deprecatedCount;
  private int validationCachePuts;
  private int validationCacheHits;

  public int getFilteredRules() {
    return filteredRules;
  }

  public void setFilteredRules(int filteredRules) {
    this.filteredRules = filteredRules;
  }

  public int getRunRules() {
    return runRules;
  }

  public void setRunRules(int runRules) {
    this.runRules = runRules;
  }

  public int getErrorCount() {
    return errorCount;
  }

  public void setErrorCount(int errorCount) {
    this.errorCount = errorCount;
  }

  public int getWarningCount() {
    return warningCount;
  }

  public void setWarningCount(int warningCount) {
    this.warningCount = warningCount;
  }

  public int getDeprecatedCount() {
    return deprecatedCount;
  }

  public void setDeprecatedCount(int deprecatedCount) {
    this.deprecatedCount = deprecatedCount;
  }

  public int getValidationCachePuts() {
    return validationCachePuts;
  }

  public void setValidationCachePuts(int validationCachePuts) {
    this.validationCachePuts = validationCachePuts;
  }

  public int getValidationCacheHits() {
    return validationCacheHits;
  }

  public void setValidationCacheHits(int validationCacheHits) {
    this.validationCacheHits = validationCacheHits;
  }
}
