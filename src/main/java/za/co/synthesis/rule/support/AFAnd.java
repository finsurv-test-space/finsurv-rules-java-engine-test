package za.co.synthesis.rule.support;

/**
 * User: jake
 * Date: 8/2/14
 * Time: 8:27 PM
 * The logical AND context function. The constructor takes in a left and a right context function and then logically
 * ands the result of the execute
 */
public class AFAnd implements IAssertionFunction {
  private IAssertionFunction left;
  private IAssertionFunction right;

  public AFAnd(IAssertionFunction left, IAssertionFunction right) {
    this.left = left;
    this.right = right;
  }

  public boolean execute(FinsurvContext context, Object value) {
    return left.execute(context, value) && right.execute(context, value);
  }
}
