package za.co.synthesis.rule.core.impl;

/**
 * Created by Marais Neethling on 2016/11/07.
 * This validation exception is an unchecked exception that can be thrown during validation routines.
 */
public class ValidationRuntimeException extends RuntimeException {
  public ValidationRuntimeException(String message) {
    super(message);
  }

  public ValidationRuntimeException(String message, java.lang.Throwable throwable) {
    super(message, throwable);
  }
}
