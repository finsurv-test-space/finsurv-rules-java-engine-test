package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 10:59 AM
 * Determines if the current value equals the given transaction field
 */
public class matchesTransactionField implements IAssertionFunction {
  private String field;

  public matchesTransactionField(String field) {
    this.field = field;
  }

  public boolean execute(FinsurvContext context, Object value) {
    Object fieldValue = context.getTransactionField(field);
    if (fieldValue != null) {
      if (fieldValue.equals(value))
        return true;
    }
    return false;
  }
}
/*
function matchesTransactionField(field) {
  return function(context, value) {
    var fieldValue = context.getTransactionField(field);
    if (fieldValue) {
      if (fieldValue === value)
        return true;
    }
    return false;
  }
}
*/