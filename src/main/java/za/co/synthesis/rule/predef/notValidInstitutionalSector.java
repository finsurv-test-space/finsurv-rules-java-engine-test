package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.core.IFinsurvContext;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 3:10 PM
 * Determines if the current value is not a valid Institutional Sector
 */
public class notValidInstitutionalSector implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    if (value == null || value instanceof IFinsurvContext.Undefined)
      return true;
    return !context.getLookups().isValidInstitutionalSector(value.toString());
  }
}

/*
function notValidInstitutionalSector(context, value) {
  return !context.lookups.isValidInstitutionalSector(value);
}
*/