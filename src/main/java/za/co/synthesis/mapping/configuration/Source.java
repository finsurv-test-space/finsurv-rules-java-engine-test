package za.co.synthesis.mapping.configuration;

import za.co.synthesis.javascript.*;
import java.util.HashMap;
import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 1:14 AM
 * This is a configuration field object that is initialised with data from a JSObject read in from the mapping file
 * A source defines where the data for the object is being sourced from
 */
public class Source {
  private String code;
  private final Map<String, Object> configMap = new HashMap<String, Object>();

  protected static Source setup(String code, JSObject jsSource) {
    Source result = new Source();
    result.code = code;

    result.configMap.clear();
    result.configMap.putAll(jsSource);
    return result;
  }

  public String getCode() {
    return code;
  }

  public Map<String, Object> configMap() {
    return configMap;
  }
}
