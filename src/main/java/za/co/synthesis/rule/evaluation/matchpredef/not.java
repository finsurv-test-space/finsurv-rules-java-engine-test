package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.MatchInfo;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;

public class not implements IMatchAssertionFunction {
  private final IMatchAssertionFunction func;

  public not(IMatchAssertionFunction func) {
    this.func = func;
  }

  @Override
  public boolean execute(MatchInfo matchInfo) {
    return !func.execute(matchInfo);
  }
}