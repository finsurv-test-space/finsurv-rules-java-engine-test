package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 8/6/14
 * Time: 3:26 PM
 * This stores the result of any triggered rule (error/warning/deprecated)
 */
public class ResultEntry {
  private ResultType type;
  private String name;
  private String code;
  private String message;
  private Scope scope;
  private int moneyInstance;
  private int importExportInstance;

  public ResultEntry(ResultType type, String name, String code, String message,
                     Scope scope, int moneyInstance, int importExportInstance) {
    this.type = type;
    this.name = name;
    this.code = code;
    this.message = message;
    this.scope = scope;
    this.moneyInstance = moneyInstance;
    this.importExportInstance = importExportInstance;
  }

  public ResultType getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public Scope getScope() {
    return scope;
  }

  public int getMoneyInstance() {
    return moneyInstance;
  }

  public int getImportExportInstance() {
    return importExportInstance;
  }
}
