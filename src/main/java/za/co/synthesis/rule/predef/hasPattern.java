package za.co.synthesis.rule.predef;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jake
 * Date: 8/5/14
 * Time: 10:56 PM
 * Checks to see that the value has the given pattern
 */
public class hasPattern implements IAssertionFunction {
  private Pattern pattern;

  public hasPattern(String regex) {
    this.pattern = Pattern.compile(regex);
  }

  public hasPattern(JSRegExLiteral regex) {
    this.pattern = regex.getPattern();
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (value != null) {
      Matcher matcher = pattern.matcher(value.toString());
      return matcher.find();
    }
    return false;
  }
}
/*
function hasPattern(pattern) {
  return function(context, value) {
    return value && value.match(pattern);
  }
}
*/