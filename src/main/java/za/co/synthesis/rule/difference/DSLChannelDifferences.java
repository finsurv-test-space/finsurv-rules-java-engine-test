package za.co.synthesis.rule.difference;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.difference.document.DocumentDifferences;
import za.co.synthesis.rule.difference.test.TestDifferences;
import za.co.synthesis.rule.difference.test.ValidationTestDifference;
import za.co.synthesis.rule.difference.validation.ValidationDifferences;

import java.io.File;

public class DSLChannelDifferences {
  private String oldRepoPath;
  private String newRepoPath;

  public DSLChannelDifferences(String oldRepoPath, String newRepoPath) {
    this.oldRepoPath = oldRepoPath;
    this.newRepoPath = newRepoPath;
  }

  private static String getChannelPath(final String repoPath, final String channleName) {
    return repoPath + File.separator + channleName;
  }

  public JSObject getValidationDifferences(String channel) throws Exception {
    JSObject jsDoc = new JSObject();
    addValidationDifferences(channel, jsDoc);
    return jsDoc;
  }

  public void addValidationDifferences(String channel, JSObject jsDoc) throws Exception {
    Object objChannel = jsDoc.get(channel);
    JSObject jsChannel;
    if (objChannel instanceof JSObject) {
      jsChannel = (JSObject)objChannel;
    }
    else {
      jsChannel = new JSObject();
      jsDoc.put(channel, jsChannel);
    }

    JSArray packDiffs = new JSArray();
    jsChannel.put("package", packDiffs);
    for (IDifference diff : ValidationDifferences.getPackageDifferences(
            getChannelPath(oldRepoPath, channel),
            getChannelPath(newRepoPath, channel))) {
      diff.addTo(packDiffs);
    }

    JSArray messsageDiffs = new JSArray();
    jsChannel.put("message", messsageDiffs);
    for (IDifference diff : ValidationDifferences.getMessageDifferences(
            getChannelPath(oldRepoPath, channel),
            getChannelPath(newRepoPath, channel))) {
      diff.addTo(messsageDiffs);
    }

    JSArray fieldDiffs = new JSArray();
    jsChannel.put("field", fieldDiffs);
    for (IDifference diff : ValidationDifferences.getFieldDifferences(
            getChannelPath(oldRepoPath, channel),
            getChannelPath(newRepoPath, channel))) {
      diff.addTo(fieldDiffs);
    }

    JSArray ruleDiffs = new JSArray();
    jsChannel.put("rule", ruleDiffs);
    for (IDifference diff : ValidationDifferences.getRuleDifferences(
            getChannelPath(oldRepoPath, channel),
            getChannelPath(newRepoPath, channel))) {
      diff.addTo(ruleDiffs);
    }
  }

  public JSObject getDocumentDifferences(String channel) throws Exception {
    JSObject jsDoc = new JSObject();
    addDocumentDifferences(channel, jsDoc);
    return jsDoc;
  }

  public void addDocumentDifferences(String channel, JSObject jsDoc) throws Exception {
    Object objChannel = jsDoc.get(channel);
    JSObject jsChannel;
    if (objChannel instanceof JSObject) {
      jsChannel = (JSObject)objChannel;
    }
    else {
      jsChannel = new JSObject();
      jsDoc.put(channel, jsChannel);
    }
    JSArray packDiffs = new JSArray();
    jsChannel.put("document", packDiffs);
    for (IDifference diff : DocumentDifferences.getRuleDifferences(
            getChannelPath(oldRepoPath, channel),
            getChannelPath(newRepoPath, channel))) {
      diff.addTo(packDiffs);
    }
  }

  public void addValidationTestDifferences(String channel, JSObject jsDoc) throws Exception {
    Object objChannel = jsDoc.get(channel);
    JSObject jsChannel;
    if (objChannel instanceof JSObject) {
      jsChannel = (JSObject)objChannel;
    }
    else {
      jsChannel = new JSObject();
      jsDoc.put(channel, jsChannel);
    }
    JSArray packDiffs = new JSArray();
    jsChannel.put("validation_test", packDiffs);
    for (IDifference diff : TestDifferences.getValidationDifferences(
            getChannelPath(oldRepoPath, channel),
            getChannelPath(newRepoPath, channel))) {
      diff.addTo(packDiffs);
    }
  }
}
