package za.co.synthesis.rule.matching;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/16/16.
 */
public class MatchingRuleSet {
  private String ruleset;
  private List<MatchConfidence> matchConfidences = new ArrayList<MatchConfidence>();
  private List<MatchBucket> matchBuckets = new ArrayList<MatchBucket>();

  public MatchingRuleSet(String ruleset) {
    this.ruleset = ruleset;
  }

  public MatchingRuleSet addMatchConfidence(MatchConfidence classification) {
    matchConfidences.add(classification);
    return this;
  }

  public MatchingRuleSet addMatchBucket(MatchBucket matchBucket) {
    matchBuckets.add(matchBucket);
    return this;
  }

  public String getRuleset() {
    return ruleset;
  }

  public List<MatchConfidence> getMatchConfidences() {
    return matchConfidences;
  }

  public List<MatchBucket> getMatchBuckets() {
    return matchBuckets;
  }
}
