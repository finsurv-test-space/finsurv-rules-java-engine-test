package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
#else*/
import java.time.LocalDate;
//#endif

/**
 * User: jake
 * Date: 8/8/14
 * Time: 11:12 AM
 * Determines if the current value is more than the given number of days in the future
 */
public class isDaysInFuture implements IAssertionFunction {
  private int days;

  public isDaysInFuture(String days) {
    this.days = Integer.parseInt(days);
  }

  public isDaysInFuture(int days) {
    this.days = days;
  }

  public boolean execute(FinsurvContext context, Object value) {
    LocalDate today = context.getSupporting().getCurrentDate();
    LocalDate futureDate = today.plusDays(days);

    LocalDate date = Util.date(value);
    if (date != null && futureDate != null) {
      return date.isAfter(futureDate);
    }
    return false;
  }
}
/*
function isDaysInFuture(days) {
  return function(context, value) {
    var today = new Date(rule_parameters.todayDate.getTime());
    var futureDate = new Date(rule_parameters.todayDate.getTime());
    futureDate.setDate(today.getDate() + days);

    if (typeof value === "string") {
      var dateParts = value.split("-");
      var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
      return date > futureDate;
    }
    return value > futureDate;
  }
}
*/