package za.co.synthesis.javascript;

import java.io.Reader;
import java.io.StringReader;

/**
 * User: jake
 * Date: 8/3/14
 * Time: 8:56 AM
 * A simple (very fit for purpose) JavaScript parser that is aimed at identifying data structures and not focusing on
 * execution at all
 */
public class JSStructureParser {
  private enum StructureType {
    Expression,
    Value
  }

  private final Reader reader;
  private final StringBuilder recorder;
  private final StringBuilder snippetRecorder;
  private int oldSnippetPos = 0;
  private int snippetPos = 0;
  private int storedCurrent = 0;
  private int current;
  private int line;
  private int column;
  private boolean resolveIdentifiers;
  private boolean inQuote = false;

  public JSStructureParser(Reader reader) {
    this.reader = reader;
    this.recorder = new StringBuilder();
    this.snippetRecorder = new StringBuilder();
  }

  public JSStructureParser(String str) {
    this.reader = new StringReader(str);
    this.recorder = new StringBuilder();
    this.snippetRecorder = new StringBuilder();
  }

  public void setResolveIdentifiers(boolean flag) {
    this.resolveIdentifiers = flag;
  }

  public Object parseValue() throws Exception {
    startSnippet();
    start();
    Object obj;
    obj = readValue(null, JSSnippetSupport.BASE);
    if( !endOfText() ) {
      throw error("Unexpected character");
    }
    if (obj != null && IJSSnippetSupport.class.isAssignableFrom(obj.getClass())) {
      augmentEndSnippet(obj);
    }
    return obj;
  }

  public Object parse() throws Exception {
    startSnippet();
    start();
    Object obj;
    obj = readJavaScript();
    if( !endOfText() ) {
      throw error("Unexpected character");
    }
    if (obj != null && IJSSnippetSupport.class.isAssignableFrom(obj.getClass())) {
      augmentEndSnippet(obj);
    }
    return obj;
  }

  private void start() throws Exception {
    line = 1;
    column = -1;
    read();
  }

  private void startSnippet() {
    snippetPos = snippetRecorder.length();
    oldSnippetPos = snippetPos;
  }

  private void revertLastSnippet() {
    snippetPos = oldSnippetPos;
  }

  private JSSnippet addSeparatorSnippet(IJSSnippetSupport jss) {
    int endPos = snippetRecorder.length() - 1;
    JSSnippet result = new JSSnippet(JSSnippet.Type.Separator, snippetRecorder.substring(snippetPos, endPos));
    jss.addSnippet(result);
    oldSnippetPos = snippetPos;
    snippetPos = endPos;
    return result;
  }

  private void addSeparatorSnippet(IJSSnippetSupport jss, int index) {
    int endPos = snippetRecorder.length() - 1;
    jss.addSnippet(new JSSnippet(JSSnippet.Type.Separator, snippetRecorder.substring(snippetPos, endPos), index));
    oldSnippetPos = snippetPos;
    snippetPos = endPos;
  }

  private JSSnippet creatStartSnippet() {
    int endPos = snippetRecorder.length() - 1;
    JSSnippet result = new JSSnippet(JSSnippet.Type.Start, snippetRecorder.substring(snippetPos, endPos));
    oldSnippetPos = snippetPos;
    snippetPos = endPos;
    return result;
  }

  private void addStartSnippet(IJSSnippetSupport jss) {
    int endPos = snippetRecorder.length() - 1;
    jss.addSnippet(new JSSnippet(JSSnippet.Type.Start, snippetRecorder.substring(snippetPos, endPos)));
    oldSnippetPos = snippetPos;
    snippetPos = endPos;
  }

  private void addEndSnippet(IJSSnippetSupport jss) {
    int endPos = snippetRecorder.length() - 1;
    jss.addSnippet(new JSSnippet(JSSnippet.Type.End, snippetRecorder.substring(snippetPos, endPos)));
    oldSnippetPos = snippetPos;
    snippetPos = endPos;
  }

  private JSSnippetSupport getLastSnippetSupportForObj(Object obj) {
    if (obj instanceof JSObject) {
      return ((JSObject)obj).snippets;
    }
    if (obj instanceof JSArray) {
      return ((JSArray)obj).snippets;
    }
    if (obj instanceof JSIdentifier) {
      JSIdentifier jsId = (JSIdentifier)obj;
      JSSnippetSupport result = jsId.snippets;
      while (jsId.getApply() != null) {
        jsId = jsId.getApply();
        result = jsId.snippets;
      }
      return result;
    }
    if (obj instanceof JSRegExLiteral) {
      return ((JSRegExLiteral)obj).snippets;
    }
    return null;
  }

  private void augmentEndSnippet(Object obj) {
    JSSnippetSupport jsss = getLastSnippetSupportForObj(obj);
    int endPos = snippetRecorder.length() - 1;
    if (endPos > snippetPos) {
      jsss.augmentEndSnippet(snippetRecorder.substring(snippetPos, endPos));
      oldSnippetPos = snippetPos;
      snippetPos = endPos;
    }
  }

  private JSSnippet addVariableSnippet(IJSSnippetSupport jss) {
    int endPos = snippetRecorder.length() - 1;
    JSSnippet result = new JSSnippet(JSSnippet.Type.Variable, snippetRecorder.substring(snippetPos, endPos));
    jss.addSnippet(result);
    oldSnippetPos = snippetPos;
    snippetPos = endPos;
    return result;
  }

  private void addBodyObjectToSnippets(IJSSnippetSupport jss, Object relatedObject) {
    jss.addSnippet(new JSSnippet(JSSnippet.Type.Body, "", relatedObject));
  }

  private void addBodySnippet(IJSSnippetSupport jss) {
    int endPos = snippetRecorder.length() - 1;
    jss.addSnippet(new JSSnippet(JSSnippet.Type.Body, snippetRecorder.substring(snippetPos, endPos)));
    oldSnippetPos = snippetPos;
    snippetPos = endPos;
  }

  private void addValueSnippet(IJSSnippetSupport jss, Object relatedObject) {
    int endPos = snippetRecorder.length() - 1;
    jss.addSnippet(new JSSnippet(JSSnippet.Type.Value, snippetRecorder.substring(snippetPos, endPos), relatedObject));
    oldSnippetPos = snippetPos;
    snippetPos = endPos;
  }

  private Object readJavaScript() throws Exception {
    Object result = null;
    do {
      StructureType structType = determineStructureType();
      if (structType.equals(StructureType.Expression)) {
        if (!(result instanceof JSScope)) {
          JSScope scope = new JSScope(null);
          scope.setPreceding(null, JSSnippetSupport.BASE);
          result = scope;
        }
        readStatement((JSScope)result);
      }
      else
      if (structType.equals(StructureType.Value)) {
        result = readValue(null, JSSnippetSupport.BASE);
      }
      skipWhiteSpace();
    } while (!endOfText());
    return result;
  }

  private StructureType determineStructureType() throws Exception {
    skipWhiteSpace();
    switch( current ) {
      case '"':
        return StructureType.Value;
      case '\'':
        return StructureType.Value;
      case '/':
        return StructureType.Value;
      case '[':
        return StructureType.Value;
      case '{':
        return StructureType.Value;
      case '-':
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        return StructureType.Value;
      default:
        return StructureType.Expression;
    }
  }

  private Object readValue(JSSnippetSupport preceding, int precedingIndex) throws Exception {
    switch( current ) {
      case '"':
        return readString(current);
      case '\'':
        return readString(current);
      case '`':
        return readMultilineString(current);
      case '/':
        return readRegEx();
      case '[':
        return readArray(preceding, precedingIndex);
      case '{':
        return readObject(preceding, precedingIndex);
      case '-':
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        return readNumber();
      default: {
        JSIdentifier id = readExpression(preceding, precedingIndex);
        if (id.isKeyword("null")) {
          revertLastSnippet();
          return null;
        }
        else
        if (id.isKeyword("true")) {
          revertLastSnippet();
          return Boolean.TRUE;
        }
        else
        if (id.isKeyword("false")) {
          revertLastSnippet();
          return Boolean.FALSE;
        }
        else
        if (id.isKeyword("function") && id instanceof JSFunctionCall) {
          JSFunctionDefinition funcDef = new JSFunctionDefinition((JSFunctionCall)id, null);
          funcDef.setPreceding(preceding, precedingIndex);
          skipWhiteSpace();
          if (readChar('{')) {
            funcDef.setReturned(readFunctionBody(funcDef));
          }
          else {
            throw expected("function definition starting with '{'");
          }
          return funcDef;
        }
        else {
          return id;
        }
      }
    }
  }

  private Object readArray(JSSnippetSupport preceding, int precedingIndex) throws Exception {
    read();
    skipWhiteSpace();
    JSArray currentArray = new JSArray();
    currentArray.setPreceding(preceding, precedingIndex);
    addStartSnippet(currentArray);
    if( readChar( ']' ) ) {
      addEndSnippet(currentArray);
      return currentArray;
    }

    int index = 0;
    do {
      skipWhiteSpace();
      // Deal with extra comma at end of array
      if (current == ']') {
        addEndSnippet(currentArray);
        read();
        return currentArray;
      }
      if (index != 0) {
        addSeparatorSnippet(currentArray, index);
      }
      index++;

      // Deal with empty value with bounded commas in the middle of the array
      if (current == ',') {
        currentArray.add(null);
      }
      else {
        Object value = readValue(currentArray.snippets, currentArray.currentSnippetIndex());
        currentArray.add(value);
        if (value != null && !IJSSnippetSupport.class.isAssignableFrom(value.getClass())) {
          addValueSnippet(currentArray, value);
        }
      }
      skipWhiteSpace();
    } while( readChar( ',' ) );
    if( !readChar( ']' ) ) {
      throw expected( "',' or ']'" );
    }
    addEndSnippet(currentArray);
    return currentArray;
  }

  // This is called when a var is encountered. Support is provided for 2 scenarios:
  // 1) A list of variables is defined i.e. var x, y, z;
  // 2) A single variable is defined and set i.e. var x = 2;
  private void readVariableDefinitions(JSObject jsObj) throws Exception {
    boolean bLookForVariable;
    do {
      skipWhiteSpace();
      String name = readName();
      skipWhiteSpace();

      if (readChar('=')) {
        skipWhiteSpace();
        JSSnippet jssVar = addVariableSnippet(jsObj);
        Object objValue = readValue(jsObj.snippets, jsObj.currentSnippetIndex());
        jssVar.setRelatedObject(objValue);
        jsObj.put(name, objValue);
        skipWhiteSpace();
        bLookForVariable = false;
      }
      else
      if (readChar(',')) {
        addVariableSnippet(jsObj);
        jsObj.put(name, null);
        bLookForVariable = true;
      }
      else
      if (current == ';') {
        addVariableSnippet(jsObj);
        jsObj.put(name, null);
        break;
      }
      else
        throw expected( "'=', ',' or ';'" );

      if (!bLookForVariable) {
        if (readChar(',')) {
          bLookForVariable = true;
        }
        else
        if (current == ';') {
          break;
        }
      }
    } while(bLookForVariable);
  }

  private JSScope findScopeForVariable(final String name, JSScope scope) {
    if (scope.containsKey(name)) {
      return scope;
    }
    if (scope.getParentScope() != null) {
      return findScopeForVariable(name, scope.getParentScope());
    }
    return scope;
  }

  private Object resolveIfIdentifier(Object potentialId, JSScope scope) {
    if (potentialId instanceof JSIdentifier) {
      String name = ((JSIdentifier)potentialId).getName();
      JSScope varScope = findScopeForVariable(name, scope);
      return varScope.get(name);
    }
    else {
      return potentialId;
    }
  }

  private void resolveIfIdentifier(JSArray values, JSScope scope) {
    for (int i=0; i<values.size(); i++) {
      Object resolvedObj = resolveIfIdentifier(values.get(i), scope);
      values.set(i, resolvedObj);
    }
  }

  private Object readStatement(JSScope scope) throws Exception {
    Object result = null;
    skipWhiteSpace();
    boolean bExpectingSemiColon;
    JSIdentifier id = readExpression(scope.snippets, scope.currentSnippetIndex());
    if (id instanceof JSFunctionCall) {
      skipWhiteSpace();
      if (readChar('{')) {
        JSFunctionDefinition funcDef = new JSFunctionDefinition((JSFunctionCall)id, scope);
        addBodyObjectToSnippets(scope, funcDef);
        funcDef.setReturned(readFunctionBody(funcDef));
        skipWhiteSpace();

        if (resolveIdentifiers) {
          resolveIfIdentifier(funcDef.getParameters(), scope);
          funcDef.setReturned(resolveIfIdentifier(funcDef.getReturned(), scope));
        }
        scope.put(funcDef.getName(), funcDef);
        bExpectingSemiColon = false;
      }
      else {
        if (resolveIdentifiers) {
          JSFunctionCall funcCall = (JSFunctionCall)id;
          resolveIfIdentifier(funcCall.getParameters(), scope);
        }
        scope.put(id.getName(), id);
        addBodyObjectToSnippets(scope, id);
        bExpectingSemiColon = true;
      }
    }
    else
    if (id.getName().equals("var")) {
      revertLastSnippet();
      readVariableDefinitions(scope);
      bExpectingSemiColon = true;
    }
    else
    if (id.getName().equals("return")) {
      revertLastSnippet();
      skipWhiteSpace();
      addBodySnippet(scope);
      result = readValue(scope.snippets, scope.currentSnippetIndex());
      addBodyObjectToSnippets(scope, result);
      if (resolveIdentifiers) {
        result = resolveIfIdentifier(result, scope);
      }
      skipWhiteSpace();
      bExpectingSemiColon = true;
    }
    else {
      skipWhiteSpace();
      if (readChar('=')) {
        skipWhiteSpace();
        revertLastSnippet();
        JSSnippet jssVar = addVariableSnippet(scope);
        JSScope varScope = findScopeForVariable(id.getName(), scope);
        Object value = readValue(scope.snippets, scope.currentSnippetIndex());
        jssVar.setName(id.getName());
        if (value != null && IJSSnippetSupport.class.isAssignableFrom(value.getClass())) {
          jssVar.setRelatedObject(value);
        }
        else {
          addValueSnippet(scope, value);
        }
        if (resolveIdentifiers) {
          value = resolveIfIdentifier(value, scope);
        }
        scope.put(id.getName(), value);
        if (varScope != scope) {
          varScope.put(id.getName(), value);
        }
        skipWhiteSpace();
        bExpectingSemiColon = true;
      }
      else {
        throw expected("variable declaration, setting a variable or declaring a function");
      }
    }
    if (!readChar( ';' )) {
      if (bExpectingSemiColon && current != '}' && !endOfText()) // We can squash expected ; if we get a closing curly brace }
        throw expected(";");
    }
    return result;
  }

  private Object readFunctionBody(JSFunctionDefinition jsFuncDef) throws Exception {
    Object result = null;
    read();
    skipWhiteSpace();
    addStartSnippet(jsFuncDef.getScope());
    if( readChar( '}' ) ) {
      return result;
    }
    do {
      Object res = readStatement(jsFuncDef.getScope());
      if (result == null)
        result = res;
      skipWhiteSpace();
    } while( !readChar( '}' ) );
    addEndSnippet(jsFuncDef.getScope());
    return result;
  }

  private Object readObject(JSSnippetSupport preceding, int precedingIndex) throws Exception {
    read();
    skipWhiteSpace();
    JSObject currentObj = new JSObject();
    currentObj.setPreceding(preceding, precedingIndex);
    addStartSnippet(currentObj);
    if( readChar( '}' ) ) {
      addEndSnippet(currentObj);
      return currentObj;
    }
    boolean bFirst = true;
    JSSnippet jssSeparator = null;
    do {
      skipWhiteSpace();
      // Deal with extra comma at end of object
      if (current == '}') {
        addEndSnippet(currentObj);
        read();
        return currentObj;
      }
      if (bFirst) {
        bFirst = false;
      }
      else {
        jssSeparator = addSeparatorSnippet(currentObj);
      }
      String name = readName();
      if (jssSeparator != null) {
        jssSeparator.setName(name);
      }
      skipWhiteSpace();
      if( !readChar( ':' ) ) {
        throw expected( "':'" );
      }
      skipWhiteSpace();
      JSSnippet jssVar = addVariableSnippet(currentObj);
      Object value = readValue(currentObj.snippets, currentObj.currentSnippetIndex());
      jssVar.setName(name);
      if (value != null && IJSSnippetSupport.class.isAssignableFrom(value.getClass())) {
        jssVar.setRelatedObject(value);
      }
      else {
        addValueSnippet(currentObj, value);
      }
      currentObj.put(name, value);
      skipWhiteSpace();
    } while( readChar( ',' ) );

    if( !readChar( '}' ) ) {
      throw expected( "',' or '}'" );
    }
    addEndSnippet(currentObj);
    return currentObj;
  }

  private void readRequiredChar( char ch ) throws Exception {
    if( !readChar( ch ) ) {
      throw expected( "'" + ch + "'" );
    }
  }

  private Object readString(int quote) throws Exception {
    inQuote = true;
    read();
    try {
      recorder.setLength(0);
      while (current != quote) {
        if (current == '\\') {
          readEscape();
        } else if (current < 0x20) {
          throw expected("valid string character");
        } else {
          recorder.append((char) current);
          read();
        }
      }
    }
    finally {
      inQuote = false;
    }
    read();
    return recorder.toString();
  }

  private Object readMultilineString(int quote) throws Exception {
    inQuote = true;
    read();
    try {
      inQuote = true;
      recorder.setLength(0);
      boolean justNewlined = false;
      int myquote = -1;
      while (current != quote) {
        if (current == '\n' || current == '\r') {
          if (!justNewlined || current == myquote) {
            myquote = current;
            justNewlined = true;
            recorder.append('\n');
            read();
          }
          else
            read();
        }
        else {
          justNewlined = false;
          myquote = -1;
          if (current == '\\') {
            readEscape();
          } else {
            recorder.append((char) current);
            read();
          }
        }
      }
    }
    finally {
      inQuote = false;
    }
    read();
    return recorder.toString();
  }

  private JSRegExLiteral readRegEx() throws Exception {
    read();
    try {
      inQuote = true;
      recorder.setLength( 0 );
      while( current != '/' ) {
        recorder.append( (char)current );
        read();
      }
    }
    finally {
      inQuote = false;
    }
    read();
    try {
      JSRegExLiteral regex = new JSRegExLiteral(recorder.toString());
      addVariableSnippet(regex);
      return regex;
    }
    catch (Exception e) {
      throw expected("legal regex: " + e.getMessage());
    }
  }

  private void readEscape() throws Exception {
    read();
    switch( current ) {
    case '\'':
    case '"':
    case '/':
    case '\\':
      recorder.append( (char)current );
      break;
    case 'b':
      recorder.append( '\b' );
      break;
    case 'f':
      recorder.append( '\f' );
      break;
    case 'n':
      recorder.append( '\n' );
      break;
    case 'r':
      recorder.append( '\r' );
      break;
    case 't':
      recorder.append( '\t' );
      break;
    case 'u':
      char[] hexChars = new char[4];
      for( int i = 0; i < 4; i++ ) {
        read();
        if( !isHexDigit( current ) ) {
          throw expected( "hexadecimal digit" );
        }
        hexChars[i] = (char)current;
      }
      recorder.append( (char)Integer.parseInt( String.valueOf( hexChars ), 16 ) );
      break;
    default:
      throw expected( "valid escape sequence" );
    }
    read();
  }

  private Object readNumber() throws Exception {
    recorder.setLength(0);
    readAndAppendChar( '-' );
    int firstDigit = current;
    if( !readAndAppendDigit() ) {
      throw expected( "digit" );
    }
    if( firstDigit != '0' ) {
      while( readAndAppendDigit() ) {
      }
    }
    readFraction();
    readExponent();
    return recorder.toString();
  }

  private boolean readFraction() throws Exception {
    if( !readAndAppendChar( '.' ) ) {
      return false;
    }
    if( !readAndAppendDigit() ) {
      throw expected( "digit" );
    }
    while( readAndAppendDigit() ) {
    }
    return true;
  }

  private boolean readExponent() throws Exception {
    if( !readAndAppendChar( 'e' ) && !readAndAppendChar( 'E' ) ) {
      return false;
    }
    if( !readAndAppendChar( '+' ) ) {
      readAndAppendChar( '-' );
    }
    if( !readAndAppendDigit() ) {
      throw expected( "digit" );
    }
    while( readAndAppendDigit() ) {
    }
    return true;
  }

  private String readName() throws Exception {
    if( current == '"' || current == '\'' ) {
      return readString(current).toString();
    }
    if ((current >= 'a' && current <= 'z') || (current >= 'A' && current <= 'Z') || current == '_' || current == '$') {
      recorder.setLength( 0 );
      recorder.append((char)current);
      read();
      while ((current >= 'a' && current <= 'z') || (current >= 'A' && current <= 'Z') ||
             (current >= '0' && current <= '9') || current == '_' || current == '$') {
        recorder.append((char)current);
        read();
      }
      return recorder.toString();
    }
    else {
      throw expected( "name" );
    }
  }

  private JSArray readParameters(JSSnippetSupport jss) throws Exception {
    if( readChar( ')' ) ) {
      return null;
    }
    int index = 0;
    JSArray currentArray = new JSArray(jss);
//    currentArray.setPreceding(preceding);
    do {
      skipWhiteSpace();
      // Deal with extra comma at end of parameter list
      if (current == ')') {
        read();
        return currentArray;
      }
      // Deal with empty value with bounded commas in the middle of the parameters
      if (current == ',') {
        currentArray.add(null);
      }
      else {
        if (index > 0) {
          addSeparatorSnippet(currentArray, index);
        }
        index++;
        Object value = readValue(jss, jss.currentSnippetIndex());
        if (value != null && !IJSSnippetSupport.class.isAssignableFrom(value.getClass())) {
          addValueSnippet(currentArray, value);
        }
        currentArray.add(value);
      }
      skipWhiteSpace();
    } while( readChar( ',' ) );

    if( !readChar( ')' ) ) {
      throw expected( "',' or ')'" );
    }
    return currentArray;
  }

  private JSIdentifier readExpression(JSSnippetSupport preceding, int precedingIndex) throws Exception {
    if ((current >= 'a' && current <= 'z') || (current >= 'A' && current <= 'Z') || current == '_' || current == '$') {
      recorder.setLength( 0 );
      recorder.append((char)current);
      read();

      while ((current >= 'a' && current <= 'z') || (current >= 'A' && current <= 'Z') ||
             (current >= '0' && current <= '9') || current == '_' || current == '$') {
        recorder.append((char)current);
        read();
      }
      skipWhiteSpace();
      // Deal with case where dot notation (with subsequent identifier) follows an identifier
      if (current == '.') {
        JSIdentifier result = new JSIdentifier(recorder.toString());
        result.setPreceding(preceding, precedingIndex);
        read();
        skipWhiteSpace();
        addStartSnippet(result);
        result.setApply(readExpression(result.snippets, JSSnippetSupport.BASE));
        return result;
      }
      else
      if (current == '(') {
        String funcName = recorder.toString();
        read();
        skipWhiteSpace();
        JSSnippet startSnippet = creatStartSnippet();
        JSFunctionCall result = new JSFunctionCall(funcName);
        result.setPreceding(preceding, precedingIndex);
        result.addSnippet(startSnippet);
        result.setParameters(readParameters(result.getSnippets()));
        addEndSnippet(result);

        // Deal with case where dot notation (with subsequent identifier) follows a close bracket
        skipWhiteSpace();
        if (current == '.') {
          read();
          skipWhiteSpace();
          addSeparatorSnippet(result);

          result.setApply(readExpression(result.snippets, JSSnippetSupport.BASE));
        }
        return result;
      }
      JSIdentifier result = new JSIdentifier(recorder.toString());
      result.setPreceding(preceding, precedingIndex);
      addStartSnippet(result);
      return result;
    }
    else {
      throw expected( "identifier" );
    }
  }

  private boolean readAndAppendChar( char ch ) throws Exception {
    if( current != ch ) {
      return false;
    }
    recorder.append( ch );
    read();
    return true;
  }

  private boolean readChar( char ch ) throws Exception {
    if( current != ch ) {
      return false;
    }
    read();
    return true;
  }

  private boolean readAndAppendDigit() throws Exception {
    if( !isDigit( current ) ) {
      return false;
    }
    recorder.append( (char)current );
    read();
    return true;
  }

  private void skipWhiteSpace() throws Exception {
    while( isWhiteSpace( current ) && !endOfText() ) {
      read();
    }
  }

  private void skipUpToWith() throws Exception {
    StringBuilder currentIdentifier = null;
    while (!endOfText()) {
      if (currentIdentifier == null) {
        if (isAlpha(current)) {
          currentIdentifier = new StringBuilder();
          currentIdentifier.append((char)current);
        }
      }
      else {
        if (isAlpha(current) || isDigit(current)) {
          currentIdentifier.append((char)current);
        }
        else {
          String identifier = currentIdentifier.toString();
          if (identifier.equals("with")) {
            while (!endOfText()) {
              if (current == '{') {
                read();
                break;
              }
              read();
            }
            break;
          }
          currentIdentifier = null;
        }
      }
      read();
    }
  }

  /**
   * This central function reads the stream of data and expressly omits comments from the stream. This allows the rest
   * of the code to happily process the JavaScript without worrying about looking for comments
   * @throws Exception
   */
  private void read() throws Exception {
    if( endOfText() ) {
      throw error( "Unexpected end of input" );
    }
    do {
      if( current == '\n' ) {
        line++;
        column = 0;
      } else {
        column++;
      }

      if (storedCurrent != 0) {
        current = storedCurrent;
        storedCurrent = 0;
      }
      else {
        current = reader.read();

        if ((line == 1 && column == 0) && (current == 65279 || current == 239)) { // BOM
          current = reader.read();
          if(current == 187) {
            current = reader.read();
            if(current == 191) {
              current = reader.read();
            }
          }

          snippetRecorder.append((char)current);
        } else {
          snippetRecorder.append((char)current);
        }
      }

      // Deal with comments (that do not appear in quotes)
      if (!inQuote) {
        if (current == '/') {
          current = reader.read();
          snippetRecorder.append((char)current);
          if (current == '/') {
            // in a single line comment
            do {
              current = reader.read();
              snippetRecorder.append((char)current);
            } while (current != '\n');
            continue;
          } else if (current == '*') {
            // in a multi-line line comment
            boolean haveStar = false;
            do {
              current = reader.read();
              snippetRecorder.append((char)current);
              if (current == '\n') {
                line++;
                column = 0;
              } else {
                column++;
              }
              if (haveStar && current == '/') {
                current = reader.read();
                snippetRecorder.append((char)current);
                break;
              }
              haveStar = (current == '*');
            } while (true);
          } else {
            // we do not have a comment so therefore remember read character to play back
            storedCurrent = current;
            current = '/';
          }
        }
      }
      break;
    } while (true);
  }

  private boolean endOfText() {
    return current == -1;
  }

  private JSStructureException expected( String expected ) {
    if( endOfText() ) {
      return error( "Unexpected end of input" );
    }
    return error( "Expected " + expected );
  }

  private JSStructureException error( String message ) {
    return new JSStructureException( message, line, column );
  }

  private static boolean isWhiteSpace( int ch ) {
    return ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r';
  }

  private static boolean isDigit( int ch ) {
    return ch >= '0' && ch <= '9';
  }

  private static boolean isHexDigit( int ch ) {
    return ch >= '0' && ch <= '9' || ch >= 'a' && ch <= 'f' || ch >= 'A' && ch <= 'F';
  }

  private static boolean isAlpha( int ch ) {
    return ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z';
  }
}
