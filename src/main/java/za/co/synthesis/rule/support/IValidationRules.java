package za.co.synthesis.rule.support;

import za.co.synthesis.rule.core.FlowType;
import za.co.synthesis.rule.core.ValidationSchema;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 8/10/14
 * Time: 2:04 PM
 * An interface that is used for retrieving validation rules. The idea is that it is possible to inject a validation
 * rule cache without impacting on the existng code.
 */
public interface IValidationRules {
  public static class ValidationList extends ArrayList<Validation> {
    public ValidationList(int i) {
      super(i);
    }

    public ValidationList() {
    }

    public ValidationList(Collection<? extends Validation> validations) {
      super(validations);
    }
  }

  ValidationList validationRules(Map<String, String> mapping, FlowType flow, String section, List<String> categories, List<String> mainCategories, String rulename);
}
