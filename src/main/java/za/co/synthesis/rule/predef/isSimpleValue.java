package za.co.synthesis.rule.predef;

import za.co.synthesis.javascript.JSRegExLiteral;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;

public class isSimpleValue implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    return value == null || value instanceof String || value instanceof Boolean || value instanceof JSRegExLiteral;
  }
}