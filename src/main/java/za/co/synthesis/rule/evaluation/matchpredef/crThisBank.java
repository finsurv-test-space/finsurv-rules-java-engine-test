package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.Context;
import za.co.synthesis.rule.evaluation.MatchInfo;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;

public class crThisBank implements IMatchAssertionFunction {
  @Override
  public boolean execute(MatchInfo matchInfo) {
    String BIC = matchInfo.getCrBankBIC();

    if (BIC != null) {
      return BIC.equals(Context.THISBANK) || BIC.matches(matchInfo.getContext().getWhoAmIRegex());
    }
    return false;
  }
}