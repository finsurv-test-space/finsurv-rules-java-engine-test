define(function () {
  return function (predef) {
    var docs;
    with (predef) {
      docs = {
        ruleset: "Transaction Documents",
        scope: "transaction",
        validations: [
          {
            field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
            rules: [
              document('dtcc1', "TaxClearance", "Provide scan of the Tax Clearance certificate",
                notEmpty).onOutflow().onSection("AB").onCategory(['512','513'])
            ]
          },
          {
            field: "Resident.Individual.IDNumber",
            rules: [
              document("drid1", "ID_Document", "In the case where the ID number fails validation provide scan of ID Document",
                notEmpty.and(notValidRSAID)).onSection("AB").notOnCategory("833")
            ]
          },
          {
            field: "NonResident.Individual.PassportNumber",
            rules: [
              document("dnripn1", "PassportDocument", "Provide scan of Passport Document {{value}} - {{NonResident.Individual.Name}} {{NonResident.Individual.Surname}}",
                notEmpty).onSection("AB").onCategory(["304","305","306","409"]),

              document("dnripn2", "ProofOfTravel", "Provide scan of air ticket or other travel ticket for {{Resident.Individual.Name}} {{Resident.Individual.Surname}}",
                notEmpty.and(not(hasAllMoneyField("ThirdParty.Individual.PassportNumber")))).onSection("AB").onCategory("256")
            ]
          }
        ]
      };
    }
    return docs;
  }
});