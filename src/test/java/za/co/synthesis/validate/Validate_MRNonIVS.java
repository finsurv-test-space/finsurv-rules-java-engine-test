package za.co.synthesis.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.util.Map;

/**
 * User: jake
 * Date: 1/20/16
 * Time: 1:00 PM
 * Test implementation of the function to validate errors relating to MRNNotOnIVS:
 * 221, "If outflow and the category 103/01 to 103/10 or 105 or 106, and no MRN can be found on the IVS, must contain the value Y"
 * 232, "If the MRN is registered on the IVS, it must not be completed"
 * 500, "If outflow and the category 103/01 to 103/10 or 105 or 106, the number supplied must be a valid MRN stored in the IVS"
 */
public class Validate_MRNonIVS implements ICustomValidate {
  private Map<String, IVSInfo> ivsInfoMap;

  public Validate_MRNonIVS(Map<String, IVSInfo> ivsInfoMap) {
    this.ivsInfoMap = ivsInfoMap;
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String mrnNotOnIVS = value != null ? value.toString() : "";
    String importControlNo = null;
    if (otherInputs.length > 0)
      importControlNo = otherInputs[0].toString();

    if (importControlNo != null) {
      if (ivsInfoMap.containsKey(importControlNo)) {
        IVSInfo ivsInfo = ivsInfoMap.get(importControlNo);

        if (ivsInfo.isOnIVS()) {
          if (mrnNotOnIVS.equals("Y")) {
            String errorCode = "232";
            String errorDesc = "If the MRN is registered on the IVS, it must not be completed";
            return new CustomValidateResult(StatusType.Fail, errorCode, errorDesc);
          }
        }
        else {
          // Not on IVS
          if (!mrnNotOnIVS.equals("Y")) {
            String errorCode = "221";
            String errorDesc = "If outflow and the category 103/01 to 103/10 or 105 or 106, and no MRN can be found on the IVS, must contain the value Y";
            return new CustomValidateResult(StatusType.Fail, errorCode, errorDesc);
          }
        }
      }
    }
    return new CustomValidateResult(StatusType.Pass);
  }
}

