package za.co.synthesis.rule.evaluation.matchpredef;

import za.co.synthesis.rule.evaluation.EvaluationUtil;
import za.co.synthesis.rule.evaluation.IMatchAssertionFunction;
import za.co.synthesis.rule.evaluation.MatchInfo;
import za.co.synthesis.rule.support.StringList;

public class crHasValue implements IMatchAssertionFunction {
  private final String field;
  private final String operation;
  private final StringList constants;

  public crHasValue(String field, String operation, String constant) {
    this.field = field;
    this.operation = operation;
    this.constants = new StringList();
    if (constant != null) {
      this.constants.add(constant);
    }
  }

  public crHasValue(String field, String operation, StringList constants) {
    this.field = field;
    this.operation = operation;
    this.constants = constants;
  }

  public crHasValue(String field) {
    this.field = field;
    this.operation = EvaluationUtil.EXISTS;
    this.constants = null;
  }

  @Override
  public boolean execute(MatchInfo matchInfo) {
    Object objValue = EvaluationUtil.getCrValueForMatchField(matchInfo, field);
    return EvaluationUtil.compareValues(objValue, operation, constants);
  }

}
