package za.co.synthesis.rule.difference.test;

import za.co.synthesis.javascript.JSFunctionCall;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.difference.DiffUtils;
import za.co.synthesis.rule.difference.FieldRule;
import za.co.synthesis.rule.difference.IDifference;
import za.co.synthesis.rule.difference.RuleMap;
import za.co.synthesis.rule.difference.validation.*;
import za.co.synthesis.rule.support.*;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.*;

/***
 * This class is used to determine the test differences between two packages. It is important to understand that this checks
 * for differences between the raw packages as opposed to differences between compound packages (with all dependsOn dependencies included).
 * This is typically used to determine the edits have been made to the tests.
 */
public class TestDifferences {
  private static class RuleInstances {
    public final String testRuleName;
    public final List<JSFunctionCall> oldTests = new ArrayList<JSFunctionCall>();
    public final List<JSFunctionCall> newTests = new ArrayList<JSFunctionCall>();

    public RuleInstances(String testRuleName) {
      this.testRuleName = testRuleName;
    }
  }

  private static class TestRuleMap extends HashMap<String, RuleInstances> {
    private final List<String> putOrder = new ArrayList<String>();

    @Override
    public RuleInstances put(String key, RuleInstances value) {
      if (!putOrder.contains(key)) {
        putOrder.add(key);
      }
      return super.put(key, value);
    }

    public List<String> keysInOrder() {
      return putOrder;
    }

    @Override
    public Collection<RuleInstances> values() {
      if (putOrder.size() > 0) {
        List<RuleInstances> values = new ArrayList<RuleInstances>();
        for (String key : putOrder) {
          values.add(get(key));
        }
        return values;
      }
      else {
        return super.values();
      }
    }

    @Override
    public void clear() {
      putOrder.clear();
      super.clear();
    }

    @Override
    public RuleInstances remove(Object key) {
      putOrder.remove(key.toString());
      return super.remove(key);
    }
  }

  private static void populateTestRuleMap(List<JSFunctionCall> oldtests, List<JSFunctionCall> newtests, TestRuleMap ruleMap) {
    for (JSFunctionCall jsFunc : oldtests) {
      String param1 = jsFunc.getParameters().get(0).toString();
      RuleInstances ri = ruleMap.get(param1);
      if (ri == null) {
        ri = new RuleInstances(param1);
        ruleMap.put(param1, ri);
      }
      ri.oldTests.add(jsFunc);
    }
    for (JSFunctionCall jsFunc : newtests) {
      String param1 = jsFunc.getParameters().get(0).toString();
      RuleInstances ri = ruleMap.get(param1);
      if (ri == null) {
        ri = new RuleInstances(param1);
        ruleMap.put(param1, ri);
      }
      ri.newTests.add(jsFunc);
    }
  }

  private static boolean areRulesTheSame(JSFunctionCall oldTest, JSFunctionCall newTest) {
    return oldTest.getParameters().equals(newTest.getParameters());
  }

  public static List<IDifference> getValidationDifferences(String oldPackagePath, String newPackagePath) throws Exception {
    List<JSFunctionCall> oldTests = DiffUtils.loadValidationTests(oldPackagePath);
    List<JSFunctionCall> newTests = DiffUtils.loadValidationTests(newPackagePath);

    TestRuleMap ruleMap = new TestRuleMap();
    populateTestRuleMap(oldTests, newTests, ruleMap);

    List<IDifference> results = new ArrayList<IDifference>();
    for(RuleInstances ri : ruleMap.values()) {
      int len = ri.oldTests.size();
      if (ri.newTests.size() > len) {
        len = ri.newTests.size();
      }
      for (int i=0; i<len; i++) {
        JSFunctionCall oldTest = (i < ri.oldTests.size()) ? ri.oldTests.get(i) : null;
        JSFunctionCall newTest = (i < ri.newTests.size()) ? ri.newTests.get(i) : null;
        if (oldTest == null && newTest != null) {
          Object objData = (newTest.getParameters().size() >= 2) ? newTest.getParameters().get(1) : null;
          Object objCustom = (newTest.getParameters().size() >= 3) ? newTest.getParameters().get(2) : null;
          if (objData != null) {
            results.add(ValidationTestDifference.createAddedTest(ri.testRuleName, i, newTest.getName(),
                    (JSObject) objData, (JSObject) objCustom));
          }
        }
        else
        if (oldTest != null && newTest == null) {
          Object objData = (oldTest.getParameters().size() >= 2) ? oldTest.getParameters().get(1) : null;
          Object objCustom = (oldTest.getParameters().size() >= 3) ? oldTest.getParameters().get(2) : null;
          if (objData != null) {
            results.add(ValidationTestDifference.createRemovedTest(ri.testRuleName, i, oldTest.getName(),
                    (JSObject) objData, (JSObject) objCustom));
          }
        }
        else
        if (oldTest != null && !areRulesTheSame(oldTest, newTest)) {
          Object objNewData = (newTest.getParameters().size() >= 2) ? newTest.getParameters().get(1) : null;
          Object objNewCustom = (newTest.getParameters().size() >= 3) ? newTest.getParameters().get(2) : null;
          Object objOldData = (oldTest.getParameters().size() >= 2) ? oldTest.getParameters().get(1) : null;
          Object objOldCustom = (oldTest.getParameters().size() >= 3) ? oldTest.getParameters().get(2) : null;
          if (objNewData != null && objOldData != null) {
            results.add(ValidationTestDifference.createChangedTest(ri.testRuleName, i,
                    oldTest.getName(), (JSObject) objOldData, (JSObject) objOldCustom,
                    newTest.getName(), (JSObject) objNewData, (JSObject) objNewCustom));
          }
        }
      }
    }
    return results;
  }
}
