package za.co.synthesis.rule.evaluation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 9/10/17.
 */
/*
       localCountryRegex: "[Zz][Aa]",
      localCurrencyRegex: "[Zz][Aa][Rr]",
      whoAmIRegex: "SBZAZA.*",
      onshoreRegex: "....ZA.*",
      onshoreADRegex: [
*/
public class Context {
  Logger logger = LoggerFactory.getLogger(Context.class);

  public static String THISBANK = "_THISBANK_";

  private String localCountryRegex;
  private String localCurrencyRegex;
  private String whoAmIRegex;
  private String onshoreRegex;
  private Boolean zz1Reportability = true;
  private final List<String> onshoreADRegexList = new ArrayList<String>();
  private final List<String> cmaRegexList = new ArrayList<String>();

  public void loadContext(Map jsContext) {
    Object objCountry = jsContext.get("localCountryRegex");
    Object objCurrency = jsContext.get("localCurrencyRegex");
    Object objWhoAmIMask = jsContext.get("whoAmIRegex");
    Object objOnshoreMask = jsContext.get("onshoreRegex");
    Object objOnshoreADMasks = jsContext.get("onshoreADRegex");
    Object objCMAMasks = jsContext.get("CMARegex");
    Object objZz1Reportability = jsContext.get("ZZ1Reportability");

    if (objCountry instanceof String) {
      localCountryRegex = (String)objCountry;
    }
    else
      localCountryRegex = "[Zz][Aa]";

    if (objCurrency instanceof String) {
      localCurrencyRegex = (String)objCurrency;
    }
    else
      localCurrencyRegex = "[Zz][Aa][Rr]";

    if (objWhoAmIMask instanceof String)
      whoAmIRegex = (String)objWhoAmIMask;
    else
      whoAmIRegex = "SBZAZA.*";

    if (objOnshoreMask instanceof String)
      onshoreRegex = (String)objOnshoreMask;
    else
      onshoreRegex = ".{4}[Zz][Aa].*";

    if (objOnshoreADMasks instanceof List) {
      for (Object mask : (List)objOnshoreADMasks) {
        if (mask instanceof String) {
          onshoreADRegexList.add((String)mask);
        }
      }
    }
    else {
      onshoreADRegexList.add("ABSAZA.*");
      onshoreADRegexList.add("FIRNZA.*");
      onshoreADRegexList.add("NEDSZA.*");
      onshoreADRegexList.add("SBZAZA.*");
      onshoreADRegexList.add("RNNSZA.*");
      onshoreADRegexList.add("CABLZA.*");
      onshoreADRegexList.add("IVESZA.*");
      onshoreADRegexList.add("YOUBZA.*");
    }

    if (objCMAMasks instanceof List) {
      for (Object mask : (List)objCMAMasks) {
        if (mask instanceof String) {
          cmaRegexList.add((String)mask);
        }
      }
    }

    if(objZz1Reportability instanceof Boolean){
      zz1Reportability = (Boolean)objZz1Reportability;
    }
  }

  public String getLocalCountryRegex() {
    return localCountryRegex;
  }

  public String getLocalCurrencyRegex() {
    return localCurrencyRegex;
  }

  public String getWhoAmIRegex() {
    return whoAmIRegex;
  }

  public String getOnshoreRegex() {
    return onshoreRegex;
  }

  public List<String> getOnshoreADRegexList() {
    return onshoreADRegexList;
  }

  public List<String> getCmaRegexList() {
    return cmaRegexList;
  }

  public Boolean getZz1Reportability() {
    return zz1Reportability;
  }
}
