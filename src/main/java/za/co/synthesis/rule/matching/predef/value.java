package za.co.synthesis.rule.matching.predef;

import za.co.synthesis.rule.core.IMatchingContext;
import za.co.synthesis.rule.matching.IShardFunction;

/**
 * Created by jake on 6/16/16.
 */
public class value implements IShardFunction {
  private String fieldname;

  public value(String fieldname) {
    this.fieldname = fieldname;
  }

  @Override
  public String[] execute(IMatchingContext context) {
    Object val = context.getField(fieldname);
    if (fieldname.equals("DrCr") && (val == null || val.equals("-"))) {
      String[] result = new String[2];
      result[0] = "D";
      result[1] = "C";
      return result;
    }
    else {
      String[] result = new String[1];
      result[0] = val != null ? val.toString() : "";
      return result;
    }
  }
}
