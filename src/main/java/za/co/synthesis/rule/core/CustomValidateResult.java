package za.co.synthesis.rule.core;

/**
 * User: jake
 * Date: 2/26/15
 * Time: 9:46 AM
 * This is the result object passed back by validation callbacks
 */
public class CustomValidateResult {
  private StatusType status;
  private String code;
  private String message;

  public CustomValidateResult(StatusType status, String code, String message) {
    this.status = status;
    this.code = code;
    this.message = message;
  }

  public CustomValidateResult(StatusType status) {
    this.status = status;
    if (status != StatusType.Pass) {
      this.code = "ERR";
      this.message = "Message and code needs to be set for validation results that do not pass";
    }
  }

  public StatusType getStatus() {
    return status;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
