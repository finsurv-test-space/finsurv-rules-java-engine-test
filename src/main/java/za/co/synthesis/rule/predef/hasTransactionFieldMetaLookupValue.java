package za.co.synthesis.rule.predef;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.impl.ValidationRuntimeException;
import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.IStringResolver;
import za.co.synthesis.rule.support.StringList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class hasTransactionFieldMetaLookupValue  implements IAssertionFunction {
    private String lookupName;
    private String lookupKey;
    private String field;

    public hasTransactionFieldMetaLookupValue(String lookupName, String lookupKey, String field) {
        this.lookupName = lookupName;
        this.lookupKey = lookupKey;
        this.field = field;
    }

    public boolean execute(FinsurvContext context, Object value) {
        if (value != null && value instanceof String) {
            Object fieldValue = context.getTransactionField(field);
            JSObject lookupValue = (JSObject) context.getCustomValue().get(lookupName);
            return lookupValue != null && lookupValue.get(lookupKey).equals(fieldValue);
        }
        return false;
    }
}
