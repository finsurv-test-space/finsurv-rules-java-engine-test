package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IStringResolver;

public class literal implements IStringResolver {
  private String constant;

  public literal(String constant) {
    this.constant = constant;
  }

  @Override
  public String resolve(FinsurvContext context, Object value) {
    return constant;
  }
}
