package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import java.math.BigDecimal;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 1:39 PM
 * Retu
 */
public class isGreaterThan implements IAssertionFunction {
  private BigDecimal constant;

  public isGreaterThan(String constant) {
    this.constant = Util.number(constant);
  }

  public boolean execute(FinsurvContext context, Object value) {
    BigDecimal numValue = Util.number(value);
    // BigDecimal.compareTo: -1, 0, or 1 as this BigDecimal is numerically less than, equal to, or greater than val.
    if (numValue != null)
      return numValue.compareTo(constant) == 1;
    return false;
  }
}
/*
function isGreaterThan(amount) {
  return function(context, value) {
    return value > amount;
  }
}

*/