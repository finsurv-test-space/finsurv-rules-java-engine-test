package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.StringList;

/**
 * User: jake
 * Date: 8/8/14
 * Time: 5:54 AM
 * Determines if all of the specified fields are missing (or have no value)
 */
public class isMissingField implements IAssertionFunction {
  private StringList fields = null;

  public isMissingField(String constant) {
    fields = new StringList();
    fields.add(constant);
  }

  public isMissingField(StringList constants) {
    this.fields = constants;
  }

  public boolean execute(FinsurvContext context, Object value) {
    if (value != null) {
      for (String field : fields) {
        if (context.hasField(value, field))
          return false;
      }
    }
    return true;
  }
}
/*
function isMissingField(fields) {
  return function(context, value) {
    if (value) {
      if (typeof fields === "string") {
        if (fields in value)
          return false;
      } else {
        for ( var i = 0; i < fields.length; i++) {
          if (fields[i] in value)
            return false;
        }
      }
    }
    return true;
  }
}
*/
